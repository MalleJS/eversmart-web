<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 */
class Auth extends ESE_Controller
{

    /**
     * Auth constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_status_model');
        $this->load->model('user_modal');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_redfish');
    }

    /**
     * Checks user credentials against database, if successful creates JSON Web Token
     *
     * @return mixed
     */
    function authenticate()
    {

        $this->account_id = new \stdClass();

        $username = $this->input->post('user_email');
        $password = $this->input->post('user_password');

        // Redfish
        $redfish = $this->curl_requests_redfish->login_check($username, $password);
        $response = $this->parseHttpResponse($redfish);

        if(isset($response['body']['Message']) && $response['body']['Message'] == 'SUCCESS') {

            // Grab the cookie
            if(isset($response['headers']['Set-Cookie'])) {
                $cookie = $this->parseCookieHeader($response['headers']['Set-Cookie']);
                $this->redfish_token = $cookie['account'];
            }

            $this->customer_type[] = 'redfish';
            $this->account_id->redfish = $response['body']['Data']['AccountNumbers'][0];

            $this->user_modal->activity_log($response['body']['Data']['AccountNumbers'][0], '1');
            $this->setup_token();
            exit;
        }


        // Junifer / Dyball
        $person_data = $this->user_status_model->is_active($username, $password);

        if (!empty($person_data)) {

            if($person_data['account_id'] == 0){
                echo json_encode([ 'success' => false, 'error' => true, 'msg'=>'Sorry, your online account is currently pending. Please allow 48 hours and attempt to login again.' ]);
                exit;
            }

            if ($person_data['billingMethod'] == 'prepay') {
                $this->customer_type[] = 'dyball';
                $this->account_id->dyball = $person_data['account_id'];
            } else {
                $this->customer_type[] = 'junifer';
                $this->account_id->junifer = $person_data['account_id'];
            }

            $this->user_modal->activity_log($person_data['id'], '1');
            $this->setup_token();
            exit;
        }

        // If we get this there wasn't a successful login
        echo json_encode([ 'success' => false, 'error' => true, 'msg'=>'Incorrect login credentials' ]);

    }

    /**
     * Generate token based on with an expiry 1 hour from now based on the information received
     *
     * @param null
     * @return string - json array
     */
    function setup_token()
    {
        $this->load->library('jwt');

        // Set up token vars
        $token['account_id'] = $this->account_id;
        $token['customer_type'] = $this->customer_type;
        $token['redfish_token'] = $this->redfish_token;
        $token['token_expiry'] = time() + 604800; // 7 days from now

        // Generate token
        $output['token'] = Jwt::encode($token, $this->salt);

        // return success and token
        echo json_encode(['success' => true, 'error' => false, 'token' => $output['token']]);
    }

    /**
     * Test
     */
    function check_login_status()
    {
        if($this->token_active_check()){
            echo 'OK';
        }
        else {
            echo ' NOT OK';
        }
    }

    /**
     * Does the MPAN match the logged in customers Junifer information?
     *
     * @param $mpan string
     * @return string - json array
     */
    function verify_account_mpan($mpan = null)
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        // Get logged in customers meter point ids
        $meter_point = $this->curl_requests_junifer->junifer_get_meterpoint($this->account_id->junifer);

        // If we have found an elec meter point id and want to compare it to MPAN
        if( isset($meter_point['meterpoint_elec_id']) && isset($mpan)  ) {

            // Get and return MPAN
            $junifer_mpan = $this->curl_requests_junifer->junifer_get_mpan_mprn($meter_point['meterpoint_elec_id']);

            // Return true if match, or false if not
            if($junifer_mpan === $mpan) {
                echo json_encode(['success' => true, 'error' => false]);
            }
            else {
                echo json_encode(['success' => false, 'error' => true, 'message' => $mpan . ' does not match MPAN found in Junifer - ' . $junifer_mpan]);
            }
        }
        else {
            echo json_encode(['success' => false, 'error' => true, 'message' => 'Data missing or not found']);
        }
    }

    /**
     * Does the MPRN match the logged in customers Junifer information?
     *
     * @param $mprn string
     * @return string - json array
     */
    function verify_account_mprn($mprn = null)
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        // Get logged in customers meter point ids
        $meter_point = $this->curl_requests_junifer->junifer_get_meterpoint($this->account_id->junifer);

        // If we have found an gas meter point id and want to compare it to MPRN
        if( isset($meter_point['meterpoint_gas_id']) && isset($mprn)  ) {

            // Get and return MPRN
            $junifer_mprn = $this->curl_requests_junifer->junifer_get_mpan_mprn($meter_point['meterpoint_gas_id']);

            // Return true if match, or false if not
            if($junifer_mprn === $mprn) {
                echo json_encode(['success' => true, 'error' => false]);
            }
            else {
                echo json_encode(['success' => false, 'error' => true, 'message' => $mprn . ' does not match MPRN found in Junifer - ' . $junifer_mprn]);
            }
        }
        else {
            echo json_encode(['success' => false, 'error' => true, 'message' => 'Data missing or not found']);
        }
    }




}
