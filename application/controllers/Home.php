<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ESE_Controller
{
	// public $body_classes = 'transparent-nav';
						
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['bodyClasses'] = $this->body_classes;
		$data['stylesheets'] = ["assets/css/new-world/home/landing-page.css"];
		$data['scripts'] = [];
		$data['content'] = 'new-world/home/landing-page';
		$this->load->view( 'new-world/master', $data );
	}
	
	function condition()
	{
		$this->load->view('layout/terms_conditions');
	}

	function policy()
	{
		$this->load->view('layout/privacy_policy');
	}
	
	function faqs()
	{
		$this->load->view('layout/help_faqs');
	}

	function career()
	{
		$this->load->view('layout/careers');
	}
}
