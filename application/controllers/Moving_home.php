<?php

/**
 * Class Moving_home
 */
class Moving_home extends ESE_Controller
{
    /**
     * Moving_home constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->library('email');
        $this->mysql = $this->load->database('mysql', true);
    }


    /**
     * Send moving home email to customer and Ops
     */
    function moving_home_email()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $MoveInfo = json_decode( file_get_contents( 'php://input' ), true );

        // Set up vars
        $id = $MoveInfo['id'];
        $junifer_id = $MoveInfo['account_id'];
        $forename = $MoveInfo['forename'];
        $surname = $MoveInfo['surname'];
        $recipient_email = $MoveInfo['email'];
        $token = md5(uniqid(rand(0,9999), true));
        $this->user_modal->token_in_database($token, $id);
        $new_address = $MoveInfo['new_add_street']. ', ' .$MoveInfo['new_add_town']. ', ' .$MoveInfo['new_add_city']. ', ' .$MoveInfo['new_postcode'];
        $moving_date = $MoveInfo['move_date'];
        $last_reading_elec = $MoveInfo['last_reading_elec'];
        $last_reading_gas = isset($MoveInfo['last_reading_gas']) ? $MoveInfo['last_reading_gas'] : '' ;


        // Email to Customer
        $this->customer_moving_home($token, $id, $junifer_id, $recipient_email, $forename, $new_address, $moving_date);

        // Email to Ops
        $this->ops_moving_home($token, $id, $junifer_id, $forename, $surname, $new_address, $moving_date, $last_reading_elec, $last_reading_gas);

        echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token() ]);

    }

    /**
     * Sends moving home email to customer
     *
     * @param $token
     * @param $id
     * @param $account_id
     * @param $recipient_email
     * @param $forename
     * @param $new_address
     * @param $moving_date
     */
    function customer_moving_home($token, $id, $account_id, $recipient_email, $forename, $new_address, $moving_date)
    {

        $email_data['email_info'] = [
            'token' => $token,
            'name' => $forename,
            'customer_number' => $account_id,
            'new_address' => $new_address,
            'moving_date' => $moving_date
        ];
        $email_data['email_header_img'] = 'switch_header.jpg';
        $email_data['content'] = 'layout/moving_home_email_content';
        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Moving home');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $id;
        $email_log_db_parameter['email_type_id'] = 20;

        // Insert into our db
        $this->user_modal->insert_email_log($email_log_db_parameter);
    }

    /**
     * Sends moving home email to Ops
     *
     * @param $token
     * @param $id
     * @param $junifer_id
     * @param $forename
     * @param $surname
     * @param $new_address
     * @param $moving_date
     * @param $last_reading_elec
     * @param $last_reading_gas
     * @param $customer_email
     */
    function ops_moving_home($token, $id, $junifer_id, $forename, $surname, $new_address, $moving_date, $last_reading_elec, $last_reading_gas)
    {

        $admin_email_data['email_info'] = [
            'token' => $token,
            'account_id' => $junifer_id,
            'forename' => $forename,
            'surname' => $surname,
            'new_address' => $new_address,
            'moving_date' => $moving_date,
            'last_reading_elec' => $last_reading_elec,
            'last_reading_gas' => $last_reading_gas
        ];

        $admin_email_data['email_header_img'] = 'switch_header.jpg';
        $admin_email_data['content'] = 'layout/moving_home_admin_email_content';
        $admin_template = $this->load->view('layout/email_template', $admin_email_data , true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to('movinghome@eversmartenergy.co.uk');
        $this->email->subject('Moving Home Request : '.$id);
        $this->email->message($admin_template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $id;
        $email_log_db_parameter['email_type_id'] = 23;

        // Insert into our db
        $this->user_modal->insert_email_log($email_log_db_parameter);
    }

}
