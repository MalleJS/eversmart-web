<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tariff extends ESE_Controller
{
    private $model = 'tariff_model';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->model);
    }

    function index()
    {
        $this->load->view('layout/tariff');
    }

    function get_all()
    {
        echo $this->tariff_model->all();
    }

    function get($id)
    {
        echo $this->tariff_model->get($id);
    }

    function create()
    {
        $input = json_decode(file_get_contents('php://input'),true);

        $data = [
            'tariff_name' => $input['tariff_name'],
            'tariff_code' => $input['tariff_code'],
            'tariff_type' => $input['tariff_type'],
            'is_visible' => $input['is_visible'],
            'created_at' => date("Y-m-d h:i:sa"),
        ];
        $response = $this->tariff_model->create($data);
        echo json_encode(['msg' => $response]);

    }

    function update($id)
    {
        $input = json_decode(file_get_contents('php://input'),true);

        $data = [
            'tariff_name' => $input['tariff_name'],
            'tariff_code' => $input['tariff_code'],
            'tariff_type' => $input['tariff_type'],
            'is_visible' => $input['is_visible'],
        ];
        $response = $this->tariff_model->update($id, $data);
        echo json_encode(['msg' => $response]);

    }

}
