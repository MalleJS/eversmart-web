<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends ESE_Controller {

    /**
     * Return index view for the Terms & Conditions page
     */
  function index()
  {
    $data['stylesheets'] = ['assets/css/new-world/help/terms_and_conditions.css'];
    $data['content'] = 'new-world/help/terms_and_conditions.php';
    $this->load->view('new-world/master', $data);
  }
}
