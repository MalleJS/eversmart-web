<?php

class Salesforce extends ESE_Controller
{
    /**
    * Functionality to create an Authorisation via SF API
    * @returns
    */
    function  create_auth()
    {
        $this->salesforce->create_auth();
    }

    /**
    * Get product from SF API via unique identifier
    * @param $id
    * @return String
    */
    function get_product($id)
    {
        echo json_decode( $this->salesforce->get_product($id));
    }

    /**
    * Get all products tagged 'Insurance Based' from Salesforce API
    * @return String
    */
    function get_insurance_products()
    {
        echo json_decode($this->salesforce->get_insurance_products());
    }

    /**
    * Get Product IPID data from Salesforce
    *
    * @param $id
    * @return String
    */
    function get_product_info($id)
    {
        echo json_decode($this->salesforce->get_ipid_data($id));
    }

}