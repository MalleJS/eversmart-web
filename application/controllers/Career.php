<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends ESE_Controller {
  function index(){

    // adding additional styles and scripts to the careers-page.php veiw
		$data['scripts'] = [base_url()."assets/js/new-world/about/careers-page.js"];
		$data['stylesheets'] = [
            "assets/css/new-world/about/careers-page.css"
        ];
    $data['content'] = 'new-world/about/careers-page';
    $data['bodyClasses'] = $this->body_classes;
		$this->load->view( $this->template, $data );

  }

  function jobs(){
    echo $this->curl->wordCurl('posts?categories=59');
  }

  function savetofolder()
  {
    if ( 0 < $_FILES['file']['error'] ) 
    {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else 
    {
      move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'\\upload\\CV_uploads\\'.$_FILES['file']['name']);
    }
  }

  function email()
  {
      $info['detail'] = array(  
      'Name'=>$this->input->post('name'),
      'Email'=>$this->input->post('email'),
      'Role'=>$this->input->post('role'),
    ); 

    if(!empty($this->input->post('CV')))
    {
      $Attachment = 'upload/CV_uploads/'.$this->input->post('CV');
    }

    $token = md5(uniqid(rand(0,9999), true));
    $email_data['email_info'] = [ 'token' => $token, 'name' => $this->session->userdata('login_data')['name']];
    $template = $this->load->view( 'layout/career_template', $info , true);
    $this->load->library('email');
    $this->email->from($this->input->post('email'), ucfirst($this->input->post('name')));
    $this->email->to('careers@eversmartenergy.co.uk');
    $this->email->subject('Thank You Message');
    $this->email->message($template);
    if($Attachment) 
    {
      $this->email->attach($Attachment);
    }
    $this->email->send();
			  
            //$token_in_database = $this->user_modal->token_in_database($token,$insert_customer_info);

}



}
