<?php
// Lock down to IP
$regex = "/172.31.\d{1,3}\.\d{1,3}/";
if (!preg_match($regex, $_SERVER['REMOTE_ADDR'])) {  exit('Error 1 - No direct script access allowed'); }

use \vendor\stripe;
require_once('./vendor/autoload.php');

class Cron extends ESE_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('api_modal');
        $this->load->model('saas_model');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_saasquatch');
        $this->mysql = $this->load->database('mysql',true);
    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart) {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if (strpos($lines[0], 'HTTP/1.1') !== false) {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0])) {
                    // 4xx or 5xx error
                    // so log the error
                    $this->logMessage('error', 'Line 0=' . $lines[0], 2);
                }

                foreach ($lines as $j => $line) {
                    if ($j === 0) {
                        $headers['http_code'] = $line;
                    } else {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            } // If last element, probably body
            else if ($i === sizeof($responseParts) - 1) {
                $body = json_decode($responsePart, true);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }

    public static function logMessage($errLevel, $msg, $depth = 1)
    {
        $btStack = debug_backtrace($depth);
        $callerFrame = $btStack[$depth - 1];

        $sid = substr(session_id(), 0, 8);
        $method = $callerFrame['class'] . '.' . $callerFrame['function'];
        $lineNo = $callerFrame['line'];
        $fullMessage = 'SID:' . $sid . ' ' . $method . '(' . $lineNo . '): ' . $msg;

        log_message($errLevel, $fullMessage);
    }


    // START ENROLL DYBALL METHODS
    /*
        function generate_dyball_import()
        {

            // Get any one who hasn't already been added to Dyball
            $DyballCustomerData = $this->get_new_dyball_customers();

            // Do we have any records?
            $DyballCustomerCount = count($DyballCustomerData);

            if($DyballCustomerCount>0) {
                // Yes, make the CSV (Attachment) and save file to location
                $Attachment = $this->generate_new_dyball_customer_csv($DyballCustomerData);

            }

            // Trigger email with attachment
            $forename = 'Dan & Matt';
            $recipient_email = 'dan.jones@eversmartenergy.co.uk';
            $cc_recipient_email = 'matthew.scrivens@eversmartenergy.co.uk';

            $email_data['email_info'] = ['name' => $forename, 'count' => $DyballCustomerCount];
            $email_data['email_header_img'] = 'account_header.jpg';
            $email_data['content'] = 'layout/new_dyball_customers_email_content';

            $template = $this->load->view('layout/email_template', $email_data, true);

            $this->load->library('email');
            $this->email->clear(true);
            $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
            $this->email->to($recipient_email);
            $this->email->cc($cc_recipient_email);
            $this->email->subject('Dyball Customer Import');
            $this->email->message($template);
            if (!empty($Attachment)) {

                // First Log attachments in db
                $elec_data['filename'] = $Attachment['elec_filename'];
                $elec_data['generated_file_type_id'] = 1;   // Elec
                $elec_data['affected_rows'] = $DyballCustomerCount;
                $this->mysql->insert('generated_file', $elec_data);

                $gas_data['filename'] = $Attachment['gas_filename'];
                $gas_data['generated_file_type_id'] = 2;    // Gas
                $gas_data['affected_rows'] = $Attachment['dual_counter'];
                $this->mysql->insert('generated_file', $gas_data);

                $this->email->attach($Attachment['elec_filename']);
                $this->email->attach($Attachment['gas_filename']);
            }
            $this->email->send();
        }

        function get_new_dyball_customers(){

            $DyballCustomerData = $this->mysql->select('*')->from('customer_info')
                ->where(array('billingMethod' => 'prepay', 'dyball_import_success' => NULL))
                ->get()->result_array();
            return $DyballCustomerData;
        }

        function generate_new_dyball_customer_csv($DyballCustomerData) {

            // Create filename
            $elec_filename = 'assets/temp_csv/elec_dyball_import_'.date('dmYHi').'.csv';
            $gas_filename = 'assets/temp_csv/gas_dyball_import_'.date('dmYHi').'.csv';

            // Set location
            $elec_file = fopen( $elec_filename, 'wb');
            $gas_file = fopen( $gas_filename, 'wb');


            // Set the column headers -
            $DyballCustomerHeadings = array();
            $DyballCustomerHeadings[] = 'TYPE';
            $DyballCustomerHeadings[] = 'CUSTID';
            $DyballCustomerHeadings[] = 'NAME';
            $DyballCustomerHeadings[] = 'Profile Class';
            $DyballCustomerHeadings[] = 'MTC';
            $DyballCustomerHeadings[] = 'LLF';
            $DyballCustomerHeadings[] = 'SSC';
            $DyballCustomerHeadings[] = 'Energisation Status';
            $DyballCustomerHeadings[] = 'MPAN';
            $DyballCustomerHeadings[] = 'SP'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'START';
            $DyballCustomerHeadings[] = 'CONTACTNAME';
            $DyballCustomerHeadings[] = 'CONTACTTEL';
            $DyballCustomerHeadings[] = 'CONTACTFAX'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'RETRIEVALMETHOD';
            $DyballCustomerHeadings[] = 'REGULARREADCYCLE';
            $DyballCustomerHeadings[] = 'ESTANNUALCONSUMPTION';
            $DyballCustomerHeadings[] = 'MEASUREMENTCLASSID';
            $DyballCustomerHeadings[] = 'METERPOINTADD1'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'METERPOINTADD2'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'House Number';
            $DyballCustomerHeadings[] = 'METERPOINTADD4'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Street';
            $DyballCustomerHeadings[] = 'METERPOINTADD6'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Locality';
            $DyballCustomerHeadings[] = 'Town';
            $DyballCustomerHeadings[] = 'County';
            $DyballCustomerHeadings[] = 'Post Code';
            $DyballCustomerHeadings[] = 'MAILADD1'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'MAILADD2'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'House Number';
            $DyballCustomerHeadings[] = 'MAILADD4'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Street';
            $DyballCustomerHeadings[] = 'MAILADD6'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'MAILADD7'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Town';
            $DyballCustomerHeadings[] = 'County';
            $DyballCustomerHeadings[] = 'MAILPOSTCODE';
            $DyballCustomerHeadings[] = 'DCAGENT';
            $DyballCustomerHeadings[] = 'DCAGTYPE';
            $DyballCustomerHeadings[] = 'DCCONTREF';
            $DyballCustomerHeadings[] = 'DCSERVREF';
            $DyballCustomerHeadings[] = 'DCSERVLEVREF';
            $DyballCustomerHeadings[] = 'MOAGENT';
            $DyballCustomerHeadings[] = 'MOAGTYPE';
            $DyballCustomerHeadings[] = 'MOCONTREF';
            $DyballCustomerHeadings[] = 'MOSERVREF';
            $DyballCustomerHeadings[] = 'MOSERVLEVREF';
            $DyballCustomerHeadings[] = 'DAAGENT';
            $DyballCustomerHeadings[] = 'DAAGTYPE';
            $DyballCustomerHeadings[] = 'DACONTREF';
            $DyballCustomerHeadings[] = 'DASERVREF';
            $DyballCustomerHeadings[] = 'DASERVLEVREF';
            $DyballCustomerHeadings[] = 'COTIND'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'DELMAILADDHELD'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'CUSTPASSWORD'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'CUSTPASSEFFDATE'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'MAXPOWERREQ'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'SPECIALACCESS'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'ADDITIONALINFO'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'SPECIALNEEDSIND'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Salesman'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Email'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pps contact'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pps phone1'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pps phone2'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'alternate contact name'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'alternate phone1'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'alternate phone2'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress1'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress2'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress3'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress4'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress5'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress6'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress7'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress8'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'pscaddress9'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'psc postcode'; // ## NOT REQUIRED ##
            $DyballCustomerHeadings[] = 'Special Needs Additional Info'; // ## NOT REQUIRED ##

            fputcsv($elec_file, $DyballCustomerHeadings);
            fputcsv($gas_file, $DyballCustomerHeadings);


            // Update user customer record success flag
            $db_parameter = ['dyball_import_success' => 1 ];

            // Initiate the gas counter
            $DualCustomer = 0;

            // output each row of the data
            foreach ($DyballCustomerData as $row) {



                // WE WILL NEED TO AMEND THE DATA AT THIS STAGE
                $DyballCustomer = array();
                $DyballCustomer[] = '';             // ## NOT REQUIRED (may need to be 'NC' for new connection) ## - TYPE
                $DyballCustomer[] = $row['id'];     // CUSTID
                $DyballCustomer[] = $row['title']. ' ' .$row['forename']. ' ' .$row['surname']; //NAME
                $DyballCustomer[] = substr($row['electricityProduct_mpans'], 1, 1); //Profile Class
                $DyballCustomer[] = substr($row['electricityProduct_mpans'], 2, 3); //MTC
                $DyballCustomer[] = substr($row['electricityProduct_mpans'], 5, 3); //LLF
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - SSC
                $DyballCustomer[] = 'D';            // ## HARDCODED ## - Energisation Status
                $DyballCustomer[] = $row['elec_mpan_core']; //MPAN
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - SP
                $DyballCustomer[] = date('d/m/Y', strtotime($row['start_date'])); // START
                $DyballCustomer[] = $row['forename']. ' ' .$row['surname']; //CONTACTNAME
                $DyballCustomer[] = $row['phone1']; //CONTACTTEL
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - CONTACTFAX
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - RETRIEVALMETHOD
                $DyballCustomer[] = 'Q';            // ## HARDCODED ## - REGULARREADCYCLE
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - ESTANNUALCONSUMPTION
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MEASUREMENTCLASSID
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - METERPOINTADD1
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - METERPOINTADD2
                $DyballCustomer[] = $row['first_line_address']; // House Number
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - METERPOINTADD4
                $DyballCustomer[] = $row['dplo_address']; // Street
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - METERPOINTADD6
                $DyballCustomer[] = $row['town_address']; // Locality
                $DyballCustomer[] = $row['town_address']; // Town
                $DyballCustomer[] = $row['city_address']; // County
                $DyballCustomer[] = $row['address_postcode']; //Post Code
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAILADD1
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAILADD2
                $DyballCustomer[] = $row['first_line_address']; //House Number
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAILADD4
                $DyballCustomer[] = $row['dplo_address']; // Street
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAILADD6
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAILADD7
                $DyballCustomer[] = $row['town_address']; // Town
                $DyballCustomer[] = $row['city_address']; // County
                $DyballCustomer[] = $row['address_postcode']; // MAILPOSTCODE
                $DyballCustomer[] = 'UDMS';         // ## HARDCODED ## - DCAGENT
                $DyballCustomer[] = 'N';            // ## HARDCODED ## - DCAGTYPE
                $DyballCustomer[] = 'ENGQNDC001';   // ## HARDCODED ## - DCCONTREF
                $DyballCustomer[] = 'ENDC';         // ## HARDCODED ## - DCSERVREF
                $DyballCustomer[] = 'NDC1';         // ## HARDCODED ## - DCSERVLEVREF
                $DyballCustomer[] = 'FUML';         // ## HARDCODED ## - MOAGENT
                $DyballCustomer[] = 'N';            // ## HARDCODED ## - MOAGTYPE
                $DyballCustomer[] = 'EVERSMTE01';   // ## HARDCODED ## - MOCONTREF
                $DyballCustomer[] = 'ESS1';         // ## HARDCODED ## - MOSERVREF
                $DyballCustomer[] = 'ESL1';         // ## HARDCODED ## - MOSERVLEVREF
                $DyballCustomer[] = 'UDMS';         // ## HARDCODED ## - DAAGENT
                $DyballCustomer[] = 'N';            // ## HARDCODED ## - DAAGTYPE
                $DyballCustomer[] = 'ENGQNDA001';   // ## HARDCODED ## - DACONTREF
                $DyballCustomer[] = 'ENDA';         // ## HARDCODED ## - DASERVREF
                $DyballCustomer[] = 'NDA1';         // ## HARDCODED ## - DASERVLEVREF
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - COTIND
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - DELMAILADDHELD
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - CUSTPASSWORD
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - CUSTPASSEFFDATE
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - MAXPOWERREQ
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - SPECIALACCESS
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - ADDITIONALINFO
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - SPECIALNEEDSIND
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - Salesman
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - Email
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pps contact
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pps phone1
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pps phone2
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - alternate contact name
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - alternate phone1
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - alternate phone2
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress1
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress2
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress3
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress4
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress5
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress6
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress7
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress8
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - pscaddress9
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - psc postcode
                $DyballCustomer[] = '';             // ## NOT REQUIRED ## - Special Needs Additional Info


                // If dyb_dual customer
                if($row['prefix'] == 'DYB_DUAL_') {
                    if(fputcsv($gas_file, $DyballCustomer)){

                        // Increment Dual Cust count for db
                        $DualCustomer++;
                    }
                    else {
                        die('Dual - Something happened during the write of the line '. $row['id']);
                    }
                }

                // Everyone goes into elec
                if(fputcsv($elec_file, $DyballCustomer)){

                    // Update record
                    $this->user_modal->update_customer_info($db_parameter,$row['id']);
                }
                else {
                    die('Elec - Something happened during the write of the line '. $row['id']);
                }

            }

            return array('elec_filename' => $elec_filename, 'gas_filename' => $gas_filename, 'dual_counter' => $DualCustomer);
        }
    */
    // END ENROLL DYBALL METHOD

    // START MONTHLY INTEREST METHODS
    function get_monthly_interest_customers()
    {

        // Get anyone who has signed up within the last year
        $MICustomerData = $this->mysql->query('SELECT id, account_id FROM `customer_info` where `monthly_interest_customer_signup_date` >= DATE_SUB(NOW(),INTERVAL 1 YEAR)')->result_array();
        return $MICustomerData;
    }

    function monthly_interest_calculation()
    {

        // Get any one who is a monthly interest customer who has signed up within a year
        $MICustomerData = $this->get_monthly_interest_customers();

        // Do we have any records?
        $MICustomerCount = count($MICustomerData);

        if($MICustomerCount>0) {

            // loop through each row of the data
            foreach ($MICustomerData as $row) {

                // Get month end total credit
                $get_balance_junifer = $this->curl->junifer_request('/rest/v1/accounts/'.$row['account_id'].'');
                $get_balance_junifer = $this->parseHttpResponse($get_balance_junifer);

                // If we find the balance, work out the percentage
                if($get_balance_junifer['headers']['http_code'] == 'HTTP/1.1 200 OK') {

                    // 1% Calculation
                    $MonthlyInterest = ($get_balance_junifer['body']['balance']>0?0:(abs($get_balance_junifer['body']['balance']) / 100));

                    // Set up parameters
                    $db_parameter = ['customer_id' => $row['id'], 'monthly_interest' => $MonthlyInterest ];

                    // Add record to database
                    $this->user_modal->insert_customer_monthly_interest($db_parameter);
                }
            }


        }


    }


    // START JUNIFER YEARLY PAYMENTS - NEW CUSTOMERS
    function customer_payment()
    {

        set_time_limit(0);

        // find any registered yearly pay customers that registered at least 14 days ago AND have not had a successful / failed payment already
        $CustomerData = $this->find_yearly_pay_customers();

        // Do we have any records?
        $CustomerCount = count($CustomerData);

        if($CustomerCount>0) {

            $currency = 'gbp';
            $stripe = array(
                "secret_key"      => $this->config->item('stripe')['sk_test'],
                "publishable_key" => $this->config->item('stripe')['pk_test']
            );

            // loop through each customer record
            foreach ($CustomerData as $row) {


                //extract data from the post
                $url = 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
                $fields = array(
                    'orgid' => urlencode('00D1l0000008d2T'),
                    'name' => urlencode($row['title'] . ' ' . $row['forename'] . ' ' . $row['surname']),
                    'email' => urlencode($row['email']),
                    'phone' => urlencode($row['phone1']),
                    'dob' => urlencode($row['dateOfBirth']),
                    'address' => urlencode($row['first_line_address']),
                    'postcode' => urlencode($row['address_postcode']),
                    'junifer_customer_id' => urlencode($row['account_id']),
                    'dyball_account_id' => urlencode(''),
                    'external_system_registration_status' => urlencode('Other')
                );

                //url-ify the data for the POST
                $fields_string = '';

                // use email to find token (card)
                try {

                    \Stripe\Stripe::setApiKey($stripe['secret_key']);

                    // Does the customer already exist?
                    $customer = \Stripe\Customer::all(array("email" => $row['email'], "limit" => 1));

                    // If we find a record
                    if(!empty($customer['data'])) {

                        // Remove comma
                        $row['tariff_newspend'] = str_replace(',','',$row['tariff_newspend']);

                        $stripe_customer_id = $customer['data'][0]['id'];
                        $stripe_customer_token = $customer['data'][0]['default_source'];
                        $stripe_amount = $row['tariff_newspend'] * 100;

                        // Make sure there was a token (card) set up
                        if($stripe_customer_token != '') {

                            try {

                                $charge = \Stripe\Charge::create(array(
                                    'customer' => $stripe_customer_id,
                                    'amount' => $stripe_amount,
                                    'source' => $stripe_customer_token,
                                    'currency' => $currency,
                                ));

                            }
                            catch (Stripe_Error $e) {

                                // Update customer record if payment failed
                                $db_parameter = ['first_payment_status_type_id' => '2'];
                                $this->user_modal->update_customer_info($db_parameter, $row['id']);

                                // SEND WEB TO CASE - open connection
                                $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                                $fields['description'] = urlencode('Failed Payment - ' . $e->getMessage() . ' for ' . $row['forename'] . ' ' . $row['surname']);

                                foreach ($fields as $key => $value) {
                                    $fields_string .= $key . '=' . $value . '&';
                                }
                                $fields_string = rtrim($fields_string, '&');

                                $ch = curl_init();
                                if ($_SERVER['HTTP_HOST'] == '127.0.0.1:8000') {
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                }
                                //set the url, number of POST vars, POST data
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, count($fields));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                                //execute post
                                $result = curl_exec($ch);

                                //close connection
                                curl_close($ch);

                                // Jump to next loop
                                continue;

                            }
                            catch (Exception  $e) {


                                // Update customer record if payment failed
                                $db_parameter = ['first_payment_status_type_id' => '2'];
                                $this->user_modal->update_customer_info($db_parameter, $row['id']);

                                // SEND WEB TO CASE - open connection
                                $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                                $fields['description'] = urlencode('Failed Payment - ' . $e->getMessage() . ' for ' . $row['forename'] . ' ' . $row['surname']);

                                foreach ($fields as $key => $value) {
                                    $fields_string .= $key . '=' . $value . '&';
                                }
                                $fields_string = rtrim($fields_string, '&');

                                $ch = curl_init();
                                if ($_SERVER['HTTP_HOST'] == '127.0.0.1:8000') {
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                }
                                //set the url, number of POST vars, POST data
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, count($fields));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                                //execute post
                                curl_exec($ch);

                                //close connection
                                curl_close($ch);

                                // Jump to next loop
                                continue;

                            }

                        }

                        // on success - NO web to case required - send success email to customer
                        if ($charge['status'] == 'succeeded' && isset($stripe_customer_id)) {

                            // Update user customer record payment success flag
                            $db_parameter = ['first_payment_status_type_id' => '1'];
                            $this->user_modal->update_customer_info($db_parameter, $row['id']);

                            $response['account_id'] = $row['account_id'];
                            $response['amount'] = $row['tariff_newspend'];

                            $row = [
                                'transaction_id' => $charge['id'],
                                'amount' => $response['amount'],
                                'currency' => $currency,
                                'status' => $charge['status'],
                                'account_id' => $response['account_id']
                            ];

                            $post = [
                                "grossAmount" => $response['amount'],
                                "salesTaxName" => "Standard VAT",
                                "accountCreditReasonName" => "Stripe Payment"
                            ];

                            $junifer_credit = $this->curl->junifer_request('/rest/v1/accounts/'.$response['account_id'].'/accountCredits', $post, 'post');

                            $get_head = $this->parseHttpResponse($junifer_credit);

                            $this->api_modal->save_transaction($row);

                            if ($get_head['headers']['http_code'] != 'HTTP/1.1 204 No Content') {
                                // Send error to Jack
                                $headers = 'From: debug@eversmartenergy.co.uk' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                                mail('jack.akrigg@eversmartenergy.co.uk', 'Junifer accountCredits issue', 'customer_payment(): '.print_r($get_head,1), $headers);
                            }

                        } // on fail - web to case - send failed email (unless salesforce can handle this HAHAHAHAHA)
                        else {

                            // Update customer record if payment failed
                            $db_parameter = ['first_payment_status_type_id' => '2'];
                            $this->user_modal->update_customer_info($db_parameter, $row['id']);

                            // SEND WEB TO CASE - open connection
                            $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                            $fields['description'] = urlencode('Failed Payment - Charge failed in Stripe for ' . $row['forename'] . ' ' . $row['surname']);

                            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                            $fields_string = rtrim($fields_string, '&');

                            $ch = curl_init();
                            if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            }
                            //set the url, number of POST vars, POST data
                            curl_setopt($ch,CURLOPT_URL, $url);
                            curl_setopt($ch,CURLOPT_POST, count($fields));
                            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                            //execute post
                            $result = curl_exec($ch);

                            //close connection
                            curl_close($ch);
                        }
                    } else {

                        // Update customer record if no customer found
                        $db_parameter = ['first_payment_status_type_id' => '2'];
                        $this->user_modal->update_customer_info($db_parameter, $row['id']);

                        // SEND WEB TO CASE - open connection
                        $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                        $fields['description'] = urlencode('Failed Payment - No account found for ' . $row['email']);

                        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                        $fields_string = rtrim($fields_string, '&');

                        $ch = curl_init();
                        if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        }
                        //set the url, number of POST vars, POST data
                        curl_setopt($ch,CURLOPT_URL, $url);
                        curl_setopt($ch,CURLOPT_POST, count($fields));
                        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                        //execute post
                        $result = curl_exec($ch);

                        //close connection
                        curl_close($ch);
                    }

                } catch( Stripe_Error $e ) {

                    // Update customer record if stripe failed
                    $db_parameter = ['first_payment_status_type_id' => '2' ];
                    $this->user_modal->update_customer_info($db_parameter, $row['id']);

                    // SEND WEB TO CASE - open connection
                    $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                    $fields['description'] = urlencode('Failed Payment - Stripe failed for ' . $row['forename'] . ' ' . $row['surname']);

                    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                    $fields_string = rtrim($fields_string, '&');

                    $ch = curl_init();
                    if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    }

                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, count($fields));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                    //execute post
                    curl_exec($ch);

                    //close connection
                    curl_close($ch);
                }

            } // End customer loop

        } // End if customers found

    }

    function find_yearly_pay_customers()
    {

        // find any registered customer (api_user_status = 1) yearly pay customers( monthly_interest_customer_signup_date IS NOT NULL)
        // that registered at least 14 days ago AND have not had a successful / failed payment already
        $CustomerData = $this->mysql->select('id, account_id, title, forename, surname, email, phone1, dateOfBirth, first_line_address, address_postcode, tariff_newspend')->from('customer_info')
            ->where('monthly_interest_customer_signup_date IS NOT NULL 
                AND UNIX_TIMESTAMP(`monthly_interest_customer_signup_date`) < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 14 DAY))
                AND first_payment_status_type_id IS NULL')
            ->get()->result_array();
        return $CustomerData;

    }

    // END JUNIFER YEARLY PAYMENTS - NEW CUSTOMERS

    // START JUNIFER YEARLY PAYMENTS - EXISTING CUSTOMERS
    function find_existing_yearly_pay_customers()
    {

        // find any registered customer (api_user_status = 1) yearly pay customers( monthly_interest_customer_signup_date IS NOT NULL)
        // that have not had a successful / failed payment already
        $CustomerData = $this->mysql->select('*')->from('customer_info')->where('monthly_interest_customer_signup_date IS NOT NULL AND first_payment_status_type_id IS NULL')->get()->result_array();

        $ExistingCustomers = array();

        // Loop through customer data to check whether they already exist
        foreach ($CustomerData as $row) {
            $Matches = $this->mysql->select('*')->from('existing_customer')->where('LOWER(CUSTOMER_NAME) LIKE \'%'.strtolower($row['forename']).' '.strtolower($row['surname']).'%\' 
            AND LOWER(CONTACT_ADDRESS_1) LIKE \'%'.strtolower($row['first_line_address']).'%\' ')->get()->first_row('array');

            if(count($Matches)>0){
                $ExistingCustomers[] = $row;
            }
        }

        return $ExistingCustomers;
    }

    function existing_customer_payment() {

        set_time_limit(0);

        // find any registered yearly pay customers that have not had a successful / failed payment already
        $CustomerData = $this->find_existing_yearly_pay_customers();

        // Do we have any records?
        $CustomerCount = count($CustomerData);

        if($CustomerCount>0) {

            $currency = 'gbp';
            $stripe = array(
                "secret_key"      => $this->config->item('stripe')['sk_test'],
                "publishable_key" => $this->config->item('stripe')['pk_test']
            );

            // loop through each customer record
            foreach ($CustomerData as $key => $row) {

                // Remove comma
                $row['tariff_newspend'] = str_replace(',','',$row['tariff_newspend']);

                //extract data from the post
                $url = 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
                $fields = array(
                    'orgid' => urlencode('00D1l0000008d2T'),
                    'name' => urlencode($row['title'] . ' ' . $row['forename'] . ' ' . $row['surname']),
                    'email' => urlencode($row['email']),
                    'phone' => urlencode($row['phone1']),
                    'dob' => urlencode($row['dateOfBirth']),
                    'address' => urlencode($row['first_line_address']),
                    'postcode' => urlencode($row['address_postcode']),
                    'junifer_customer_id' => urlencode($row['account_id']),
                    'dyball_account_id' => urlencode(''),
                    'external_system_registration_status' => urlencode('Other')
                );

                //url-ify the data for the POST
                $fields_string = '';

                // use email to find token (card)
                try {

                    \Stripe\Stripe::setApiKey($stripe['secret_key']);

                    // Does the customer already exist?
                    $customer = \Stripe\Customer::all(array("email" => $row['email'], "limit" => 1));

                    // If we find a record
                    if(!empty($customer['data'])) {

                        $stripe_customer_id = $customer['data'][0]['id'];
                        $stripe_customer_token = $customer['data'][0]['default_source'];
                        $stripe_amount = $row['tariff_newspend'] * 100;

                        // Make sure there was a token (card) set up
                        if($stripe_customer_token !== '') {

                            try {

                                $charge = \Stripe\Charge::create(array(
                                    'customer' => $stripe_customer_id,
                                    'amount' => $stripe_amount,
                                    'source' => $stripe_customer_token,
                                    'currency' => $currency,
                                ));

                            } catch (Stripe_Error $e) {

                                // Update customer record if payment failed
                                $db_parameter = ['first_payment_status_type_id' => '2'];
                                $this->user_modal->update_customer_info($db_parameter, $row['id']);

                                // SEND WEB TO CASE - open connection
                                $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                                $fields['description'] = urlencode('Failed Payment - ' . $e->getMessage() . ' for ' . $row['forename'] . ' ' . $row['surname']);

                                foreach ($fields as $key => $value) {
                                    $fields_string .= $key . '=' . $value . '&';
                                }
                                $fields_string = rtrim($fields_string, '&');

                                $ch = curl_init();
                                if ($_SERVER['HTTP_HOST'] == '127.0.0.1:8000') {
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                }
                                //set the url, number of POST vars, POST data
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, count($fields));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                                //execute post
                                curl_exec($ch);

                                //close connection
                                curl_close($ch);

                                // Jump to next loop
                                continue;

                            }
                            catch (Exception  $e) {

                                // Update customer record if payment failed
                                $db_parameter = ['first_payment_status_type_id' => '2'];
                                $this->user_modal->update_customer_info($db_parameter, $row['id']);

                                // SEND WEB TO CASE - open connection
                                $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                                $fields['description'] = urlencode('Failed Payment - ' . $e->getMessage() . ' for ' . $row['forename'] . ' ' . $row['surname']);

                                foreach ($fields as $key => $value) {
                                    $fields_string .= $key . '=' . $value . '&';
                                }
                                $fields_string = rtrim($fields_string, '&');

                                $ch = curl_init();
                                if ($_SERVER['HTTP_HOST'] == '127.0.0.1:8000') {
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                }
                                //set the url, number of POST vars, POST data
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, count($fields));
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                                //execute post
                                curl_exec($ch);

                                //close connection
                                curl_close($ch);

                                // Jump to next loop
                                continue;
                            }

                        }

                        // on success - NO web to case required - send success email to customer
                        if ( isset($charge['status']) && $charge['status'] == 'succeeded' && isset($stripe_customer_id) ) {

                            // Update user customer record payment success flag
                            $db_parameter = ['first_payment_status_type_id' => '1'];
                            $this->user_modal->update_customer_info($db_parameter, $row['id']);

                            $response['account_id'] = $row['account_id'];
                            $response['amount'] = $row['tariff_newspend'];

                            $row = [
                                'transaction_id' => $charge['id'],
                                'amount' => $response['amount'],
                                'currency' => $currency,
                                'status' => $charge['status'],
                                'account_id' => $response['account_id']
                            ];

                            $post = [
                                "grossAmount" => $response['amount'],
                                "salesTaxName" => "Standard VAT",
                                "accountCreditReasonName" => "Stripe Payment"
                            ];

                            $junifer_credit = $this->curl->junifer_request('/rest/v1/accounts/'.$response['account_id'].'/accountCredits', $post, 'post');

                            $get_head = $this->parseHttpResponse($junifer_credit);

                            $this->api_modal->save_transaction($row);

                            if ($get_head['headers']['http_code'] != 'HTTP/1.1 204 No Content') {
                                // Send error to Jack
                                $headers = 'From: debug@eversmartenergy.co.uk' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                                mail('jack.akrigg@eversmartenergy.co.uk', 'Junifer accountCredits issue', 'existing_customer_payment(): '.print_r($get_head,1), $headers);
                            }

                        } else {

                            // Update customer record if payment failed
                            $db_parameter = ['first_payment_status_type_id' => '2'];
                            $this->user_modal->update_customer_info($db_parameter, $row['id']);

                            // SEND WEB TO CASE - open connection
                            $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                            $fields['description'] = urlencode('Failed Payment - Charge failed in Stripe for ' . $row['forename'] . ' ' . $row['surname']);

                            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                            $fields_string = rtrim($fields_string, '&');

                            $ch = curl_init();
                            if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            }
                            //set the url, number of POST vars, POST data
                            curl_setopt($ch,CURLOPT_URL, $url);
                            curl_setopt($ch,CURLOPT_POST, count($fields));
                            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                            //execute post
                            $result = curl_exec($ch);

                            //close connection
                            curl_close($ch);
                        }

                    } else {

                        // Update customer record if no customer found
                        $db_parameter = ['first_payment_status_type_id' => '2'];
                        $this->user_modal->update_customer_info($db_parameter, $row['id']);

                        // SEND WEB TO CASE - open connection
                        $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                        $fields['description'] = urlencode('Failed Payment - No account found for ' . $row['email']);

                        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                        $fields_string = rtrim($fields_string, '&');


                        $ch = curl_init();
                        if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        }
                        //set the url, number of POST vars, POST data
                        curl_setopt($ch,CURLOPT_URL, $url);
                        curl_setopt($ch,CURLOPT_POST, count($fields));
                        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                        //execute post
                        $result = curl_exec($ch);

                        //close connection
                        curl_close($ch);
                    }

                } catch( Stripe_Error $e ) {

                    // Update customer record if stripe failed
                    $db_parameter = ['first_payment_status_type_id' => '2'];
                    $this->user_modal->update_customer_info($db_parameter, $row['id']);

                    // SEND WEB TO CASE - open connection
                    $fields['subject'] = urlencode('Failed Payment - Junifer Customer ID:' . $row['account_id'] . ' - Failed');
                    $fields['description'] = urlencode('Failed Payment - Stripe failed for ' . $row['forename'] . ' ' . $row['surname']);

                    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                    $fields_string = rtrim($fields_string, '&');

                    $ch = curl_init();
                    if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ) {
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    }
                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, count($fields));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

                    //execute post
                    $result = curl_exec($ch);

                    //close connection
                    curl_close($ch);

                }

            } // End customer loop

        } // End if customers found

    }
    // END JUNIFER YEARLY PAYMENTS - EXISTING CUSTOMERS

    // START - Simply Smart to Family Saver Club tariff (along with duosid, unit rates, standing charges and prod codes)
    function lookup_yearly_tariff_data($dist_id, $rate_type)
    {
        if($rate_type=='STD') { $rate_type = '1R'; }
        return $this->mysql->select('*')->from('new_yearly_tariff')->where('ratetype',$rate_type)->where('duosid',$dist_id)->get()->first_row('array');
    }

    function get_data_fix_customers()
    {
        // pick up the bad customer data
        return $this->mysql->select('*')->from('customer_info')->where('
        tariff_tariffName = "Simply Smart" 
        AND monthly_interest_customer_signup_date IS NOT NULL 
        AND first_payment_status_type_id IS NULL')->get()->result_array();
    }

    /*
        function update_customer_record($id, $data) {

            return $this->mysql->where('id',$id)->update('customer_info', $data);
        }

        function run_data_fix_loop() {

            // pick up the bad customer data
            $GetCustomers = $this->get_data_fix_customers();

            // start loop
            foreach ($GetCustomers as $data ) {

                // Get dist id
                $dist_paramter['postcode'] = $data['address_postcode'];
                $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_paramter, 'post'), true);

                // If not found default to 17
                if(!empty($dist_response['data']['entriesFromMpanFields'])) {
                    $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
                    if($ElecDistId>=24){ $ElecDistId = 17; }
                }
                else {
                    $ElecDistId = 17;
                }

                // Get yearly tariff
                $YearlyTariff = $this->lookup_yearly_tariff_data($ElecDistId, '1R');


                // Set db params
                $db_parameter = [
                    'tariff_tariffName' => $YearlyTariff['productname'],
                    'duosid' => $ElecDistId,
                    'tariff_standingCharge1Elec' => $YearlyTariff['electricity_sc'],
                    'tariff_unitRate1Elec' => $YearlyTariff['electricity_dur'],
                    'electricityProduct_productCode' => $YearlyTariff['productname_elec']
                ];

                // For Dual customers
                if($data['gasProduct_mprns'] != 'na')
                {
                    $db_parameter['gasProduct_productCode'] = $YearlyTariff['productname_gas'];
                    $db_parameter['tariff_standingCharge1Gas'] = $YearlyTariff['gas_sc'];
                    $db_parameter['tariff_unitRate1Gas'] = $YearlyTariff['gas_ur'];
                }

                // do we have gas deets to work out the correct newspend
                if($data['gasProduct_mprns'] != 'na' && $data['quotation_aq'] > 0)
                {
                    $EstimatedGas = (($YearlyTariff['gas_sc'] * 365 ) + ($data['quotation_aq'] * $YearlyTariff['gas_ur']));
                }
                else
                {
                    $EstimatedGas = 0;
                }

                // If we have elec deets - update newspend
                if($data['quotation_eac'] > 0)
                {
                    $EstimatedElec = (($YearlyTariff['electricity_sc'] * 365 ) + ($data['quotation_eac'] * $YearlyTariff['electricity_dur']));
                    $NewPrice = number_format(($EstimatedElec + $EstimatedGas),2);
                    $db_parameter['tariff_newspend'] = $NewPrice;
                }

                $this->update_customer_record($data['id'], $db_parameter);

            } // End loop
        }

        function datafix_edit()
        {

            // pick up the bad customer data
            $GetCustomers = $this->get_data_fix_customers();

            // start loop
            foreach ($GetCustomers as $data ) {

                // Get dist id
                $dist_paramter['postcode'] = $data['address_postcode'];
                $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_paramter, 'post'), true);

                // If not found default to 17
                if(!empty($dist_response['data']['entriesFromMpanFields'])) {
                    $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
                    if($ElecDistId>=24){ $ElecDistId = 17; }
                }
                else {
                    $ElecDistId = 17;
                }

                // Get yearly tariff
                $YearlyTariff = $this->lookup_yearly_tariff_data($ElecDistId, '1R');


                // Set db params
                $db_parameter = [
                    'tariff_tariffName' => 'Loyal Family Saver',
                    'duosid' => $ElecDistId,
                    'tariff_standingCharge1Elec' => ($YearlyTariff['electricity_sc'] * 100),
                    'tariff_unitRate1Elec' => ($YearlyTariff['electricity_dur'] * 100),
                    'electricityProduct_productCode' => $YearlyTariff['productname_elec']
                ];

                // For Dual customers
                if($data['gasProduct_mprns'] != 'na')
                {
                    $db_parameter['gasProduct_productCode'] = $YearlyTariff['productname_gas'];
                    $db_parameter['tariff_standingCharge1Gas'] = ($YearlyTariff['gas_sc'] * 100);
                    $db_parameter['tariff_unitRate1Gas'] = ($YearlyTariff['gas_ur'] * 100);
                }

                // do we have gas deets to work out the correct newspend
                //if($data['gasProduct_mprns'] != 'na' && $data['quotation_aq'] > 0)
                //{
                //    $EstimatedGas = (($YearlyTariff['gas_sc'] * 365 ) + ($data['quotation_aq'] * $YearlyTariff['gas_ur']));
                //}
                //else
                //{
                //    $EstimatedGas = 0;
                //}

                // If we have elec deets - update newspend
                //if($data['quotation_eac'] > 0)
                //{
                //    $EstimatedElec = (($YearlyTariff['electricity_sc'] * 365 ) + ($data['quotation_eac'] * $YearlyTariff['electricity_dur']));
                //    $NewPrice = number_format(($EstimatedElec + $EstimatedGas),2);
                //    //$db_parameter['tariff_newspend'] = $NewPrice;
                //}

                //$this->update_customer_record($data['id'], $db_parameter);

            } // End loop
        }
    */

    // START - CUSTOMER DD CHANGE NOTIFICATION
    function direct_debit_change_notification()
    {

        // Find people to notify
        $Customers = $this->user_modal->dd_change_customers();
        if(count($Customers) > 0) {

            $this->load->library('email');

            // Start loop
            foreach ($Customers as $CustomerData ) {

                // Send email
                $this->direct_debit_changes($CustomerData['name'], $CustomerData['email'], $CustomerData['old_dd'], $CustomerData['new_dd']);

                // Mark as sent
                $data = [ 'date_sent' => date('Y-m-d H:i:s',time()) ];
                $this->user_modal->update_direct_debit_change($data,$CustomerData['direct_debit_change_id']);

            }
        }
    }

    function direct_debit_changes($forename, $recipient_email, $old_dd, $new_dd) {


        $email_data['email_info'] = ['name' => $forename , 'old_dd' => $old_dd , 'new_dd' => $new_dd ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/direct_debit_change_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Direct debit changes');
        $this->email->message($template);
        $this->email->send();

    }
    // END - CUSTOMER DD CHANGE NOTIFICATION

    /*
    function find_legacy_dyball_customers(){

        return $this->mysql->select('*')->from('customer_info')->where('billingMethod = \'prepay\' AND account_id = \'0\'')->get()->result_array();
    }

    function find_dyball_equivalent()
    {

        $legacy_data = $this->find_legacy_dyball_customers();

        $i = 0;

        foreach ($legacy_data as $row) {

            echo $row['email'];

            if ($i > 10) { break; }

            $data = $this->mysql->select('*')->from('legacy_dyball')->where('EMAIL', $row['email'])->get()->result_array();
            echo '<pre>' . print_r($data,1) . '</pre>';

            if( isset($data['﻿ACCOUNT_NUMBER']) ) {

                echo 'Update';
                echo $row['email'] . ' : ' .  $data['﻿ACCOUNT_NUMBER'] . '<br/>';
                //$this->user_modal->update_customer_info([ 'account_id' => $data['﻿ACCOUNT_NUMBER'] ],$row['id']);
            }
            else {
                echo 'Not found<br/>';
            }

            $i++;
        }
    }
    */

    /**
     * Check junifer status of customer by id
     * @param $id int customer_info_id
     *
     * @return boolean
     */
    function registation_status_check($id)
    {
        // get account id
        $customer_details = $this->user_modal->get_account_detail($id);

        // get meter point id
        $agreements = $this->curl_requests_junifer->junifer_agreements($customer_details['account_id']);
        $elec_meter_point_id = $agreements['results'][0]['products'][0]['assets'][0]['id'];

        // get latest status
        $current_status = $this->curl_requests_junifer->junifer_supply_status($elec_meter_point_id);
        $current_supply_status = $current_status['history'][0]['supplyStatus'];

        $converted_status = ["Registered", "Registration Completed", "Registration Confirmed"];

        if( in_array($current_supply_status, $converted_status) ) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Automated process to loop through saas log and mark record as converted in DB and Saas
     *
     */
    function saas_convert_live_junifer_customers()
    {

        $referrer_data = $this->saas_model->none_converted_referrer();

        if (!empty($referrer_data)) {

            foreach ($referrer_data as $referrer) {

                if (!strstr($referrer['referrer_customer_id'],'ESE') && $this->registation_status_check($referrer['referrer_customer_id'])) {

                    // mark referrer as converted in saas
                    $referrer_saas_data = $this->curl_requests_saasquatch->saas_customer_lookup($referrer['referrer_customer_id']);
                    $response = $this->curl_requests_saasquatch->mark_saas_converted(['id' => $referrer['referrer_customer_id'], 'saasref' => $referrer_saas_data['referralCode']]);
                    echo '<pre>' . print_r($response,1) . '</pre>';

                    // mark as converted in DB
                    $this->saas_model->saas_converted(['type' => 'referrer', 'referral_log_id' => $referrer['referral_log_id']]);
                }
            }
        }

        $referred_data = $this->saas_model->none_converted_referred();

        if (!empty($referred_data)) {

            foreach ($referred_data as $referred) {

                if ($this->registation_status_check($referred['referred_customer_id'])) {

                    // mark referred as converted in saas
                    $referred_saas_data = $this->curl_requests_saasquatch->saas_customer_lookup($referred['referred_customer_id']);
                    $this->curl_requests_saasquatch->mark_saas_converted(['id' => $referred['referred_customer_id'], 'saasref' => $referred_saas_data['referralCode']]);

                    // mark as converted in DB
                    $this->saas_model->saas_converted(['type' => 'referred', 'referral_log_id' => $referred['referral_log_id']]);

                    // Auto redeem ESE staff referrers
                    if (strstr($referred['referrer_customer_id'], 'ESE')) {
                        $this->saas_model->saas_converted(['type' => 'referrer', 'referral_log_id' => $referred['referral_log_id']]);
                        $this->saas_model->saas_customer_redeemed(['type' => 'referrer', 'referral_log_id' => $referred['referral_log_id']]);
                    }
                }
            }
        }
    }
}
