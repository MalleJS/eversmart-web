<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Policies extends ESE_Controller {

  function index(){

    // adding additional styles and scripts to the landing-page.php veiw
		$data['stylesheets'] = ['assets/css/new-world/help/privacy-page.css'];

		$data['content'] = 'new-world/help/privacy-page';
		$this->load->view( 'new-world/master', $data );
  }

}
