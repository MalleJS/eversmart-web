<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FamilySaver extends ESE_Controller {

  function index(){

    // adding additional styles and scripts to the landing-page.php veiw
    $data['stylesheet'] = "assets/css/new-world/familysaver/familysaver-page.css";
    $data['content'] = 'new-world/familysaver/familysaver-page';
    $this->load->view( 'new-world/master', $data );

  }

}
