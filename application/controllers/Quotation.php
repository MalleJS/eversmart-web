<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Quotation extends ESE_Controller
{

    private $website_tariffs;

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('tariff_model');
        $this->mysql = $this->load->database('mysql', true);
    }

    function address_lookup()
    {
        $this->load->model('curl/curl_requests_junifer');

        $pc = trim(str_replace(' ', '', $this->input->post('postcode')));
        $pc = wordwrap($pc, strlen($pc)-3,' ', true);

        $pc_arr = explode(' ', $pc);
        count($pc_arr)===3 ? $pc = $pc_arr[0] . ' ' . $pc_arr[1].$pc_arr[2] : null;

        $e = $this->curl_requests_junifer->junifer_elec_addresses_pcod($pc);
        $g = $this->curl_requests_junifer->junifer_gas_addresses_pcod($pc);

        echo json_encode([
            "elec" => $e,
            "gas" => $g
        ]);
    }

    // function elec_addresses()
    // {
    //     $this->load->model('curl/curl_requests_junifer');
    //     $pc = $this->input->post('postcode');
    //     $addresses = $this->curl_requests_junifer->junifer_elec_addresses_pcod($pc);
    //     var_dump($addresses);
    // }

    // function gas_addresses()
    // {
    //     $this->load->model('curl/curl_requests_junifer');
    //     $pc = $this->input->post('postcode');
    //     $addresses = $this->curl_requests_junifer->junifer_elec_addresses_pcod($pc);
    //     var_dump($addresses);
    // }

    // function elec_addresses()
    // {
    //     $this->load->model('curl/curl_requests_ecoes');
    //     $pc = $this->input->post('postcode');
    //     $addresses = $this->curl_requests_ecoes->ecoes_get_addresses_postcode($pc);
    //     echo json_encode(['error' => false, 'data' => $addresses]);
    // }

    // function elec_meter_details()
    // {
    //     $this->load->model('curl/curl_requests_ecoes');
    //     $mpan = $this->input->post('mpan');
    //     $meterDetails = $this->curl_requests_ecoes->ecoes_get_technical_mpan($mpan);
    //     echo json_encode(['error' => '0', 'data' => $meterDetails]);
    // }

    function index()
    {
        $data['scripts'] = [
            base_url() . 'assets/js/new-world/quotation/quotation-page.js',
            'https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js',
            'https://js.stripe.com/v3/',
            'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/tingle/0.14.0/tingle.min.js'
        ];
        $data['stylesheets'] = [
            base_url() . 'assets/css/new-world/quotation/quotation-page.css',
            'https://unpkg.com/flickity@2.0.5/dist/flickity.css',
            'https://cdnjs.cloudflare.com/ajax/libs/tingle/0.14.0/tingle.min.css'
        ];
        $data['content'] = 'new-world/quotation/quotation-page';
        $this->load->view('new-world/master', $data);
    }

    function postcode()
    {
        $data['back_link'] = base_url();
        $this->load->view('energy/postcode', $data);
    }

    function signup_success()
    {
        // $_POST['uid'] = 10001627;
        // $_POST['ref'] = "http://eversmart.family/AlexHeafy78";
        $uid = $this->input->post("uid");
        $ref = $this->input->post("ref");
        if ($uid == null) {
            // redirect("/");
        }


        $this->load->library('parser');
        $data['ref'] = $ref;
        $data['stylesheets'] = [base_url() . 'assets/css/new-world/quotation/signup-success.css'];
        $data['scripts'] = [base_url() . 'assets/js/new-world/quotation/signup-success.js'];
        $data['content'] = 'new-world/quotation/signup-success';
        $data['bodyClasses'] = $this->body_classes;
        $data['supplyStart'] = date("l jS \of F Y", strtotime("+21 days"));
        $this->parser->parse($this->template, $data);
    }

    function quotefor()
    {

        $data1 = ['pcod' => $this->input->post('postcode')];
        $response = $this->curl->curl_request('/v3.1/partner-resources/' . $this->affiliate_id . '/mpas/addresses-by-pcod/', $data1, 'post');

        $address_list = $this->parseHttpResponse($response);

        // if($address_list['statusCode'] == '1046')
        // {
        //     $address_data = array();
        //     $address_data1 = $address_list['data']['addresses'];

        //     for( $mm=0; $mm<count($address_data1); $mm++ )
        //     {

        //         for( $j=0; $j<count( $address_data1[$mm]['metersElec'] ); $j++ )
        //         {
        //             if( $address_data1[$mm]['metersElec'][$j]['mpanProfileClass'] != '03' )
        //             {
        //                   $address_data[] = $address_data1[$mm];
        //             }
        //         }

        //     }

        //    // debug( $address_data,1 );

        //     $this->session->set_userdata('address_data',$address_data);

        //     //
        // }
        // else {
        //     echo  $address_list['statusCode'];
        // }


        $response_gas = $this->curl->curl_request('/v3.1/partner-resources/' . $this->affiliate_id . '/mpas/addresses/' . str_replace(' ', '', $this->input->post('postcode')) . '/');
        $address_list_gas = $this->parseHttpResponse($response_gas);


        $AddressData = [
            'elec' => isset($address_list['body']['data']['addresses']) && !empty($address_list['body']['data']['addresses']) ? $address_list['body']['data']['addresses'] : [],
            'gas' => isset($address_list_gas['body']['data']['addresses']) && !empty($address_list_gas['body']['data']['addresses']) ? $address_list_gas['body']['data']['addresses'] : []
        ];

        echo json_encode($AddressData);

        // print_r($address_list_gas);


        // if($address_list_gas['body']['statusCode'] == '1046')
        // {
        //     $address_data_gas = array();
        //     $address_data1_gas = $address_list_gas['data']['addresses'];

        //     for( $mm=0; $mm<count($address_data1_gas); $mm++ )
        //     {

        //         for( $j=0; $j<count( $address_data1_gas[$mm]['metersGas'] ); $j++ )
        //         {

        //          $address_data_gas[] = $address_data1_gas[$mm];

        //         }

        //     }

        //    // debug( $address_data,1 );

        //     $this->session->set_userdata('address_data_gas',$address_data_gas);
        //   //  $this->address_list();
        //     //
        // }
        // else {
        //     echo  $address_list_gas['statusCode'];
        // }


        //    $this->address_list();
    }

    function new_quotefor()
    {
        $this->load->view('energy/quotefor');
    }

    function address_list()
    {
        $data = array();
        $address_data = $this->session->userdata('address_data');
        //debug($address_data,1);
        if (!empty($address_data)) {
            for ($i = 0; $i < count($address_data); $i++) {
                ($address_data[$i]['dept']) ? $dept = $address_data[$i]['dept'] : $dept = '';
                ($address_data[$i]['orgn']) ? $orgn = $address_data[$i]['orgn'] : $orgn = '';
                ($address_data[$i]['pobx']) ? $pobx = $address_data[$i]['pobx'] : $pobx = '';
                ($address_data[$i]['dept']) ? $dept = $address_data[$i]['dept'] : $dept = '';
                ($address_data[$i]['subb']) ? $subb = $address_data[$i]['subb'] : $subb = '';
                ($address_data[$i]['bnam']) ? $bnam = $address_data[$i]['bnam'] : $bnam = '';
                ($address_data[$i]['dpth']) ? $dpth = $address_data[$i]['dpth'] : $dpth = '';
                ($address_data[$i]['thor']) ? $thor = $address_data[$i]['thor'] : $thor = '';
                ($address_data[$i]['ddlo']) ? $ddlo = $address_data[$i]['ddlo'] : $ddlo = '';
                ($address_data[$i]['dplo']) ? $dplo = $address_data[$i]['dplo'] : $dplo = '';
                ($address_data[$i]['town']) ? $town = $address_data[$i]['town'] : $town = '';
                ($address_data[$i]['cnty']) ? $cnty = $address_data[$i]['cnty'] : $cnty = '';
                ($address_data[$i]['pcod']) ? $pcod = $address_data[$i]['pcod'] : $pcod = '';
                $address_data[$i]['address'] = $address_data[$i]['bnum'] . "" . $dept . " " . $orgn . " " . $pobx . " " . $subb . " " . $bnam . " " . $dpth . " " . $thor . " " . $ddlo . " " . $dplo . " " . $town . " " . $cnty . " " . $pcod;
                $address_data[$i]['pcod'] = $address_data[$i]['pcod'];

                //electricity
                if (!empty($address_data[$i]['metersElec'])) {
                    //$address_data[$i]['mpan'] = count($address_data[$i]['metersElec']);
                    for ($m = 0; $m < count($address_data[$i]['metersElec']); $m++) {
                        if (count($address_data[$i]['metersElec']) > 1) {
                            $address_data[$i]['mpan'][] = $address_data[$i]['metersElec'][$m]['mpan'];
                            $address_data[$i]['mpanLower'][] = $address_data[$i]['metersElec'][$m]['mpanLower'];
                            $address_data[$i]['meterType'] = $address_data[$i]['metersElec'][$m]['pricePlanCode'];
                            $address_data[$i]['meterSerialElec'][] = $address_data[$i]['metersElec'][$m]['meterSerialElec'];
                            $db_mpan_check = $this->user_modal->db_mpan_check($address_data[$i]['metersElec'][$m]['mpanLower']);
                            if ($db_mpan_check == '1') {
                                $address_data[$i]['elec_used_in_db2'] = '1';
                            } else {
                                $address_data[$i]['elec_used_in_db2'] = '0';
                            }
                        } else {
                            $address_data[$i]['metersElec'][$m]['mpanLower'];
                            $address_data[$i]['mpan'][] = $address_data[$i]['metersElec'][$m]['mpan'];
                            $address_data[$i]['mpanLower'][] = $address_data[$i]['metersElec'][$m]['mpanLower'];
                            $address_data[$i]['meterType'] = $address_data[$i]['metersElec'][$m]['pricePlanCode'];
                            $address_data[$i]['meterSerialElec'][] = $address_data[$i]['metersElec'][$m]['meterSerialElec'];
                            $db_mpan_check = $this->user_modal->db_mpan_check($address_data[$i]['metersElec'][$m]['mpanLower']);

                            if ($db_mpan_check == 1) {
                                $address_data[$i]['elec_used_in_db1'] = '1';
                            } else {
                                $address_data[$i]['elec_used_in_db1'] = '0';
                            }
                        }
                    }
                } else {
                    $address_data[$i]['mpan'] = array();
                    $address_data[$i]['meterType'] = array();
                    $address_data[$i]['meterSerialElec'] = array();
                    $address_data[$i]['elec_used_in_db3'] = '0';
                }

                //gas
                if (!empty($address_data[$i]['metersGas'])) {
                    for ($m = 0; $m < count($address_data[$i]['metersGas']); $m++) {

                        if (count($address_data[$i]['metersGas']) > 1) {
                            $address_data[$i]['mprn'][] = $address_data[$i]['metersGas'][$m]['mprn'];
                            $address_data[$i]['meterSerialGas'][] = $address_data[$i]['metersGas'][$m]['meterSerialGas'];
                        } else {
                            $address_data[$i]['mprn'][] = $address_data[$i]['metersGas'][$m]['mprn'];
                            $address_data[$i]['meterSerialGas'][] = $address_data[$i]['metersGas'][$m]['meterSerialGas'];
                        }
                    }
                } else {
                    $address_data[$i]['mprn'] = array();
                    $address_data[$i]['meterSerialGas'] = array();
                }

                //unset some array value
                // $address_data[$i]['metersGas'];
                // $address_data[$i]['metersElec'];
                // $address_data[$i]['gsps'];
                // $address_data[$i]['postcodeMapping'];
            }
            $data['address_list'] = $address_data;
            //debug($data,1);
        }

        $address_data_gas = $this->session->userdata('address_data_gas');

        if (!empty($address_data_gas)) {
            for ($i = 0; $i < count($address_data_gas); $i++) {
                ($address_data_gas[$i]['dept']) ? $dept_gas = $address_data_gas[$i]['dept'] : $dept_gas = '';
                ($address_data_gas[$i]['orgn']) ? $orgn_gas = $address_data_gas[$i]['orgn'] : $orgn_gas = '';
                ($address_data_gas[$i]['pobx']) ? $pobx_gas = $address_data_gas[$i]['pobx'] : $pobx_gas = '';
                ($address_data_gas[$i]['dept']) ? $dept_gas = $address_data_gas[$i]['dept'] : $dept_gas = '';
                ($address_data_gas[$i]['subb']) ? $subb_gas = $address_data_gas[$i]['subb'] : $subb_gas = '';
                ($address_data_gas[$i]['bnam']) ? $bnam_gas = $address_data_gas[$i]['bnam'] : $bnam_gas = '';
                ($address_data_gas[$i]['dpth']) ? $dpth_gas = $address_data_gas[$i]['dpth'] : $dpth_gas = '';
                ($address_data_gas[$i]['thor']) ? $thor_gas = $address_data_gas[$i]['thor'] : $thor_gas = '';
                ($address_data_gas[$i]['ddlo']) ? $ddlo_gas = $address_data_gas[$i]['ddlo'] : $ddlo_gas = '';
                ($address_data_gas[$i]['dplo']) ? $dplo_gas = $address_data_gas[$i]['dplo'] : $dplo_gas = '';
                ($address_data_gas[$i]['town']) ? $town_gas = $address_data_gas[$i]['town'] : $town_gas = '';
                ($address_data_gas[$i]['cnty']) ? $cnty_gas = $address_data_gas[$i]['cnty'] : $cnty_gas = '';
                ($address_data_gas[$i]['pcod']) ? $pcod_gas = $address_data_gas[$i]['pcod'] : $pcod_gas = '';
                $address_data_gas[$i]['address'] = $dept_gas . " " . $orgn_gas . " " . $pobx_gas . " " . $subb_gas . " " . $bnam_gas . " " . $address_data_gas[$i]['bnum'] . " " . $dpth_gas . " " . $thor_gas . " " . $ddlo_gas . " " . $dplo_gas . " " . $town_gas . " " . $cnty_gas . " " . $pcod_gas;
                $address_data_gas[$i]['pcod'] = $address_data_gas[$i]['pcod'];

                //gas
                if (!empty($address_data_gas[$i]['metersGas'])) {
                    //$address_data[$i]['mpan'] = count($address_data[$i]['metersElec']);
                    for ($m = 0; $m < count($address_data_gas[$i]['metersGas']); $m++) {
                        if (count($address_data_gas[$i]['metersGas']) > 1) {
                            $address_data_gas[$i]['mprn'][] = $address_data_gas[$i]['metersGas'][$m]['mprn'];
                            $address_data_gas[$i]['meterSerialGas'][] = $address_data_gas[$i]['metersGas'][$m]['meterSerialGas'];
                        } else {
                            $address_data_gas[$i]['mprn'][] = $address_data_gas[$i]['metersGas'][$m]['mprn'];
                            $address_data_gas[$i]['meterSerialGas'][] = $address_data_gas[$i]['metersGas'][$m]['meterSerialGas'];
                        }
                    }
                } else {
                    $address_data_gas[$i]['mprn'] = array();
                    $address_data_gas[$i]['meterSerialGas'] = array();
                }
            }
            $data['address_list_gas'] = $address_data_gas;
        }
        $this->load->view('energy/address_list', $data);
    }

    function payEnergy()
    {
        $this->load->view('energy/pay_energy');
    }

    function know_supplier()
    {
        $this->load->view('energy/know_supplier');
    }

    function hear_about()
    {
        $this->load->view('energy/hear_about');
    }

    function select_supplier()
    {
        //get supplier list

        $eversmart_data = array();
        $response = $this->curl->curl_request('/v3.1/partner-resources/' . $this->affiliate_id . '/suppliers/', [], 'post');
        $supplier_list = json_decode($response, true);
        //debug($eversmart_data);
        if (!empty($supplier_list)) {
            if ($supplier_list['statusCode'] == '1013') {
                /*for( $m=0; $m<count($supplier_list); $m++ ){
                  if( $supplier_list[$m]['supplierName'] == 'Eversmart Energy' ){
                     print_r($supplier_list[$m]);
                  }
                }*/
                //debug($eversmart_data);
                $data['supplier_data'] = $supplier_list['data']['suppliers'];
            }
        } else {
            $data['supplier_data'] = array();
        }
        //debug($data,1);
        $this->load->view('energy/select_supplier', $data);
    }

    function get_tariff()
    {
        $parameter = ['supplierName' => $this->input->post('elec_supplier')];
        //$parameter = ['supplierName' => 'abc energy' ];
        $quotation_for = $this->input->post('tariff_fuelType');
        if ($quotation_for == 'both') {
            $quotation_for = 'elecgas';
        }
        if ($quotation_for == 'electricity') {
            $quotation_for = 'elec';
        }
        $data = array();
        $response = $this->curl->curl_request('/v3.1/partner-resources/' . $this->affiliate_id . '/tariffs/', $parameter, 'post');
        $tariff_list = json_decode($response, true);
        //  debug($tariff_list,1);
        //echo '==='.count($tariff_list['data']['tariffs']);

        if ($tariff_list['statusCode'] == '1040') {
            //$data = $tariff_list['data']['tariffs'];
            for ($t = 0; $t < count($tariff_list['data']['tariffs']); $t++) {
                //echo $quotation_for == $tariff_list['data']['tariffs'][$t]['fuelType'];
                // if( $quotation_for == $tariff_list['data']['tariffs'][$t]['fuelType'] ){
                //debug($tariff_list['data']['tariffs'][$t]);
                $data[] = $tariff_list['data']['tariffs'][$t];
                // }
            }
            if (empty($data)) {
                $data = array();
            }
            echo json_encode(['error' => '0', 'data' => $data]);
        }
        if ($tariff_list['statusCode'] == '2000') {
            echo json_encode(['error' => '1', 'data' => $tariff_list['errors']['supplierName']]);
        }
        if ($tariff_list['statusCode'] == '2025') {
            echo json_encode(['error' => '2', 'data' => $tariff_list['message']]);
        }
    }

    function current_energy()
    {
        $this->load->view('energy/current_energy');
    }

    function home_type()
    {
        $this->load->view('energy/home_type');
    }

    function people_live()
    {
        $this->load->view('energy/people');
    }

    function number_bedroom()
    {
        $this->load->view('energy/number_bedroom');
    }

    function enter_energy()
    {
        $this->load->view('energy/enter_usage');
    }

    function enter_spend()
    {
        $this->load->view('energy/enter_spend');
    }

    function stop_quotation()
    {
        $this->load->view('energy/stop_page_message');
    }

    function summary()
    {
        $form = json_decode(file_get_contents('php://input'), true);
        $tariff_response = $this->tariff_model->get_visible();
        $tariffs = json_decode($tariff_response, true);

        foreach ($tariffs as $data) {
            $this->website_tariffs[] = $data['tariff_name'];
        }

        if ($form['hasExistingSupply'] == 'yes') {
            $form['hasExistingSupply'] = 'true';
        }

        if ($form['hasExistingSupply'] == 'no') {
            $form['hasExistingSupply'] = 'false';
        }

        if ($form['quotation_pay_energy'] == 'prepay') {
            $form['existingPayTypesElec'] = 5;
        }

        if ($form['quotation_pay_energy'] == 'directdebit') {
            $form['existingPayTypesElec'] = 1;
        }

        if ($form['quotation_pay_energy'] == 'paperbill') {
            $form['existingPayTypesElec'] = '15';
        }

        //desire pay type
        if ($form['desiredPayTypes'] == 'prepay') {
            $form['desiredPayTypes'] = 5;
        }

        if ($form['desiredPayTypes'] == 'directdebit') {
            $form['desiredPayTypes'] = 1;
        }

        if ($form['desiredPayTypes'] == 'paperbill') {
            $form['desiredPayTypes'] = '15';
        }


        // Get dist id
        $dist_paramter['postcode'] = $form['postcode'];
        $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_paramter, 'post'), true);


        // If not found default to 10
        if (!empty($dist_response['data']['entriesFromMpanFields'])) {

            $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
            if ($ElecDistId >= 24) {
                $ElecDistId = 17;
            }
        } else {
            $ElecDistId = 17;
        }

        $parameter['postcode'] = $form['postcode'];
        $parameter['hasExistingSupply'] = false;
        $parameter['existingPayTypesElec'] = (int)$form['existingPayTypesElec'];
        $parameter['desiredPayTypes'] = (int)$form['desiredPayTypes'];
        $parameter['meterTypeElec'] = 'STD';


        if ($form['quotation_for'] == 'electricity') {

            $parameter['nightUsePercentElec'] = '10';
            $parameter['existingSupplierIdElec'] = 5;
            $parameter['existingTariffNameElec'] = 'Standard';

            if ($form['meterTypeElec'] == 'E7') {
                $parameter['nightUsePercentElec'] = '10';
            }
            $parameter['existingkWhsElec'] = $form['existingkWhsElec'];
            $parameter['existingkWhsIntervalElec'] = $form['existingkWhsIntervalElec'];
            // $parameter['existingSpendElec'] = $form['existingSpendElec'];
            $parameter['peopleElec'] = $form['bedroomsElec'];
            $parameter['typeOfHomeElec'] = $form['typeOfHomeElec'];
            $parameter['bedroomsElec'] = $form['bedroomsElec'];
            if ($parameter['existingkWhsElec'] == NULL) {
                $parameter['unknownUsageTypeElec'] = 'Use profiling';
            }

            $response = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elec/', $parameter, 'post');
            $this->session->set_userdata('signup_detail', $form);
        }

        if ($form['quotation_for'] == 'both') {

            // this needs extensive tidying up by jack and austin

            $form['existingSupplierIdElec'] = 5;
            $form['meterTypeElec'] = 'STD';
            $form['existingkWhsIntervalElec'] = 'year';
            $form['hasExistingSupply'] = true;

            if ($form['gas_supplier_id'] == $form['existingSupplierIdElec']) {
                $elecgas = 'single';
            } else if ($form['quotation_current_energy_usage'] == 'no') {
                $elecgas = 'separate';
            } else {
                $elecgas = 'separate';
            }

            $parameter = [
                'singleOrSeparate' => 'separate',
                'hasExistingSupply' => $form['hasExistingSupply'],
                'existingPayTypesGas' => 1,
                'desiredPayTypes' => (int)$form['desiredPayTypes'],
                'existingkWhsElec' => $form['quotation_elec_energy_usage'] != '' ? $form['quotation_elec_energy_usage'] : $form['existingkWhsElec'],
                'existingkWhsGas' => $form['quotation_gas_energy_usage'],
                'existingPayTypesElec' => (int)$form['existingPayTypesElec'],
                'existingSupplierIdElec' => 5,
                'existingTariffNameGas' => 'Standard',
                'existingkWhsIntervalGas' => $form['quotation_gas_energy_interval'],
                'existingkWhsIntervalElec' => $form['existingkWhsIntervalElec'],
                'existingSupplierIdGas' => 5,
                'postcode' => $form['postcode'],
                'nightUsePercentElec' => '10',
                'existingTariffNameElec' => 'Standard',
                'meterTypeElec' => $form['meterTypeElec'],
                'distId' => $ElecDistId,
            ];

            if ($form['quotation_current_energy_usage'] == 'no') {
                $parameter['peopleElec'] = $form['peopleElec'];
                $parameter['typeOfHomeElec'] = $form['typeOfHomeElec'];
                $parameter['bedroomsElec'] = $form['bedroomsElec'];
                $parameter['peopleGas'] = $form['peopleElec'];
                $parameter['typeOfHomeGas'] = $form['typeOfHomeElec'];
                $parameter['bedroomsGas'] = $form['bedroomsElec'];
                $parameter['filterSupplierGroupId'] = '89';
                $parameter['ofgemAverageElec'] = 'low user';
                $parameter['ofgemAverageGas'] = 'low user';
            }

            $response = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elecgas/', $parameter, 'post');

            $this->session->set_userdata('signup_detail', $form);
        }

        /*
            $YearlyTariff = $this->lookup_yearly_tariff_data($ElecDistId, $form['meterTypeElec']);

            if ($form['quotation_for'] == 'both') {
                $EstimatedGas = (($YearlyTariff['gas_sc'] * 365 ) + ($form['quotation_gas_energy_usage'] * $YearlyTariff['gas_ur'])) ;
            }
            else{
                $EstimatedGas = 0;
            }

            $EstimatedElec = (($YearlyTariff['electricity_sc'] * 365 ) + ($form['quotation_elec_energy_usage'] * $YearlyTariff['electricity_dur']))  ;
        */

        $response = json_decode($response, true);

        // If a successful response is returned
        if ($response['statusCode'] == "1009") {


            $i = 0;
            foreach ($response['data']['searchResults'] as $key => $tariff) {

                if (in_array($tariff['tariffName'], $this->website_tariffs)) {

                    $tariff_data = $this->mysql->select('*')->from('tariff')->like('tariff_name', $tariff['tariffName'])->get()->first_row('array');

                    // Dont show she's electric tariff to Bury FC referrals
                    if (($form['saasref'] == 'BURYFC' || $form['saasref'] == 'MATCHDAYBURY') && $tariff['tariffName'] == 'She\'s Electric') {
                        continue;
                    }

                    $price = floor($tariff['newSpend']);
                    $data['new_data'][$i] = [
                        'type' => 'monthly',
                        'paymentType' => 'pay monthly',
                        'title' => $tariff['tariffName'],
                        'price' => $price,
                        'monthlyPrice' => floor($price / 12),
                        'eac' => $form['quotation_elec_energy_usage'] != '' ? $form['quotation_elec_energy_usage'] : $form['existingkWhsElec'], // estimated yearly consumption
                        'aq' => $form['quotation_gas_energy_usage'], // anual quantity
                        'elec_ur' => $tariff['unitRate1Elec'],
                        'elec_sc' => $tariff['standingChargeElec'],
                        'gas_ur' => $tariff['unitRate1Gas'],
                        'gas_sc' => $tariff['standingChargeGas'],
                        'elec_product_code' => $tariff_data['tariff_code'] . '-E',
                        'gas_product_code' => $tariff_data['tariff_code'] . '-G',
                        'showBadge' => false,
                        'extraCaption' => [
                            "* Earn 12% interest per annum on your credit balance!",
                            "*t&amp;c's apply",
                            "* Earn interest on your refer a friend credit balance"
                        ],
                        'tariff' => [
                            'tabID' => rand(),
                            'statements' => [
                                [
                                    'tag' => "h3",
                                    'text' => $tariff['tariffName']
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "Electricity unit rate – " . number_format($tariff['unitRate1Elec'], 1) . " pence per kWh",
                                    'condition' => json_encode(!empty($tariff['unitRate1Elec']))
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "Gas unit rate – " . number_format($tariff['unitRate1Gas'], 1) . " pence per kWh",
                                    'condition' => json_encode(!empty($tariff['unitRate1Gas']))
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "Electricity standing charge – " . number_format($tariff['standingChargeElec'], 1) . " pence per day",
                                    'condition' => json_encode(!empty($tariff['standingChargeElec']))
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "Gas standing charge – " . number_format($tariff['standingChargeGas'], 1) . " pence per day",
                                    'condition' => json_encode(!empty($tariff['standingChargeGas']))
                                ]
                            ]
                        ],
                        'important' => [
                            'tabID' => rand(),
                            'statements' => [
                                [
                                    'tag' => "h3",
                                    'text' => "Don’t worry – we'll do all the hard work for you!"
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                ],
                                [
                                    'tag' => "h3",
                                    'text' => "Just so you know.."
                                ],
                                [
                                    'tag' => "p",
                                    'text' => "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                ],
                                [
                                    'tag' => "a",
                                    'target' => "_blank",
                                    'href' => "/Terms",
                                    'text' => "Read our Terms & Conditions here"
                                ]
                            ]
                        ]
                    ];

                    $data['new_data'][$i]['boiler_cover'] = false;


                    switch ($tariff['tariffType']) {

                        case 'Fixed':
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p",'text' => 'Fixed price tariff where the prices do not move with the market' ];
                            break;

                    }



                    switch ($tariff['tariffName']) {

                        case 'Safeguard PAYG':

                            $data['new_data'][$i]['saving'] = "Best Value Tariff";
                            $data['new_data'][$i]['boiler_cover'] = true;
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => 'No monthly bills - you buy credit when it suits you'
                                ],
                                [
                                    'label' => 'Top-up your balance via phone, app or SMS (smart meters only)'
                                ],
                                [
                                    'label' => 'View your remaining credit whenever you like'
                                ]
                            ];
                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => "We don’t charge exit fees!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! Unlike most other suppliers, we won’t charge you an early exit fee."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "You’ll get a free smart meter!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Here when you need us"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                            break;

                        case 'SmartMeter 12':

                            $data['new_data'][$i]['best_value'] = true;
                            $data['new_data'][$i]['saving'] = "Save up to to £252 per year";
                            $data['new_data'][$i]['boiler_cover'] = true;
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => '£100 bill credit with Eversmart Smartmeter'
                                ],
                                [
                                    'label' => 'Free glow stick worth £30',
                                    'link' => [
                                        'href' => 'https://www.eversmartenergy.co.uk/blog/glowstick',
                                        'text' => '(read more)'
                                    ]
                                ],
                                [
                                    'label' => 'Free boiler & heating cover - saving £89'
                                ],
                                [
                                    'label' => '12 month fixed tariff'
                                ],
                                [
                                    'label' => 'No nasty price rises'
                                ]
                            ];

                            $data['new_data'][$i]['price'] = $data['new_data'][$i]['price'] - 100;
                            $data['new_data'][$i]['monthlyPrice'] = floor($data['new_data'][$i]['price'] / 12);

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => "You’ll get a free smart meter!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment. If you do not have a smart meter fitted, the discount will not be applied."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Here when you need us"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                            break;

                        case 'FixedSmart 12':

                            $data['new_data'][$i]['saving'] = "Save up to £193 per year";
                            $data['new_data'][$i]['boiler_cover'] = true;
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => '12 month fixed tariff'
                                ],
                                [
                                    'label' => 'Pay by monthly direct debit'
                                ],
                                [
                                    'label' => 'No nasty price rises'
                                ]
                            ];

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => "You’ll get a free smart meter!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Here when you need us"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                            break;

                        case 'FixedSmart 18':

                            $data['new_data'][$i]['saving'] = "Save up to £156 per year";
                            $data['new_data'][$i]['boiler_cover'] = true;
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => 'Fix your price for longer'
                                ],
                                [
                                    'label' => '18 month fixed tariff'
                                ],
                                [
                                    'label' => 'Pay more now and make bigger savings in the long run'
                                ],
                                [
                                    'label' => 'No nasty price rises'
                                ]
                            ];

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => "You’ll get a free smart meter!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Here when you need us"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                            break;

                        case 'Spring Smart Special':

                            $data['new_data'][$i]['cheapest_tariff'] = false;
                            $data['new_data'][$i]['saving'] = "Save up to to £306 per year";
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => '18 month fixed tariff'
                                ],
                                [
                                    'label' => '£50 bill credit with Eversmart Smartmeter'
                                ],
                                [
                                    'label' => 'No nasty price rises'
                                ]
                            ];

                            $data['new_data'][$i]['price'] = $data['new_data'][$i]['price'] - 50;
                            $data['new_data'][$i]['monthlyPrice'] = floor($data['new_data'][$i]['price'] / 12);

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => '£50 bill credit with Eversmart installed Smartmeter'
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Receive £50 credit to spend on your Eversmart energy when you order a free smart meter with us. Ensure you have transparency with your readings and utilise energy efficiency.  Build a sustainable future and protect our environment by using our renewable electricity and metering control."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Here when you need us"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                            break;

                        case 'She\'s Electric':

                            $data['new_data'][$i]['cheapest_tariff'] = true;
                            $data['new_data'][$i]['saving'] = "Save up to £359 per year";
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => '12 month fixed tariff'
                                ],
                                [
                                    'label' => 'Pay by monthly direct debit'
                                ],
                                [
                                    'label' => 'Available for Gas & Elec'
                                ]
                            ];

                            $data['new_data'][$i]['price'] = $data['new_data'][$i]['price'];
                            $data['new_data'][$i]['monthlyPrice'] = floor($data['new_data'][$i]['price'] / 12);

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => 'Cheapest fixed tariff on the market'
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Save up to £359 a year against other standard fixed tariff on the market, calculated against the Ofgem pricing cap."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "You’ll get a free smart meter!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you! There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];
                            $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £30 per fuel' ];

                            break;

                        case 'Vari Cheap':

                            $data['new_data'][$i]['cheapest_tariff'] = false;
                            $data['new_data'][$i]['saving'] = "Save up to £378.50 per year";
                            $data['new_data'][$i]['highlights'] = [
                                [
                                    'label' => 'Variable tariff'
                                ],
                                [
                                    'label' => 'Pay by monthly direct debit'
                                ],
                                [
                                    'label' => 'No exit fees'
                                ]
                            ];

                            $data['new_data'][$i]['benefits'] = [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => 'Here when you need us'
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Switching is smooth and seamless"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                    ]
                                ]
                            ];

                            break;

                    }

                    $i++;
                }
            }


            // Direct debit / PAYG

            /*
                        // Yearly tariff
                        if ($form['desiredPayTypes'] != 5) { // Not prepay

                            $data['new_data'][1] = [
                                'type' => 'yearly',
                                'paymentType' => 'pay yearly',
                                'title' => $YearlyTariff['productname'],
                                'price' => number_format(($EstimatedElec + $EstimatedGas), 2),
                                'eac' => $form['quotation_elec_energy_usage']!=''?$form['quotation_elec_energy_usage']:$form['existingkWhsElec'], // estimated yearly consumption
                                'aq' => $form['quotation_gas_energy_usage'], // anual quantity
                                'showBadge' => true,
                                'extraCaption' => array(),
                                'highlights' => [
                                    [
                                        'img' => '/assets/images/icons/chart.png',
                                        'label' => 'Earn 12% annual interest on your credit balance'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/medal.png',
                                        'label' => 'get on the cheapest fixed-rate tariff on the market'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/wallet.png',
                                        'label' => 'Make 1 simple payment for the whole year'
                                    ]
                                ],
                                'tariff' => [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => ucwords(strtolower($YearlyTariff['productname']))
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Electricity unit rate – " . number_format($YearlyTariff['electricity_dur'] * 100, 1) . " pence per kWh",
                                            'condition' => json_encode(!empty($YearlyTariff['electricity_dur']))
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Gas unit rate – " . number_format($YearlyTariff['gas_ur'] * 100, 1) . " pence per kWh",
                                            'condition' => json_encode(!empty($YearlyTariff['gas_ur']))
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Electricity standing charge – " . number_format($YearlyTariff['electricity_sc'] * 100, 1) . " pence per day",
                                            'condition' => json_encode(!empty($YearlyTariff['electricity_sc']))
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Gas standing charge – " . number_format($YearlyTariff['gas_sc'] * 100, 1) . " pence per day",
                                            'condition' => json_encode(!empty($YearlyTariff['gas_sc']))
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Earn 12% interest per annum on your credit balance"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Earn interest on your refer a friend credit balance"
                                        ]
                                    ]
                                ],
                                'benefits' => [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "No hard feelings!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! However, because we've comitted to a years worth of gas and electricity, there will be an exit fee. Please see T&Cs for more info."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email or social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ],
                                'important' => [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "Don’t worry – we'll do all the hard work for you!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Just so you know.."
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                        ],
                                        [
                                            'tag' => "a",
                                            'target' => "_blank",
                                            'href' => "/Terms",
                                            'text' => "Read our Terms & Conditions here"
                                        ]
                                    ]
                                ]
                            ];
                        }
             */

            $data['desiredPayTypes'] = $form['desiredPayTypes'];
            $data['quote_summary'] = $response['data'];
            $this->session->set_userdata('tariff_detail', $response['data']['searchResults'][0]);
            echo json_encode(['error' => false, 'data' => $data]);
            exit;
        }


        if ($response['statusCode'] == "3042") {

            if ($form['quotation_for'] == 'both') {

                $parameter = [
                    'singleOrSeparate' => $elecgas,
                    'hasExistingSupply' => $form['hasExistingSupply'],
                    'existingPayTypesGas' => (int)$form['existingPayTypesElec'],
                    'desiredPayTypes' => (int)$form['desiredPayTypes'],
                    'existingkWhsElec' => $form['quotation_elec_energy_usage'] != '' ? $form['quotation_elec_energy_usage'] : $form['existingkWhsElec'],
                    'existingkWhsGas' => $form['quotation_gas_energy_usage'],
                    'existingPayTypesElec' => (int)$form['existingPayTypesElec'],
                    'existingSupplierIdElec' => 5,
                    'existingTariffNameGas' => 'Standard',
                    'existingkWhsIntervalGas' => $form['quotation_gas_energy_interval'],
                    'existingkWhsIntervalElec' => $form['existingkWhsIntervalElec'],
                    'existingSupplierIdGas' => 5,
                    'postcode' => $form['postcode'],
                    'nightUsePercentElec' => '10',
                    'existingTariffNameElec' => 'Standard',
                    'meterTypeElec' => $form['meterTypeElec'],
                    'distId' => $ElecDistId,
                ];

                $response = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elecgas/', $parameter, 'post');
            } else {

                if ($form['quotation_supplier'] && $form['quotation_tariff']) {
                    $parameter['nightUsePercentElec'] = '10';
                    $parameter['existingSupplierIdElec'] = 5;
                    $parameter['existingTariffNameElec'] = 'Standard';
                }
                if ($form['quotation_current_energy_usage'] == 'spend') {
                    $parameter['existingSpendElec'] = $form['existingSpendElec'];
                    $parameter['existingSpendIntervalElec'] = $form['existingSpendIntervalElec'];
                }
                if ($form['quotation_current_energy_usage'] == 'kwh') {
                    $parameter['existingkWhsElec'] = $form['quotation_elec_energy_usage'];
                    $parameter['existingkWhsIntervalElec'] = $form['existingkWhsIntervalElec'];
                }
                if ($form['quotation_current_energy_usage'] == 'no') {
                    $parameter['peopleElec'] = $form['peopleElec'];
                    $parameter['typeOfHomeElec'] = $form['typeOfHomeElec'];
                    $parameter['bedroomsElec'] = $form['bedroomsElec'];
                    $parameter['unknownUsageTypeElec'] = 'Use profiling';
                }

                $response = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elec/', $parameter, 'post');
            }

            $response = json_decode($response, true);

            $data['desiredPayTypes'] = $form['desiredPayTypes'];

            if (isset($response['data'])) {

                // Direct debit / PAYG
                $i = 0;
                foreach ($response['data']['searchResults'] as $key => $tariff) {

                    if (in_array($tariff['tariffName'], $this->website_tariffs)) {

                        $tariff_data = $this->mysql->select('*')->from('tariff')->like('tariff_name', $tariff['tariffName'])->get()->first_row('array');

                        $price = floor($tariff['newSpend']);
                        $data['new_data'][$i] = [
                            'type' => 'monthly',
                            'paymentType' => 'pay monthly',
                            'title' => $tariff['tariffName'],
                            'price' => $price,
                            'monthlyPrice' => floor($price / 12),
                            'eac' => $form['quotation_elec_energy_usage'] != '' ? $form['quotation_elec_energy_usage'] : $form['existingkWhsElec'], // estimated yearly consumption
                            'aq' => $form['quotation_gas_energy_usage'], // anual quantity
                            'elec_ur' => $tariff['unitRate1Elec'],
                            'elec_sc' => $tariff['standingChargeElec'],
                            'gas_ur' => $tariff['unitRate1Gas'],
                            'gas_sc' => $tariff['standingChargeGas'],
                            'elec_product_code' => $tariff_data['tariff_code'] . '-E',
                            'gas_product_code' => $tariff_data['tariff_code'] . '-G',
                            'showBadge' => false,
                            'extraCaption' => [
                                "* Earn 12% interest per annum on your credit balance!",
                                "*t&amp;c's apply",
                                "* Earn interest on your refer a friend credit balance"
                            ],
                            'tariff' => [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => $tariff['tariffName']
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Electricity unit rate – " . number_format($tariff['unitRate1Elec'], 1) . " pence per kWh",
                                        'condition' => json_encode(!empty($tariff['unitRate1Elec']))
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Gas unit rate – " . number_format($tariff['unitRate1Gas'], 1) . " pence per kWh",
                                        'condition' => json_encode(!empty($tariff['unitRate1Gas']))
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Electricity standing charge – " . number_format($tariff['standingChargeElec'], 1) . " pence per day",
                                        'condition' => json_encode(!empty($tariff['standingChargeElec']))
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "Gas standing charge – " . number_format($tariff['standingChargeGas'], 1) . " pence per day",
                                        'condition' => json_encode(!empty($tariff['standingChargeGas']))
                                    ],
                                ]
                            ],
                            'important' => [
                                'tabID' => rand(),
                                'statements' => [
                                    [
                                        'tag' => "h3",
                                        'text' => "Don’t worry – we'll do all the hard work for you!"
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                    ],
                                    [
                                        'tag' => "h3",
                                        'text' => "Just so you know.."
                                    ],
                                    [
                                        'tag' => "p",
                                        'text' => "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                    ],
                                    [
                                        'tag' => "a",
                                        'target' => "_blank",
                                        'href' => "/Terms",
                                        'text' => "Read our Terms & Conditions here"
                                    ]
                                ]
                            ]
                        ];


                        switch ($tariff['tariffType']) {

                            case 'Fixed':
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p",'text' => 'Fixed price tariff where the prices do not move with the market' ];
                                break;

                        }

                        switch ($tariff['tariffName']) {

                            case 'Safeguard PAYG':

                                $data['new_data'][$i]['saving'] = "Best Value Tariff";
                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'img' => '/assets/images/icons/shield.png',
                                        'label' => 'No monthly bills - you buy credit when it suits you'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/card.png',
                                        'label' => 'Top-up your balance via phone, app or SMS (smart meters only)'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/hands.png',
                                        'label' => 'View your remaining credit whenever you like'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "We don’t charge exit fees!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! Unlike most other suppliers, we won’t charge you an early exit fee."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];


                                break;

                            case 'SmartMeter 12':

                                $data['new_data'][$i]['best_value'] = true;
                                $data['new_data'][$i]['saving'] = "Save up to to £252 per year";
                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'label' => '12 month fixed tariff'
                                    ],
                                    [
                                        'label' => '£50 bill credit with Eversmart installed Smartmeter'
                                    ],
                                    [
                                        'label' => 'No nasty price rises'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment. If you do not have a smart meter fitted, the discount will not be applied."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                                break;

                            case 'FixedSmart 12':

                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'img' => '/assets/images/icons/shield.png',
                                        'label' => '12 month fixed tariff'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/card.png',
                                        'label' => 'Pay by monthly direct debit'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/hands.png',
                                        'label' => 'No nasty price rises'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                                break;

                            case 'FixedSmart 18':

                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'img' => '/assets/images/icons/shield.png',
                                        'label' => '18 month fixed tariff'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/card.png',
                                        'label' => 'Pay by monthly direct debit'
                                    ],
                                    [
                                        'img' => '/assets/images/icons/hands.png',
                                        'label' => 'No nasty price rises'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                                break;

                            case 'Spring Smart Special':

                                $data['new_data'][$i]['cheapest_tariff'] = false;
                                $data['new_data'][$i]['saving'] = "Save up to to £306 per year";
                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'label' => '18 month fixed tariff'
                                    ],
                                    [
                                        'label' => '£50 bill credit with Eversmart installed Smartmeter'
                                    ],
                                    [
                                        'label' => 'No nasty price rises'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => '£50 bill credit with Eversmart installed Smartmeter'
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Receive £50 credit to spend on your Eversmart energy when you order a free smart meter with us. Ensure you have transparency with your readings and utilise energy efficiency.  Build a sustainable future and protect our environment by using our renewable electricity and metering control."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Here when you need us"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £50 per fuel' ];

                                break;

                            case 'She\'s Electric':

                                $data['new_data'][$i]['cheapest_tariff'] = true;
                                $data['new_data'][$i]['saving'] = "Save up to to £359 per year";
                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'label' => 'Cheapest fixed tariff on the market'
                                    ],
                                    [
                                        'label' => 'Pay by monthly direct debit'
                                    ],
                                    [
                                        'label' => 'No nasty price rises'
                                    ]
                                ];

                                $data['new_data'][$i]['price'] = $data['new_data'][$i]['price'];
                                $data['new_data'][$i]['monthlyPrice'] = floor($data['new_data'][$i]['price'] / 12);

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => 'Cheapest fixed tariff on the market'
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Save up to £359 a year against other standard fixed tariff on the market, calculated against the Ofgem pricing cap."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "You’ll get a free smart meter!"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you! There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];
                                $data['new_data'][$i]['tariff']['statements'][] = [ 'tag' => "p", 'text' => 'Exit Fees of £30 per fuel' ];

                                break;

                            case 'Vari Cheap':

                                $data['new_data'][$i]['cheapest_tariff'] = false;
                                $data['new_data'][$i]['saving'] = "Save up to £378.50 per year";
                                $data['new_data'][$i]['highlights'] = [
                                    [
                                        'label' => 'Variable tariff'
                                    ],
                                    [
                                        'label' => 'Pay by monthly direct debit'
                                    ],
                                    [
                                        'label' => 'No exit fees'
                                    ]
                                ];

                                $data['new_data'][$i]['benefits'] = [
                                    'tabID' => rand(),
                                    'statements' => [
                                        [
                                            'tag' => "h3",
                                            'text' => 'Here when you need us'
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "Our friendly staff are based in the UK and available 6 days a week if you need help or have a problem. You can contact us by phone, email, on live chat or over social media – whatever works for you!"
                                        ],
                                        [
                                            'tag' => "h3",
                                            'text' => "Switching is smooth and seamless"
                                        ],
                                        [
                                            'tag' => "p",
                                            'text' => "There’ll be no interruption to your gas & electricity supply during the switch."
                                        ]
                                    ]
                                ];

                                break;

                        }


                        $i++;
                    }
                }

                $data['quote_summary'] = $response['data'];
                $this->session->set_userdata('tariff_detail', $response['data']['searchResults'][0]);

                echo json_encode(['error' => false, 'data' => $data]);
                exit;
            } else {

                // Error! Unfortunately we do not have a tariff available for the search criteria you have provided
                echo $response['statusCode'];
                exit;
            }
        }
    }

    function lookup_yearly_tariff_data($dist_id, $rate_type)
    {

        // Look up
        if ($rate_type == 'STD') {
            $rate_type = '1R';
        }
        return $this->mysql->select('*')->from('new_yearly_tariff')->where('ratetype', $rate_type)->where('duosid', $dist_id)->get()->first_row('array');
    }

    function get_projection()
    {
        if ($this->input->post('quotation_pay_energy') == 'prepay') {
            $_POST['existingPayTypesElec'] = '5';
            $_POST['desiredPayTypes'] = '5';
        }
        if ($this->input->post('quotation_pay_energy') == 'directdebit') {
            $_POST['existingPayTypesElec'] = '1';
            $_POST['desiredPayTypes'] = '1';
        }
        if ($this->input->post('quotation_pay_energy') == 'paperbill') {
            $_POST['existingPayTypesElec'] = '15';
            $_POST['desiredPayTypes'] = '15';
        }


        $parameter = [
            'existingkWhsElec' => $this->input->post('elec_usage'),
            'nightUsePercentElec' => '10',
            'existingPayTypesElec' => $_POST['existingPayTypesElec'],
            'existingSupplierNameElec' => $this->input->post('quotation_supplier'),
            'existingTariffNameElec' => $this->input->post('quotation_tariff'),
            'meterTypeElec' => 'STD',
            'distId' => '10'
        ];

        //echo '<pre>'.print_r($parameter,1).'</pre>';
        $response = $this->curl->curl_request('/v2.1/partner-resources/' . $this->affiliate_id . '/projections/elec-by-annual-kwhs/', $parameter, 'post');
        //echo '<pre>'.print_r(json_decode($response,true),1).'</pre>';
        //exit;

        $response_data = json_decode($response, true);
        if ($response_data['statusCode'] == '1049') {
            $this->session->set_userdata('projection_data', $response_data['data']['energySearchProfile']);
            $this->session->set_userdata('signup_detail', $this->input->post());
            $data['projection_data'] = $response_data['data'];
            $this->load->view('energy/switch_template', $data);
        }
        if ($response_data['statusCode'] == '3042') {
            echo $response_data['statusCode'];
        }
    }

    function test_template()
    {
        $this->load->view('energy/quote');
    }

    function get_desire_pay()
    {
        $this->load->view('energy/desire_pay');
    }

    function check_mpan()
    {
        $mpan = $this->input->get('mpan');
        $db_mpan_check = $this->user_modal->db_mpan_check($mpan);
        //debug($db_mpan_check)
        if (count($db_mpan_check) > 0) {
            echo json_encode(['error' => '1', 'msg' => 'This address is already registered with us']);
        } else {
            echo json_encode(['error' => '0', 'msg' => '']);
        }
    }

    function check_mprn()
    {
        $mprn = $this->input->get('mprn');
        $db_mpan_check = $this->user_modal->db_mprn_check($mprn);
        //debug($db_mpan_check)
        if (count($db_mpan_check) > 0) {
            echo json_encode(['error' => '1', 'msg' => 'This address is already registered with us']);
        } else {
            echo json_encode(['error' => '0', 'msg' => '']);
        }
    }

    function getmprn()
    {
        $gasserial = $this->input->get('meterserial');
        $selectedpostcode = $this->input->get('postcode');

        //debug($db_mpan_check)
        if ($gasserial) {

            $gasserial_para = ['meterSerialGas' => $gasserial];
            $response = $this->curl->curl_request('/v3.1/partner-resources/EVERSMARTENERGYSUPP/mpas/addresses-by-meter-serial-gas/', $gasserial_para, 'post');
            $response = json_decode($response);

            if (!empty($response->data->addresses)) {

                for ($r = 0; $r < count($response->data->addresses); $r++) {

                    if ($response->data->addresses[$r]->pcod == $selectedpostcode) {

                        if ($response->data->addresses[$r]->metersGas[0]->meterSerialGas == $gasserial) {

                            $checkedserial = $response->data->addresses[$r]->metersGas[0]->meterSerialGas;

                            $mprn = $response->data->addresses[$r]->metersGas[0]->mprn;

                            echo json_encode(['success' => 'success', 'gasserial' => $checkedserial, 'mprn' => $mprn, 'msg' => 'We found you']);

                            break;
                        } else {
                            echo json_encode(['success' => 'fail', 'msg' => 'Could not find Mprn']);
                        }
                    } else {
                        echo json_encode(['success' => 'fail', 'msg' => 'Serial number does not relate to your postcode']);
                    }
                }
            } else {
                echo json_encode(['success' => 'fail', 'msg' => 'Serial number could not be found']);
            }
        }
    }

    function getmpan()
    {
        $elecserial = $this->input->get('meterserial');
        $selectedpostcode = $this->input->get('postcode');

        //debug($db_mpan_check)
        if (isset($elecserial)) {

            $elecserial_para = ['meterSerialElec' => $elecserial];
            $response = $this->curl->curl_request('/v3.1/partner-resources/EVERSMARTENERGYSUPP/mpas/addresses-by-meter-serial-elec/', $elecserial_para, 'post');
            $response = json_decode($response);

            if (!empty($response->data->addresses)) {

                for ($r = 0; $r < count($response->data->addresses); $r++) {

                    if ($response->data->addresses[$r]->pcod == $selectedpostcode) {

                        if ($response->data->addresses[$r]->metersElec[0]->meterSerialElec == $elecserial) {

                            $checkedserial = $response->data->addresses[$r]->metersGas[0]->meterSerialElec;

                            $mpan = $response->data->addresses[$r]->metersElec[0]->mpan;

                            echo json_encode(['success' => 'success', 'elecserial' => $checkedserial, 'mpan' => $mpan, 'msg' => 'We found you']);

                            break;
                        } else {
                            echo json_encode(['success' => 'fail', 'msg' => 'Could not find Mpan']);
                        }
                    } else {
                        echo json_encode(['success' => 'fail', 'msg' => 'Serial number does not relate to your postcode']);
                    }
                }
            } else {
                echo json_encode(['success' => 'fail', 'msg' => 'Serial number could not be found']);
            }
        }
    }


    function find_address_from_meter_serial($msn, $end_point = 'ecoes/mpans', $return_type = 'json')
    {
        $Address = array();
        $response = $this->curl->junifer_request('/rest/v1/' . $end_point . '?meterSerialNumber=' . $msn);
        $Response = $this->parseHttpResponse($response);

        if (isset($Response['body']['results']) && !empty($Response['body']['results'])) {
            $Address = ['error' => false, 'data' => $Response['body']['results']];
        } else {
            $Address = ['error' => true, 'data' => array()];
        }

        if ($return_type == 'array') {
            return $Address;
        } else {
            return json_encode($Address);
        }
    }

    function validate_card()
    {
        echo json_encode(['success' => true, 'error' => false]);
        return;
        $data = json_decode(file_get_contents('php://input'), true);

        $response = $this->curl->junifer_request('/rest/v1/directDebits/validateBankAccount', $data, 'post');
        $Response = $this->parseHttpResponse($response);

        // There is no Go Cardless UAT so always returns false in UAT
        if (isset($Response['body']['errorSeverity']) && !empty($Response['body']['errorSeverity'])) {
            echo json_encode(['success' => false, 'error' => $Response['body']['errorDescription']]);
        } else {
            echo json_encode(['success' => true, 'error' => false]);
        }
    }

    function find_address_from_meter_serial_both()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $msn = $data['meterSerialNumber'];
        $type = $data['type'];

        $Address = array();

        if ($type == 'elec') {
            $location = 'ecoes/mpans';
        } else {
            $location = 'des/mprns';
        }

        $FindAddress = $this->find_address_from_meter_serial($msn, $location, 'array');

        if (isset($FindAddress) && !empty($FindAddress)) {
            $Address = $FindAddress;
        }

        echo json_encode($Address);
    }
}
