<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
Class Api extends CI_Controller{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api_modal');
    $this->load->model('user_modal');
    if( !empty($_SERVER['HTTP_HOST'] == 'localhost') )
		{
			$this->upload_path = 'E:\server\htdocs\eversmartnew';
		}
		else
		{
			$this->upload_path = '/var/www/html/eversmartnew';
		}

      $this->mysql = $this->load->database('mysql',true);

  }


  public static function logMessage(String $errLevel, $msg, $depth=1)
    {
      $btStack = debug_backtrace($depth);
      $callerFrame = $btStack[$depth-1];

      $sid = substr(session_id(),0,8);
      $method = $callerFrame['class'].'.'.$callerFrame['function'];
      $lineNo = $callerFrame['line'];
      $fullMessage = 'SID:'.$sid.' '.$method.'('.$lineNo.'): '.$msg;

      log_message($errLevel, $fullMessage);
    }

  function parseHttpResponse($response)
    {
        // Parse the headers
      $headers = array();

      $body = null;

      // Split up the httpResponse
      $responseParts = explode("\r\n\r\n", $response);
      foreach ($responseParts as $i => $responsePart)
        {
          $lines = explode("\r\n", $responsePart);

          // If the part has 200 response Code, parse the rest of the lines as headers.
          //if($lines[0] == "HTTP/1.1 " )
          if(strpos($lines[0], 'HTTP/1.1') !== false )
          {
          if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0]))
            {
            // 4xx or 5xx error
            // so log the error
            $this->logMessage('error', 'Line 0='.$lines[0], 2);
            }

          foreach ($lines as $j => $line)
            {
                if ($j === 0)
            {
                    $headers['http_code'] = $line;
            }
                else
                {
                    list ($key, $value) = explode(': ', $line);

                    $headers[$key] = $value;
                }
            }
          }
          // If last element, probably body
          else if($i === sizeof($responseParts)-1)
          {
            $body = json_decode($responsePart,true);
          }
        }

        return array(
          'headers' => $headers,
          'body' => $body
        );
    }

  function image_upload(){
      $customer_id = $this->input->post('customer_id');
      $meterIdentifier = $this->input->post('meterIdentifier');
      if(!isset($meterIdentifier) || $meterIdentifier=='' )
      {
        $this->jres->failure('meterIdentifier required');
      }
      else if( !isset($customer_id) || $customer_id=='' ){
        $this->jres->failure('customer_id required');
      }
      else if( !isset($_FILES) || $_FILES=='' ){
        $this->jres->failure('meter_image required');
      }else{
        $target_dir = $this->upload_path.'/upload/meter';
        $temp_path = $_FILES['meter_image']['tmp_name'];
        $image_name = $_FILES['meter_image']['name'];
        $new_file_name = date('Y_m_d').'_'.$image_name;
        $new_path = $target_dir.'/'.$new_file_name;
        if (move_uploaded_file($temp_path, $new_path)) {
              $row = [
                'meterIdentifier' => $meterIdentifier,
                'customer_id' => $customer_id,
                'image_path' => '/upload/meter/'.$new_file_name,
                'create_at' => date('Y-m-d H:i:s')
              ];
            $upload_meter_image = $this->api_modal->upload_meter_image($row);
            if( $upload_meter_image > 0 ){
              $this->jres->success([],'image uploaded');
            }else{
                $this->jres->failure('database error');
            }
          }else{
              $this->jres->failure('image upload fail');
          }
      }
  }

  function get_meter_image(){
    $customer_id = $this->input->post('customer_id');
    if(!isset($customer_id) || $customer_id=='' )
    {
      $this->jres->failure('customer_id required');
    }else{
      $get_list = $this->api_modal->get_customer_meter_image($customer_id);
      if( !empty($get_list) ){
        for( $m=0; $m<count($get_list); $m++ ){
          $get_list[$m]['image_path'] = base_url().$get_list[$m]['image_path'];
        }
        $this->jres->success($get_list,'image uploaded');
      }else{
        $this->jres->failure('no data available');
      }
    }

  }

  function login(){

    $email = $this->input->post('email');
    $password = $this->input->post('password');
    if(!isset($email) || $email=='' )
    {
      $this->jres->failure('email required');
    }
    elseif(!isset($password) || $password==''){
      $this->jres->failure('password required');
    }else{
      $check_user_active = $this->api_modal->check_user_active( $email, $password );
      //debug($check_user_active,1);
      if( !empty($check_user_active) ){
        if($check_user_active['active'] == '0')
        {
          $this->jres->failure('account is not active');
          exit;
        }else{
          if( $check_user_active['signup_type'] == '0' || $check_user_active['signup_type'] == '2' )
          {
              $info['title'] = $check_user_active['title'];
              $info['id'] = $check_user_active['id'];
              $info['first_name'] = $check_user_active['forename'];
              $info['last_name'] = $check_user_active['surname'];
              $info['signup_type'] = $check_user_active['signup_type'];
              $info['user_status'] = $check_user_active['api_user_status'];
              $info['phone_number'] = $check_user_active['phone1'];
              $info['user_mprn'] = $check_user_active['gasProduct_mprns'];
              $info['address'] = $check_user_active['address_address1'];
              $info['mpancore'] = $check_user_active['elec_mpan_core'];

              $info['first_reading_gas'] = $check_user_active['first_reading_gas'];
              $info['first_reading_elec'] = $check_user_active['first_reading_elec'];
               //$info['status'] = $check_user_active['active'];



              $info['elec_meter_serial'] = $check_user_active['elec_meter_serial'];
              $info['gas_meter_serial'] = $check_user_active['gas_meter_serial'];

              $info['customer_id'] = $check_user_active['customer_id'];
              $info['account_id'] = $check_user_active['account_id'];
              $info['meterTypeElec'] = $check_user_active['meterTypeElec'];

              $info['email'] = $check_user_active['email'];
              $info['mpancore'] = $check_user_active['elec_mpan_core'];
              $info['meterpoint_mpan'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPAN"];
              $info['meterpoint_mprn'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPRN"];

              // echo '/rest/v1/meterPoints?meterSerialNumber='.$check_user_active['gas_meter_serial'].'&queryDttm='.date('Y-m-d H:i:s');
              // exit;

              $account_balanace = $this->curl->junifer_request('/rest/v1/accounts/'.$check_user_active['account_id']);
              $response = $this->parseHttpResponse($account_balanace);
              //debug($response,1);
              if( $response['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                //$info['balance'] = $response['body']['balance'];
                $info['balance'] = $response['body']['balance'];
                $info['account_number'] = $response['body']['number'];
              }else{
                $info['balance'] = 0;
                $info['account_number'] = 0;
              }

          $this->jres->success($info,'data fetched');

      }else{
          $this->jres->failure('invalid login detaill');
      }

    }

  }else{
          $this->jres->failure('invalid login detail');
      }

}
} // login


  function downloadbill($billid)
    {
    	$data['pdf_url'] = 'http://18.191.137.119/index.php/user/downloadbill/2';
    	debug($data,1);

      $curl = curl_init();
      if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '18.191.137.119' ||  $_SERVER['HTTP_HOST'] == '13.58.101.121'  )  //local
      {


      curl_setopt_array($curl, array(
        CURLOPT_PORT => "43002",
        CURLOPT_URL => "http://134.213.125.150:44002/rest/v1/billFiles/".$billid."/image",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "X-Junifer-X-apikey: zH7bDom4",
          "X-Junifer-X-username: APIUser"
        ),
      ));

    }

    if(  $_SERVER['HTTP_HOST'] == '52.56.76.183' || $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk' ) //dev
    {

      curl_setopt_array($curl, array(
      CURLOPT_PORT => "43002",
      CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/billFiles/".$billid."/image",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        'X-Junifer-X-apikey: dgiZaFDb',
        'X-Junifer-X-username: api',
      ),
    ));
  }
  }

function get_account(){

  $account_id = $this->input->post('account_id');
  if(!isset($account_id) || $account_id=='' )
  {
    $this->jres->failure('account_id required');
  }else{

  $account_balanace = $this->curl->junifer_request('/rest/v1/accounts/'.$account_id);
  $response = $this->parseHttpResponse($account_balanace);
  //debug($response,1);
  if( $response['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
    //$info['balance'] = $response['body']['balance'];
    $info['balance'] = $response['body']['balance'];
    $info['account_number'] = $response['body']['number'];
    $this->jres->success($info,'data fetched');
  }else{
    $info['balance'] = 0;
    $info['account_number'] = 0;
    //$this->jres->failure('invalid or empty account id');
    $this->jres->success($info,'data fetched');
  }

}

} //get account

// get reading from junifer
function get_reading(){
  $account_id = $this->input->post('account_id');

   if(!isset($account_id) || $account_id=='' )
  {
    $this->jres->failure('account_id required');
  }else{

      $current_year = date('Y-m-d');
    $last_year = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
    $fromdate_monthly = date('Y-m-01');        //date('Y-m-01')
    $todate_monthly = date('Y-m-01', strtotime("+1 month"));


          $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$account_id.'/agreements');
          $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);




          if( !empty( $get_meterpoint_ids_responce->results ) ){
            for( $r = 0; $r<count( $get_meterpoint_ids_responce->results ) ; $r++ ){


              if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

              if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

                $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

              }
            }

              elseif ( $get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

                if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

                  $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

        }
        }
        }}


 if (isset($electric_meter_point_id)) {
              $el_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
              $response = json_decode($el_response_monthly_array,true);
              $data['elec_reading'] = $response['results'];
              $data['last_reading_elec'] = end($response['results']);
   }


 if (isset($gas_meter_point_id)) {
             $gas_response  = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
             $gas_response = json_decode($gas_response,true);
             $gas_reading = $gas_response['results'];
             $last_reading_gas = end($gas_response['results']);
             //var_dump($data['gas_results']);
 }

 if( !empty($elec_reading) || !empty($last_reading_elec) )
 {
    $data['elec_reading'] = $elec_reading;
    $data['last_reading_elec'] = $last_reading_elec;
 }
 else
 {
    $data['elec_reading'] = array();
    $data['last_reading_elec'] = array();
 }

 if( !empty($gas_reading) || !empty($last_reading_gas) )
 {
    $data['gas_reading'] = $gas_reading;
    $data['last_reading_gas'] = $last_reading_gas;
 }
 else
 {
    $data['gas_reading'] = array();
    $data['last_reading_gas'] = array();
 }

  } // else loop

  if( !empty($data) )
  {
    $this->jres->success($data);
  }else
  {
    $this->jres->failure('no data found');
  }

}

//submit reading
function submit_reading(){

    $get_user_account_id = $this->input->post('account_id');
    $reading_day = $this->input->post('reading_day');
    $reading_night = $this->input->post('reading_night');
    $gas_reading = $this->input->post('gas_reading');
    $meterTypeElec = $this->input->post('meterTypeElec');
    $id = $this->input->post('id');
    $supplyType = $this->input->post('supplyType');

    if(!isset($get_user_account_id) || $get_user_account_id=='' )
    {
      $this->jres->failure('account_id required');
      exit;
    }
    else if(!isset($reading_day) || $reading_day=='' )
    {
      $this->jres->failure('reading required');
      exit;
    }
    else if(!isset($id) || $id=='' )
    {
      $this->jres->failure('id required');
      exit;
    }
    else if(!isset($supplyType) || $supplyType=='' )
    {
      $this->jres->failure('supplyType required');
      exit;
    }
    else{
      $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$get_user_account_id.'/agreements');
      $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);
      //debug($get_meterpoint_ids_responce,1);


      if( !empty($get_meterpoint_ids_responce) ){
        if( !isset($get_meterpoint_ids_responce->errorCode) ){
        for( $r = 0; $r<count( $get_meterpoint_ids_responce->results ) ; $r++ ){


          if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

          if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

            $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

          }
        }

          elseif ( $get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

            if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

              $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

    }
    }
    }
  }
}

      if(!isset($electric_meter_point_id)){
          echo json_encode([ 'error' => '1', 'errorDescription' => 'electric_meter_point_id not found']);
      }

    if( $supplyType == 'elec' || $supplyType == 'electricity' ){
      if( $meterTypeElec === 'E7'  )
      {
          $parameter =  json_encode(array(
            "ignoreWarnings" => "false",
            "readingDt" => date('Y-m-d'),
            "sequenceType" => "Normal",
            "source" => "Customer",
            "quality" => "Normal",
            "registerReads" => array(array(
                  "reading" => $reading_day,
                  "readingType"=> "Day"),
                  array(
                  "reading" => $reading_night,
                  "readingType"=> "Night",
                  )
            )
          )
          );
      }
      else
      {
          $parameter =  json_encode(array(
            "ignoreWarnings" => "false",
            "readingDt" => date('Y-m-d'),
            "sequenceType" => "Normal",
            "source" => "Customer",
            "quality" => "Normal",
            "registerReads" => array(array(
                  "reading" => $reading_day,
                  "readingType"=> "Standard"
            ))
          )
          );
      }
      //echo $supplyType; exit;


    if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '13.58.101.121' || $_SERVER['HTTP_HOST'] == '18.191.137.119' )  //local
    {

        $curl_post_readings = curl_init();
        curl_setopt( $curl_post_readings, CURLOPT_POSTFIELDS,  $parameter);
        curl_setopt_array($curl_post_readings, array(
        CURLOPT_PORT => "43002",

        //junifer local
        CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/meterPoints/".$electric_meter_point_id."/readingsWithoutMtds",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",

        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
                      // local junifer
                    'X-Junifer-X-apikey: zH7bDom4',
                    'X-Junifer-X-username: apiuser1',
        ),
        ));
    }


    //if( $_SERVER['HTTP_HOST'] == '18.191.137.119' ) //production
    if( $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk' )
    {

        $curl_post_readings = curl_init();
        curl_setopt( $curl_post_readings, CURLOPT_POSTFIELDS,  $parameter);
        curl_setopt_array($curl_post_readings, array(
        CURLOPT_PORT => "43002",

         CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/meterPoints/".$electric_meter_point_id."/readingsWithoutMtds",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",

        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
                      // production junifer
                      'X-Junifer-X-apikey: dgiZaFDb',
                      'X-Junifer-X-username: api',
        ),
        ));
    }

    $responsey = curl_exec($curl_post_readings);
    $err = curl_error($curl_post_readings);
    $responsey = json_decode($responsey);
    curl_close($curl_post_readings);

    $db_parameter = [
                'customer_id' => $id,
                'meter_reading' => $reading_day,
                'fuel_type_id' => 2 // Elec
            ];

    if (isset($responsey->errorDescription)) {

    //echo json_encode([ 'error' => '1', 'errorDescription' => $responsey->errorDescription]);
    $this->jres->failure($responsey->errorDescription);

    }

   else {

    $electric_first_update = $this->user_modal->electric_first_update( $id );
     $this->user_modal->log_meter_reading($db_parameter);
      //echo json_encode([ 'error' => '0', 'updated' => $electric_first_update,  ]);
      $this->jres->success([],$electric_first_update);
    }

    }//type elec ends

    if( $supplyType == 'gas' )
    {
     // echo $supplyType; exit;
         $parameter =  json_encode(array(
            "ignoreWarnings" => "false",
            "readingDt" => date('Y-m-d'),
            "sequenceType" => "Normal",
            "source" => "Customer",
            "quality" => "Normal",
            "registerReads" => array(array(
                  "reading" => $reading_day,
                  "readingType"=> "Standard"
            ))
          )
          );


          if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '13.58.101.121' || $_SERVER['HTTP_HOST'] == '18.191.137.119'  )  //local
          {
            $curl_post_readings = curl_init();
            curl_setopt( $curl_post_readings, CURLOPT_POSTFIELDS,  $parameter);
            curl_setopt_array($curl_post_readings, array(
              CURLOPT_PORT => "43002",

              //junifer local
             CURLOPT_URL => "http://134.213.125.150:44002/rest/v1/meterPoints/".$gas_meter_point_id."/readingsWithoutMtds",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
             CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                            // local junifer
                           'X-Junifer-X-apikey: zH7bDom4',
                     'X-Junifer-X-username: apiuser1',

              ),
            ));
          }


          //if( $_SERVER['HTTP_HOST'] == '18.191.137.119' ) //production
          if( $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk' )
          {
            $curl_post_readings = curl_init();
              curl_setopt( $curl_post_readings, CURLOPT_POSTFIELDS,  $parameter);
              curl_setopt_array($curl_post_readings, array(
                CURLOPT_PORT => "43002",

                 //junifer production
                CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/meterPoints/".$gas_meter_point_id."/readingsWithoutMtds",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
               CURLOPT_HTTPHEADER => array(
                  "Cache-Control: no-cache",
                  "Content-Type: application/json",
                              // production junifer
                              'X-Junifer-X-apikey: dgiZaFDb',
                              'X-Junifer-X-username: api',
                ),
              ));
                        }


              $responsey = curl_exec($curl_post_readings);
              $err = curl_error($curl_post_readings);
              $responsey = json_decode($responsey);
              curl_close($curl_post_readings);

               $db_parameter = [
                'customer_id' => $id,
                'meter_reading' => $reading_day,
                'fuel_type_id' => 1 // Gas
            ];


                         // var_dump($parameter);

                        // $responsey = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readingsWithoutMtds', $parameter, 'post');
                       // $response = $this->curl->junifer_request_test('/rest/v1/meterPoints/402/readings', $parameter, 'post');
                        // $response = $this->parseHttpResponse($response);
                       //$responsey = json_decode($responsey,true);

                          //var_dump($err);
                        // debug($response,1);

                        if (isset($responsey->errorDescription)) {

                          //echo json_encode([ 'error' => '1', 'errorDescription' => $responsey->errorDescription]);
                          $this->jres->failure($responsey->errorDescription);
                        }

                        else {

                          $gas_first_update = $this->user_modal->gas_first_update( $id );
                          $this->user_modal->log_meter_reading($db_parameter);
                          //echo json_encode([ 'error' => '0', 'updated' => $gas_first_update,  ]);
                          $this->jres->success([],$gas_first_update);
                        }
    } //type gas ends

    } //main else


} //end function

// add the billing address to the database
function insert_billing_address($data){
    $this->mysql->insert('billing_address',$data);
    return $this->mysql->insert_id();
}

// tie the customer to the billing address
function insert_customer_billing_address($data){
    $this->mysql->insert('customer_billing_address',$data);
    return $this->mysql->insert_id();
}

// payment clear balance
function clear_payment(){

    $stripe_amount = $this->input->post('amount') * 100;  // A positive integer in the smallest currency unit (e.g., 100 pence to charge £1.00)
    $amount = $this->input->post('amount');
    $account_id = $this->input->post('account_id');
    $token = $this->input->post('token');
    $junifer_account_id = $this->input->post('junifer_account_id');
    $email = $this->input->post('email');
    $billing_address_id = $this->input->post('billing_address_id');
    //$line1 = $this->input->post('existing_line1');
    //$postcode = preg_replace('/\s+/', '', $this->input->post('existing_postcode'));
    $currency = 'gbp';

    // Have we been passed an existing billing address?
    if($billing_address_id > 0 ) {

        $ExistingBillingAddress = $this->mysql->select('*')->from('billing_address')->where('billing_address_id', $billing_address_id)->get()->result_array();
        //$line1 = $ExistingBillingAddress['address_line_1'];
        //$postcode = preg_replace('/\s+/', '', $ExistingBillingAddress['postcode']);

    }
    // Have we been passed a new billing address?
    else if(
        $this->input->post('ba_line1') !='' && $this->input->post('ba_line2') !='' &&
        $this->input->post('ba_postcode') !='' && $this->input->post('customer_id') !=''
    ) {

        // Set the db parameters in an array
        $ba_db_parameter['address_line_1'] = $this->input->post('ba_line1');
        $ba_db_parameter['address_line_2'] = $this->input->post('ba_line2');
        if($this->input->post('ba_line3') != '') { $ba_db_parameter['address_line_3'] = $this->input->post('ba_line3'); }
        if($this->input->post('ba_line4') != '') { $ba_db_parameter['address_line_4'] = $this->input->post('ba_line4'); }
        $ba_db_parameter['postcode'] = $this->input->post('ba_postcode');

        // Do the insert
        $BillingAddressID = $this->insert_billing_address($ba_db_parameter);

        // Now tie the new address to the customer
        $cba_db_parameter['customer_id'] = $this->input->post('customer_id');
        $cba_db_parameter['billing_address_id'] = $BillingAddressID;
        $CustomerBillingAddressID = $this->insert_customer_billing_address($cba_db_parameter);

        // Overwrite the variables with the new billing address details
        //$line1 = $this->input->post('ba_line1');
        //$postcode = preg_replace('/\s+/', '', $this->input->post('ba_postcode'));

    }


    if(!isset($amount) || $amount=='' ) {
		$this->jres->failure('amount required');
    }elseif(!isset($account_id) || $account_id=='' ) {
      $this->jres->failure('account_id required');
    }else{


        $stripe = array(
          "secret_key"      => $this->config->item('stripe')['sk_test'],
          "publishable_key" => $this->config->item('stripe')['pk_test']
        );



        try{

            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            // Does the customer already exist?
            $customer = \Stripe\Customer::all(array("email" => $email, "limit" => 1));

            if(empty($customer['data'])){ // If the customer doesn't exist, create them

                $customer = \Stripe\Customer::create(array(
                    'email' => $email,
                    'source'  => $token)
                );

                // And catch the id to create the charge
                $stripe_customer_id = $customer['id'];
                $stripe_customer_token = $customer['default_source'];

            } else {

                // Get customer id
                $stripe_customer_id = $customer['data'][0]['id'];
                $stripe_customer_token = $customer['data'][0]['default_source'];
            }


            $charge = \Stripe\Charge::create(array(
              'customer'   => $stripe_customer_id,
              'amount' => $stripe_amount,
              'source' =>$stripe_customer_token,
              'currency' => $currency,
            ));

            if($charge['status'] == 'succeeded' && isset($stripe_customer_id)){

                $response['transaction_id'] = $charge['id'];
                $response['account_id'] = $account_id;
                $response['amount'] = $amount;

                $row = [
                  'transaction_id' => $charge['id'],
                  'amount' => $amount,
                  'currency' => $currency,
                  'status' => $charge['status'],
                  'account_id' => $account_id
                ];

                $post = [
                    "grossAmount" => 	$amount,
                    "salesTaxName" => "Standard VAT",
                    "accountDebitReasonName" => "Saasquatch Referral"
                ];


                $junifer_credit = $this->curl->junifer_request('/rest/v1/accounts/'.$junifer_account_id.'/accountDebits', $post, 'post');

                $get_head = $this->parseHttpResponse($junifer_credit);

                //debug($get_head,1);

                $this->api_modal->save_transaction($row);


                if( $get_head['headers']['http_code'] == 'HTTP/1.1 204 No Content')
                {
                    $this->jres->success_stripe($response,$charge['status']);
                }
                else
                {
                    $this->jres->failure('Error while payment in stripe');
                }

            }

        }
        catch( \Stripe\Error\Card $e )
        {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $this->jres->failure($err);
        }
        catch (\Stripe\Error\RateLimit $e) {
          // Too many requests made to the API too quickly
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $this->jres->failure($err);
        }
    }
} // function end


function create_yearly_customer(){


  $stripe_amount = $this->input->post('amount') * 100;  // A positive integer in the smallest currency unit (e.g., 100 pence to charge £1.00)
  $amount = $this->input->post('amount');

  $account_id = $this->input->post('account_id');
  $token = $this->input->post('token');
  $junifer_account_id = $this->input->post('junifer_account_id');
  $email = $this->input->input_stream('email');
  $billing_address_id = $this->input->post('billing_address_id');
  //$line1 = $this->input->post('existing_line1');
  //$postcode = preg_replace('/\s+/', '', $this->input->post('existing_postcode'));
  $currency = 'gbp';


  // Have we been passed an existing billing address?
  if($billing_address_id > 0 ) {

      $ExistingBillingAddress = $this->mysql->select('*')->from('billing_address')->where('billing_address_id', $billing_address_id)->get()->result_array();
      //$line1 = $ExistingBillingAddress['address_line_1'];
      //$postcode = preg_replace('/\s+/', '', $ExistingBillingAddress['postcode']);

  }
  // Have we been passed a new billing address?
  else if(
      $this->input->post('ba_line1') !='' && $this->input->post('ba_line2') !='' &&
      $this->input->post('ba_postcode') !='' && $this->input->post('customer_id') !=''
  ) {

      // Set the db parameters in an array
      $ba_db_parameter['address_line_1'] = $this->input->post('ba_line1');
      $ba_db_parameter['address_line_2'] = $this->input->post('ba_line2');
      if($this->input->post('ba_line3') != '') { $ba_db_parameter['address_line_3'] = $this->input->post('ba_line3'); }
      if($this->input->post('ba_line4') != '') { $ba_db_parameter['address_line_4'] = $this->input->post('ba_line4'); }
      $ba_db_parameter['postcode'] = $this->input->post('ba_postcode');

      // Do the insert
      $BillingAddressID = $this->insert_billing_address($ba_db_parameter);

      // Now tie the new address to the customer
      $cba_db_parameter['customer_id'] = $this->input->post('customer_id');
      $cba_db_parameter['billing_address_id'] = $BillingAddressID;
      $CustomerBillingAddressID = $this->insert_customer_billing_address($cba_db_parameter);

      // Overwrite the variables with the new billing address details
      //$line1 = $this->input->post('ba_line1');
      //$postcode = preg_replace('/\s+/', '', $this->input->post('ba_postcode'));

  }


  if(!isset($amount) || $amount=='' ) {
  $this->jres->failure('amount required');
  
  }else{


      $stripe = array(
        "secret_key"      => $this->config->item('stripe')['sk_test'],
        "publishable_key" => $this->config->item('stripe')['pk_test']
      );



      try{

          \Stripe\Stripe::setApiKey($stripe['secret_key']);

          // Does the customer already exist?
          $customer = \Stripe\Customer::all(array("email" => $email, "limit" => 1));

          if(empty($customer['data'])){ // If the customer doesn't exist, create them

              $customer = \Stripe\Customer::create(array(
                  'email' => $email,
                  'source'  => $token)
              );

              // And catch the id to create the charge
              $stripe_customer_id = $customer['id'];
              $stripe_customer_token = $customer['default_source'];

          } else {

              // Get customer id
              $stripe_customer_id = $customer['data'][0]['id'];
              $stripe_customer_token = $customer['data'][0]['default_source'];
          }


          if(isset($stripe_customer_id)){

           echo json_encode(['api_status' => '1', 'token' => $token, 'message' => 'Signup Successful']);

          }
          else
          {
            echo json_encode(['api_status' => '2', 'token' => $token, 'message' => 'There was an issue validating your card']);
          }

      }
      catch( Stripe_Error $e )
      {
          $this->jres->failure($e->getMessage());
         
      }
  }
} // function end


  function payments(){
      $account_id = $this->input->post('account_id');
      if(!isset($account_id) || $account_id=='' )
  		{
    		  $this->jres->failure('account_id required');
      }else{
        $get_payments = $this->curl->junifer_request('/rest/v1/ecoes/accounts/'.$account_id.'/paymentSchedulePeriods');
        $response = $this->parseHttpResponse($get_payments);
        //debug($response);
        // if($response['headers']['http_code'] == 'HTTP/1.1 404 Not Found'){
        //   $this->jres->failure('server error');
        //   exit;
        // }
        // if($response['headers']['http_code'] == 'HTTP/1.1 200 Ok')
        // {
        //   $data['nextPaymentDt'] = $response['body']['results']['nextPaymentDt'];
        //   $data['amount'] = $response['body']['results']['amount'];
        //   $this->jres->success($data);
        // }

        $data['nextPaymentDt'] = "2015-04-07";
        $data['amount'] = "80.00";
        $this->jres->success($data);
      }
  }

  function last_payment(){
        $account_id = $this->input->post('account_id');
        if(!isset($account_id) || $account_id=='' )
        {
            $this->jres->failure('account_id required');
        }else{
          $get_payments = $this->curl->junifer_request('/rest/v1/accounts/'.$account_id.'/payment');
          $response = $this->parseHttpResponse($get_payments);
          //debug($response);
          // if($response['headers']['http_code'] == 'HTTP/1.1 404 Not Found'){
          //   $this->jres->failure('server error');
          //   exit;
          // }
          // if($response['headers']['http_code'] == 'HTTP/1.1 200 Ok')
          // {
          //   $data['nextPaymentDt'] = $response['body']['results']['nextPaymentDt'];
          //   $data['amount'] = $response['body']['results']['amount'];
          //   $this->jres->success($data);
          // }

          $array = [
            [
              'id' => 1,
              'amount' => 80.00,
              'acceptedDttm'=> "2015-02-16T00:00:00.000",
            ],
            [
              'id' => 1,
              'amount' => 80.00,
              'acceptedDttm'=> "2015-02-13T00:00:00.000",
            ]
          ];
          $this->jres->success($array);
        }
  }

  function bills(){
        $account_id = $this->input->post('account_id');
        //debug( $this->input->post());
        if(!isset($account_id) || $account_id=='' )
        {
            $this->jres->failure('account_id required');
        }else{

           $login_response = $this->user_modal->get_account_detail( $account_id );

          if( $login_response['signup_type'] == 0 || $login_response['signup_type'] == 2 ){

            //$get_user_account_id = $this->session->userdata('login_data')['account_id'] ;

          $response = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$account_id.'/bills');
          //$image_download = $this->curl->junifer_request_test('/rest/v1/billFiles/7428');
          $response = json_decode($response,true);

          //debug($image_download,1);
          if( !empty($image_download) ){
            $data['image_download'] = $image_download['links']['self'];
          }
         // debug($response,1);
          if( !empty($response) )
          {
            $this->jres->success($response['results']);
          }
          else
          {
            $this->jres->failure('no bills found');
          }
       //   $response = json_decode($response,true);

        }
      }
    }

    function stripetest(){
      $this->load->view('user/teststripe');
    }

    function account_status()
    {
        $user_id = $this->input->post('user_id');
        if(!isset($user_id) || $user_id=='' )
        {
            $this->jres->failure('user_id required');
        }else{

              $login_response = $this->user_modal->get_account_detail( $user_id );

               if( !empty($login_response) ){

      $elecMeterPointID = 'na';
      $gasMeterPointID = 'na';
        //$next_week = date('d/m/Y', strtotime($login_response['create_at'].'+1 week')); // 26/05/2014

        if($login_response['signup_type'] == 0 || $login_response['signup_type'] == 2)
        {

          $response = $this->curl->junifer_request('/rest/v1/accounts/'.$login_response['account_id'].'/agreements');
          $response = $this->parseHttpResponse($response);
          //debug($response,1);

          if( $response['headers']['http_code'] == 'HTTP/1.1 200 OK' )
          {


             $agreementResponse = $response['body']['results'];
             //debug($agreementResponse,1);

             // update meterpointid in db

            if( !empty($agreementResponse) ){
              for( $r = 0; $r<count( $agreementResponse ) ; $r++ ){


                if ($agreementResponse[$r]['products'][0]['type'] == 'Electricity Supply') {

                if(isset($agreementResponse[$r]['products'][0]['assets'][0]['id'])){

                  $elecMeterPointID = $agreementResponse[$r]['products'][0]['assets'][0]['id'] ;

                }
              }

                elseif ( $agreementResponse[$r]['products'][0]['type'] == 'Gas Supply') {

                  if(isset($agreementResponse[$r]['products'][0]['assets'][0]['id'])){

                    $gasMeterPointID  = $agreementResponse[$r]['products'][0]['assets'][0]['id'] ;

          }
          }
          }}



             $data1['elecMeterPointID'] = $elecMeterPointID;
             $data1['gasMeterPointID'] = $gasMeterPointID;
             //debug($data1,1);
             $updateMeterPointID = $this->api_modal->updateMeterPointID($data1,$login_response['id'] );

          } // if loop

          $elec_status = '';
          $gas_status = '';

              $check_meterpoint = $this->api_modal->get_account_detail($login_response['id']);
              if( $check_meterpoint['elecMeterPointID'] != 'na' || $check_meterpoint['gasMeterPointID'] != 'na' )
              {



                if( $check_meterpoint['elecMeterPointID'] != 'na' ) {

                $elec_response = $this->curl->junifer_request('/rest/v1/meterPoints/'.$check_meterpoint['elecMeterPointID'].'/supplyStatusHistory');
                $elec_response = $this->parseHttpResponse($elec_response);

                //debug($elec_response);
                //get status of elec

                if( $elec_response['headers']['http_code'] == 'HTTP/1.1 200 OK' )
                {
                    $elec_status = $elec_response['body']['history'][0]['eventType'];
                }
                else
                {
                  $elec_status = 'na';
                }

              }
              else
              {
                $elec_status = 'na';
              }

              if( $check_meterpoint['gasMeterPointID'] != 'na' ) {

                $gas_response = $this->curl->junifer_request('/rest/v1/meterPoints/'.$check_meterpoint['gasMeterPointID'].'/supplyStatusHistory');
                $gas_response = $this->parseHttpResponse($gas_response);
                //debug($elec_response);
                //get status of gas
                if( $gas_response['headers']['http_code'] == 'HTTP/1.1 200 OK' )
                {
                    $gas_status = $gas_response['body']['history'][0]['eventType'];
                }
                else
                {
                  $gas_status = 'na';
                }
              }
              else
              {
                $gas_status = 'na';
              }
            }

          if( $gas_status == 'RegistrationCompleted' || $elec_status=='RegistrationConfirmed' )
          {
            $difference = '0';
            $data['current_day'] = '21';
          }
          else {
            $difference = 14;
            $data['current_day'] = '13';
          }

        }

        $login_response['create_at'];
        $next_week = strtotime($login_response['create_at'].'+20 days');
        $noOfDay = date( 'Y-m-d', $next_week );  // date after 21 days



        //echo $now = date( 'Y-m-d', strtotime($login_response['create_at'])); // or your date as well
        $now = date('Y-m-d');
        $date1 = date_create( $now);
        $date2 = date_create(date( 'Y-m-d', strtotime($login_response['create_at'])));

        //difference between two dates
        $diff = date_diff($date1,$date2);

        //count days
        //$info['current_day'] = $diff->format("%a");


        $difference = $next_week - time(); // next weeks date minus todays date
        $difference = date('j', $difference);
        $difference . (($difference > 1) ? ' days ' : ' day ') . ' left';

            // if( $difference >= 21  )
            // {
            //   $difference = '0';
            // }

            $data['cooling_off'] = $difference  ;
            $data['phone1'] = $login_response['phone1'] ;
            $info['create_at'] = $login_response['create_at'];
            $data['first_reading_gas'] = $login_response['first_reading_gas'];
            $data['first_reading_elec'] = $login_response['first_reading_elec'];
            $info['booked_meter'] = $login_response['booked_meter'];
            $info['day_left'] = $difference;
            $info['id'] = $login_response['id'];
            $info['title'] = $login_response['title'];
            $info['first_name'] = $login_response['forename'];
            $info['last_name'] = $login_response['surname'];
            $info['signup_type'] = $login_response['signup_type'];
            $info['account_status'] = $login_response['api_user_status'];
            // $info['user_mpan'] = $check_user_active['electricityProduct_mpans'];
            $data['user_mprn'] = $login_response['gasProduct_mprns'];
            $info['address'] = $login_response['first_line_address'];
            $info['town_address'] = $login_response['town_address'];
            $info['city_address'] = $login_response['city_address'];
            $data['mpancore'] = $login_response['elec_mpan_core'];

            $data['elec_meter_serial'] = $login_response['elec_meter_serial'];
            $data['gas_meter_serial'] = $login_response['gas_meter_serial'];

            $data['customer_id'] = $login_response['customer_id'];
            $data['account_id'] = $login_response['account_id'];
            $data['account_number'] = $login_response['account_id'];

            $info['email'] = $login_response['email'];
            $info['mpancore'] = $login_response['elec_mpan_core'];
            $info['meterpoint_mpan'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPAN"];
            $info['meterpoint_mprn'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPRN"];

            $info['billingMethod'] = $login_response['billingMethod'];
            $info['tariff_newspend'] = $login_response['tariff_newspend'];
            $info['exitFeeElec'] = $login_response['exitFeeElec'];
            $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
            $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];
                $this->session->set_userdata('login_data',$info);
            }
            $data['elecMeterPointID'] = $elecMeterPointID;
            $data['gasMeterPointID'] = $gasMeterPointID;

          if( !empty($login_response) )
          {

            $this->jres->success($data);
          }
          else{
            $this->jres->failure('no data available for this account');
          }
        }

    }

    function audio_test(){
      $this->load->view('address/test_file');
    }

    function booked_meter()
    {
        $user_id = $this->input->post('user_id');
        $meter_value = $this->input->post('meter_value');
        //$contact_no = $this->input->post('meter_contact');
        if(!isset($meter_value) || $meter_value=='' )
        {
            $this->jres->failure('meter_value required');
        }elseif(!isset($user_id) || $user_id=='')
        {
          $this->jres->failure('user_id required');
        }
          else{
            $this->user_modal->book_meter($user_id,$meter_value);

            $login_response = $this->user_modal->get_account_detail( $user_id );

            $account_id = $login_response['account_id'] ;
            $user_phone = $login_response['phone1'] ;
            $post_code =  $login_response['address_postcode'] ;
            $name = $login_response['forename'] ;
            $lastname = $login_response['surname'] ;

            $token = md5(uniqid(rand(0,9999), true));

            $this->book_meter($account_id, $token, $name, $login_response['email'],$user_phone, $post_code);
            $this->book_meter($account_id, $token, $name, 'Jack.Akrigg@eversmartenergy.co.uk',$user_phone, $post_code);

           // $token_in_database = $this->user_modal->token_in_database($token,$email_check['id'],'1');
            //echo  json_encode([ 'error' => '0', 'msg'=>'Mail send', 'Post code' => $post_code, 'Phone' =>  $user_phone , 'name'=> $name, 'last name' => $lastname]);
            if( $login_response )
            {
              $this->jres->success([],'Mail send');
            }
            else{
              $this->jres->failure('server error');
            }

        }
    }


    function book_meter($customer_info_id, $token, $forename, $recipient_email, $user_phone, $post_code) {

        //$customer_id = 528;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$user_phone = '[user_phone]';
        //$post_code = '[post_code]';

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename, 'customer_number' => $customer_number, 'user_phone' => $user_phone, 'post_code' => $post_code ];
        $email_data['email_header_img'] = 'meter_header.jpg';
        $email_data['content'] = 'layout/book_meter_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Book meter');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 19;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Book meter email sent to: '.$recipient_email]);

    }


    function customer_address()
    {

      $customer_id = $this->input->post('customer_id');
      if(!isset($customer_id) || $customer_id=='' )
      {
          $this->jres->failure('customer_id required');
      }
      else
      {
          $BillingAddress = $this->user_modal->get_customer_billing_addresses( $customer_id );
          if( !empty($BillingAddress) )
          {
              for( $m=0; $m<count($BillingAddress); $m++ )
              {
                 if( empty($BillingAddress[$m]['address_line_1']) || $BillingAddress[$m]['address_line_1'] == null )
                 {
                    $BillingAddress[$m]['address_line_1'] = '';
                 }
                 if( empty($BillingAddress[$m]['address_line_2']) || $BillingAddress[$m]['address_line_2'] == null )
                 {
                    $BillingAddress[$m]['address_line_2'] = '';
                 }
                 if( empty($BillingAddress[$m]['address_line_3']) || $BillingAddress[$m]['address_line_3'] == null )
                 {
                    $BillingAddress[$m]['address_line_3'] = '';
                 }
                 if( empty($BillingAddress[$m]['address_line_4']) || $BillingAddress[$m]['address_line_4'] == null )
                 {
                    $BillingAddress[$m]['address_line_4'] = '';
                 }
                 if( empty($BillingAddress[$m]['postcode']) || $BillingAddress[$m]['postcode'] == null )
                 {
                    $BillingAddress[$m]['postcode'] = '';
                 }
              }

              $this->jres->success($BillingAddress);
          }
          else{
              $this->jres->failure('no row found for this customer id');
          }
      }


    }


function get_ese_share_link($customer_id){
      $ESECustomerShare = $this->mysql->select('*')
          ->from('referral_link')
          ->where('customer_id', $customer_id)
          ->get()->first_row('array');
      return $ESECustomerShare;
  }

  function insert_ese_share_link($data){
      $this->mysql->insert('referral_link',$data);
      return $this->mysql->insert_id();
  }


function referral()
{
  $id = $this->input->post('id');
  if(!isset($id) || $id=='' )
  {
      $this->jres->failure('user id required');
  }
  else
  {
      $login_response = $this->user_modal->get_account_detail( $id );

      $account_id = $login_response['account_id'] ;
      $name = $login_response['forename'] ;
      $lastname = $login_response['surname'] ;
      $email = $login_response['email'] ;
      //print_r($account_id);

      /*---CREATE--- creates new user */

      $random = rand(0,99);
      $random = ($random <=9 ? "0" . $random : $random);

      $para =  [
          "id" => $account_id,
          "accountId" => $account_id,
          "firstName" => $name,
          "lastName" => $lastname,
          "email" => $email,
          "referable" => "true",
          "referralCode" => $name . $lastname . $random,    // forename . surname . random number between 00 and 99
          "locale" => "en_UK"
      ];

      $creating_user = $this->curl->curlSaas('open/account/'.$account_id.'/user/'.$account_id.'', $para, 'post');
      $creating_user = json_decode($creating_user);
      //print_r($creating_user);


      /*---SEARCH--- search user via user and acccountId. displays share link info */

      $para =  [
          "id" => $account_id,
          "accountId" => $account_id
      ];



      $search_user = $this->curl->curlSaas('user', $para, 'post');
      $search_user = json_decode($search_user);
      //debug($search_user,1);
      if( isset($search_user->statusCode) )
      {
         $this->jres->failure($search_user->message);
      }
      else
      {

      // Find out if we already have share link or we need to generate one
      $ESEShareLink = $this->get_ese_share_link($id);

      if($ESEShareLink['ese_generated_link']!=''){
          $esesharelink = $ESEShareLink['ese_generated_link'];
      }
      else {

          $esesharelink = 'http://eversmart.family/'.$name . $lastname . $random;

          $db_parameter = [
              'customer_id' =>$id,
              'ese_generated_link' => $esesharelink,
              'saas_generated_link' => $search_user->shareLinks->shareLink
          ];

          $this->insert_ese_share_link($db_parameter);
      }


      $sharelink = $search_user->shareLinks->shareLink;
      $sharelink_fbook = $search_user->shareLinks->facebookShareLink;
      $sharelink_twitt = $search_user->shareLinks->twitterShareLink;
      $sharelink_email = $search_user->shareLinks->emailShareLink;
      $sharelink_mob = $search_user->shareLinks->mobileShareLink;
      $sharelink_fbook_mob = $search_user->shareLinks->mobileFacebookShareLink;
      $sharelink_twitt_mob = $search_user->shareLinks->mobileTwitterShareLink;
      $sharelink_email_mob = $search_user->shareLinks->mobileEmailShareLink;

      /*
      print_r($sharelink); echo '<br/>';
      print_r($sharelink_fbook); echo '<br/>';
      print_r($sharelink_twitt); echo '<br/>';
      print_r($sharelink_email); echo '<br/>';
      print_r($sharelink_mob); echo '<br/>';
      print_r($sharelink_fbook_mob); echo '<br/>';
      print_r($sharelink_twitt_mob); echo '<br/>';
      print_r($sharelink_email_mob); echo '<br/>';
      */


      /*----LIST REWARDS--- displays users current rewards using accountId */

      $list_reward = $this->curl->curlSaas('reward/balance?accountId='.$account_id.'');
      $list_reward = json_decode($list_reward);

      if($list_reward == null)
      {
          $format_bal = 0;
          $format_redeem_bal = 0;
      }
      else
      {
          $assigned_bal = $list_reward[0]->totalAssignedCredit;
          $redeemed_bal = $list_reward[0]->totalRedeemedCredit;
          $current_bal = $assigned_bal - $redeemed_bal;
          $format_bal = $current_bal / 100;
          //print_r($format_bal);
          $format_redeem_bal = $redeemed_bal / 100;
      }

      /*----NUM OF REFERRALS------ displays how many users a person has referred */

      $referrals = $this->curl->curlSaas('account/'.$account_id.'/user/'.$account_id.'/pii');
      $referrals = json_decode($referrals);

      $ref = $referrals->referrals->totalCount;
      //print_r($ref);


      /*----REWARDS EARNED------ displays the number of rewards a user has received */

      $rewards = $this->curl->curlSaas('account/'.$account_id.'/user/'.$account_id.'/pii');
      $rewards = json_decode($rewards);

      $rew = $rewards->rewards->totalCount;
      //print_r($rew);


      /*----REFERRED FRIENDS------ displays list of friends a user has referred */

      $friends_referred = $this->curl->curlSaas('open/referrals?referringAccountId='.$account_id.'&referringUserId='.$account_id.'');
      $friends_referred = json_decode($friends_referred);
      //print_r($friends_referred);

      $counter=0; $counters=0;
      if($friends_referred->referrals == null)
      {
        $rewards_earned = 0;
      }
      else
      {

          for ($i = 0; $i < count($friends_referred->referrals); $i++)
          {
              $ref_status = $friends_referred->referrals[$i]->referrerModerationStatus;
              $ref_reward =  $friends_referred->referrals[$i]->referrerModerationStatus;
              $count_referred = $friends_referred->referrals[$i]->referredReward;

              if($count_referred == null)
              {
                  $counters++;
              }
          }

          $rewards_earned = $ref - $counters;
      }
      /*----REWARDS PENDING ------ displays amount of pending rewards for user */

      $pending = $this->curl->curlSaas('open/referrals?referringAccountId='.$account_id.'&referringUserId='.$account_id.'');
      $pending = json_decode($pending);
      $pendingcounter=0; $pendingcounters=0;
      if($pending->referrals == null)
      {
         $pendingcounters = 0;
      }
      else
      {
          for ($i = 0; $i < count($pending->referrals); $i++)
          {
              $ref_statusp = $pending->referrals[$i]->referrerModerationStatus;
              $ref_rewardp =  $pending->referrals[$i]->referrerModerationStatus;
              $count_referredp = $pending->referrals[$i]->referredReward;

              if($count_referredp == null)
              {
                  $pendingcounters++;
              }
          }
      }
      //echo $pendingcounters



      //print_r($pending);
      $data['rewards_earned'] = $rewards_earned;
      $data['rewards_pending'] = $pendingcounters;
      $data['friend_referred'] = $ref;
      /*----------*/

      $data['format_bal'] = $format_bal;
      $data['format_redeem_bal'] = $format_redeem_bal;
      $data['esesharelink'] = $esesharelink;
      $data['sharelink'] = $sharelink;
      $data['sharelink_fbook'] = $sharelink_fbook;
      $data['sharelink_twitt'] = $sharelink_twitt;
      $data['sharelink_email'] = $sharelink_email;
      $data['sharelink_mob'] = $sharelink_mob;
      $data['sharelink_fbook_mob'] = $sharelink_fbook_mob;
      $data['sharelink_twitt_mob'] = $sharelink_twitt_mob;
      $data['sharelink_email_mob'] = $sharelink_email_mob;
      $data['ref'] = $ref;  //number of friends referred
      $data['friends_referred'] = $friends_referred;
      $data['rew'] = $rew;  //number of rewards earned
      $data['pending'] = $pending;  //number of rewards pending
      $this->jres->success($data);
    }
  }
}


} //class ends

 ?>
