<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08/01/2019
 * Time: 14:58
 */

class Dashboard extends ESE_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('saas_model');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_redfish');
        $this->load->model('curl/curl_requests_dyball');
        $this->load->model('curl/curl_requests_saasquatch');
        $this->mysql = $this->load->database('mysql', true);
    }


    /**
     * Get the snippets of info for dashboard
     *
     * @params null
     * @returns string json array
     */
    function index()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $data = array();

        if (in_array('junifer', $this->customer_type)) {
            $data = $this->get_junifer_dashboard_info();
        }

        if (in_array('dyball', $this->customer_type)) {
            $data = $this->get_dyball_dashboard_info();
        }

        if (in_array('redfish', $this->customer_type)) {
            $data = $this->get_redfish_dashboard_info();
        }

        if(!empty($data)) {
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        }
        else {
            echo json_encode(['success' => false, 'error' => 'No information found.']);
        }
    }


    /**
     * Get junifer customer data
     *
     * @params null
     * @returns array php
     */
    function get_junifer_dashboard_info()
    {

        $from_date = date('Y-m-d', strtotime("-1 months"));
        $to_date = date('Y-m-d');

        // My Account
        $TariffData = $this->user_modal->get_tariff_details($this->account_id->junifer);
        $CustomerData = $this->user_modal->get_basic_customer_info($this->account_id->junifer);

        $data['my_account'] = [
            'account_id' => $this->account_id->junifer,
            'tariff' => ucwords(strtolower(trim($TariffData['tariff_tariffName'])))
        ];

        // Billing
        $BillingData = $this->curl_requests_junifer->junifer_get_bills($this->account_id->junifer, 'array');
        $LastBill = $BillingData[0];
        $data['billing'] = [
            'last_bill_date' => $LastBill['issue_date'] ? $LastBill['issue_date'] : 'N/A',
            'last_bill_amount' => $LastBill['gross_amount'] ? $LastBill['gross_amount'] : 'N/A'
        ];

        // Payment
        $PaymentData = $this->curl_requests_junifer->junifer_get_balance($this->account_id->junifer, 'array');
        $InterestData = $this->user_modal->calculate_total_customer_interest($this->account_id->junifer);
        $junifer_payments = $this->curl_requests_junifer->junifer_payment_details($this->account_id->junifer);

        $data['payment'] = [
            'balance' => $PaymentData['balance'],
            'created_date' => $PaymentData['date'],
            'interest_total' => $InterestData['accrued_interest']?$InterestData['accrued_interest']:'N/A',
            'next_bill_date' => isset($junifer_payments['results']) && !empty($junifer_payments['results'])?$junifer_payments['results'][0]['nextPaymentDt']:''
        ];

        // Messages
        $MessageData = $this->curl_requests_junifer->junifer_get_messages($this->account_id->junifer);
        $LastMessage = end($MessageData['results']);
        $data['messages'] = [
            'total' => count($MessageData['results']),
            'last_messages' => $LastMessage['type']
        ];

        // Energy Usage & Meter Reading
        $MeterInfo = $this->user_modal->get_meterread_details($this->account_id->junifer);

        isset($MeterInfo['quotation_aq']) && $MeterInfo['quotation_aq'] > 0 ? $energy = 'dual' : $energy = 'elec';  // sets the $energy variable depending on if AQ is set in db

        $MeterReads = $this->curl_requests_junifer->junifer_get_meterreads($this->account_id->junifer, $from_date, $to_date, $energy);

        if (!empty($MeterReads['elec_readings']['results'])) {
            $elec = array_slice(array_reverse($MeterReads['elec_readings']['results']), 0, 2);
            $data['meter_reading']['elec_readings'] = $elec[0]['cumulative'];
        } else {
            $data['meter_reading']['elec_readings'] = 'N/A';
        }

        if (!empty($MeterReads['gas_readings']['results'])) {
            $gas = array_slice(array_reverse($MeterReads['gas_readings']['results']), 0, 2);
            $data['meter_reading']['gas_readings'] = $gas[0]['cumulative'];
        } else {
            $data['meter_reading']['gas_readings'] = 'N/A';
        }

        // Friend Referral
        $saas_data = $this->curl_requests_saasquatch->saasquatch_get_user($CustomerData['id']);
        $ese_link = $this->saas_model->get_ese_share_link($this->account_id->junifer);

        $data['friend_referral'] = [
            'ref' => $saas_data['referrals'],
            'format_bal' => $saas_data['currentBal'],
            'redeemed_bal' => $saas_data['redeemedBal'],
            'ese_saas_link' => $ese_link['ese_generated_link']
        ];

        // Moving Home
        $data['moving_home'] = [
            'postcode' => $CustomerData['address_postcode'],
            'first_line_address' => ucwords(strtolower(trim($CustomerData['first_line_address'])))
        ];

        return $data;
    }


    /**
     * Get dyball customer data
     *
     * @params null
     * @returns array php
     */
    function get_dyball_dashboard_info()
    {

        // My Account
        $CustomerData = $this->user_modal->get_basic_customer_info(NULL, $this->account_id->dyball);
        $TariffData = $this->user_modal->get_tariff_details($this->account_id->dyball);
        $data['my_account'] = [
            'account_id' => $this->account_id->dyball,
            'tariff' => $TariffData['tariff_tariffName'],
        ];

        // Payment
        /*$dyb_data =[
            "AccountNumber" => $this->account_id->dyball,
            "AuthKey"=> "1234",
            "StartDate"=> date('Y-m-d',strtotime("-3 month")),
            "EndDate"=> date('Y-m-d')
        ];
        $Payments = $this->curl_requests_dyball->dyball_payments($dyb_data);
        $data['payment'] = [
            'balance' => 'N/A',
            'created_date' => 'N/A'
        ];*/

        // Moving Home
        $data['moving_home'] = [
            'postcode' => $CustomerData['address_postcode'],
            'first_line_address' => ucwords(strtolower(trim($CustomerData['first_line_address'])))
        ];

        return $data;
    }


    /**
     * Get redfish customer data
     *
     * @params null
     * @returns array php
     */
    function get_redfish_dashboard_info()
    {

        // My Account
        $CustomerData = $this->curl_requests_redfish->redfish_my_account($this->account_id->redfish);
        $response = $this->parseHttpResponse($CustomerData);
        $response = $response['body']['Data'];

        //echo '<pre>' . print_r($response,1) . '</pre>';


        if(!empty($response)) {

            $data['my_account'] = [
                'account_id' => $this->account_id->redfish,
                'tariff' => ucwords(strtolower(trim($response['Tariff']['Name'])))
            ];

        }

        // Billing
        /*$BillingData = $this->curl_requests_junifer->junifer_get_bills($this->account_id->junifer, 'array');
        $LastBill = end($BillingData);
        $data['billing'] = [
            'last_bill_date' => $LastBill['invoice_date'] ? $LastBill['invoice_date'] : 'N/A',
            'last_bill_amount' => $LastBill['gross_amount'] ? $LastBill['gross_amount'] : 'N/A'
        ];

        // Payment
        $PaymentData = $this->curl_requests_junifer->junifer_get_balance($this->account_id->junifer, 'array');
        $data['payment'] = [
            'balance' => $PaymentData['balance'],
            'created_date' => $PaymentData['date']
        ];

        // Energy Usage & Meter Reading
        $from_date = date('Y-m-d', strtotime("-1 months"));
        $to_date = date('Y-m-d');

        $MeterInfo = $this->user_modal->get_meterread_details($this->account_id->junifer);

        isset($MeterInfo['quotation_aq']) && $MeterInfo['quotation_aq'] > 0 ? $energy = 'dual' : $energy = 'elec';  // sets the $energy variable depending on if AQ is set in db

        $ElecMeterReads = $this->curl_requests_redfish->redfish_customer_meter_reading($this->account_id->redfish, 'mpan', $response['Sites'][0]['ElectricMeters'][0]['Mpan']);

        if (!empty($MeterReads['elec']['results'])) {
            $elec = array_slice(array_reverse($MeterReads['elec']['results']), 0, 2);
            $data['meter_reading']['elec_readings'] = $elec[0];
            $data['usage']['daily_use_elec_total'] = $elec[0]['consumption'] + $elec[0]['consumption'];
        } else {
            $data['meter_reading']['elec_readings'] = 'N/A';
            $data['usage']['daily_use_elec_total'] = 0;
        }

        if (!empty($MeterReads['gas']['results'])) {
            $gas = array_slice(array_reverse($MeterReads['gas']['results']), 0, 2);

            $data['meter_reading']['gas_readings'] = $gas[0];
            $data['usage']['daily_use_gas_total'] = $gas[0]['consumption'] + $gas[0]['consumption'];
        } else {
            $data['meter_reading']['gas_reading'] = 'N/A';
            $data['usage']['daily_use_gas_total'] = 0;
        }*/

        // Messages
        $data['messages'] = [
            'total' => 'N/A',
            'last_messages' => 'N/A'
        ];


        // Moving Home
        $address = explode(',', $response['Address']);
        $data['moving_home'] = [
            'postcode' => $address[array_key_last($address)],
            'first_line_address' => ucwords(strtolower(trim($address[0])))
        ];

        return $data;
    }
}
