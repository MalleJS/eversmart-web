<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpfaqs extends ESE_Controller {

  function index()
  {
		$data['scripts'] = [base_url()."assets/js/new-world/help/faqs-page.js"];
		$data['stylesheets'] = ['assets/css/new-world/help/faqs-page.css'];
		$data['content'] = 'new-world/help/faqs-page';
		$this->load->view( 'new-world/master', $data );
  }

}


