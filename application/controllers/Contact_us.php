<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends ESE_Controller  {


    function index()
    {
		$data['stylesheets'] = ["assets/css/new-world/help/contact-page.css"];
		$data['scripts'] = [];
		$data['content'] = 'new-world/help/contact-page';
        $this->load->view( 'new-world/master', $data );
    }


    function email()
    {

        $info['detail'] = array(
            'first_name'=>$this->input->post('fname'),
            'Email'=>$this->input->post('email'),
            'Phone'=>$this->input->post('phone'),
            'Message'=>$this->input->post('message')
        );

        $token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $this->session->userdata('login_data')['name']];
        $template = $this->load->view( 'layout/contact_template', $info , true);
        $this->load->library('email');
        $this->email->from($this->input->post('email'), ucfirst($this->input->post('fname')));
        $this->email->to('hello@eversmartenergy.co.uk');
        $this->email->subject('Thank You Message');
        $this->email->message($template);
        $this->email->send();

    }

			  
}

