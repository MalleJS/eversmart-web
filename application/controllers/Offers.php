<?php

/**
 * Class Offers
 */
class Offers extends ESE_Controller
{
    public function index()
    {
        $this->load->helper('url');
        redirect('/?offers_url=' . Date('M-20y'));
    }
}
