<?php

/**
 * Class Meter_reading
 */
class Meter_reading extends ESE_Controller
{
    /**
     * Meter_reading constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_redfish');
        $this->load->model('curl/curl_requests_energylinx');
        $this->load->model('user_modal');
    }

    /**
     * Retrieves all customer meter reads from Junifer
     *
     * @returns string
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $junifer_data = array();
        $redfish_data = array();

        if(in_array('junifer',$this->customer_type) ) {

            $junifer_data = $this->get_junifer_meterreads();
        }

        if(in_array('redfish',$this->customer_type) ) {

            $redfish_data = $this->load_redfish_meterreads('mpan');
        }

        // Merge arrays - this will work if one or both are empty
        $data = array_merge($junifer_data,$redfish_data);

        // Check its not an empty array
        if (!empty($data)) {

            // return success and data
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        }
        else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => 'No customer information found']);
        }
    }


    /**
     * Retrieves Junifer meter reads
     *
     * @return array
     * @throws Exception
     */
    function get_junifer_meterreads()
    {
        $MeterInfo = $this->user_modal->get_meterread_details($this->account_id->junifer);

        $date = $MeterInfo['create_at'];    // gets date that record was added to db
        $dt = new DateTime($date);
        $from_date = $dt->format('Y-m-d');  // converts created date to Ymd format
        $to_date = date('Y-m-d');   // gets todays date
        isset($MeterInfo['quotation_aq']) && $MeterInfo['quotation_aq'] > 0 ? $energy = 'dual' : $energy = 'elec';  // sets the $energy variable depending on if AQ is set in db

        // If we cant find elec meter serial check junifer
        if ($MeterInfo['elec_meter_serial'] == '' || $MeterInfo['elec_meter_serial'] == '0'){

            // Go to Junifer
            $elec_meter_serial = $this->curl_requests_junifer->junifer_elec_meter_serial($MeterInfo['address_postcode'], $MeterInfo['elec_mpan_core']);

            // Next time we don't want to have to go off to Energylinx, so save it in the database
            if ($elec_meter_serial) {

                $data = ['elec_meter_serial' => $elec_meter_serial];
                $this->user_modal->update_customer_info_by_account_id($data, $this->account_id->junifer);

                $MeterInfo['elec_meter_serial'] = $elec_meter_serial;
            }
        }

        // If we cant find gas meter serial check junifer
        if ($MeterInfo['gas_meter_serial'] == '' || $MeterInfo['gas_meter_serial'] == '0'){

            // Go to Junifer
            $gas_meter_serial = $this->curl_requests_junifer->junifer_gas_meter_serial($MeterInfo['address_postcode'], $MeterInfo['gasProduct_mprns']);

            // Next time we don't want to have to go off to Energylinx, so save it in the database
            if ($gas_meter_serial) {

                $data = ['gas_meter_serial' => $gas_meter_serial];
                $this->user_modal->update_customer_info_by_account_id($data, $this->account_id->junifer);

                $MeterInfo['gas_meter_serial'] = $gas_meter_serial;
            }
        }

        $meter_reads = $this->curl_requests_junifer->junifer_get_meterreads($this->account_id->junifer, $from_date, $to_date, $energy);

        if (!empty($meter_reads['elec_readings']['results'])) {
            $elec = array_slice(array_reverse($meter_reads['elec_readings']['results']), 0, 5);
            $ElecResults['elec_readings'] = $elec;
        } else {
            $ElecResults['elec_readings'] = [];
        }

        $meterpoint_elec_id = $meter_reads['meterpoint_elec'];
        isset($meter_reads['meter_identifier_elec']) ? $meter_identifier_elec = $meter_reads['meter_identifier_elec'] : $meter_identifier_elec = '';
        isset($meter_reads['meter_digits_elec']) ? $meter_digits_elec = $meter_reads['meter_digits_elec'] : $meter_digits_elec = '';

        $ElecResults['meterpoint_elec'] = $meterpoint_elec_id;
        $ElecResults['meter_identifier_elec'] = $meter_identifier_elec;
        $ElecResults['meter_digits_elec'] = $meter_digits_elec;

        if (isset($meter_reads['gas_readings']['results'])) {
            $gas = array_slice(array_reverse($meter_reads['gas_readings']['results']), 0, 5);
            $GasResults['gas_readings'] = $gas;
        } else {
            $GasResults['gas_readings'] = [];
        }

        $meterpoint_gas_id = $meter_reads['meterpoint_gas'];
        isset($meter_reads['meter_identifier_gas']) ? $meter_identifier_gas = $meter_reads['meter_identifier_gas'] : $meter_identifier_gas = '';
        isset($meter_reads['meter_digits_gas']) ? $meter_digits_gas = $meter_reads['meter_digits_gas'] : $meter_digits_gas = '';

        $GasResults['meterpoint_gas'] = $meterpoint_gas_id;
        $GasResults['meter_identifier_gas'] = $meter_identifier_gas;
        $GasResults['meter_digits_gas'] = $meter_digits_gas;

        return array_merge(array_merge($ElecResults, $GasResults), $MeterInfo);
    }


    /**
     * Retrieve Redfish meter reads
     *
     * @return array
     */
    function load_redfish_meterreads($energy)
    {
        $account = $this->curl_requests_redfish->redfish_my_account($this->account_id->redfish);
        $response = $this->parseHttpResponse($account);

        $data = array();

        if($response['body']['Message'] == 'SUCCESS') {

            // If we have a token returned in the header
            if (isset($response['headers']['Set-Cookie'])) {

                // Turn header into something usable
                $cookie = $this->parseCookieHeader($response['headers']['Set-Cookie']);

                // Assign to the public variable
                $this->redfish_token = $cookie['account'];

                // Refresh JWT token with latest Redfish token
                $refresh = json_decode($this->refresh_token(),true);
                $_SERVER['HTTP_X_CUSTOMER_TOKEN'] = $refresh['token'];
            }

            $data['account_number'] = $response['body']['Data']['AccountNumber'];

            if (!empty($response['body']['Data']['Sites'])) {
                $energy == 'mpan' ? $meterpoint = $response['body']['Data']['Sites'][0]['ElectricMeters'][0]['Mpan'] : $meterpoint = $response['body']['Data']['Sites'][0]['GasMeters'][0]['Mprn'];
            }

            $reading = $this->curl_requests_redfish->redfish_customer_meter_reading($this->account_id->redfish, $energy, $meterpoint);
            $reading = $this->parseHttpResponse($reading);

            if( !empty($reading['body']['Meters'][0]['Registers'][0]['Readings'] )) {
                $energy == 'mpan' ? $fuel = 'elec' : $fuel = 'gas';
                $data[$fuel.'_readings'] = $reading['body']['Meters'][0]['Registers'][0]['Readings'];
                /*$last_read = end($data[$fuel.'_readings']);
                $data['timestamp'] = strtotime(str_replace('/','-', $last_read['ReadingDate']));*/

                unset($data['account_number']);
                return $data;
            } else {
                $data = array();
                return $data;
            }
        } else {
            return $data;
        }
    }

    /**
     * Lookup supply status history in Junifer and return latest one
     *
     * return array
     */
    function junifer_get_current_supply_status()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        // Do we have MTDs?
        $meter_details = $this->user_modal->get_meterread_details($this->account_id->junifer);
        $agreements = $this->curl_requests_junifer->junifer_agreements($this->account_id->junifer);

        $elec_data = array();
        $gas_data = array();

        // If elecMeterPointID is not set, go check junifer
        if (($meter_details['elecMeterPointID'] == '' || $meter_details['elecMeterPointID'] == 0)
            && isset($agreements['results'][0]['products'][0]['assets'][0]['id'])
            && $agreements['results'][0]['products'][0]['assets'][0]['id'] > 0
        ) {
            $data = ['elecMeterPointID' => $agreements['results'][0]['products'][0]['assets'][0]['id']];
            $this->user_modal->update_customer_info_by_account_id($data, $this->account_id->junifer);
            $meter_details['elecMeterPointID'] = $agreements['results'][0]['products'][0]['assets'][0]['id'];
        }

        // If gasMeterPointID is not set, go check junifer
        if (($meter_details['gasMeterPointID'] == '' || $meter_details['gasMeterPointID'] == 0)
            && isset($agreements['results'][1]['products'][0]['assets'][0]['id'])
            && $agreements['results'][1]['products'][0]['assets'][0]['id'] > 0
        ) {
            $data = ['gasMeterPointID' => $agreements['results'][1]['products'][0]['assets'][0]['id']];
            $this->user_modal->update_customer_info_by_account_id($data, $this->account_id->junifer);
            $meter_details['gasMeterPointID'] = $agreements['results'][1]['products'][0]['assets'][0]['id'];
        }


        // If we have the elec meter point id, go look for the current status
        if ($meter_details['elecMeterPointID'] > 0) {

            $elec_supply_status = $this->curl_requests_junifer->junifer_supply_status($meter_details['elecMeterPointID']);
            $elec_ssd = $this->curl_requests_junifer->junifer_supply_start_date($meter_details['elecMeterPointID']);

            if($elec_supply_status['history'][0]['supplyStatus']) {

                $elec_supply_status = preg_split('/(?=[A-Z])/',$elec_supply_status['history'][0]['supplyStatus']);
                $elec_supply_status = trim(implode(" ",$elec_supply_status));

                $elec_data = [
                    'elec_proposed_start_date' => $agreements['results'][0]['fromDt']?$agreements['results'][0]['fromDt']:'N/A',
                    'elec_status' => $elec_supply_status,
                    'elec_ssd' => $elec_ssd?$elec_ssd:'N/A',
                    'elec_meter_point_id' => $meter_details['elecMeterPointID']
                ];
            }
        }

        // If we have the gas meter point id, go look for the current status
        if ($meter_details['gasMeterPointID'] > 0) {

            $gas_supply_status = $this->curl_requests_junifer->junifer_supply_status($meter_details['gasMeterPointID']);
            $gas_ssd = $this->curl_requests_junifer->junifer_supply_start_date($meter_details['gasMeterPointID']);

            if($gas_supply_status['history'][0]['supplyStatus']) {

                $gas_supply_status = preg_split('/(?=[A-Z])/',$gas_supply_status['history'][0]['supplyStatus']);
                $gas_supply_status = implode(" ",$gas_supply_status);

                $gas_data = [
                    'gas_proposed_start_date' => $agreements['results'][1]['fromDt']?$agreements['results'][1]['fromDt']:'N/A',
                    'gas_status' => trim($gas_supply_status),
                    'gas_ssd' => $gas_ssd?$gas_ssd:'N/A',
                    'gas_meter_point_id' => $meter_details['gasMeterPointID']
                ];
            }

        }

        $data = array_merge($elec_data, $gas_data);

        if(!empty($data)) {
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        }
        else {
            echo json_encode(['success' => false, 'error' => 'No information found.', 'token' => $this->get_latest_token() ]);
        }

    }

    /**
     * Submits electricity reading to associated meterpoint ID in Junifer
     *
     * return string
     */
    function submit_elec_reading()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        // Get data from posted json
        $data = json_decode(file_get_contents('php://input'), true);

        if(in_array('junifer',$this->customer_type) ) {

            // Do we have MTDs?
            $meter_details = $this->user_modal->get_meterread_details($this->account_id->junifer);

            // If we have the elec meter point id, go look for the current status
            if ($meter_details['elecMeterPointID'] > 0) {

                $current_status = $this->curl_requests_junifer->junifer_supply_status($meter_details['elecMeterPointID']);
                $current_supply_status = $current_status['history'][0]['supplyStatus'];
            }

            // If there is a current elec status
            if (isset($current_supply_status) && $current_supply_status != '') {

                // Set up status arrays to search through - RegistrationWithdrawRejected, LossNotified, LossObjected, LossConfirmed
                $error_responses = [
                    'RegistrationObjectionUpheld',
                    'RegistrationObjected',
                    'RegistrationRejected',
                    'RegistrationWithdrawing',
                    'NotSupplied'
                ];

                $post_mtd = [
                    'RegistrationCompleted',
                    'Registered',
                    'RegistrationConfirmed'
                ];

                $pre_mtd = [
                    'RegistrationRequested',
                    'RegistrationAccepted'
                ];

                // If status is one of the error triggers, trigger error
                if (in_array($current_supply_status, $error_responses)) {

                    $current_supply_status = preg_split('/(?=[A-Z])/', $current_supply_status);
                    echo json_encode(['success' => false, 'error' => 'Unable to submit meter reading because your switch status is currently at "' . trim(implode(" ", $current_supply_status)) . '"', 'token' => $this->get_latest_token() ]);
                    exit;

                } // If we have MTDs, submit using standard API call
                else if (in_array($current_supply_status, $post_mtd)) {

                    // Data required in API call
                    $read_data = [
                        'meter_identifier' => $meter_details['elec_meter_serial'],
                        'reading' => $data['reading'],
                        'meterpoint_id' => $meter_details['elecMeterPointID']
                    ];
                    #
                    $submit_with_mtds = $this->curl_requests_junifer->junifer_submit_readings($read_data);
                    $response = json_decode($submit_with_mtds, true);

                } // If we don't have MTD's yet, send data using pre-MTD API call
                else if (in_array($current_supply_status, $pre_mtd)) {

                    // Data required in API call
                    $read_data = [
                        'reading' => $data['reading'],
                        'meterpoint_id' => $meter_details['elecMeterPointID']
                    ];

                    $submit_without_mtds = $this->curl_requests_junifer->junifer_submit_readings_without_mtds($read_data);
                    $response = json_decode($submit_without_mtds, true);
                }


                if (isset($response)) { // READ THIS: No response means a successful API call

                    $error = isset($response['errorSeverity']) ? $response['errorSeverity'] : '';
                    $validation_error = isset($response['validationErrors'][0]['errorDetail']['errorSeverity']) ? $response['validationErrors'][0]['errorDetail']['errorSeverity'] : '';
                    $validation_description = isset($response['validationErrors'][0]['errorDetail']['errorDescription']) ? $response['validationErrors'][0]['errorDetail']['errorDescription'] : '';

                    if (isset($error) && ($error == 'Error' || $error == 'Warning')) {
                        echo json_encode(['success' => false, 'error' => 'Unable to submit electricity meter reading - junifer error.', 'token' => $this->get_latest_token()]);
                    } else if (isset($validation_error) && ($validation_error == 'Error' || $validation_error == 'Warning')) {
                        echo json_encode(['success' => false, 'error' => $validation_description, 'token' => $this->get_latest_token()]);
                    } else {
                        echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token()]);
                    }
                }
                else {
                    echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token()]);
                }

            }

        }
        else if (in_array('redfish',$this->customer_type) ) {

            $read_data = [
                'fuel_type' => 'electricity',
                'reading' => $data['reading']
            ];

            $this->submit_redfish_meter_reading($read_data);
        }
        else {

            echo json_encode(['success' => false, 'error' => 'Unable to submit electricity meter reading.', 'token' => $this->get_latest_token()]);
        }
    }


    /**
     * Submits gas reading to associated meterpoint ID in Junifer
     *
     * @return string
     */
    function submit_gas_reading()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        // Get data from posted json
        $data = json_decode(file_get_contents('php://input'), true);

        if (in_array('junifer',$this->customer_type) ) {

            // Do we have MTDs?
            $meter_details = $this->user_modal->get_meterread_details($this->account_id->junifer);

            // If we have the elec meter point id, go look for the current status
            if ($meter_details['gasMeterPointID'] > 0) {

                $current_status = $this->curl_requests_junifer->junifer_supply_status($meter_details['gasMeterPointID']);
                $current_supply_status = $current_status['history'][0]['supplyStatus'];
            }

            // If there is a current gas status
            if (isset($current_supply_status) && $current_supply_status != '') {

                // Set up status arrays to search through - RegistrationWithdrawRejected, LossNotified, LossObjected, LossConfirmed
                $error_responses = [
                    'RegistrationObjectionUpheld',
                    'RegistrationObjected',
                    'RegistrationRejected',
                    'RegistrationWithdrawing',
                    'NotSupplied'
                ];

                $post_mtd = [
                    'RegistrationCompleted',
                    'Registered',
                    'RegistrationConfirmed'
                ];

                $pre_mtd = [
                    'RegistrationRequested',
                    'RegistrationAccepted'
                ];

                // If status is one of the error triggers, trigger error
                if (in_array($current_supply_status, $error_responses)) {

                    $current_supply_status = preg_split('/(?=[A-Z])/', $current_supply_status);
                    echo json_encode(['success' => false, 'error' => 'Unable to submit meter reading because your switch status is currently at "' . trim(implode(" ", $current_supply_status)) . '"', 'token' => $this->get_latest_token() ]);
                    exit;

                } // If we have MTDs, submit using standard API call
                else if (in_array($current_supply_status, $post_mtd)) {

                    // Data required in API call
                    $read_data = [
                        'meter_identifier' => $meter_details['gas_meter_serial'],
                        'reading' => $data['reading'],
                        'meterpoint_id' => $meter_details['gasMeterPointID']
                    ];

                    $submit_with_mtds = $this->curl_requests_junifer->junifer_submit_readings($read_data);
                    $response = json_decode($submit_with_mtds, true);

                } // If we don't have MTD's yet, send data using pre-MTD API call
                else if (in_array($current_supply_status, $pre_mtd)) {

                    // Data required in API call
                    $read_data = [
                        'reading' => $data['reading'],
                        'meterpoint_id' => $meter_details['gasMeterPointID']
                    ];

                    $submit_without_mtds = $this->curl_requests_junifer->junifer_submit_readings_without_mtds($read_data);
                    $response = json_decode($submit_without_mtds, true);
                }

                if (isset($response)) { // READ THIS: No response means a successful API call

                    $error = isset($response['errorSeverity']) ? $response['errorSeverity'] : '';
                    $validation_error = isset($response['validationErrors'][0]['errorDetail']['errorSeverity']) ? $response['validationErrors'][0]['errorDetail']['errorSeverity'] : '';
                    $validation_description = isset($response['validationErrors'][0]['errorDetail']['errorDescription']) ? $response['validationErrors'][0]['errorDetail']['errorDescription'] : '';

                    if (isset($error) && ($error == 'Error' || $error == 'Warning')) {
                        echo json_encode(['success' => false, 'error' => 'Unable to submit gas meter reading.', 'token' => $this->get_latest_token()]);
                    } else if (isset($validation_error) && ($validation_error == 'Error' || $validation_error == 'Warning')) {
                        echo json_encode(['success' => false, 'error' => $validation_description, 'token' => $this->get_latest_token()]);
                    } else {
                        echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token()]);
                    }
                }
                else {
                    echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token()]);
                }


            }

        }
        else if (in_array('redfish',$this->customer_type) ) {

            $read_data = [
                'fuel_type' => 'gas',
                'reading' => $data['reading']
            ];

            $this->submit_redfish_meter_reading($read_data);
        }
        else {

            echo json_encode(['success' => false, 'error' => 'Unable to submit gas meter reading.', 'token' => $this->get_latest_token()]);
        }
    }


    /**
     * Submits meter reading to redfish
     *
     * @return string
     */
    function submit_redfish_meter_reading($read_data){

        // Get the account data to use in the meter read call
        $account = $this->curl_requests_redfish->redfish_my_account($this->account_id->redfish);
        $response = $this->parseHttpResponse($account);

        // If the call was successful
        if($response['body']['Message'] == 'SUCCESS') {

            // If we have a token returned in the header
            if (isset($response['headers']['Set-Cookie'])) {

                // Turn header into something usable
                $cookie = $this->parseCookieHeader($response['headers']['Set-Cookie']);

                // Assign to the public variable
                $this->redfish_token = $cookie['account'];

                // Refresh JWT token with latest Redfish token
                $refresh = json_decode($this->refresh_token(),true);
                $_SERVER['HTTP_X_CUSTOMER_TOKEN'] = $refresh['token'];
            }


            // Now lets set up the post fields for meter read curl
            if($read_data['fuel_type'] == 'electricity') {

                $elec_response = $response['body']['Data']['Sites'][0]['ElectricMeters'][0];
                $serial = $elec_response['Meters'][0]['SerialNumber'];
                $register = end($elec_response['Meters'][0]['Registers']);

                $mpan = $elec_response['Mpan'];
                $mprn = null;

            } else {

                $gas_response = $response['body']['Data']['Sites'][0]['GasMeters'][0];
                $serial = $gas_response['Meters'][0]['SerialNumber'];
                $register = end($gas_response['Meters'][0]['Registers']);

                $mpan = null;
                $mprn = $gas_response['Mprn'];
            }


            // Put them in a nice little array
            $data = [
                'Reading' => $read_data['reading'],
                'Register' => $register,
                'SerialNumber' => $serial
            ];

            // Send the data
            $curl_response = $this->curl_requests_redfish->redfish_meter_reading($this->account_id->redfish, $data, $mpan, $mprn);
            $meter_response = $this->parseHttpResponse($curl_response);

            // If we have a token returned in the header
            if (isset($meter_response['headers']['Set-Cookie'])) {

                // Turn header into something usable
                $mr_cookie = $this->parseCookieHeader($meter_response['headers']['Set-Cookie']);

                // Assign to the public variable
                $this->redfish_token = $mr_cookie['account'];

                // Refresh JWT token with latest Redfish token
                $refresh = json_decode($this->refresh_token(),true);
                $_SERVER['HTTP_X_CUSTOMER_TOKEN'] = $refresh['token'];
            }

            // Did it work
            if( $meter_response['body']['Message'] == 'SUCCESS' ) {
                echo json_encode([ 'success' => true, 'error' => false, 'data'=>ucfirst($read_data['fuel_type']).' meter reading saved', 'token' => $this->get_latest_token() ]);
            } else {
                echo json_encode([ 'success' => false, 'error' => ucfirst($read_data['fuel_type']).' meter reading failed to save', 'token' => $this->get_latest_token() ]);
            }
        }
        else {
            echo json_encode([  'success' => false, 'error' => ucfirst($read_data['fuel_type']).' meter reading failed to save' , 'token' => $this->get_latest_token() ]);
        }
    }


    /**
     * Deduce meter type from information we have
     *
     * @param $elec_meter_serial
     * @param $mpan
     * @param $gas_meter_serial
     * @param $mprn
     * @param $postcode
     * @param $first_line_address
     * @return array
     */
    function customer_meter_type()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        if(in_array('junifer',$this->customer_type) ) {
            $id = $this->account_id->junifer;
        }
        else if (in_array('dyball',$this->customer_type)) {
            $id = $this->account_id->dyball;
        }

        $meter_details = $this->user_modal->get_meterread_details($id);

        // If we have the elec meter serial saved in the database use this to determine if smart meter
        if($meter_details['elec_meter_serial'] && $meter_details['elec_meter_serial'] != '0' && $meter_details['elec_meter_serial'] != ''){

            $elec_meter_serial = $meter_details['elec_meter_serial'];

        } else {

            // Go to Energylinx
            $elec_meter_serial = $this->curl_requests_energylinx->energylinx_get_meter_serial($meter_details['address_postcode'], $meter_details['elec_mpan_core'], null);

            // Next time we don't want to have to go off to Energylinx, so save it in the database
            if($elec_meter_serial){
                $data = ['elec_meter_serial' => $elec_meter_serial];

                $this->user_modal->update_customer_info_by_account_id($data,$id);

            }
        }


        // Dual fuel customer?
        if($meter_details['gasProduct_mprns'] && $meter_details['gasProduct_mprns'] != '0' && $meter_details['gasProduct_mprns'] != '') {

            // If we have the gas meter serial saved in the database use this to determine if smart meter
            if ($meter_details['gas_meter_serial'] && $meter_details['gas_meter_serial'] != 0 && $meter_details['gas_meter_serial'] != '') {

                $gas_meter_serial = $meter_details['gas_meter_serial'];

            } else {

                // Go to Energylinx
                $gas_meter_serial = $this->curl_requests_energylinx->energylinx_get_meter_serial($meter_details['address_postcode'], null, $meter_details['gasProduct_mprns']);

                // Next time we don't want to have to go off to Energylinx, so save it in the database
                if ($gas_meter_serial) {

                    $data = ['gas_meter_serial' => $gas_meter_serial];
                    $this->user_modal->update_customer_info_by_account_id($data, $id);

                }
            }
        }

        // Regex to find smart meters
        $pattern = '/^S1.*|^G4P.*|^NSS.*|^(1[3-9])P.*$/';

        $data = [
            'elec_smart' => isset($elec_meter_serial) && (preg_match( $pattern, $elec_meter_serial ) ) ? true : false,
            'gas_smart' => isset($gas_meter_serial) && (preg_match( $pattern, $gas_meter_serial ) ) ? true : false
        ];

        echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token()]);


    }


}
