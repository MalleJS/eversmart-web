<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends ESE_Controller
{

    private $int_customer_id, $str_customer_token;

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('error_modal');
        $this->load->model('api_modal');
        $this->load->model('saas_model');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_saasquatch');
        $this->load->model('curl/curl_requests_redfish');

        $this->load->helper('eversmart_helper');

        $this->mysql = $this->load->database('mysql', true);
        $this->load->library('session');
    }

    function monthyear()
    {

        $monthyear = $this->input->get('monthyearinput');

        $this->session->set_userdata('monthyear', $monthyear);

        echo json_encode(['monthyear' => $monthyear]);

    }

    function yearlypayment()
    {

        $data['id'] = $this->session->userdata('id');
        $data['email'] = $this->session->userdata('email');
        $data['year_price'] = $this->session->userdata('year_price');
        $data['address'] = $this->session->userdata('address');
        $data['postcode'] = $this->session->userdata('postcode');
        $data['junifer_id'] = $this->session->userdata('junifer_id');

        $data['content'] = 'energy/yearlypayment';
        $this->load->view('energy/yearlypayments',$data);

    }

    function lookup_yearly_tariff_data($dist_id, $rate_type)
    {

        // Look up
        if($rate_type=='STD') { $rate_type = '1R'; }
        return $this->mysql->select('*')->from('new_yearly_tariff')->where('ratetype',$rate_type)->where('duosid',$dist_id)->get()->first_row('array');

    }

    function getYearlyTarriff()
    {
        $data = json_decode( file_get_contents( 'php://input' ), true );
        $dist_parameter['postcode'] = $data['postcode'];
        $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_parameter, 'post'), true);

        // If not found default to 10
        if(!empty($dist_response['data']['entriesFromMpanFields'])) {
            $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
            if($ElecDistId>=24){ $ElecDistId = 17; }
        }
        else {
            $ElecDistId = 10;
        }

        if($data['meterTypeElec']=='STD') { $RateType = '1R'; } else { $RateType = 'E7'; }
        return $this->lookup_yearly_tariff_data($ElecDistId, $RateType);
    }

    function enroll_customer()
    {
        $input = file_get_contents( 'php://input' );
        $data = json_decode( $input, true );

        $escaped = $this->mysql->escape($input);

        $sql = "INSERT INTO initial_customer_info (json_output) VALUES ($escaped);";
        $this->mysql->query($sql);
        $original_id = $this->mysql->insert_id();

        $data['dob'] = str_replace('/', '-', $data['dob']);
        $data['dob'] = date('Y-m-d', strtotime($data['dob']));

        $data['newSpend'] = str_replace(',','',$data['newSpend']);

        $salt = file_get_contents('ese_key.txt');

        $dist_parameter['postcode'] = $data['postcode'];
        $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_parameter, 'post'), true);

        // If not found or greater than 24, default to 17
        $elec_dist_id = !empty($dist_response['data']['entriesFromMpanFields']) ? ( $dist_response['data']['entriesFromMpanFields'][0]['distId'] >= 24 ? 17 : $dist_response['data']['entriesFromMpanFields'][0]['distId'] ) : 17;

        // Find the tariff code matching the tariff name
        if (isset($data['tariffName'])) {

            if ($data['monthyear'] === 'yearlypay' && $data['quotation_pay_energy'] !== 'prepay') {
                $tariff_code = 'FSC1R-F12-NOV2019';
            } else {
                $tariff_data = $this->mysql->select('*')->from('tariff')->like('tariff_name', $data['tariffName'])->get()->first_row('array');
                $tariff_code = $tariff_data['tariff_code'];
            }
        }

        // Sign up customer in Junifer
        if ($data['quotation_pay_energy'] == 'directdebit' || $data['quotation_pay_energy'] == 'paperbill') {

            // Standard parameters across all customers / tariffs
            $parameter = [
                'senderReference' => 'Eversmart',
                'reference' => 'Eversmart',
                'billingEntityCode' => 'Eversmart',
                'marketingOptOutFl' => false,
                'submittedSource' => 'Tele',
                'changeOfTenancyFl' => false,
                'contacts' => [
                    [
                        'title' => $data['title'],
                        'forename' => $data['forename'],
                        'surname' => $data['surname'],
                        'email' => $data['email'],
                        'phone1' => $data['phone'],
                        'phone2' => $data['telephone'],
                        'dateOfBirth' => $data['dob'],
                        'billingMethod' => "Both",
                        'primaryContact' => true,
                        'address' => [
                            'careOf' => $data['forename'] . ' ' . $data['surname'],
                            'address1' => $data['quotation_first_line_address'],
                            'address2' => isset($data['quotation_dplo_address']) ? $data['quotation_dplo_address'] : null,
                            'town' => isset($data['quotation_town_address']) ? $data['quotation_town_address'] : null,
                            'county' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                            'postcode' => $data['postcode'],
                            'countryCode' => 'GB'
                        ],
                    ]
                ],
                'supplyAddress' => [
                    'careOf' => $data['forename'] . ' ' . $data['surname'],
                    'address1' => $data['quotation_first_line_address'],
                    'address2' => isset($data['quotation_dplo_address']) ? $data['quotation_dplo_address'] : null,
                    'town' => isset($data['quotation_town_address']) ? $data['quotation_town_address'] : null,
                    'county' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                    'postcode' => $data['postcode'],
                    'countryCode' => 'GB'
                ],
                'electricityProduct' => [
                    'previousSupplier' => isset($data['quotation_supplier']) ? $data['quotation_supplier'] : 'British Gas',
                    'previousTariff' => $data['existingTariffNameElec'],
                    'productCode' => $tariff_code . '-E',
                    'mpans' => array(['mpanCore' => $data['quotation_mpan']]),
                    'reference' => $this->user_modal->latest_product_ref('elec_reference_count')
                ],
                'electricityEstimate' => [
                    'consumption' => isset($data['quotation_elec_energy_usage']) ? $data['quotation_elec_energy_usage'] : $data['existingkWhsElec'],
                    'period' => 'Year'
                ]
            ];

            if ($data['quotation_for'] == 'both') {
                $parameter['gasProduct'] = [
                    'reference' => $this->user_modal->latest_product_ref('gas_reference_count'),
                    'productCode' => $tariff_code . '-G',
                    'mprns' => array(['mprn' => $data['quotation_mprn']])
                ];
                $parameter['gasEstimate'] = [
                    'consumption' => $data['quotation_gas_energy_usage'],
                    'period' => 'Year'
                ];
            }

            // Add dd credentials to junifer params
            $parameter['directDebitInfo'] = [
                'bankAccountSortCode' => $data['sort_code'],
                'bankAccountNumber' => $data['account_number'],
                'bankAccountName' => $data['account_name'],
                'regularPaymentAmount' => number_format(($data['newSpend'] / 12), 2),
                'regularPaymentDayOfMonth' => 15
            ];


            // Sign the customer up in Junifer
            $response = $this->curl->junifer_request('/rest/v1/customers/enrolCustomer', $parameter, 'post');
            $junifer_response = $this->parseHttpResponse($response);

            // Verify address is not already registered
            if (isset($junifer_response['body']['errorDescription']) && (
                    strstr($junifer_response['body']['errorDescription'], 'is currently billed to a customer') ||
                    strstr($junifer_response['body']['errorDescription'], 'This enrolment has previously been successful')
                )
            ) {
                $sql = "UPDATE initial_customer_info (finished_at) WHERE id = " . $original_id . " VALUES (NOW());";
                $this->mysql->query($sql);
                $this->error_modal->log('signup error', $junifer_response['body']['errorDescription']);
                echo json_encode(['error' => true, 'token' => null, 'msg' => $junifer_response['body']['errorDescription']]);
                exit;
            } else {

                if (!isset($junifer_response['body']['customerId']) || $junifer_response['body']['customerId'] == '') {
                    $this->error_modal->log('signup error', $junifer_response['body']['errorDescription']);
                }

                // Set up database parameters
                $db_parameter = [
                    'customer_id' => isset($junifer_response['body']['customerId']) ? $junifer_response['body']['customerId'] : 0,
                    'title' => $data['title'],
                    'forename' => $data['forename'],
                    'surname' => $data['surname'],
                    'billingMethod' => $data['quotation_pay_energy'],
                    'email' => $data['email'],
                    'phone1' => $data['telephone'],
                    'phone2' => $data['phone'],
                    'dateOfBirth' => $data['dob'],
                    'address_careOf' => $data['forename'] . ' ' . $data['surname'],
                    'first_line_address' => $data['quotation_first_line_address'],
                    'dplo_address' => isset($data['quotation_dplo_address']) ? $data['quotation_dplo_address'] : null,
                    'town_address' => isset($data['quotation_town_address']) ? $data['quotation_town_address'] : null,
                    'city_address' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                    'address_postcode' => $data['postcode'],
                    'address_countryCode' => 'GB',
                    'supplyAddress_careOf' => $data['forename'] . ' ' . $data['surname'],
                    'supplyAddress_address1' => $data['quotation_first_line_address'],
                    'supplyAddress_town_address' => $data['quotation_town_address'],
                    'supplyAddress_city_address' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                    'supplyAddress_postcode' => $data['postcode'],
                    'supplyAddress_countryCode' => 'GB',
                    "signup_type" => $data['desiredPayTypes'] == '' ? '0' : '2',
                    'duosid' => $elec_dist_id,
                    'quotation_eac' => isset($data['quotation_elec_energy_usage']) ? $data['quotation_elec_energy_usage'] : $data['existingkWhsElec'],
                    'quotation_aq' => $data['quotation_gas_energy_usage'],
                    'elec_mpan_core' => $data['quotation_mpan'],
                    'psr_flag' => isset($data['psr_flag']) ? $data['psr_flag'] : NULL
                ];

                if ($data['monthyear'] === 'yearlypay') {

                    // Get LFSC tariff info
                    $yearly_tariff = $this->lookup_yearly_tariff_data($elec_dist_id, '1R');

                    $tariff_info = [
                        'tariff_tariffName' => 'LOYAL FAMILY SAVER',
                        //'tariff_newspend' => $this->session->userdata('year_price'),
                        'tariff_unitRate1Elec' => $yearly_tariff['electricity_dur'],
                        'tariff_standingCharge1Elec' => $yearly_tariff['electricity_sc'],
                        'monthly_interest_customer_signup_date' => date('Y-m-d H:i:s', time()),
                        'exitFeeElec' => 0,
                    ];

                } else {

                    $tariff_info = [
                        'tariff_newspend' => $data['newSpend'],
                        'tariff_tariffName' => $data['tariffName'],
                        'tariff_unitRate1Elec' => round(($data['unitRate1Elec'] / 100), 4),
                        'tariff_standingCharge1Elec' => round(($data['standingChargeElec'] / 100), 4),
                        //'exitFeeElec' => $data['exitFeeElec'],
                        'bankAccountSortCode' => $data['sort_code'],
                        'bankAccountNumber' => $data['account_number'],
                        'bankAccountName' => $data['account_name']
                    ];
                }

                $db_parameter = array_merge($db_parameter, $tariff_info);

                $db_parameter['elecProduct_reference'] = $this->user_modal->latest_product_ref('elec_reference_count');
                $db_parameter['electricityProduct_previousSupplier'] = isset($data['quotation_supplier']) ? $data['quotation_supplier'] : 'British Gas';
                $db_parameter['electricityProduct_previousTariff'] = $data['existingTariffNameElec'];
                $db_parameter['electricityProduct_productCode'] = $tariff_code . '-E';
                $db_parameter['electricityProduct_mpans'] = $data['quotation_mpan'];
                $db_parameter['elec_meter_serial'] = $data['quotation_elec_meter_serial'];
                $db_parameter['saasref'] = $data['saasref'] != '' ? $data['saasref'] : NULL;

                if ($data['quotation_for'] == 'both') {

                    $db_parameter['gas_meter_serial'] = $data['quotation_gas_meter_serial'];
                    $db_parameter['gasProduct_reference'] = $this->user_modal->latest_product_ref('gas_reference_count');
                    $db_parameter['gasProduct_productCode'] = $tariff_code . '-G';
                    $db_parameter['gasProduct_mprns'] = $data['quotation_mprn'];

                    if ($data['monthyear'] === 'yearlypay') {
                        $db_parameter['tariff_unitRate1Gas'] = $yearly_tariff['gas_ur'];
                        $db_parameter['tariff_standingCharge1Gas'] = $yearly_tariff['gas_sc'];
                    } else {
                        $db_parameter['tariff_unitRate1Gas'] = round(($data['unitRate1Gas'] / 100), 4);
                        $db_parameter['tariff_standingCharge1Gas'] = round(($data['standingChargeGas'] / 100), 4);
                    }
                }

                $insert_customer_info = $this->user_modal->insert_customer_info($db_parameter);

                $sql = "UPDATE customer_info SET password=AES_ENCRYPT(SHA1(?), '" . $salt . "') WHERE id=?";
                $this->mysql->query($sql, array($data['password-confirm'], $insert_customer_info));

                // Start Saasquatch handling

                // Create user
                $random = rand(0, 99);
                $para = [
                    "id" => $insert_customer_info,
                    "accountId" => $insert_customer_info,
                    "firstName" => $data['forename'],
                    "lastName" => $data['surname'],
                    "email" => $data['email'],
                    "referable" => "true",
                    "referralCode" => trim($data['forename']) . trim($data['surname']) . ($random <= 9 ? "0" . $random : $random),
                    "locale" => "en_UK",
                    "referredBy" =>
                        array(
                            "code" => $data['saasref'] ? $data['saasref'] : '',
                            "isConverted" => false
                        )
                ];
                $saas_response = $this->curl->curlSaas('open/account/' . $insert_customer_info . '/user/' . $insert_customer_info . '', $para, 'post');
                $new_saas_data = $this->parseHttpResponse($saas_response);

                if (isset($new_saas_data['body']['shareLinks']['shareLink'])) {

                    $ese_share_link = 'http://eversmart.family/' . trim($data['forename']) . trim($data['surname']) . ($random <= 9 ? "0" . $random : $random);

                    // Tie nasty saas link to nice ESE link
                    $db_parameter = [
                        'customer_id' => $insert_customer_info,
                        'ese_generated_link' => $ese_share_link,
                        'saas_generated_link' => $new_saas_data['body']['shareLinks']['shareLink']
                    ];
                    $this->saas_model->insert_ese_share_link($db_parameter);
                }

                // Do we need to tie this new customer to a referrer?
                if ($data['saasref']) {
                    $this->saas_referral_log($insert_customer_info, $data['saasref']);
                }
                // End Saasquatch handling

                $this->intercom_create($insert_customer_info, $data['email'], $data['forename'] . ' ' . $data['surname']);

                if (isset($data['psr_flag']) && $data['psr_flag'] == '1') {

                    $PSRData = [
                        'name' => $data['forename'] . ' ' . $data['surname'],
                        'email' => $data['email'],
                        'phone' => $data['telephone']
                    ];

                    $this->psr_email($insert_customer_info, $PSRData);
                }

                // Send email and store token
                $token = bin2hex(openssl_random_pseudo_bytes(16));
                $this->activation_email($insert_customer_info, $token, $data['forename'], $this->input->post('email'));
                $this->user_modal->token_in_database($token, $insert_customer_info);

                registration_sms_alert($data['phone'], isset($ese_share_link) ? $ese_share_link : '');

                // If boiler_cover is opted-in place into boiler_cover table using ID as a primary key
                if ($data['boiler_cover'] == "1") {
                    $this->user_modal->boiler_cover_optin($insert_customer_info);
                }

                // If the customer was successfully created in junifer - get the account id and tie it to the customer record
                $get_account_id = $this->curl->junifer_request('/rest/v1/accounts/?email=' . $data['email']);
                $get_account = $this->parseHttpResponse($get_account_id);

                if (strtolower($get_account['headers']['http_code']) == 'http/1.1 200 ok') {

                    for ($var = 0; $var < count($get_account['body']); $var++) {
                        if ($var === 0) {
                            $first_account = count($get_account['body']['results']) > 0 ? $get_account['body']['results'][$var]['id'] : '';
                        }
                    }

                    if (isset($first_account) && $first_account > 0) {
                        $this->user_modal->update_account_id($insert_customer_info, $first_account);
                        $sql = "UPDATE `initial_customer_info` SET `finished_at` = now() WHERE `id` = $original_id;";
                        $this->mysql->query($sql);
                    }
                } else {
                    $this->error_modal->log('signup error', $get_account['body']['errorDescription']);
                }
                echo json_encode([
                    'error' => false,
                    'msg' => 'signup successful',
                    'junifer_id' => isset($junifer_response['body']['customerId']) ? $junifer_response['body']['customerId'] : 0,
                    'ese_share_link' => isset($ese_share_link) ? $ese_share_link : '',
                    'cust_info_id' => $insert_customer_info
                ]);
            }
        }


        if ($data['quotation_pay_energy'] == 'prepay') {

            // Set up DB variables
            $db_parameter = [
                'title' => $data['title'],
                'forename' => $data['forename'],
                'surname' => $data['surname'],
                'billingMethod' => $data['quotation_pay_energy'],
                'email' => $data['email'],
                'phone1' => $data['telephone'],
                'phone2' => $data['phone'],
                'dateOfBirth' => $data['dob'],
                'address_careOf' => $data['forename'] . ' ' . $data['surname'],
                'first_line_address' => $data['quotation_first_line_address'],
                'dplo_address' => '',
                'town_address' => $data['quotation_town_address'],
                'city_address' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                'address_postcode' => $data['postcode'],
                'address_countryCode' => 'GB',
                'supplyAddress_careOf' => $data['forename'] . ' ' . $data['surname'],
                'supplyAddress_address1' => $data['quotation_first_line_address'],
                'supplyAddress_town_address' => isset($data['quotation_town_address']) ? $data['quotation_town_address'] : null,
                'supplyAddress_city_address' => isset($data['quotation_city_address']) ? $data['quotation_city_address'] : null,
                'supplyAddress_postcode' => $data['postcode'],
                'supplyAddress_countryCode' => 'GB',
                "electricityProduct_previousSupplier" => isset($data['quotation_supplier']) ? $data['quotation_supplier'] : 'British Gas',
                "electricityProduct_previousTariff" => $data['existingTariffNameElec'],
                "electricityProduct_productCode" => $tariff_code . "-E",
                "electricityProduct_mpans" => $data['quotation_mpan'],
                'elec_mpan_core' => $data['quotation_mpan'],
                'elec_meter_serial' => $data['quotation_elec_meter_serial'],
                "signup_type" => '3',
                'start_date' => date('Y-m-d'),
                'tariff_newspend' => $data['newSpend'],
                'tariff_tariffName' => $data['tariffName'],
                'tariff_unitRate1Elec' => round(($data['unitRate1Elec'] / 100), 4),
                'tariff_standingCharge1Elec' => round(($data['standingChargeElec'] / 100), 4),
                'duosid' => $elec_dist_id,
                'quotation_eac' => isset($data['quotation_elec_energy_usage']) ? $data['quotation_elec_energy_usage'] : $data['existingkWhsElec'],
                'exitFeeElec' => isset($data['exitFeeElec'])?$data['exitFeeElec']:0,
                'psr_flag' => isset($data['psr_flag'])?$data['psr_flag']:NULL
            ];

            if ($data['quotation_for'] == 'both') {

                $db_parameter['gas_meter_serial'] = $data['quotation_gas_meter_serial'];
                $db_parameter["gasProduct_productCode"] = $tariff_code . "-G";
                $db_parameter["gasProduct_mprns"] = $data['quotation_mprn'];
                $db_parameter['quotation_aq'] = $data['quotation_gas_energy_usage'];
                $db_parameter['tariff_unitRate1Gas'] = round(($data['unitRate1Gas'] / 100), 4);
                $db_parameter['tariff_standingCharge1Gas'] = round(($data['standingChargeGas'] / 100), 4);
                $db_parameter['prefix'] = 'DYB_DUAL_';
            }

            // Add Dyball customer to the db
            $insert_customer_info = $this->user_modal->insert_customer_info($db_parameter);

            // If it was successful
            if ($insert_customer_info > 0) {

                // Update the password record
                $sql = "UPDATE customer_info SET password=AES_ENCRYPT(SHA1(?), '" . $salt . "') WHERE id=?";
                $this->mysql->query($sql, array($data['password-confirm'], $insert_customer_info));


                // Insert token in database and send email
                $token = md5(uniqid(rand(0, 9999), true));
                $this->activation_email($insert_customer_info, $token, $data['forename'], $data['email']);
                $this->user_modal->token_in_database($token, $insert_customer_info);


                // Only insert into opt-in DB if tariff is eligible
                if ($data['boiler_cover'] == "1") {
                    $this->user_modal->boiler_cover_optin($insert_customer_info);
                }

                registration_sms_alert($data['phone'], isset($ese_share_link) ? $ese_share_link : '');

                // Do we need to send the PSR notification
                if (isset($data['psr_flag']) && $data['psr_flag'] == '1') {

                    $PSRData = [
                        'name' => $data['forename'] . ' ' . $data['surname'],
                        'email' => $data['email'],
                        'phone' => $data['telephone']
                    ];

                    $this->psr_email($insert_customer_info, $PSRData);
                }

                // Register the customer in Dyball
                $dyb_parameter = [
                    'AuthKey' => $this->authkey_dyball_salemade,
                    'CustomerReference' => "DYB_" . $insert_customer_info,
                    'SaleMadeBy' => "Eversmart Website",
                    'SaleMadeOn' => date('Y-m-d', time()),
                    'SaleType' => "CoS", // only CoS currently supported
                    'CustomerTitle' => $data['title'],
                    'CustomerFirstName' => $data['forename'],
                    'CustomerLastName' => $data['surname'],
                    'CustomerDateOfBirth' => $data['dob'],
                    'CustomerAddressLine1' => $data['quotation_first_line_address'],
                    'CustomerPostcode' => $data['postcode'],
                    'CustomerPhoneNumber' => $data['telephone'],
                    'PaymentMethod' => "PrePay",
                    'ElectricityTariffName' => $data['tariffName'],
                    'ElectricEAC' => isset($data['quotation_elec_energy_usage']) ? $data['quotation_elec_energy_usage'] : $data['existingkWhsElec'],
                    'MPANs' => array(array( // Leave this - API expects double array
                        "MPAN" => $data['quotation_mpan'],
                        "UseIndustryDataLookup" => true
                    ))
                ];

                if ($data['quotation_for'] == 'both') {
                    $dyb_parameter['GasTariffName'] = $data['tariffName'];
                    $dyb_parameter['GasEAC'] = $data['quotation_mprn'];
                    $dyb_parameter['MPRNs'] = [[ // Leave this - API expects double array
                        "MPRN" => $data['quotation_mprn'],
                        "UseIndustryDataLookup" => true
                    ]];
                }

                $this->curl->dyball_curl($dyb_parameter, 'salemade');

                // End register customer in Dyball

                $this->intercom_create($insert_customer_info, $data['email'], $data['forename'].' '.$data['surname']);

                if ($insert_customer_info) {
                    $sql = "UPDATE initial_customer_info (finished_at) WHERE id = " . $original_id . " VALUES (NOW());";
                    $this->mysql->query($sql);
                    echo json_encode(['error' => false, 'msg' => 'Sign-up successful']);
                } else {
                    echo json_encode(['error' => true, 'msg' => 'Error registering prepay customer']);
                }

            } else {
                echo json_encode(['error' => true, 'msg' => 'Error registering prepay customer']);
            }
        }

    }

    /**
     * Tie together new customer with referrer
     *
     * @param
     * $id int
     * $code varchar
     * @return boolean
     */
    function saas_referral_log($id, $code){

        // get referrer id
        $referrer_id = $this->curl_requests_saasquatch->get_saas_id_by_code($code);

        // tie together in saas log
        $log_data = [
            'referrer_customer_id' => $referrer_id,
            'referred_customer_id' => $id
        ];

        $this->saas_model->referral_log($log_data);

        return true;
    }

    public static function logMessage($errLevel, $msg, $depth = 1)
    {
        $btStack = debug_backtrace($depth);
        $callerFrame = $btStack[$depth - 1];
        $sid = substr(session_id(), 0, 8);
        $method = $callerFrame['class'] . '.' . $callerFrame['function'];
        $lineNo = $callerFrame['line'];
        $fullMessage = 'SID:' . $sid . ' ' . $method . '(' . $lineNo . '): ' . $msg;
        log_message($errLevel, $fullMessage);
    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart) {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if (strpos($lines[0], 'HTTP/1.1') !== false) {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0])) {
                    // 4xx or 5xx error
                    // so log the error
                    $this->logMessage('error', 'Line 0=' . $lines[0], 2);
                }

                foreach ($lines as $j => $line) {
                    if ($j === 0) {
                        $headers['http_code'] = $line;
                    } else {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            } // If last element, probably body
            else if ($i === sizeof($responseParts) - 1) {
                $body = json_decode($responsePart, true);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }

    function confirmation()
    {
        if (empty($_GET['t'])) {

            $data = [ 'error' => 1, 'message' => 'No token provided. Please contact support at support@eversmartenergy.co.uk'];
            $this->load->view('layout/confirmation_pending_template', $data);
        }

        //check for the token
        $check_for_token = $this->user_modal->check_token_confirm($_GET['t']);
        $get_customer_detail = $this->user_modal->get_account_detail($check_for_token['customer_id']);


        // Dyball
        if ($get_customer_detail['signup_type'] === 3) {

            $this->user_modal->change_status($_GET['t']);
            $this->load->view('layout/confirmation_template');

        }


        // Junifer
        if ($get_customer_detail['signup_type'] == '0' && $get_customer_detail['success'] == null ) {

            $parameter = ["token" => $_GET['t']];

            $response = $this->curl->junifer_request('/rest/v1/webUsers/activate', $parameter, 'post');
            $response = $this->parseHttpResponse($response);


            if ($response['headers']['http_code'] === 'HTTP/1.1 204 No Content') {

                $this->user_modal->change_status($_GET['t']);
                $this->load->view('layout/confirmation_template');

            } elseif ($response['headers']['http_code'] === 'HTTP/1.1 400 Bad Request') {

                $data = [ 'error' => 1, 'message' => 'We seem to be having a problem activating your online account.<br>Please drop us an e-mail at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to let us know and we’ll get right on it!'];
                $this->load->view('layout/confirmation_pending_template', $data);

            } else {

                $data = [ 'error' => 1, 'message' => 'We seem to be having a problem activating your online account.<br>Please drop us an e-mail at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to let us know and we’ll get right on it!'];
                $this->load->view('layout/confirmation_pending_template', $data);

            }

        }
        else {

            $data = [ 'error' => 1, 'message' => 'We seem to be having a problem activating your online account.<br>Please drop us an e-mail at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to let us know and we’ll get right on it!'];
            $this->load->view('layout/confirmation_pending_template', $data);

        }

    }

    function login()
    {
        $data['scripts'] = [];
        $data['stylesheets'] = [
            base_url() . 'assets/new-world/style.css',
            'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
        ];
        $data['bol_bootstrap_enabled'] = true;
        $data['content'] = 'user/login';
        $this->load->view('new-world/master', $data);
    }

    function check_login()
    {

        $token = $this->input->post('token');

        if ($this->input->post('user_email') == 'meterbooking@eversmartenergy.co.uk' && $this->input->post('user_password') == 'admin') {
            $info['customer_id'] = 0;
            $info['account_id'] = 0;
            $info['account_number'] = 0;
            $info['title'] = 'Mr.';
            $info['first_name'] = 'Admin';
            $info['account_status'] = '0';
            $info['signup_type'] = '6';
            $this->session->set_userdata('login_data', $info);
            echo json_encode(['error' => '5', 'data' => [], 'msg' => 'admin login']);
            exit;
        }

        $login_response = $this->user_modal->login_check($this->input->post());

        if (isset($login_response['admin_role_type_id']) && in_array($login_response['admin_role_type_id'], array(1,2,3,4,6))) {

            $this->session->set_userdata('login_data', $login_response);

            switch ($login_response['admin_role_type_id']){
                case 1:
                    $message = 'admin_success';
                    break;
                case 2:
                    $message = 'cs_success';
                    break;
                case 3:
                    $message = 'tm_success';
                    break;
                case 4:
                    $message = 'sf_success';
                    break;
                case 6:
                    $message = 'finance_success';
                    break;

            }

            echo json_encode(['error' => '0', 'msg' => $message]);
            exit;

        } else if (!empty($login_response) || $login_response != null) {

            // Get / Set & Get intercom credentials in session
            if(isset($login_response['intercom_id']) && $login_response['intercom_id'] != '')
            {
                $IntercomData = $this->get_intercom($login_response['intercom_id']);

                //print_r($IntercomData);

                if(isset($IntercomData['errors'][0]['message']) && $IntercomData['errors'][0]['message'] == 'User Not Found') {
                    $name = $login_response['forename'].' '.$login_response['surname'];
                    $IntercomData = $this->intercom_create($login_response['id'], $login_response['email'], $name);
                }
            }
            else
            {
                $name = $login_response['forename'].' '.$login_response['surname'];
                $IntercomData = $this->intercom_create($login_response['id'], $login_response['email'], $name);
            }
            // End if else intercom in session


            if ($login_response['active'] == 1) {

                if ($login_response['api_user_status'] == 0) {

                    //$next_week = date('d/m/Y', strtotime($login_response['create_at'].'+1 week')); // 26/05/2014

                    $DateSeg = explode('-', $login_response['create_at']);
                    $CreatedAt = date('Y-m-d H:i:s', mktime(23, 59, 59, (int)$DateSeg[1], (int)$DateSeg[2], (int)$DateSeg[0]));

                    $next_week = strtotime($CreatedAt . '+21 days');
                    $noOfDay = date('Y-m-d', $next_week);  // date after 21 days

                    //echo $now = date( 'Y-m-d', strtotime($login_response['create_at'])); // or your date as well
                    $now = date('Y-m-d');
                    $date1 = date_create($now);
                    $date2 = date_create(date('Y-m-d', strtotime($login_response['create_at'])));

                    //difference between two dates
                    $diff = date_diff($date1, $date2);

                    //count days
                    $info['current_day'] = $diff->format("%a");


                    //$difference = $next_week - time(); // next weeks date minus todays date
                    //$difference = date('j', $difference);

                    // if we are after the 3 weeks set difference to 0
                    if ($next_week < time()) {
                        $difference = '0';
                    } else {

                        $difference = $next_week - time(); // next weeks date minus todays date
                        $difference = date('j', $difference);
                    }


                    $info['day_left'] = $difference;

                    $info['create_at'] = $login_response['create_at'];
                    $info['first_reading_gas'] = $login_response['first_reading_gas'];
                    $info['first_reading_elec'] = $login_response['first_reading_elec'];
                    $info['booked_meter'] = $login_response['booked_meter'];
                    $info['admin_role_type_id'] = $login_response['admin_role_type_id'];
                    $info['id'] = $login_response['id'];
                    $info['intercom_id'] = $IntercomData['id'];
                    $info['intercom_user_id'] = $IntercomData['user_id'];
                    $info['intercom_name'] = $IntercomData['name'];
                    $info['intercom_email'] = $IntercomData['email'];
                    $info['title'] = $login_response['title'];
                    $info['first_name'] = $login_response['forename'];
                    $info['last_name'] = $login_response['surname'];
                    $info['phone1'] = $login_response['phone1'];
                    $info['signup_type'] = $login_response['signup_type'];
                    $info['account_status'] = $login_response['api_user_status'];
                    // $info['user_mpan'] = $check_user_active['electricityProduct_mpans'];
                    $info['user_mprn'] = $login_response['gasProduct_mprns'];
                    $info['address'] = $login_response['first_line_address'];
                    $info['town_address'] = $login_response['town_address'];
                    $info['dplo_address'] = $login_response['dplo_address'];
                    $info['city_address'] = $login_response['city_address'];
                    $info['address_postcode'] = $login_response['address_postcode'];
                    $info['phone1'] = $login_response['phone1'];
                    $info['mpancore'] = $login_response['elec_mpan_core'];

                    $info['elec_meter_serial'] = $login_response['elec_meter_serial'];
                    $info['gas_meter_serial'] = $login_response['gas_meter_serial'];

                    $info['customer_id'] = $login_response['customer_id'];
                    $info['account_id'] = $login_response['account_id'];
                    $info['account_number'] = $login_response['account_id'];

                    $info['email'] = $login_response['email'];
                    $info['mpancore'] = $login_response['elec_mpan_core'];
                    $info['meterpoint_mpan'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPAN"];
                    $info['meterpoint_mprn'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPRN"];

                    $info['billingMethod'] = $login_response['billingMethod'];
                    $info['tariff_newspend'] = $login_response['tariff_newspend'];
                    $info['exitFeeElec'] = $login_response['exitFeeElec'];
                    $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
                    $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];

                    $info['meterTypeElec'] = $login_response['meterTypeElec'];

                    // Initialise this var...will be overwritten on first successful login
                    // this is when api_user_status is set so we wont set this in the future
                    $info['junifer_account_active'] = 0;

                    // debug($info,1);
                    $this->session->set_userdata('login_data', $info);

                    // Prepay (Dyball) customers should be taken straight into the portal with a restricted view
                    if ($login_response['signup_type'] == 3) { // Dyball

                        echo json_encode(['error' => '0', 'data' => $info, 'msg' => 'dyball_success']);
                        exit;
                    } else {
                        echo json_encode(['error' => '4', 'msg' => 'user status at junifer is pending']);
                        exit;

                    }

                }

                if ($login_response['signup_type'] == 0 || $login_response['signup_type'] == 2) {

                    //get user detail from junifer
                    //debug($junifer_response);
                    if (!empty($login_response['customer_id'])) {

                        $info['admin_role_type_id'] = $login_response['admin_role_type_id'];
                        $info['id'] = $login_response['id'];
                        $info['intercom_id'] = $IntercomData['id'];
                        $info['intercom_user_id'] = $IntercomData['user_id'];
                        $info['intercom_name'] = $IntercomData['name'];
                        $info['intercom_email'] = $IntercomData['email'];
                        $info['title'] = $login_response['title'];
                        $info['first_name'] = $login_response['forename'];
                        $info['last_name'] = $login_response['surname'];
                        $info['signup_type'] = $login_response['signup_type'];
                        // $info['user_mpan'] = $check_user_active['electricityProduct_mpans'];
                        $info['user_mprn'] = $login_response['gasProduct_mprns'];
                        $info['address'] = $login_response['first_line_address'];
                        $info['town_address'] = $login_response['town_address'];
                        $info['dplo_address'] = $login_response['dplo_address'];
                        $info['city_address'] = $login_response['city_address'];
                        $info['address_postcode'] = $login_response['address_postcode'];
                        $info['phone1'] = $login_response['phone1'];
                        $info['mpancore'] = $login_response['elec_mpan_core'];
                        $info['account_status'] = $login_response['api_user_status'];
                        $info['elec_meter_serial'] = $login_response['elec_meter_serial'];
                        $info['gas_meter_serial'] = $login_response['gas_meter_serial'];

                        $info['customer_id'] = $login_response['customer_id'];
                        $info['account_id'] = $login_response['account_id'];

                        $info['email'] = $login_response['email'];
                        $info['mpancore'] = $login_response['elec_mpan_core'];
                        $info['meterpoint_mpan'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPAN"];
                        $info['meterpoint_mprn'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPRN"];

                        $info['billingMethod'] = $login_response['billingMethod'];
                        $info['tariff_newspend'] = $login_response['tariff_newspend'];
                        $info['exitFeeElec'] = $login_response['exitFeeElec'];
                        $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
                        $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];
                        $info['meterTypeElec'] = $login_response['meterTypeElec'];

                        $info['junifer_account_active'] = 1;





                        $account_balanace = $this->curl->junifer_request('/rest/v1/accounts/' . $login_response['account_id']);


                        // $account_balanace = $this->curl->junifer_request('/rest/v1/accounts/525');

                        $response = $this->parseHttpResponse($account_balanace);

                        if (isset($response['headers']['http_code'])) {

                            if ($response['headers']['http_code'] == 'HTTP/1.1 200 OK') {
                                $info['balance'] = $response['body']['balance'];
                                $info['account_number'] = $response['body']['number'];
                            } else {
                                $info['balance'] = 0;
                                $info['account_number'] = 0;
                            }
                        } else {
                            $info['balance'] = 0;
                            $info['account_number'] = 0;
                        }


                        /*

                        $info['balance'] = 0;
                        $info['account_number'] = 0;

                          /** remove junifer is live */


                        //set data in session
                        $this->session->set_userdata('login_data', $info);
                        echo json_encode(['error' => '0', 'data' => $info, 'msg' => 'success']);

                    } else {
                        echo json_encode(['error' => '1', 'data' => [], 'msg' => 'error']);
                        exit;
                    }
                } elseif ($login_response['signup_type'] == 3) {

                    $info['admin_role_type_id'] = $login_response['admin_role_type_id'];
                    $info['id'] = $login_response['id'];
                    $info['intercom_id'] = $IntercomData['id'];
                    $info['intercom_user_id'] = $IntercomData['user_id'];
                    $info['intercom_name'] = $IntercomData['name'];
                    $info['intercom_email'] = $IntercomData['email'];
                    $info['title'] = $login_response['title'];
                    $info['first_name'] = $login_response['forename'];
                    $info['last_name'] = $login_response['surname'];
                    $info['signup_type'] = $login_response['signup_type'];
                    // $info['user_mpan'] = $check_user_active['electricityProduct_mpans'];
                    $info['user_mprn'] = $login_response['gasProduct_mprns'];
                    $info['address'] = $login_response['first_line_address'];
                    $info['dplo_address'] = $login_response['dplo_address'];
                    $info['town_address'] = $login_response['town_address'];
                    $info['city_address'] = $login_response['city_address'];
                    $info['address_postcode'] = $login_response['address_postcode'];
                    $info['phone1'] = $login_response['phone1'];

                    $info['mpancore'] = $login_response['elec_mpan_core'];
                    $info['account_status'] = $login_response['api_user_status'];
                    $info['elec_meter_serial'] = $login_response['elec_meter_serial'];
                    $info['gas_meter_serial'] = $login_response['gas_meter_serial'];

                    $info['customer_id'] = $login_response['customer_id'];
                    $info['account_id'] = $login_response['account_id'];
                    $info['prefix'] = $login_response['prefix'];
                    $info['email'] = $login_response['email'];
                    $info['mpancore'] = $login_response['elec_mpan_core'];
                    $info['meterpoint_mpan'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPAN"];
                    $info['meterpoint_mprn'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPRN"];

                    $info['account_number'] = $login_response['prefix'] . $login_response['id'];

                    $info['billingMethod'] = $login_response['billingMethod'];
                    $info['tariff_newspend'] = $login_response['tariff_newspend'];
                    $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
                    $info['exitFeeElec'] = $login_response['exitFeeElec'];
                    $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];
                    $info['meterTypeElec'] = $login_response['meterTypeElec'];

                    $info['junifer_account_active'] = 0;

                    //set data in session
                    $this->session->set_userdata('login_data', $info);
                    echo json_encode(['error' => '0', 'data' => $info, 'msg' => 'dyball_success']);
                }

            } else {

                if (empty($token)) {
                    echo json_encode(['error' => '1', 'msg' => 'Token required for reset password']);
                    exit;
                } else {

                    //check token in database
                    $check_token = $this->user_modal->check_token($token);

                    //check for token type
                    if ($check_token['token_type'] == '0') {

                        //check for token expiry
                        $curret_date = date('Y-m-d H:i:s');
                        $token_date = $check_token['expire_date'];

                        //Convert them to timestamps.
                        $date1Timestamp = strtotime($token_date);
                        $date2Timestamp = strtotime($curret_date);

                        //Calculate the difference.
                        $difference = $date2Timestamp - $date1Timestamp;

                        //echo '<br>'.$difference;
                        if ($difference > 0) {
                            echo json_encode(['error' => '1', 'msg' => 'Token expired']);
                            exit;
                        } else {

                            // update query
                            $customer_update = $this->user_modal->customer_update($login_response['id']);
                            if ($customer_update) {

                                if ($login_response['signup_type'] == 0 || $login_response['signup_type'] == 2) {

                                    //get user detail from junifer
                                    if (!empty($login_response['customer_id'])) {

                                        $info['admin_role_type_id'] = $login_response['admin_role_type_id'];
                                        $info['id'] = $login_response['id'];
                                        $info['intercom_id'] = $IntercomData['id'];
                                        $info['intercom_user_id'] = $IntercomData['user_id'];
                                        $info['intercom_name'] = $IntercomData['name'];
                                        $info['intercom_email'] = $IntercomData['email'];
                                        $info['title'] = $login_response['title'];
                                        $info['first_name'] = $login_response['forename'];
                                        $info['last_name'] = $login_response['surname'];
                                        $info['signup_type'] = $login_response['signup_type'];
                                        // $info['user_mpan'] = $check_user_active['electricityProduct_mpans'];
                                        $info['user_mprn'] = $login_response['gasProduct_mprns'];
                                        $info['address'] = $login_response['first_line_address'];
                                        $info['dplo_address'] = $login_response['dplo_address'];
                                        $info['town_address'] = $login_response['town_address'];
                                        $info['city_address'] = $login_response['city_address'];
                                        $info['address_postcode'] = $login_response['address_postcode'];
                                        $info['phone1'] = $login_response['phone1'];
                                        $info['mpancore'] = $login_response['elec_mpan_core'];
                                        $info['account_status'] = $login_response['api_user_status'];
                                        $info['elec_meter_serial'] = $login_response['elec_meter_serial'];
                                        $info['gas_meter_serial'] = $login_response['gas_meter_serial'];

                                        $info['customer_id'] = $login_response['customer_id'];
                                        $info['account_id'] = $login_response['account_id'];

                                        $info['email'] = $login_response['email'];
                                        $info['mpancore'] = $login_response['elec_mpan_core'];
                                        $info['meterpoint_mpan'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPAN"];
                                        $info['meterpoint_mprn'] = ["id" => 314, "identifier" => "1200020346970", "type" => "MPRN"];

                                        $info['billingMethod'] = $login_response['billingMethod'];
                                        $info['tariff_newspend'] = $login_response['tariff_newspend'];
                                        $info['exitFeeElec'] = $login_response['exitFeeElec'];
                                        $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
                                        $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];

                                        $account_balanace = $this->curl->junifer_request('/rest/v1/accounts/' . $login_response['account_id']);
                                        $response = $this->parseHttpResponse($account_balanace);
                                        //debug($response,1);

                                        if ($response['headers']['http_code'] == 'HTTP/1.1 200 OK') {
                                            $info['balance'] = $response['body']['balance'];
                                            $info['account_number'] = $response['body']['number'];
                                        } else {
                                            $info['balance'] = 0;
                                            $info['account_number'] = 0;
                                        }

                                        //set data in session
                                        $this->session->set_userdata('login_data', $info);
                                        echo json_encode(['error' => '0', 'data' => $info, 'msg' => 'success']);
                                    } else {
                                        echo json_encode(['error' => '1', 'data' => [], 'msg' => 'error']);
                                    }


                                } elseif ($login_response['signup_type'] == 3) {

                                    $info['admin_role_type_id'] = $login_response['admin_role_type_id'];
                                    $user_login_detail['id'] = $login_response['id'];
                                    $user_login_detail['customers'] = $login_response['id'];
                                    $user_login_detail['accounts'] = $login_response['prefix'] . $login_response['id'];
                                    $user_login_detail['account_number'] = $login_response['prefix'] . $login_response['id'];
                                    $user_login_detail['user_mpan'] = $login_response['electricityProduct_mpans'];
                                    $user_login_detail['user_mprn'] = $login_response['gasProduct_mprns'];
                                    $user_login_detail['user_name'] = $login_response['forename'];
                                    $info['address'] = $login_response['first_line_address'];
                                    $info['town_address'] = $login_response['town_address'];
                                    $info['dplo_address'] = $login_response['dplo_address'];
                                    $info['city_address'] = $login_response['city_address'];
                                    $user_login_detail['signup_type'] = $login_response['signup_type'];
                                    $user_login_detail['prefix'] = $login_response['prefix'];
                                    $user_login_detail['title'] = $login_response['title'];
                                    $user_login_detail['account_status'] = $login_response['api_user_status'];
                                    $user_login_detail['billingMethod'] = $login_response['billingMethod'];
                                    $user_login_detail['tariff_newspend'] = $login_response['tariff_newspend'];
                                    $user_login_detail['tariff_tariffName'] = $login_response['tariff_tariffName'];
                                    $user_login_detail['exitFeeElec'] = $login_response['exitFeeElec'];
                                    $user_login_detail['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];

                                    //set data in session
                                    $this->session->set_userdata('login_data', $user_login_detail);
                                    echo json_encode(['error' => '0', 'data' => $user_login_detail, 'msg' => 'success']);

                                }

                            }
                        }

                    } else {

                        echo json_encode(['error' => '1', 'msg' => 'invalid token']);

                    }
                }
            }

        } else {

            echo json_encode(['error' => '2', 'msg' => 'wrong username password']);

        }

    }

    function dashboard()
    {

        if ($this->session->has_userdata('login_data')) {

            $daily_use_gas_total = 0;
            $daily_use_elec_total = 0;

            $get_user_account_id = $this->session->userdata('login_data')['account_id'];
            $get_id = $this->session->userdata('login_data')['id'];


            /* Payment */
            if ($this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 2) {

                $get_balance_junifer = $this->curl->junifer_request('/rest/v1/accounts/' . $get_user_account_id . '');
                $get_balance_junifer = $this->parseHttpResponse($get_balance_junifer);
                if (isset($get_balance_junifer['headers']['http_code']) && $get_balance_junifer['headers']['http_code'] == 'HTTP/1.1 200 OK') {
                    $data['balance'] = $get_balance_junifer['body']['balance'];
                }

                $from_date = date('Y-m-d', strtotime("-1 days"));
                $to_date = date('Y-m-d');
                $fromdate_monthly = date('Y-m-01');
                $todate_monthly = date('Y-m-01', strtotime("+1 month"));
                $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/' . $get_user_account_id . '/agreements');
                $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);

                if (!empty($get_meterpoint_ids_responce->results)) {
                    for ($r = 0; $r < count($get_meterpoint_ids_responce->results); $r++) {
                        if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {
                            if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {
                                $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;
                            }
                        } elseif ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {
                            if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {
                                $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;
                            }
                        }
                    }
                }

                if (isset($electric_meter_point_id)) {

                    $daily_use_elec_total = 0;
                    $el_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $electric_meter_point_id . '/readings?fromDt=' . $fromdate_monthly . '&toDt=' . $todate_monthly . '');
                    $response = json_decode($el_response_monthly_array, true);

                    $daily_use_elec = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $electric_meter_point_id . '/readings?fromDt=' . $from_date . '&toDt=' . $to_date . '');
                    $daily_use_elec = json_decode($daily_use_elec, true);
                    if(isset($daily_use_elec['results'][0]['consumption'])){
                        $daily_use_elec_total = $daily_use_elec['results'][0]['consumption'] + $daily_use_elec['results'][1]['consumption'];
                    }
                    else {
                        $daily_use_elec_total = 0;
                    }
                    $data['results'] = $response['results'];
                    $data['last_reading'] = end($response['results']);

                }

                if (isset($gas_meter_point_id)) {
                    $daily_use_gas_total = 0;
                    $gas_response = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $gas_meter_point_id . '/readings?fromDt=' . $fromdate_monthly . '&toDt=' . $todate_monthly . '');
                    $gas_response = json_decode($gas_response, true);

                    $daily_use_gas = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $gas_meter_point_id . '/readings?fromDt=' . $from_date . '&toDt=' . $to_date . '');
                    $daily_use_gas = json_decode($daily_use_gas, true);
                    if(isset($daily_use_gas['results'][0]['consumption'])){
                        $daily_use_gas_total = $daily_use_gas['results'][0]['consumption'] + $daily_use_gas['results'][1]['consumption'];
                    }
                    else {
                        $daily_use_gas_total = 0;
                    }
                    $data['daily_use_gas_total'] = $daily_use_gas_total;
                    $data['results'] = $gas_response['results'];
                    $data['last_reading_gas'] = end($gas_response['results']);
                }

            }


            /* Saas */
            $referrals = $this->curl->curlSaas('account/' . $get_id . '/user/' . $get_id . '/pii');
            $referrals = json_decode($referrals);

            if(isset($referrals->apiErrorCode) && $referrals->apiErrorCode == 'USER_NOT_FOUND'){
                $ref = 0;
                $format_bal = 0;
            }
            else {

                $ref = 0;
                $format_bal = 0;
                $ref = $referrals->referrals->totalCount;
                //echo '<pre>'.print_r($ref,1).'</pre>';
                $list_reward = $this->curl->curlSaas('reward/balance?accountId=' . $get_id . '');
                $list_reward = json_decode($list_reward);
                //echo '<pre>'.print_r($list_reward,1).'</pre>';
                if ($list_reward == null) {
                    $format_bal = 0;
                } else {
                    $assigned_bal = $list_reward[0]->totalAssignedCredit;
                    $redeemed_bal = $list_reward[0]->totalRedeemedCredit;
                    $current_bal = $assigned_bal - $redeemed_bal;
                    $format_bal = $current_bal / 100;
                }
            }

            /* Messages */
            if ($this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 2) {
                $junifer_msgs = $this->curl->junifer_request('/rest/v1/accounts/' . $get_user_account_id . '/communications');
                $junifer_msgs = $this->parseHttpResponse($junifer_msgs);
                if (isset($junifer_msgs['headers']['http_code']) && $junifer_msgs['headers']['http_code'] == 'HTTP/1.1 200 OK') {
                    $junifer_msgs = $junifer_msgs['body'];
                } else {
                    $junifer_msgs = [];
                }
            } else {
                $junifer_msgs = [];
            }
            $data['daily_use_elec_total'] = $daily_use_elec_total;
            $data['daily_use_gas_total'] = $daily_use_gas_total;
            $data['junifer_msgs'] = $junifer_msgs;
            $data['ref'] = $ref;
            $data['format_bal'] = $format_bal;
            $data['session_data'] = $this->session->userdata('login_data');
            $this->load->view('user/dashboard', $data);
        } else {
            $this->login();
        }
    }

    function logout()
    {
        session_destroy();
        echo json_encode(['error' => '0', 'data' => [], 'msg' => 'logout']);
    }

    function meter_reading()
    {
        //debug($this->session->userdata('login_data'),1);
        if (empty($this->session->userdata('login_data'))) {
            header('location:' . base_url() . 'index.php/user/login');
            exit;
        }
        //check session data
        //debug($this->session->userdata('login_data'));
        if ($this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 2)
            if ($this->session->has_userdata('login_data')) {

                //get user reading from junifer
                //debug($this->session->userdata('login_data'));
                $current_year = date('Y-m-d');
                $last_year = date('Y-m-d', strtotime(date("Y-m-d", time()) . " - 365 day"));
                $fromdate_monthly = date('Y-m-01');        //date('Y-m-01')
                $todate_monthly = date('Y-m-01', strtotime("+1 month"));


                //get meterpoint id
                //  echo '/rest/v1/ecoes/mpans?postcode=WA15%208RS&limit=5';
                // $curl_request = $this->curl->junifer_request('/rest/v1/ecoes/mpans?postcode=WA15%208RS&limit=5');

                // echo '/rest/v1/ecoes/meterPoints?meterSerialNumber=D07W550193&queryDttm=2018-05-17';
                // $curl_request = $this->curl->junifer_request('/rest/v1/ecoes/meterPoints?meterSerialNumber=D07W550193&queryDttm=2018-05-17' );
                // $curl_request = $this->parseHttpResponse($curl_request);
                // debug($curl_request,1);
                // //echo '/rest/v1/meterPoints/'.$this->session->userdata('login_data')['account_id'].'/paymentscheduleperiods?fromDt='.$current_year.'&toDt='.$last_year;
                //
                // $parameter = [ 'fromDt' => $current_year, 'toDt' => $last_year ];
                // $response = $this->curl->junifer_request('/rest/v1/meterPoints/'.$this->session->userdata('login_data')['account_id'].'/paymentscheduleperiods?fromDt='.$current_year.'&toDt='.$last_year);
                // $response = $this->parseHttpResponse($response);

                $get_user_account_id = $this->session->userdata('login_data')['account_id'];
                $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/' . $get_user_account_id . '/agreements');
                $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);


                if (!empty($get_meterpoint_ids_responce->results)) {
                    for ($r = 0; $r < count($get_meterpoint_ids_responce->results); $r++) {


                        if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

                            if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                                $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;

                            }
                        } elseif ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

                            if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                                $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;

                            }
                        }
                    }
                }


                if (isset($electric_meter_point_id)) {
                    $el_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $electric_meter_point_id . '/readings?fromDt=' . $fromdate_monthly . '&toDt=' . $todate_monthly . '');
                    $response = json_decode($el_response_monthly_array, true);
                    $data['results'] = $response['results'];
                    $data['last_reading'] = end($response['results']);
                }


                if (isset($gas_meter_point_id)) {
                    $gas_response = $this->curl->junifer_request_usage('/rest/v1/meterPoints/' . $gas_meter_point_id . '/readings?fromDt=' . $fromdate_monthly . '&toDt=' . $todate_monthly . '');
                    $gas_response = json_decode($gas_response, true);
                    $data['gas_reading'] = $gas_response['results'];
                    $data['last_reading_gas'] = end($gas_response['results']);
                    //var_dump($data['gas_results']);
                }


                //  $response = $this->curl->junifer_request_test('/rest/v1/meterPoints/402/readings');
                //  $response = json_decode($response,true);
                // debug($response);
                header('content-type: text/html');
//          $data['results'] = $response['results'];
                $data['session_data'] = $this->session->userdata('login_data');

                $data['content'] = 'dashboard/meter_reading';
                //debug($data,1);
            }
        if ($this->session->userdata('login_data')['signup_type'] == 3) {
            // echo '1';
            $result_response = array();
            $gas_results = array();
            if (!empty($this->session->userdata('login_data')['mpancore'])) {
                $parameter = ["AuthKey" => $this->authkey_dyball_serve, "AccountNumber" => "DYB_ELEC_" . $this->session->userdata('login_data')['id']];
                //debug($parameter);
                $response = $this->curl->curl_dyball('/api/customerselfservice/GetElectricityReadings', $parameter, 'post');
                //debug($response,1);
                $response = json_decode($response, true);
                $result = $response['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                for ($i = 0; $i < count($result); $i++) {
                    $result_response[$i]['readingDttm'] = $result[$i]['ReadDate'];
                    $result_response[$i]['consumption'] = $result[$i]['Reading'];
                }
                //$data['last_reading'] = end($response['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings']);
            } else {
                $result_response = '';
            }
            if (!empty($this->session->userdata('login_data')['user_mprn'])) {
                $parameter_gas = ["AuthKey" => "EVERSMARTENERGYSUPP", "AccountNumber" => "EVDYB" . $this->session->userdata('login_data')['account_id']];
                $response_gas = $this->curl->junifer_request_test('/api/CustomerSelfService/GetGasReadings', $parameter_gas, 'post');
                $response_gas = json_decode($response_gas, true);
                $result_gas = $response_gas['Mprns'][0]['MprnMeters'][0]['Readings'];
                for ($i = 0; $i < count($result_gas); $i++) {
                    $gas_results[$i]['readingDttm'] = $result_gas[$i]['ReadDate'];
                    $gas_results[$i]['consumption'] = $result_gas[$i]['Reading'];
                }
                if (isset($response_gas['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                    $data['last_gas_reading'] = end($response_gas['Mprns'][0]['MprnMeters'][0]['Readings']);
                } else {
                    $data['last_gas_reading'] = [];
                }
            } else {
                $gas_results = '';
            }

            //debug($response);
            header('content-type: text/html');
            $data['results'] = $result_response;
            $data['gas_reading'] = $gas_results;
            $data['session_data'] = $this->session->userdata('login_data');

            $data['content'] = 'dashboard/meter_reading';
        }
        $data['title'] = 'Meter Reading';
        //debug($data,1);
        $this->load->view('layout/dashboard_master', $data);


    }

    function submit_reading()
    {

        if ($this->input->post('signupType') == 3) // Dyball
        {

            $login_response = $this->user_modal->get_account_detail($this->session->userdata('login_data')['id']);
            $users_mpan = $login_response['elec_mpan_core']; // '1610009348896' ;

            if (isset($users_mpan)) {

                $parameter = [
                    "AuthKey" => "b450e45e-63b9-4c68-b1d7-7b5794115314",
                    "MPAN" => $users_mpan,
                    "CurrentMeterOnly" => "True"
                ];

                $response_getmeterdetails = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetMpanMeterDetails', $parameter, 'post');
                $response_getmeterdetails = $this->parseHttpResponse($response_getmeterdetails);


                $parameter_elec = json_encode(array(
                    "AuthKey" => "b450e45e-63b9-4c68-b1d7-7b5794115314",
                    "MPAN" => $users_mpan,
                    "readingDt" => date('Y-m-d'),
                    "Meters" => array(array(
                        "Registers" => array(array(
                            "Reading" => $this->input->post('elec_submit_reds')
                        ))
                    ))
                ));

            }

        }


        if ($this->input->post('signupType') == 0 || $this->input->post('signupType') == 2)  {// Junifer / Both

            $get_user_account_id = $this->session->userdata('login_data')['account_id'];
            $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/' . $get_user_account_id . '/agreements');
            $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);


            if (!empty($get_meterpoint_ids_responce)) {
                for ($r = 0; $r < count($get_meterpoint_ids_responce->results); $r++) {

                    if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

                        if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                            $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;

                        }

                    } elseif ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

                        if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                        }
                    }
                }}

            if ( $this->session->userdata('login_data')['meterTypeElec'] === 'E7') {

                $parameter =  json_encode(array(
                        "ignoreWarnings" => "false",
                        "readingDt" => date('Y-m-d'),
                        "sequenceType" => "Normal",
                        "source" => "Customer",
                        "quality" => "Normal",
                        "registerReads" => array(array(
                            "reading" => $this->input->post('elec_submit_reds'),
                            "readingType"=> "Day"),
                            array(
                                "reading" => $this->input->post('elec_submit_reds_two'),
                                "readingType"=> "Night",
                            )
                        )
                    )
                );

                // print_r( $parameter);


            }

            else {
                $parameter =  json_encode(array(
                        "ignoreWarnings" => "false",
                        "readingDt" => date('Y-m-d'),
                        "sequenceType" => "Normal",
                        "source" => "Customer",
                        "quality" => "Normal",
                        "registerReads" => array(array(
                            "reading" => $this->input->post('elec_submit_reds'),
                            "readingType"=> "Standard"
                        ))
                    )
                );
            }




            if ($_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '18.191.137.119')  //local
            {

                $curl_post_readings = curl_init();
                curl_setopt($curl_post_readings, CURLOPT_POSTFIELDS, $parameter);
                curl_setopt_array($curl_post_readings, array(
                    CURLOPT_PORT => "43002",

                    //junifer local
                    CURLOPT_URL => "http://134.213.125.150:44002/rest/v1/meterPoints/" . $electric_meter_point_id . "/readingsWithoutMtds",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",

                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        // local junifer
                        'X-Junifer-X-apikey: zH7bDom4',
                        'X-Junifer-X-username: apiuser1',
                    ),
                ));
            }


            if ($_SERVER['HTTP_HOST'] == '52.56.76.183' || $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk') //production
            {

                $curl_post_readings = curl_init();
                curl_setopt($curl_post_readings, CURLOPT_POSTFIELDS, $parameter);
                curl_setopt_array($curl_post_readings, array(
                    CURLOPT_PORT => "43002",

                    CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/meterPoints/" . $electric_meter_point_id . "/readingsWithoutMtds",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",

                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        // production junifer
                        'X-Junifer-X-apikey: dgiZaFDb',
                        'X-Junifer-X-username: api',
                    ),
                ));
            }


            $responsey = curl_exec($curl_post_readings);
            $err = curl_error($curl_post_readings);
            $responsey = json_decode($responsey);
            curl_close($curl_post_readings);

            $db_parameter = [
                'customer_id' => $this->session->userdata('login_data')['id'],
                'meter_reading' => $this->input->post('elec_submit_reds'),
                'fuel_type_id' => 2 // Elec
            ];


            if (isset($responsey->errorDescription)) {
                echo json_encode(['error' => '1', 'errorDescription' => $responsey->errorDescription]);
            } else {
                $electric_first_update = $this->user_modal->electric_first_update($this->session->userdata('login_data')['id']);
                $this->user_modal->log_meter_reading($db_parameter);
                echo json_encode(['error' => '0', 'updated' => $electric_first_update,]);
            }

        }

    }

    function submit_reading_gas()
    {

        if( $this->input->post('signupType') == 3 )
        {
            $parameter = [
                'AuthKey' => $this->authkey_dyball_serve,
                'AccountNumber' => 'DYB_ELEC_'.$this->session->userdata('login_data')['id'],
                'MeterSerialNumber' => date('Y-m-dTH:i:s'),
                'MPAN' => 'Normal',
                'ReadingDate' => date('Y-m-dTH:i:s'),
                'RegisterID1' => 'Normal',
                //'Reading1' => $this->input->post('elec_meter_submit')
                'Reading1' => '1111'
            ];

            //debug($parameter);
            $response = $this->curl->curl_dyball('/api/CustomerSelfService/SubmitElectricityReading', $parameter, 'post');
            //debug($response,1);
            $response = json_decode($response,true);
            echo json_encode([ 'error' => '0' ]);
        }

        if( $this->input->post('signupType') == 0 || $this->input->post('signupType') == 2 ) {

            $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;
            $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$get_user_account_id.'/agreements');
            $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);

            if (!empty($get_meterpoint_ids_responce)) {
                for ($r = 0; $r < count($get_meterpoint_ids_responce->results); $r++) {


                    if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

                        if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                            $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;

                        }
                    } elseif ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

                        if (isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)) {

                            $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id;

                        }
                    }
                }
            }

            $parameter =  json_encode(array(
                "ignoreWarnings" => "false",
                "readingDt" => date('Y-m-d'),
                "sequenceType" => "Normal",
                "source" => "Customer",
                "quality" => "Normal",
                "registerReads" => array(array(
                    "reading" => $this->input->post('gas_submit_reds'),
                    "readingType"=> "Standard"
                ))
            ));

            if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '18.191.137.119'  )  //local
            {
                $curl_post_readings = curl_init();
                curl_setopt( $curl_post_readings, CURLOPT_POSTFIELDS,  $parameter);
                curl_setopt_array($curl_post_readings, array(
                    CURLOPT_PORT => "43002",

                    //junifer local
                    CURLOPT_URL => "http://134.213.125.150:44002/rest/v1/meterPoints/" . $gas_meter_point_id . "/readingsWithoutMtds",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        // local junifer
                        'X-Junifer-X-apikey: zH7bDom4',
                        'X-Junifer-X-username: apiuser1',
                    ),
                ));
            }

            if ($_SERVER['HTTP_HOST'] == '52.56.76.183' || $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk') //production
            {
                $curl_post_readings = curl_init();
                curl_setopt($curl_post_readings, CURLOPT_POSTFIELDS, $parameter);
                curl_setopt_array($curl_post_readings, array(
                    CURLOPT_PORT => "43002",

                    //junifer production
                    CURLOPT_URL => "http://134.213.12.122:43002/rest/v1/meterPoints/" . $gas_meter_point_id . "/readingsWithoutMtds",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        // production junifer
                        'X-Junifer-X-apikey: dgiZaFDb',
                        'X-Junifer-X-username: api',
                    ),
                ));
            }

            $responsey = curl_exec($curl_post_readings);
            $err = curl_error($curl_post_readings);
            $responsey = json_decode($responsey);
            curl_close($curl_post_readings);

            $db_parameter = [
                'customer_id' => $this->session->userdata('login_data')['id'],
                'meter_reading' => $this->input->post('gas_submit_reds'),
                'fuel_type_id' => 1 // Gas
            ];


            if (isset($responsey->errorDescription)) {
                echo json_encode(['error' => '1', 'errorDescription' => $responsey->errorDescription]);
            } else {
                $gas_first_update = $this->user_modal->gas_first_update($this->session->userdata('login_data')['id']);
                $this->user_modal->log_meter_reading($db_parameter);
                echo json_encode(['error' => '0', 'updated' => $gas_first_update,]);
            }

        }

    }

    function usage()
    {
        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }


        $current_year = date('Y-m-d');
        $the_year = date('Y');
        $last_year = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));

        $fromdate_weekly = date( 'Y-m-d', strtotime( 'monday this week' ) );
        $todate_weekly = date( 'Y-m-d', strtotime( 'sunday this week' ) );
        //  $fromdate_weekly = '2018-07-20';
        // $todate_weekly = '2018-07-31';
        $fromdate_monthly = date('Y-m-01');        //date('Y-m-01')
        $todate_monthly = date('Y-m-01', strtotime("+1 month"));
        $fromdate_yearly = '2017-01-01';
        $todate_yearly = '2018-01-01';
        $jan_start_date = $the_year.'-01-01';
        $feb_start_date = $the_year.'-02-01';
        $march_start_date = $the_year.'-03-01';
        $april_start_date = $the_year.'-04-01';
        $may_start_date = $the_year.'-05-01';
        $june_start_date = $the_year.'-06-01';
        $july_start_date = $the_year.'-07-01';
        $august_start_date = $the_year.'-08-01';
        $september_start_date = $the_year.'-09-01';
        $october_start_date = $the_year.'-10-01';
        $november_start_date = $the_year.'-11-01';
        $december_start_date = $the_year.'-12-01';
        $december_end_date = $the_year.'-12-31';

        $get_tarrif = $this->session->userdata('login_data')['tariff_tariffName'];

        $get_electric_tarrif_rate = 0 ;
        $get_standardcharge_tarrif_electric = 0 ;

        $get_gas_tarrif_rate = 0 ;
        $get_standardcharge_tarrif_gas = 0 ;

        /*
            if ( $get_electric_tarrif === 'Simply Cred' )  {
              $get_electric_tarrif_rate = 0.1275000 ;
              $get_standardcharge_tarrif_electric = 0.1000000 ;

              $get_gas_tarrif_rate = 0.1275000 ;
              $get_standardcharge_tarrif_gas = 0.1000000 ;

            }
            elseif ( $get_electric_tarrif === 'Northern Glory' )  {
              $get_electric_tarrif_rate = 0.1220000 ;
              $get_standardcharge_tarrif_electric = 0.1000000 ;

              $get_gas_tarrif_rate = 0.0250000 ;
              $get_standardcharge_tarrif_gas = 0.1000000 ;
            }
            elseif ( $get_electric_tarrif === 'Fixed Smart Special' )  {
              $get_electric_tarrif_rate = 0.1115200 ;
              $get_standardcharge_tarrif_electric = 0.2100000 ;

              $get_gas_tarrif_rate = 0.0257000 ;
              $get_standardcharge_tarrif_gas = 0.2100000 ;
               }
            elseif ( $get_electric_tarrif === 'Fixed Smart' )  {
              $get_electric_tarrif_rate = 0.1115200 ;
              $get_standardcharge_tarrif_electric = 0.2100000 ;

              $get_gas_tarrif_rate = 0.0260000 ;
              $get_standardcharge_tarrif_gas = 0.2100000 ;
            }
            elseif ( $get_electric_tarrif === 'VariSmart' )  {
              $get_electric_tarrif_rate = 0.1230500 ;
              $get_standardcharge_tarrif_electric = 0.2000000 ;

              $get_gas_tarrif_rate = 0.0270000 ;
              $get_standardcharge_tarrif_gas = 0.2000000;
            }
            elseif ( $get_electric_tarrif === 'Goodbye standing charge' )  {
              $get_electric_tarrif_rate = 0.1471000 ;
              $get_standardcharge_tarrif_electric = 0.0000000 ;

              $get_gas_tarrif_rate = 0.0379050 ;
              $get_standardcharge_tarrif_gas = 0.2400000 ;
            }
            elseif ( $get_electric_tarrif === 'Our lowest Ever payg' )  {
              $get_electric_tarrif_rate = 0.1090000 ;
              $get_standardcharge_tarrif_electric = 0.2400000 ;

              $get_gas_tarrif_rate = 0.0361000;
              $get_standardcharge_tarrif_gas = 0.2400000 ;
            }
            elseif ( $get_electric_tarrif === 'Prepay Direct' )  {
              $get_electric_tarrif_rate = 0.1230000 ;
              $get_standardcharge_tarrif_electric = 0.2400000 ;

              $get_gas_tarrif_rate = 0.0361000 ;
              $get_standardcharge_tarrif_gas = 0.2400000 ;
            }
            elseif ( $get_electric_tarrif === 'Standard Variable' )  {
              $get_electric_tarrif_rate = 0.1050000 ;
              $get_standardcharge_tarrif_electric = 0.2750000 ;

              $get_gas_tarrif_rate =0.0181000 ;
              $get_standardcharge_tarrif_gas = 0.2750000 ;
            }
        */

        $parameter_find_tarif = [ 'supplierName' => 'Eversmart Energy', 'tariffName' => $get_tarrif,  'fuelType' => 'elecgas', "datesToUse" => "regulatory-dates" ];
        $eneryLinx_find_tarif = $this->curl->curl_request('/v3/partner-resources/EVERSMARTENERGYSUPP/tariffs-with-price-data/', $parameter_find_tarif , 'post');
        $eneryLinx_find_tarif = $this->parseHttpResponse($eneryLinx_find_tarif);


        if(
            $eneryLinx_find_tarif['body']['statusCode'] == '1035' ){
            $get_standardcharge_tarrif_gas = $eneryLinx_find_tarif['body']['data']['tariffs'][0]['prices'][0]['standingChargeGas'];
            $get_standardcharge_tarrif_electric = $eneryLinx_find_tarif['body']['data']['tariffs'][0]['prices'][0]['standingChargeElec'];
            $get_gas_tarrif_rate= $eneryLinx_find_tarif['body']['data']['tariffs'][0]['prices'][0]['unitRate1Gas'];
            $get_electric_tarrif_rate = $eneryLinx_find_tarif['body']['data']['tariffs'][0]['prices'][0]['unitRate1Elec'];
        }



        if( $this->session->userdata('login_data')['signup_type'] == '0' || $this->session->userdata('login_data')['signup_type'] == '2' )
        {
            /*
             print_r ($electric_meter_point_id);
             $user_meterpoint_id = 542;
             $get_user_account_id = 223;
             $get_meterpoint_ids = 32323
            */


            $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;
            $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$get_user_account_id.'/agreements');
            $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);


            //print_r($get_meterpoint_ids_responce);


            if( !empty($get_meterpoint_ids_responce->results) ){
                for( $r = 0; $r<count( $get_meterpoint_ids_responce->results ) ; $r++ ){


                    if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply') {

                        if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

                            $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

                        }
                    }

                    elseif ( $get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply') {

                        if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id)){

                            $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;

                        }
                    }
                }}


            if (isset($electric_meter_point_id)) {

                $el_response = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$fromdate_weekly.'&toDt='.$todate_weekly.'');
                $el_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
                $el_response_jan_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$jan_start_date.'&toDt='.$feb_start_date.'');
                $el_response_feb_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$feb_start_date.'&toDt='.$march_start_date.'');
                $el_response_march_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$march_start_date.'&toDt='. $april_start_date.'');
                $el_response_april_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$april_start_date.'&toDt='.$may_start_date.'');
                $el_response_may_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$may_start_date.'&toDt='.$june_start_date.'');
                $el_response_june_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$june_start_date.'&toDt='.$july_start_date.'');
                $el_response_july_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$july_start_date.'&toDt='.$august_start_date.'');
                $el_response_august_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$august_start_date.'&toDt='.$september_start_date.'');
                $el_response_september_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$september_start_date.'&toDt='.$october_start_date.'');
                $el_response_october_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$october_start_date.'&toDt='.$november_start_date.'');
                $el_response_november_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$november_start_date.'&toDt='.$december_start_date.'');
                $el_response_december_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$december_start_date.'&toDt='. $december_end_date.'');

                $el_response = json_decode($el_response,true);

                $el_response_jan_array = json_decode($el_response_jan_array);
                $el_response_feb_array = json_decode($el_response_feb_array);
                $el_response_march_array = json_decode($el_response_march_array);
                $el_response_april_array = json_decode($el_response_april_array);
                $el_response_may_array = json_decode($el_response_may_array);
                $el_response_june_array = json_decode($el_response_june_array);
                $el_response_july_array = json_decode($el_response_july_array);
                $el_response_august_array = json_decode($el_response_august_array);
                $el_response_september_array = json_decode($el_response_september_array);
                $el_response_october_array = json_decode($el_response_october_array);
                $el_response_november_array = json_decode($el_response_november_array);
                $el_response_december_array = json_decode($el_response_december_array);
                $el_response_monthly_array = json_decode($el_response_monthly_array);

                if(isset($el_response_monthly_array )) {
                    $el_monthly_split = array_chunk($el_response_monthly_array->results,7, true);
                    $data['el_monthly_split'] =  $el_monthly_split;
                }

                $data['el_results'] = $el_response['results'];


                $data['el_response_jan_array'] =  $el_response_jan_array;
                $data['el_response_feb_array'] =  $el_response_feb_array;
                $data['el_response_march_array'] =  $el_response_march_array;
                $data['el_response_april_array'] =  $el_response_april_array;
                $data['el_response_may_array'] =  $el_response_may_array;
                $data['el_response_june_array'] =  $el_response_june_array;
                $data['el_response_july_array'] =  $el_response_july_array;
                $data['el_response_august_array'] =  $el_response_august_array ;
                $data['el_response_september_array'] =  $el_response_september_array;
                $data['el_response_october_array'] =  $el_response_october_array;
                $data['el_response_november_array'] =  $el_response_november_array;
                $data['el_response_december_array'] =  $el_response_december_array;



            }

            //////// gas

            if (isset($gas_meter_point_id)) {
                $gas_response = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$fromdate_weekly.'&toDt='.$todate_weekly.'');
                $gas_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
                $gas_response_jan_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$jan_start_date.'&toDt='.$feb_start_date.'');
                $gas_response_feb_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$feb_start_date.'&toDt='.$march_start_date.'');
                $gas_response_march_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$march_start_date.'&toDt='. $april_start_date.'');
                $gas_response_april_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$april_start_date.'&toDt='.$may_start_date.'');
                $gas_response_may_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$may_start_date.'&toDt='.$june_start_date.'');
                $gas_response_june_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$june_start_date.'&toDt='.$july_start_date.'');
                $gas_response_july_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$july_start_date.'&toDt='.$august_start_date.'');
                $gas_response_august_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$august_start_date.'&toDt='.$september_start_date.'');
                $gas_response_september_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$september_start_date.'&toDt='.$october_start_date.'');
                $gas_response_october_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$october_start_date.'&toDt='.$november_start_date.'');
                $gas_response_november_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$november_start_date.'&toDt='.$december_start_date.'');
                $gas_response_december_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$december_start_date.'&toDt='. $december_end_date.'');

                $gas_response = json_decode($gas_response,true);
                $gas_response_jan_array = json_decode($gas_response_jan_array);
                $gas_response_feb_array = json_decode($gas_response_feb_array);
                $gas_response_march_array = json_decode($gas_response_march_array);
                $gas_response_april_array = json_decode($gas_response_april_array);
                $gas_response_may_array = json_decode($gas_response_may_array);
                $gas_response_june_array = json_decode($gas_response_june_array);
                $gas_response_july_array = json_decode($gas_response_july_array);
                $gas_response_august_array = json_decode($gas_response_august_array);
                $gas_response_september_array = json_decode($gas_response_september_array);
                $gas_response_october_array = json_decode($gas_response_october_array);
                $gas_response_november_array = json_decode($gas_response_november_array);
                $gas_response_december_array = json_decode($gas_response_december_array);
                $gas_response_monthly_array = json_decode($gas_response_monthly_array);

                if (isset($gas_response_monthly_array)){
                    $gas_monthly_split = array_chunk($gas_response_monthly_array->results,7, true);
                }

                $data['gas_results'] = $gas_response['results'];

                if (isset($gas_monthly_split )){
                    $data['gas_monthly_split'] =  $gas_monthly_split;
                }
                $data['gas_response_jan_array'] =  $gas_response_jan_array;
                $data['gas_response_feb_array'] =  $gas_response_feb_array;
                $data['gas_response_march_array'] =  $gas_response_march_array;
                $data['gas_response_april_array'] =  $gas_response_april_array;
                $data['gas_response_may_array'] =  $gas_response_may_array;
                $data['gas_response_june_array'] =  $gas_response_june_array;
                $data['gas_response_july_array'] =  $gas_response_july_array;
                $data['gas_response_august_array'] =  $gas_response_august_array ;
                $data['gas_response_september_array'] =  $gas_response_september_array;
                $data['gas_response_october_array'] =  $gas_response_october_array;
                $data['gas_response_november_array'] =  $gas_response_november_array;
                $data['gas_response_december_array'] =  $gas_response_december_array;
            }
//var_dump($gas_response['results']);

        }

        if( $this->session->userdata('login_data')['signup_type'] == '3' )
        {


            $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );
            $users_mprn =  $login_response['gasProduct_mprns']; //  '1498085706' ;  //
            $users_mpan = $login_response['elec_mpan_core']; // '1610009348896' ;

//var_dump( $users_mpan);


            if (isset( $users_mprn )) {
                $parameter_gas_daily = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $fromdate_weekly, 'EndDate' =>  $todate_weekly ];
                $parameter_gas_weekly = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $fromdate_monthly, 'EndDate' =>  $todate_monthly];
                $parameter_gas_monthly = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' =>  $fromdate_monthly, 'EndDate' => $todate_monthly];
                $parameter_gas_jan = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $jan_start_date, 'EndDate' => $feb_start_date];
                $parameter_gas_feb = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $feb_start_date, 'EndDate' => $march_start_date];
                $parameter_gas_march = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $march_start_date, 'EndDate' => $april_start_date];
                $parameter_gas_april = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $april_start_date, 'EndDate' =>  $may_start_date ];
                $parameter_gas_may = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' =>  $may_start_date, 'EndDate' =>  $june_start_date];
                $parameter_gas_june = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' =>  $june_start_date, 'EndDate' =>  $july_start_date];
                $parameter_gas_july = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $july_start_date, 'EndDate' => $august_start_date];
                $parameter_gas_august = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $august_start_date, 'EndDate' => $september_start_date];
                $parameter_gas_september = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $september_start_date, 'EndDate' =>  $october_start_date];
                $parameter_gas_october = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' =>  $october_start_date, 'EndDate' => $november_start_date];
                $parameter_gas_november = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $november_start_date, 'EndDate' => $december_start_date];
                $parameter_gas_december = [ 'AuthKey' => 'ed3e6b7a-ca3d-402b-b353-cc83ca121912', 'Mprn' => $users_mprn, 'StartDate' => $december_start_date, 'EndDate' =>  $december_end_date];



                $dyball_gas_readings = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_daily , 'post');
                $dyball_gas_readings = $this->parseHttpResponse($dyball_gas_readings);

                //var_dump($dyball_gas_readings );

                if( $dyball_gas_readings['headers']['http_code'] == 'HTTP/1.1 200 OK' ){

                    if (isset($dyball_gas_readings['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_daily'] = $dyball_gas_readings['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_daily'] = [] ; }
                }


                $dyball_gas_readings_weekly = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_weekly , 'post');
                $dyball_gas_readings_weekly = $this->parseHttpResponse($dyball_gas_readings_weekly);

                if( $dyball_gas_readings_weekly['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_gas_readings_weekly['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_weekly'] = $dyball_gas_readings_weekly['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_weekly'] = [] ; }
                }

                $dyball_gas_readings_monthly = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings',  $parameter_gas_monthly , 'post');
                $dyball_gas_readings_monthly = $this->parseHttpResponse($dyball_gas_readings_monthly);

                if(
                    $dyball_gas_readings_monthly['headers']['http_code'] == 'HTTP/1.1 200 OK' ){

                    if (isset($dyball_gas_readings_monthly['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $dyball_gas_readings_monthly = $dyball_gas_readings_monthly['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                        $dyball_gas_readings_monthly = array_chunk($dyball_gas_readings_monthly,7, true);
                        $data['gas_readings_monthly'] = $dyball_gas_readings_monthly;
                    }
                    else {
                        $data['gas_readings_monthly'] = [] ; }
                }

                $dyball_gas_readings_jan = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_jan , 'post');
                $dyball_gas_readings_jan = $this->parseHttpResponse($dyball_gas_readings_jan);

                if(
                    $dyball_gas_readings_jan['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_gas_readings_jan['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_jan'] = $dyball_gas_readings_jan['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_jan']  = [] ; }
                }

                $dyball_gas_readings_feb = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings',  $parameter_gas_feb  , 'post');
                $dyball_gas_readings_feb = $this->parseHttpResponse($dyball_gas_readings_feb);

                if(
                    $dyball_gas_readings_feb['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_gas_readings_feb['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_feb'] = $dyball_gas_readings_feb['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_feb']  = [] ; }
                }


                $dyball_gas_readings_march = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_march , 'post');
                $dyball_gas_readings_march = $this->parseHttpResponse($dyball_gas_readings_march);

                if(
                    $dyball_gas_readings_march['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_gas_readings_feb['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_march'] = $dyball_gas_readings_march['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_march']  = [] ; }
                }

                $dyball_gas_readings_april = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings',  $parameter_gas_april , 'post');
                $dyball_gas_readings_april = $this->parseHttpResponse($dyball_gas_readings_april);

                if(
                    $dyball_gas_readings_april['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_gas_readings_april['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_april'] = $dyball_gas_readings_april['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_april']  = [] ; }
                }

                $dyball_gas_readings_may = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_may , 'post');
                $dyball_gas_readings_may = $this->parseHttpResponse($dyball_gas_readings_may);

                if(
                    $dyball_gas_readings_may['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_may['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_may'] = $dyball_gas_readings_may['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_may']  = [] ; }
                }

                $dyball_gas_readings_june = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings',  $parameter_gas_june , 'post');
                $dyball_gas_readings_june = $this->parseHttpResponse($dyball_gas_readings_june);

                if(
                    $dyball_gas_readings_june['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_june['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_june'] = $dyball_gas_readings_june['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_june']  = [] ; }
                }


                $dyball_gas_readings_july = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_july, 'post');
                $dyball_gas_readings_july = $this->parseHttpResponse($dyball_gas_readings_july);

                if(
                    $dyball_gas_readings_july['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_july['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_july'] = $dyball_gas_readings_july['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_july']  = [] ; }
                }

                $dyball_gas_readings_august = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_august , 'post');
                $dyball_gas_readings_august = $this->parseHttpResponse($dyball_gas_readings_august);

                if(
                    $dyball_gas_readings_august['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_august['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_august'] = $dyball_gas_readings_august['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_august']  = [] ; }
                }


                $dyball_gas_readings_septemeber = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_september , 'post');
                $dyball_gas_readings_septemeber = $this->parseHttpResponse($dyball_gas_readings_septemeber);

                if(
                    $dyball_gas_readings_septemeber['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_septemeber['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_septemeber'] = $dyball_gas_readings_septemeber['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_septemeber']  = [] ; }
                }


                $dyball_gas_readings_october = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_october , 'post');
                $dyball_gas_readings_october = $this->parseHttpResponse($dyball_gas_readings_october);

                if(
                    $dyball_gas_readings_october['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_october['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_october'] = $dyball_gas_readings_october['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_october']  = [] ; }
                }


                $dyball_gas_readings_november = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_november , 'post');
                $dyball_gas_readings_november = $this->parseHttpResponse($dyball_gas_readings_november);

                if(
                    $dyball_gas_readings_november['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_november['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_november'] = $dyball_gas_readings_november['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_november']  = [] ; }
                }

                $dyball_gas_readings_december = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/gasmanager/getreadings', $parameter_gas_december , 'post');
                $dyball_gas_readings_december = $this->parseHttpResponse($dyball_gas_readings_december);

                if(
                    $dyball_gas_readings_december['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_gas_readings_december['body']['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                        $data['gas_readings_december'] = $dyball_gas_readings_december['body']['Mprns'][0]['MprnMeters'][0]['Readings'];
                    }
                    else {
                        $data['gas_readings_december']  = [] ; }
                }

            }


            if (isset($users_mpan)){
                $parameter_electric_daily = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $fromdate_weekly, 'EndDate' =>  $todate_weekly ];
                $parameter_electric_weekly = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $fromdate_monthly, 'EndDate' =>  $todate_monthly];
                $parameter_electric_monthly = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' =>  $fromdate_monthly, 'EndDate' => $todate_monthly];
                $parameter_electric_jan = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $jan_start_date, 'EndDate' => $feb_start_date];
                $parameter_electric_feb = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $feb_start_date, 'EndDate' => $march_start_date];
                $parameter_electric_march = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan,  'StartDate' => $march_start_date, 'EndDate' => $april_start_date];
                $parameter_electric_april = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $april_start_date, 'EndDate' =>  $may_start_date ];
                $parameter_electric_may = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' =>  $may_start_date, 'EndDate' =>  $june_start_date];
                $parameter_electric_june = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' =>  $june_start_date, 'EndDate' =>  $july_start_date];
                $parameter_electric_july = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $july_start_date, 'EndDate' => $august_start_date];
                $parameter_electric_august = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan,  'StartDate' => $august_start_date, 'EndDate' => $september_start_date];
                $parameter_electric_september = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $september_start_date, 'EndDate' =>  $october_start_date];
                $parameter_electric_october = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $september_start_date, 'EndDate' =>  $october_start_date];
                $parameter_electric_november = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan,  'StartDate' => $november_start_date, 'EndDate' => $december_start_date];
                $parameter_electric_december = [ 'AuthKey' => 'b450e45e-63b9-4c68-b1d7-7b5794115314', 'MPAN' => $users_mpan, 'StartDate' => $december_start_date, 'EndDate' =>  $december_end_date];


                $dyball_electric_readings = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_daily , 'post');
                $dyball_electric_readings = $this->parseHttpResponse($dyball_electric_readings);

                if(
                    $dyball_electric_readings['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_daily'] = $dyball_electric_readings['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_daily']  = [] ; }
                }


                $dyball_electric_readings_weekly = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_weekly , 'post');
                $dyball_electric_readings_weekly = $this->parseHttpResponse($dyball_electric_readings_weekly);

                if(
                    $dyball_electric_readings_weekly['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_weekly['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_weekly'] = $dyball_electric_readings_weekly['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_weekly']  = [] ; }
                }

                $dyball_electric_readings_monthly = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_monthly, 'post');
                $dyball_electric_readings_monthly = $this->parseHttpResponse($dyball_electric_readings_monthly);

                if(
                    $dyball_electric_readings_monthly['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_monthly['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $dyball_electric_readings_monthly = $dyball_electric_readings_monthly['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                        $dyball_electric_readings_monthly = array_chunk($dyball_electric_readings_monthly,7, true);
                        $data['electric_readings_monthly'] = $dyball_electric_readings_monthly;
                    }
                    else {
                        $data['electric_readings_monthly']  = [] ; }
                }


                $dyball_electric_readings_jan = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_jan , 'post');
                $dyball_electric_readings_jan = $this->parseHttpResponse($dyball_electric_readings_jan);

                if(
                    $dyball_electric_readings_jan['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_jan['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_jan'] = $dyball_electric_readings_jan['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_jan']  = [] ; }
                }

                $dyball_electric_readings_feb = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_feb , 'post');
                $dyball_electric_readings_feb = $this->parseHttpResponse($dyball_electric_readings_feb);

                if(
                    $dyball_electric_readings_feb['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_electric_readings_feb['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_feb'] = $dyball_electric_readings_feb['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_feb']  = [] ; }
                }

                $dyball_electric_readings_march = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_march , 'post');
                $dyball_electric_readings_march = $this->parseHttpResponse($dyball_electric_readings_march);

                if(
                    $dyball_electric_readings_march['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_march['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_march'] = $dyball_electric_readings_march['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_march']  = [] ; }
                }

                $dyball_electric_readings_april = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_april , 'post');
                $dyball_electric_readings_april = $this->parseHttpResponse($dyball_electric_readings_april);

                if(
                    $dyball_electric_readings_april['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_april['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_april'] = $dyball_electric_readings_april['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_april']  = [] ; }
                }

                $dyball_electric_readings_may = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_may , 'post');
                $dyball_electric_readings_may = $this->parseHttpResponse($dyball_electric_readings_may);


                if(
                    $dyball_electric_readings_may['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_may['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_may'] = $dyball_electric_readings_may['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_may']  = [] ; }
                }

                $dyball_electric_readings_june = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_june , 'post');
                $dyball_electric_readings_june = $this->parseHttpResponse($dyball_electric_readings_june);

                if(
                    $dyball_electric_readings_june['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_june['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_june'] = $dyball_electric_readings_june['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_june']  = [] ; }
                }

                $dyball_electric_readings_july = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_july , 'post');
                $dyball_electric_readings_july = $this->parseHttpResponse($dyball_electric_readings_july);

                if(
                    $dyball_electric_readings_july['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_electric_readings_july['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_july'] = $dyball_electric_readings_july['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_july']  = [] ; }
                }

                $dyball_electric_readings_august = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_august  , 'post');
                $dyball_electric_readings_august = $this->parseHttpResponse($dyball_electric_readings_august);

                if(
                    $dyball_electric_readings_august['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_august['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_august'] = $dyball_electric_readings_august['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_august']  = [] ; }
                }

                $dyball_electric_readings_septemeber = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_september , 'post');
                $dyball_electric_readings_septemeber = $this->parseHttpResponse($dyball_electric_readings_septemeber);

                if(
                    $dyball_electric_readings_septemeber['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_septemeber['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_septemeber'] = $dyball_electric_readings_septemeber['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_septemeber']  = [] ; }
                }

                $dyball_electric_readings_october = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_october , 'post');
                $dyball_electric_readings_october = $this->parseHttpResponse($dyball_electric_readings_october);

                if(
                    $dyball_electric_readings_october['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset($dyball_electric_readings_october['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_october'] = $dyball_electric_readings_october['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_october']  = [] ; }
                }

                $dyball_electric_readings_november = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_november, 'post');
                $dyball_electric_readings_november = $this->parseHttpResponse($dyball_electric_readings_november);

                if(
                    $dyball_electric_readings_november['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_november['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_november'] = $dyball_electric_readings_november['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_november']  = [] ; }
                }

                $dyball_electric_readings_december = $this->curl->curlPost('https://engq.dyballapi.co.uk/api/RegManager/GetReadings', $parameter_electric_december , 'post');
                $dyball_electric_readings_december = $this->parseHttpResponse($dyball_electric_readings_december);

                if(
                    $dyball_electric_readings_december['headers']['http_code'] == 'HTTP/1.1 200 OK' ){
                    if (isset( $dyball_electric_readings_december['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'])) {
                        $data['electric_readings_december'] = $dyball_electric_readings_december['body']['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                    }
                    else {
                        $data['electric_readings_december']  = [] ; }
                }
            }

        }


        header('content-type: text/html');

        $data['get_electric_tarrif_rate']  = $get_electric_tarrif_rate ;
        $data['get_standardcharge_tarrif_electric']  = $get_standardcharge_tarrif_electric ;

        $data['get_gas_tarrif_rate']  =  $get_gas_tarrif_rate  ;
        $data['get_standardcharge_tarrif_gas']  = $get_standardcharge_tarrif_gas ;

        $data['session_data'] = $this->session->userdata('login_data');
        //$data['last_reading'] = end($response['results']);
        $data['content'] = 'dashboard/energy_usage';
        $this->load->view('layout/dashboard_master',$data);
    }

    function billing()
    {
        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );

        if( $login_response['signup_type'] == 0 || $login_response['signup_type'] == 2 ){

            $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;

            $response = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$get_user_account_id.'/bills');
            //$image_download = $this->curl->junifer_request_test('/rest/v1/billFiles/7428');
            //$image_download = json_decode($image_download,true);

            //debug($image_download,1);
            if( !empty($image_download) ){
                $data['image_download'] = $image_download['links']['self'];
            }

            $response = json_decode($response,true);

            // print_r($response['results']);
        }

        header('content-type: text/html');
        if( !empty($response) ){
            if(isset($response['results'])) {
                $data['results'] = $response['results'];
            }
            else {
                $data['results'] = [];
            }
        }
        //debug($data,1);
        $data['content'] = 'dashboard/billing';
        $data['title'] = 'Billing';
        $this->load->view('layout/dashboard_master',$data);
    }

    function payments()
    {
        //debug($this->session->userdata('login_data'),1 );
        // for live
        //$get_payment_junifer = $this->curl->junifer_request_test('/rest/v1/accounts/'.$this->session->userdata('login_data')['customers'].'/paymentscheduleperiods');

        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        $data['payment_date'] = 0 ;
        $data['payment_amount'] = 0 ;
        $data['balance'] = 0;

        if( $this->session->userdata('login_data')['signup_type'] == '0' || $this->session->userdata('login_data')['signup_type'] == '2' ){

            $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;

            $get_payment_junifer = $this->curl->junifer_request('/rest/v1/accounts/'.$get_user_account_id.'/paymentSchedulePeriods/');
            $get_payment_junifer =  $this->parseHttpResponse($get_payment_junifer);

            if(  isset($get_payment_junifer['headers']['http_code']) && $get_payment_junifer['headers']['http_code'] == 'HTTP/1.1 200 OK') {

                $data['payment_junifer'] = $get_payment_junifer['body']['results'];

                if (isset($get_payment_junifer['body']['results'][0]['nextPaymentDt']) ) {

                    $payment_date = $get_payment_junifer['body']['results'][0]['nextPaymentDt'];
                    $payment_amount = $get_payment_junifer['body']['results'][0]['amount'];
                    $data['payment_amount'] =  $payment_amount ;

                    $payment_date = date("d", strtotime($payment_date));
                    //$nf = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
                    //$payment_date = $nf->format($payment_date);
                    $data['payment_date'] =  $payment_date ;
                }
            }

            $get_balance_junifer = $this->curl->junifer_request('/rest/v1/accounts/'.$get_user_account_id.'');
            $get_balance_junifer = $this->parseHttpResponse($get_balance_junifer);



            if(  isset($get_balance_junifer['headers']['http_code']) && $get_balance_junifer['headers']['http_code'] == 'HTTP/1.1 200 OK') {
                $data['balance'] = $get_balance_junifer['body']['balance'] ;

            }

        }

        $monthly_interest = $this->user_modal->get_monthly_interest_date($this->session->userdata('login_data')['id']);
        $AccruedInterestData = $this->calculate_total_customer_interest($this->session->userdata('login_data')['id']);
        $data['accrued_interest_data'] = $this->get_customer_interest_data($this->session->userdata('login_data')['id']);
        $data['accrued_interest'] = $AccruedInterestData['accrued_interest'];
        $data['signup_date'] = $AccruedInterestData['signup_date'];
        $data['tariff'] = $this->session->userdata('login_data')['tariff_tariffName'];
        $data['tariff_newspend'] = $this->session->userdata('login_data')['tariff_newspend'];
        $data['go_live_date'] = $monthly_interest['go_live_date'];
        $data['monthly_interest'] = $monthly_interest['monthly_interest_customer_signup_date'];

        if( $this->session->userdata('login_data')['signup_type'] == '3' ){
            //for test
            $parameter = [ 'AuthKey' => $this->authkey, 'AccountNumber' => '12434' ];
            $get_payment_junifer = $this->curl->junifer_request_test('/customerselfservice/getcustomerbalance', $parameter, 'post');
            $get_payment_junifer = json_decode($get_payment_junifer,true);
            $data['title'] = 'Payments';
            $data['payment_junifer'] = $get_payment_junifer;
        }
        header('content-type: text/html');
        //debug($data,1);
        $data['content'] = 'dashboard/payments';
        $this->load->view('layout/dashboard_master',$data);
    }

    function moving_home()
    {
        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }
        $data['content'] = 'dashboard/moving_home';
        $data['title'] = 'Moving Home';
        $this->load->view('layout/dashboard_master',$data);
    }

    function save_moving_home()
    {
        $session_data = $this->session->userdata('login_data');

        $mov_date = $this->input->get('date');
        $mov_elec_read = $this->input->get('elec_submit_reds');
        $mov_gas_read = $this->input->get('gas_submit_reds');
        $mov_address = $this->input->get('new_address');

        $conf = $mov_date . ', ' . $mov_elec_read . ', ' . $mov_gas_read . ', ' . $mov_address;

        //send mail

        if(!empty($mov_date) && !empty($mov_elec_read) && !empty($mov_gas_read) && !empty($mov_address))
        {
            $token = md5(uniqid(rand(0,9999), true));
            $email_data['email_info'] = [ 'token' => $token, 'name' => $session_data['first_name']. ' ' . $session_data['last_name'] ];

            $template = $this->load->view( 'layout/email_template', $email_data , true);

            $this->load->library('email');
            $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
            $this->email->to($session_data['email']);
            $this->email->subject('Moving Home Confirmation');
            $this->email->message($template);
            $this->email->send();

            echo json_encode([ 'success' => 'success', 'msg' => 'Moving Home request successful' ]);
        }
        else
        {
            echo json_encode([ 'success' => 'fail', 'msg' => 'Email failed to send' ]);
        }
    }

    function change_password()
    {
        //echo "current date time ".date('Y-m-d H:i:s') ;
        $token = $this->input->get('t');
        if( empty($token) ) {
            $data = [ 'error' => '1', 'msg' => 'Token required for reset password' ];
        }
        else {

            // check reset password token in database
            $check_token = $this->user_modal->check_token($token);

            //check for token type
            if( $check_token['token_type'] == '1' ){

                //check for token expiry
                $curret_date = date( 'Y-m-d H:i:s' );
                $token_date = $check_token['expire_date'];

                //Convert them to timestamps.
                $date1Timestamp = strtotime($token_date);
                $date2Timestamp = strtotime($curret_date);

                //Calculate the difference.
                $difference = $date2Timestamp - $date1Timestamp;

                if( $difference > 0 )
                {
                    $data = [ 'error' => '2', 'msg' => 'Your password reset has expired. Select \'Forgotten password?\' below and we’ll get right on it!' ];
                }
                else{
                    $data = [ 'error' => '0', 'msg' => '', 'user_id' => $check_token['customer_id'] ];
                }

            }
            else if( isset($_REQUEST['t']) && $_REQUEST['t'] !='' && isset($_REQUEST['ev']) && $_REQUEST['ev'] !='') {

                // If we have everything we need for password reset in redfish
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($_REQUEST['t']).'&ev='.urlencode($_REQUEST['ev']),
                    CURLOPT_HTTPHEADER => array(
                        'Api-Key: '.$this->Api_Key,
                        'Api-Type: Website',
                        'Client-IP: '.$this->Client_ip,
                        'Content-Type:application/json',
                        'Accept: application/json',
                        'Client-UA:'.$this->client_agent,
                    ),
                ));
                $Elec_result = curl_exec($curl);
                $elec_result_decode = json_decode($Elec_result);

                if($elec_result_decode->Message == 'SUCCESS')
                {
                    $data = [ 'error' => '0', 'msg' => '', 'user_id' => '', 'email' => $elec_result_decode->Data->Email];
                }
                else
                {
                    $data = [ 'error' => '2', 'msg' => 'Your password reset has expired. Select \'Forgotten password?\' below and we’ll get right on it!' ];
                }

            }
            else {

                // Are we doing a redfish activate?
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $this->Api_Url.'CreatePassword/?t='.urlencode($_REQUEST['t']),
                    CURLOPT_HTTPHEADER => array(
                        'Api-Key: '.$this->Api_Key,
                        'Api-Type: Website',
                        'Client-IP: '.$this->Client_ip,
                        'Content-Type:application/json',
                        'Accept: application/json',
                        'Client-UA:'.$this->client_agent,
                    ),
                ));
                $Activate_result = curl_exec($curl);
                $activate_result_decode = json_decode($Activate_result);

                if($activate_result_decode->Message == 'SUCCESS')
                {
                    $data = [ 'error' => '0', 'msg' => '', 'user_id' => '', 'email' => $activate_result_decode->Data->Email];
                }
                else
                {
                    $data = [ 'error' => '1', 'msg' => 'Your account activation has expired.<br>Please drop us an e-mail at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to let us know and we’ll get right on it!' ];
                }

            }
        }
        //debug($data,1);
        $this->load->view('user/reset-two',$data);
    }

    function reset_confirmation()
    {
        $this->load->view('user/reset-confirmation');
    }

    function customer_reset_password()
    {

        //debug( $this->input->post() );

        if( $this->input->post('user_id') > 0 ) {

            $info = $this->input->post();
            $change_password = $this->user_modal->customer_change_password($info);
            if ($change_password) {
                echo json_encode(['error' => '0', 'msg' => 'Password change successful']);
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Server error while changing password']);
            }
        }
        else {

            // Go check Redfish

            $reading_parameter['Email'] = $this->input->post('email');
            $reading_parameter['Password'] = $this->input->post('password');
            $reading_parameter['ConfirmPassword'] = $this->input->post('confirm_password');

            //die();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1,
                CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($this->input->post('t')).'&ev='.urlencode($this->input->post('ev')),
                CURLOPT_HTTPHEADER => array(
                    'Api-Key: '.$this->Api_Key,
                    'Api-Type: Website',
                    'Client-IP: '.$this->Client_ip,
                    'Content-Type:application/json',
                    'Accept: application/json',
                    'Client-UA:'.$this->client_agent,
                ),
                CURLOPT_POSTFIELDS => json_encode($reading_parameter),
            ));
            $Elec_result = curl_exec($curl);
            $elec_result_decode = json_decode($Elec_result);

            if($elec_result_decode->Message == 'SUCCESS')
            {

                // Check our database and make sure it matches Redfish
                $customer_exists = $this->user_modal->email_check($reading_parameter['Email']);

                if(isset($customer_exists['id'])){

                    $value = [
                        'user_id' => $customer_exists['id'],
                        'confirm_password' => $reading_parameter['ConfirmPassword']
                    ];

                    $this->user_modal->customer_change_password($value);
                }

                echo json_encode(['error' => '0', 'msg' => 'Password change successful']);

            } else {
                echo json_encode(['error' => '1', 'msg' => 'Server error while changing password']);
            }

        }
    }

    /**
     * Update password in our database. Ff the ev param is set, we know it is a RF account so handle need to handle this
     *
     * @param null
     * @return string - JSON array
     */
    function update_password()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: https://portal.eversmartenergy.co.uk");
        header("Access-Control-Allow-Headers: Content-Type, Accept, Origin, Referer, User-Agent");
        header("Access-Control-Allow-Methods: GET, POST");
        header("X-Content-Type-Options: nosniff");
        header("Content-Type: text/json");

        $data = json_decode( file_get_contents( 'php://input' ), true );

        // If there is no ev - the customer should be in our DB (ev comes from Redfish)
        if( !isset($data['ev']) || $data['ev'] == ''){

            // All we need is the customer id to update the DB record
            $customer = $this->user_modal->email_check($data['email']);

            $password_data = [
                'user_id' => $customer['id'],
                'confirm_password' => $data['confirmPassword']
            ];

            $change_password = $this->user_modal->customer_change_password($password_data);

            if ($change_password) {
                echo json_encode(['success' => true, 'msg' => 'Password updated successfully.']);
            } else {
                echo json_encode(['success' => false, 'msg' => 'Sorry, there was an error whilst updating your password.']);
            }

        } else {

            // Go get email from Redfish
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($data['token']).'&ev='.urlencode($data['ev']),
                CURLOPT_HTTPHEADER => array(
                    'Api-Key: '.$this->Api_Key,
                    'Api-Type: Website',
                    'Client-IP: '.$this->Client_ip,
                    'Content-Type:application/json',
                    'Accept: application/json',
                    'Client-UA:'.$this->client_agent,
                ),
            ));
            $get_email = curl_exec($curl);
            $get_email_decode = json_decode($get_email);

            // If we got a successful response from Redfish...update password
            if(isset($get_email_decode->Message) && $get_email_decode->Message == 'SUCCESS') {

                $parameter = [
                    'Email' => $get_email_decode->Data->Email,
                    'Password' => $data['password'],
                    'ConfirmPassword' => $data['confirmPassword']
                ];

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_POST => 1,
                    CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($data['token']).'&ev='.urlencode($data['ev']),
                    CURLOPT_HTTPHEADER => array(
                        'Api-Key: '.$this->Api_Key,
                        'Api-Type: Website',
                        'Client-IP: '.$this->Client_ip,
                        'Content-Type:application/json',
                        'Accept: application/json',
                        'Client-UA:'.$this->client_agent,
                    ),
                    CURLOPT_POSTFIELDS => json_encode($parameter)
                ));
                $password_update = curl_exec($curl);
                $password_update_decode = json_decode($password_update);

                if(isset($password_update_decode->Message) && $password_update_decode->Message == 'SUCCESS') {

                    // Also need to check our database to see if the customer exists here as well.
                    // If they do, make sure password matches that in Redfish
                    $customer_exists = $this->user_modal->email_check($parameter['Email']);

                    if (isset($customer_exists['id'])) {

                        $value = [
                            'user_id' => $customer_exists['id'],
                            'confirm_password' => $parameter['ConfirmPassword']
                        ];

                        $this->user_modal->customer_change_password($value);
                    }

                    echo json_encode(['success' => true, 'msg' => 'Password change successful']);

                } else {
                    echo json_encode(['success' => false, 'msg' => $password_update_decode->Message]);
                }

            } else {
                echo json_encode(['success' => false, 'msg' => $get_email_decode->Message]);
            }
        }
    }

    /**
     * Activate Junifer / Dyball account
     *
     * @param null
     * @return string - JSON array
     */
    function activate()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");

        $data = json_decode( file_get_contents( 'php://input' ), true );

        // check for the token
        $check_for_token = $this->user_modal->check_token_confirm($data['token']);
        $get_customer_detail = $this->user_modal->get_account_detail($check_for_token['customer_id']);

        // make sure customer login was correct
        $db = [
            'user_email' => $data['username'],
            'user_password' => $data['password']
        ];
        $login_check = $this->user_modal->login_check($db);

        // If customer login was successful and matches a token we have in the DB....activate
        if( isset($login_check['id']) && isset($get_customer_detail['id']) && $login_check['id'] === $get_customer_detail['id'] ) {

            $this->user_modal->change_status($data['token']);

            // Junifer activate
            if( $get_customer_detail['signup_type'] !== 3 ){
                $this->curl->junifer_request('/rest/v1/webUsers/activate', ["token" => $data['token']], 'post');
            }

            echo json_encode([ 'success' => true, 'error' => false]);

        } else {

            $email_message = 'We seem to be having a problem activating your online account.<br>Please drop us an e-mail at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to let us know and we’ll get right on it!';
            echo json_encode([ 'success' => false, 'error' => $email_message]);
        }
    }

    /**
     * Activate Redfish account
     *
     * @param null
     * @return string - JSON array
     */
    function activate_account()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");

        $data = json_decode( file_get_contents( 'php://input' ), true );

        $reading_parameter = [
            'Email' => $data['email'],
            'Password' => $data['password'],
            'ConfirmPassword' => $data['confirm_password']
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_URL => $this->Api_Url.'CreatePassword/?t='.urlencode($data['token']),
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $activate_result = curl_exec($curl);
        $activate_result_decode = json_decode($activate_result);

        if($activate_result_decode->Data->Email == $data['email']) {
            echo json_encode(['success' => true, 'msg' => 'Account has been activated successfully.']);
        } else {
            echo json_encode(['success' => false, 'msg' => 'Server error while creating password.']);
        }

    }

    function dyball_gas_submit_reading()
    {
        $reading = $this->input->post('gas_meter_submit0').$this->input->post('gas_meter_submit1').$this->input->post('gas_meter_submit2').$this->input->post('gas_meter_submit3').$this->input->post('gas_meter_submit4').$this->input->post('gas_meter_submit5');
        if( $this->input->post('signupType') == 3 )
        {
            $parameter = [
                'AuthKey' => $this->authkey,
                'AccountNumber' => $this->session->userdata('login_data')['accounts'],
                'MeterSerialNumber' => date('Y-m-dTH:i:s'),
                'MPRN' => 'Normal',
                'ReadingDate' => date('Y-m-dTH:i:s'),
                'Reading1' => $reading
            ];
            $response = $this->curl->junifer_request_test('/api/CustomerSelfService/SubmitGasReading', $parameter, 'post');
            $response = json_decode($response,true);
            //debug($response);
            if(!empty($response['success'])){
                echo json_encode([ 'error' => '0', 'msg' => $response['success']]);
            }else{
                echo json_encode([ 'error' => '1', 'msg' => 'Error! while submitting reading']);
            }
        }
    }

    function countdown()
    {
        $this->load->view('energy/countdown');
    }

    function move_home_1()
    {
        $this->load->view('dashboard/moving_home_1');
    }

    function move_home_2()
    {
        $this->load->view('dashboard/moving_home_2');
    }

    function move_home_3()
    {
        $this->load->view('dashboard/moving_home_3');
    }

    function move_home_steps()
    {
        if(  empty($this->session->userdata('login_data')) ) {
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 2 ) {
            if( $this->session->has_userdata('login_data') )
            {
                $fromdate_monthly = date('Y-m-01');
                $todate_monthly = date('Y-m-01', strtotime("+1 month"));
                $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;
                $get_meterpoint_ids = $this->curl->junifer_request_usage('/rest/v1/accounts/'.$get_user_account_id.'/agreements');
                $get_meterpoint_ids_responce = json_decode($get_meterpoint_ids);

                if( !empty( $get_meterpoint_ids_responce->results ) )
                {
                    for( $r = 0; $r<count( $get_meterpoint_ids_responce->results ) ; $r++ )
                    {
                        if ($get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Electricity Supply')
                        {
                            if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id))
                            {
                                $electric_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;
                            }
                        }
                        elseif ( $get_meterpoint_ids_responce->results[$r]->products[0]->type == 'Gas Supply')
                        {
                            if(isset($get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id))
                            {
                                $gas_meter_point_id = $get_meterpoint_ids_responce->results[$r]->products[0]->assets[0]->id ;
                            }
                        }
                    }
                }

                if (isset($electric_meter_point_id))
                {
                    $el_response_monthly_array = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$electric_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
                    $response = json_decode($el_response_monthly_array,true);
                    $data['results'] = $response['results'];
                    $data['last_reading'] = end($response['results']);
                }

                if (isset($gas_meter_point_id))
                {
                    $gas_response  = $this->curl->junifer_request_usage('/rest/v1/meterPoints/'.$gas_meter_point_id.'/readings?fromDt='.$fromdate_monthly.'&toDt='.$todate_monthly.'');
                    $gas_response = json_decode($gas_response,true);
                    $data['gas_reading'] = $gas_response['results'];
                    $data['last_reading_gas'] = end($gas_response['results']);
                }

                header('content-type: text/html');
                $data['session_data'] = $this->session->userdata('login_data');

                $data['content'] = 'dashboard/meter_reading';
            }
        }

        if( $this->session->userdata('login_data')['signup_type'] == 3 ) {
            $result_response = array();
            $gas_results = array();
            if( !empty($this->session->userdata('login_data')['mpancore']) )
            {
                $parameter = [ "AuthKey" => $this->authkey_dyball_serve, "AccountNumber" => "DYB_ELEC_".$this->session->userdata('login_data')['id'] ];
                $response = $this->curl->curl_dyball('/api/customerselfservice/GetElectricityReadings', $parameter, 'post');
                $response = json_decode($response,true);
                $result = $response['Mpans'][0]['MpanMeters'][0]['MpanMeterRegisters'][0]['Readings'];
                for( $i=0; $i<count($result); $i++ )
                {
                    $result_response[$i]['readingDttm'] = $result[$i]['ReadDate'];
                    $result_response[$i]['consumption'] = $result[$i]['Reading'];
                }
            }
            else
            {
                $result_response = '';
            }
            if( !empty($this->session->userdata('login_data')['user_mprn']) )
            {
                $parameter_gas = [ "AuthKey" => "EVERSMARTENERGYSUPP", "AccountNumber" => "EVDYB".$this->session->userdata('login_data')['account_id'] ];
                $response_gas = $this->curl->junifer_request_test('/api/CustomerSelfService/GetGasReadings', $parameter_gas, 'post');
                $response_gas = json_decode($response_gas,true);
                $result_gas = $response_gas['Mprns'][0]['MprnMeters'][0]['Readings'];
                for( $i=0; $i<count($result_gas); $i++ )
                {
                    $gas_results[$i]['readingDttm'] = $result_gas[$i]['ReadDate'];
                    $gas_results[$i]['consumption'] = $result_gas[$i]['Reading'];
                }
                if (isset($response_gas['Mprns'][0]['MprnMeters'][0]['Readings'])) {
                    $data['last_gas_reading'] = end($response_gas['Mprns'][0]['MprnMeters'][0]['Readings']);
                }
                else
                {
                    $data['last_gas_reading'] = [];
                }
            }
            else
            {
                $gas_results = '';
            }

            header('content-type: text/html');
            $data['results'] = $result_response;
            $data['gas_reading'] = $gas_results;
            $data['session_data'] = $this->session->userdata('login_data');

            $data['content'] = 'dashboard/moving_home_steps';
        }

        $this->load->view('dashboard/moving_home_steps', $data);
    }

    function check_email()
    {
        $json = json_decode( file_get_contents( 'php://input' ), true );
        $email_check = $this->user_modal->email_check( $json['email'] );
        echo json_encode( $email_check['id'] ? [ 'error' => '1', 'data'=> ['exists' => true] ] : [ 'error' => '0', 'data'=> ['exists' => false] ] ) ;
    }

    /**
     * Check Redfish for customer, send email if exists
     * Look up on our DB, sends reset password, adds row to email log.
     *
     * @param null
     * @return string - JSON array
     */
    function forgotten_password()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: https://portal.eversmartenergy.co.uk");
        header("Access-Control-Allow-Headers: Content-Type, Accept, Origin, Referer, User-Agent");
        header("Access-Control-Allow-Methods: GET, POST");
        header("X-Content-Type-Options: nosniff");
        header("Content-Type: text/json");


        $data = json_decode( file_get_contents( 'php://input' ), true );

        if(empty($data)){
            $data = $this->input->post();
        }

        // No customer found...go check Redfish
        $json_response = $this->curl_requests_redfish->redfish_reset_password($data['user_email']);
        $response = json_decode($json_response,true);

        // Was Redfish password reset was successful
        if($response['error'] == 0) {

            // Redfish success
            echo $json_response;

        } else {

            // Verify email address
            $email_check = $this->user_modal->email_check($data['user_email']);

            // If we have all the information we need
            if( isset($email_check['id']) && isset($email_check['forename']) && isset($email_check['email']) ){

                // Generate token and set up email data
                $token = md5(uniqid(rand(0,9999), true));
                $email_data['email_info'] = [ 'token' => $token, 'name' => ucfirst($email_check['forename']) ];
                $this->load->view( 'layout/reset_template', $email_data , true);

                // Trigger email
                $this->reset_password($email_check['id'], $token, $email_check['forename'], $email_check['email']);

                // Store token
                $this->user_modal->token_in_database($token, $email_check['id'],'1');

                // Success message
                echo json_encode([ 'error' => '0', 'msg'=>'Reset password email sent to '. $email_check['email'] ]);

            }
            else {
                echo json_encode([ 'error' => '1', 'msg'=>'Email is not recognised by our system' ]);
            }
        }
    }

    /**
     * Check DB for token
     *
     * @param null
     * @return string - JSON array
     */
    function verify_token(){

        // Response headers needs
        header("Access-Control-Allow-Origin: https://portal.eversmartenergy.co.uk");
        header("Access-Control-Allow-Headers: Content-Type, Accept, Origin, Referer, User-Agent");
        header("Access-Control-Allow-Methods: GET, POST");
        header("X-Content-Type-Options: nosniff");
        header("Content-Type: text/json");

        $data = json_decode( file_get_contents( 'php://input' ), true );
        $error_msg =  'Sorry, there is an issue with the link. Please \'Generate a new password reset token\' using the link below.';

        if($data['token']) {

            // Look up in DB
            $lookup = $this->user_modal->check_token($data['token']);

            if ($lookup['customer_id'] > 0) {

                $customer = $this->user_modal->get_account_detail($lookup['customer_id']);

                if( isset($customer['email']) && $customer['email'] != '' ){
                    echo json_encode([ 'success' => true, 'email' => $customer['email']]);
                } else {
                    echo json_encode([ 'success' => false, 'error' => $error_msg]);
                }

            } else {

                // Are we doing a redfish activate?
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $this->Api_Url.'CreatePassword/?t='.urlencode($data['token']),
                    CURLOPT_HTTPHEADER => array(
                        'Api-Key: '.$this->Api_Key,
                        'Api-Type: Website',
                        'Client-IP: '.$this->Client_ip,
                        'Content-Type: application/json',
                        'Accept: application/json',
                        'Client-UA:'.$this->client_agent,
                    ),
                ));
                $activate_result = curl_exec($curl);
                $activate_result_decode = json_decode($activate_result);

                if($activate_result_decode->Message == 'SUCCESS') {
                    echo json_encode([ 'success' => true, 'email' => $activate_result_decode->Data->Email]);
                } else if(isset($activate_result_decode->Message) && !strstr($activate_result_decode->Message, 'This token is not valid.')) {
                    echo json_encode([ 'success' => false, 'error' => $activate_result_decode->Message]);
                } else {
                    echo json_encode([ 'success' => false, 'error' => $error_msg]);
                }
            }
        } else {
            echo json_encode([ 'success' => false, 'error' => $error_msg]);
        }
    }

    function changeformat()
    {
        //debug($this->input->post());
        $post_value = $this->input->post();

        foreach( $post_value as $key => $value ){
            echo '<input type="text" name="'.$key.'" value="'.$value.'" >';
        }
    }

    function signup_success()
    {
        if(isset( $_SESSION['customer_id'])) {
            $data['fname'] = $_SESSION['fname'];
            $data['lname'] =  $_SESSION['lname'];
            $data['first_account'] =  $_SESSION['customer_id'];
            $this->load->view('layout/confirmation_signup', $data);
        }
        else {
            $this->load->view('layout/confirmation_signup');
        }
    }

    function signup_pending()
    {
        if(isset( $_SESSION['customer_id'])) {
            $data['fname'] = $_SESSION['fname'];
            $data['lname'] =  $_SESSION['lname'];
            $data['first_account'] =  $_SESSION['customer_id'];
            $this->load->view('layout/confirmation_pending_signup', $data);
        }
        else {
            $this->load->view('layout/confirmation_pending_signup');
        }
    }

    function account()
    {

        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        //debug( $this->session->userdata('login_data'),1 );
        $data['user_info'] = $this->user_modal->get_account_detail($this->session->userdata('login_data')['id']);
        $data['content'] = 'dashboard/account';
        $this->load->view('layout/dashboard_master',$data);

    }

    function update_account()
    {

        $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );

        if(!empty($_POST))
        {
            //parse_str($_POST['data'], $searcharray);

            $phone = $_POST['phonenum'];
            //$password = $searcharray['password'];

            $data = [
                //'password' => base64_encode($password),
                'phone1' => $phone,
            ];

            $customer_id = $this->session->userdata('login_data')['id'];

            //$data = json_encode($data);

            $update_customer_info = $this->user_modal->update_customer_info($data,$customer_id);

            if($login_response['signup_type'] == 0 || $login_response['signup_type'] == 2)
            {

                // $parameter['forename'] = $this->input->post('first_name');
                //$parameter['surname'] = $this->input->post('last_name');
                $parameter['phoneNumber1'] = $phone;
                $update_account = $this->curl->junifer_request( '/rest/v1/customers/'.$login_response['account_id'].'/primaryContact', $parameter, 'put' );
                $header_response = $this->parseHttpResponse($update_account);

                if($header_response ['headers']['http_code'] == 'HTTP/1.1 204 No Content' )
                {
                    echo  json_encode([ 'success' => 'true']);

                }
                else {
                    echo  json_encode([ 'success' => 'false']);
                }
            }
            else {
                echo  json_encode([ 'success' => 'false']);
            }

        }


    }

    function get_first_meter_reading_from_log($customer_id, $fuel_type_id)
    {

        return $this->mysql->select('*')
            ->from('meter_reading_log')
            ->where('customer_id' , $customer_id)
            ->where('fuel_type_id' , $fuel_type_id)
            ->order_by('created_at','asc')
            ->limit(1)
            ->get()->first_row('array');
    }


    function pay_dashboard()
    {
        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        $data['balance'] = 0;

        $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );

        if( $this->session->userdata('login_data')['signup_type'] == '0' || $this->session->userdata('login_data')['signup_type'] == '2' ){

            $get_user_account_id = $this->session->userdata('login_data')['account_id'] ;

            $get_balance_junifer = $this->curl->junifer_request('/rest/v1/accounts/'.$get_user_account_id.'');
            $get_balance_junifer = $this->parseHttpResponse($get_balance_junifer);

            if(  $get_balance_junifer['headers']['http_code'] == 'HTTP/1.1 200 OK') {

                $data['balance'] = $get_balance_junifer['body']['balance'] ;

            }


        }


        $data['content'] = 'dashboard/pay-dashboard';
        $this->load->view('layout/dashboard_master',$data);
        //$this->load->view('dashboard/pay-dashboard');
    }

    function booked_meter()
    {

        $this->user_modal->book_meter($this->input->post('user_id'),$this->input->post('meter'));

        // Get the customer info
        $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );

        // Set the variables for email
        $token = md5(uniqid(rand(0,9999), true));
        $account_id = $login_response['account_id'] ;
        $user_phone = $login_response['phone1'] ;
        $post_code =  $login_response['address_postcode'] ;
        $name = $login_response['forename'] ;
        $lastname = $login_response['surname'] ;
        $email = $login_response['email'] ;

        // Send email
        $this->agent_calling_customer_email($login_response['id'], $token, $name, $email);

        // Log token
        $token_in_database = $this->user_modal->token_in_database($token,$login_response['id'],'1');

        // Response
        echo  json_encode([ 'error' => '0', 'msg'=>'Mail send', 'Post code' => $post_code, 'Phone' =>  $user_phone , 'name'=> $name, 'last name' => $lastname]);

    }

    function admin_page()
    {
        $data['booked_detail'] = $this->user_modal->booking_meter();
        $data['confirmed_detail'] = $this->user_modal->confirmed_meter();
        $data['content'] = 'dashboard/admin_page';
        //debug($data);
        $data['title'] = 'Admin Panel';
        $this->load->view('layout/dashboard_master',$data);

    }

    function status_active()
    {

        //update api_user_status to '1' in db
        $change_user_status = $this->user_modal->change_user_status( $this->input->post('user_id') );

        if( $change_user_status ) {

            $login_response = $this->user_modal->get_account_detail( $this->input->post('user_id') );


            // Check for referer code
            if(isset($login_response['saasref']) && $login_response['saasref']!='' && $login_response['saasref']!='false'){

                $para = [
                    'id' => $login_response['id'],
                    'accountId' => $login_response['id'],
                    'referredBy' => [
                        'code' => $login_response['saasref'], // referer code
                        'isConverted'=> true // woohoo - trigger the reward
                    ]
                ];

                // If we have one, go and mark them as isConverted = true ( this will give them (referee) and the referrer 50 squid )
                $this->curl->curlSaas('open/account/' . $login_response['id'] . '/user/' . $login_response['id'] . '', $para, 'put');
            }

            // Set up some data
            if( !empty($login_response) ){

                $DateSeg = explode('-', $login_response['create_at']);
                $CreatedAt = date('Y-m-d H:i:s', mktime(23,59,59,(int)$DateSeg[1],(int)$DateSeg[2],(int)$DateSeg[0]));
                $next_week = strtotime($CreatedAt.'+21 days');
                $now = date('Y-m-d');
                $date1 = date_create($now);
                $date2 = date_create(date( 'Y-m-d', strtotime($login_response['create_at'])));
                $diff = date_diff($date1,$date2);//difference between two dates
                $info[ 'current_day' ] = $diff->format("%a");//count days

                // if we are after the 3 weeks set difference to 0
                if ( $next_week < time() ) {
                    $difference = '0';
                }
                else {
                    $difference = $next_week - time(); // next weeks date minus todays date
                    $difference = date('j', $difference);
                }

                $info['create_at'] = $login_response['create_at'];
                $info['booked_meter'] = $login_response['booked_meter'];
                $info['day_left'] = $difference;
                $info['id'] = $login_response['id'];
                $info['title'] = $login_response['title'];
                $info['first_name'] = $login_response['forename'];
                $info['last_name'] = $login_response['surname'];
                $info['signup_type'] = $login_response['signup_type'];
                $info['account_status'] = $login_response['api_user_status'];
                $info['user_mprn'] = $login_response['gasProduct_mprns'];
                $info['address'] = $login_response['first_line_address'];
                $info['town_address'] = $login_response['town_address'];
                $info['city_address'] = $login_response['city_address'];
                $info['mpancore'] = $login_response['elec_mpan_core'];
                $info['elec_meter_serial'] = $login_response['elec_meter_serial'];
                $info['gas_meter_serial'] = $login_response['gas_meter_serial'];
                $info['customer_id'] = $login_response['customer_id'];
                $info['account_id'] = $login_response['account_id'];
                $info['account_number'] = $login_response['account_id'];
                $info['email'] = $login_response['email'];
                $info['mpancore'] = $login_response['elec_mpan_core'];
                $info['meterpoint_mpan'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPAN"];
                $info['meterpoint_mprn'] = ["id"=>314,"identifier"=>"1200020346970","type"=>"MPRN"];
                $info['billingMethod'] = $login_response['billingMethod'];
                $info['tariff_newspend'] = $login_response['tariff_newspend'];
                $info['exitFeeElec'] = $login_response['exitFeeElec'];
                $info['tariff_tariffName'] = $login_response['tariff_tariffName'];
                $info['tariff_unitRate1Elec'] = $login_response['tariff_unitRate1Elec'];
                $info['junifer_account_active'] = 1;

                $this->session->set_userdata('login_data',$info);
            }

            $info['junifer_account_active'] = 1;
            $info['current_day'] = 0;

            $this->session->set_userdata('login_data',$info);
            echo json_encode(['error' => 0, 'msg' => 'account update']);

        }
        else {
            echo json_encode(['error' => 1, 'msg' => 'account update fail']);
        }
    }

    function saascode()
    {
        $code = json_decode(file_get_contents('php://input'), true);

        $response = $this->curl->curlSaas('code/'.$code['code']);
        $saas_response = json_decode($response,true);

        $saas_amount = '£40';
        $referred_saas_amount = '£10';
        echo json_encode([
            'error' => '0',
            'ref_name' => $saas_response['referrerName'],
            'saas_amount' => $saas_amount,
            'referred_saas_amount' => $referred_saas_amount
        ]);
    }

    function messages()
    {

        if(  empty($this->session->userdata('login_data')) ){
            header('location:'.base_url().'index.php/user/login');
            exit;
        }

        $login_response = $this->user_modal->get_account_detail( $this->session->userdata('login_data')['id'] );
        $account_id = $login_response['account_id'] ;

        if($login_response['signup_type'] == 0 || $login_response['signup_type'] == 2)
        {

            $junifer_msgs = $this->curl->junifer_request('/rest/v1/accounts/'.$account_id.'/communications');

            $junifer_msgs = $this->parseHttpResponse($junifer_msgs);

            if( isset($junifer_msgs['headers']['http_code']) && $junifer_msgs['headers']['http_code'] == 'HTTP/1.1 200 OK')
            {
                $junifer_msgs = $junifer_msgs['body'];
            }
            else
            {
                $junifer_msgs = [];
            }
        }
        else
        {
            $junifer_msgs = [];
        }

        $data['junifer_msgs'] =  $junifer_msgs ;
        $data['content'] = 'dashboard/messages';
        $this->load->view('layout/dashboard_master',$data);

    }

    function insert_email_log($data)
    {
        $this->mysql->insert('email_log',$data);
        return $this->mysql->insert_id();
    }

    function all_email_templates()
    {
        $this->load->view('layout/all_email_templates');
    }

    function activation_email($customer_info_id, $token, $forename, $recipient_email)
    {

        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'thumb_header.jpg';
        $email_data['content'] = 'layout/activation_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Signup Confirmation');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 1;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

    }

    function psr_email($customer_info_id, $CustomerData)
    {

        $this->load->library('email');

        $customer_name = $CustomerData['name'];
        $customer_email = $CustomerData['email'];
        $customer_phone = $CustomerData['phone'];

        $email_data['email_info'] = [ 'name' => $customer_name , 'email' => $customer_email , 'phone' => $customer_phone ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/psr_customer_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->email->from($customer_email, $customer_name);
        $this->email->to('PSR@eversmartenergy.co.uk');
        $this->email->subject('PSR Customer');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 22;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

    }

    function provide_meter_reading($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'meter_header.jpg';
        $email_data['content'] = 'layout/provide_meter_reading_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Please provide a meter reading');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 5;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Provide meter reading email sent to: '.$recipient_email]);

    }

    function first_meter_reading($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'meter_header.jpg';
        $email_data['content'] = 'layout/first_meter_reading_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Please provide a meter reading');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 4;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'First meter reading email sent to: '.$recipient_email]);

    }

    function payment_plan_cancelled($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/payment_plan_cancelled_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Payment plan cancelled');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 2;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Payment plan cancelled email sent to: '.$recipient_email]);

    }

    function latest_energy_bill($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/latest_energy_bill_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Latest energy bill');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 3;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Latest energy bill email sent to: '.$recipient_email]);

    }

    function direct_debit_confirmation($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');

        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/direct_debit_confirmation_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Direct debit confirmation');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 10;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Direct debit confirmation email sent to: '.$recipient_email]);

    }

    function direct_debit_changes($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/direct_debit_change_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Direct debit changes');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 11;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Direct debit confirmation email sent to: '.$recipient_email]);

    }

    function direct_debit_problem($customer_info_id, $token, $forename, $recipient_email)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/direct_debit_problem_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Direct debit problem');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 12;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Direct debit problem email sent to: '.$recipient_email]);

    }

    function leaving($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/leaving_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Sorry to see you go');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 6;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Sorry to see you go email sent to: '.$recipient_email]);

    }

    function balance_overdue_1($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/balance_overdue_1_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Your balance is overdue');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 7;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Balance overdue 1 email sent to: '.$recipient_email]);

    }

    function balance_overdue_2($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/balance_overdue_2_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Your balance is still overdue');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 8;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Balance overdue 2 email sent to: '.$recipient_email]);

    }

    function balance_overdue_3($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/balance_overdue_3_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Your overdue balance');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 9;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Balance overdue 3 email sent to: '.$recipient_email]);

    }

    function new_payment_plan($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        //$customer_info_id = 000299;
        //$forename = '[customer_name]';
        //$recipient_email = $this->input->post('email_address');
        $customer_number = $customer_info_id;
        //$bill_number = '[bill_number]';


        //$token = md5(uniqid(rand(0,9999), true));
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/new_payment_plan_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('New payment plan');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 13;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'New payment plan email sent to: '.$recipient_email]);

    }

    function new_payment_schedule($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {
        $customer_number = $customer_info_id;
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'wallet_header.jpg';
        $email_data['content'] = 'layout/new_payment_schedule_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('New payment schedule');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 14;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'New payment schedule email sent to: '.$recipient_email]);

    }

    function objection($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {
        $customer_number = $customer_info_id;
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/objection_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('We have objected to your switch');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 15;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Objection email sent to: '.$recipient_email]);

    }

    function previous_supplier_objection($customer_info_id, $token, $forename, $recipient_email, $bill_number)
    {

        $customer_number = $customer_info_id;

        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename , 'customer_number' => $customer_number , 'bill_number' => $bill_number ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/previous_supplier_objection_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Your current supplier has stopped your switch');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] =16;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Previous supplier objection email sent to: '.$recipient_email]);

    }

    function registration_withdrawal($customer_info_id, $token, $forename, $recipient_email)
    {
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/registration_withdrawal_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('We weren\'t able to switch you');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] =17;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Registration withdrawal email sent to: '.$recipient_email]);

    }

    function reset_password($customer_info_id, $token, $forename, $recipient_email)
    {
        $customer_number = $customer_info_id;
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename, 'customer_number' => $customer_number ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/reset_password_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Reset password');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 18;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Reset password email sent to: '.$recipient_email]);

    }

    function book_meter($customer_info_id, $token, $forename, $recipient_email, $user_phone, $post_code)
    {
        $customer_number = $customer_info_id;

        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename, 'customer_number' => $customer_number, 'user_phone' => $user_phone, 'post_code' => $post_code ];
        $email_data['email_header_img'] = 'meter_header.jpg';
        $email_data['content'] = 'layout/book_meter_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Book meter');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 19;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Book meter email sent to: '.$recipient_email]);

    }

    function moving_home_email($customer_info_id, $token, $forename, $recipient_email, $new_address)
    {
        $customer_number = $customer_info_id;
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename, 'customer_number' => $customer_number, 'new_address' => $new_address ];
        $email_data['email_header_img'] = 'switch_header.jpg';
        $email_data['content'] = 'layout/moving_home_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Moving home');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 20;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Moving home email sent to: '.$recipient_email]);

    }

    function agent_calling_customer_email($customer_info_id, $token, $forename, $recipient_email)
    {
        $email_data['email_info'] = [ 'token' => $token, 'name' => $forename ];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/agent_calling_customer_email_content';

        $template = $this->load->view('layout/email_template', $email_data , true);

        $this->load->library('email');
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('An agent will be calling you soon');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 22;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'Agent calling you soon email sent to: '.$recipient_email]);

    }

    function generate_pdf($data)
    {

        // Lets send some data from controller to view file
        $pdf_data['customer_name'] = $data['customer_name'];
        $pdf_data['customer_number'] = $data['account_number'];
        $pdf_data['email'] = $data['email'];
        $pdf_data['address_line_1'] = $data['contact_address_1'];
        $pdf_data['address_line_2'] = $data['contact_address_2'];
        $pdf_data['address_line_3'] = $data['contact_address_3'];
        $pdf_data['postcode'] = $data['contact_post_code'];

        $pdf_data['current_elec_sc'] = $data['current_elec_sc'];
        $pdf_data['current_gas_sc'] = $data['current_gas_sc'];
        $pdf_data['new_elec_sc'] = $data['new_elec_sc'];
        $pdf_data['new_gas_sc'] = $data['new_gas_sc'];

        $pdf_data['current_elec_dur'] = $data['current_elec_dur'];
        $pdf_data['current_gas_ur'] = $data['current_gas_ur'];
        $pdf_data['new_elec_dur'] = $data['new_elec_dur'];
        $pdf_data['new_gas_ur'] = $data['new_gas_ur'];

        $pdf_data['rate_type'] = $data['rate_type'];
        $pdf_data['current_elec_nur'] = $data['current_elec_nur'];
        $pdf_data['new_elec_nur'] = $data['new_elec_nr'];

        $pdf_data['tariff_change_date'] = '25th November 2018';
        $pdf_data['tariff_date_to'] = '25/11/18';
        $pdf_data['tariff_date_from'] = '26/11/18';
        $pdf_data['filename'] = 'assets/temp_pdf/tariff_change_'.date('dmY').'_'.$pdf_data['customer_number'].'.pdf';

        $this->load->view('tariff_change_pdf_template', $pdf_data);

        return $pdf_data['filename'];

    }

    function variable_tariff_changing($data)
    {

        // Returns the pdf filename
        $Attachment = $this->generate_pdf($data);

        //print_r($CustomerData);
        $forename = $data['customer_name'];
        $recipient_email = $data['email'];
        $customer_number = $data['account_number'];

        $token = md5(uniqid(rand(0, 9999), true));

        $EncodedHash = trim(base64_encode(mktime(0,0,0,11,26,18).'|'.$customer_number),'=');
        $EncodedHash = str_replace('/', '_', $EncodedHash);
        $EncodedHash = str_replace('+', '-', $EncodedHash);

        $email_data['email_info'] = ['token' => $token, 'name' => $forename, 'customer_number' => $customer_number, 'encoded_hash' => $EncodedHash];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/variable_price_change_email_content';

        $template = $this->load->view('layout/email_template', $email_data, true);

        $this->load->library('email');
        $this->email->clear(true);
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Our variable tariff is changing');
        $this->email->message($template);
        if($Attachment) {
            $this->email->attach($Attachment);
        }
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_number;
        $email_log_db_parameter['email_type_id'] = 21;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        //echo json_encode(['success' => 'variable tariff changing email sent to: '.$recipient_email]);

    }

    /*function sendTariffChangeNotification(){

        // Load library
        $this->load->library('myfpdf');

        $Customers = $this->mysql->select('account_number, customer_name, email, contact_address_1, contact_address_2, contact_address_3, contact_post_code, current_elec_sc, current_gas_sc,  new_elec_sc, new_gas_sc, current_elec_dur, current_gas_ur, new_elec_dur, new_gas_ur, rate_type, current_elec_nur, new_elec_nr')->from('tariff_change_notification')->where('email != \'\'', NULL, FALSE)->order_by("account_number", "ASC")->get()->result_array();

        // loop through each one
        foreach($Customers as $CustomerData) {
            $this->variable_tariff_changing($CustomerData);
        }

    }*/

    function calculate_total_customer_interest($customer_id)
    {

        $AccruedInterest = $this->mysql->select('SUM(cmi.monthly_interest) AS `accrued_interest`, ci.monthly_interest_customer_signup_date AS signup_date ')
            ->from('customer_monthly_interest cmi')
            ->join('customer_info ci',  'ci.id = cmi.customer_id' )
            ->where('cmi.customer_id',$customer_id)
            ->get()->first_row('array');
        return $AccruedInterest;

    }

    function get_customer_interest_data($customer_id){

        $AccruedInterest = $this->mysql->select('*')
            ->from('customer_monthly_interest cmi')
            ->where('cmi.customer_id',$customer_id)
            ->get()->result_array();
        return $AccruedInterest;

    }

    function web_to_case_log()
    {
        $data = json_decode(file_get_contents('php://input'));
        $this->mysql->insert('web_to_case_log',$data);
        return $this->mysql->insert_id();
    }

    function get_intercom($intercom_id)
    {
        $get_com_user = $this->curl->curlIntercom('users/'. $intercom_id . '');
        return json_decode($get_com_user, true);
    }

    function intercom_create($insert_customer_info, $email, $name)
    {
        $para_com = [
            "user_id" => $insert_customer_info,
            "email" => $email,
            "name" => $name
        ];

        //create intercom user
        $creating_com_user = $this->curl->curlIntercom('users', $para_com, 'post');

        //get user
        $get_com_user = $this->curl->curlIntercom('users?user_id='. $insert_customer_info . '');
        $get_com_user = json_decode($get_com_user, true);

        $db_parameter = ["intercom_id" => $get_com_user['id']];

        $this->user_modal->update_customer_info($db_parameter, $insert_customer_info);

        return $get_com_user;
    }

    function web_to_lead_log()
    {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        $this->mysql->insert('web_to_lead_log',$data);
        return $this->mysql->insert_id();
    }

    /**
     * Check customer token exists in the DB
     * @param $int_custmomer_id
     * @return Boolean
     */
    function check_customer_token($int_custmomer_id)
    {
        $token = $this->user_modal->get_customer_token($int_custmomer_id);
        if(!$token){
            log_message('error', 'Customer does not have token assigned to them');
            return false;
        }
        $this->str_customer_token = $token;
        return true;
    }

    /**
     * Activate customer account if there is a token
     * @param $int_customer_id
     * @return Boolean
     */
    function activate_with_token($int_customer_id)
    {
        $bol_token_exists = $this->check_customer_token($int_customer_id);
        if (!$bol_token_exists) {
            log_message('error', 'Token not found in database against customer logging in');
            return false;
        }

        $this->user_modal->update_customer_info('active', '1');
        return true;
    }

}


