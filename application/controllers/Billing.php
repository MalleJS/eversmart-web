<?php

/**
 * Class Billing
 */
class Billing extends ESE_Controller
{
    /**
     * Billing constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_redfish');
    }


    /**
     * Retrieves all customer bills from Junifer and redfish, merges the two and returns a sorted array
     *
     * @param null
     * @return string json array
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");


        $junifer_data = array();
        $redfish_data = array();

        if(in_array('junifer',$this->customer_type) ) {

            $junifer_data = $this->curl_requests_junifer->junifer_get_bills($this->account_id->junifer);
            $junifer_payments = $this->curl_requests_junifer->junifer_payment_details($this->account_id->junifer);

        }

        if(in_array('redfish',$this->customer_type) ) {

            $redfish_data = $this->load_redfish_bills();
        }

        // Merge arrays - this will work if one or both are empty
        $data = array_merge($junifer_data,$redfish_data);

        // Check its not an empty array
        if (!empty($data)) {

            // Sort by timestamp field - descending
            function cmp($a, $b)
            {
                return strcmp($b["timestamp"], $a["timestamp"]);
            }

            usort($data, "cmp");

            $next_bill_date = isset($junifer_payments['results']) && !empty($junifer_payments['results'])?$junifer_payments['results'][0]['nextPaymentDt']:'';

            // return success and data
            echo json_encode(['success' => true, 'error' => false, 'next_bill_date' => $next_bill_date, 'data' => $data, 'token' => $this->get_latest_token() ]);
        }
        else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => 'No billing information found']);
        }
    }


    /**
     * Return Redfish bill array
     *
     * @param null
     * @return array php
     */
    function load_redfish_bills(){

        $invoice = $this->curl_requests_redfish->redfish_customer_invoice($this->account_id->redfish);
        $response = $this->parseHttpResponse($invoice);

        $data = array();

        // If the call was success full
        if($response['body']['Message'] == 'SUCCESS') {

            // If we have a token returned in the header
            if (isset($response['headers']['Set-Cookie'])) {

                // Turn header into something usable
                $cookie = $this->parseCookieHeader($response['headers']['Set-Cookie']);

                // Assign to the public variable
                $this->redfish_token = $cookie['account'];

                // Refresh JWT token with latest Redfish token
                $this->refresh_token();
            }

            foreach ($response['body']['Data'][0]['Invoices'] as $bill) {

                $data[] = [
                    'issue_date' => $bill['InvoiceDate'],
                    'invoice_period' => '',
                    'timestamp' => strtotime(str_replace('/','-',$bill['InvoiceDate'])),
                    'gross_amount' => $bill['GrossValue']
                ];
            }
        }

        return $data;
    }


    /**
     * Displays Junifer bill in pdf format
     *
     * @param $bill_id
     * @return mixed
     */
    function download_bill($billid)
    {
        // Response headers needs

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token ");
        header('Content-type: application/pdf');    // ensures that image of bill is displayed as a PDF

        if(in_array('junifer',$this->customer_type) ) {

            // First we need to check that the message id is actually for this customer
            if ($this->verify_bill_download($billid) == false) {
                echo json_encode(['success' => false, 'error' => 'No bill found.']);
            }

            return $this->curl_requests_junifer->junifer_view_bill($billid);
        }
    }


    /**
     * Check whether bill belongs to this customer
     *
     * @param $msgid
     * @return boolean
     */
    function verify_bill_download($billid){

        if(in_array('junifer',$this->customer_type) ) {

            $response = $this->curl_requests_junifer->junifer_get_bills($this->account_id->junifer);

            // If we cant find any messages for this customer send them to a 404
            if (empty($response)) {
                return false;
            }

            // Set up an array of bill ids
            $CheckID = array();
            foreach ($response as $BillData) {
                $CheckID[] = $BillData['id'];
            }

            // If the bill id is not in the customers bill, kick them out
            if (!in_array($billid, $CheckID)) {
                return false;
            } else {
                return true;
            }
        }
    }

}
