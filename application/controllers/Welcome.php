<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends ESE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	function get_address()
	{
		$data1 = ['pcod' => 'wa158rs' ];
		$response = $this->curl->curl_request('/v3.1/partner-resources/'.$this->affiliate_id.'/mpas/addresses-by-pcod/', $data1, 'post' );
		$data['response'] = json_decode($response);
		$data['title'] = 'Address';
		$data['content'] = 'address/address_list';
		$this->load->view('layout/theme_master_blue',$data);
	}

	function get_supplier()
	{
			echo $response = $this->curl->curl_request('/v3.1/partner-resources/'.$this->affiliate_id.'/suppliers/', [], 'post'  );
	}
}
