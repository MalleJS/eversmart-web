<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examples extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
	}

	public function _example_output($output = null)
	{
		$admin_data['page_ref'] = 'meter_booking';
        $admin_data['page_title'] = 'Meter Bookings';
        $admin_data['content'] = (array)$output;
        $this->load->view('layout/admin_template',$admin_data);
	} 

	public function offices()
	{ 
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function sessions()
	{
		$didgit = $this->input->get('pagemunber'); 
		$this->session->set_userdata('pagemunber', $didgit);
		echo json_encode(['sessions' => $didgit]);
	}

	public function meter_bookings()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('twitter-bootstrap');
			$crud->set_table('addressbook');

			//$crud->where('booked_meter','2');
			//$crud->set_relation('booked_meter','booked_meter','booked_meter_words');
			$crud->columns('ACCOUNT_NUMBER','MPAN', 'MPRN', 'ELEC_START_DATE', 'CUSTOMER_NAME', 'CONTACT_ADDRESS_1','CONTACT_POST_CODE', 'CONTACT_TELEPHONE','EMAIL', 'Outcome','Agent', 'Notes', 'Date');
			$crud->display_as('ACCOUNT_NUMBER','ACCOUNT NUMBER')
				 ->display_as('ELEC_START_DATEr','ELEC START DATE')
				 ->display_as('CUSTOMER_NAME', 'CUSTOMER NAME')
				 ->display_as('CONTACT_ADDRESS_1','CONTACT ADDRESS 1')
				 ->display_as('CONTACT_POST_CODE', 'CONTACT POST CODE')
				 ->display_as('CONTACT_TELEPHONE', 'CONTACT TELEPHONE');
			$crud->set_subject('Meter Booking');
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_print();

			$crud->order_by('ELEC_START_DATE','desc');
		
			$output = $crud->render();
			if ( empty($this->session->userdata('login_data'))) {
				redirect('/index.php/user/login/');
			} else {
				if (isset($savedsearch)) {
				    $this->_example_output($output, $savedsearch);
				} else {
				    $this->_example_output($output);
				}
			}

			
		
	}

}
