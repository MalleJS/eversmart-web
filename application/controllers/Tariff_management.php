<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tariff_management extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        $admin_data['page_ref'] = 'tariffs';
        $admin_data['page_title'] = 'Tariff Management';
        $this->load->view('admin/tariffs',$admin_data);
    }

}
