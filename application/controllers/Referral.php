<?php

/**
 * Class Referral
 */
class Referral extends ESE_Controller
{
    /**
     * Referral constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('curl/curl_requests_saasquatch');
        $this->load->model('user_modal');
        $this->load->model('saas_model');
    }

    /**
     * Retrieves Saasquatch user from Saasquatch
     *
     * @return string
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $saasquatch = $this->user_modal->get_referral_details($this->account_id->junifer);
        $saas = $this->curl_requests_saasquatch->saasquatch_get_user($saasquatch['id']);
        $refs = $this->curl_requests_saasquatch->saasquatch_lookup_referrals($saasquatch['id']);
        $ese_link = $this->saas_model->get_ese_share_link($this->account_id->junifer);

        $data = array_merge($ese_link, array_merge($saas, $refs));

        // Check its not an empty array
        if (!empty($data)) {
            // return success and data
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        } else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => 'No customer information found']);
        }
    }

    /**
     * Credits Junifer account and debits Saasquatch account
     *
     * @return string
     */
    function redeem_credit()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $data = json_decode( file_get_contents( 'php://input' ), true );

        $customer = $this->user_modal->get_basic_customer_info($this->account_id->junifer);
        $response = $this->saas_model->get_referral_log_id($customer['id']);

        if (!empty($response)) {

            $referral_data = [
                'referral_log_id' => $response[0]['referral_log_id'],
                'type' => $response[0]['referrer_customer_id'] == $customer['id'] ? 'referrer' : 'referred'
            ];

            $update = $this->saas_model->saas_customer_redeemed($referral_data);

            if (!empty($update)) {

                if (isset($data['phone'])) {
                    $this->user_modal->update_customer_info_by_account_id(['phone1' => $data['phone']], $this->account_id->junifer);
                }

                echo json_encode(['success' => true, 'error' => false, 'token' => $this->get_latest_token()]);

            } else {
                echo json_encode(['success' => false, 'error' => 'Sorry, no reward available.', 'token' => $this->get_latest_token()]);
            }

        } else {
            echo json_encode(['success' => false, 'error' => 'Sorry, no reward available.', 'token' => $this->get_latest_token()]);
        }
    }
}
