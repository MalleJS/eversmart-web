<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
class Csv extends CI_Controller {
 
    function __construct() {
        parent::__construct(); 
        $this->load->model('csv_model');
        $this->load->library('csvimport');
    }
 
    function index() {
      $data['addressbook'] = $this->csv_model->get_addressbook();
      $data['page_ref'] = 'csv_import';
      $data['page_title'] = 'Csv Import';
        $this->load->view('csvindex', $data);
    }
 
    function importcsv() {
        $data['addressbook'] = $this->csv_model->get_addressbook();
        $data['error'] = '';    //initialize image upload error array to empty
 
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = 204800;
 
        $this->load->library('upload', $config);
 
 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();
 
            $this->load->view('csvindex', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path =  './upload/'.$file_data['file_name'];
 
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $row) {
                    $insert_data = array(
                        'ACCOUNT_NUMBER'=>$row['ACCOUNT_NUMBER'],
                        'MPAN'=>$row['MPAN'],
                        'MPRN'=>$row['MPRN'],
                        'ELEC_START_DATE'=>$row['ELEC_START_DATE'],
                        'CUSTOMER_NAME'=>$row['CUSTOMER_NAME'],
                        'CONTACT_ADDRESS_1'=>$row['CONTACT_ADDRESS_1'],
                        'CONTACT_POST_CODE'=>$row['CONTACT_POST_CODE'],
                        'CONTACT_TELEPHONE'=>$row['CONTACT_TELEPHONE'],
                        'EMAIL'=>$row['EMAIL']
                    );

                    $accountnumber = $row['ACCOUNT_NUMBER'];
                    $MPAN = $row['MPAN'];
                    $MPRN = $row['MPAN'];
                    $ELEC_START_DATE = $row['ELEC_START_DATE'];
                    $CUSTOMER_NAME = $row['CUSTOMER_NAME'];
                    $CONTACT_ADDRESS_1 = $row['CONTACT_ADDRESS_1'];
                    $CONTACT_POST_CODE = $row['CONTACT_POST_CODE'];
                    $CONTACT_TELEPHONE = $row['CONTACT_TELEPHONE'];
                    $EMAIL = $row['EMAIL'];


    
        
                    $email = $row['EMAIL'];
                    $result = $this->csv_model->checkEmail($email);
                   
                    if(empty($result))  {
                    $this->csv_model->insert_csv($insert_data);
                    $msg = "Data Insert Successfully";}
                    else
                    {
                     $msg = "Email Already Exists";
                     }
                
                     $this->session->set_flashdata('msg', $msg);
            
                    
                    
            
                    
                }
                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect(base_url().'csv');
                //echo "<pre>"; print_r($insert_data);
            } else 
                $data['error'] = "Error occured";
                $data['page_ref'] = 'csv_import';
                $data['page_title'] = 'Csv Import';
              
                $this->load->view('csvindex', $data);
            }
 
        } 
 
}
/*END OF FILE*/