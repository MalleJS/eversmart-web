<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeservices extends ESE_Controller {
    
    private $environment = 'test';

    function __construct()
    {
        parent::__construct();
        //$this->load->model('test');
        $this->load->model('salesforce_model');
        $this->load->model('user_modal');
    }

    /*
     * First step of the Home Services Quote process
     *
     * @returns CI_View
     */
    function step_one()
    {
        $data['content'] = 'home_services/postcode';
        $this->load->view($this->template, $data);
    }

    /*
     * Load the Home Services landing page
     *
     * @returns $data
     */
    function index()
    {
        $data['bodyClasses'] = $this->body_classes;
        $data['footer_options'] = ['hide_consumer_checklist' => true];
        $data['scripts'] = [base_url().'assets/js/new-world/home-services/landing-page.js'];
        $data['stylesheets'] = ['assets/css/new-world/home-services/landing-page.css'];
        $data['content'] = 'new-world/home-services/landing-page';
        $this->load->view($this->template, $data);
    }

    /*
     * Load the Quote page
     *
     * @returns $data
     */
    function quote()
    {
        $data['bodyClasses'] = $this->body_classes;
        $data['footer_options'] = ['hide_consumer_checklist' => true];
        $data['scripts'] = [
            base_url().'assets/js/new-world/home-services/quotation-page.js',
            'https://'.$this->environment.'.protectedpayments.net/asperato-form-1.0.js'
        ];
        $data['stylesheets'] = [base_url().'assets/css/new-world/home-services/quotation-page.css'];
        $data['content'] = 'new-world/home-services/quotation-page';
        $this->load->view($this->template, $data);
    }

    /*
     * Lookup address using Experian API & return
     *
     * @returns Array
     */
    function get_address_list()
    {
        $this->load->library('curl');
        $json = json_decode( file_get_contents( 'php://input' ), true );
        $body = ['pcod' => $json['postcode'] ];
        $response = $this->curl->curl_request('/v3.1/partner-resources/'.$this->affiliate_id.'/mpas/addresses-by-pcod/', $body, 'post' );

        $address_list = $this->parseHttpResponse($response);
        $address_data = [
            'elec' => isset($address_list['body']['data']['addresses'])&&!empty($address_list['body']['data']['addresses'])?$address_list['body']['data']['addresses']:[]
        ];

        echo json_encode($address_data);
    }

   /*
    * Get all Salesforce Products
    *
    * @returns Array
    */
    function get_salesforce_products()
    {
        $this->load->model('salesforce_model');
        echo json_decode($this->salesforce->get_insurance_products());
    }

    /*
     * Get initial period for calculating monthly cover payments
     *
     * @returns Array
     */
    function initial_period(){
        echo json_encode([ 'initial_period' => 3 ]);
    }

    /*
     * Compiles unit tests for all units relating to this controller
     *
     * @returns View::__construct()
     */
    function unit_test()
    {
        $data['tests'] = [
            $this->test->test_data_type($this->get_salesforce_products(), 'array', 'Salesforce API Products get_salesforce_products()'),
            $this->test->test_data_type($this->get_address_list(), 'array', 'Experian API get_address_list()')
        ];
        $this->load->view('tests/unit_test_report', $data);
    }

    /**
     * Use first line and postcode to verify whether customer already exists
     * If they do already exist we will give them the option to login and pre-populate form fields
     *
     * @param null
     * @return string JSON array with boolean
     */
    function existing_customer_check()
    {
        //$json = json_decode( file_get_contents( 'php://input' ), true );
        //$customer_check = $this->user_modal->customer_lookup_by_address($json);
        $customer_check = 1;
        echo json_encode([ 'customer_exists' => $customer_check>0?true:false]);
    }

    /**
     * Use email and password to get details which will pre-populate front end form
     *
     * @param null
     * @return string JSON array with customer details
     */
    function login_customer_details()
    {

        $json = json_decode( file_get_contents( 'php://input' ), true );
        $customer_details = $this->user_modal->login_check($json);
        
        if(!empty($customer_details)) {

            $customer_data = [
                'title' => $customer_details['title'],
                'forename' => $customer_details['forename'],
                'surname' => $customer_details['surname'],
                'email' => $customer_details['email'],
                'phone1' => $customer_details['phone1'],
                'dateOfBirth' => $customer_details['dateOfBirth'],
                'first_line_address' => $customer_details['first_line_address'],
                'dplo_address' => $customer_details['dplo_address'],
                'town_address' => $customer_details['town_address'],
                'city_address' => $customer_details['city_address'],
                'address_postcode' => $customer_details['address_postcode']
            ];

            echo json_encode(['success' => true, 'data' =>  $customer_data]);
        }
        else {

            echo json_encode(['success' => false, 'msg' => 'Invalid login details entered.']);
        }
    }


    /**
     * Insert homeservices customer information into the database
     *
     * @params null
     * @returns string json
     */
    function enroll_customer(){

        $data = json_decode( file_get_contents( 'php://input' ), true );

        // Set up billing address data array
        $billing_address_data = [
            'billing_address_street' => $data['billing_address_street'],
            'billing_address_town' => $data['billing_address_town'],
            'billing_address_city' => $data['billing_address_city'],
            'billing_address_postcode' => $data['billing_address_postcode']
        ];

        // Set up risk address data array
        $risk_address_data = [
            'risk_address_line_1' => $data['risk_address_line_1'],
            'risk_address_town' => $data['risk_address_town'],
            'risk_address_county' => $data['risk_address_county'],
            'risk_address_postcode' => $data['risk_address_postcode']
        ];

        // Save customer and catch id
        $customer_id = $this->home_services_modal->insert_customer($data);
        if($customer_id>0){

            // Save Billing Address
            $billing_address_data['customer_id'] = $customer_id;
            $billing_address_data = json_encode($billing_address_data);
            $this->home_services_modal->insert_billing_address($billing_address_data);

            // Save Risk Address
            $risk_address_data['customer_id'] = $customer_id;
            $risk_address_data = json_encode($risk_address_data);
            $this->home_services_modal->insert_risk_address($risk_address_data);

        }
    }
}
