<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29/08/2018
 * Time: 08:26
 */
require('fpdf/fpdf.php');

class Myfpdf extends Fpdf {
    function __construct () {
        parent::__construct();
        $CI =& get_instance();
    }


    // Page header
    function Header()
    {
        // Logo
        $this->Image('assets/images/pdf_header.png',0,0, 215);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(80);
        // Line break
        $this->Ln(60);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $CurrentY = $this->GetY();

        // Arial italic 8
        //$this->SetFont('Arial','I',8);
        // Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        $this->Image('assets/images/pdf_footer.png',50,$CurrentY, 120);

    }


    //MultiCell with bullet
    function MultiCellBlt($w, $h, $blt, $txt, $border=0, $align='J', $fill=false)
    {
        //Get bullet width including margins
        $blt_width = $this->GetStringWidth($blt)+$this->cMargin*2;

        //Save x
        $bak_x = $this->x;

        //Output bullet
        $this->Cell($blt_width,$h,$blt,0,'',$fill);

        //Output text
        $this->MultiCell($w-$blt_width,$h,$txt,$border,$align,$fill);

        //Restore x
        $this->x = $bak_x;
    }

}