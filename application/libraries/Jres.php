<?php

class Jres
{

	//array $data=[], 
	//'data'=>$data,
	public function  success_stripe (array $data=[], $msg="")
	{

		header('content-type:application/json');



		echo json_encode(['api_status'=>"1",'data'=>$data,'status_message'=>$msg]); exit;
	}

	public function  success (array $data=[], $msg="")
	{
		header('content-type:application/json');
		echo json_encode(['api_status'=>"1",'data'=>$data,'status_message'=>$msg]); exit;
	}
	public function  failure( $msg="")
	{
		header('content-type:application/json');
		echo json_encode(['api_status'=>'0','status_message'=>$msg]); exit;
	}

	public function  successotp ($data, $msg="")
	{
		header('content-type:application/json');
		if( empty($data) )
		{
			echo json_encode(['api_status'=>'1','status_message'=>$msg]); exit;
		}
		else
		{
			echo json_encode(['api_status'=>'1','otp'=>$data,'status_message'=>$msg]); exit;
		}
	}

	public function success_message($msg)
	{
		header('content-type:application/json');
		echo json_encode(['api_status'=>'1','status_message'=>$msg]); exit;
	}
}
