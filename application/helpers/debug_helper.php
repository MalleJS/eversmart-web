<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('debug'))
{
	function debug( $arr, $exit='' )
	{
		echo "<pre>"; print_r($arr);
		if( $exit ==1 )
		{
			exit;
		}
	}
}