<?php

require APPPATH.'/models/curl/Curl_model_sagepay.php';

/**
 * Class Curl_requests_sagepay
 */
Class Curl_requests_sagepay extends Curl_model_sagepay
{

    /**
     * Curl_requests_sagepay constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Generates and returns Sagepay Merchant Session Key
     *
     * @return mixed
     */
    function sagepay_generate_msk()
    {
        $data = json_encode(['vendorName' => 'sandbox']);
        $keygen = $this->sagepay_curl_request('POST', '/merchant-session-keys/', $data);
        $keygen = json_decode($keygen, true);
        return $keygen['merchantSessionKey'];
    }

    /**
     * Generates and returns Card Identifier
     *
     * @param $name
     * @param $number
     * @param $exp
     * @param $cvc
     * @return mixed
     */
    function sagepay_generate_ci($data, $msk)
    {
        $body = json_encode([
            'cardDetails' => [
                'cardholderName' => $data['forename']. ' ' .$data['surname'],
                'cardNumber' => $data['card_num'],
                'expiryDate' => $data['card_exp'],
                'securityCode' => $data['card_cvc']
            ]
        ]);

        $ci = $this->sagepay_curl_request('POST', '/card-identifiers/', $body, 'ci', $msk);
        $ci = json_decode($ci, true);
        return $ci['cardIdentifier'];
    }

    /**
     * @params $data
     */
    function sagepay_process_transaction($data)
    {
        $ref = base64_encode($data['id'].'|'.time());
        $ref = str_replace("=", "", $ref);

        $body = json_encode([
            'paymentMethod' => [
                'card' => [
                    'merchantSessionKey' => $data['msk'],
                    'cardIdentifier' => $data['ci']
                ],
            ],
            'transactionType' => 'Payment',
            'vendorTxCode' => $ref,
            'amount' => $data['amount'],
            'currency' => 'GBP',
            'customerFirstName' => $data['forename'],
            'customerLastName' => $data['surname'],
            'billingAddress' => [
                'address1' => $data['first_line_address'],
                'city' => $data['town_address'],
                'postalCode' => $data['address_postcode'],
                'country' => 'GB',
            ],
            'description' => 'Top-up: ' . $data['forename'] . ' ' . $data['surname'] . date('d-m-Y')
        ]);
        $transaction = $this->sagepay_curl_request('POST', '/transactions/', $body);
        $transaction = json_decode($transaction, true);
        if (!empty($transaction)) {
            return $transaction;
        }
    }

    /**
     * Helper to remove unwanted characters from SagePay response
     *
     * @param $input
     * @param $chars
     * @return mixed
     */
    function remove_unwanted($input, $chars)
    {
        $output = str_replace($chars, "", json_encode($input));
        return $output;
    }

}
