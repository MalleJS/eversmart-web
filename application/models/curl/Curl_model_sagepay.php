<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_sagepay
 */
Class Curl_model_sagepay extends Curl_model {

    /**
     * Curl_model_sagepay constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Defines base SagePay curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function sagepay_curl_request($method, $url, $data, $type = 'json', $ci_auth = null)
    {
        $credentials = [];
        $names = file(APPPATH.'/config/credentials/SagePay.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        $optional_headers = [
            'Content-Type: application/json'
        ];

        if ($type=='ci') {
            $credentials[0] = 'Authorization: Bearer '. $ci_auth;
        }

        return $this->callAPI($method, $credentials[1].$url, $data, $credentials, $optional_headers);
    }


}
