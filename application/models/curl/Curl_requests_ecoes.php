<?php

require APPPATH . '/models/curl/Curl_model_ecoes.php';

/**
 * Class Curl_requests_ecoes
 */
class Curl_requests_ecoes extends Curl_model_ecoes
{

    /**
     * Curl_requests_ecoes constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieves Address list by postcode
     *
     * @param $postcode
     * @return array|string
     */
    function ecoes_get_addresses_postcode($postcode)
    {
        $data['Postcode'] = $postcode;
        $res = json_decode($this->ecoes_curl_request('POST', 'https://www.ecoes.co.uk/WebServices/Service/ECOESApi.svc/RESTful/JSON', '/SearchUtilityAddress', $data), true);
        if ($res != NULL) {
            $res = $res['Results'][0];

            if ($res['Errors'] == []) {
                $addresses = [];
                // var_dump($res['UtilityAddressMatches']);
                foreach ($res['UtilityAddressMatches'] as $address) {

                    $a = $address['AddressDetails'];
                    $tmp = [];
                    foreach ($a as $part) {
                        if ($part['Key'] == "mpan_core") {
                            $tmp['mpan'] = $part['Value'];
                        } elseif ($part['Value'] != null) {
                            if (strpos($part['Key'], "address") > -1) {
                                @$tmp['address'] .= "{$part['Value']} ";
                            } elseif ($part['Key'] == "postcode") {
                                @$tmp['postcode'] = $part['Value'];
                            }
                        }
                    }
                    $addresses[] = $tmp;
                }
                return $addresses;
            }
        } 
        return false;
    }

    /**
     * Retrieves Elec meter details by mpan
     *
     * @param $mpan
     * @return array|string
     */
    function ecoes_get_technical_mpan($mpan)
    {
        $data['Mpan'] = $mpan;
        $res = json_decode($this->ecoes_curl_request('POST', 'https://www.ecoes.co.uk/WebServices/Service/ECOESApi.svc/RESTful/JSON', '/GetTechnicalDetailsByMpan', $data), true);
        if ($res != NULL) {
            $res = $res['Results'][0];

            if ($res['Errors'] == []) {
                $r = [];
                $meter = $res['UtilityMatches'][0]['Meters'][0]['MeterDetails'];
                $r['meter_mpan_core'] = $meter[0]['Value'];
                $r['meter_serial_number'] = $meter[1]['Value'];
                $r['meter_type'] = $meter[3]['Value'];
                return $r;
            }
        }
        return false;
    }
}
