<?php

require APPPATH . '/models/curl/Curl_model_junifer.php';

/**
 * Class Curl_requests_junifer
 */
class Curl_requests_junifer extends Curl_model_junifer
{

    /**
     * Curl_requests_junifer constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieves customer data from Junifer
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function junifer_customer_data($id, $return_type = 'json')
    {
        $get_account = $this->junifer_curl_request('GET', '/rest/v1/customers/', $id, false);
        $get_account = json_decode($get_account);

        $data['forename'] = isset($get_account->forename) ? $get_account->forename : '';
        $data['surname'] = isset($get_account->surname) ? $get_account->surname : '';
        $data['email'] = isset($get_account->primaryContact->email) ? $get_account->primaryContact->email : '';
        $data['phone1'] = isset($get_account->primaryContact->phoneNumber1) ? $get_account->primaryContact->phoneNumber1 : '';

        if ($return_type == 'array') {
            return $data;
        } else {
            echo json_encode(['error' => false, 'data' => $data]);
        }
    }

    /**
     * Updates customer data from Junifer
     *
     * @param $id
     * @param $data
     * @param string $return_type
     * @return array|string
     */
    function junifer_update_customer_data($id, $data, $return_type = 'json')
    {
        // Get the existing primary contact
        $PrimaryContact = $this->junifer_get_primary_contact($id, 'array');

        // Merge the new phone number over the existing array and encode it
        $data = json_encode(array_merge($PrimaryContact, $data));

        // Now update the primary contact
        $UpdateAccount = $this->junifer_curl_request('PUT', '/rest/v1/customers/', $id . '/primaryContact', $data);
        $Response = json_decode($UpdateAccount, true);

        if ($return_type == 'array') {
            return $Response;
        } else {
            echo json_encode(['error' => false, 'data' => $Response]);
        }
    }

    /**
     * Retrieves customers contact details from Junifer
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function junifer_get_primary_contact($id, $return_type = 'json')
    {
        $get_account = $this->junifer_curl_request('GET', '/rest/v1/customers/', $id, false);
        $get_account = json_decode($get_account, true);

        $data = $get_account['primaryContact'];

        if ($return_type == 'array') {
            return $data;
        } else {
            echo json_encode(['error' => false, 'data' => $data]);
        }
    }

    /**
     * Retrieves customer data from Junifer
     *
     * @param $email
     * @return string
     */
    function junifer_get_id_by_email($email)
    {
        $get_account = $this->junifer_curl_request('GET', '/rest/v1/uk/accounts?email=', $email, false);
        $get_account = json_decode($get_account, true);
        return $get_account['results'][0]['id'];
    }

    /**
     * Retrieves customer billing address from Junifer
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function junifer_get_billing_address($id, $return_type = 'array')
    {
        $address = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id, false);
        $address = json_decode($address, true);

        $Address = array(
            'first_line_address' => $address['billingAddress']['address1'],
            'address_postcode' => $address['billingAddress']['postcode']
        );

        unset($address['billingAddress']['address1']);
        unset($address['billingAddress']['postcode']);

        $AddressData = array_values($address['billingAddress']);
        $ArrayCount = count($address['billingAddress']);

        if ($ArrayCount == 2) {
            $Address['town_address'] = $AddressData[0];
            $Address['city_address'] = $AddressData[1];
        } else if ($ArrayCount == 3) {
            $Address['town_address'] = $AddressData[0] . ', ' . $AddressData[1];
            $Address['city_address'] = $AddressData[2];
        } else if ($ArrayCount == 4) {
            $Address['town_address'] = $AddressData[0] . ', ' . $AddressData[1];
            $Address['city_address'] = $AddressData[2] . ', ' . $AddressData[3];
        }

        if ($return_type == 'array') {
            return $Address;
        } else {
            echo json_encode(['error' => false, 'data' => $Address]);
        }
    }

    /**
     * Retrieves customer balance from Junifer
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function junifer_get_balance($id, $return_type = 'json')
    {
        $get_balance = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id, false);
        $get_balance = json_decode($get_balance);

        $Date = explode('T', $get_balance->createdDttm);

        $BalanceData = array();
        isset($get_balance->balance) ? $BalanceData['balance'] = $get_balance->balance : '';
        isset($get_balance->createdDttm) ? $BalanceData['date'] = $Date[0] : '';

        if ($return_type == 'array') {
            return $BalanceData;
        } else if ($return_type == 'value') {
            return $BalanceData['amount'];
        } else {
            echo json_encode(['error' => false, 'data' => $BalanceData]);
        }
    }

    /**
     * Updates customer balance in Junifer, second parameter must be either 'credit' or 'debit'
     *
     * @param $id
     * @param $transaction
     * @param $amount
     * @return string
     */
    function junifer_adjust_balance($id, $transaction, $amount)
    {
        if ($transaction == 'credit') {
            $acc_reason = 'accountCreditReasonName';
            $acc_trans = '/accountCredits';
        } else if ($transaction == 'debit') {
            $acc_reason = 'accountDebitReasonName';
            $acc_trans = '/accountDebits';
        }

        $data = json_encode(['grossAmount' => $amount, 'salesTaxName' => 'Standard VAT', $acc_reason => 'Saasquatch Referral']);
        $adjust_balance = $this->junifer_curl_request('POST', '/rest/v1/accounts/', $id . $acc_trans, $data);
        $adjust_balance = json_encode($adjust_balance);

        if (isset($adjust_balance['errorSeverity']) && $adjust_balance['errorSeverity'] == 'Error') {
            return json_encode(['status' => 'fail', 'data' => $adjust_balance['errorDescription']]);
        } else {
            return json_encode(['status' => 'success']);
        }
    }

    /**
     * Retrieves customer bills from Junifer
     *
     * @param $id
     * @return array
     */
    function junifer_get_bills($id)
    {
        $bills = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/bills', false);
        $bills = json_decode($bills, true);

        $data = array();
        if (!empty($bills['results'])) {

            foreach ($bills['results'] as $bill) {

                if ($bill['supersededFl'] == false) {

                    $data[] = [
                        'id' => $bill['id'],
                        'invoice_period' => date("d/m/Y", strtotime($bill['periodFrom'])) . ' - ' . date("d/m/Y", strtotime($bill['periodTo'])),
                        'issue_date' => date("d/m/Y", strtotime($bill['issueDt'])),
                        'timestamp' => strtotime(str_replace('/', '-', $bill['issueDt'])),
                        'gross_amount' => $bill['grossAmount']
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * Displays customer bill
     *
     * @param $billid
     */
    function junifer_view_bill($billid)
    {
        $get_bill = $this->junifer_curl_request('GET', '/rest/v1/billFiles/', $billid . '/image', false, 'pdf');
        echo $get_bill;
    }

    /**
     * Retrieves customer payments from Junifer
     *
     * @param $id
     * @return array
     */
    function junifer_get_payments($id)
    {
        $payments = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/payments', false);
        $payments = json_decode($payments, true);
        return $payments;
    }

    /**
     * Retrieves customer messages from Junifer
     *
     * @param $id
     * @return array
     */
    function junifer_get_messages($id)
    {
        $messages = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/communications', false);
        $messages = json_decode($messages, true);
        return $messages;
    }

    /**
     * Displays customer message
     *
     * @param $msgid
     */
    function junifer_view_message($msgid)
    {
        $get_msg = $this->junifer_curl_request('GET', '/rest/v1/communicationsFiles/', $msgid . '/image', false, 'pdf');
        echo $get_msg;
    }

    /**
     * Retrieves customer payments from Junifer
     *
     * @param $id
     * @param $from_date
     * @param $to_date
     * @param $energy
     * @return array
     */
    function junifer_get_meterreads($id, $from_date, $to_date, $energy)
    {

        $meter_point = $this->junifer_get_meterpoint($id);

        $data = array();

        if ($meter_point['meterpoint_elec_id']) {
            $elec_readings = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point['meterpoint_elec_id'] . '/readings?fromDt=' . $from_date . '&toDt=' . $to_date, false);

            $elec_readings = json_decode($elec_readings, true);

            $today = date('Y-m-d');
            $get_meter_identifier = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point['meterpoint_elec_id'] . '/meterStructure?effectiveDt=' . $today, false);
            $get_meter_identifier = json_decode($get_meter_identifier, true);

            if (!empty($get_meter_identifier['meters'])) {
                $meter_identifier_elec = $get_meter_identifier['meters'][0]['identifier'];
                $meter_digits_elec = $get_meter_identifier['meters'][0]['registers'][0]['digits'];
                $data['meter_identifier_elec'] = $meter_identifier_elec;
                $data['meter_digits_elec'] = $meter_digits_elec;
            }

            $data['meterpoint_elec'] = $meter_point['meterpoint_elec_id'];
            if (!empty($elec_readings)) {
                $data['elec_readings'] = $elec_readings;
            } else {
                $data['elec_readings'] = [];
            }
        }

        if ($energy == 'dual') {

            if ($meter_point['meterpoint_gas_id']) {
                $gas_readings = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point['meterpoint_gas_id'] . '/readings?fromDt=' . $from_date . '&toDt=' . $to_date, false);

                $gas_readings = json_decode($gas_readings, true);

                $today = date('Y-m-d');
                $get_meter_identifier = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point['meterpoint_gas_id'] . '/meterStructure?effectiveDt=' . $today, false);
                $get_meter_identifier = json_decode($get_meter_identifier, true);

                if (!empty($get_meter_identifier['meters'])) {
                    $meter_identifier_gas = $get_meter_identifier['meters'][0]['identifier'];
                    $meter_digits_gas = $get_meter_identifier['meters'][0]['registers'][0]['digits'];
                    $data['meter_identifier_gas'] = $meter_identifier_gas;
                    $data['meter_digits_gas'] = $meter_digits_gas;
                }

                $data['meterpoint_gas'] = $meter_point['meterpoint_gas_id'];
                if (!empty($gas_readings)) {
                    $data['gas_readings'] = $gas_readings;
                } else {
                    $data['gas_readings'] = [];
                }
            }
        }

        return $data;
    }

    /**
     * Add / change meter points
     *
     * @param $id integer
     * @param $data array
     * @return string - json array
     */
    function junifer_additional_meter_point($id, $data)
    {
        $data = json_encode($data);

        $enroll = $this->junifer_curl_request('POST', '/rest/v1/accounts/', $id . '/enrolMeterPoints', $data);
        $enroll_response = json_decode($enroll, true);

        if (isset($enroll_response['errorSeverity']) && $enroll_response['errorSeverity'] == 'Error') {
            return json_encode(['status' => 'fail']);
        } else {
            return json_encode(['status' => 'success']);
        }
    }

    /**
     * Sends new meter reading to Junifer
     *
     * @param $data
     * @return string - json array
     */
    function junifer_submit_readings($read_data)
    {
        $data = json_encode([
            'readings' => [
                [
                    'meterIdentifier' => $read_data['meter_identifier'],
                    'readingDttm' => date('Y-m-d'),
                    'sequenceType' => 'Normal',
                    'source' => 'Customer',
                    'quality' => 'Normal',
                    'cumulative' => $read_data['reading']
                ]
            ]
        ]);

        $reading_submit = $this->junifer_curl_request('POST', '/rest/v1/meterPoints/', $read_data['meterpoint_id'] . '/readings', $data);
        return $reading_submit;
    }

    /**
     * Sends new meter reading to Junifer (without meter technical details)
     *
     * @param $read_data
     * @return string
     */
    function junifer_submit_readings_without_mtds($read_data)
    {
        $data = json_encode([
            'readingDt' => date('Y-m-d'),
            'sequenceType' => 'Normal',
            'source' => 'Customer',
            'quality' => 'Normal',
            'registerReads' => [
                'reading' => $read_data['reading'],
                'readingType' => 'Standard'
            ]
        ]);

        $reading_submit = $this->junifer_curl_request('POST', '/rest/v1/meterPoints/', $read_data['meterpoint_id'] . '/readingsWithoutMtds', $data);
        return $reading_submit;
    }

    /**
     * Look up meter point ids by account id
     *
     * @param $id integer
     * @return array $data
     */
    function junifer_get_meterpoint($id)
    {
        $agreements = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/agreements', false);
        $agreements = json_decode($agreements, true);

        $data = array();

        if (!empty($agreements['results'])) {
            for ($r = 0; $r < count($agreements['results']); $r++) {
                if ($agreements['results'][$r]['products'][0]['type'] == 'Electricity Supply') {
                    if (isset($agreements['results'][$r]['products'][0]['assets'][0]['id'])) {
                        $data['meterpoint_elec_id'] = $agreements['results'][$r]['products'][0]['assets'][0]['id'];
                    }
                } elseif ($agreements['results'][$r]['products'][0]['type'] == 'Gas Supply') {
                    if (isset($agreements['results'][$r]['products'][0]['assets'][0]['id'])) {
                        $data['meterpoint_gas_id'] = $agreements['results'][$r]['products'][0]['assets'][0]['id'];
                    }
                }
            }
        }

        return $data;
    }


    /**
     * Get the MPAN or MPRN using Meter Point ID
     *
     * @param $meter_point_id string
     * @return string - MPAN / MPRN
     */
    function junifer_get_mpan_mprn($meter_point_id)
    {
        $meter_info = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point_id, NULL);
        $meter_info = json_decode($meter_info, true);
        return $meter_info['identifier'];
    }


    /**
     * Get payment schedule information from junifer - includes nextPaymentDt
     *
     * @param $id string
     * @return array
     */
    function junifer_payment_details($id)
    {

        $payment = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/paymentSchedulePeriods', NULL);
        return json_decode($payment, true);
    }

    /**
     * Get payment schedule information from junifer - includes nextPaymentDt
     *
     * @param $id string
     * @return array
     */
    function junifer_supply_status($meter_point_id)
    {
        $supply_status = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point_id . '/supplyStatusHistory', NULL);
        return json_decode($supply_status, true);
    }


    /**
     * Get agreement info
     *
     * @param $id string
     * @return array
     */
    function junifer_agreements($id)
    {
        $agreement = $this->junifer_curl_request('GET', '/rest/v1/accounts/', $id . '/agreements', NULL);
        return json_decode($agreement, true);
    }


    /**
     * Get supply start date from junifer
     *
     * @param $id string
     * @return array
     */
    function junifer_supply_start_date($meter_point_id)
    {
        $supply_status = $this->junifer_curl_request('GET', '/rest/v1/meterPoints/', $meter_point_id, NULL);
        $supply_status = json_decode($supply_status, true);
        return $supply_status['supplyStartDate'];
    }


    /**
     * Get meter serial info by MPAN
     *
     * @param $postcode array
     * @param $mpan array
     * @return value
     */
    function junifer_elec_meter_serial($postcode = null, $mpan = null)
    {
        $data = ['postcode' => $postcode];
        $elec_meter_details = $this->junifer_curl_request('GET', '/rest/v1/ecoes/mpans/', '', $data);
        $elec_meter_details = json_decode($elec_meter_details, true);

        foreach ($elec_meter_details['results'] as $key => $elec_meter_details) {
            if ($elec_meter_details['mpanCore'] == $mpan) {
                return $elec_meter_details['meterIdSerialNumber'];
            }
        }
    }


    /**
     * Get meter serial info by MPRN
     *
     * @param $postcode array
     * @param $mprn array
     * @return array
     */
    function junifer_gas_meter_serial($postcode = null, $mprn = null)
    {

        $data = ['postcode' => $postcode];
        $gas_meter_details = $this->junifer_curl_request('GET', '/rest/v1/des/mprns/', '', $data);
        $gas_meter_details = json_decode($gas_meter_details, true);

        foreach ($gas_meter_details['results'] as $key => $gas_meter_details) {
            if ($gas_meter_details['mprn'] == $mprn) {
                return $gas_meter_details['meterSerialNumber'];
            }
        }
    }

    /**
     * Lookup MPANs for Postcode
     *
     * @param $postcode array
     * @return array
     */
    function junifer_elec_addresses_pcod($postcode = null)
    {
        $data = ['postcode' => $postcode, 'formatAddress' => "true"];
        $elec = $this->junifer_curl_request('GET', '/rest/v1/ecoes/mpans', '', $data);
        $elec = json_decode($elec, true);
        return $this->sortAddresses($elec['results']);
    }

    /**
     * Lookup MPRNs for Postcode
     *
     * @param $postcode array
     * @return array
     */
    function junifer_gas_addresses_pcod($postcode = null)
    {
        $data = ['postcode' => $postcode, 'formatAddress' => "true"];
        $gas = $this->junifer_curl_request('GET', '/rest/v1/des/mprns/', '', $data);
        $gas = json_decode($gas, true);
        return $this->sortAddresses($gas['results']);
    }

    private function sortAddresses($addresses){
        usort($addresses, function ($a, $b) {
            return strnatcasecmp(implode(" ", [$a['address1'],@$a['address2'],@$a['town']]), implode(" ", [$b['address1'],@$b['address2'],@$b['town']]));
        });
        return $addresses;
    }
}
