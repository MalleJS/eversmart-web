<?php

require APPPATH.'/models/curl/Curl_model_redfish.php';

/**
 * Class Curl_requests_redfish
 */
Class Curl_requests_redfish extends Curl_model_redfish {

    /**
     * Curl_requests_redfish constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieves invoice data from redfish
     *
     * @param $id
     * @return string json
     */
    function redfish_customer_invoice($id)
    {
       return $this->redfish_curl_request('GET', '/Invoice/', $id, false);
    }

    /**
     * Retrieves meter reads data from redfish
     *
     * @param $account_number
     * @param $mpan
     * @return bool|string
     */
    function redfish_customer_meter_reading($account_number, $energy, $meterpoint)
    {
        $energy == 'mpan' ? $type = '/Electricity/' : $type = '/Gas/';
        return $this->redfish_curl_request('GET', '/MeterReading/', $account_number.$type.$meterpoint, false);
    }

    /**
     * Reset password return success array if customer exists, or fail
     *
     * @param string $email
     * @return string - json encoded array
     */
    function redfish_reset_password($email)
    {
        $data['Email'] = $email;
        $data = json_encode($data);

        $reset = $this->redfish_curl_request('POST', '/ResetPassword', null, $data, 'json', null, false);
        $response = json_decode($reset,true);

        if( $response['Message'] == 'SUCCESS' ) {
            return json_encode([ 'error' => '0', 'msg'=>'Reset password email sent to ' . $email]);
        } else {
            return json_encode([ 'error' => '1', 'msg'=>'Email is not recognised by our system' ]);
        }
    }

    /**
     * Get Redfish Account ID using email and password
     *
     * @param string $email
     * @param string $password
     * @return string json
     */
    function login_check($email = null, $password = null)
    {
        $encoded_login = base64_encode($email.':'.$password);
        return $this->redfish_curl_request('POST', '/Login', null, null, 'login', $encoded_login);
    }

    /**
     * Get all Redfish Account information for a customer
     *
     * @param string $id
     * @return string json
     */
    function redfish_my_account($id)
    {
        return $this->redfish_curl_request('GET', '/Account/', $id, null, 'json');
    }

    /**
     * Save meter reading
     *
     * @param string $id
     * @param array $data
     * @param string $mpan
     * @param string $mprn
     * @return string - json encoded array
     */
    function redfish_meter_reading($id, $data, $mpan = null, $mprn = null)
    {

        $location = $mprn?'/Gas/'.$mprn:'/Electricity/'.$mpan;
        $data = json_encode($data);

        return $this->redfish_curl_request('POST', '/MeterReading/', $id.$location, $data);

    }

    function verify_redfish($email)
    {
        $data = json_encode([ 'Email' => $email ]);

        return $this->redfish_curl_request('GET', '/ForgottenPassword', $data, null, 'json', null, false);
    }


}