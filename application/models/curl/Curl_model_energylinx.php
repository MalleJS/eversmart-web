<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_energylinx
 */
Class Curl_model_energylinx extends Curl_model {

    /**
     * Curl_model_energylinx constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Defines base Energylinx curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function energylinx_curl_request($method, $url, $param, $data, $type = 'json')
    {
        $credentials = [];
        $names = file(APPPATH.'/config/credentials/Energylinx.txt');
        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        if ($type=='json') {
            $optional_headers = [
                'Content-Type: application/json',
            ];
        }

        return $this->callAPI($method, $url.$credentials[0].$param, $data, $credentials, $optional_headers);
    }

}