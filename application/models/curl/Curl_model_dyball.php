<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_dyball
 */
Class Curl_model_dyball extends Curl_model {

    /**
     * Curl_model_dyball constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Defines base Dyball curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function dyball_curl_request($method, $url, $param, $data, $type = 'json')
    {
        $headers = [];
        $credentials = [];

        $names = file(APPPATH.'/config/credentials/Dyball.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        if ($type=='json') {
            $headers = [
                'Content-Type: application/json',
                'Accept: application/json',
                'Api-Type: Website'
            ];
        }

        return $this->callAPI($method, $credentials[0].$url.$param, $data, $headers, $credentials);
    }
}