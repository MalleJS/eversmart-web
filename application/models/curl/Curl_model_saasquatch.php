<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_saasquatch
 */
Class Curl_model_saasquatch extends Curl_model {

    /**
     * @var string
     */
    private $cred;

    /**
     * Curl_model_saasquatch constructor.
     */
    function __construct()
    {
        parent::__construct();

        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '35.177.2.247' || $_SERVER['HTTP_HOST'] == 'staging.eversmartenergy.co.uk' ) {
            $this->cred = 'UAT';
        } else {
            $this->cred = 'LIVE';
        }
    }

    /**
     * Defines base Saasquatch curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function saasquatch_curl_request($method, $url, $param, $data, $type = 'json')
    {
        $credentials = [];
        $names = file(APPPATH.'/config/credentials/Saasquatch'.$this->cred.'.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        if ($type=='json') {
            $optional_headers = [
                'Content-Type: application/json',
            ];
        }

        return $this->callAPI($method, $url.$credentials[1].$param, $data, $credentials, $optional_headers);
    }

}