<?php

require APPPATH.'/models/curl/Curl_model_saasquatch.php';

/**
 * Class Curl_requests_saasquatch
 */
Class Curl_requests_saasquatch extends Curl_model_saasquatch {

    /**
     * Curl_requests_saasquatch constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Creates new Saasquatch user
     *
     * @param $id
     * @param $fname
     * @param $lname
     * @param $email
     * @param string $return_type
     * @return array|string
     */
    function saasquatch_create_user($id, $fname, $lname, $email, $return_type = 'json')
    {
        $random = rand(0, 99);
        $random = ($random <= 9 ? "0" . $random : $random);

        $data = json_encode([
            'id' => $id,
            'accountId' => $id,
            'firstName' => $fname,
            'lastName' => $lname,
            'email' => $email,
            'referralCode' => $fname . $lname . $random,
            'referable' => true,
            'locale' => 'en_UK']);

        $saasquatch = $this->saasquatch_curl_request('POST', 'https://app.referralsaasquatch.com/api/v1/', '/open/account/'. $id .'/user/'. $id, $data);
        $saasquatch = json_decode($saasquatch, true);

        if(isset($saasquatch['statusCode']) && ($saasquatch['statusCode'] == '400' || $saasquatch['statusCode'] == '404'))
        {
            if ($return_type == 'array') {
                return $saasquatch['message'];
            } else {
                echo json_encode(['error' => true, 'data' => $saasquatch['message']]);
            }
        } else {
            if ($return_type == 'array') {
                return $saasquatch;
            } else {
                echo json_encode(['error' => false, 'data' => $saasquatch]);
            }
        }
    }

    /**
     * Retrieves Saasquatch user
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function saasquatch_get_user($id, $return_type = 'array')
    {
        $get_user = $this->saasquatch_curl_request('GET', 'https://app.referralsaasquatch.com/api/v1/', '/account/'.$id.'/user/'.$id.'/pii', false);
        $get_user = json_decode($get_user, true);

        if (!empty($get_user)) {
            $sharelink = isset($get_user['shareLinks']['shareLink']) ? $get_user['shareLinks']['shareLink'] : '';
            $sharelinkFbook = isset($get_user['shareLinks']['facebookShareLink']) ? $get_user['shareLinks']['facebookShareLink'] : '';
            $sharelinkTwitt = isset($get_user['shareLinks']['twitterShareLink']) ? $get_user['shareLinks']['twitterShareLink'] : '';
            $sharelinkEmail = isset($get_user['shareLinks']['emailShareLink']) ? $get_user['shareLinks']['emailShareLink'] : '';
            $num_of_rewards = isset($get_user['rewards']['totalCount']) ? $get_user['rewards']['totalCount'] : '';
            $num_of_referrals = isset($get_user['referrals']['totalCount']) ? $get_user['referrals']['totalCount'] : '';

            if (isset($get_user['rewardBalances'][0]['totalAssignedCredit']) && isset($get_user['rewardBalances'][0]['totalRedeemedCredit'])) {
                $Assigned = $get_user['rewardBalances'][0]['totalAssignedCredit'];
                $Redeemed = $get_user['rewardBalances'][0]['totalRedeemedCredit'];
                $assigned_bal = $Assigned / 100;
                $redeemed_bal = $Redeemed / 100;
            }
            else {
                $assigned_bal = 0;
                $redeemed_bal = 0;
            }

            $Saas = [
                'sharelink' => $sharelink,
                'sharelinkFBK' => $sharelinkFbook,
                'sharelinkTWT' => $sharelinkTwitt,
                'sharelinkMOB' => $sharelinkEmail,
                'assignedBal' => $assigned_bal,
                'redeemedBal' => $redeemed_bal,
                'currentBal' => $assigned_bal - $redeemed_bal,
                'rewards' => $num_of_rewards,
                'referrals' => $num_of_referrals
            ];
        }
        else {
            $Saas = [
                'sharelink' => '',
                'sharelinkFBK' => '',
                'sharelinkTWT' => '',
                'sharelinkMOB' => '',
                'assignedBal' => '',
                'redeemedBal' => '',
                'currentBal' => '',
                'rewards' => '',
                'referrals' => ''
            ];
        }

        if ($return_type == 'array') {
            return $Saas;
        } else {
            echo json_encode(['error' => false, 'data' => $Saas]);
        }
    }

    /**
     * Retrieves Saasquatch referrals associated with Saasquatch ID
     *
     * @param $id
     * @param string $return_type
     * @return array|string
     */
    function saasquatch_lookup_referrals($id, $return_type = 'array')
    {
        $referrals = $this->saasquatch_curl_request('GET', 'https://app.referralsaasquatch.com/api/v1/', '/open/referrals?referringAccountId='.$id.'&referringUserId='.$id , false);
        $referrals = json_decode($referrals, true);

        $ref = $referrals['referrals'];
        $pending = 0;
        $switched = 0;

        foreach($ref as $referral) {
            $paid = $referral['dateReferralPaid'];

            if (!isset($paid)) {
                $pending++;
            } else {
                $switched++;
            }
        }

        $Saas = [
            'pending' => $pending,
            'switched' => $switched
        ];

        if ($return_type == 'array') {
            return $Saas;
        } else {
            echo json_encode(['error' => false, 'data' => $Saas]);
        }
    }

    /**
     * Debits Saasquatch user with associated Saasquatch ID
     *
     * @param $id
     * @param string $amount
     * @return string
     */
    function saasquatch_debit_user($id, $amount = '5000')
    {
        $data = json_encode(['accountId' => $id, 'unit' => 'cents', 'amount' => $amount]);

        $saas_debit = $this->saasquatch_curl_request('POST', 'https://app.referralsaasquatch.com/api/v1/', '/credit/bulkredeem', $data);
        $saas_debit = json_decode($saas_debit, true);

        if (isset($saas_debit['statusCode']) && $saas_debit['statusCode'] == '400') {
            return json_encode([ 'status' => 'fail']);
        } else {
            return json_encode([ 'status' => 'success']);
        }
    }
    
    /**
     * Gets Saasquatch user associated to referral code
     *
     * @param $code
     * @return int
     */
    function get_saas_id_by_code($code)
    {
        $saas = $this->saasquatch_curl_request('GET', 'https://app.referralsaasquatch.com/api/v1/', '/user?referralCode='.$code, null);
        $saas = json_decode($saas, true);

        if (isset($saas['statusCode']) && ($saas['statusCode'] == '400' || $saas['statusCode'] == '404')) {
            return false;
        } else {
            return $saas['id'];
        }
    }

    /**
     * Mark as converted in Saasquatch 
     * 
     * @param $data
     * @return null
     */
    function mark_saas_converted($data)
    {
        $para = json_encode([
            'id' => $data['id'],
            'accountId' => $data['id'],
            'referredBy' => [
                'code' => $data['saasref'],
                'isConverted'=> true
            ]
        ]);

        return $this->saasquatch_curl_request('PUT', 'https://app.referralsaasquatch.com/api/v1/', '/open/account/' . $data['id'] . '/user/' . $data['id'], $para);
    }

    /**
     * Mark as converted in Saasquatch
     *
     * @param $data
     * @return null
     */
    function saas_customer_lookup($id)
    {
        $response = $this->saasquatch_curl_request('GET', 'https://app.referralsaasquatch.com/api/v1/', '/account/' . $id . '/user/' . $id, NULL);
        return json_decode($response, true);
    }


    /**
     * Debits Saasquatch user with associated Saasquatch ID
     *
     * @param $id
     * @param string $amount
     * @return string
     */
    function saasquatch_debit_referral_user($id, $amount)
    {
        return true;

        // $data = json_encode(['accountId' => $id, 'unit' => 'cents', 'amount' => $amount]);
        // $saas_debit = $this->saasquatch_curl_request('POST', 'https://app.referralsaasquatch.com/api/v1/', '/credit/bulkredeem', $data);
        // $saas_debit = json_decode($saas_debit, true);

        // if (isset($saas_debit['statusCode']) && $saas_debit['statusCode'] == '400') {
        //     return false;
        // } else {
        //     return true;
        // }
    }

}
