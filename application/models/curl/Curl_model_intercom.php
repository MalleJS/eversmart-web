<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_intercom
 */
Class Curl_model_intercom extends Curl_model {

    /**
     * Curl_model_intercom constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Defines base Intercom curl request
     *
     * @param $method
     * @param $url
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function intercom_curl_request($method, $url, $data, $type = 'json')
    {
        $credentials = [];
        $names = file(APPPATH.'/config/credentials/Intercom.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        if ($type=='json') {
            $optional_headers = [
                'Content-Type: application/json',
            ];
        }

        return $this->callAPI($method, $url, $data, $credentials, $optional_headers);
    }

}