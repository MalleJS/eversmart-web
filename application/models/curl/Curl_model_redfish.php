<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_redfish
 */
Class Curl_model_redfish extends Curl_model {

    /**
     * @var string
     */
    private $cred;

    /**
     * Curl_model_redfish constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->library('jwt');

        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '35.177.2.247' || $_SERVER['HTTP_HOST'] == 'jwtstaging.eversmartenergy.co.uk' ) {
            $this->cred = 'UAT';
        } else {
            $this->cred = 'LIVE';
        }
    }

    // Takes a token and decodes it
    private function decode_token($token){

        // Have we received a token
        if(isset($token)) {

            // Decode token
            return Jwt::decode($token, $this->salt, array('HS256'));
        }
        else {
            return false;
        }
    }

    /**
     * Defines base redfish curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @param string $encoded_login
     * @param boolean $return_headers
     * @return bool|string
     */
    function redfish_curl_request($method, $url, $param, $data, $type = 'json', $encoded_login = NULL, $return_headers = true)
    {
        $credentials = [];
        $optional_headers = [];

        $names = file(APPPATH.'/config/credentials/Redfish'.$this->cred.'.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }


        // Login request requires Authorisation
        if ($type=='login') {
            $optional_headers = [
                'Api-Type: Website',
                'Content-Type: application/json',
                'Accept: application/json',
                'Client-IP: 123.123.123.123',
                'Client-UA: Chrome',
                'Authorization: Basic '. $encoded_login
            ];
        }


        // All other requests require token
        if ($type=='json') {

            $optional_headers = [
                'Api-Type: Website',
                'Content-Type: application/json',
                'Accept: application/json',
                'Client-IP: 123.123.123.123',
                'Client-UA: Chrome'
            ];

            // Get Redfish token from JWT token
            if(isset($_SERVER['HTTP_X_CUSTOMER_TOKEN'])) {
                $decoded_token = $this->decode_token($_SERVER['HTTP_X_CUSTOMER_TOKEN']);
                $optional_headers[] = 'Cookie: Account=' . $decoded_token->redfish_token;
            }
        }


        return $this->callAPI($method, $credentials[0].$url.$param, $data, $credentials, $optional_headers, $return_headers);
    }

}