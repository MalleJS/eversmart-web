<?php

/**
 * Class Curl_model
 */
Class Curl_model extends CI_Model{

    /**
     * Curl_model constructor.
     */
    function __construct() {
        parent::__construct();
        $this->mysql = $this->load->database('mysql', true);
    }

    /**
     * Defines base curl request
     *
     * @param $method
     * @param $url
     * @param $data
     * @param $headers
     * @param $optional_headers
     * @param $return_headers boolean - default to false as only Redfish need headers returned. Overwritten the
     * Redfish requests with true - exception, redfish_reset_password call no headears .
     * @return bool|string
     */
    function callAPI($method, $url, $data, $headers = [], $optional_headers = [], $return_headers = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                else
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(array()));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        $Headers = [];

        if($headers !== []){
            $Headers[] = $headers[0];
            $Headers[] = $headers[1];
        }

        if (isset($optional_headers) && !empty($optional_headers)) {
            foreach ($optional_headers as $key => $value) {
                $Headers[] = $value;
            }
        }


        // OPTIONS:
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if($return_headers) {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($Headers !== []) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $Headers);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        
        if (!curl_errno($curl)) {
            switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                case 200:  // success - do nothing
                    break;
                case 201:  // success - do nothing
                    break;
                case 202:  // success - do nothing
                    break;
                default:   // error - insert into db

                    $err = [
                        'error_request' => $url,
                        'error_response' => $result
                    ];

                    if ($data) {
                        $err['error_data'] = $data;
                    }

                    $this->curl_error($err);
            }
        }

        curl_close($curl);

        return $result;
    }

    /**
     * Insert error into database
     *
     * @param $data
     * @return mixed
     */
    function curl_error($data)
    {
        return $this->mysql->insert('error_log', $data);
    }

}
