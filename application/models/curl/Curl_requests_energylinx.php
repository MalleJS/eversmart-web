<?php

require APPPATH.'/models/curl/Curl_model_energylinx.php';

/**
 * Class Curl_requests_energylinx
 */
Class Curl_requests_energylinx extends Curl_model_energylinx {

    /**
     * Curl_requests_energylinx constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieves District ID
     *
     * @param $param
     * @param string $return_type
     * @return array|string
     */
    function energylinx_get_distid($postcode, $return_type = 'json')
    {
        $data = json_encode(['postcode' => $postcode]);

        $get_distid = $this->energylinx_curl_request('POST', 'https://apis2.x.energylinx.com/v2/partner-resources/', '/dist-ids/', $data);
        $get_distid = json_decode($get_distid, true);

        $distid = $get_distid['data']['entriesFromMpanFields'][0]['distId'];

        if($return_type == 'array'){
            return $distid;
        }
        else {
            return json_encode(['error' => false, 'data' => $distid]);
        }
    }


    /**
     * Retrieves meter serial by postcode
     *
     * @param $postcode
     * @param string $mpan
     * @param string $mprn
     * @return array|string
     */
    function energylinx_get_meter_serial($postcode, $mpan = null, $mprn = null)
    {
        $data = json_encode(['pcod' => $postcode]);

        $all_addresses = $this->energylinx_curl_request('POST', 'https://apis2.awsprod.energylinx.com/v3.1/partner-resources/', '/mpas/addresses-by-pcod/', $data);
        $all_addresses = json_decode($all_addresses, true);

        foreach ($all_addresses['data']['addresses'] as $key => $address) {

            if($mpan) {
                foreach ($address['metersElec'] as $key => $meter) {
                    if (strstr($meter['mpan'], $mpan)) {
                        return ['meter_serial_elec' => $meter['meterSerialElec'], 'meter_type' => $meter['mpanMeterTypeVerbose']];
                    };
                }
            }
            else if($mprn) {
                foreach ($address['metersGas'] as $key => $meter) {
                    if (strstr($meter['mprn'], $mprn)) {
                        return $meter['meterSerialGas'];
                    };
                }
            }
        }
    }

    /**
     * Retrieves dual/elec only energy quote form Energylinx
     *
     * @param $param
     * @param $energytype
     * @param $paytype
     * @param $eac
     * @param $aq
     * @param string $return_type
     * @return array|string
     */
    function energylinx_get_quote($postcode, $energytype, $paytype, $eac, $aq, $return_type = 'array')
    {
        $ElecDistId = $this->energylinx_get_distid($postcode);

        if($energytype === 'dual') {

            $data = json_encode([
                'singleOrSeparate' => 'separate',
                'hasExistingSupply' => true,
                'desiredPayTypes' => [$paytype],
                'existingkWhsElec' => $eac,
                'existingkWhsGas' => $aq,
                'existingPayTypesElec' => [$paytype],
                'existingPayTypesGas' => [$paytype],
                'existingSupplierNameGas' => 'British Gas',
                'existingSupplierNameElec' => 'British Gas',
                'existingTariffNameGas' => 'Standard',
                'existingTariffNameElec' => 'Standard',
                'existingkWhsIntervalGas' => 'year',
                'existingkWhsIntervalElec' => 'year',
                'postcode' => $postcode,
                'nightUsePercentElec' => '10',
                'meterTypeElec' => 'STD',
                'distId' => $ElecDistId
            ]);

            $quote_results = $this->energylinx_curl_request('POST', 'https://apis2.awsprod.energylinx.com/v2/partner-resources/', '/quotes/elecgas/', $data);
        }
        else if($energytype === 'elec') {
            $data = json_encode([
                'postcode' => $postcode,
                'hasExistingSupply' => true,
                'existingPayTypesElec' => [$paytype],
                'desiredPayTypes' => [$paytype],
                'existingkWhsElec' => $eac,
                'existingSupplierNameElec' => 'British Gas',
                'existingTariffNameElec' => 'Standard',
                'existingkWhsIntervalElec' => 'year',
                'nightUsePercentElec' => '10',
                'meterTypeElec' => 'STD'
            ]);

            $quote_results = $this->energylinx_curl_request('POST', 'https://apis2.awsprod.energylinx.com/v2/partner-resources/', '/quotes/elec/', $data);
        }

        $quote_results = json_decode($quote_results, true);
        $quote = $quote_results['data']['searchResults'][0];

        if (!empty($quote)) {
            $Quote = [
                'tariffName' => $quote['tariffName'],
                'newSpend' => $quote['newSpend'],
                'newSpendElec' => $quote['newSpendElec'],
                'newSpendGas' => $quote['newSpendGas'],
                'unitRate1Elec' => round(($quote['unitRate1Elec'] / 100), 4),
                'standingChargeElec' => round(($quote['standingChargeElec'] / 100), 4),
                'unitRate1Gas' => round(($quote['unitRate1Gas'] / 100), 4),
                'standingChargeGas' => round(($quote['standingChargeGas'] / 100), 4)
            ];
        } else {
            $Quote = [
                'tariffName' => '',
                'newSpend' => '',
                'newSpendElec' => '',
                'newSpendGas' => '',
                'unitRate1Elec' => '',
                'standingChargeElec' => '',
                'unitRate1Gas' => '',
                'standingChargeGas' => ''
            ];
        }

        if($return_type == 'array'){
            return $Quote;
        }
        else {
            echo json_encode(['error' => false, 'data' => $Quote]);
        }
    }
}
