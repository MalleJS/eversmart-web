<?php

Class Api_modal extends CI_Model{

function __construct(){
    parent::__construct();
    $this->mysql = $this->load->database('mysql',true);
  }

function upload_meter_image($data){
  $this->mysql->insert('meter_image',$data);
  return $this->mysql->insert_id();
}

function get_customer_meter_image($customer_id){
  return $this->mysql->select('*')->from('meter_image')->where('customer_id', $customer_id)->order_by('create_at', 'desc')->get()->result_array();
}

function check_user_active($email,$password){

    $Salt = file_get_contents('ese_key.txt');
    $sql = "SELECT * FROM customer_info WHERE (( signup_type = '0' AND email = '".$email."' AND password = AES_ENCRYPT(SHA1('".$password."'), '".$Salt."')) OR (signup_type = '2')) ";
    $query = $this->mysql->query($sql);
    $result = $query->result_array();

    if(empty($result)){
        return $result;
    }
    else {
        return $result[0];
    }

}

function account_detail($id)
{
  return $this->mysql->select('*')->from('customer_info')->where(['account_id' => $id])->get()->result_array();
}

function save_transaction($row){
	return $this->mysql->insert('transaction',$row);
}

function get_account_detail($id){
  return $this->mysql->select('*')->from('customer_info')->where(['id' => $id])->get()->first_row('array');
}

function update_booked_meter($data,$id){

  return $this->mysql->where('id',$id)->update('customer_info', $data);
//  echo $this->mysql->last_query();
}

function updateMeterPointID($data,$user_id){
    return $this->mysql->where('id',$user_id)->update('customer_info',$data);
    //echo $this
}

}

 ?>
