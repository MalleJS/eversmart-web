<?php

Class Experian extends CI_Model{

    private $exp_token = '  ';

    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get Experian Address List
     *
     * @arguments $query (string)
     */
    function get_address_list($query)
    {
        try {
            $ch = curl_init('https://api.experianmarketingservices.com/capture/v1/search-address/text?query=' . $query . '&country=GBR&skip=0&take=1000&auth-token=' . $this->exp_token);
            $data['address_list'] = json_encode(curl_exec($ch));
        } catch(Exception $exception) {
            log_message('error', $exception);
        }
    }
}
