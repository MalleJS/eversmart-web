<?php

class Asperato_model extends CI_Model
{
    private $auth_url,
            $auth_id,
            $payment_ref,
            $iframe_height,
            $iframe_width;

    function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->payment_ref = '1363';
        $this->iframe_height = '800';
        $this->iframe_width = '500';
        $this->auth_id = 'a4C1l00000004fm';
        $this->auth_url = 'https://test.protectedpayments.net/PMWeb1?pmRef=' . $this->payment_ref . '&DLcnp=jdi&aid=' . $this->auth_id;
    }

    /*
    * Generate an Asperato iFrame
    *
    * @returns $call (string)
    */
    function authorization()
    {
        echo "<iframe src=".$this->auth_url." width=".$this->iframe_width."px height=".$this->iframe_height."px/>";
    }
}