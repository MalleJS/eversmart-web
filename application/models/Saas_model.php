<?php

Class Saas_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->mysql = $this->load->database('mysql', true);
    }


    /**
     * Tie together new customer and the referrer
     *
     * @params $data array
     *
     * @returns int
     */
    function referral_log($data)
    {
        $this->mysql->insert('referral_log', $data);
        return $this->mysql->insert_id();
    }

    /**
     * Tie together saas link and ese link
     *
     * @params $data array
     *
     * @returns int
     */
    function insert_ese_share_link($data)
    {
        $this->mysql->insert('referral_link', $data);
        return $this->mysql->insert_id();
    }


    /**
     * Get the log data relating to customer id
     *
     * @param $id
     * @return mixed
     */
    function get_referral_log_id($id)
    {
        $sql = "SELECT * FROM referral_log WHERE 
        ( referrer_is_converted = 1 AND referrer_customer_id = {$id} AND referrer_is_redeemed IS NULL ) OR ( referred_is_converted = 1 AND referred_customer_id = {$id} AND referred_is_redeemed IS NULL )";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    /**
     * Mark as converted
     *
     * @params
     * $data int
     * $type text
     * @returns int
     */
    function saas_converted($data)
    {
        switch ($data['type']) {
            case 'referrer':
                $db_param = ['referrer_is_converted' => '1', 'referrer_converted_date' => date('Y-m-d H:i:s', time())];
                break;
            case 'referred':
                $db_param = ['referred_is_converted' => '1', 'referred_converted_date' => date('Y-m-d H:i:s', time())];
                break;
        }
        return $this->mysql->where('referral_log_id', $data['referral_log_id'])->update('referral_log', $db_param);
    }

    /**
     * Customer mark as redeemed
     *
     * @params
     * $data array
     *
     * @returns int
     */
    function saas_customer_redeemed($data)
    {
        switch ($data['type']) {
            case 'referrer':
                $db_param = ['referrer_is_redeemed' => '1'];
                break;
            case 'referred':
                $db_param = ['referred_is_redeemed' => '1'];
                break;
        }
        return $this->mysql->where('referral_log_id', $data['referral_log_id'])->update('referral_log', $db_param);
    }

    /**
     * Customer mark ad redeemed
     *
     * @params
     * $data array
     *
     * @returns int
     */
    function saas_admin_redeemed($data)
    {
        switch ($data['type']) {
            case 'referrer':
                $db_param = ['referrer_redeemed_date' => date('Y-m-d H:i:s', time())];
                break;
            case 'referred':
                $db_param = ['referred_redeemed_date' => date('Y-m-d H:i:s', time())];
                break;
        }
        return $this->mysql->where('referral_log_id', $data['referral_log_id'])->update('referral_log', $db_param);
    }

    /**
     * find all none converted referrer in referral log table
     *
     * @return mixed
     */
    function none_converted_referrer()
    {
        return $this->mysql->select('*')->from('referral_log')->where(['referrer_is_converted' => NULL])->get()->result_array();
    }

    /**
     * find all none converted referrer in referral log table
     *
     * @return mixed
     */
    function none_converted_referred()
    {
        return $this->mysql->select('*')->from('referral_log')->where(['referred_is_converted' => NULL])->get()->result_array();
    }

    function insert_payment_transaction($record = [])
    {
        $sql = "INSERT INTO payment_log (account_id, amount,payment_type, insert_date) ";
        $sql .= "VALUES ({$record['account_id']}, '{$record['amount']}', '{$record['payment_type']}' ,'{$record['insert_date']}' )";
        $query = $this->mysql->query($sql);
        return $this->mysql->affected_rows();
    }

    /**
     * Class method to return list of customers
     * due to be paid for referring a friend
     *
     * @return void
     */
    function get_referral_rewards()
    {
        $array_referrer = [];
        $array_referrer_id = $this->get_redeemed_referrer();

        foreach ($array_referrer_id as $referrer_id) {
            $sql = "SELECT id, customer_id, account_id, forename, surname, email, phone1 FROM customer_info 
                    WHERE customer_id='{$referrer_id['referrer_customer_id']}'";
            $query = $this->mysql->query($sql);
            if (count($query->row()) > 0) {
                $array_referrer[] = $query->row();
            }
        }
        return $array_referrer;
    }

    /**
     * Class method to return list of customers
     * due to be paid after they are referred
     *
     * @return void
     */
    function get_referred_rewards()
    {
        $array_referred = [];
        $array_referred_id = $this->get_redeemed_referred();

        foreach ($array_referred_id as $referred_id) {

            $sql = "SELECT id,customer_id, account_id, forename, surname, email, phone1 FROM customer_info 
                    WHERE customer_id={$referred_id['referred_customer_id']}";
            $query = $this->mysql->query($sql);
            if (count($query->row()) > 0) {
                $array_referred[] = $query->row();
            }
//            $array_referred[] = $query->row();

        }

        return $array_referred;
    }

    /**
     * Class method to return a list of referrer
     * identifiers that have actioned that they
     * are ready to redeem their reward
     *
     * @return void
     */
    function get_redeemed_referrer()
    {
        $sql = "SELECT referrer_customer_id FROM referral_log WHERE referrer_is_redeemed = 1
        AND referrer_is_converted = 1 AND referrer_converted_date IS NOT NULL AND referrer_redeemed_date IS NULL ";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    /**
     * Class method to return a list of referred customers
     * that have actioned that they
     * are ready to redeem their reward
     *
     * @return void
     */
    function get_redeemed_referred()
    {
        $sql = "SELECT referred_customer_id FROM referral_log WHERE referred_is_redeemed = 1
        AND referred_is_converted = 1 AND referred_redeemed_date IS NULL AND referred_converted_date IS NOT NULL";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    /**
     * Get ESE SaaS link from DB
     *
     * @param $id
     * @return mixed
     */
    function get_ese_share_link($id)
    {
        return $this->mysql->select('rl.ese_generated_link')
            ->from('referral_link rl')
            ->join('customer_info ci', 'rl.customer_id = ci.id')
            ->where('ci.account_id', $id)
            ->get()->first_row('array');
    }

    /** Admin panel redeem update
     *
     * @param string $type
     * @param string $todays_date
     * @param int $customer_id
     * @return void
     */
    function admin_reward_confirmation($type, $todays_date, $customer_id)
    {
        if ($type == 'referrer') {
            $this->mysql->set('referrer_redeemed_date', $todays_date);
            $this->mysql->where('referrer_customer_id', $customer_id);
            $this->mysql->update('referral_log');
        } else {
            $this->mysql->set('referred_redeemed_date', $todays_date);
            $this->mysql->where('referred_customer_id', $customer_id);
            $this->mysql->update('referral_log');
        }
    }

} //class endtoken_in_database
