<?php
 
class Csv_model extends CI_Model {
 
    function __construct() {
        parent::__construct();
 
    }
 
    function get_addressbook() {     
        $query = $this->db->get('addressbook');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_csv($data) {

       $this->db->insert('addressbook', $data);
      // $this->db->where_not_in('ACCOUNT_NUMBER', $acnumber)->insert('addressbook', $data);
    }

    public function checkEmail($email)
        {
           $this -> db -> select('*');
           $this -> db -> from('addressbook');
           $this -> db -> where('EMAIL', $email);
           $query = $this -> db -> get();
           return $query->result_array();
        }

    public function insertData($data)
        {
            $this->db->insert('users', $data);
        }
}
/*END OF FILE*/