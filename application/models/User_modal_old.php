<?php

Class User_modal extends CI_Model{

function __construct(){
    parent::__construct();
    $this->mysql = $this->load->database('mysql',true);
  }

function insert_customer_info($data){
	//$this->mysql->truncate('customer_info');
	$this->mysql->insert('customer_info',$data);
	return $this->mysql->insert_id();
}

//update account id
function update_account_id($userid, $account_id){
  $data['account_id'] = $account_id;
  $this->mysql->where('id',$userid)->update('customer_info', $data);
}

//update user status
function change_status($token){
    //get user id
    $get_token = $this->mysql->select('customer_id')->from('token')->where('token',$token)->get()->first_row('array');
    if(!empty($get_token))
    {
      $id = $get_token['customer_id'];
      $data['active'] = '1';
      $update_status = $this->mysql->where('id',$id)->update('customer_info', $data);
    }
    // echo $this->mysql->last_query();
    // debug($get_token);
}

function update_customer_info($data,$customer_id) 
{
	return $this->mysql->where('id', $customer_id)->update('customer_info',$data);
}

function update_customer_info_by_account_id($data,$account_id)
{
	return $this->mysql->where('account_id', $account_id)->update('customer_info',$data);
}

function delete_customer_info($id)
{
    $sql = "DELETE FROM customer_info WHERE id = '".$id."'";
    return $this->mysql->query($sql);
}

function login_check($data)
{
    $Salt = file_get_contents('ese_key.txt');

    $sql = "SELECT * FROM customer_info WHERE email = '".$data['user_email']."' AND password = AES_ENCRYPT(SHA1('".$data['user_password']."'), '".$Salt."')";
    $query = $this->mysql->query($sql);
    $result = $query->result_array();

    if(empty($result)){
        return $result;
    }
    else {
        return $result[0];
    }
}

/**
 * Add a record of customers successful login to db
 *
 * @params
 * $id varchar
 * $activity_type_id int
 *
 * @return mixed
 */
function activity_log($id, $activity_type_id)
{
    $data = [
        'customer_info_id'=> !strstr($id, 'EVE')?$id:NULL,
        'redfish_account_id'=> strstr($id, 'EVE')?$id:NULL,
        'activity_type_id' => $activity_type_id
    ];
    $this->mysql->insert('activity_log', $data);
    return $this->mysql->insert_id();
}

function get_account_number()
{
	return $this->mysql->select('*')->from('customer_info')->order_by('create_at','desc')->get()->first_row('array');
}

function get_customer_billing_addresses($customer_id)
{
	return $this->mysql->select('ba.*')->from('customer_billing_address cba')
        ->join('billing_address ba', 'cba.billing_address_id = ba.billing_address_id', 'INNER')
        ->where('cba.customer_id' , $customer_id)
        ->order_by('created_at','desc')
    ->get()->result_array();
}

function insert_token($token, $id, $type = '0')
{
	$data = [
		'token' => $token,
		'customer_id' => $id,
        'token_type' => $type,
		'expire_date' => date('Y-m-d H:i:s', strtotime('+1 day', strtotime( date('Y-m-d H:i:s') ))),
		'create_at' => date('Y-m-d H:i:s')
	];
	return $this->mysql->insert('token',$data);
}

function token_in_database($token, $id, $type = '0')
{
	$data = [
		'token' => $token,
		'customer_id' => $id,
        'token_type' => $type,
		'expire_date' => date('Y-m-d H:i:s', strtotime('+30 day', strtotime( date('Y-m-d H:i:s') ))),
		'create_at' => date('Y-m-d H:i:s')
	];
	return $this->mysql->insert('token',$data);
}

function email_check($email){
      return $this->mysql->select('*')->from('customer_info')->where('email',$email)->get()->first_row('array');
}

function verify_junifer($email){
    return $this->mysql->select('email')->from('customer_info')->where('email',$email)->get()->first_row('array');
}

function check_token($token){
  return $this->mysql->select('*')->from('token')->where('token',$token)->get()->first_row('array');
}

//change customer password
function customer_change_password($value){

    $Salt = file_get_contents('ese_key.txt');

    // Now update the record
    $sql = "UPDATE customer_info SET password=AES_ENCRYPT(SHA1(?), '".$Salt."') WHERE id=?";
    return $this->mysql->query($sql, array($value['confirm_password'], $value['user_id']));

}

function customer_update($data)
{
	$row['active'] = '1';
	return $this->mysql->where('id',$data)->update('customer_info',$row);

}

function electric_first_update($data)
{
	$row['first_reading_elec'] = 'yes';
	return $this->mysql->where('id',$data)->update('customer_info',$row);

}
function gas_first_update($data)
{
	$row['first_reading_gas'] = 'yes';
	return $this->mysql->where('id',$data)->update('customer_info',$row);

}

function log_meter_reading($data)
{
    return $this->mysql->insert('meter_reading_log',$data);
}


function db_mpan_check($mpan){
  return $this->mysql->select('*')->from('customer_info')->where('elec_mpan_core',$mpan)->get()->first_row('array');
  echo $this->mysql->last_query();
  // if($elec_mpan_core){
  //   return true;
  // }
  // else{
  //   return false;
  // }
} 

function db_mprn_check($mprn){
  return $this->mysql->select('*')->from('customer_info')->where('gasProduct_mprns',$mprn)->get()->first_row('array');
}

function get_account_detail($id){
  return $this->mysql->select('*')->from('customer_info')->where('id',$id)->get()->first_row('array');
}

function book_meter($id,$meter){
  return $this->mysql->where( 'id', $id )->update( 'customer_info',[ 'booked_meter' => $meter] );
}

function booked_meter_info()
{
  return $this->mysql->select('*')->from('customer_info')->where('booked_meter', '2')->get()->result_array();
}

function booking_meter(){

	return $this->mysql->select('*')->from('customer_info')->where('booked_meter', '2')->get()->result_array();
}

function confirmed_meter(){

	return $this->mysql->select('*')->from('customer_info')->where('booked_meter', '2')->get()->result_array();
}


function check_token_confirm($token){
    return $this->mysql->select('*')->from('token')->where('token',$token)->get()->first_row('array');
}

function change_user_status($id)
{
    $data['api_user_status'] = '1';
    return $this->mysql->where('id',$id)->update('customer_info',$data);
    //echo $this->mysql->last_query();
}

function log_customer_payment($data){
    return $this->mysql->insert('payment_log',$data);
}

function get_customer_payment($account_id){
    return $this->mysql->select('amount, payment_type, insert_date, UNIX_TIMESTAMP(insert_date) AS timestamp')->from('payment_log')->where('account_id',$account_id)->get()->result_array();
}

function insert_customer_monthly_interest($data){
    return $this->mysql->insert('customer_monthly_interest',$data);
}

function get_monthly_interest_date($id){
    return $this->mysql->select('monthly_interest_customer_signup_date, DATE_ADD(monthly_interest_customer_signup_date, INTERVAL 21 DAY) AS go_live_date')->from('customer_info')->where('id', $id)->get()->first_row('array');
}

function latest_product_ref($type)
{
    $result = $this->mysql->select('value')->from('standing_data')->where('description', $type)->get()->first_row('array');

    $data = [ "value" => $result['value'] + 1 ];
    $this->mysql->where('description',$type)->update('standing_data', $data);

    if($type == 'gas_reference_count') {
        return 'gas_' . ($result['value'] + 1);
    }
    else {
        return 'elec_' . ($result['value'] + 1);
    }

}

function dd_change_customers()
{
    // pick up the customers wo have not recieved the email
    return $this->mysql->select('*')->from('direct_debit_change')->where('date_sent IS NULL')->get()->result_array();
}

function update_direct_debit_change($data,$direct_debit_change_id)
{
    return $this->mysql->where('direct_debit_change_id', $direct_debit_change_id)->update('direct_debit_change',$data);
}

    function get_email_template_details($id){
        return $this->mysql->select('phone1, address_postcode')->from('customer_info')->where('id', $id)->get()->first_row('array');
    }


function get_customer_tariff_info($junifer_acount_id = null, $dyball_account_id = null)
{

    $Query = 'SELECT exitFeeElec, tariff_newspend, billingMethod, tariff_tariffName, tariff_unitRate1Elec, tariff_standingCharge1Elec, tariff_unitRate1Gas, tariff_standingCharge1Gas ';
    $Query .= 'FROM `customer_info` ';
    if($junifer_acount_id != '') {
        $Query .= 'WHERE account_id = ' . $junifer_acount_id;
    }
    if($dyball_account_id != '') {
        $Query .= 'WHERE account_id = ' . $dyball_account_id;
    }
    $TariffInfo = $this->mysql->query($Query)->first_row('array');
    return $TariffInfo;
}

function get_basic_customer_info($junifer_account_id = null, $dyball_account_id = null)
{

    $Query = 'SELECT id, customer_id, account_id, dyball_account_id, admin_role_type_id, intercom_id, title, forename, surname, billingMethod, email, phone1,
         supplyAddress_address1 AS first_line_address, supplyAddress_postcode AS address_postcode, supplyAddress_town_address AS town_address, supplyAddress_city_address AS city_address ';
    $Query .= 'FROM `customer_info` ';
    if($junifer_account_id != '') {
        $Query .= 'WHERE account_id = ' . $junifer_account_id;
    }
    if($dyball_account_id != '') {
        $Query .= 'WHERE account_id = ' . $dyball_account_id;
    }
    $BasicData = $this->mysql->query($Query)->first_row('array');
    return $BasicData;
}

function find_pending_prepay_customers()
{

    // find any prepay customer that we do not have an id for
    $CustomerData = $this->mysql->select('CONCAT(\'DYB_\',id) AS id')->from('customer_info')->where('signup_type = \'3\' AND customer_id = \'0\' AND account_id = \'0\' ')->get()->result_array();
    return $CustomerData;
}



    function calculate_total_customer_interest($account_id){
        $AccruedInterest = $this->mysql->select('SUM(cmi.monthly_interest) AS `accrued_interest`, ci.monthly_interest_customer_signup_date AS signup_date ')
            ->from('customer_monthly_interest cmi')
            ->join('customer_info ci',  'ci.id = cmi.customer_id' )
            ->where('ci.account_id',$account_id)
            ->get()->first_row('array');
        return $AccruedInterest;
    }

    function get_customer_interest_data($account_id){
        $AccruedInterest = $this->mysql->select('cmi.*')
            ->from('customer_monthly_interest cmi')
            ->join('customer_info ci',  'ci.id = cmi.customer_id' )
            ->where('ci.account_id',$account_id)
            ->order_by('cmi.customer_monthly_interest_id', 'desc')
            ->get()->result_array();
        return $AccruedInterest;
    }

    function get_tariff_details($id){
        return $this->mysql->select('tariff_tariffName, tariff_newspend, signup_type, electricityProduct_productCode, gasProduct_productCode')->from('customer_info')->where('account_id', $id)->get()->first_row('array');
    }

    function get_payment_details($id){
        $monthly_interest = $this->get_monthly_interest_date($id);
        $CalculateAccruedInterest = $this->calculate_total_customer_interest($id);
        $GetAccruedInterest = $this->get_customer_interest_data($id);
        $GetTariffDetails = $this->get_tariff_details($id);
        $data['monthly_interest'] = $monthly_interest['monthly_interest_customer_signup_date'];
        $data['go_live_date'] = $monthly_interest['go_live_date'];
        $data['accrued_interest'] = $CalculateAccruedInterest['accrued_interest'];
        $data['signup_date'] = $CalculateAccruedInterest['signup_date'];
        $data['accrued_interest_data'] = $GetAccruedInterest;
        $data = array_merge($data, $GetTariffDetails);
        return $data;
    }

    /**
     * Check for customer token in Database
     *
     * @param string $customer_id
     * @return integer
     */
    function activate_customer_via_token ($customer_id) {

        $query = $this->mysql->select('*')
            ->from('token')
            ->where('customer_id', $customer_id)->get()->row();

        if ($query) {
            $this->mysql->insert('active', '1');
            return true;
        } else {
            echo 'Token not found for customer';
            return false;
        }

    }

    function get_meterread_details($id)
    {
        return $this->mysql->select('elec_mpan_core, gasProduct_mprns, elec_meter_serial, gas_meter_serial, create_at, quotation_aq, meterTypeElec, signup_type, elecMeterPointID, gasMeterPointID, address_postcode')->from('customer_info')->where('account_id', $id)->get()->first_row('array');
    }

    function get_referral_details($id){
        return $this->mysql->select('id')->from('customer_info')->where('account_id', $id)->get()->first_row('array');
    }

    function get_ese_share_link($id)
    {
        return $this->mysql->select('rl.ese_generated_link')
            ->from('referral_link rl')
            ->join('customer_info ci',  'rl.customer_id = ci.id' )
            ->where('ci.account_id',$id)
            ->get()->first_row('array');
    }

    function insert_email_log($data)
    {
        $this->mysql->insert('email_log',$data);
        return $this->mysql->insert_id();
    }

    /**
     * Class method to return list of customers
     * due to be paid for referring a friend
     *
     * @return void
     */
    function get_referrals_rewards  (){

        $array_referrer = [];
        $array_referrer_id = $this->get_redeemed_referrers();

        foreach($array_referrer_id as $referrer_id){

            $sql = "SELECT customer_id, forename, surname, email, phone1 FROM customer_info 
                    WHERE customer_id={$referrer_id['referrer_customer_id']}";
            $query  = $this->mysql->query($sql);
            $array_referrer[] = $query->result_array();

        }
        return $array_referrer;
    }

    /**
     * Class method to return a list of referrer
     * identifiers that have actioned that they
     * are ready to redeem their reward
     *
     * @return void
     */
    function get_redeemed_referrers () {

        $sql = "SELECT referrer_customer_id FROM referral_log WHERE referrer_is_redeemed = 1
        AND is_converted = 1 AND referrer_redeemed_date IS NULL";
        $query  = $this->mysql->query($sql);
        return $query->result_array();

    }

} //class endtoken_in_database
