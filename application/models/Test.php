<?php

Class Test extends CI_Model {

    private $test_name,
            $expected_result;

    function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }

    /*
     * Test Data Type of Unit
     *
     * @arguments $test | $data_type
     */
    function test_data_type($test, $data_type, $identifier = '', $notes = '')
    {
        if ($identifier) {
            $this->test_name = $identifier;
        }

        $this->unit->set_test_items(array('test_name', 'res_datatype', 'result'));
        switch($data_type) {
            case 'array':
                $this->expected_result = [];
                break;
            case 'string':
                $this->expected_result = '';
                break;
            case 'integer':
                $this->expected_result === 0;
                break;
            case 'boolean':
                $this->expected_result = true || false;
                break;
            default:
                $this->expected_result;
                break;
        }

        return $this->unit->run($test, $this->expected_result, $this->test_name, 'is_'.$data_type);
    }
}
