<?php
    Class Tarrif_change extends CI_Model{

        function __construct(){
            parent::__construct();
            $this->mysql = $this->load->database('mysql',true);
          }
    
        function get_customer_data($url){
           return $this->mysql->select('*, 
           CASE WHEN redfish_eac > 0 THEN redfish_eac ELSE dyball_eac END AS estimated_annual_consumption, 
           CASE WHEN redfish_aq > 0 THEN redfish_aq ELSE dyball_aq END AS annual_quantity', FALSE)->from('tariff_change_notification')->where('account_number',$url)->get()->first_row('array');
           
        }

        function yearly_tariff_lookup($url){

            // Get ratetype and duosid
            $CustomerInfo = $this->mysql->select('account_number, rate_type, duostouse')->from('tariff_change_notification')->where('account_number',$url)->get()->result_array();
        
            // Look up
            return $this->mysql->select('*')->from('new_yearly_tariff')->where('ratetype',$CustomerInfo[0]['rate_type'])->where('duosid',$CustomerInfo[0]['duostouse'])->get()->result_array();
        
        }
    }
?> 