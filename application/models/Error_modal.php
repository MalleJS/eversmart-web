<?php

Class Error_modal extends CI_Model{

  function __construct(){
    parent::__construct();
    $this->mysql = $this->load->database('mysql',true);
  }

  function log($error_type, $message){
    $data['error_type'] = $error_type;
    $data['message'] = $message;
    return $this->mysql->insert('log',$data);
  }

}
