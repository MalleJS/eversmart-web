<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/11/2018
 * Time: 16:12
 */
Class User_status_model extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->mysql = $this->load->database('mysql',true);
    }

    function is_active($email,$password){

        $Salt = file_get_contents('ese_key.txt');
        $sql = "SELECT * FROM customer_info WHERE email = '".$email."' AND password = AES_ENCRYPT(SHA1('".$password."'), '".$Salt."') ";
        $query = $this->mysql->query($sql);
        $result = $query->result_array();

        if(empty($result)){
            return $result;
        }
        else {

            if($result[0]['active'] == 0) { $this->auto_activate($result[0]['id']); }

            return $result[0];
        }

    }


    function auto_activate($id){

        return $this->mysql->where('id',$id)->update('customer_info', ['active' => '1']);
    }
}
