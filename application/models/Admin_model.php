<?php

Class Admin_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->mysql = $this->load->database('mysql', true);
        $this->load->model('user_modal');
//        $this->load->model('curl/curl_requests_junifer');
//        $this->load->model('curl/curl_requests_dyball');
    }

    public function get_pending_junifer_registration()
    {

        $sql = "  SELECT  id,customer_id, forename, surname, email, phone1, phone2, UPPER(billingMethod) as billingMethod , first_line_address, UPPER(address_postcode) as 'address_postcode', create_at, electricityProduct_mpans, gasProduct_mprns, elec_meter_serial, gas_meter_serial, added_by_admin,
                  gasProduct_reference, elecProduct_reference 
                  FROM customer_info  WHERE  account_id = '0' AND billingMethod = 'directdebit'";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    public function get_pending_dyball_registration()
    {
        $sql = "  SELECT  id,customer_id, forename, surname, email, phone1, billingMethod, first_line_address, address_postcode, create_at, electricityProduct_mpans, gasProduct_mprns, added_by_admin,
                  gasProduct_reference, elecProduct_reference 
                  FROM customer_info  WHERE  account_id = '0' AND gas_meter_serial = '0' AND gasProduct_mprns = 'na' AND elec_meter_serial = '0' 
                  AND electricityProduct_mpans = 'na' AND elec_mpan_core = 0 AND billingMethod = 'prepay' ORDER BY create_at DESC";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    public function get_customer_info($id)
    {
        $sql = "  SELECT  * FROM customer_info  WHERE  id='{$id}'";
        $query = $this->mysql->query($sql);
        return $query->result_array();
    }

    public function update_customer_id_junifer($junifer_id, $account_id, $database_id, $mpan, $mprn, $elec_meter_serial, $gas_meter_serial)
    {
        $sql = "UPDATE customer_info SET customer_id='{$junifer_id}', account_id='{$account_id}' ,electricityProduct_mpans = '{$mpan}', gasProduct_mprns='{$mprn}',elec_meter_serial='{$elec_meter_serial}', gas_meter_serial='{$gas_meter_serial}' WHERE  id='{$database_id}'";
        $this->mysql->query($sql);
    }

    public function update_customer_id_dyball($id, $database_id, $mpan, $mprn)
    {
        $sql = "UPDATE customer_info SET electricityProduct_mpans = '{$mpan}', gasProduct_mprns='{$mprn}' WHERE  id='{$database_id}'";
        $this->mysql->query($sql);
    }


    function enroll_junifer_customer($database_id, $mpan, $mprn, $type, $elec_meter_serial, $gas_meter_serial)
    {

        $data = $this->get_customer_info($database_id);
        $data = array_shift($data);
        $parameter = [
            'senderReference' => 'Eversmart',
            'reference' => 'Eversmart',
            'billingEntityCode' => 'Eversmart',
            'marketingOptOutFl' => false,
            'submittedSource' => 'Tele',
            'changeOfTenancyFl' => false,
            'contacts' => [
                [
                    'title' => $data['title'],
                    'forename' => $data['forename'],
                    'surname' => $data['surname'],
                    'email' => $data['email'],
                    'phone1' => (!empty($data['phone1'])) ? $data['phone1'] : $data['phone2'],
                    'phone2' => $data['phone2'],
                    'dateOfBirth' => $data['dateOfBirth'],
                    'billingMethod' => "Both",
                    'primaryContact' => true,
                    'address' => [
                        'careOf' => $data['forename'] . ' ' . $data['surname'],
                        'address1' => $data['first_line_address'],
                        'address2' => $data['dplo_address'],
                        'town' => $data['town_address'],
                        'county' => $data['city_address'],
                        'postcode' => $data['address_postcode'],
                        'countryCode' => 'GB'
                    ],
                ]
            ],
            'supplyAddress' => [
                'careOf' => $data['forename'] . ' ' . $data['surname'],
                'address1' => $data['first_line_address'],
                'address2' => $data['dplo_address'],
                'town' => $data['town_address'],
                'county' => $data['city_address'],
                'postcode' => $data['address_postcode'],
                'countryCode' => 'GB'
            ],
            'electricityProduct' => [
                'previousSupplier' => $data['electricityProduct_previousTariff'],
                'previousTariff' => $data['electricityProduct_previousTariff'],
                'productCode' => $data['electricityProduct_productCode'],
                'mpans' => array(['mpanCore' => $mpan]),
                'reference' => $this->user_modal->latest_product_ref('elec_reference_count')
            ]
        ];

        if ($data['tariff_unitRate1Gas'] !== NULL) {
            $parameter['gasProduct'] = [
                'reference' => $this->user_modal->latest_product_ref('gas_reference_count'),
                'productCode' => $data['gasProduct_productCode'],
                'mprns' => array(['mprn' => $mprn])
            ];
        }

        // Add dd credentials to junifer params
        $parameter['directDebitInfo'] = [
            'bankAccountSortCode' => $data['bankAccountSortCode'],
            'bankAccountNumber' => $data['bankAccountNumber'],
            'bankAccountName' => $data['bankAccountName'],
            'regularPaymentAmount' => number_format(($data['tariff_newspend'] / 12), 2),
            'regularPaymentDayOfMonth' => 20
        ];

        // Sign the customer up in Junifer

        // var_dump($parameter);

        $response = $this->curl->junifer_request('/rest/v1/customers/enrolCustomer', $parameter, 'post');
        $junifer_response = $this->parseHttpResponse($response);


        // Now grab the account id
            $junifer_account_response = $this->curl->junifer_request('/rest/v1/accounts/?email=' . $data['email']);
            $junifer_account_id = $this->parseHttpResponse($junifer_account_response);
//
//            echo "<pre>";
//            print_r($junifer_account_id);
//            echo "</pre>";


//        echo "<pre>";
//        print_r($junifer_response);
//        echo "</pre>";



        $format_msg = [];

        if (isset($junifer_response['body']['errorCode'])) {
            $format_msg = ['error' => true, 'description' => $junifer_response['body']['errorDescription']];
        } else {
            $format_msg = ['error' => false, 'customer_id' => $junifer_response['customerId']];
            $this->update_customer_id_junifer($junifer_response['body']['customerId'], $junifer_account_id['body']['results'][0]['id'], $database_id,$mpan, $mprn, $elec_meter_serial, $gas_meter_serial);
        }
        return $format_msg;
    }

    public function enroll_dyball_customer($database_id, $mpan, $mprn, $type)
    {

        $data = $this->get_customer_info($database_id);
        $data = array_shift($data);
        $dyb_parameter = [
            'AuthKey' => '682d34cb-0a9e-420e-8e05-49464a517dd1',
            'CustomerReference' => "DYB_" . $database_id,
            'SaleMadeBy' => "Eversmart Website",
            'SaleMadeOn' => date('Y-m-d', time()),
            'SaleType' => "CoS", // only CoS currently supported
            'CustomerTitle' => $data['title'],
            'CustomerFirstName' => $data['forename'],
            'CustomerLastName' => $data['surname'],
            'CustomerDateOfBirth' => $data['dateOfBirth'],
            'CustomerAddressLine1' => $data['first_line_address'],
            'CustomerPostcode' => $data['address_postcode'],
            'CustomerPhoneNumber' => (!empty($data['phone1'])) ? $data['phone1'] : $data['phone2'],
            'PaymentMethod' => "PrePay",
            'ElectricityTariffName' => $data['tariff_tariffName'],
            'ElectricEAC' => $data['quotation_eac'],
            'MPANs' => array(array( // Leave this - API expects double array
                "MPAN" => $mpan,
                "UseIndustryDataLookup" => true
            ))
        ];

        if ($data['tariff_unitRate1Gas'] !== NULL) {
            $dyb_parameter['GasTariffName'] = $data['tariff_tariffName'];
            $dyb_parameter['GasEAC'] = $data['quotation_aq'];
            $dyb_parameter['MPRNs'] = array(array( // Leave this - API expects double array
                "MPRN" => $mprn,
                "UseIndustryDataLookup" => true
            ));
        }

        $response = $this->curl->dyball_curl($dyb_parameter, 'salemade');
        $dyball_response = $this->parseHttpResponse($response);

        if ((isset($dyball_response['body']['StatusCode']['error']) && $dyball_response['body']['StatusCode']['error'] == 200) || (isset($dyball_response['body']['StatusCode']) && $dyball_response['body']['StatusCode'] == 200)) {

            $format_msg = ['error' => false];
            $this->update_customer_id_dyball(0, $database_id, $mpan, $mprn);
        } else {
            $format_msg = ['error' => true, 'description' => $dyball_response['body']['Value']['ErrorMessage']];
        }

        return $format_msg;

    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart) {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if (strpos($lines[0], 'HTTP/1.1') !== false) {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0])) {
                    // 4xx or 5xx error
                    // so log the error
                    // $this->logMessage('error', 'Line 0=' . $lines[0], 2);
                }

                foreach ($lines as $j => $line) {
                    if ($j === 0) {
                        $headers['http_code'] = $line;
                    } else {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            } // If last element, probably body
            else if ($i === sizeof($responseParts) - 1) {
                $body = json_decode($responsePart, true);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }


}
