<?php

Class ESE_Controller extends CI_Controller
{


    // Make sure these are available throughout system
    public $account_id = array();
    public $customer_type = array();
    public $redfish_token = '';
    protected $template = 'new-world/master',
    $body_classes = 'transparent-nav';


    function __construct()
    {
        parent::__construct();
        $this->load->library('jwt');
        $this->load->helper('url');

        // Password / token
        $this->salt = file_get_contents('ese_key.txt'); // JWT Key

        if(isset($_SERVER['HTTP_X_CUSTOMER_TOKEN'])) {

            // Check if token has expired
            $active_check = $this->token_active_check();

            // If we have an unexpired token...
            if($active_check === true) {

                // Decode and return it
                $decoded_token = $this->decode_token($_SERVER['HTTP_X_CUSTOMER_TOKEN']);

                // If junifer_account_id exists in token...refresh it
                if ($decoded_token->account_id) {

                    $this->account_id = $decoded_token->account_id;
                    $this->customer_type = $decoded_token->customer_type;

                    $refresh_return = $this->refresh_token();
                    $Refresh = json_decode($refresh_return, 1);

                    // If not active - redirect
                    if ($Refresh['success'] != 1) {
                        redirect('/', 'refresh');
                        exit;
                    }
                }
                else {

                    // If not active - redirect
                    redirect('/', 'refresh');
                    exit;
                }
            }
            else {

                // If not active - redirect
                redirect('/', 'refresh');
                exit;
            }

        }


        // Energylinx
        $this->affiliate_id = 'EVERSMARTENERGYSUPP';

        // Dyball
        $this->authkey = 'EVERSMARTENERGYSUPP';
        $this->authkey_dyball_regman = 'b450e45e-63b9-4c68-b1d7-7b5794115314';
        $this->authkey_dyball_quote = '4bd1ed71-834c-4c85-960b-df794cce7f19';
        $this->authkey_dyball_serve = 'eff9df58-ac90-42fb-9f01-e948d96402a9';
        $this->authkey_dyball_gasman = 'ed3e6b7a-ca3d-402b-b353-cc83ca121912';
        $this->authkey_dyball_salemade = '682d34cb-0a9e-420e-8e05-49464a517dd1';

        // Redfish
        if( $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk' || $_SERVER['HTTP_HOST'] == 'eversmartenergy.co.uk' ) {
            // Live
            $this->Api_Url = "https://eversmartapi.redfishuk.com/api/";
            $this->Api_Key = "861E0415-4FBD-4877-8586-E80A1B356C2A";
        }
        else {
            // UAT
            $this->Api_Url = "https://eversmartapi.uat.redfishuk.com/api/";
            $this->Api_Key = "00E442A4-B54E-4A94-82CE-1E929560A23E";
        }

        // Redfish - temp fix for invalid token issues
        $this->Client_ip =  '123.123.123.123';
        $this->client_agent = 'Chrome';

        $this->sales_report_pass = 'amass.implicit.famish.gawky';

    }


    /**
     * Takes a token and decodes it
     *
     * @param string $token
     * @return string token array
     */
    private function decode_token($token){

        // Have we received a token
        if(isset($token)) {

            // Decode token
            return Jwt::decode($token, $this->salt, array('HS256'));

        } else {

            // No headers - destroy session
            $this->load->helper('url');
            redirect('/', 'refresh');
            exit;
        }
    }

    /**
     * Check to see if expiry date is set and is in the future
     *
     * @param null
     * @return boolean
     */
    function token_active_check(){

        $token = $_SERVER['HTTP_X_CUSTOMER_TOKEN'];

        try {
            // Decode token
            $decoded = Jwt::decode($token, $this->salt, array('HS256'));

            if( $decoded->token_expiry > 0 && $decoded->token_expiry>time() ){
                return true;
            } else {
                return false;
            }

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Every time there is a new call, we need to set up a refreshed token (with an updated expiry date)
     *
     * @param null
     * @return string json array
     */
    function refresh_token()
    {
        // Set up token vars
        $token['account_id'] = $this->account_id;
        $token['customer_type'] = $this->customer_type;
        $token['redfish_token'] = $this->redfish_token;
        $token['token_expiry'] = time() + 604800; // 1 hour from now

        // Generate token
        $output['token'] = Jwt::encode($token, $this->salt);

        // return success and token
        return json_encode(['success' => true, 'error' => false, 'token' => $output['token']]);

    }

    /**
     * Get the latest token for response
     *
     * @param null
     * @return string json array
     */
    function get_latest_token()
    {
        // Set up token vars
        $token['account_id'] = $this->account_id;
        $token['customer_type'] = $this->customer_type;
        $token['redfish_token'] = $this->redfish_token;
        $token['token_expiry'] = time() + 3600; // 1 hour from now

        // Generate token
        $output['token'] = Jwt::encode($token, $this->salt);

        // return success and token
        return $output['token'];

    }

    public static function logMessage($errLevel, $msg, $depth = 1)
    {
        $btStack = debug_backtrace($depth);
        $callerFrame = $btStack[$depth - 1];

        $sid = substr(session_id(), 0, 8);
        $method = $callerFrame['class'] . '.' . $callerFrame['function'];
        $lineNo = $callerFrame['line'];
        $fullMessage = 'SID:' . $sid . ' ' . $method . '(' . $lineNo . '): ' . $msg;

        log_message($errLevel, $fullMessage);
    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart) {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if (strpos($lines[0], 'HTTP/1.1') !== false) {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0])) {
                    // 4xx or 5xx error
                    // so log the error
                    $this->logMessage('error', 'Line 0=' . $lines[0], 2);
                }

                foreach ($lines as $j => $line) {
                    if ($j === 0) {
                        $headers['http_code'] = $line;
                    } else {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            } // If last element, probably body
            else if ($i === sizeof($responseParts) - 1) {
                $body = json_decode($responsePart, true);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }

    function unescapeHeaderValue($v)
    {
        // When json strings are parsed, escape characters like '\/' are converted to '/'
        return json_decode('"' . $v . '"');
    }

    function parseCookieHeader($header)
    {
        $headerUnescaped = $this->unescapeHeaderValue($header);
        $headerParts = explode("; ", $headerUnescaped);
        $accountPart = $headerParts[0];
        $expiresPart = $headerParts[1];

        if (empty($accountPart))
        {
            $msg = 'Empty cookie received from Red Fish';
            $this->logMessage('error', $msg, 2);
            // $this->redirectOnError($msg);
        }
        return array(
            'account' => substr($accountPart, strpos($accountPart, "=")+1),
            'expires'=> substr($expiresPart, strpos($expiresPart, "=")+1)
        );
    }

}
