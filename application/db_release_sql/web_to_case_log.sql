

CREATE TABLE `web_to_case_log` (
  `web_to_case_log_id` int(11) NOT NULL,
  `org_id` varchar(255) DEFAULT NULL COMMENT 'orgid',
  `name` varchar(255) DEFAULT NULL COMMENT '00N1n00000SB9PS',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL COMMENT '00N1n00000SB9PN',
  `address` varchar(255) DEFAULT NULL COMMENT '00N1n00000SB9PI',
  `postcode` varchar(255) DEFAULT NULL COMMENT '00N1n00000SB9Ph',
  `subject` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `junifer_customer_id` int(11) DEFAULT NULL COMMENT '00N1n00000SB9OK',
  `dyball_account_id` int(11) DEFAULT NULL COMMENT '00N1n00000SB9NR',
  `external_system_registration_status` varchar(255) DEFAULT NULL COMMENT '00N1n00000SB9Pc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `web_to_case_log` ADD PRIMARY KEY (`web_to_case_log_id`);

ALTER TABLE `web_to_case_log` MODIFY `web_to_case_log_id` int(11) NOT NULL AUTO_INCREMENT;


INSERT INTO `admin_role_type` (`admin_role_type_id`, `role_type`) VALUES (NULL, 'SalesForce');




ALTER TABLE `web_to_case_log` ADD `contact_name` VARCHAR(255) NULL DEFAULT NULL AFTER `external_system_registration_status`, ADD `discount_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `contact_name`, ADD `discount_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `discount_elec`, ADD `supply_end_date_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `discount_gas`, ADD `supply_end_date_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `supply_end_date_elec`, ADD `tariff_comparison_rate_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `supply_end_date_gas`, ADD `tariff_comparison_rate_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `tariff_comparison_rate_gas`, ADD `expected_supply_start_date_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `tariff_comparison_rate_elec`, ADD `expected_supply_start_date_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `expected_supply_start_date_gas`, ADD `estimated_annual_cost_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `expected_supply_start_date_elec`, ADD `estimated_annual_cost_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `estimated_annual_cost_elec`, ADD `gas_unit_rate` VARCHAR(255) NULL DEFAULT NULL AFTER `estimated_annual_cost_gas`, ADD `elec_unit_rate` VARCHAR(255) NULL DEFAULT NULL AFTER `gas_unit_rate`, ADD `daily_standing_charge_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `elec_unit_rate`, ADD `daily_standing_charge_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `daily_standing_charge_elec`, ADD `assumed_annual_consumption_elec` VARCHAR(255) NULL DEFAULT NULL AFTER `daily_standing_charge_gas`, ADD `assumed_annual_consumption_gas` VARCHAR(255) NULL DEFAULT NULL AFTER `assumed_annual_consumption_elec`, ADD `record_type` VARCHAR(255) NULL DEFAULT NULL AFTER `assumed_annual_consumption_gas`, ADD `company` VARCHAR(255) NULL DEFAULT NULL AFTER `record_type`;
ALTER TABLE `web_to_case_log` ADD `case_send_date` VARCHAR(255) NULL DEFAULT NULL AFTER `company`;

