ALTER TABLE `customer_info` ADD `new_password` BINARY(255) NOT NULL AFTER `password`;
ALTER TABLE `customer_info` CHANGE `new_password` `new_password` BLOB NOT NULL;
ALTER TABLE `customer_info` CHANGE `new_password` `new_password` BLOB NULL DEFAULT NULL;




-- Run these after password update function - update_passwords()
-- ALTER TABLE `customer_info` DROP `password`;
-- ALTER TABLE `customer_info` CHANGE `new_password` `password` BLOB NULL DEFAULT NULL;