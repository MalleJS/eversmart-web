
ALTER TABLE `customer_info` CHANGE `customer_id` `customer_id` VARCHAR(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT 'Junifer - customer id is first entry point for all account info';

ALTER TABLE `customer_info` CHANGE `account_id` `account_id` INT(11) NOT NULL DEFAULT '0' COMMENT 'Junifer account id';

ALTER TABLE `customer_info` ADD `dyball_account_id` INT NULL DEFAULT NULL COMMENT 'Dyball' AFTER `account_id`;
