CREATE TABLE `fuel_type` ( `fuel_type_id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , PRIMARY KEY (`fuel_type_id`)) ENGINE = InnoDB;
INSERT INTO `fuel_type` (`fuel_type_id`, `name`) VALUES (NULL, 'Gas'), (NULL, 'Elec');


CREATE TABLE `meter_reading_log` ( `meter_reading_log_id` INT NOT NULL AUTO_INCREMENT , `customer_id` INT(11) NOT NULL, `fuel_type_id` TINYINT NOT NULL, `meter_reading` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL , PRIMARY KEY (`meter_reading_log_id`)) ENGINE = InnoDB;


