CREATE TABLE `customer_monthly_interest` ( `customer_monthly_interest_id` INT NOT NULL AUTO_INCREMENT , `customer_id` INT(11) NOT NULL , `interest_redemption_type_id` TINYINT(1) NULL , `interest_redemption_date` TIMESTAMP NULL , `monthly_interest` FLOAT(6,2) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`customer_monthly_interest_id`)) ENGINE = InnoDB;
CREATE TABLE `interest_redemption_type` ( `interest_redemption_type_id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `created_date` TIMESTAMP NOT NULL , PRIMARY KEY (`interest_redemption_type_id`)) ENGINE = InnoDB;
INSERT INTO `interest_redemption_type` (`interest_redemption_type_id`, `name`, `created_date`) VALUES (NULL, 'Cash', CURRENT_TIMESTAMP), (NULL, 'Credit', CURRENT_TIMESTAMP);

ALTER TABLE `customer_info` ADD `monthly_interest_customer_signup_date` TIMESTAMP NULL AFTER `dyball_import_success`;
