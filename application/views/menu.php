<div id="sticky-anchor"></div>
<ul class="dashboard-menu">

    <?php if( $this->session->userdata('login_data')['signup_type'] != 3 ) { // Only show these if you are not a Dyball customer ?>

        <li class="dashboard">
            <a href="<?php echo base_url(); ?>index.php/user/overview"><span class="small-dashboard-icon"></span>Overview</a>
        </li>
		
		<li class="referral">
            <a href="<?php echo base_url(); ?>index.php/user/referral"><span class="small-dashboard-icon"></span>Refer a friend</a>
        </li>

		<li class="accounts">
            <a href="<?php echo base_url(); ?>index.php/user/account"><span class="small-dashboard-icon"></span>My Account</a>
        </li>

        <li class="billing">
            <a href="<?php echo base_url(); ?>index.php/user/invoice"><span class="small-dashboard-icon"></span>Invoices</a>
        </li>

        <!--<li class="payments">
            <a href="<?php //echo base_url(); ?>index.php/user/payments"><span class="small-dashboard-icon"></span>Payments
        </a></li>-->

        <li class="meter_reading">
            <a href="<?php echo base_url(); ?>index.php/user/meter_reading"><span class="small-dashboard-icon"></span>Elec. Meter Reading
        </a></li>
		
		 <li class="meter_reading">
            <a href="<?php echo base_url(); ?>index.php/user/meter_reading"><span class="small-dashboard-icon"></span>Gas Meter Reading
        </a></li>

        <!---<li class="usage">
            <a href="<?php// echo base_url(); ?>index.php/user/usage"><span class="small-dashboard-icon"></span>Usage</a>
        </li>-->

	    <li class="communications">
            <a href="<?php echo base_url(); ?>index.php/user/messages"><span class="small-dashboard-icon"></span>Messages</a>
        </li>

        <!--<li class="moving_home">
            <a href="<?php// echo base_url(); ?>index.php/user/moving_home"><span class="small-dashboard-icon"></span>Moving Home</a>
        </li>-->

        <?php } else { ?>

            <li class="accounts">
                <a href="<?php echo base_url(); ?>index.php/user/account"><span class="small-dashboard-icon"></span>My Account</a>
            </li>

        <?php } ?>
	</ul>