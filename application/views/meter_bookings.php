<!DOCTYPE html> 
<html lang="en">

<head> 
    <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | <?php if( !empty($title) ){ echo $title; }else{ echo 'Dashboard'; } ?></title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">

		   <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url() ?>dashboard/css/dashboard.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
 
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />



<style>
#note {
    margin:auto;
    position:fixed;
    opacity:0;
	bottom:-200px;
	right:0

}

div#note img {
    max-width: 200px;
}
.red-rounded-wave {
    background: transparent;
    background-size: contain;
    float: left;
    width: 100%;
}
.termandconditions {
    margin-top: 0;
    background: white;
}
.text-left.field-sorting {
    font-size: 16px;
    text-transform: uppercase;
}
th.no-sorter {
    font-size: 16px;
    text-transform: uppercase;
}
td .text-left {
    text-align: left!important;
    color: #000;
    font-weight: 400;
}
table.tablesorter thead .field-sorting, table.tablesorter thead .field-sorting.asc, table.tablesorter thead .field-sorting.desc, table.tablesorter thead .field-sorting.asc_disabled, table.tablesorter thead .field-sorting.desc_disabled {
    cursor: pointer;
    padding-right: 0!important;
}
.topbar .container {
    max-width: 1500px!important;
}
#my-energy-pd {
    padding-bottom: 0;
}

footer.main_footer {
    display: none;
}
#my-energy-pd .bolier_home-h-l {
    margin-top: 10px;
    position: relative;
    margin-top: -63px;
    font-size: 35px;
}
.loading-page {
margin:20% auto;
width: 100px
 }

 .loader {
  border: 8px solid #fda1b8;
  border-radius: 50%;
  border-top: 8px solid #f64d76;
  width: 90px;
  height: 90px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.btn {
    display: inline-block;
    padding: 4px 14px;
    margin-bottom: 0;
    font-size: 14px;
    line-height: 20px;
    color: #000!important;
}

.btn .caret {
    margin-top: 8px;
    margin-left: 0;
    display: none;
}

ul.pager a {
    background: #ea495c;
    color: #fff!important;
}
li.previous.first-button.disabled a {
    background: #ea495c!important;
}
section.termandconditions .col-sm-12.col-md-12.col-lg-8 {
    margin: auto;
    max-width: 100%!important;
    flex: 0 0 100%!important;
}

.large-screen-fix {
    width: 100%!important;
    margin: 0 auto;
}
.container {
    margin-right: auto;
    margin-left: auto;
    width: 100%!important;
    max-width: 100%!important;
}
.span12 {
    width: 100%!Important;
}
.maxy {
    max-width: 100%;
}
#ajax_list .span12 {
    padding: 0;
    margin: 0;
}
</style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>


<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->

	 <div class="main-inner" id="main_content" >
		<div class="main-rounded-red-header nofixed">
		<div class="red-rounded-wave" id="dashboard_main_bg">
		<div class="topbar" id="red_top">

			<div class="container">
				<div class="row">


					<span class="col-sm-12 col-md-4 col-lg-6 px-0">
						<a class="logo" id="logo-dash" href="<?php echo base_url(); ?>">eversmart.</a>
					</span>

					<div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right" id="mobile_menu" style="position: relative; float:left; padding-top:10px">

					<span class="product-code-no-d">
		
					</span>
					</div>

                    <div class=" col-sm-12 col-md-4 col-lg-2 text-right" id="navbarSupportedContent-3-main">


						 <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2">
					
									<a id="logout_user" onclick="logout_user()" class="dropdown-item waves-effect waves-light" href="javascript:void(0)"><i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout</a>
                                </div>

                    </div>
 

				</div>
			</div>
			<!--- End container-------->
			<!--- End container-------->

		</div>

		<!---end top bar bar----------->


<!----------------start postcode-------------------->

		<section class="mt0 ">
				<div class="boiler_header-h" id="my-energy-pd">
						<div class="container">
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Meter Bookings</span>

								</div>
							</div>
						</div><!------------------End Container----------------->

				</div><!------------------End Postcode------------->

			</section>
<!----------------End postcode--------------------->
		</div>
	</div>


		<!--------------------Start breadcrumbs Care Section------------>

		<section class="termandconditions">
				<div class="container-fluid">
		


						<div class="col-sm-12 col-md-12 col-lg-8">
						  <!---<div class="alert alert-warning">
								<strong>Notification:</strong> Your account is not activated with Junifer. Please activate your account to see your bills. You should have an activation email, if not, <a href="#">click here</a> to resend activation email.
							  </div>-->
						<div class="large-screen-fix">
							<div class="row justify-content-center">
                <?php //debug( $this->session->userdata('login_data') );
                  //echo  $this->session->userdata('login_data')['id'];
                 ?>
						<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
</head>
<body>
	<div style='height:20px;'></div>  
    <div class="maxy">
    <div class="maxy" class="container">
	<div class="maxy" class="col-md-12">
		<?php echo $output; ?>
        </div>
        </div>
    </div>


							</div>
							</div>
						</div>
 

						

						</div>


					</div>




				</div>
				<div id="note">
<img src="<?php echo base_url(); ?>assets/images/random/jordan.png">
</div>
			</section>

<!--------------------End breadcrumbs Care Section------------->


<!----------------Start Footer-------------------------->


    <footer class="main_footer">
		<div class="footer-top-sky-rounded"></div>
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row">
    					<div class="col-sm-4 col-md-3">
    						<h4>eversmart.</h4>
    						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
							<h5>Opening Times: </h5>
							<p>MON - FRI 8am-8pm  <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
    					</div>
    					<div class="col-sm-4 col-md-2">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
    								<li><a href="#">Smart Meters</a></li>
    								<li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Useful Links</h4>
    							<ul class="links-vertical">
									<li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
									<li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_eng.pdf ">Consumer Checklist (UK)</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_wel.pdf ">Consumer Checklist (Welsh)</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-12 col-md-3 col-lg-3">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical" id="small-lower-text">
    								<li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i>   0330 102 7901</a></li>
									<li> <i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
									<li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; 2018 Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>
 
    	</footer>
    	<!----end footer------------>

</div>
      <!-- SCRIPTS -->
      <!-- JQuery -->


      <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


<script>
$(function(){ 

var searchval = '<?php if ( $this->session->userdata('search') !== null ) 
{
echo $this->session->userdata('search'); 
} else { echo '0' ;} 
?>';

var pagemuber = '<?php if ( $this->session->userdata('pagemunber') !== null ) 
{
echo $this->session->userdata('pagemunber'); 
} else { echo '0' ;} 
?>';

var optionselected = '<?php if ( $this->session->userdata('optionselected') !== null ) 
{
echo $this->session->userdata('optionselected'); 
} else { echo '0' ;} 
?>';




if ( !$( '#alert_sucess' ).hasClass( "hide" ) ) {
    $('#note').animate({'opacity':'1'}, 1000);
    $('#note').animate({'bottom': '0px'}, 1000);
     $('#note').animate({'bottom': '-200px'}, 1500);
     $('#note').animate({'opacity':'0'}, 1500);
}
 console.log(searchval);

 if (pagemuber != 0) {
	$('#tb_crud_page').val(pagemuber);

changenumber = $('#tb_crud_page').val();
console.log(changenumber );

$('#tb_crud_page').trigger('submit');
}

if (searchval != 0 &&  optionselected === 'CONTACT_POST_CODE'){
$("#search_field option[value=CONTACT_POST_CODE]").attr('selected', 'selected');
$('#search_text').val(searchval);
$('#crud_search').trigger('submit');
console.log('success');
}

if (searchval != 0 &&  optionselected === 'ELEC_START_DATE'){
$("#search_field option[value=ELEC_START_DATE]").attr('selected', 'selected');
$('#search_text').val(searchval);
$('#crud_search').trigger('submit');
console.log('success');
}

});
</script>	

<script>
$(document).ready(function () {

$(document).on('click', 'a.editzy', function(e) { 


var pagedigit = $('input#tb_crud_page').val();

  $.ajax({
		url: base_url+'index.php/examples/sessionss',
		type: 'get',
		dataType: 'json',
		data:{pagemunber :pagedigit},
		complete: function(){
		
	},
	success:function(response){ }
});

});



$(document).on('click', '.pager li.next-button', function(e) { 

var pagedigit = $('input#tb_crud_page').val();

  $.ajax({
		url: base_url+'index.php/examples/sessionss',
		type: 'get',
		dataType: 'json',
		data:{pagemunber :pagedigit},
		complete: function(){
		
	},
	success:function(response){ }
});

});

$(document).on('click', '.pager li.prev-button', function(e) { 

var pagedigit = $('input#tb_crud_page').val();

  $.ajax({
		url: base_url+'index.php/examples/sessionss',
		type: 'get',
		dataType: 'json',
		data:{pagemunber :pagedigit},
		complete: function(){
		
	},
	success:function(response){ }
});

});

$('#tb_crud_page').on( "keydown", function(event) {
	 
	if(event.which == 13) 
{
var pagedigit = $('input#tb_crud_page').val();

  $.ajax({
		url: base_url+'index.php/examples/sessionss',
		type: 'get',
		dataType: 'json',
		data:{pagemunber :pagedigit},
		complete: function(){
		
	},
	success:function(response){ }
});
}
});

});
</script>
  </body>
  </html>
 