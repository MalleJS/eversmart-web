
<style>



.texty {
	line-height: 1.4;
	padding-top: 9px!important;
}


	.missingmeters {
    float: left;
    margin-top: 10px;
    line-height: 1.4;
    text-align: center;
	display:none;
}
#error_address_msg {
    font-size: large;
    float: left;
    display: block;
    text-align: center;
    margin-top: 26px;
    line-height: 1.2;
}
span#check_btn {
    float: none;
    top: 25px;
    position: relative;
}
input#checkinput {
    width: 93%;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    -webkit-appearance: none;
    font-size: 18px;
    padding: 6px;
    margin-top: 30px;
	text-align: center;
}
#success_address_msg {
    font-size: 25px;
    text-align: center;
    margin-top: 70px;
    background: #f24981;
    color: white;
    padding: 20px;
    border-radius: 11px;
    width: 90%;
    margin-left: auto;
    margin-right: auto;
}
.col-sm-12.gasdropdownlist {
    margin-top: 20px;
}
.gasaddresstitle {
    margin-bottom: 20px;
    text-align: center;
    padding-top: 15px;
}

#quotation_address_gas {
    padding-right: 40px !important;
    background-size: 14px auto !important;
    overflow: scroll;
    font-size: 18px;
    width: 100%;
    text-transform: uppercase;
}
.gasdropdownlist{display:none;}
</style>
<div class="container">
 

	<div class="row">
		<div class="col-md-8 mx-auto">

			<span class="back_step" id="media-mobile-m" style="top: -52px; position: relative;"><a href="javascript:void(0)" id="back_posty" class="material-icons" >
				<a href="javascript:void(0)" id="back_quotefor" class="material-icons back-quotefor" >
				<img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> 
			</span>
	
			<span class="back_step" id="main-bak-dd">
				<a href="javascript:void(0)" id="back_quotefor" class="material-fix material-icons" style="top:-60px;">
					<!-- <a href="javascript:void(0)" id="back_postcode" class="material-icons" style="top:-61px;"> -->
					<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/>
				</a> </span>
			<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
				<div class="content">
					<form action="javascript:void(0)" id="address_form" class="switch-form" name="">
						<div class="row wow fadeInUp" data-wow-delay="400ms">
							<div class="form-group postCodeBox">

								<div class="input-group col-md-8 mx-auto">
								<span class="post_box_headingblue red-txt-energy">Your address</span>
								<div class="col-sm-12">
								<div class="col-sm-12">
									<!-- <input class="form-control paddingtb" id="postcode" name="postcode" required="" type="text" placeholder="Enter Address"> -->
									<select name="quotation_address" id="quotation_address" class="custom-select  form-control paddingtb">
										<option value="0">Please Select Address</option>
										<?php

										###########################################################################
										######                                                               ######
										######          WHAT THE HELL IS HAPPENING HERE?????                 ######
										######          JACK WILL FIX THIS AS SOON AS HE HAS A SECOND        ######
										######          IT'S NOT HOW MULTIDIMENTIONAL ARRAYS SHOULD WORK     ######
										######          IT IS A FUCKING MIRACLE IT'S WORKING                 ######
										######                                                               ######
										###########################################################################

												if( !empty($address_list) ){
													for( $j=0; $j<count($address_list); $j++ ){
														// mpan
													

														if( !empty($address_list[$j]['meterType']) )
														{
															$meterType = $address_list[$j]['meterType'];
														}
														else
														{
															$meterType = '';
														} 

														if( !empty($address_list[$j]['mpan']) ){
															for($m=0; $m<count($address_list[$j]['mpan']); $m++){
																if( count($address_list[$j]['mpan']) >= 1 ){
																	$mpan = implode(',',$address_list[$j]['mpan']);

																}
																else {
																	$mpan = $address_list[$j]['mpan'][$j];

																}
															}
														}else{
															$mpan = '';
														}

														//gas meter serial number
														if( !empty($address_list[$j]['meterSerialGas']) ){
															for($m=0; $m<count($address_list[$j]['meterSerialGas']); $m++){
																if( count($address_list[$j]['meterSerialGas']) >= 1 ){
																	$meterSerialGas = implode(',',$address_list[$j]['meterSerialGas']);

																}
																else {
																	$meterSerialGas = $address_list[$j]['meterSerialGas'][$j];

																}
															}
														}else{
															$meterSerialGas = '';
														}

														// elec meter serial number
														if( !empty($address_list[$j]['meterSerialElec']) ){
															for($m=0; $m<count($address_list[$j]['meterSerialElec']); $m++){
																if( count($address_list[$j]['meterSerialElec']) >= 1 ){
																	$meterSerialElec = implode(',',$address_list[$j]['meterSerialElec']);

																}
																else {
																	$meterSerialElec = $address_list[$j]['meterSerialElec'][$j];

																}
															}
														}else{
															$meterSerialElec = '';
														}

														if( !empty($address_list[$j]['mpanLower']) ){
															for($m=0; $m<count($address_list[$j]['mpanLower']); $m++){
																if( count($address_list[$j]['mpanLower']) >= 1 ){
																	$mpanLower = implode(',',$address_list[$j]['mpanLower']);

																}
																else {
																	$mpanLower = $address_list[$j]['mpanLower'][$j];

																}
															}
														}else{
															$mpanLower = '';
														}

														// mprn
														if( !empty($address_list[$j]['mprn']) ){
															for($m=0; $m<count($address_list[$j]['mprn']); $m++){
																if( count($address_list[$j]['mprn']) > 1 ){
																	$mprn = implode(',',$address_list[$j]['mprn']);
																   $mprn = $address_list[$j]['mprn'][0];
																	$secondmprn =  $address_list[$j]['mprn'][1];

																	//prinht_r($address_list);
																}
																else {
																	$mprn = $address_list[$j]['mprn'][0];
																	$secondmprn = "";
																}
															}
														}else{
															$mprn = '';
															$secondmprn = "";
														}
														?>
															<option data-mpan="<?php echo $mpan ?>" 
															data-gasmeter="<?php echo $meterSerialGas ?>" 
															data-elecmeter="<?php echo $meterSerialElec ?>" 
															data-mpanlower="<?php echo $mpanLower ?>" 
															data-metertype="<?php echo $meterType; ?>" 
															data-mprn="<?php echo $mprn ?>" 
															data-secondmprn="<?php echo $secondmprn ?>"
															data-subb="<?php echo $address_list[$j]['subb'] ?>"
															data-bnam="<?php echo $address_list[$j]['bnam'] ?>"
															data-bnum="<?php echo $address_list[$j]['bnum'] ?>"
															data-thor="<?php echo $address_list[$j]['thor'] ?>"
															data-dplo="<?php echo $address_list[$j]['dplo'] ?>"
															data-town="<?php echo $address_list[$j]['town'] ?>"  
															data-cnty="<?php echo $address_list[$j]['cnty'] ?>"
																data-selectedpostcode="<?php echo $address_list[$j]['pcod']; ?>" 
																value="<?php echo $address_list[$j]['address'] ?>"> 
																<?php echo $address_list[$j]['address']; ?> </option>
												<?php
												}
											}
										?>
									
									</select>

										
									</div>

															<!-- gas dropdown -->

	<div class="col-sm-12 gasdropdownlist">

	<div class="gasaddresstitle">Please select your gas address</div>
										<!-- <input class="form-control paddingtb" id="postcode" name="postcode" required="" type="text" placeholder="Enter Address"> -->
										<select name="quotation_address_gas" id="quotation_address_gas" class="custom-select  form-control paddingtb">
											<option value="0">Please Select Address</option>
											<?php

													if( !empty($address_list_gas) ){
														for( $j=0; $j<count($address_list_gas); $j++ ){
															// mpan

															if( !empty($address_list_gas[$j]['meterType']) )
															{
																$meterType_gas = $address_list_gas[$j]['meterType'];
															}
															else
															{
																$meterType_gas = '';
															} 
	

															//gas meter serial number
															if( !empty($address_list_gas[$j]['meterSerialGas']) ){
																for($m=0; $m<count($address_list_gas[$j]['meterSerialGas']); $m++){
																	if( count($address_list_gas[$j]['meterSerialGas']) >= 1 ){
																		$meterSerialGas_gas = implode(',',$address_list_gas[$j]['meterSerialGas']);

																	}
																	else {
																		$meterSerialGas_gas = $address_list_gas[$j]['meterSerialGas'][$j];

																	}
																}
															}else{
																$meterSerialGas_gas = '';
															}

															// mprn

																// mprn
														if( !empty($address_list_gas[$j]['mprn']) ){
															for($m=0; $m<count($address_list_gas[$j]['mprn']); $m++){
																if( count($address_list_gas[$j]['mprn']) > 1 ){
																	$mprn_gas = implode(',',$address_list_gas[$j]['mprn']);
																    $mprn_gas = $address_list_gas[$j]['mprn'][0];
																	$secondmprn_gas = $address_list_gas[$j]['mprn'][1];

																	//prinht_r($address_list);
																}
																else {
																	$mprn_gas = $address_list_gas[$j]['mprn'][0];
																	$secondmprn_gas = "";
																}
															}
														}else{
															$mprn_gas = '';
															$secondmprn_gas = "";
														}
															
															
															?>
																<option data-gasmeter_gas="<?php echo $meterSerialGas_gas ?>" 
																data-gasmprn_gas="<?php echo $mprn_gas ?>" 
																data-gasmprn_second="<?php echo $secondmprn_gas ?>" 
																data-subb_gas="<?php echo $address_list_gas[$j]['subb'] ?>"
																data-bnam_gas="<?php echo $address_list_gas[$j]['bnam'] ?>"
																data-bnum_gas="<?php echo $address_list_gas[$j]['bnum'] ?>"
																data-thor_gas="<?php echo $address_list_gas[$j]['thor'] ?>"
																data-dplo_gas="<?php echo $address_list_gas[$j]['dplo'] ?>"
																data-town_gas="<?php echo $address_list_gas[$j]['town'] ?>"  
																data-cnty_gas="<?php echo $address_list_gas[$j]['cnty'] ?>"
																	data-selectedpostcode_gas="<?php echo $address_list_gas[$j]['pcod']; ?>" 
																	value="<?php echo $address_list_gas[$j]['address'] ?>"> 
																	<?php echo $address_list_gas[$j]['address']; ?> </option>
													<?php	}
													}
											?>
											<option value="noaddress">My address is not listed</option>
										</select>
										
										</div>

										
										<br>
									

										<div class="missingmeters">
										
										<a href="#" data-toggle="tooltipmsn" data-placement="top" class="msntooltip" title="A meter serial number often referred to as an 'MSN' is a unique number used to identify your meter. You'll be able to find this on the front of your meter, engraved into the meter box or printed on a sticker attached to the meter box. It usually contains a combination of numbers and letters.
										Your MSN is shown on your bill under the 'meter readings' section">Please enter your <span class="gsel"></span> serial number below to continue.</a>
										

										<input type="text" value="" id="checkinput" placeholder="Enter your serial number">

										<span class="input-group-btn btnNew" id="check_btn" >
										<button style="font-size:17px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp  waves-effect waves-light " type="submit">Check</button>
										</span>

										</div>

										<div id="error_address_msg"></div>
										<div class="hidbtn">
										<span class="input-group-btn btnNew" id="continue_btn" >
										<button style="font-size:17px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
										</span>

										<span class="link-address-list hidbtn texty"><a href="<?php echo base_url(); ?>index.php/contact_us">Need some help? <br>Click here to speak with one of our representatives </a></span>
														
																	</div>
														</div>
													</div>
												</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltipmsn"]').tooltip();   
});
</script>