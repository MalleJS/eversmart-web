<div class="container">

							<div class="row">
									<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" class="material-icons backknowusage"><img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> </span>
								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">Please enter your current energy usage&hellip;</h4>

								</div>
								
							</div>

								<div class="switch_box_main">
								<form action="#">
								<div class="row justify-content-center">
								<span class="back_step" id="main-bak-dd"><a href="javascript:void()" class="material-icons backknowusage" style="top:1px;">
									<img src="<?php echo base_url() ?>assets/images/red-back-button.svg"></a></span>
										<div class="col-md-4" id="elec_usage">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/elec-new.png" alt=""/></span>
												</div>
												<!---<div class="content">
													<span class="gas_box_text">
													Electricity
													</span>
												</div>-->
									<span class="usgage_box ">
										<div class="md-form row">
											<div class="col-sm-6">
											<div class="">
											<input maxlength="5" onkeypress="return isNumberKey(event)" id="enter_elec_usage" class="form-control" type="text" placeholder="Usage">
											</div>
											</div>
											<div class="col-sm-6">
												<select class="custom-select elec_usage_select">
                          							<option value="0">Please Select Interval </option>
													<option value="year" selected>Per Year</option>
													<option disabled="disabled" value="month">Per Month</option>
													<!-- <option value="quater">Per Quater</option>
													<option value="week">Per Week</option> -->
												</select>
										</div>
										</div>
										

									</span>


											</div>
										</div><!----------------End First Box-------------------->

							<div class="col-md-4" id="gas_usage">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/gas-new.png" alt=""/></span>
									</div>
									<!---<div class="content">
										<span class="gas_box_text">Gas</span>
									</div>-->

									<span class="usgage_box">

										<div class="md-form row">
										<div class="col-sm-6">

											<input placeholder="Usage" onkeypress="return isNumberKey(event)" maxlength="5" id="enter_gas_usage" class="form-control" type="text">
											</div>
											<div class="col-sm-6">
												<select class="custom-select gas_usage_select">
                                                    <option value="0">Please Select Interval </option>
													<option value="year" selected>Per Year</option>
													<option disabled="disabled" value="month">Per Month</option>
													<!-- <option value="quater">Per Quater</option>
													<option value="week">Per Week</option> -->
												</select>
												</div>
										</div>
										

									</span>

								</div>
							</div><!-----------------End Second Box------------------>

								</div><!-------------------End Row---------------->
  
								<br>
								<div id="error_msg"></div>
								<div class="row justify-content-center">
									<div class="col-sm-12">
									<span class="input-group-btn btnNew" id="continue_btn">
										<button id="enter_usage_form" style="background:#fff!important; color:#ee4392!important; padding:10px 40px !important; font-size:20px " class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="button">Continue</button>
										</span>
									</div>

								</div>
								</form>
								</div>
								<div class="backend_error" ></div>
							</div>
