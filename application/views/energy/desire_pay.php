<div class="container">

							<div class="row">
								<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" id="backtoprepay" class="material-icons"><img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> </span>
								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4">How would you like to pay your bill from now&nbsp;on?</h4>

								</div>
								
							</div>

								<div class="switch_box_main">
								<div class="row justify-content-center">
									<span class="back_step" id="main-bak-dd"><a href="javascript:void(0)" id="backtoprepay" class="material-icons" style="top:-1px;"><img src="<?php echo base_url() ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card desire_pay" data-desire_pay="prepay">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/prepay.png" alt=""/></span>
														</div>

														
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card desire_pay" data-desire_pay="directdebit">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/direct-debit-logo-symbol.png" alt=""/></span>
														</div>

													
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Debit---------------->

										<!-- <div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card pay_energy" data-valuepayenergy="paperbill">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/paper.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Paper Bill</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Paper Bill.</p>
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Paper Bill----------------->

									</div><!-------------------End Row------------->


								</div><!------------------End switch_box_main----------------->
							</div><!------------------End Container----------------->
