<div class="container">

    <div class="row">

        <div class="col-md-10 mx-auto text-center">
            <div id="saasmessage"></div>
            <h4 class="post_box_heading4">I want a quote for&hellip;</h4>

        </div>

    </div>

    <div class="switch_box_main">
        <div class="row justify-content-center">
            <div class="col-md-4 main_card_box" id="elect_quote">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card quotefor" data-quotefor="electricity">
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon">
                                    <img src="<?php echo base_url(); ?>assets/images/energy/elec-new.png" class="energy-icon" alt=""/>
                                </span>
                            </div>
                            <span class="click_select_edit">SELECT</span>
                        </a>
                    </figcaption>
                </figure>
            </div>
            <!-------------End Electricity---------------->

            <div class="col-md-4 main_card_box" id="both_quote">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card quotefor" data-quotefor="both">
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon">
                                    <img class="both-energy-icon" src="<?php echo base_url(); ?>assets/images/energy/both-new.png" alt="" class="energy-icon"/>
                                </span>
                            </div>
                            <span class="click_select_edit">SELECT</span>
                        </a>
                    </figcaption>
                </figure>
            </div><!-------------End Both----------------->
        </div><!-------------------End Row------------->
    </div><!------------------End switch_box_main----------------->
</div><!------------------End Container----------------->

</div><!------------------End Postcode------------->
<script>
    $(document).ready(function () {
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        }

        var code = getQueryVariable("rsCode");
        $(document).ready(function () {
            $("#saasref").val(code != false ? code : '');
            //alert(code);
        });

        if (code) {
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/user/saascode',
                data: {code: code},
                dataType: 'json',
                type: 'post',
                cache: false,
                success: function (response) {
                    if (response.error == 0) {
                        if (code === 'EMMABRADLEY' || code === 'EMMADREW' || code === 'BECKYGODDARD') {
                            jQuery('#saasmessage').html('Join the Eversmart family today!');
                        }
                    }
                }
            });
        }
    });
</script>