<div class="container">
    <div class="col-md-10 mx-auto text-center">
        <h4 class="post_box_heading4">Do you know what your current energy usage is?</h4>
    </div>

    <div class="switch_box_main">
        <div class="row justify-content-center">
            <span class="back_step" id="backtoprepay"><a href="javascript:void(-1)" class="material-icons ie-padding ie-extra-padding"><img src="<?php echo base_url() ?>assets/images/red-back-button.svg"></a> </span>
            <div class="col-md-4 main_card_box" id="cc-energy">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card know_current_usage" data-know_current_usage="kwh">
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon"><img id="kwh-icon" src="<?php echo base_url() ?>assets/images/energy/flash.png" alt=""></span>
                            </div>
                            <span class="click_select_edit">SELECT</span>
                        </a>
                    </figcaption>
                </figure>
            </div>

            <div class="col-md-4 main_card_box" id="cc-energy" style="display:none">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card know_current_usage" data-know_current_usage="spend">
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/wallet.png" alt=""></span>
                            </div>

                            <!--<div class="content">
                                <span class="gas_box_text dark_heading" id="kwh-text" >
                                Enter Spent in £</span>
                            </div>-->

                            <span class="click_select_edit">SELECT</span>
                        </a>
                    </figcaption>
                </figure>
            </div>

            <div class="col-md-4 main_card_box" id="cc-energy">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card know_current_usage" data-know_current_usage="no">
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/Other.png" alt=""></span>
                            </div>

                            <!--<div class="content">
                                <span class="gas_box_text dark_heading">
                                I dont know</span>
                            </div>-->

                            <span class="click_select_edit">SELECT</span>
                        </a>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</div>