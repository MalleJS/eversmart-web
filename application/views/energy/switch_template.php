<div class="container">
<?php //debug($projection_data,1); ?>
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<h2>Your Quotation Summary</h2>

								</div>
							</div>
							<div class="row">
								<div class="col-md-5 mx-auto">

									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
											<div class="card-tariff text-center">
												<div class="switch-me text-center">
													<h3>Switching to us will cost you</h3>
													<!-- <span class="price_switch">£25.75</span> -->
														<span class="price_switch"><?php if( !empty($projection_data['existingTariffElec']['unitRates']) ) { echo '£'. $projection_data['existingTariffElec']['unitRates'][0]['value'].'/year'; } else { echo '-'; } ?></span>

													<span class="price_switch_plan">Per Month</span>
													<a style="background:green!important;padding: 10px 64px;font-size: 16px;" class="btn btn-primary btn-round btn-info" href="<?php echo base_url(); ?>index.php/user/signup">Switch Me!</a>
												</div>



												<h4 class="category"><?php if( !empty($projection_data['energySearchProfile']['existingSupplierNameElec']) ) { echo $projection_data['energySearchProfile']['existingSupplierNameElec']; } else { echo '-'; } ?></h4>
												<ul>
													<!-- <li>£308.91 /year</li> -->
													<li><?php if( !empty($projection_data['existingTariffElec']['unitRates']) ) { echo '£'. $projection_data['existingTariffElec']['unitRates'][0]['value'].'/year'; } else { echo '-'; } ?></li>

													<li>12 months fixed rate</li>
													<li>No exit fees</li>
													<li>No minimum term</li>
												</ul>

												<button type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Tariff Information
												</button>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
<!-- tariff modal -->
<div class="modal fade" id="Triffinfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
						  <div class="modal-dialog" role="document" >
							<div class="modal-content">
							  <div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Your tariff information</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							  </div>
							  <div class="modal-body">

									<table class="table_model">
										<tr>
											<td>Tariff Namee:</td>
											<td><?php if( !empty($projection_data['energySearchProfile']['existingTariffNameElec']) ) { echo $projection_data['energySearchProfile']['existingTariffNameElec']; } else { echo '-'; } ?></td>
										</tr>
										<tr>
											<td>Tariff Type:</td>
											<td>Fixed</td>
										</tr>
										<tr>
											<td>Payment Method:</td>
											<td><?php
												if( !empty($projection_data['energySearchProfile']['desiredPayTypes']) ) {
													if( $projection_data['energySearchProfile']['desiredPayTypes'][0] == 1 )
														{
																	echo 'Direct Debit';
														}
														if( $projection_data['energySearchProfile']['desiredPayTypes'][0] == 5 )
															{
																		echo 'PrePay';
															}
															if( $projection_data['energySearchProfile']['desiredPayTypes'][0] == 15 )
																{
																			echo 'Paper Bill';
																}
													} else {
														echo '-';
													} ?></td>
										</tr>
										<tr>
											<td>Exit Fees</td>
											<td>None</td>
										</tr>
									</table>
							  <h4 class="model-heading">Electricity</h4>
								<table class="table_model">
										<tr>
											<td>Unit Rate:</td>
											<td><?php if( !empty($projection_data['energySearchProfile']['resultsExistingkWhsElec']) ) { echo $projection_data['energySearchProfile']['resultsExistingkWhsElec']. 'p per kWh'; } else { echo '-'; } ?></td>
										</tr>
										<tr>
											<td>Tariff Comparison Rate:</td>
											<td><?php if( !empty($projection_data['existingTariffElec']['standingChargeElec']) ) { echo $projection_data['existingTariffElec']['standingChargeElec']. 'p per kWh'; } else { echo '-'; } ?></td>
										</tr>

								</table>



							  </div>

							</div>
						  </div>
						</div>
