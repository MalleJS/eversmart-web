<?php //debug($quote_summary,1); ?>

<div class="container">
						
						<div class="row posi-rele">
						<!-- <span class="back_step" style="position:absolute; top:94px;left:-6px"><a href="boiler-switch-five.html" class="material-icons"><img src="images/red-back-button.svg"></a>
						</span> -->
								<div class="col-md-6  col-sm-6 ">
																			
									<h3 class="text-center quote-heading">What you paying?</h3>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
										<div class="inner-card" style="padding-bottom:74px;">
										<span class="registration-page-heading-energy-q">										
										Your Current Energy <br>Spend
										</span>
										
										<span class="registration-page-energy-q">										
										 <span class="quo-per-month" style="font-size:60px; line-height:72px;">	<strong>&#163; 
										 <?php
										 	if( !empty($quote_summary['energySearchProfile']['resultsExistingSpend']) ){
										 		echo $quote_summary['energySearchProfile']['resultsExistingSpend'];
										 	}else{ echo '0'; }
										 ?></strong> </span>
										</span>
										</div>
										</div>
									</div>
								
								</div>
								
									<div class="col-md-6 col-sm-6 ">								
									<h3 class="text-center quote-heading">What we Offers?</h3>
									<?php if( !empty($quote_summary['searchResults']) ){
											for( $i=0; $i<count($quote_summary['searchResults']); $i++ ){
												if( $i== 0 ){
											 ?>
													<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
														<div class="content">
														<div class="inner-card">
														<span class="registration-page-heading-energy-q green-txt-quote">										
														<?php echo $quote_summary['searchResults'][$i]['tariffName'] ?> <br> <span style="font-size:16px;">will save</span> &#163; <?php echo $quote_summary['energySearchProfile']['resultsExistingSpend'] - $quote_summary['searchResults'][$i]['newSpend']; ?>/mo.
														</span>
														
														<span class="registration-page-energy-q">										
														 <span class="quo-per-month brown-txt-quote">	
														 	<input type="hidden" id="orginial_price" value="<?php echo $quote_summary['searchResults'][$i]['newSpend']; ?>">
														 	<strong>&#163; <span id="total_price">
														 	<?php 
														 	 echo $quote_summary['searchResults'][$i]['newSpend']; ?></span></strong><br> <span class="small-price-txt">/mo.</span></span>
														</span>
														<a href="javascript:void(0)" class="" data-toggle="modal" data-target="#Triffinfo">
																  View plan details
																</a>
														</div>
														</div>
													</div>
										<?php	}
									}
									}else{ ?>
											<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
														<div class="content">
														<div class="inner-card">
														<span class="registration-page-heading-energy-q green-txt-quote">									Not Available	
														 <br> <span style="font-size:16px;">will save</span> &#163; 0/mo.
														</span>
														
														<span class="registration-page-energy-q">										
														 <span class="quo-per-month brown-txt-quote">	
														 	<input type="hidden" id="orginial_price" value="0">
														 	<strong>&#163; <span id="total_price">
														 	0</span></strong><br> <span class="small-price-txt">/mo.</span></span>
														</span>
														<a href="javascript:void(0)" class="" data-toggle="modal" data-target="#Triffinfo">
																  View plan details
																</a>
														</div>
														</div>
													</div>
									<?php } ?>

									
								
								</div>
						</div><!---------------End Row----------->
						
							<div class="row ">
							
						</div><!---------------End Row---------->
						
						
						
						 
						<div class="row" id="bill-plan-table">
							<h3 class="text-center quote-heading">Add Homecare Service <span style="font-size:14px;">(optional):</span></h3>

							<div class="col-sm-3 text-center home_service_plan" data-homeserviceprice="3.12" data-planid='1'>
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"><span class="b-price-a">£3.12</span>
														</span>														
													</span>
												</span>											
												<span class="plan-home-care"><span>Homecare 2.2</span></span>												
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill" id="select_plan_1">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									<span class="buy-now-bill" id="selected_plan_1" style="display: none">
										<a href="#" class="red-btn">Selected Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
							
							<div class="col-sm-3 text-center home_service_plan" data-homeserviceprice="4.14" data-planid='2'>
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"> <span class="b-price-a">£4.14</span>
														</span>														
													</span>
												</span>
													<span class="plan-home-care"><span>Homecare 1.2</span></span>
																							
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill" id="select_plan_2">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									<span class="buy-now-bill" id="selected_plan_2" style="display: none">
										<a href="#" class="red-btn">Selected Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
							
							<div class="col-sm-3 text-center home_service_plan" data-homeserviceprice="2.18" data-planid='3'>
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"><span class="b-price-a">£2.18</span>
														</span>														
													</span>
												</span>	
												<span class="plan-home-care"><span>Homecare 2.1</span></span>
																								
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill" id="select_plan_3">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									<span class="buy-now-bill" id="selected_plan_3" style="display: none">
										<a href="#" class="red-btn">Selected Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
					
						
							<div class="col-sm-3 text-center home_service_plan" data-homeserviceprice="5.21" data-planid='4'>
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"> <span class="b-price-a">£5.21</span>
														</span>														
													</span>
												</span>	

												<span class="plan-home-care"><span>Homecare 2.2</span></span>											
																								
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
											
									<span class="buy-now-bill" id="select_plan_4">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									<span class="buy-now-bill" id="selected_plan_4" style="display: none">
										<a href="#" class="red-btn">Selected Plan</a>
									</span>
										
									</div>
						
							</div><!---------End Box One---------->
						
						
								
				
						
							
						</div>
						
						<div class="row justify-content-center">
									<div class="col-sm-12">
										<form id="quote_summary_form" method="post" action="<?php echo base_url();?>index.php/user/signup">
											<input type="hidden" name="plan_price" id="plan_price">
											<input type="hidden" name="complete_price" id="complete_price">
											<input type="hidden" name="plan_id" id="plan_id">
											<span class="input-group-btn btnNew" id="continue_btn">
												<button id="to_signup_form" style="background:#fff!important; color:#ee4392!important; padding:10px 80px !important; font-size:20px;" class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">
													Continue
												</button>
												</span>
										</form>
									</div>

								</div>
						</div>