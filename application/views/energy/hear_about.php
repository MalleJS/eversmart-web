<div class="container">

							<div class="row">
							<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" id="back_known" class="material-icons" >
									<img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> </span>
								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">How did you hear about us?</h4>

								</div>
								
							</div>

								<div class="switch_box_main">
								<div class="row">
								<span class="back_step" id="main-bak-dd"><a href="javascript:void(0)" id="back_known" class="material-icons" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>
									
										<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="google">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/google.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading">Google</span>
														</div>-->													
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
										
											<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="TV">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/tv.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading">TV</span>
														</div>-->														
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
										
										<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="Radio">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Radio.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading">Radio</span>
														</div>-->														
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
										
										<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="Facebook">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img class="" src="<?php echo base_url(); ?>assets/images/energy/facebook-logo.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading">Facebook</span>
														</div>-->													
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->


								</div>

								<div class="row justify-content-center main-top-margin-60">
								
								
								<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="Comparison Site">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_compare_arrows_black_24px.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading" id="com-price-site">Price Comparison Site </span>
														</div>-->														
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
								
										<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="Referral">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img id="ref-icon" src="<?php echo base_url(); ?>assets/images/energy/Referral.png" alt=""/></span>
														</div>
														<!---<div class="content">
															<span class="gas_box_text dark_heading">Referral</span>
														</div>-->														
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
										
										
										<div class="col-md-3 hear_about_us main_card_box" id="here-about-us-page" data-hearabout="Others">
											<figure class="effect-sadie">
												<figcaption>
													<div class="gas_card ">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Others.png" alt=""/></span>
														</div>
														<!--<div class="content">
															<span class="gas_box_text dark_heading">Others</span>
														</div>-->												
														<span class="click_select_edit">SELECT</span>
													</div>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->
								



								</div>
								</div>
							</div>
