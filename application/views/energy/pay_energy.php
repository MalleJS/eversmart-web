<div class="quoteStepWrap" id="quoteStepWrap--paymentType">

	<h4 class="post_box_heading4">How do you pay for your energy?</h4>

	<span class="back_step" id="media-mobile-m">
		<a href="javascript:void(0)" id="back_address" class="material-icons">
			<img style="background:none; padding:0; height:auto; width:60px;"src="<?php echo base_url() ?>assets/images/backnew-mobile.png"/>
		</a>
	</span>

	<div class="quoteStepContent">
		<ul>
			<li>
				<a class="gas_card pay_energy" data-valuepayenergy="prepay" data-payment-type="pay as you go">
					<div class="gas_box_icon gap_icon imgWrap">
						<img src="<?php echo base_url() ?>assets/images/Quote/method-payg.svg" alt=""/>
					</div>
					<span class='hoverTxt'>SELECT</span>
				</a>
			</li>
			<li>
				<a class="gas_card pay_energy" data-valuepayenergy="directdebit" data-payment-type="pay monthly">
					<div class="gas_box_icon gap_icon imgWrap">
						<img src="<?php echo base_url() ?>assets/images/Quote/method-monthly.svg" alt=""/>
					</div>
					<span class='hoverTxt'>SELECT</span>
				</a>
			</li>
			<!-- <li>
				<a class="gas_card pay_energy" data-valuepayenergy="directdebit" data-payment-type="pay yearly">
					<div class="gas_box_icon gap_icon imgWrap">
						<img src="<?php echo base_url() ?>assets/images/Quote/method-yearly.svg" alt=""/>
					</div>
					<span class='hoverTxt'>SELECT</span>
				</a>
			</li> -->
		</ul>
	</div>

</div>
