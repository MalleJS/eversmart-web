<div class="container">
						
							<div class="row">
							<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" id="backtobedroom" class="material-icons select_bed"><img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> </span>
								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4 mt30 mb30" style="padding-bottom:60px;">How many people live in your home?</h4>
									
								</div>
							
							</div>
							<div class="row">
										<span class="back_step" id="main-bak-dd"><a href="javascript:void(0)" id="backtobedroom" class="material-icons select_bed" style="top:-1px;"><img src="<?php echo base_url() ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card people_live" data-numberPeople="1">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/user-one.png" alt=""/></span>
														</div>

														<!---<div class="content">
															<span class="gas_box_text dark_heading">
															1-2 People</span>
														</div>-->
														<!-- <p class="text-center">If you have a gas meter. your boiler uses Prepay.</p> -->
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card people_live" data-numberPeople="2">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/user-two.png" alt=""/></span>
														</div>

														<!--<div class="content">
															<span class="gas_box_text dark_heading">
															3-4 People</span>
														</div>-->
														<!-- <p class="text-center">If you have a gas meter. your boiler uses Direct Debit</p> -->
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Debit---------------->
 
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card people_live" data-numberPeople="3">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/user-plus.png" alt=""/></span>
														</div>

														<!---<div class="content">
															<span class="gas_box_text dark_heading">
															5+ People</span>
														</div>-->
														<!-- <p class="text-center">If you have a gas meter. your boiler uses Paper Bill.</p> -->
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Paper Bill---------------->

									</div><!-------------------End Row------------>
									<div class="backend_error" ></div>
						</div>