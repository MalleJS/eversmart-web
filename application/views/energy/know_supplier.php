<div class="container">

							<div class="row">
								<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" class="material-icons backfromprepay"><img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a> </span>
								<div class="col-md-8 mx-auto text-center">
									<h4 class="post_box_heading4">Do you know the name of your current<br> Energy Supplier <span style="font-weight:bold;">and</span> Tariff?</h4>

								</div>
								
							</div>

								<div class="switch_box_main">
								<div class="row justify-content-center">
									<span class="back_step" id="main-bak-dd"><a href="javascript:void(0)" id="" class="material-icons backfromprepay" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_energy" data-know_current_energy="no">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/no.png" alt=""/></span>
														</div>

														<!---<div class="content">
															<span class="gas_box_text dark_heading">
															NO</span>
														</div>-->

														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
									</div><!-------------End NO------------------>

										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_energy" data-know_current_energy="yes">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Yes.png" alt=""/></span>
														</div>

														<!--<div class="content">
															<span class="gas_box_text dark_heading">
																Yes</span>
														</div>-->

														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
									</div><!-------------End yes----------------->

							</div>
							</div>
							</div>
