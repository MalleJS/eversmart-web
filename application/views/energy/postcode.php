<div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto text-center">
       <h4 class="post_box_heading4">Enter your postcode below and we'll give you a&nbsp;quote</h4>
    </div>
  </div>

  <div class="row">
    <span class="back_step" id="media-mobile-m" style="top: 10px; position: relative;">
      <a href="javascript:void(0)" id="back_posty" class="material-icons" >
        <img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/>
      </a> 
    </span>

    <div class="col-md-6 mx-auto mtop50">
      <span class="back_step" id="main-bak-dd">
        <a href="javascript:void(0)" id="back_posty" class="material-icons" style="top:-79px;">
          <img src="<?php echo base_url() ?>assets/images/red-back-button.svg"/>
        </a>
      </span>

      <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
        <div class="content">
          <form id="form-postcode-simple" class="switch-form" name="">
            <div class="row wow fadeInUp" data-wow-delay="400ms" style="padding:12px 0px">
              <div class="form-group postCodeBox" id="rounder-b-energy">
                <div class="input-group">
                  <div class="md-form" id="p-s-code">
                    <input  id="form-postcode" class="form-control" type="text" placeholder="Postcode" maxlength="10" spellcheck="false" style="text-transform:uppercase" required>
                  </div>
                  <span class="input-group-btn btnNew">
                  <button  id="redd-btn-pc" class=" red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit" style="padding:18px 23px">Get Quote</button>
                  </span>
                </div>
              </div>
            </div>
            <div class="error_msg" style="display:none">
              <div class="alert alert-danger" role="alert" id="error_message"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
