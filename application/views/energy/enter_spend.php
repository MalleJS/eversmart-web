<?php //debug( $this->input->post() ); ?>
<div class="container">
						
							<div class="row">
							<span class="back_step" id="media-mobile-m"><a id="backtocurrentenergy" href="javascript:void(0)" class="material-icons">
										<img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a>  </span>
								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">Please enter your current energy cost&hellip;</h4>
									
								</div>
								
							</div>
															
								<div class="switch_box_main">
								<form id="enter_spend_form">
									<div class="row justify-content-center">
									<span class="back_step" id="main-bak-dd"><a id="backtocurrentenergy" href="javascript:void(0)" class="material-icons" style="top:1px;">
										<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a></span>
											<div class="col-md-4" id="elec_spend">
												<div class="gas_card ">
													<div class="gas_box_icon">
														<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/elec-new.png" alt=""/></span>
													</div>
													<!---<div class="content">
														<span class="gas_box_text" style="padding:10px 0px 4px">
														Electricity
														</span>
													</div>-->
										<span class="usgage_box">
										<!---<div class="">
											<input class="" type="range" min="1" max="100" value="50" id="value_elec_range">
											</div>-->
											<div class="md-form">
												<input placeholder="£" onkeypress="return isNumberKey(event)" id="output_elec_range" class="form-control" type="text">
													<select class="custom-select usage_select" id="elec_spend_interval">
														<option value="0">Please Select Interval </option>
														<option value="year" selected>Per Year</option>
														<option value="month">Per Month</option>
														<!-- <option value="quater">Per Quater</option>
														<option value="week">Per Week</option> -->
													</select>
											</div>
										</span>
													
													
												</div>
											</div><!----------------End First Box-------------------->
											
								<div class="col-md-4" id="gas_spend">
									<div class="gas_card">
										<div class="gas_box_icon">
											<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/gas-new.png" alt=""/></span>
										</div>
										<!----<div class="content">

											<span class="gas_box_text" style="padding:10px 0px 4px">
	                                      Gas</span>
										</div>-->
										
										<span class="usgage_box">
										<!---<div class="">
											<input class="" type="range"  min="1" max="100" value="50" id="value_gas_range">
											</div>--->
											<div class="md-form">
												<input placeholder="£" onkeypress="return isNumberKey(event)" id="output_gas_range" class="form-control" type="text">
													<select class="custom-select usage_select" id="gas_spend_interval">
														<option value="0">Please Select Interval </option>
														<option value="year" selected>Per Year</option>
														<option value="month">Per Month</option>
														<!-- <option value="quater">Per Quater</option>
														<option value="week">Per Week</option> -->
													</select>
											</div>
										</span>

									</div>
								</div><!-----------------End Second Box------------------>			 
														
									</div><!-------------------End Row---------------->
									
									<br>
									<div id="error_msg"></div>
									<div class="row justify-content-center">
										<div class="col-sm-12">
										<span class="input-group-btn btnNew" id="continue_btn">
											<button style="background:#fff!important; color:#ee4392!important; padding:10px 40px !important; font-size:20px " class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
											</span>
										</div>
									
									</div>
								</form>
							</div>
							<div class="backend_error" ></div>
						</div>