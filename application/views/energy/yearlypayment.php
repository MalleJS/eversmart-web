<style type="text/css">/**
* The CSS shown here will not be introduced in the Quickstart guide, but shows
* how you can use CSS to style your Element's container.
*/
.StripeElement {background-color: white;padding:20px 12px 9px;border-bottom: 1px solid #ced4da;}
.StripeElement--focus {box-shadow: 0 1px 3px 0 #cfd7df;}

#example1-name--focus {box-shadow: 0 1px 3px 0 #cfd7df;}
.StripeElement--invalid {border-color: #fa755a;}
.StripeElement--webkit-autofill {background-color: #fefde5 !important;}
.card-element{ padding-top:20px; color:#ced4da; font-size:25px; margin-bottom:40px; font-weight:500; }
.gas_card{padding:40px 80px;}
.payment-details {display: none; position: relative; width: 100%; margin-bottom: 30px;border-radius: 10px; color: rgba(0,0,0, 0.87); background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); padding:30px 20px;}
.ElementsApp, .ElementsApp .InputElement{font-size:17px;}
.pay-detail{font-size:18px; line-height:23px; float:left; width:100%; margin:5px 0px; color:#000; padding:20px;}
.pay-price {font-size: 22px;font-weight: 500;width: 100%;float: left;color: #ea495c;line-height: 35px;padding: 0 20px 20px;}
.pay-pay-now{font-size:35px; font-weight:500; width:100%; float:left; color:#000; line-height:40px; padding:20px;}
#example1-name{margin-bottom: 10px; border-bottom: 1px solid #ced4da!important; border:none;}
.StripeElement{float:left; width:100%;}

.process-tab-cs{padding-top:8px;}
.energy-process-icon{width:90px; height:auto!important;}
#active-process{color:#000;}
.days-process{color:#1cb81c; font-size:16px; font-weight:bold; width:33%; text-align:center; float:left; margin-bottom:10px; clear: both;}
.dash-over-text-green{color:#1cb81c; font-size:16px; font-weight:bold; width:100%; text-align:center; float:left; }
.dash-over-text{padding:0px 0px 15px 0px;}
.energy-main-process{padding:50px 0px; float:left; width:100%;}
.energy-process-icon{ background:#fff; padding:15px;}
.checkmark{width: 56px;height: 56px;border-radius: 50%;display: block;stroke-width: 2;stroke: #fff;stroke-miterlimit: 10;margin: 60px auto; position: relative;}
.User-number-eversmart{font-size:45px; color:#fc5462; font-weight:400;float: left;width: 100%;text-align: center; margin: 30px 0px; }
.dont-have{width:100%; float:left; text-align:center; margin:10px 0px 30px; 0px;}
.dont-have a{color:#3c99e7; font-size:14px; font-weight:400;}
.main_card_box .gas_card{width:100%}
.fre-ques{float:left;font-size:20px; width:100%; margin:23px 0px 10px; color:#666; font-weight:400;}
.accordion{float:left; width:100%;}
.pay-form-db-tab{float:left; width:100%;}
.StripeElement {margin-bottom:10px;}
#example1-name::placeholder{opacity: 1;color: #aab7c4; font-weight:400; font-family: 'Quicksand',! sans-serif important;}
#card-element{position:relative;}
#card-type-icon1{position:absolute; right: 9px;top:22px;z-index:1;}
#card-type-icon1.fa-cc-visa{font-size:18px;}
.secure-pay-strip{padding: 170px 0px 10px!important;}
.left-tab-side ul li a{font-size:19px;}
.__PrivateStripeElement-input::placeholder{font-family: 'Quicksand',! sans-serif important;}
.InputElement::placeholder{font-family: 'Quicksand',! sans-serif important;} 
img.img-fluid.stripe {padding-bottom: 25px;}
.card.card-raised2.card-form-horizontal.wow.fadeInUp.pay {margin-bottom: 0; border: none; box-shadow: none;}
button.red-btn.btn.btn-md.btn-eversmart.btn-round.weight-300.text-center.wow.fadeInUp.switchButton.waves-effect.waves-light.pay {font-size: 1.3rem;}
.InputElement{border-top:none;border-left:none;border-right:none;}
	
.amountyearly {
    display: block;
    font-size: 60px;
    color: #ea495c;
    font-weight: 800;
    text-align: center;
    margin: 0px 0 25px;
    padding-top: 100px;
}
section.termandconditions {
    background: #ea495c url(/assets/img/balalaa.png) bottom;
    background-repeat: no-repeat;
}
.back_step .material-icons {
    position: absolute;
    left: -75px;
    top: 50px;
}
</style>


	
<div class="col-md-12" style="padding: 0;margin-top: 50px;">
	<div class="gas_card">
		<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
			<div class="content pd0">
				<div class="tabbable tabs-left row">
					<div class="col-sm-12 pd0">
						<div class="tab-content " id="tab-data-box">
							<span class="pay-pay-now text-center">Yearly Payment</span>
							<div class="amountyearly">£<?php echo $year_price ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="gas_card">
			<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
				<div class="content pd0">
					<div class="tabbable tabs-left row">
						<div class="col-sm-12 pd0">
							<div class="tab-content " id="tab-data-box">
								<div id="billing_error_msg"></div>
								<div class="tab-pane active" id="b">
									<span class="pay-pay-now text-center">Payment Details</span>
									<div id="existing_billing_address">
										<?php
										$BillingAddress = $this->user_modal->get_customer_billing_addresses( $this->session->userdata('login_data')['customer_id'] );
										if(count($BillingAddress)>0) { ?>
											<select id="billing_address_id" name="billing_address_id">
												<option value="" selected="selected"><?=$this->session->userdata('login_data')['address']; ?>, <?=$this->session->userdata('login_data')['town_address']; ?>, <?=$this->session->userdata('login_data')['address_postcode']; ?> </option>
												<?php foreach ($BillingAddress as $key => $BillingAddressData) { ?>
													<option value="<?=$BillingAddressData['billing_address_id']?>"><?=$BillingAddressData['address_line_1']?>, <?=$BillingAddressData['address_line_2']?>, <?=$BillingAddressData['postcode']?></option>
												<?php } ?>
											</select>
										<?php } ?>
									</div>
									<input type="checkbox" name="toggle_new_card_address" id="toggle_new_card_address" style="display:none" />
									<span class="pay-form-db-tab">
										<form action="" method="post" id="payment-form-yp" onsubmit="return false">
											<div class="row">
												<div class="col-lg-12  col-sm-12 col-md-12">
													<div style="position:relative">
														<div id="card-type-icon1"  style="display: none;">
															<i class="fa fa-cc-visa" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
														</div>
														<div id="card-element" class="StripeElement"></div>
													</div>
													<input type="hidden" id="amount_pay" value="<?php echo $year_price ; ?>">
													<input type="hidden" id="user_id" value="<?php echo $id ; ?>">
													<input type="hidden" id="junifer_account_id" value="<?php echo $junifer_id; ?>">
													<input type="hidden" id="existing_line1" value="<?php echo $address ; ?>">
													<input type="hidden" id="existing_postcode" value="<?php echo $postcode ; ?>">
													<input type="hidden" id="email" value="<?php echo $email ; ?>">
													<input type="hidden" id="customer_id" value="<?php echo $junifer_id; ?>">
													<input class="StripeElement ElementsApp InputElement" id="example1-name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" required="" autocomplete="name" novalidate>
													<div id="card-dd_expire"></div>
													<div id="card-dd_ccv"></div>
													<!-- Used to display form errors. -->
													<div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>
												</div>
												<div class="col-lg-12  col-sm-12 col-md-12 text-center">
													<button type="submit" id="stripebtn" style="margin-top:45px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">Submit Payment</button>
												</div>
											</div>
										</form>
									</span>
								</div>
							</div>
						</div>
						<span class="pay-detail text-center">
							Security is one of the biggest considerations in everything we do. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. 
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" id="thank_you">
		<div class="payment-details" >
			<div class="col-md-10 col-sm-12 mx-auto ">
				<div class="gas_box_icon gap_icon_dashboard">
					<span class="progress-bar-step" id="swith-process-bar">
						<svg class="checkmark" xmlns="" viewBox="0 0 52 52">
							<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
							<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
						</svg>
					</span>
					<div class="content" style="padding:0px 0px 30px">
						<div class="col-md-10 mx-auto">
							<span class="registration-page-heading-energy-H">										
								<span id="Transaction_status"></span> 
								<span id="Transaction"></span> 
							</span>
							<span class="registration-page-energy-d confirm-pay" style="padding-top:0px!important">Please check your emails</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="https://js.stripe.com/v3/"></script>