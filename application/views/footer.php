
</div>
</div>
</div>
</div>
</div>
</div>

  <?php $this->load->view('faq_slider'); ?>

</section>
	




<footer class="dash_footer">
    <div class="container">

        <div class="social-footer-dash text-center">
            <ul class="social-buttons socialButtonHome">
                <li><a class="btn btn-just-icon btn-simple btn-twitter dash" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a class="btn btn-just-icon btn-simple btn-facebook dash" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                <li><a class="btn btn-just-icon btn-simple btn-twitter dash" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

            </ul>
        </div>

        <div class="copyright text-center dash">
                &copy; 2018 Eversmart Energy Ltd - 09310427
            </div>

    </div>

</footer>


<!-- modal for select account -->
<div id="SelectAccount" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
      <div class="modal-body">
        <?php require_once 'select_account.php'; ?>
    </div>

  </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?= base_url(); ?>js/jquery-1.11.2.min.js"></script>

<script src="<?= base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>js/common.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?= base_url(); ?>js/bootstrap.js"></script>

<script type="text/javascript" src="<?= base_url; ?>/js/slideout.min.js"></script>

    <script type="text/javascript">
        $().ready(function(){
            $('[rel="tooltip"]').tooltip();

        });

        function rotateCard(btn){
            var $card = $(btn).closest('.card-container');
            console.log($card);
            if($card.hasClass('hover')){
                $card.removeClass('hover');
            } else {
                $card.addClass('hover');
            }
        }
    </script>


    <script type="text/javascript">



    $(document).ready(function(){

    $(".front.first").click(function(){

    $(".manual-flip.first").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.first").click(function(){
    $(".manual-flip.first").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $(".front.second").click(function(){

    $(".manual-flip.second").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.second").click(function(){
    $(".manual-flip.second").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $(".front.last").click(function(){

    $(".manual-flip.last").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.last").click(function(){
    $(".manual-flip.last").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $('.updateaccount').hide();

    $('.editaccount').click(function(){
         $('.editaccount').hide();
         $('.updateaccount').show();
         $('#phonenum').prop('disabled', false);
         $('#phonenum').css('background-color','#ffff66');
         $('#phonenum').focus();

     });

    $('.updateaccount').click(function(){
         $('.editaccount').show();
         $('.updateaccount').hide();
         $('#phonenum').css('background-color','#f1f1f1');
         $('#phonenum').prop('disabled', true);
     });



        // Moving Home Smart Meter

    $(document).on('keypress','#Dial_Moving_Seventh',function () {
        $("#Dial_Moving_Eight").focus();
        $("#Dial_Moving_Eight").select();
    });
    $(document).on('keypress','#Dial_Moving_Sixth',function () {
        $("#Dial_Moving_Seventh").focus();
        $("#Dial_Moving_Seventh").select();
    });
    $(document).on('keypress','#Dial_Moving_Five',function () {
        $("#Dial_Moving_Sixth").focus();
        $("#Dial_Moving_Sixth").select();
    });
    $(document).on('keypress','#Dial_Moving_Fourth',function () {
        $("#Dial_Moving_Five").focus();
        $("#Dial_Moving_Five").select();
    });
    $(document).on('keypress','#Dial_Moving_Third',function () {
        $("#Dial_Moving_Fourth").focus();
        $("#Dial_Moving_Fourth").select();
    });
    $(document).on('keypress','#Dial_Moving_Second',function () {
        $("#Dial_Moving_Third").focus();
        $("#Dial_Moving_Third").select();
    });
    $(document).on('keypress','#Dial_Moving_First',function () {
        $("#Dial_Moving_Second").focus();
        $("#Dial_Moving_Second").select();
    });

    $(document).on('keypress','#Dial_Gas_Moving_Seventh',function () {
        $("#Dial_Gas_Moving_Eight").focus();
        $("#Dial_Gas_Moving_Eight").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Sixth',function () {
        $("#Dial_Gas_Moving_Seventh").focus();
        $("#Dial_Gas_Moving_Seventh").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Five',function () {
        $("#Dial_Gas_Moving_Sixth").focus();
        $("#Dial_Gas_Moving_Sixth").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Fourth',function () {
        $("#Dial_Gas_Moving_Five").focus();
        $("#Dial_Gas_Moving_Five").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Third',function () {
        $("#Dial_Gas_Moving_Fourth").focus();
        $("#Dial_Gas_Moving_Fourth").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Second',function () {
        $("#Dial_Gas_Moving_Third").focus();
        $("#Dial_Gas_Moving_Third").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_First',function () {
        $("#Dial_Gas_Moving_Second").focus();
        $("#Dial_Gas_Moving_Second").select();
    });


    function logout_user() {

        $.ajax({
            url: '<?= base_url; ?>/index.php/user/logout',
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.error == '0') {
                    window.location.href = '<?= base_url; ?>/index.php/user/login';
                }
            }
        });

    }
            // active to link
            var current = location.href;
            //alert(current);
            //var dashboard = current.match(/overview/g);
            if( current.match(/billing/g) == 'billing' )
            {
                $('.billing').attr('class','billing actiove-link');
            }
            if( current.match(/meter_reading/g) == 'meter_reading' )
            {
                $('.meter_reading').attr('class','meter_reading actiove-link');
            }
            if( current.match(/payments/g) == 'payments' )
            {
                $('.payments').attr('class','payments actiove-link');
            }
            if( current.match(/pay_dashboard/g) == 'pay_dashboard' )
            {
                $('.payments').attr('class','payments actiove-link');
            }
            if( current.match(/usage/g) == 'usage' )
            {
                $('.usage').attr('class','usage actiove-link');
            }
            if( current.match(/moving_home/g) == 'moving_home' )
            {
                $('.moving_home').attr('class','moving_home actiove-link');
            }
            if( current.match(/referral/g) == 'referral' )
            {
                $('.referral').attr('class','referral actiove-link');
            }
            if( current.match(/account/g) == 'account' )
            {
                $('.accounts').attr('class','accounts actiove-link');
            }
            if( current.match(/messages/g) == 'messages' )
            {
                $('.communications').attr('class','communications actiove-link');
            }

            // submit meter reading
            $(document).on('submit', '#submit_reading_meter', function(e){

                e.preventDefault();
                $.ajax({
                    url: '<?= base_url; ?>/index.php/user/submit_reading',
                    type: 'post',
                    dataType: 'json',
                    data: $('#submit_reading_meter').serialize(),
                    success:function(response){
                      
                        if( response.error == '0' ){

                            $('#error_msg-elec').html('<div class="alert alert-success" role="alert">Reading Submitted!</div>');

                            setTimeout(function(){ $('#error_msg_elec').empty() }, 1000);

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10){ dd='0'+dd; }
                            if(mm<10){ mm='0'+mm; }
                            var today = dd+'/'+mm+'/'+yyyy;

                            if( $('#gas_submit_reds') && $('#gas_submit_reds').val() ) {

                                $('#elec_button').css('visibility','hidden');
                                $('#elec_field_wrapper').css('background-color','#CCC');
                                $('#select_elec').removeClass('white-blue');
                                $('#select_elec').addClass('white-border');
                                $("#elec_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/elec-grey.svg");

                            }
                            else {
                                $('#gas_card_selected').trigger("click");
                            }
                            $('#select_elec').css('pointer-events','none');
                            $('#select_elec').addClass('form_opacity');
                            $('#first_elec_meter_reading').replaceWith('First Reading: '+$('#elec_submit_reds').val() + ' - ' + today );

                        }
                        else {

                            var errorss = response.errorDescription ;
                            $('#error_msg_elec').html('<div class="alert alert-danger" role="alert">Error! while submitting reading '+errorss+' </div>');
                            setTimeout(function(){ $('#error_msg_elec').empty() }, 10000);
                        }
                    }
                });
            });

            $(document).on('submit', '#submit_reading_meter_gas', function(e){
                e.preventDefault();
                $.ajax({
                    url: '<?= base_url; ?>/index.php/user/submit_reading_gas',
                    type: 'post',
                    dataType: 'json',
                    data: $('#submit_reading_meter_gas').serialize(),
                    success:function(response){
                        //console.log(response);
                        if( response.error == '0' ){
                            $('#error_msg_gas').html('<div class="alert alert-success" role="alert">Reading Submitted!</div>');

                            setTimeout(function(){ $('#error_msg_gas').empty() }, 1000);

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10){ dd='0'+dd; }
                            if(mm<10){ mm='0'+mm; }
                            var today = dd+'/'+mm+'/'+yyyy;


                            if( $('#elec_submit_reds') && $('#elec_submit_reds').val()) {

                                $('#gas_button').css('visibility','hidden');
                                $('#gas_field_wrapper').css('background-color','#CCC');
                                $('#gas_card_selected').removeClass('white-gold');
                                $('#gas_card_selected').addClass('white-border');
                                $("#gas_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/gas.svg");

                            }
                            else {
                                $('#select_elec').trigger("click");
                            }
                            $('#gas_card_selected').css('pointer-events','none');
                            $('#gas_card_selected').addClass('form_opacity');
                            $('#first_gas_meter_reading').replaceWith('First Reading: '+$('#gas_submit_reds').val() + ' - ' + today );

                        }
                        else{

                            var errorss = response.errorDescription ;
                            $('#error_msg_gas').html('<div class="alert alert-danger" role="alert">Error! while submitting reading '+errorss+' </div>');
                            setTimeout(function(){ $('#error_msg_gas').empty() }, 10000);
                        }
                    }
                });
            });

            $(document).on('click', '#step_1', function(){
                    $.ajax({
                    url: '<?= base_url; ?>/index.php/user/move_home_steps',
                    type: 'get',
                    beforeSend:function(){
                    $('.loading-page').fadeIn();
                    $('#main_content').fadeOut();
                    },
                    complete:function(){
                    $('.loading-page').fadeOut();
                    $('#main_content').fadeIn();
                    },
                    success:function(response){

          // var params = { width:1680, height:1050 };
          // var str = jQuery.param( params );
          // var pathname = window.location.pathname.str;
          // console.log(pathname);
                    var date = new Date();
                    var html_append = $('#response_dashboard').html(response);
                        datepicker_function(html_append);
                    }
                    });
                    });

        $(document).on('click', '#save_meter_reading',function(){
            $('#move_step_3').trigger('submit');
        })

            $(document).on('submit', '#move_step_3', function(e){
      e.preventDefault();
      // alert();
      // return false;
                    $.ajax({
                    url: '<?= base_url; ?>/index.php/user/move_home_3',
                    type: 'get',
                    beforeSend:function(){
                    $('.loading-page').fadeIn();
                    $('#main_content').fadeOut();
                    },
                    complete:function(){
                    $('.loading-page').fadeOut();
                    $('#main_content').fadeIn();
                    },
                    success:function(response){
                    $('#response_dashboard').html(response);
                    }
                    });
                    });

        });

    function datepicker_function(response)
    {
    var dateage = new Date();
    dateage.setDate( dateage.getDate() - 0 );
    //dateage.setFullYear( dateage.getFullYear() - 16 );
    //alert(new Date(dateage));

     response.find('#datepicker').datepicker({
      format: "dd-mm-yyyy",
      //endDate: new Date(dateage),
      minDate:new Date(),
      autoclose: true,
      uiLibrary: 'bootstrap4'
    });
    }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
          $('.showhidetabs .nav-tabs > li > a').click(function(event) {
            event.preventDefault(); //stop browser to take action for clicked anchor
            //get displaying tab content jQuery selector
            var active_tab_selector = $('.showhidetabs .nav-tabs > li.active > a').attr('href');
            //find actived navigation and remove 'active' css
            var actived_nav = $('.showhidetabs .nav-tabs > li.active');
            actived_nav.removeClass('active');
            //add 'active' css into clicked navigation
            $(this).parents('li').addClass('active');
            //hide displaying tab content
            $(active_tab_selector).removeClass('active');
            $(active_tab_selector).addClass('hide');
            //show target tab content
            var target_tab_selector = $(this).attr('href');
            $(target_tab_selector).removeClass('hide');
            $(target_tab_selector).addClass('active');
          });

        $(".weeklyu").hide();
        $(".monthlyu").show();
        $(".yearlyu").hide();

        $("#weeklyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").show();
        $(".monthlyu").hide();
        $(".yearlyu").hide();
        });
        $("#monthlyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").hide();
        $(".monthlyu").show();
        $(".yearlyu").hide();
        });
        $("#yearlyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").hide();
        $(".monthlyu").hide();
        $(".yearlyu").show();
        });

        $(".weekly-gas").hide();
        $(".weekly-electric").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").addClass("clicked");

        $(".monthly-gas").hide();
        $(".monthly-electric").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").addClass("clicked");

        $(".yearly-gas").hide();
        $(".yearly-electric").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").addClass("clicked");

        $("a#weeklyuelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weekly-gas").hide();
        $(".weekly-electric").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#weeklyugas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weekly-electric").hide();
        $(".weekly-gas").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").removeClass("clicked");
        });

        $("a#monthlyelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".monthly-gas").hide();
        $(".monthly-electric").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#monthlygas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".monthly-electric").hide();
        $(".monthly-gas").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").removeClass("clicked");
        });

         $("a#yearlyelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".yearly-gas").hide();
        $(".yearly-electric").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#yearlygas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".yearly-electric").hide();
        $(".yearly-gas").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").removeClass("clicked");
        });
    });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            if ($(window).width() >= 1730)
            {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            }

            var current = location.href;
            if (current.match(/\/dashboard\//g) == 'dashboard') {
                $('.dashboard').attr('class', 'dashboard actiove-link');
            }

            this.$slideOut = $('#slideOut');

            // Slideout show
            this.$slideOut.find('.slideOutTab').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });

            this.$slideOut.find('#closy').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });
			$('.faq-contact-icons').hide();
			$('.faq-contact-help').click(function() {
		$('.faq-contact-help').hide();
		$('.faq-contact-icons').addClass("slide-icons");
		$('.faq-contact-icons').show();
		
	})
        });
    </script>




</body>
</html>
