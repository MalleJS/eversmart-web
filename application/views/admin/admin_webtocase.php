<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Web to Case</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</a>
    <?php } ?>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase" selected>Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
        <?php } ?>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>

<div style="display: inline-flex">
    <div class="JD_switch">
        <input type="checkbox" name="JD_switch" class="JD_switch-checkbox" id="JD_switch" checked>
        <label class="JD_switch-label" for="JD_switch">
            <span class="JD_switch-inner"></span>
            <span class="JD_switch-switch"></span>
        </label>
    </div>

    <div class="DE_switch">
        <input type="checkbox" name="DE_switch" class="DE_switch-checkbox" id="DE_switch" checked>
        <label class="DE_switch-label" for="DE_switch">
            <span class="DE_switch-inner"></span>
            <span class="DE_switch-switch"></span>
        </label>
    </div>
</div>

<div class="container col-lg-12 webtocase">

    <form id="webtocase">

        <section class="col-lg-12">

            <h4>Web to Case</h4>

            <div class="col-lg-3 wtc_input">
                <label for="org_id">Org ID:</label>
                <select id="org_id" name="org_id" data-webtocase-name="org_id">
                    <option value="00D0Y000000YWqc">00D0Y000000YWqc</option>
                    <!--<option value="00D1l0000008d2T">00D1l0000008d2T (UAT)</option>-->
                </select>
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9PS">Full Name: </label>
                <input id="00N1n00000SB9PS" name="00N1n00000SB9PS" type="text" value="" data-webtocase-name="name"
                       onkeypress="return !isNumberKey(event)" maxlength="20" style="text-transform: capitalize">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="email">Email: </label>
                <input id="wtc_email" name="email" type="text" value="" data-webtocase-name="email">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="phone">Phone: </label>
                <input id="phone" name="phone" type="number" value="" data-webtocase-name="phone"
                       onkeypress="return isNumberKey(event)" maxlength="14">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="dob">Date of Birth: </label>
                <input id="dob" name="dob" type="date" value="">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9PI">Address Line 1: </label>
                <input id="00N1n00000SB9PI" name="00N1n00000SB9PI" type="text" value="" data-webtocase-name="address"
                       style="text-transform: capitalize">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9Ph">Postcode: </label>
                <input id="00N1n00000SB9Ph" name="00N1n00000SB9Ph" type="text" value="" data-webtocase-name="postcode"
                       maxlength="10" style="text-transform: uppercase">
            </div>

            <div id="junifer_id" class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9OK">Junifer Customer ID: </label>
                <input id="00N1n00000SB9OK" name="00N1n00000SB9OK" type="text" value=""
                       data-webtocase-name="junifer_customer_id" onkeypress="return isNumberKey(event)">
            </div>

            <div id="dyball_id" class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9NR">Dyball Account ID: </label>
                <input id="00N1n00000SB9NR" name="00N1n00000SB9NR" type="text" value="" name="00N1n00000SB9NR"
                       onkeypress="return isNumberKey(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SB9Pc">Registration Status: </label>
                <select id="00N1n00000SB9Pc" name="00N1n00000SB9Pc"
                        data-webtocase-name="external_system_registration_status">
                    <option value="default">(Please Select)</option>
                    <option value="Registration Success">Registration Success</option>
                    <option value="Registration Pending">Registration Pending</option>
                </select>
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="subject">Subject: </label>
                <input id="subject" name="subject" type="text" value="" data-webtocase-name="subject">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="description">Description: </label>
                <input id="description" name="description" type="text" value="" data-webtocase-name="description">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Sb2kW">Billing Frequency: </label>
                <select id="00N1n00000Sb2kW" name="00N1n00000Sb2kW" data-webtocase-name="billing_frequency">
                    <option value="directdebit">Direct Debit</option>
                    <option value="prepay">Pre-pay</option>
                </select>
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Sb2k2">Tariff Name: </label>
                <select id="00N1n00000Sb2k2" name="00N1n00000Sb2k2" data-webtocase-name="tariff_name">
                    <option value="Simply Smart">Simply Smart</option>
                    <option value="Family Saver Club">Family Saver Club</option>
                    <option value="Safeguard PAYG">Safeguard PAYG</option>
                </select>
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="recordType">Record Type: </label>
                <select id="recordType" name="recordType" data-webtocase-name="record_type">
                    <option value="0121n0000007GkH">New Junifer Account</option>
                    <option value="0121n0000007GkE">Dyball</option>
                    <option value="0120Y000000FfqQ">Customer Issue</option>
                    <option value="0121n0000007GkF">ECO Case</option>
                    <option value="0121n0000007GkG">Internal Case</option>
                    <option value="0120Y000000FbCo">Internal IT Support</option>
                    <option value="0121n0000007GkI">Privacy Type</option>
                    <option value="0121n0000007GkJ">Utility Case</option>
                </select>
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SB9PN" name="00N1n00000SB9PN" type="hidden" value="" data-webtocase-name="dob">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SB9ST" name="00N1n00000SB9ST" type="hidden" value="<?php echo date("d-m-Y"); ?>"
                       data-webtocase-name="case_send_date">
            </div>

        </section>

        <section class="col-lg-12 wtc_elec">

            <h4>Electricity</h4>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Saz7d">MPAN: </label>
                <input id="00N1n00000Saz7d" name="00N1n00000Saz7d" type="text" value=""
                       data-webtocase-name="meter_supply_number_elec" maxlength="13"
                       onkeypress="return isNumberKey(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Saz7b">Meter Serial Number: </label>
                <input id="00N1n00000Saz7b" name="00N1n00000Saz7b" type="text" value=""
                       data-webtocase-name="meter_serial_number_elec" style="text-transform: uppercase">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNn">EAC: </label>
                <input id="00N1n00000SajNn" name="00N1n00000SajNn" type="text" value=""
                       data-webtocase-name="assumed_annual_consumption_elec" onkeypress="return isNumberKey(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNr">Unit Rate (p/kWh): </label>
                <input id="00N1n00000SajNr" name="00N1n00000SajNr" type="text" value=""
                       data-webtocase-name="elec_unit_rate" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNp">Standing Charge (p/day): </label>
                <input id="00N1n00000SajNp" name="00N1n00000SajNp" type="text" value=""
                       data-webtocase-name="daily_standing_charge_elec" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNt">Estimated Annual Cost: </label>
                <input id="00N1n00000SajNt" name="00N1n00000SajNt" type="text" value=""
                       data-webtocase-name="estimated_annual_cost_elec" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="supp_start_elec">Supply Start Date: </label>
                <input id="supp_start_elec" name="supp_start_elec" type="date" value="">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SajNv" name="00N1n00000SajNv" type="hidden" value=""
                       data-webtocase-name="expected_supply_start_date_elec">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="supp_end_elec">Supply End Date: </label>
                <input id="supp_end_elec" name="supp_end_elec" type="date" value="">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SakSF" name="00N1n00000SakSF" type="hidden" value=""
                       data-webtocase-name="supply_end_date_elec">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000Saz7f" name="00N1n00000Saz7f" type="hidden" value=""
                       data-webtocase-name="product_name_elec">
            </div>

        </section>

        <section class="col-lg-12" id="dual_switch">

            <h4>Gas</h4>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Saz7e">MPRN: </label>
                <input id="00N1n00000Saz7e" name="00N1n00000Saz7e" type="text" value=""
                       data-webtocase-name="meter_supply_number_gas" maxlength="11"
                       onkeypress="return isNumberKey(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000Saz7c">Meter Serial Number: </label>
                <input id="00N1n00000Saz7c" name="00N1n00000Saz7c" type="text" value=""
                       data-webtocase-name="meter_serial_number_gas" style="text-transform: uppercase">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNo">AQ: </label>
                <input id="00N1n00000SajNo" name="00N1n00000SajNo" type="text" value=""
                       data-webtocase-name="assumed_annual_consumption_gas" onkeypress="return isNumberKey(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNs">Unit Rate (p/kwh): </label>
                <input id="00N1n00000SajNs" name="00N1n00000SajNs" type="text" value=""
                       data-webtocase-name="gas_unit_rate" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNq">Standing Charge (p/day): </label>
                <input id="00N1n00000SajNq" name="00N1n00000SajNq" type="text" value=""
                       data-webtocase-name="daily_standing_charge_gas" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="00N1n00000SajNu">Estimated Annual Cost: </label>
                <input id="00N1n00000SajNu" name="00N1n00000SajNu" type="text" value=""
                       data-webtocase-name="estimated_annual_cost_gas" onkeypress="return numberDecimals(event)">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="supp_start_gas">Supply Start Date:</label>
                <input id="supp_start_gas" name="supp_start_gas" type="date" value="">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SajNw" name="00N1n00000SajNw" type="hidden" value=""
                       data-webtocase-name="expected_supply_start_date_gas">
            </div>

            <div class="col-lg-3 wtc_input">
                <label for="supp_end_gas">Supply End Date: </label>
                <input id="supp_end_gas" name="supp_end_gas" type="date" value="">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000SakSK" name="00N1n00000SakSK" type="hidden" value=""
                       data-webtocase-name="supply_end_date_gas">
            </div>

            <div class="col-lg-3 wtc_input">
                <input id="00N1n00000Saz7g" name="00N1n00000Saz7g" type="hidden" value=""
                       data-webtocase-name="product_name_gas">
            </div>

        </section>

        <section class="col-lg-12 wtc_submit">
            <div class="col-lg-6">
                <button id="submit-form" name="submit-form" class="btn btn-primary">SUBMIT FORM</button>
            </div>
            <div class="col-lg-6">
                <button id="clear-form" name="clear-form" class="btn btn-primary">CLEAR FORM</button>
            </div>
        </section>

    </form>
</div>

<div id="wtc_outcome" style="float:left; width:100%"></div>


<?php $this->load->view('dashboard/webtocase_form'); ?>

<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_webtocase.js"></script>
</body>
