	<select name="address_full" id="quotation_address" style="font-size:1rem;background-color:white" class="custom-select  form-control paddingtb">
    <option value="0">(Please Select Address)</option>
    <?php

        if( !empty($address_list) ){

            for( $j=0; $j<count($address_list); $j++ ){
                // mpan

                if( !empty($address_list[$j]['meterType']) )
                {
                    $meterType = $address_list[$j]['meterType'];
                }
                else
                {
                    $meterType = '';
                }

                if( !empty($address_list[$j]['mpan']) ){
                    for($m=0; $m<count($address_list[$j]['mpan']); $m++){
                        if( count($address_list[$j]['mpan']) >= 1 ){
                            $mpan = implode(',',$address_list[$j]['mpan']);

                        }
                        else {
                            $mpan = $address_list[$j]['mpan'][$j];

                        }
                    }
                }else{
                    $mpan = '';
                }

                //gas meter serial number
                if( !empty($address_list[$j]['meterSerialGas']) ){
                    for($m=0; $m<count($address_list[$j]['meterSerialGas']); $m++){
                        if( count($address_list[$j]['meterSerialGas']) >= 1 ){
                            $meterSerialGas = implode(',',$address_list[$j]['meterSerialGas']);

                        }
                        else {
                            $meterSerialGas = $address_list[$j]['meterSerialGas'][$j];

                        }
                    }
                }else{
                    $meterSerialGas = '';
                }

                // elec meter serial number
                if( !empty($address_list[$j]['meterSerialElec']) ){
                    for($m=0; $m<count($address_list[$j]['meterSerialElec']); $m++){
                        if( count($address_list[$j]['meterSerialElec']) >= 1 ){
                            $meterSerialElec = implode(',',$address_list[$j]['meterSerialElec']);

                        }
                        else {
                            $meterSerialElec = $address_list[$j]['meterSerialElec'][$j];

                        }
                    }
                }else{
                    $meterSerialElec = '';
                }

                if( !empty($address_list[$j]['mpanLower']) ){
                    for($m=0; $m<count($address_list[$j]['mpanLower']); $m++){
                        if( count($address_list[$j]['mpanLower']) >= 1 ){
                            $mpanLower = implode(',',$address_list[$j]['mpanLower']);

                        }
                        else {
                            $mpanLower = $address_list[$j]['mpanLower'][$j];

                        }
                    }
                }else{
                    $mpanLower = '';
                }

                // mprn
                if( !empty($address_list[$j]['mprn']) ){
                    for($m=0; $m<count($address_list[$j]['mprn']); $m++){
                        if( count($address_list[$j]['mprn']) >= 1 ){
                            $mprn = implode(',',$address_list[$j]['mprn']);
                        }
                        else {
                            $mprn = $address_list[$j]['mprn'][$j];
                        }
                    }
                }else{
                    $mprn = '';
                }

                if( !empty($address_list[$j]['metersElec']) ){
                    for($m=0; $m<count($address_list[$j]['metersElec']); $m++){
                        if( count($address_list[$j]['metersElec']) >= 1 ) {
                            $mpanMeterType = $address_list[$j]['metersElec'][0]['mpanMeterType'];
                        }
                    }
                } else {
                    $mpanMeterType = '';
                }

                if( !empty($address_list[$j]['metersElec']) ){
                    for($m=0; $m<count($address_list[$j]['metersElec']); $m++){
                        if( count($address_list[$j]['metersElec']) >= 1 ) {
                            $mpanVerbose = $address_list[$j]['metersElec'][0]['mpanMeterTypeVerbose'];
                        }
                    }
                } else {
                    $mpanVerbose = '';
                }

                $stripped = preg_replace('/\s+/', ' ', $address_list[$j]['address']);
                ?>
                    <option data-mpan="<?php echo $mpan ?>"
                    data-gasmeter="<?php echo $meterSerialGas ?>"
                    data-elecmeter="<?php echo $meterSerialElec ?>"
                    data-mpanlower="<?php echo $mpanLower ?>"
                    data-metertype="<?php echo $meterType; ?>"
                    data-mpanmetertype="<?php echo $mpanMeterType; ?>"
                    data-mpanmeterverb="<?php echo $mpanVerbose; ?>"
                    data-mprn="<?php echo $mprn ?>"
                    data-subb="<?php echo $address_list[$j]['subb'] ?>"
                    data-bnam="<?php echo $address_list[$j]['bnam'] ?>"
                    data-bnum="<?php echo $address_list[$j]['bnum'] ?>"
                    data-thor="<?php echo $address_list[$j]['thor'] ?>"
                    data-dplo="<?php echo $address_list[$j]['dplo'] ?>"
                    data-town="<?php echo $address_list[$j]['town'] ?>"
                    data-cnty="<?php echo $address_list[$j]['cnty'] ?>"
                    data-selectedpostcode="<?php echo $address_list[$j]['pcod']; ?>"
                    value="<?php echo $stripped ?>">
                    <?php echo $stripped; ?> </option>
            <?php
            }
        }
    ?>

</select>

<div class="gasdropdownlist" style="display: none">
    <select name="quotation_address_gas" id="quotation_address_gas" style="font-size:1rem;background-color:white" class="custom-select  form-control paddingtb">
        <option value="0">(Select Gas Address)</option>
        <?php

        if( !empty($address_list_gas) ){
            for( $j=0; $j<count($address_list_gas); $j++ ){
                // mpan

                if( !empty($address_list_gas[$j]['meterType']) )
                {
                    $meterType_gas = $address_list_gas[$j]['meterType'];
                }
                else
                {
                    $meterType_gas = '';
                }


                //gas meter serial number
                if( !empty($address_list_gas[$j]['meterSerialGas']) ){
                    for($m=0; $m<count($address_list_gas[$j]['meterSerialGas']); $m++){
                        if( count($address_list_gas[$j]['meterSerialGas']) >= 1 ){
                            $meterSerialGas_gas = implode(',',$address_list_gas[$j]['meterSerialGas']);

                        }
                        else {
                            $meterSerialGas_gas = $address_list_gas[$j]['meterSerialGas'][$j];

                        }
                    }
                }else{
                    $meterSerialGas_gas = '';
                }

                // mprn

                // mprn
                if( !empty($address_list_gas[$j]['mprn']) ){
                    for($m=0; $m<count($address_list_gas[$j]['mprn']); $m++){
                        if( count($address_list_gas[$j]['mprn']) > 1 ){
                            $mprn_gas = implode(',',$address_list_gas[$j]['mprn']);
                            $mprn_gas = $address_list_gas[$j]['mprn'][0];
                            $secondmprn_gas = $address_list_gas[$j]['mprn'][1];

                            //prinht_r($address_list);
                        }
                        else {
                            $mprn_gas = $address_list_gas[$j]['mprn'][0];
                            $secondmprn_gas = "";
                        }
                    }
                }else{
                    $mprn_gas = '';
                    $secondmprn_gas = "";
                }

                $stripped_gas = preg_replace('/\s+/', ' ', $address_list_gas[$j]['address']);
                ?>
                <option data-gasmeter_gas="<?php echo $meterSerialGas_gas ?>"
                        data-gasmprn_gas="<?php echo $mprn_gas ?>"
                        data-gasmprn_second="<?php echo $secondmprn_gas ?>"
                        data-subb_gas="<?php echo $address_list_gas[$j]['subb'] ?>"
                        data-bnam_gas="<?php echo $address_list_gas[$j]['bnam'] ?>"
                        data-bnum_gas="<?php echo $address_list_gas[$j]['bnum'] ?>"
                        data-thor_gas="<?php echo $address_list_gas[$j]['thor'] ?>"
                        data-dplo_gas="<?php echo $address_list_gas[$j]['dplo'] ?>"
                        data-town_gas="<?php echo $address_list_gas[$j]['town'] ?>"
                        data-cnty_gas="<?php echo $address_list_gas[$j]['cnty'] ?>"
                        data-selectedpostcode_gas="<?php echo $address_list_gas[$j]['pcod']; ?>"
                        value="<?php echo $stripped_gas ?>">
                        <?php echo $stripped_gas; ?> </option>
            <?php	}
        }
        ?>
        <option value="noaddress">My address is not listed</option>
    </select>
</div>

<div class="missingmeters" style="display: none;">
    <input class="form-control input-md" type="text" value="" id="checkinput" name="checkinput" placeholder="Enter meter serial number" maxlength="13">
    <button id="check_msn_btn" class="btn btn-primary" type="submit">Check</button>
    <div id="msn_msg"></div>
</div>

<div class="multiplemeters" style="display: none;">
    <input class="form-control input-md" type="text" value="" id="checkinput2" name="checkinput2" placeholder="Enter meter serial number" maxlength="13">
    <button id="check_msn_btn2" class="btn btn-primary" type="submit">Check</button>
    <div id="msn2_msg"></div>
</div>
