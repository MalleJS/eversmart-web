	<select name="address_full" id="quotation_address" style="font-size:1rem;background-color:white" class="custom-select  form-control paddingtb">
    <option value="0">(Please Select Address)</option>
    <?php

            if( !empty($address_list) ){

                for( $j=0; $j<count($address_list); $j++ ){
                    // mpan

                    if( !empty($address_list[$j]['meterType']) )
                    {
                        $meterType = $address_list[$j]['meterType'];
                    }
                    else
                    {
                        $meterType = '';
                    } 

                    if( !empty($address_list[$j]['mpan']) ){
                        for($m=0; $m<count($address_list[$j]['mpan']); $m++){
                            if( count($address_list[$j]['mpan']) >= 1 ){
                                $mpan = implode(',',$address_list[$j]['mpan']);

                            }
                            else {
                                $mpan = $address_list[$j]['mpan'][$j];

                            }
                        }
                    }else{
                        $mpan = '';
                    }

                    //gas meter serial number
                    if( !empty($address_list[$j]['meterSerialGas']) ){
                        for($m=0; $m<count($address_list[$j]['meterSerialGas']); $m++){
                            if( count($address_list[$j]['meterSerialGas']) >= 1 ){
                                $meterSerialGas = implode(',',$address_list[$j]['meterSerialGas']);

                            }
                            else {
                                $meterSerialGas = $address_list[$j]['meterSerialGas'][$j];

                            }
                        }
                    }else{
                        $meterSerialGas = '';
                    }

                    // elec meter serial number
                    if( !empty($address_list[$j]['meterSerialElec']) ){
                        for($m=0; $m<count($address_list[$j]['meterSerialElec']); $m++){
                            if( count($address_list[$j]['meterSerialElec']) >= 1 ){
                                $meterSerialElec = implode(',',$address_list[$j]['meterSerialElec']);

                            }
                            else {
                                $meterSerialElec = $address_list[$j]['meterSerialElec'][$j];

                            }
                        }
                    }else{
                        $meterSerialElec = '';
                    }

                    if( !empty($address_list[$j]['mpanLower']) ){
                        for($m=0; $m<count($address_list[$j]['mpanLower']); $m++){
                            if( count($address_list[$j]['mpanLower']) >= 1 ){
                                $mpanLower = implode(',',$address_list[$j]['mpanLower']);

                            }
                            else {
                                $mpanLower = $address_list[$j]['mpanLower'][$j];

                            }
                        }
                    }else{
                        $mpanLower = '';
                    }

                    // mprn
                    if( !empty($address_list[$j]['mprn']) ){
                        for($m=0; $m<count($address_list[$j]['mprn']); $m++){
                            if( count($address_list[$j]['mprn']) >= 1 ){
                                $mprn = implode(',',$address_list[$j]['mprn']);
                            }
                            else {
                                $mprn = $address_list[$j]['mprn'][$j];
                            }
                        }
                    }else{
                        $mprn = '';
                    }
                    $stripped = preg_replace('/\s+/', ' ', $address_list[$j]['address']);
                    ?>
                        <option data-mpan="<?php echo $mpan ?>" 
                        data-gasmeter="<?php echo $meterSerialGas ?>" 
                        data-elecmeter="<?php echo $meterSerialElec ?>" 
                        data-mpanlower="<?php echo $mpanLower ?>" 
                        data-metertype="<?php echo $meterType; ?>" 
                        data-mprn="<?php echo $mprn ?>" 
                        data-subb="<?php echo $address_list[$j]['subb'] ?>"
                        data-bnam="<?php echo $address_list[$j]['bnam'] ?>"
                        data-bnum="<?php echo $address_list[$j]['bnum'] ?>"
                        data-thor="<?php echo $address_list[$j]['thor'] ?>"
                        data-dplo="<?php echo $address_list[$j]['dplo'] ?>"
                        data-town="<?php echo $address_list[$j]['town'] ?>"  
                        data-cnty="<?php echo $address_list[$j]['cnty'] ?>"
                        data-selectedpostcode="<?php echo $address_list[$j]['pcod']; ?>"
                        value="<?php echo $stripped ?>">
                        <?php echo $stripped; ?> </option>
            <?php	}
            }
    ?>

</select>
