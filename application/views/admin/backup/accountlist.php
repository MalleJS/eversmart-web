<?php

if( !empty($account_list) )
{

    ?>

    <select name="account_outcome" id="account_outcome" style="font-size:1rem;" class="custom-select form-control">
        <option value="default">(Please Select Account)</option>
        <?php
        foreach($account_list as $key => $AccountData )
        {
            ?>
            <option data-id="<?php echo $AccountData['id']?>"
                    data-forename="<?php echo $AccountData['forename']?>"
                    value="<?php echo $AccountData['email']?>">
                <?php echo $AccountData['account']; ?>
            </option>
            <?php
        }

        ?>

    </select>
<?php }
else
{
    ?>
    <p>No account found.</p>
    <?php
}
?>
