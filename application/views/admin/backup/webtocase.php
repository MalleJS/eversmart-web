
<?php
// Get the sign up type

// Set up some vars depending on what account type we are
if ( $signup_type == 0 ) {  // 0 = Junifer

}
else if ( $signup_type == 3 ) {  // 3 = Dyball {

}

?>


<style>

    .button_wrapper {
        clear: both;
        margin-top: 50px;
    }

    .button_wrapper a {
        background-color: #ea495c;
        color: white;
        border-radius: 30px;
        margin-right: 20px;
        padding: 8px 20px;
    }

    .button_wrapper a:hover, .button_wrapper a.active {
        background-color: navy;
    }

    .web_to_case_testing_wrapper{
        margin: 50px 30px 0 0;
        float: left;
    }

    label {
        float: left;
        clear: both;
    }

    input, select {
        overflow: visible;
        float: left;
        clear: both;
        margin-bottom: 20px;
        width: 300px;
        height: 30px;
    }

    input {
        padding: 15px 10px;
    }

</style>

<div class="button_wrapper">

    <p>Use the toggle below to select a junifer / dyball customer, then press the send button to test the web to case form</p>
    <p style="margin-bottom: 50px"><strong>Note: We keep a log of all sent web to cases.</strong></p>

    <a href="#" id="junifer_customer" <?=($signup_type==0?'class="active"':'');?>>Junifer Customer</a>
    <a href="#" id="dyball_customer" <?=($signup_type==3?'class="active"':'');?>>Dyball Customer</a>
</div>

<form action="" id="webtocaseform">

    <div class="web_to_case_testing_wrapper">

        <label>orgid:</label>
        <input type="text" name="orgid" id="orgid" value="00D1l0000008d2T">
        <!-- Customer Name -->
        <label>customer name:</label>
        <input id="00N1n00000SB9PS" maxlength="255" name="00N1n00000SB9PS" size="20" type="text" value="<?=$customer['forename'];?> <?=$customer['surname'];?>" />
        <!-- Email -->
        <label>email:</label>
        <input  id="wtc_email" maxlength="80" name="wtc_email" size="20" type="text" value="<?=$customer['email'];?>" />
        <!-- Phone -->
        <label>phone:</label>
        <input id="phone" maxlength="40" name="phone" size="20" type="text" value="<?=$customer['phone1'];?>" />
        <!-- Customer Date of Birth -->
        <label>date of birth:</label>
        <input  id="00N1n00000SB9PN" maxlength="50" name="00N1n00000SB9PN" size="20" type="text" value="<?=$customer['dateOfBirth'];?>" /><br>
        <!-- Address Line 1 -->
        <label>address line 1:</label>
        <input  id="00N1n00000SB9PI" maxlength="255" name="00N1n00000SB9PI" size="20" type="text" value="<?=$customer['first_line_address'];?>" />

    </div>


    <div class="web_to_case_testing_wrapper">

        <!-- Postcode -->
        <label>postcode:</label>
        <input id="00N1n00000SB9Ph" maxlength="255" name="00N1n00000SB9Ph" size="20" type="text" value="<?=$customer['address_postcode'];?>" />
        <!-- Subject -->
        <label>subject:</label>
        <input id="subject" maxlength="80" name="subject" size="20" type="text" value="<?=$subject;?>" />
        <!-- Description -->
        <label>description:</label>
        <input id="description" maxlength="80" name="description" size="20" type="text" value="<?=$description;?>" />
        <!-- Junifer Customer ID -->
        <label>junifer id:</label>
        <input  id="00N1l000000Ntsj" maxlength="50" name="00N1l000000Ntsj" size="20" type="text" value="<?=$junifer_customer_id;?>" />
        <!-- Dyball Account Number -->
        <label>dyball id:</label>
        <input  id="00N1n00000SB9NR" maxlength="50" name="00N1n00000SB9NR" size="20" type="text" value="<?=$dyball_account_id;?>" />
        <!-- External System Registration Status -->
        <label>External System Registration Status:</label>
        <select id="00N1l000000O97T" name="00N1l000000O97T" title="External System Registration Status">
            <option value="">--None--</option>
            <option <?=(!isset($status_type)||$status_type=='Meter Booking'?'selected="selected"':'');?> value="Meter Booking">Meter Booking</option>
            <option <?=(isset($status_type)&&$status_type=='Registration Pending'?'selected="selected"':'');?>value="Registration Pending">Registration Pending</option>
            <option <?=(isset($status_type)&&$status_type=='Registration Success'?'selected="selected"':'');?>value="Registration Success">Registration Success</option>
            <option <?=(isset($status_type)&&$status_type=='Other'?'selected="selected"':'');?>value="Other">Other</option>
            <option value="Tariff Switch Success">Tariff Switch Success</option>
            <option value="Tariff Switch Failure">Tariff Switch Failure</option>
            <option value="Tariff Switch Pending">Tariff Switch Pending</option>
        </select>

    </div>


    <div class="web_to_case_testing_wrapper">

        <!-- Contact Name - ONLY ACCEPTS SALESFORCE ID -->
        <label>Contact Name</label>
        <input id="name" maxlength="80" name="name" size="20" type="text" value=""/>
        <!-- Discounts Electricity -->
        <label>Discounts Electricity</label>
        <input id="00N1n00000SakSP" maxlength="255" name="00N1n00000SakSP" size="20" type="text" value=""/>
        <!-- Discounts Gas -->
        <label>Discounts Gas</label>
        <input id="00N1n00000SakSU" maxlength="255" name="00N1n00000SakSU" size="20" type="text" value=""/>
        <!-- Supply End Date Electricity -->
        <label>Supply End Date Electricity</label>
        <input id="00N1n00000SakSF" maxlength="255" name="00N1n00000SakSF" size="20" type="text" value="<?=(isset($elec_supply_end_date)?$elec_supply_end_date:'');?>"/>
        <!-- Supply End Date Gas -->
        <label>Supply End Date Gas</label>
        <input id="00N1n00000SakSK" maxlength="255" name="00N1n00000SakSK" size="20" type="text" value="<?=(isset($gas_supply_end_date)?$gas_supply_end_date:'');?>"/>
        <!-- Tariff comparison rate Gas -->
        <label>Tariff comparison rate Gas</label>
        <input id="00N1n00000SajO2" maxlength="255" name="00N1n00000SajO2" size="20" type="text" value="<?=(isset($elec_tariff_comparison_rate)?$elec_tariff_comparison_rate:'');?>"/>

    </div>


    <div class="web_to_case_testing_wrapper">

        <!-- Tariff comparison rate Electricity -->
        <label>Tariff comparison rate Electricity</label>
        <input id="00N1n00000SajO1" maxlength="255" name="00N1n00000SajO1" size="20" type="text" value="<?=(isset($elec_comparison_rate)?$elec_comparison_rate:'');?>"/>
        <!-- Expected supply start date Gas -->
        <label>Expected supply start date Gas</label>
        <input id="00N1n00000SajNw" maxlength="255" name="00N1n00000SajNw" size="20" type="text" value="<?=(isset($gas_expected_supply_start_date)?$gas_expected_supply_start_date:'');?>"/>
        <!-- Expected supply start date Electricity -->
        <label>Expected supply start date Electricity</label>
        <input id="00N1n00000SajNv" maxlength="255" name="00N1n00000SajNv" size="20" type="text" value="<?=(isset($elec_expected_supply_start_date)?$elec_expected_supply_start_date:'');?>"/>
        <!-- Estimated annual cost Electricity -->
        <label>Estimated annual cost Electricity</label>
        <input id="00N1n00000SajNt" maxlength="255" name="00N1n00000SajNt" size="20" type="text" value="<?=(isset($elec_estimated_annual_cost)?$elec_estimated_annual_cost:'');?>"/>
        <!-- Estimated annual cost Gas -->
        <label>Estimated annual cost Gas</label>
        <input id="00N1n00000SajNu" maxlength="255" name="00N1n00000SajNu" size="20" type="text" value="<?=(isset($gas_estimated_annual_cost)?$gas_estimated_annual_cost:'');?>"/>
        <!-- Energy unit rate (p/kwh) Gas -->
        <label>Energy unit rate (p/kwh) Gas</label>
        <input id="00N1n00000SajNs" maxlength="255" name="00N1n00000SajNs" size="20" type="text" value="<?=(isset($gas_unit_rate)?$gas_unit_rate:'');?>"/>

    </div>


    <div class="web_to_case_testing_wrapper">

        <!-- Energy unit rate (p/kwh) Electricity -->
        <label>Energy unit rate (p/kwh) Electricity</label>
        <input id="00N1n00000SajNr" maxlength="255" name="00N1n00000SajNr" size="20" type="text" value="<?=(isset($elec_unit_rate)?$elec_unit_rate:'');?>"/>
        <!-- Daily Standing charge (p/day) Electricity -->
        <label>Daily Standing charge (p/day) Electricity</label>
        <input id="00N1n00000SajNp" maxlength="255" name="00N1n00000SajNp" size="20" type="text" value="<?=(isset($elec_daily_standing_charge)?$elec_daily_standing_charge:'');?>"/>
        <!-- Daily Standing charge (p/day) Gas -->
        <label>Daily Standing charge (p/day) Gas</label>
        <input id="00N1n00000SajNq" maxlength="255" name="00N1n00000SajNq" size="20" type="text" value="<?=(isset($gas_daily_standing_charge)?$gas_daily_standing_charge:'');?>"/>
        <!-- Assumed annual consumption Electricity -->
        <label>Assumed annual consumption Electricity</label>
        <input id="00N1n00000SajNn" maxlength="255" name="00N1n00000SajNn" size="20" type="text" value="<?=(isset($elec_annual_consumption)?$elec_annual_consumption:'');?>"/>
        <!-- Assumed annual consumption Gas -->
        <label>Assumed annual consumption Gas</label>
        <input id="00N1n00000SajNo" maxlength="255" name="00N1n00000SajNo" size="20" type="text" value="<?=(isset($gas_annual_consumption)?$gas_annual_consumption:'');?>"/>
        <!-- Case Record Type -->
        <label>Case Record Type</label>
        <select id="recordType" name="recordType">
            <option value="">--None--</option>
            <option <?=(isset($CustomerIssue)?$CustomerIssue:'')?> value="0120Y000000FfqQ">Customer Issue</option>
            <option <?=(isset($Dyball)?$Dyball:'')?> value="0121n0000007GkE">Dyball</option>
            <option <?=(isset($ECOCase)?$ECOCase:'')?> value="0121n0000007GkF">ECO Case</option>
            <option <?=(isset($InternalCase)?$InternalCase:'')?> value="0121n0000007GkG">Internal Case</option>
            <option <?=(isset($InternalITSupport)?$InternalITSupport:'')?> value="0120Y000000FbCo">Internal IT Support</option>
            <option <?=(isset($NewJuniferAccount)?$NewJuniferAccount:'')?> value="0121n0000007GkH">New Junifer Account</option>
            <option <?=(isset($PrivacyType)?$PrivacyType:'')?> value="0121n0000007GkI">Privacy Type</option>
            <option <?=(isset($UtilityCase)?$UtilityCase:'')?> value="0121n0000007GkJ">Utility Case</option>
        </select>

    </div>


    <div style="clear: both"></div>


    <div class="web_to_case_testing_wrapper">

        <!-- Meter Supply Number Electricity -->
        <label>Meter Supply Number Electricity:</label>
        <input id="00N1n00000Saz7d" name="00N1n00000Saz7d" size="20" type="text" value=""/>
        <!-- Meter Supply Number Gas -->
        <label>Meter Supply Number Gas:</label>
        <input id="00N1n00000Saz7e" name="00N1n00000Saz7e" size="20" type="text" value=""/>

        <!-- Product Name ELECTRICITY -->
        <label>Product Name ELECTRICITY:</label>
        <input id="00N1n00000Saz7f" name="00N1n00000Saz7f" size="20" type="text" value=""/>
        <!-- Product Name Gas -->
        <label>Product Name Gas:</label>
        <input id="00N1n00000Saz7g" name="00N1n00000Saz7g" size="20" type="text" value=""/>

        <!-- Meter Serial Number Electricity -->
        <label>Meter Serial Number Electricity:</label>
        <input id="00N1n00000Saz7b" name="00N1n00000Saz7b" size="20" type="text" value=""/>
        <!-- Meter Serial Number Gas -->
        <label>Meter Serial Number Gas:</label>
        <input id="00N1n00000Saz7c" name="00N1n00000Saz7c" size="20" type="text" value=""/>

    </div>


    <div class="web_to_case_testing_wrapper">

        <!-- Company - NO COMMERCIAL CLIENTS AT THE MOMENT  -->
        <label>Company</label>
        <input id="company" maxlength="80" name="company" size="20" type="text" value=""/>
        <!-- Case send date & time -->
        <label>Case send date & time</label>
        <input id="00N1n00000SB9ST" name="00N1n00000SB9ST" size="20" type="text" value="<?=date('d/m/Y');?>"/>

    </div>


</form>

<div class="button_wrapper">
    <br><br><a href="#" class="send">Send</a>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">

    var base_url = '<?php echo base_url();?>';

    $(document).ready(function () {

        $(document).on('click', 'a#junifer_customer', function () {

            $('#junifer_customer').addClass('active');
            $('#dyball_customer').removeClass('active');
            $('#signup_type').val(0);
            $('#status_type').val($('#00N1l000000O97T').val());
            $('#hidden_form').submit();
        });

        $(document).on('click', 'a#dyball_customer', function () {

            $('#dyball_customer').addClass('active');
            $('#junifer_customer').removeClass('active');
            $('#signup_type').val(3);
            $('#status_type').val($('#00N1l000000O97T').val());
            $('#hidden_form').submit();

        });

        $(document).on('change', '#00N1l000000O97T', function () {

            $('#dyball_customer').addClass('active');
            $('#junifer_customer').removeClass('active');
            $('#status_type').val($('#00N1l000000O97T').val());
            $('#hidden_form').submit();

        });

        $(document).on('click', '.send', function () {

            // Log web to case form
            $.ajax({
                url: base_url + 'index.php/user/web_to_case_log',
                type: 'post',
                data: {
                    org_id: $('#orgid').val(),
                    name: $('#00N1n00000SB9PS').val(), // Name
                    email: $('#wtc_email').val(),
                    phone: $('#phone').val(),
                    dob: $('#00N1n00000SB9PN').val(), // DOB
                    address: $('#00N1n00000SB9PI').val(), // Address line 1
                    postcode: $('#00N1n00000SB9Ph').val(), // Postcode
                    subject: $('#subject').val(),
                    description: $('#description').val(),
                    junifer_customer_id: $('#00N1l000000Ntsj').val(), // Junifer customer id
                    dyball_account_id: $('#00N1n00000SB9NR').val(), // dyball account number
                    external_system_registration_status: $('#00N1l000000O97T').val(), // External System Registration Status*/

                    contact_name: $('#name').val(), // Contact Name
                    discount_elec: $('#00N1n00000SakSP').val(), // Discount Elec
                    discount_gas: $('#00N1n00000SakSU').val(), // Discounts Gas
                    supply_end_date_elec: $('#00N1n00000SakSF').val(), // Supply End Date Electricity
                    supply_end_date_gas: $('#00N1n00000SakSK').val(), // Supply End Date Gas
                    tariff_comparison_rate_gas: $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
                    tariff_comparison_rate_elec: $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
                    expected_supply_start_date_gas: $('#00N1n00000SajNw').val(), // Expected supply start date Gas
                    expected_supply_start_date_elec: $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
                    estimated_annual_cost_elec: $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
                    estimated_annual_cost_gas: $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
                    gas_unit_rate: $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
                    elec_unit_rate: $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
                    daily_standing_charge_elec: $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
                    daily_standing_charge_gas: $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
                    assumed_annual_consumption_elec: $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
                    assumed_annual_consumption_gas: $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
                    record_type: $('#recordType').val(), // Case Record Type
                    company: $('#company').val(),
                    case_send_date: $('#00N1n00000SB9ST').val(), // Case send date & time

                    meter_supply_number_elec: $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
                    meter_supply_number_gas: $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
                    product_name_gas: $('#00N1n00000Saz7g').val(), // Product Name Gas
                    product_name_elec: $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
                    meter_serial_number_elec: $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
                    meter_serial_number_gas: $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

                }
            });
            // End log web to case

            // Send the web to case form
            $.ajax({
                url: 'https://eversmartenergy--QA.cs108.my.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8',
                type: 'post',
                data: {
                    orgid: $('#orgid').val(),
                    '00N1n00000SB9PS': $('#00N1n00000SB9PS').val(), // Name
                    email: $('#wtc_email').val(),
                    phone: $('#phone').val(),
                    '00N1n00000SB9PN': $('#00N1n00000SB9PN').val(), // DOB
                    '00N1n00000SB9PI': $('#00N1n00000SB9PI').val(), // Address line 1
                    '00N1n00000SB9Ph': $('#00N1n00000SB9Ph').val(), // Postcode
                    subject: $('#subject').val(),
                    description: $('#description').val(),
                    '00N1l000000Ntsj': $('#00N1l000000Ntsj').val(), // Junifer customer id
                    '00N1n00000SB9NR': $('#00N1n00000SB9NR').val(), // dyball account number
                    '00N1l000000O97T': $('#00N1l000000O97T').val(), // External System Registration Status

                    name: $('#name').val(), // Contact Name
                    '00N1n00000SakSP': $('#00N1n00000SakSP').val(), // Discount Elec
                    '00N1n00000SakSU': $('#00N1n00000SakSU').val(), // Discounts Gas
                    '00N1n00000SakSF': $('#00N1n00000SakSF').val(), // Supply End Date Electricity
                    '00N1n00000SakSK': $('#00N1n00000SakSK').val(), // Supply End Date Gas
                    '00N1n00000SajO2': $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
                    '00N1n00000SajO1': $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
                    '00N1n00000SajNw': $('#00N1n00000SajNw').val(), // Expected supply start date Gas
                    '00N1n00000SajNv': $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
                    '00N1n00000SajNt': $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
                    '00N1n00000SajNu': $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
                    '00N1n00000SajNs': $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
                    '00N1n00000SajNr': $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
                    '00N1n00000SajNp': $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
                    '00N1n00000SajNq': $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
                    '00N1n00000SajNn': $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
                    '00N1n00000SajNo': $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
                    recordType: $('#recordType').val(), // Case Record Type
                    company: $('#company').val(),
                    '00N1n00000SB9ST': $('#00N1n00000SB9ST').val(), // Case send date & time

                    '00N1n00000Saz7d': $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
                    '00N1n00000Saz7e': $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
                    '00N1n00000Saz7g': $('#00N1n00000Saz7g').val(), // Product Name Gas
                    '00N1n00000Saz7f': $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
                    '00N1n00000Saz7b': $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
                    '00N1n00000Saz7c': $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

                }
            });

            alert('Success');

        });

    });

</script>