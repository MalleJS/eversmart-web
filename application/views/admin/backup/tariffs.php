<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if(empty($AdminUser) || $AdminUser['admin_role_type_id'] !='3'){
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Admin - Tariff Management</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
    </head>

    <style type="text/css">

        label {
            width: 130px;
        }

        fieldset {
            padding: 20px;
        }

        #response {
            color: #0dcc0d;
            font-weight: 900;
        }

        #save-new-tariff, #save-edit-tariff, button#clear-form {
            width: 100px;
            border-radius: 30px;
            font-weight: 600;
            font-size: 16px;
        }

        #add-new-tariff {
            float: right;
            margin-bottom: 20px;
        }

        #tariff-management {
            float: left;
            width: auto;
        }

        #add-new-tariff {
            float: right;
        }

        .clear {
            clear: both;
            margin-bottom: 20px;
        }

        p {
            margin-bottom: 15px;
        }

        thead {
            font-weight: 900;
        }

        tbody {
            font-size: 0.85em;
        }

        #add-tariff-form input, #edit-tariff-form input {
            padding: 5px;
        }

        .input-wrap {
            margin-bottom: 10px;
        }

    </style>

    <body>

        <div class="topbar-admin col-lg-12">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png"></a>
            <a class="admin-links admin-here" href="<?php echo base_url() ?>tariff/">Tariff Management</a>
            <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
        </div>

        <div class="container custform col-lg-12">

            <div class="row contact_row">

                <div class="contact_col first_col col-md-6">

                    <fieldset>

                        <legend id="tariff-management">Tariff Management</legend>
                        <button id="add-new-tariff" class="btn">Add New Tariff</button>

                        <div class="clear"></div>

                        <p>Below is a list of all our tariffs. You can amend existing tariffs using the <strong>'Edit'</strong> buttons listed below. Alternatively, you can add a new tariff using the <strong>'Add New Tariff'</strong> button above.</p>

                        <p id="response"></p>

                        <table id="tariff_table" class="table admin-lead-table">
                            <thead>
                                <td>Tariff Name</td>
                                <td>Tariff Code</td>
                                <td>Type</td>
                                <td>Website Visible?</td>
                                <td>Created On</td>
                                <td></td>
                            </thead>
                        </table>

                    </fieldset>

                </div>

                <div id="add-tariff" class="contact_col first_col col-md-6" style="display:none;">

                    <fieldset>

                        <legend>Add Tariff</legend>

                        <form name="add-tariff-form" id="add-tariff-form">
                            <div class="input-wrap">
                                <label>Tariff Name</label>
                                <input type="text" name="tariff_name" id="tariff_name" value=""/>
                            </div>

                            <div class="input-wrap">
                                <label>Tariff Code</label>
                                <input type="text" name="tariff_code" id="tariff_code" value=""/>
                            </div>

                            <div class="input-wrap">
                                <label>Type</label>
                                <select name="tariff_type" id="tariff_type">
                                    <option value="Fixed">Fixed</option>
                                    <option value="Variable">Variable</option>
                                </select>
                            </div>

                            <div class="input-wrap">
                                <label>Website Visible?</label>
                                <input type="checkbox" name="is_visible" id="is_visible" value="1"/>
                            </div>

                            <div class="submit_col second_col col-md-6">
                                <fieldset>
                                    <div class="form-group">
                                        <button id="save-new-tariff" name="submit-form" class="btn btn-primary">Save</button>
                                        <button id="clear-form" name="clear-form" class=" close-tariff btn btn-primary">Cancel</button>
                                    </div>
                                </fieldset>
                            </div>

                        </form>

                    </fieldset>

                </div>


                <div id="edit-tariff" class="contact_col first_col col-md-6" style="display:none;">

                    <fieldset>

                        <legend>Edit Tariff</legend>

                        <form name="edit-tariff-form" id="edit-tariff-form">

                            <input type="hidden" name="tariff_id" id="tariff_id" value=""/>

                            <div class="input-wrap">
                                <label>Tariff Name</label>
                                <input type="text" name="tariff_name" id="tariff_name" value=""/>
                            </div>

                            <div class="input-wrap">
                                <label>Tariff Code</label>
                                <input type="text" name="tariff_code" id="tariff_code" value=""/>
                            </div>

                            <div class="input-wrap">
                                <label>Type</label>
                                <select name="tariff_type" id="tariff_type">
                                    <option value="Fixed">Fixed</option>
                                    <option value="Variable">Variable</option>
                                </select>
                            </div>

                            <div class="input-wrap">
                                <label>Website Visible?</label>
                                <input type="checkbox" name="is_visible" id="is_visible" value="1"/>
                            </div>

                            <div class="submit_col second_col col-md-6">
                                <fieldset>
                                    <div class="form-group">
                                        <button id="save-edit-tariff" name="submit-form" class="btn btn-primary">Save</button>
                                        <button id="clear-form" name="clear-form" class=" close-tariff btn btn-primary">Cancel</button>
                                    </div>
                                </fieldset>
                            </div>

                        </form>

                    </fieldset>

                </div>

            </div>

        </div>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/handlebars-v4.0.12.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/TimelineMax.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/TweenMax.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/jquery-3.3.1.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin/tariff.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/global.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/plugins.js"></script>

    </body>

