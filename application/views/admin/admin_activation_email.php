+<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Resend Emails</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending
            Registrations</a>
    <?php } ?>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email" selected>Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update" selected>Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
        <?php } ?>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>

<div class="container custform col-lg-12">
    <div class="row contact_row">
        <div class="contact_col first_col col-md-6">
            <form id="search-params">
                <fieldset>

                    <legend>Search Parameters (enter one or both)</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="search_surname">Surname</label>

                        <div class="form-style-field-lg">
                            <input id="search_surname" name="search_surname" type="text"
                                   onkeypress="return !isNumberKey(event)" placeholder="" maxlength="20"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="search_email">Email</label>

                        <div class="form-style-field-lg">
                            <input id="search_email" name="search_email" type="email" placeholder="" maxlength="35"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <input name="page" type="hidden" value="customer_info">
                </fieldset>
            </form>

            <div class="form-group">

                <label class="control-label form-style-label" for="email-lookup"></label>

                <div class="form-style-field-lg">
                    <button id="email-lookup" name="email-lookup" class="btn btn-primary">Look Up</button>
                </div>
            </div>

        </div>

        <div class="contact_col second_col col-md-6">

            <fieldset>

                <legend>Customer Details</legend>
                <div class="line-sep"></div>
                <div id="account_outcome"></div>

            </fieldset>

        </div>
    </div>

    <div class="row submit_row">
        <div class="contact_col first_col col-md-6">

            <fieldset>

                <legend>Select Email Type</legend>
                <div class="line-sep"></div>

                <div id="email_type"></div>

            </fieldset>
        </div>

        <div class="contact_col second_col col-md-6">

            <fieldset>

                <legend>Confirm & Send</legend>
                <div class="line-sep"></div>

                <div class="form-group">

                    <button id="send-email" name="send-email" class="btn btn-primary">Send Email</button>
                </div>

            </fieldset>

        </div>

    </div>
</div>

<div id="email_outcome"></div>

<section class="postcode_top mt0">
    <div class="postcode_box ie-extra-padding" id="background-none">
        <form id="email_form" style="display:none">
            <input type="text" name="id" id="id">
            <input type="text" name="forename" id="forename">
            <input type="text" name="email" id="email">
            <input type="text" name="email_type" id="email_type">
        </form>
    </div>
</section>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_email.js"></script>

</body>

