<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Lead Update</title>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body>

<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending
            Registrations</a>

    <?php } ?>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup" selected>Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration" selected>Pending
                Registrations
            </option>
        <?php } ?>
    </select>
    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>

</div>
<div class="container col-12">

    <div class="row submit_row">
        <div class="contact_col first_col col-md-12">
            <legend>Pending Registration</legend>
            <div class="line-sep"></div>
        </div>

        <?php

        if (isset($error_msg) && $error_msg) {

            echo "<div class='alert alert-danger' role='alert'>{$error_msg}</div>";
        }
        if (isset($msg) && $msg) {

            echo "<div class='alert alert-success' role='alert'>{$msg}</div>";
        }

        ?>
        <!-- Bootstrap CSS -->
        <!-- jQuery first, then Bootstrap JS. -->
        <!-- Nav tabs -->
        <div class="row col-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#junifer" role="tab" data-toggle="tab"><span
                                class="badge badge-secondary"><?php echo isset($junifer_registration) ? count($junifer_registration) : '' ?></span>
                        Direct Debit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#dyball" role="tab" data-toggle="tab"><span
                                class="badge badge-secondary"><?php echo isset($dyball_registration) ? count($dyball_registration) : '' ?></span>
                        Pre Pay</a>
                </li>
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="junifer">
                <div class='col-12' style="font-size: 14px">
                    <?php
                    if (isset($junifer_registration) && count($junifer_registration) > 0) {
                        ?>
                        <table class="table table-responsive table-hover">
                            <thead>
                            <tr class="">
                                <th>ID</th>
                                <th>Forename</th>
                                <th>Surname</th>
                                <th>Email Address</th>
                                <th>Phone Number</th>
                                <th>Billing Method</th>
                                <th>Address</th>
                                <th>Post Code</th>
                                <th>MPAN/MPRN</th>
                                <th>Elec Serial</th>
                                <th>Gas Serial</th>
                                <th>Created On</th>
                            </tr>
                            </thead>

                            <?php foreach ($junifer_registration as $junifer_records) {
                                $uniq = md5($junifer_records['id']);
                                echo "<tr>";
                                echo "<td> {$junifer_records['id']} </td>";
                                echo "<td> {$junifer_records['forename']} </td>";
                                echo "<td> {$junifer_records['surname']} </td>";
                                echo "<td> {$junifer_records['email']} </td>";
                                echo "<td> {$junifer_records['phone1']} </td>";
                                echo "<td> {$junifer_records['billingMethod']} </td>";
                                echo "<td> {$junifer_records['first_line_address']} </td>";
                                echo "<td> {$junifer_records['address_postcode']} </td>";
                                $hidden = array('database_id' => $junifer_records['id'], 'type' => 'junifer');
                                echo form_open('Admin/admin_show_pending_energy_registration', ["id" => $uniq], $hidden);
                                echo "<td>  <div class='col-auto'> <label class='sr-only' for='inlineFormInputGroup'>MPAN</label><div class='input-group mb-2'>
                                    <div class='input-group-prepend'>
                                    <div class='input-group-text'>MPAN</div>
                                    </div>
                                    <input type='text' for='{$uniq}' name='mpan' class='form-control' maxlength='50' id='inlineFormInputGroup' value='{$junifer_records['electricityProduct_mpans']}'>
                                    </div>
                                    </div><br/>";

                                echo "<div class='col-auto'> <label class='sr-only' for='inlineFormInputGroup'>MPRN</label><div class='input-group mb-2'>
                                    <div class='input-group-prepend'>
                                    <div class='input-group-text'>MPRN</div>
                                    </div>
                                    <input type='text' for='{$uniq}' name='mprn' class='form-control' maxlength='50' id='inlineFormInputGroup' value='{$junifer_records['gasProduct_mprns']}'>
                                    </div>
                                    </div></td>";
                                echo "<td> <input type='text' for='{$uniq}' name='elec_serial' class='form-control' value='{$junifer_records['elec_meter_serial']}' placeholder=''/></td>";
                                echo "<td> <input type='text' for='{$uniq}' name='gas_serial' class='form-control' value='{$junifer_records['gas_meter_serial']}' placeholder=''/></td>";
                                echo "<td> {$junifer_records['create_at']} </td>";

                                echo "<td> <button for='{$uniq}' type='submit' class='btn btn-outline-danger'>Register</button></td>";
                                echo "</form>";
                                echo "</tr>";
                            } ?>
                        </table>
                    <?php } ?>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="dyball">
                <div class='col-12' style="font-size: 14px">
                    <?php
                    if (isset($dyball_registration) && count($dyball_registration) > 0) {
                        ?>
                        <table class="table table-hover">
                            <thead>
                            <tr class="">
                                <th>ID</th>
                                <th>Forename</th>
                                <th>Surname</th>
                                <th>Email Address</th>
                                <th>Phone Number</th>
                                <th>Billing Method</th>
                                <th>Address</th>
                                <th>Post Code</th>
                                <th>MPAN</th>
                                <th>MPRN</th>
                                <th>Created On</th>
                            </tr>
                            </thead>
                            <?php foreach ($dyball_registration as $dyball_records) {
                                echo "<tr>";
                                echo "<td> {$dyball_records['id']} </td>";
                                echo "<td> {$dyball_records['forename']} </td>";
                                echo "<td> {$dyball_records['surname']} </td>";
                                echo "<td> {$dyball_records['email']} </td>";
                                echo "<td> {$dyball_records['phone1']} </td>";
                                echo "<td> {$dyball_records['billingMethod']} </td>";
                                echo "<td> {$dyball_records['first_line_address']} </td>";
                                echo "<td> {$dyball_records['address_postcode']} </td>";
                                $hidden = array('database_id' => $dyball_records['id'], 'type' => 'dyball');
                                echo form_open('Admin/admin_show_pending_energy_registration', '', $hidden);
                                echo "<td>  <div class='col-auto'> <label class='sr-only' for='inlineFormInputGroup'>MPAN</label><div class='input-group mb-2'>
                                    <div class='input-group-prepend'>
                                    <div class='input-group-text'>MPAN</div>
                                    </div>
                                    <input type='text' name='dy_mpan' class='form-control' maxlength='50' id='inlineFormInputGroup' placeholder='enter mpan'>
                                    </div>
                                    </div></td>";

                                echo "<td>  <div class='col-auto'> <label class='sr-only' for='inlineFormInputGroup'>MPRN</label><div class='input-group mb-2'>
                                    <div class='input-group-prepend'>
                                    <div class='input-group-text'>MPRN</div>
                                    </div>
                                    <input type='text' name='dy_mprn' class='form-control' maxlength='50' id='inlineFormInputGroup' placeholder='enter mprn'>
                                    </div>
                                    </div></td>";
                                echo "<td> {$dyball_records['create_at']} </td>";
                                echo "<td> <button type='submit' class='btn btn-outline-danger'>Register</button></td>";
                                echo "</form>";
                                echo "</tr>";
                            } ?>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--assets/js/jquery-3.2.1.min.js"></script>-->
<script>
</script>
</body>
</html>
