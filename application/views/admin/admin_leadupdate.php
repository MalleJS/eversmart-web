<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if(empty($AdminUser) || $AdminUser['admin_role_type_id'] !='1'){
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Lead Update</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</a>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</option>
        <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>

<div class="container custform col-lg-12">
    <?php // print_r($leads) ?>

    <div class="row contact_row">
        <div class="contact_col first_col col-md-7">
            <form id="lead-search">
                <legend>Search Filters</legend>
                <div class="line-sep"></div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="search_status">Lead Status</label>
                    <div class="form-style-field-lg">
                        <select id="search_status" name="search_status" class="form-control">
                            <?php if( !empty($lead_status) ){
                                for( $j = 0; $j<count( $lead_status )-2 ; $j++ ){?>
                                    <option value="<?php echo $lead_status[$j]['lead_status_type_id']?>"><?php echo $lead_status[$j]['name']?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label form-style-label" for="lead-lookup"></label>
                    <div class="form-style-field-lg">
                        <button id="lead-lookup" name="lead-lookup" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row submit_row">
        <div class="contact_col first_col col-md-7">
            <legend>Non-converted Leads</legend>
            <div class="line-sep"></div>

            <div id="search-error"></div>
            <div id="search-results">

                <table id="admin_lead_table" class="table admin-lead-table" style="">
                    <thead>
                    <tr class="admin_lead_table_head">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Quote</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>

        <div class="contact_col second_col col-md-5">
            <legend>Selected Lead</legend>
            <div class="line-sep"></div>

            <div id="selected-lead">
                <form method="post" action="<?php echo base_url() ?>admin/admin_customer_signup" id="lead_details">
                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-name">Name</label>
                        <div class="form-style-field-lg">
                            <input id="lead-name" name="lead_name" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-dob">Date of Birth</label>
                        <div class="form-style-field-lg">
                            <input id="lead-dob" name="lead_dob" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-phone">Phone</label>
                        <div class="form-style-field-lg">
                            <input id="lead-phone" name="lead_phone" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-email">Email</label>
                        <div class="form-style-field-lg">
                            <input id="lead-email" name="lead_email" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-address">Address</label>
                        <div class="form-style-field-lg">
                            <input id="lead-address" name="lead_address" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <input id="lead_title" name="lead_title" type="hidden" value="">
                    <input id="lead_forename" name="lead_forename" type="hidden" value="">
                    <input id="lead_surname" name="lead_surname" type="hidden" value="">
                    <input id="lead_postcode" name="lead_postcode" type="hidden" value="">
                    <input id="lead_id" name="lead_id" type="hidden" value="">
                    <input id="lead_status" name="lead_status" type="hidden" value="">

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-tariff">Tariff</label>
                        <div class="form-style-field-lg">
                            <input id="lead-tariff" name="lead_tariff" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-eac">EAC</label>
                        <div class="form-style-field-lg">
                            <input id="lead-eac" name="lead_eac" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-aq">AQ</label>
                        <div class="form-style-field-lg">
                            <input id="lead-aq" name="lead_aq" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="lead-name">Quote</label>
                        <div class="form-style-field-lg">
                            <input id="lead-quote" name="lead_quote" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group" id="update">
                        <label style="margin-top: 0px" class="control-label form-style-label" for="update-status">New Status</label>
                        <div class="form-style-field-lg">
                            <select id="update-status" name="update-status" class="form-control">
                                <option value="0">(Please Select) </option>
                                <option value="2">Call back 1</option>
                                <option value="3">Call back 2</option>
                                <option value="4">Call back 3</option>
                                <option value="6">Not interested</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="form-style-field-lg" style="width:100%">
                                <button id="lead-update-final" name="lead-update-final"class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-style-field-lg" style="width:100%">
                            <button id="lead-update" name="lead-update"class="btn btn-primary">Update Lead (Callback failed)</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-style-field-lg" style="width:100%">
                            <button id="lead-submit" name="lead_submit" type="submit" class="btn btn-primary">Convert Lead (Callback success)</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div id="update_result"></div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_leadupdate.js"></script>

</body>

