<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Sales Data Capture</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</a>
    <?php } ?>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture" selected>Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update" selected>Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
        <?php } ?>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>
<div id="admin_datacapture_msg"></div>
<div class="container custform col-lg-12">
    <form id="form-quote">
        <div class="row quote_row">
            <div class="quote_col first_col col-md-6">
                <fieldset>
                    <legend>Contact Address</legend>
                    <div class="line-sep"></div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-postcode">Postcode</label>
                        <div class="form-style-field-lg">
                            <input id="contact-postcode" name="address_postcode" type="search" placeholder=""
                                   maxlength="10" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-lookup"></label>
                        <div class="form-style-field-lg">
                            <button id="contact-lookup" name="contact-lookup" class="btn btn-primary">Look Up</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="address_select" id="pcod-address"></label>
                        <div class="form-style-field-lg">
                            <div id="address_select"></div>
                        </div>
                    </div>

                </fieldset>
            </div>
            <div class="quote_col second_col col-md-6">
                <legend>Tariff & Direct Debit</legend>
                <div class="line-sep"></div>
                <div class="form-group">
                    <label class="control-label form-style-label" for="tariff-tariff">Tariff</label>
                    <div class="form-style-field-lg">
                        <input id="tariff-tariff" name="tariff" type="text" class="form-control input-md"
                               value="Safeguard PAYG" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="tariff-fuel">Fuel Type</label>
                    <div class="form-style-field-lg">
                        <select id="tariff-fuel" name="tariff_fuel" class="form-control">
                            <option value="dual">Dual</option>
                            <option value="elec">Elec only</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="tariff-consum-elec">Home Type</label>
                    <div class="form-style-field-lg">
                        <select id="tariff-consum-elec" name="tariff-consum-elec" class="form-control">
                            <option value="1">(Please Select)</option>
                            <option value="2">1-2 bed Flat</option>
                            <option value="3">1-2 bed Terrace</option>
                            <option value="4">1-2 bed Semi</option>
                            <option value="5">1-2 bed Detached</option>
                            <option value="6">3-4 bed Flat</option>
                            <option value="7">3-4 bed Terrace</option>
                            <option value="8">3-4 bed Semi</option>
                            <option value="9">3-4 bed Detached</option>
                            <option value="10">5+ bed Flat</option>
                            <option value="11">5+ bed Terrace</option>
                            <option value="12">5+ bed Semi</option>
                            <option value="13">5+ bed Detached</option>
                        </select>
                    </div>
                </div>

                <div id="dual-select">
                    <div class="form-group">
                        <label class="control-label form-style-label" for="tariff-consum-gas">Home Type (Gas)</label>
                        <div class="form-style-field-lg">
                            <select id="tariff-consum-gas" name="tariff-consum-gas" class="form-control">
                                <option value="1">(Please Select)</option>
                                <option value="2">1-2 bed Flat</option>
                                <option value="3">1-2 bed Terrace</option>
                                <option value="4">1-2 bed Semi</option>
                                <option value="5">1-2 bed Detached</option>
                                <option value="6">3-4 bed Flat</option>
                                <option value="7">3-4 bed Terrace</option>
                                <option value="8">3-4 bed Semi</option>
                                <option value="9">3-4 bed Detached</option>
                                <option value="10">5+ bed Flat</option>
                                <option value="11">5+ bed Terrace</option>
                                <option value="12">5+ bed Semi</option>
                                <option value="13">5+ bed Detached</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <input id="supply_postcode" name="supply_postcode" type="hidden" value="">
            <input id="monthyear" name="monthyear" type="hidden" value="monthlypay">
            <input id="billingMethod" name="billingMethod" type="hidden" value="prepay">
            <input id="eac" name="eac" type="hidden" value="">
            <input id="aq" name="aq" type="hidden" value="">
            <input id="quotefor" name="quotefor" type="hidden" value="both">
            <input id="elecPrevSupplier" name="elecPrevSupplier" type="hidden" value="British Gas">
            <input id="gasPrevSupplier" name="elecPrevSupplier" type="hidden" value="British Gas">
            <input id="elecPrevTariff" name="elecPrevTariff" type="hidden" value="Standard">
            <input id="gasPrevTariff" name="elecPrevTariff" type="hidden" value="Standard">

            <div class="container" style="margin-top:40px">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <fieldset>
                            <div class="form-group">
                                <button id="get-quote" name="get-quote" class="btn btn-primary">GET QUOTE</button>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-sm-1"></div>
                    <div id="admin_quote"></div>
                </div>
            </div>
        </div>
    </form>

    <div id="submit-panel">
        <form id="form-submit">
            <div class="row contact_row">
                <div class="contact_col first_col col-md-6">
                    <fieldset id="quote-rates">
                        <legend>Quote & Rates</legend>
                        <div class="line-sep"></div>

                        <div class="form-group">
                            <label class="control-label form-style-label" for="quote-annual">Annual Cost (£)</label>
                            <div class="form-style-field-lg">
                                <input id="quote-annual" name="quote_annual" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label form-style-label" for="quote-monthly">Weekly Cost (£)</label>
                            <div class="form-style-field-lg">
                                <input id="quote-monthly" name="quote_weekly" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label form-style-label" for="quote-elec-ur">Elec Unit Rate
                                (p/kWh)</label>
                            <div class="form-style-field-lg">
                                <input id="quote-elec-ur" name="quote_elec_ur" type="text"
                                       class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label form-style-label" for="quote-elec-sc">Elec S.Charge
                                (p/day)</label>
                            <div class="form-style-field-lg">
                                <input id="quote-elec-sc" name="quote_elec_sc" type="text"
                                       class="form-control input-md">
                            </div>
                        </div>
                        <div id="dual-quote">
                            <div class="form-group">
                                <label class="control-label form-style-label" for="quote-elec-ur">Gas Unit Rate
                                    (p/kWh)</label>
                                <div class="form-style-field-lg">
                                    <input id="quote-gas-ur" name="quote_gas_ur" type="text"
                                           class="form-control input-md">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label form-style-label" for="quote-elec-ur">Gas S.Charge
                                    (p/day)</label>
                                <div class="form-style-field-lg">
                                    <input id="quote-gas-sc" name="quote_gas_sc" type="text"
                                           class="form-control input-md">
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="contact_col second_col col-md-6">
                    <fieldset id="cust-details">
                        <legend>Contact Details</legend>
                        <div class="line-sep"></div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="contact-title">Name</label>
                            <div class="form-style-field-sm">
                                <select class="form-control input-md" id="contact-title" name="title" tabindex="0"
                                        required>
                                    <option selected value="0">Title</option>
                                    <option value="Mr">Mr.</option>
                                    <option value="Mrs">Mrs.</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Prof">Prof</option>
                                    <option value="Dr">Dr</option>
                                </select>
                            </div>
                            <div class="form-style-field-md">
                                <input id="contact-fname" onkeypress="return !isNumberKey(event)" name="forename"
                                       type="text" placeholder="Forename" class="form-control input-md" maxlength="25"
                                       spellcheck="false">
                            </div>
                            <div class="form-style-field-md">
                                <input id="contact-lname" onkeypress="return !isNumberKey(event)" name="surname"
                                       type="text" placeholder="Surname" class="form-control input-md" maxlength="25"
                                       spellcheck="false">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="contact-dob">Date of Birth</label>
                            <div class="form-style-field-lg">
                                <input id="contact-dob" name="dob" type="date" placeholder=""
                                       class="form-control input-md">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="contact-email">Email</label>
                            <div class="form-style-field-lg">
                                <input id="contact-email" name="email" type="email" placeholder="" spellcheck="false"
                                       class="form-control input-md">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="contact-tel">Phone</label>
                            <div class="form-style-field-lg">
                                <input id="contact-tel" name="phone" onkeypress="return isNumberKey(event)" type="text"
                                       placeholder="" maxlength="11" class="form-control input-md">
                            </div>
                        </div>

                        <input id="created_by" name="created_by" type="hidden" value="<?php if ($AdminUser) {
                            echo $AdminUser['id'];
                        } ?>">
                    </fieldset>
                </div>
            </div>

            <div class="row mrk-consent_row">

                <div class="contact_col first_col col-md-6">

                    <legend>Marketing consent</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="mrk-consent">Opt in to:</label>
                        <div class="marketing-check">
                            <div class="checkbox">
                                <label for="mrk-consent-0">
                                    <input type="checkbox" name="phone_opt_in" id="mrk-consent-0" value="1">
                                    Phone
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="mrk-consent-1">
                                    <input type="checkbox" name="email_opt_in" id="mrk-consent-1" value="1">
                                    Email
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="mrk-consent-3">
                                    <input type="checkbox" name="text_opt_in" id="mrk-consent-3" value="1">
                                    Text
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="mrk-consent-4">
                                    <input type="checkbox" name="post_opt_in" id="mrk-consent-4" value="1">
                                    Post
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="mrk-consent-5">
                                    <input type="checkbox" name="social_opt_in" id="mrk-consent-5" value="1">
                                    Social media
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="mrk-consent-6">
                                    <input type="checkbox" name="marketing_opt_out" id="mrk-consent-6" value="1">
                                    None of the above (I like the dark)
                                </label>
                            </div>
                        </div>
                        <p class="form-print">Please note you can change these options any time by contacting us on our
                            website.</p>
                        <p class="form-print">Eversmart Energy does not participate in the Warm Home Discount
                            Scheme.</p>
                        <p class="form-print">Eversmart Energy does not participate in the Green Deal.</p>
                        <p class="form-print" style="margin-bottom: 3rem">You have a 14-day cooling off period, whereby
                            you can cancel this agreement.
                            If you wish to do so, please email us (hello@eversmartenergy.co.uk) or call us (0330 102
                            7901).</p>
                    </div>

                </div>

                <div class="contact_col second_col col-md-6">

                    <legend>Priority Services Register</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">
                        <p class="form-print">Does anybody living in the property require any medical support, have a
                            medical condition/disability or is over the age of 60?</p>

                        <div class="psr-check">
                            <label class="radio-inline" for="psr-0">
                                <input type="radio" name="psr" id="psr-0" value="1">Yes
                            </label>
                            <label class="radio-inline" for="psr-1">
                                <input type="radio" name="psr" id="psr-1" value="">No
                            </label>
                        </div>

                        <div id="psr-yes">
                            <p class="form-print">I ask because we have a Priority Service Register which helps us to
                                support our customers. For example if there was a planned disruption
                                to your energy supply and you had breathing equipment, we would notify you so you can
                                make alternative arrangements.
                                For many other benefits, please visit www.eversmartenergy.co.uk</p>
                            <p class="form-print">Before adding you to the Priority Service Register, I need to make you
                                aware we will share this information with third parties such as other
                                energy suppliers and meter operators. This information will not be used for any
                                marketing purposes.</p>
                            <p class="form-print">Please can you confirm you understand the above and are happy for this
                                information to be shared?</p>

                            <div class="psr-check">
                                <label class="radio-inline" for="psr-consent-0">
                                    <input type="radio" name="psr_consent" id="psr-consent-0" value="1">Yes
                                </label>
                                <label class="radio-inline" for="psr-consent-1">
                                    <input type="radio" name="psr_consent" id="psr-consent-1" value="">No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contact_col third_col col-md-12">
                    <p class="form-print form-confirm">On submission of this form, the information you have provided
                        will be shared with Eversmart Energy.<br>
                        Please can you confirm that you are happy to go ahead with this?</p>
                </div>
            </div>

            <div class="row submit_row" id="form-end">
                <div class="submit_col first_col col-md-6">
                    <fieldset>
                        <div class="form-group">
                            <button id="submit-form" name="submit-form" class="btn btn-primary">SUBMIT FORM</button>
                        </div>
                    </fieldset>
                </div>
                <div class="submit_col second_col col-md-6">
                    <fieldset>
                        <div class="form-group">
                            <button id="clear-form" name="clear-form" class="btn btn-primary">CLEAR FORM</button>
                        </div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_datacapture.js"></script>
</body>
