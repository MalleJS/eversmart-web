<?php if( !empty($mpan) ) { ?>
    
    <label class="control-label form-style-label" for="multiple_mpan_select" id="multi_mpan">Select MPAN:</label>

    <div class="form-style-field-lg">
        <select name="selected_mpan" id="selected_mpan" style="font-size:1rem;" class="custom-select form-control">
            <option value="default">(Please Select MPAN)</option>
            <?php foreach($mpan as $key => $MPANCode ) { ?>
                <option value="<?php echo $MPANCode?>"><?php echo $MPANCode; ?></option>
            <?php } ?>
        </select>
    </div>

<?php } else { ?>
    
    <p>No mpans for selected address.</p>
    
<?php } ?>