<div class="container">

							<div class="row">

								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4">Please choose from following !</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row">
								<span class="back_step"><a href="javascript:void(0)" id="back_postcode" class="material-icons" style="top:-1px;"><img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card quotefor" data-quotefor="electricity">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/elec.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Electricity</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Electricity.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Electricity----------------->

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card quotefor" data-quotefor="gas">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/gas.svg" alt=""/></span>
														</div>
														<div class="content">
															<span class="gas_box_text dark_heading">
															Gas</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Gas.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Gas----------------->

										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card quotefor" data-quotefor="both">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Both.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Both</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Both.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Both----------------->

									</div><!-------------------End Row------------->


								</div><!------------------End switch_box_main----------------->
							</div><!------------------End Container----------------->

						</div><!------------------End Postcode------------->
