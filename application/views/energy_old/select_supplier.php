<?php //debug($supplier_data,1); ?>
<div class="container">

							<div class="row">

								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">My current Energy Supplier and Tariff Is?</h4>
								</div>
							</div>

								<div class="switch_box_main">
								<div class="row justify-content-center">
								<span class="back_step"><a href="javascript:void(0)" id="back_known_supplier" class="material-icons" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a></span>
										<div class="col-md-4" id="elect_supplier">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/elec.svg" alt=""/></span>
												</div>
												<div class="content">
													<span class="gas_box_text" id="gas_box_text_red">
													<a href="#" target="_blank">Electricity</a>
													</span>
												</div>
									<span class="usgage_box">
										<form>
										<div class="md-form">
												<select class="custom-select usage_select" id="elec_selected_supplier">
													<option value="0">Select Supplier</option>
                          <?php
                              if( !empty($supplier_data) )
                              {
                                for( $m=0; $m<count($supplier_data);$m++ ){ ?>
                                  <option data-supplierid="<?php echo $supplier_data[$m]['supplierId']; ?>" value="<?php echo $supplier_data[$m]['supplierName']; ?>"><?php echo $supplier_data[$m]['supplierName']; ?></option>
                              <?php  }
                            }else {
                              	echo '<option value="0">No data available</option>';
                            }
                          ?>
												</select>
										</div>
										<div class="md-form">
												<select class="custom-select usage_select" id="elec_tariff_list">
												</select>
										</div>
										</form>
									</span>


											</div>
										</div><!----------------End First Box--------------------->

							<div class="col-md-4" id="gas_supplier">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/gas.svg" alt=""/></span>
									</div>
									<div class="content">

										<span class="gas_box_text" id="gas_box_text_red">
                                        <a href="javascript:void(0)" target="_blank">Gas</a></span>
									</div>

									<span class="usgage_box">
										<form>
										<div class="md-form">
												<select class="custom-select usage_select" id="gas_selected_supplier">
													<option value="0">Select Supplier</option>
													<?php
															if( !empty($supplier_data) )
															{
																for( $m=0; $m<count($supplier_data);$m++ ){ ?>
																	<option value="<?php echo $supplier_data[$m]['supplierName']; ?>"><?php echo $supplier_data[$m]['supplierName']; ?></option>
															<?php  }
														}else {
																echo '<option value="0">No data available</option>';
														}
													?>
												</select>
										</div>
										<div class="md-form">
												<select class="custom-select usage_select"></select>
										</div>
										</form>
									</span>

								</div>
							</div><!-----------------End Second Box------------------>

								</div><!-------------------End Row---------------->

								<div class="row justify-content-center">
									<div class="col-sm-12">
									<span class="input-group-btn btnNew" id="continue_btn">
									<form action="javascript:void(0)">
										<button  style="background:#fff!important; font-weight:bold; color:#ee4392!important; padding:10px 40px !important; font-size:20px " id="supplier_submit" class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light red-btn" type="button">Continue</button>
                  </form>
										</span>
									</div>

								</div>
								</div>
							</div>
