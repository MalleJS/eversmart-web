<?php //debug($address_list,1); ?>
<div class="container">

							<div class="row">
								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">Please enter your address!</h4>

								</div>
							</div>
							<div class="row">
								<div class="col-md-8 mx-auto">
									<span class="back_step"><a href="javascript:void(0)" id="back_quotefor" class="material-icons" style="top:-61px;"><img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
											<form action="javascript:void(0)" id="address_form" class="switch-form" name="">
												<div class="row wow fadeInUp" data-wow-delay="400ms">
													<div class="form-group postCodeBox">

														<div class="input-group col-md-10 mx-auto">
														<span class="post_box_headingblue red-txt-energy">Please enter your address!</span>
														<div class="col-sm-12">
														<div class="col-sm-12">
															<!-- <input class="form-control paddingtb" id="postcode" name="postcode" required="" type="text" placeholder="Enter Address"> -->
															<select name="quotation_address" id="quotation_address" class="custom-select  form-control paddingtb">
																<option value="0">Please Select Address</option>
																<?php
																		if( !empty($address_list) ){
																			for( $j=0; $j<count($address_list); $j++ ){
																				// mpan
																				if( !empty($address_list[$j]['mpan']) ){
																					for($m=0; $m<count($address_list[$j]['mpan']); $m++){
																						if( count($address_list[$j]['mpan']) >= 1 ){
																							$mpan = implode(',',$address_list[$j]['mpan']);
																						}
																						else {
																							$mpan = $address_list[$j]['mpan'][$j];
																						}
																					}
																				}else{
																					$mpan = '';
																				}

																				// mprn
																				if( !empty($address_list[$j]['mprn']) ){
																					for($m=0; $m<count($address_list[$j]['mprn']); $m++){
																						if( count($address_list[$j]['mprn']) >= 1 ){
																							$mprn = implode(',',$address_list[$j]['mprn']);
																						}
																						else {
																							$mprn = $address_list[$j]['mprn'][$j];
																						}
																					}
																				}else{
																					$mprn = '';
																				}
																				?>
																					<option data-mpan="<?php echo $mpan ?>" data-mprn="<?php echo $mprn ?>" value="<?php echo $address_list[$j]['address'] ?>"> <?php echo $address_list[$j]['address'] ?> </option>
																		<?php	}
																		}
																?>
															</select>
															</div>
															<span class="input-group-btn btnNew" id="continue_btn" >
															<button class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
															</span>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
