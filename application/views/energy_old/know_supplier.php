<div class="container">

							<div class="row">

								<div class="col-md-8 mx-auto text-center">
									<h4 class="post_box_heading4">Do you know your current energy Supplier and Tariff?</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row justify-content-center">
									<span class="back_step"><a href="javascript:void(0)" id="" class="material-icons backfromprepay" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_energy" data-know_current_energy="no">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/no.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															NO</span>
														</div>

														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
									</div><!-------------End NO----------------->

										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_energy" data-know_current_energy="yes">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/yes.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
																Yes</span>
														</div>

														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
									</div><!-------------End yes----------------->

							</div>
							</div>
							</div>
