<div class="container">

							<div class="row">

								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">Please tell us your current energy cost</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<form action="energy-switch-quotation.html">
								<div class="row justify-content-center">
								<span class="back_step"><a href="javascript:void()" class="material-icons backknowusage" style="top:1px;">
									<img src="<?php echo base_url() ?>assets/images/red-back-button.svg"></a></span>
										<div class="col-md-4" id="elec_usage">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/elec.svg" alt=""/></span>
												</div>
												<div class="content">
													<span class="gas_box_text"  id="gas_box_text_red">
													<a href="#" target="_blank">Electricity</a>
													</span>
												</div>
									<span class="usgage_box">
										<div class="md-form">
											<input maxlength="5" id="enter_elec_usage" class="form-control" type="text" placeholder="Annual Usage">
												<select class="custom-select elec_usage_select" style="display:none;">
                          <option value="0">Please Select Interval </option>
													<option value="year" selected>Per Year</option>
													<option value="month">Per Month</option>
													<option value="quater">Per Quater</option>
													<option value="week">Per Week</option>
												</select>
										</div>
										<div id="error_msg"></div>

									</span>


											</div>
										</div><!----------------End First Box--------------------->

							<div class="col-md-4" id="gas_usage">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/gas.svg" alt=""/></span>
									</div>
									<div class="content">

										<span class="gas_box_text"  id="gas_box_text_red">
                                        <a href="#" target="_blank">Gas</a></span>
									</div>

									<span class="usgage_box">

										<div class="md-form">
											<input placeholder="Enter Amount" id="form5" class="form-control" type="text">
												<select class="custom-select usage_select">
													<option selected>Per Month</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
												</select>
										</div>

									</span>

								</div>
							</div><!-----------------End Second Box------------------>

								</div><!-------------------End Row---------------->

								<div class="row justify-content-center">
									<div class="col-sm-12">
									<span class="input-group-btn btnNew" id="continue_btn">
										<button id="enter_usage_form" style="background:#fff!important; color:#62c6f2!important; padding:10px 40px !important; font-size:20px background:#fff!important; font-weight:bold; color:#ee4392!important; padding:10px 40px !important; font-size:20px" class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="button">Continue</button>
										</span>
									</div>

								</div>
								</form>
								</div>
							</div>
