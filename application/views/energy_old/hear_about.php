<div class="container">

							<div class="row">

								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">How did you hear about us?</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row">
								<span class="back_step"><a href="javascript:void(0)" id="back_known" class="material-icons" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-3 hear_about_us" data-hearabout="google">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/google-glass-logo.svg" alt=""/></span>
												</div>
												<div class="content">
													<span class="gas_box_text">
													Google
													</span>
												</div>
											</div>
										</div>

							<div class="col-md-3 hear_about_us" data-hearabout="TV">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_tv_black_24px.svg" alt=""/></span>
									</div>
									<div class="content">

										<span class="gas_box_text">
                                        TV</span>
									</div>
								</div>
							</div>
							<div class="col-md-3 hear_about_us" data-hearabout="Radio">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_radio_black_24px.svg" alt=""/></span>
									</div>
									<div class="content">
										<span class="gas_box_text">Radio</span>
									</div>
								</div>
							</div>

							<div class="col-md-3 hear_about_us" data-hearabout="Facebook">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/facebook-logo.svg" alt=""/></span>
									</div>
									<div class="content">
										<span class="gas_box_text">Facebook</span>
									</div>
								</div>
							</div>


								</div>

								<div class="row justify-content-center">

										<div class="col-md-3 hear_about_us" data-hearabout="Comparison Site">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_compare_arrows_black_24px.svg" alt=""/></span>
												</div>
												<div class="content">
													<span class="gas_box_text">
													Price Comparison Site
													</span>
												</div>
											</div>
										</div>

							<div class="col-md-3 hear_about_us" data-hearabout="Referral">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Referral.svg" alt=""/></span>
									</div>
									<div class="content">

										<span class="gas_box_text">
                                        Referral</span>
									</div>
								</div>
							</div>
							<div class="col-md-3 hear_about_us" data-hearabout="Others">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_help_black_24px.svg" alt=""/></span>
									</div>
									<div class="content">
										<span class="gas_box_text">Others</span>
									</div>
								</div>
							</div>



								</div>
								</div>
							</div>
