<div class="container">

							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<h2>Your Quotation Summary</h2>

								</div>
							</div>
							<div class="row">
								<div class="col-md-5 mx-auto">

									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
											<div class="card-tariff text-center">
												<div class="switch-me text-center">
													<h3>Switching to us will cost you</h3>
													<span class="price_switch">£25.75</span>
													<span class="price_switch_plan">Per Month</span>
													<a style="background:green!important;padding: 10px 64px;font-size: 16px;" class="btn btn-primary btn-round btn-info" href="javascript:void(0)" data-toggle="modal" data-target="#tariffInfoModal">Switch Me!</a>
												</div>



												<h4 class="category">Goodbye standing charge</h4>
												<ul>
													<li>£308.91 /year</li>
													<li>12 months fixed rate</li>
													<li>No exit fees</li>
													<li>No minimum term</li>
												</ul>

												<button type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Tariff Information
												</button>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
