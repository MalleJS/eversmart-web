<div class="container">

							<div class="row">

								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4">Please choose from following !</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row">
								<span class="back_step"><a href="energy-switch-one.html" class="material-icons" style="top:-1px;"><img src="images/back.svg"/></a> </span>
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="energy-switch-three.html" class="gas_card">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="img/switchicon/elec.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Electricity</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Electricity.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Electricity----------------->

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="energy-switch-twelve.html" class="gas_card">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="img/switchicon/gas.svg" alt=""/></span>
														</div>
														<div class="content">
															<span class="gas_box_text dark_heading">
															Gas</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Gas.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Gas----------------->

										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="energy-switch-twelve.html" class="gas_card">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="img/switchicon/Both.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Both</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Both.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Both----------------->

									</div><!-------------------End Row------------->


								</div><!------------------End switch_box_main----------------->
							</div><!------------------End Container----------------->

						</div><!------------------End Postcode------------->
