<?php //debug( $this->session->userdata('login_data'),1 ); ?>
<!DOCTYPE html>
<html lang="en">

<head> 
    <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Eversmart | <?php if( !empty($title) ){ echo $title; }else{ echo 'Sign up'; } ?></title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) --> 
	<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

		   <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url() ?>dashboard/css/dashboard.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
 
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

	<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

<script>
  window.intercomSettings = {
    app_id: "qjs70gbf"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qjs70gbf';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>


<!-- <script type="text/javascript">

function movetoNext(current, nextFieldID) {
if (current.value.length >= current.maxLength) {
document.getElementById(nextFieldID).focus();
}
}
</script> -->
<style>



.loading-page {
margin:20% auto;
width: 100px
 }

 .loader {
  border: 8px solid #fda1b8;
  border-radius: 50%;
  border-top: 8px solid #f64d76;
  width: 90px;
  height: 90px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


</style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

    <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
<div class="loading-page">
    <div class="loader"></div>
</div>


<div class="cd-panel cd-panel--from-left js-cd-panel-left">

    <header class="cd-panel__header">
   <p class="mobilementitles">Dashboard</p>
      <a href="#0" class="cd-panel__close js-cd-close"></a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">

          <nav id="menuslideoutjs" class="menu slideout-menu">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php if( $this->session->userdata('login_data')['signup_type'] != 3 ) { // Only show these if you are not a Dyball customer ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/dashboard">Overview</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/referral">Refer a friend</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/billing">Billing</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/meter_reading">Meter Reading</a></li>
                <!--<li><a href="<?php //echo base_url(); ?>index.php/user/usage">Usage</a></li>-->
                <li><a href="<?php echo base_url(); ?>index.php/user/referral">Messages</a></li>
                <!--<li><a href="<?php //echo base_url(); ?>index.php/user/moving_home">Moving Home</a></li>-->
            <?php } else { ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>
        </ul>
      </section>
    </nav>

      </div> <!-- cd-panel__content -->


   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->



<div class="cd-panel cd-panel--from-right js-cd-panel-main">
   <header class="cd-panel__header">
      <p class="mobilementitles">Menu</p>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
            <li><a id="logout_user" onclick="logout_user();" href="#">logout</a></li>
        </ul>
      </section>
    </nav>
      </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->
</div> <!-- cd-panel -->



	<div class="cd-main-content">
	 <div class="main-inner" id="main_content" style="display:none" >
     <div class="main-rounded-red-header">
		<div class="red-rounded-waves" id="dashboard_main_bg">
		<div class="topbar" id="red_top">

			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
					<!-- <p class="accnum">Account Number: </p>

					<span class="product-code-no-d">
									<?php
										/*
										if( $this->session->has_userdata('login_data') ){
											 if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
												echo $this->session->userdata('login_data')['account_number'];
												}
												elseif($this->session->userdata('login_data')['signup_type'] == 3)
												{
													echo $this->session->userdata('login_data')['account_number'];
												}
										}else{
											echo '-';
										}
										*/
									?>
					</span> -->
					</div>

					<div class="mobileonly usermobilemenu toggle-button-menu-right cd-btn js-cd-panel-trigger" data-panel="main"><i class="fa fa-user" aria-hidden="true"></i></div>

                    <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                        <div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="left"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> </button></div>
                    <?php } ?>


           
    <?php $this->load->view('layout/menu'); ?>


				</div>
			</div>
			<!--- End container-------->
			<!--- End container-------->

		</div>

		<!---end top bar bar----------->


<!----------------start postcode-------------------->


<!----------------End postcode--------------------->
		</div>
	</div>


		<!--------------------Start breadcrumbs Care Section------------>

		<section class="termandconditions">
				<div class="container-fluid">
					<div class="row amend">
					


						<div class="col-sm-12 col-md-12 col-lg-8 dashboard-panel-right">
						  <!---<div class="alert alert-warning">
								<strong>Notification:</strong> Your account is not activated with Junifer. Please activate your account to see your bills. You should have an activation email, if not, <a href="#">click here</a> to resend activation email.
							  </div>-->
						<div class="large-screen-fix dashboardss">
							<div class="row justify-content-center">
                <?php //debug( $this->session->userdata('login_data') );
                  //echo  $this->session->userdata('login_data')['id'];
                 ?>
             
									<?php $this->load->view($content); ?>
							</div>
							</div>
						</div>

						</div>

					</div>

				</div>

        </section>

<!--------------------End breadcrumbs Care Section------------->


<!----------------Start Footer-------------------------->


  <?php $this->load->view('layout/common_footer'); ?>
                  </div>

    	<!----end footer------------>

</div>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aos.js"></script>
    <script src="<?php echo base_url() ?>dashboard/js/jquery.easy_number_animate.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/mdb.min.js"></script>
    <!-- common JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/common.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>


    <script type="text/javascript">

    function logout_user() {

        $.ajax({
            url: '<?php echo base_url() ?>index.php/user/logout',
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.error == '0') {
                    window.location.href = '<?php echo base_url() ?>index.php/user/login';
                }
            }
        });

    }
     
    </script>


</div>
</body>
</html>
