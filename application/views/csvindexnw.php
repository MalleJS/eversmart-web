<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Customer Import</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" rel="stylesheet" />
 
        <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/jquery-3.2.1.min.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
   
      <!-- common JavaScript -->
<style>
    .container {
    max-width: 100%;
}
caption {
    padding-top: 1.75rem;
    padding-bottom: 0.75rem;
    color: #6c757d;
    text-align: left;
    caption-side: bottom;
    display:none;
}
</style> 
    </head>

    <body>

 
        <div class="container" style="margin-top:50px">    
             <br>
 
             <?php if (isset($error)): ?>
                <div class="alert alert-error"><?php echo $error; ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
            
            
            <h2>Chris's Incredible CSV Importer</h2> 
                <form method="post" action="<?php echo base_url() ?>csv_nw/importcsvnw" enctype="multipart/form-data">
                    <input type="file" name="userfile" ><br><br>
                    <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
                </form>
 
            <br><br>
            <table class="table table-striped table-hover table-bordered">
                <caption>Christopher J. Callaghan</caption>
                <thead>
                    <tr>
                        <th>ACCOUNT NUMBER</th>
                        <th>MPAN</th>
                        <th>MPRN</th>
                        <th>ELEC START DATE</th>
                        <th>CUSTOMER_NAME</th>
                        <th>CONTACT_ADDRESS_1</th>
                        <th>CONTACT_POST_CODE</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($addressbook == FALSE): ?>
                        <tr><td colspan="50">There is currently No Data</td></tr>
                    <?php else: ?>
                        <?php foreach ($addressbook as $row): ?>
                            <tr>
                                <td><?php echo $row['ACCOUNT_NUMBER']; ?></td>
                                <td><?php echo $row['MPAN']; ?></td>
                                <td><?php echo $row['MPRN']; ?></td>
                                <td><?php echo $row['ELEC_START_DATE']; ?></td>
                                <td><?php echo $row['CUSTOMER_NAME']; ?></td>
                                <td><?php echo $row['CONTACT_ADDRESS_1']; ?></td>
                                <td><?php echo $row['CONTACT_POST_CODE']; ?></td>
                                <td><?php echo $row['EMAIL']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
 
 
            <hr>
            <footer>
                <p>&copy;Eversmart 2018</p>
            </footer>
 
        </div>
 
 
 
    </body>
</html>