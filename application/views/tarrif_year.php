<style type="text/css">/**
* The CSS shown here will not be introduced in the Quickstart guide, but shows
* how you can use CSS to style your Element's container.
*/

#tab-data-box {
    width: 100%;
}
video.yearlyvideo {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
    display: block;
    margin-top: 30px;
    pointer-events: visible;
}
.StripeElement {background-color: white;padding:20px 12px 9px;border-bottom: 1px solid #ced4da;}
.StripeElement--focus {box-shadow: 0 1px 3px 0 #cfd7df;}

#example1-name--focus {box-shadow: 0 1px 3px 0 #cfd7df;}
.StripeElement--invalid {border-color: #fa755a;}
.StripeElement--webkit-autofill {background-color: #fefde5 !important;}
.card-element{ padding-top:20px; color:#ced4da; font-size:25px; margin-bottom:40px; font-weight:500; }
.gas_card{padding:40px 80px;}
.payment-details {display: none; position: relative; width: 100%; margin-bottom: 30px;border-radius: 10px; color: rgba(0,0,0, 0.87); background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); padding:30px 20px;}
.ElementsApp, .ElementsApp .InputElement{font-size:17px;}
.pay-detail{font-size:18px; line-height:23px; float:left; width:100%; margin:5px 0px; color:#000; padding:20px;}
.pay-price {font-size: 22px;font-weight: 500;width: 100%;float: left;color: #ea495c;line-height: 35px;padding: 0 20px 20px;}
.pay-pay-now {
    font-size: 51px;
    font-weight: 500;
    width: 100%;
    float: left;
    color: #12375c;
    line-height: 40px;
    padding: 20px;
    pointer-events: none;
    margin-bottom: 10px;
}#example1-name{margin-bottom: 10px; border-bottom: 1px solid #ced4da!important; border:none;}
.StripeElement{float:left; width:100%;}

.process-tab-cs{padding-top:8px;}
.energy-process-icon{width:90px; height:auto!important;}
#active-process{color:#000;}
.days-process{color:#1cb81c; font-size:16px; font-weight:bold; width:33%; text-align:center; float:left; margin-bottom:10px; clear: both;}
.dash-over-text-green{color:#1cb81c; font-size:16px; font-weight:bold; width:100%; text-align:center; float:left; }
.dash-over-text{padding:0px 0px 15px 0px;}
.energy-main-process{padding:50px 0px; float:left; width:100%;}
.energy-process-icon{ background:#fff; padding:15px;}
.checkmark{width: 56px;height: 56px;border-radius: 50%;display: block;stroke-width: 2;stroke: #fff;stroke-miterlimit: 10;margin: 60px auto; position: relative;}
.User-number-eversmart{font-size:45px; color:#fc5462; font-weight:400;float: left;width: 100%;text-align: center; margin: 30px 0px; }
.dont-have{width:100%; float:left; text-align:center; margin:10px 0px 30px; 0px;}
.dont-have a{color:#3c99e7; font-size:14px; font-weight:400;}
.main_card_box .gas_card{width:100%}
.fre-ques{float:left;font-size:20px; width:100%; margin:23px 0px 10px; color:#666; font-weight:400;}
.accordion{float:left; width:100%;}
.pay-form-db-tab{float:left; width:100%;}
.StripeElement {margin-bottom:10px;}
#example1-name::placeholder{opacity: 1;color: #aab7c4; font-weight:400; font-family: 'Quicksand',! sans-serif important;}
#card-element{position:relative;}
#card-type-icon1{position:absolute; right: 9px;top:22px;z-index:1;}
#card-type-icon1.fa-cc-visa{font-size:18px;}
.secure-pay-strip{padding: 170px 0px 10px!important;}
.left-tab-side ul li a{font-size:19px;}
.__PrivateStripeElement-input::placeholder{font-family: 'Quicksand',! sans-serif important;}
.InputElement::placeholder{font-family: 'Quicksand',! sans-serif important;} 
img.img-fluid.stripe {padding-bottom: 25px;}
.card.card-raised2.card-form-horizontal.wow.fadeInUp.pay {margin-bottom: 0; border: none; box-shadow: none;}
button.red-btn.btn.btn-md.btn-eversmart.btn-round.weight-300.text-center.wow.fadeInUp.switchButton.waves-effect.waves-light.pay {font-size: 1.3rem;}
.InputElement{border-top:none;border-left:none;border-right:none;}
.error_msg {
    top: 20px;
    position: relative;
    display: block;
    color: red;
}
#exTab1 {
    text-align: center;
}
.amountyearly {
    display: block;
    font-size: 95px;
    color: #fcb550;
    font-weight: 800;
    text-align: center;
    margin: 0px 0 25px;
    padding-top: 120px;
}
section.termandconditions {
    background: #ea495c url(/assets/img/balalaa.png) bottom;
    background-repeat: no-repeat;
}
.back_step .material-icons {
    position: absolute;
    left: -75px;
    top: 50px;
}
ul#yearly_selectors {
    text-align: center;
    margin-top: 25px;
    margin-bottom: 20px;
}

.termstext {
    display: block;
    position: relative;
    font-size: 22px;
    line-height: 1.3;
    text-align: center;
    margin-top: 20px;
    color: #706e6e;
    font-weight: 800;
}
.gas_card {
    padding: 40px 80px;
    pointer-events: none;
}
ul#yearly_selectors {
    text-align: center;
    margin-top: 25px;
    margin-bottom: 20px;
    pointer-events: visible;
}


</style>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
		  <?php //print_r($signup_info); 

		  ?><br> 
        </div>
    </div>
</div>
        


	
	<div class="col-md-12" style="padding: 0;margin-top: 50px;">
		<div class="gas_card">



		<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
				<div class="content pd0">

					<div class="tabbable tabs-left row">
						
						<div class="col-sm-12 pd0">
							<div class="tab-content " id="tab-data-box">
							<span class="pay-pay-now text-center">Loyal Family Saver Club</span>

<div class="amountyearly">£<?php echo $year_price ?> </div>
<div class="blockpos"><span class="pay-pay-now text-center">a year</span></div>

<div class="termstext">* Earn 12% interest per annum on your credit balance!
*t&c’s apply<br>
* Earn interest on your refer a friend credit balance
</div>

<div class="yearlyvideo">
<video class="yearlyvideo"  autoplay loop>
<source src="<?php echo base_url(); ?>assets/vid/familytariffvideofinal.mp4" type="video/mp4">
</video> 
</div>


 <div id="exTab1" class="container">



<ul id="yearly_selectors"  class="nav nav-pills">
	<li><a href="#1a" data-toggle="tab" style="border-radius: 30px 0 0 30px;">Tariff</a></li>
	<li><a href="#2a" id="yearly_tariff_toggle" class="active show" data-toggle="tab">Benefits</a></li>
	<li><a href="#3a" data-toggle="tab" style="border-radius: 0 30px 30px 0;">Important</a></li>
</ul>


<div class="tab-content clearfix">
	<div class="tab-pane" id="1a">
		<div id="yearly_tariff_breakdown" class="card">
			<h3><?=ucwords(strtolower($yearly_tariff_lookup[0]['productname']));?></h3>
			<?php if( !empty($yearly_tariff_lookup[0]['electricity_dur']) ) { ?> <li>Electricity unit rate – <?=number_format($yearly_tariff_lookup[0]['electricity_dur']*100,1);?> pence per kWh</li><?php } ?>
			<?php if( !empty($yearly_tariff_lookup[0]['gas_ur']) ) { ?> <li>Gas unit rate – <?=number_format($yearly_tariff_lookup[0]['gas_ur']*100,1);?> pence per kWh</li><?php } ?>
			<?php if( !empty($yearly_tariff_lookup[0]['electricity_sc']) ) { ?> <li>Electricity standing charge – <?=number_format($yearly_tariff_lookup[0]['electricity_sc']*100,1);?> pence per day</li><?php } ?>
			<?php if( !empty($yearly_tariff_lookup[0]['gas_sc']) ) { ?> <li>Gas standing charge – <?=number_format($yearly_tariff_lookup[0]['gas_sc']*100,1);?> pence per day</li><?php } ?>
			<li>Earn 12% interest per annum on your credit balance</li>
			<li>Earn interest on your refer a friend credit balance</li>
		</div>
	</div>
	<div class="tab-pane active" id="2a">
		<div class="card">
			<h3>No hard feelings!</h3>
			<p>We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! However, because we've comitted to a years worth of gas and electricity, there will be an exit fee. Please see T&Cs for more info.</p>
			<h3>You’ll get a free smart meter</h3>
			<p>Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment.</p>
			<h3>Here when you need us</h3>
			<p>Our friendly staff are based in the UK and available 7 days a week if you need help or have a problem. You can contact us by phone, email or social media – whatever works for you!</p>
			<h3>Switching is smooth and seamless!</h3>
			<p>There’ll be no interruption to your gas & electricity supply during the switch.</p>
		</div>
	</div>
	<div class="tab-pane" id="3a">
		<div class="card">
			<h3>Don’t worry – we'll do all the hard work for you!</h3>
			<p>We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!</p>
			<h3>Just so you know..</h3>
			<p>The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21.</p>
			<p><a href="<?php echo base_url(); ?>index.php/Terms" target="_blank">Read our Terms & Conditions here</a></p>
		</div>
	</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>


	<div class="col-md-12" style="padding: 0;margin-top: 50px;">
		<div class="gas_card">



		<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
				<div class="content pd0">

					<div class="tabbable tabs-left row">
						
						<div class="col-sm-12 pd0">
							<div class="tab-content " id="tab-data-box">
							<span class="pay-pay-now text-center">Your Details</span>

<div class="yourdetail">
<p><b>Name: </b> <?php echo $signup_info['customer_name']; ?> </p>
<p><b>Address:  </b> <?php echo $signup_info['contact_address_1']; ?></p> 
<p><b>City:  </b> <?php echo $signup_info['contact_address_2']; ?></p> 
<p><b>Postcode:</b>   <?php echo $signup_info['contact_address_3']; ?></p> 
<p><b>Email:  </b> <?php echo $signup_info['email']; ?></p> 
<p><b>Phone:  </b> 0<?php echo $signup_info['telephone']; ?></p>      
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="row allowpoint"> 
	
	<div class="col-md-12">
		<div class="gas_card" style="pointer-events: visible;">

				<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
				<div class="content pd0">

					<div class="tabbable tabs-left row">
						
						<div class="col-sm-12 pd0">
							<div class="tab-content " id="tab-data-box">

								<div id="billing_error_msg"></div>

					

							<div class="tab-pane active" id="b">
							<span class="pay-pay-now text-center">Payment Details</span>

						
							
<input type="checkbox" name="toggle_new_card_address" id="toggle_new_card_address" style="display:none" />
								<span class="pay-form-db-tab">
									<form action="" method="post" id="current-newjunifer" onsubmit="return false">
										<div class="row">

											<div class="col-lg-12  col-sm-12 col-md-12">

											<div style="position:relative">
												<div id="card-type-icon1"  style="display: none;">
													<i class="fa fa-cc-visa" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
													<i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>

												</div>
												<div id="card-element" class="StripeElement"></div>
											</div>

											<input type="hidden" id="amount_pay" value="<?php echo $year_price ; ?>">
											<input type="hidden" id="user_id" value="<?php // echo $id ; ?>">
											<input type="hidden" id="junifer_account_id" value="<?php // echo $junifer_id; ?>">
											<input type="hidden" id="existing_line1" value=" <?php echo $signup_info['contact_address_1']; ?>">
											<input type="hidden" id="existing_postcode" value=" <?php echo $signup_info['contact_address_3']; ?>">
											<input type="hidden" id="email" value="<?php echo $signup_info['email']; ?>">
											<input type="hidden" id="customer_id" value="<?php // echo $junifer_id; ?>">
											<input class="StripeElement ElementsApp InputElement" id="example1-name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" required="" autocomplete="name" novalidate>

												<?php // echo print_r($this->session->userdata('login_data'),1); ?>

											<div id="card-dd_expire"></div>

											<div id="card-dd_ccv"></div>

											<!-- Used to display form errors. -->
											<div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>

											<div class="error_msg"></div>
       										 <div class="signup_msg" style="clear:both"></div>

											</div>

											<div class="col-lg-12  col-sm-12 col-md-12 text-center">
											<button type="submit" id="stripebtn" style="margin-top:45px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">Submit Payment</button>
											</div>

										</div>

									</form>
							</span>
							</div>
							
							</div>
						</div>
						<span class="pay-detail text-center">
							Security is one of the biggest considerations in everything we do. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. 
						</span>
					</div>
					
					
				</div>
				</div>
			</div>
		
	</div>

	<div class="col-md-12" id="thank_you">
		<div class="payment-details" >
			<div class="col-md-10 col-sm-12 mx-auto ">
				<div class="gas_box_icon gap_icon_dashboard">
					<span class="progress-bar-step" id="swith-process-bar">
					<svg class="checkmark" xmlns="" viewBox="0 0 52 52">
					<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
					<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
					</svg>
					<div class="content" style="padding:0px 0px 30px">
					<div class="col-md-10 mx-auto">
						<span class="registration-page-heading-energy-H">										
						 <span id="Transaction_status"></span> 
						 <span id="Transaction"></span> 
						</span>
						<span class="registration-page-energy-d confirm-pay" style="padding-top:0px!important">								
						Please check your emails 
						</span>
						<!-- <span class="User-number-eversmart">								
							+44-123-456-7890
							</span>
							<span class="dont-have">
							<a href="#">Don't have access to this number?</a>
							</span>	 -->
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-------------- Start WebToCase  ------------>

<?php $this->load->view('dashboard/webtocase_form'); ?>

<!-------------- End WebToCase  ------------>


	<script src="https://js.stripe.com/v3/"></script>    