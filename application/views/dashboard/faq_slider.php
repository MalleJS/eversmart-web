<?php $page = array_slice(explode('/', rtrim($_SERVER['REQUEST_URI'], '/')), -1)[0]; ?>
<!-- // SlideOut -->
<div id="slideOut" <?=($page == 'dashboard'?'style="top: 180px;"':'');?>>
    <!--   // Tab -->
    <div class="slideOutTab">
        <div>
            <img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/slidericonx.png" alt="">
        </div>
    </div>
    <div class="modal-content" id="faqslideout">
        <div class="modal-header">
            <button type="button" class="close" id="closy" data-dismiss="slideOut" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">FAQs</h4>
        </div>
        <div class="modal-body" id="nopadz" style="overflow-y: auto;max-height: 560px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div id="MainMenu">

                            <?php if($page == 'dashboard') { ?>

                                <div class="list-group panel">
                                    <a href="#demo4" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Overview</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Is there an Eversmart app? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                <p>Yes there is. <br><br>
                                                
                                                Click <a href="https://itunes.apple.com/gb/app/eversmart/id1261416062" target="_blank">here</a> for our iPhone app.<br><br>
                                                
                                                Click <a href="https://play.google.com/store/apps/details?id=com.paypoint.eversmart" target="_blank">here</a> for our Android app.<br></p>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I get a smart meter? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                            <p>If you have just switched to us, we will contact you to arrange a smart meter installation. We aim to do this within 21 days after signing up. <br><br>
                                            
                                            If you don’t currently have a smart meter and you would like one, please email <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> to arrange a free installation. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I submit a meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                You can submit a meter reading on your <a href="<?php echo base_url(); ?>index.php/user/meter_reading">Meter Reading</a> page. <br><br>
                                                
                                                If you are having trouble understanding your meter, <a href="https://www.eversmartenergy.co.uk/blog/how-to-submit-a-meter-reading/">see this handy guide</a>.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu4" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How can I make a payment? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu4">
                                            <div id="padz">
                                                If you have just received a bill, you can make a payment on your <a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a> page. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu5" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do payments work? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu5">
                                            <div id="padz">
                                                Payments are taken one month in advance, and are put into your account as credit. <br><br>
                                                
                                                If you don’t pay by Direct Debit, you will receive a bill by email every month which you will need to pay. <br><br>
                                                
                                                You can make a payment by clicking the "Top Up" button on your <a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a> page.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'referral') { ?>
    
                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Refer a friend</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I get free energy? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                Every time you refer a friend to Eversmart, you will both receive £50 credit in your account. <br><br>
                                                
                                                This money can be used to cover your energy bills, effectively giving you free energy!<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I redeem my rewards? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                You and your friend will both receive £50 credit when your friend’s switch is complete (the switching takes 21 days). <br><br>
                                                
                                                Your friend must sign up using your unique referral link in order to qualify for the £50 credit.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I refer my friends? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                You can refer your friends & family to Eversmart using your unique referral link. You can find your link on this page.<br><br>

                                                When your friend clicks on this link, they can sign up to Eversmart and we will know who referred them. You and your friend will each receive £50 in credit.<br><br>

                                                You can share your link via email, text or social media.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'account') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">My Account</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Where can I find my Eversmart account number? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                You can view your account number when you log in to your online account. <br><br>
                                                
                                                You can also find it on a recent bill or email. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Can I add another account holder to my account? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                At the moment we can’t add extra people to your online account – however this is a feature we are planning to add in the future.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'billing') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Billing</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How can I make a payment? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                If you have just received a bill, you can make a payment by selecting the <a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a> option. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Can I withdraw credit from my account? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                Before you try and withdraw credit from your account, please make sure you have submitted a recent meter reading. Also, remember that payments are taken one month in advance, so your account will usually be slightly in credit. <br><br>Finally, remember that your energy use will be higher in the winter and your bills are averaged out across the year – so you might find yourself with a large amount of credit built up during the summer.<br><br>

                                                If you still think there is too much credit in your account, you can email us at <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a> and ask for a refund.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How are estimated readings calculated? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                If we haven’t received a meter reading for a while, we have to use estimated readings to calculate your bill. We do this so that we can continue supplying you with energy.<br><br>

                                                Estimated readings are based your past energy use, as reported by your meters. We use your older meter readings to calculate how much energy you are likely to use in the future.<br><br>

                                                To keep your bills accurate, you should submit a meter a reading around once every 3 months. You can do this quickly & easily in the <a href="<?php echo base_url(); ?>index.php/user/meter_reading">Meter Reading</a> section of the portal.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'payments') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Payments</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do payments work? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                Payments are taken one month in advance, and are put into your account as credit. <br><br>
                                                
                                                If you don’t pay by Direct Debit, you will receive a bill by email every month which you will need to pay. <br><br>
                                                
                                                You can make a payment by clicking the <a href="<?php echo base_url(); ?>index.php/user/pay_dashboard/?pay=0">Top Up</a> button on this page.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Do you take payments in advance? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                Yes – your first payment will come out on the day that your switch is complete. <br><br>
                                                
                                                Taking payments in advance allows us to buy energy more cheaply and pass the savings on to you.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What should I do if my account is in debit? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                Your account may sometimes go slightly into debit, especially during the winter months when your energy use is higher.  <br><br>
                                                
                                                If you haven’t done so recently, you should submit a meter reading to make sure your bills are nice & accurate. <br><br>

                                                If you need to, you can top up your account with a one-time payment on this page.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu4" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What does credit or debit mean? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu4">
                                            <div id="padz">
                                                If you use less energy than you pay for, your account will end up in credit. <br><br>

                                                If you use more energy than your payment amount covers, your account will be in debit. <br><br>

                                                Remember that your energy use will vary during summer and winter, and we calculate your monthly amount to average out throughout the year. <br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'meter_reading') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Meter Reading</a>
                                    <div id="demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I submit a meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                You can submit a meter reading on this page. <br><br>
                                                
                                                If you are having trouble understanding your meter, <a href="https://www.eversmartenergy.co.uk/blog/how-to-submit-a-meter-reading/">see this handy guide</a>.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">When should I submit a meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                We will send you an email reminder when it’s time to submit a meter reading. <br><br>
                                                
                                                However you are free to submit readings as often as you like.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What should I do if I submitted the wrong meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                Please <a href="<?php echo base_url(); ?>index.php/contact_us">contact us</a> as soon as possible so we can update your information.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu4" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What do I do if I can’t submit a meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu4">
                                            <div id="padz">
                                                If you can’t submit a reading yourself, a meter reader will visit your property and take a reading for you – usually twice a year. <br><br>
                                                
                                                If you have trouble submitting your own readings, you might benefit from getting a smart meter.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'usage') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Usage</a>
                                    <div id="demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How is my energy usage calculated? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                Your energy usage is calculated using your meter readings. <br><br>
                                                
                                                Be sure to submit a reading at least once every three months to keep your bills accurate. <br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'messages') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Messages</a>
                                    <div id="demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Can I download my message history? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                Yes you can! Simply click on the download button to the right of the message.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How can I contact Eversmart? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                If you can’t find the information that you are looking for on our <a href="<?php echo base_url(); ?>index.php/Helpfaqs">FAQ pages</a>, you can chat to one of our advisers by clicking on the chat button in the bottom right corner of this page.<br><br>

                                                Alternatively, you can find more ways to contact us <a href="<?php echo base_url(); ?>index.php/contact_us">here</a>.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if($page == 'moving_home') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Moving Home</a>
                                    <div id="demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What should I do if I’m moving out? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                You will need to close your account and provide a final set of meter readings. <br><br>
                                                
                                                If you are moving into a new home and you want to transfer your Eversmart account, simply fill in your details on this page.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What should I do if I’m moving into a new home? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                You will need to close your account at your old address and open a new account for your new home. <br><br>
                                                
                                                We will also need meter readings for both addresses on the day that you move. You can do all of this using the form on this page.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What should I do if I’m moving out of or into a home with prepayment meters? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                If you are moving out of or into a home with prepayment meters, we will need some additional information from you. Please call us on  <a href="tel:03301027901">0330 102 7901</a>.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php } else if($page == 'account_status') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Your new Account</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">When will I get my final bill from my old supplier? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                It usually takes around 2-3 weeks for your old supplier to send your final bill. <br><br>
                                                
                                                It is very important that you send us a meter reading first – once we have received your opening meter readings, we will share them with the old supplier and they can generate your final bill.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">What happens to credit or debit with my old supplier? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                If you owe money to your old supplier, you will need to pay it back when you receive your final bill. <br><br>
                                                
                                                If you were in credit with your old supplier, you should receive a refund from them.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu3" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Should I cancel my Direct Debit with my old supplier? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu3">
                                            <div id="padz">
                                                Don’t cancel your direct debit until you have settled your final bill with your old supplier. <br><br>
                                                
                                                Cancelling  your direct debit when you still owe money could delay your switch.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu4" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">When am I getting my smart meter? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu4">
                                            <div id="padz">
                                                We will contact you to arrange a time to come and fit your smart meter. We aim to do this within the first 21 days after signing up, but it may take longer depending on where you live.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu5" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">When should I give my first meter reading? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu5">
                                            <div id="padz">
                                                We will email you when it’s time to submit your first meter reading. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu6" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Why has a payment been taken prior to my account being open? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu6">
                                            <div id="padz">
                                                We usually take payments a month in advance. Doing this allows us to buy energy more cheaply and pass the savings on to you. <br><br>
                                                
                                                If you have any questions or concerns about payments & direct debits, please email <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a>. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu7" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How can I get in touch with Eversmart? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu7">
                                            <div id="padz">
                                                If you can’t find the information that you are looking for on our <a href="<?php echo base_url(); ?>index.php/Helpfaqs">FAQ pages</a>, you can chat to one of our advisers by clicking on the chat button in the bottom right corner of this page.<br><br>

                                                Alternatively, you can find more ways to contact us <a href="<?php echo base_url(); ?>index.php/contact_us">here</a>.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php } else if($page == 'login' || $_GET['utm_medium'] == 'email') { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Login</a>
                                    <div id="demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">I can't log in to my Eversmart Account. <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                You can reset your password by following <a href="<?php echo base_url(); ?>index.php/Reset_one/resetone">this link</a>. <br><br>
                                                
                                                If you’re still having problems, please email <a href="mailto:hello@eversmartenergy.co.uk">hello@eversmartenergy.co.uk</a>. <br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">Is my password secure? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                Your account password should be at least 8 characters long, with at least one uppercase letter, one lowercase letter, one number and one symbol. <br><br>
                                                
                                                We recommend that you don’t use words or phrases that would be easy for somebody else to guess. <br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php } else if($_GET['pay'] >= 0) { ?>

                                <div class="list-group panel">
                                    <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu" id="mainacc">Payments</a>
                                    <div id="#demo3">
                                        <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I pay off my full balance? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu1">
                                            <div id="padz">
                                                Your full balance is listed in the top left section of this page (under 'Balance'). <br><br>

                                                To pay this amount off, click the 'Pay Now' button below your full balance. <br><br> 
                                                
                                                At the bottom of the page, enter your payment details and then click the 'Submit Payment' button.<br><br>
                                            </div>
                                        </div>
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1" id="subacc">How do I pay a specific amount? <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse list-group-submenu" id="SubMenu2">
                                            <div id="padz">
                                                To pay off a specific amount of your balance, enter the amount you wish to pay in the top right section of this page (under 'Pay Other').<br><br>

                                                From here, click the 'Pay Now' button below the entered amount. <br><br> 
                                                
                                                At the bottom of the page, enter your payment details and then click the 'Submit Payment' button.<br><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="faq-contact-help">
                <a href="#">Not answered your question?</a>
            </div>
            <div class="faq-contact-icons">

                <div class="faq-icons faq-intercom" id="faq">
                    <figure class="effect-sadie">
                        <figcaption>
                            <a href="#" onclick="return false;" class="open_intercom">
                                <span class="switch_icon"><img class="faq-img" src="<?php echo base_url(); ?>assets/images/chat-icon.png" alt=""></span>
                            </a>
                        </figcaption>
                    </figure>
                </div>
                
                <div class="faq-icons" id="faq">
                    <figure class="effect-sadie">
                        <figcaption>
                            <a href="mailto:hello@eversmartenergy.co.uk">
                                <span class="switch_icon"><img class="faq-img" src="<?php echo base_url(); ?>assets/images/email-icon.png" alt=""></span>
                            </a>
                        </figcaption>
                    </figure>
                </div>

                <div class="faq-icons" id="faq">
                    <figure class="effect-sadie">
                        <figcaption>
                            <a href="tel:03301027901">
                                <span class="switch_icon"><img class="faq-img" src="<?php echo base_url(); ?>assets/images/call-icon.png" alt=""></span>
                            </a>
                        </figcaption>
                    </figure>
                </div>


            </div>
        </div>
    </div>
</div>

<script>

window.intercomSettings = {
    app_id: "qjs70gbf",
    custom_launcher_selector: ".open_intercom"
    <?php if(isset($this->session->userdata('login_data')['intercom_user_id'])){ ?>
        , user_id: "<?=$this->session->userdata('login_data')['intercom_user_id'];?>"
    <?php } ?>
    <?php if(isset($this->session->userdata('login_data')['intercom_id'])){ ?>
        , id: "<?=$this->session->userdata('login_data')['intercom_id'];?>"
    <?php } ?>
    <?php if(isset($this->session->userdata('login_data')['intercom_name'])){ ?>
        , name: "<?=$this->session->userdata('login_data')['intercom_name'];?>"
    <?php } ?>
    <?php if(isset($this->session->userdata('login_data')['intercom_email'])){ ?>
        , email: "<?=$this->session->userdata('login_data')['intercom_email'];?>"
    <?php } ?>
};
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qjs70gbf';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
