<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 main-content">
            <a href="javascript:void(0)" class="gas_card" style="margin-bottom:10px">
                                                    
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Overview/messages.svg" alt=""></span>
                    <span class="dash-over-text title">Messages</span>
                    <span class="dash-over-text account"> Your Messages</span>

                    <div class="table-responsive" style="padding: 0px 30px;">
                        <table class="table product-table msg-table">

                        <tr>
                            <th rowspan="2" style="width:9%;">Date</th>
                            <th></th>
                            <th rowspan="2" style="width:12%;">Subject</th>
                            <th colspan="2"></th>
                        </tr> 

                        <tr>
                        <th class="red_border"></th>
                        <th colspan="2" class="red_border"></th>
                        </tr>

                        <?php if( !empty($junifer_msgs) ){
                            
                            for( $r = 0; $r<count( $junifer_msgs['results'] ) ; $r++ ){ 
                            
                            $msgid = $junifer_msgs['results'][$r]['files'][0]['id'] ;
                            $date = $junifer_msgs['results'][$r]['createdDt'] ;

                            ?>
                        
                        <tr>
                            <td class="datesmsg" colspan="2"  style="width:45%">
                                <?php echo $date ; ?>
                            </td>

                            <td class="titlesmsg" colspan="2">
                                <?php echo $junifer_msgs['results'][$r]['type'] ; ?>
                            </td>

                            <td class="downloadPDFs" style="padding-bottom: 0!important;">
                                <a target="_blank" href="<?php echo base_url() ?>index.php/user/downloadmessage/<?php echo $msgid ?>" data-msgid="<?php echo $msgid ?>" class="downloadPDF">
                                <img src="<?php echo base_url() ?>dashboard/images/dashboard/Billing/download.svg" alt="">
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td class="table-gap" colspan="5"></td>
                        </tr>
                        
                        <?php }
                    
                        } 
                        else
                        { ?>
                            <span class="pay-y-cp-b-l" >
                                You currently have no history of messages on your account.
                            </span>
                        <?php
                        } ?>
                        </table>
                    </div>

                </div>

            </a>
        </div>
    </div>
</div>

