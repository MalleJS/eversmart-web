<style>
					#tariff-table tr td{width:50%;}
					.edit-icon{float:right; font-size:16px; color:#636363; margin-top:-30px; font-weight:400;}
					.edit-icon img{width:20px; height:auto; margin-top: -6px;}
					
					</style>
					
					<div class="col-sm-12">
							<div class="row">
								<div class="col-md-12 ">
								<a href="javascript:void(0)" class="gas_card">
																		
									<div class="gas_box_icon gap_icon_dashboard">
										
										<span class="dash-over-text" style="padding-top:0px;"><img style="width:34px; height:auto;" src="<?php echo base_url() ?>dashboard/images/dashboard/Overview/display.svg" alt="">&nbsp; Account Info</span>
										<span class="edit-icon" id="edit_account">
										<img src="<?php echo base_url() ?>dashboard/images/dashboard/edit-pen-icon.svg" alt=""> &nbsp;
										Edit</span>
  
									</div>
								
								</a>
								</div>

								</div>
								<div class="row">
								<div class="col-md-12 col-sm-12 col-lg-12 ">
									<form id="updateCustomerDetail">
									<div class="gas_card">
										<div id="main">
											<div class="row" id="main_div">
												<div class="col-md-12 col-sm-12 col-lg-6 ">											
													<div class="table-responsive">														
															<table class="table product-table usage-table-tariff" id="tariff-table">

																<tr>
																	<td>First Name:</td>
																	<td><?php echo ucfirst($user_info['forename']); ?>	</td>
																</tr>
																<tr>
																	<td>Last Name:</td>
																	<td><?php echo ucfirst($user_info['surname']); ?></td>
																</tr>
																<tr class="address">
																	<td>Address:</td>
																	<td><?php echo $user_info['first_line_address']; ?></td>
																	<?php  if(isset($signup_info['quotation_dplo_address'])) {?>
																	<td><?php echo $user_info['dplo_address']; ?></td>
																	<?php } ?> 
																	<td><?php echo $user_info['town_address']; ?></td>
																	<td><?php echo $user_info['city_address']; ?></td>
																	<td><?php echo $user_info['address_postcode']; ?></td>
																</tr>
															</table>

													</div>
																						
												</div>
												
												<div class="col-md-12 col-sm-12 col-lg-6 ">											
													<div class="table-responsive">														
															<table class="table product-table usage-table-tariff" id="tariff-table">

																<tr>
																	<td>Email:</td>
																	<td> <?php echo $user_info['email']; ?>	</td>
																</tr>
																<tr>
																	<td>Contact:</td>
																	<td><?php echo $user_info['phone1']; ?></td>
																</tr>
																<tr>
																	<td>Password:</td>
																	<td><a href="javascript:void(0)">Reset Password</a>	</td>
																</tr>
															</table>

													</div>
												
												
												</div>
										</div> <!-- main div -->
										<div id="edit_div" style="display: none">
											
												<div class="row">
													<div class="col-md-12 col-sm-12 col-lg-6 ">											
															<div class="table-responsive">

																<table class="table product-table usage-table-tariff" id="tariff-table">

																	<tr>
																		<td>First Name:</td>
																		<td><input type="text" name="first_name" value="<?php echo ucfirst($user_info['forename']); ?>"> 	</td>
																	</tr>
																	<tr>
																		<td>Last Name:</td>
																		<td><input type="text" name="last_name" value="<?php echo ucfirst($user_info['surname']); ?>"></td>
																	</tr>
																	<tr>
																		<td>Address:</td>
																		<td><input type="text" disabled="disabled" name="user_address" value="<?php echo $user_info['address_address1']; ?>"></td>
																	</tr>
																</table>

														</div>
																							
													</div>
													
													<div class="col-md-12 col-sm-12 col-lg-6 ">											
														<div class="table-responsive">														
																<table class="table product-table usage-table-tariff" id="tariff-table">

																	<tr>
																		<td>Email:</td>
																		<td><input type="email" name="user_email" value="<?php echo $user_info['email']; ?>">
																			<input type="hidden" name="customer_id" value="<?php echo $this->session->userdata('login_data')['customer_id']; ?>">
																		</td>
																	</tr>
																	<tr>
																		<td>Contact:</td>
																		<td><input type="text" name="user_phone1" value="<?php echo $user_info['phone1']; ?>"></td>
																	</tr>
																	<tr>
																		<td>Password:</td>
																		<td><a href="<?php echo base_url(); ?>/index.php/Reset_one/resetone">Reset Password</a>	</td>
																	</tr>
																</table>

														</div>
													
													
													</div>
											</div>
										</div>
									</div>

								<span class="text-center " style="float:left; width:100%;">
								<a href="javascript:void(0)" id="red-btn-default" class="btn btn-default waves-effect waves-light red-btn rounded-login-btn">Update</a>
								</span>
								<input type="submit" style="display: none" name="submit" value="submit_form">
							</div>
							</form>
							</div>
						</div>
						</div>
						
