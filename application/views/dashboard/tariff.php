<?php
if( !empty($this->session->userdata('login_data')) )
{
	$login_data = $this->session->userdata('login_data');
}
?>

<div class=" col-lg-12 col-md-12 col-sm-12">
								<a href="javascript:void(0)" class="gas_card">
									<div class="gas_box_icon gap_icon_dashboard">
										<span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/menu/tariff.svg" alt=""></span>
										<span class="dash-over-text">Tariff Information</span>

									</div>
								</a>
								</div>



								<div class="col-sm-12 col-md-6 col-lg-6 ">
								<div class="gas_card">
								<div class="table-responsive">
										<span class="tariff-h-c-p">Tariff</span>
										<table class="table product-table usage-table-tariff"id="tariff-table">

											<tr>
												<td>Tariff:</td>
												<td><?php if( $login_data['tariff_tariffName'] ) { echo $login_data['tariff_tariffName']; }else{ echo 'N/A'; } ?></td>
											</tr>
											<tr>
												<td>Exit Fees:</td>
												<td><?php if( $login_data['exitFeeElec'] ) { echo '&#163;' . $login_data['exitFeeElec']; }else{ echo 'N/A'; } ?></td>
											</tr>
											<tr>
												<td>Estimate annual cost:</td>
												<td><?php if( $login_data['tariff_newspend'] ) { echo '&#163;' . $login_data['tariff_newspend']; }else{ echo 'N/A'; } ?></td>
											</tr>

											</table>

								</div>
										<!---<span class="tariff-bill-down">
											<a href="#">Download Tariff Information</a>

										</span>-->
								</div>
								</div>

											<div class="col-sm-12 col-md-6 col-lg-6 ">
								<div class="gas_card">
								<div class="table-responsive">
									<span class="tariff-h-c-p">Electricity | Gas</span>
										<table class="table product-table usage-table-tariff " id="tariff-table">

											<tr>
												<td>Payment Method:</td>
												<td><?php 
												if( $login_data['billingMethod'] ) 
												{ 
													if($login_data['billingMethod'] == "directdebit")
													{
														echo "Direct Debit";
													}
													else
													{ 
														echo "Prepay";
													} 
												}
												?> </td>
											</tr>
											<tr>
												<td>Rate / Unit</td>
												<td><?php if( $login_data['tariff_unitRate1Elec'] ) { echo $login_data['tariff_unitRate1Elec']; }else{ echo 'N/A'; } ?> p/kwh</td>
											</tr>
											<tr>
												<td>Electricity Standing Charge</td>
												<td><?php if( $login_data['tariff_unitRate1Elec'] ) { echo $login_data['tariff_unitRate1Elec']; }else{ echo 'N/A'; } ?> p/kwh</td>
											</tr>

											</table>
								</div>

								</div>
								</div>
