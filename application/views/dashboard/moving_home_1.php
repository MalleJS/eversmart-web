<div class="col-sm-12">
							<div class="row">
								<div class="col-md-12 ">
								<a href="#" class="gas_card">
									<div class="gas_box_icon gap_icon_dashboard">
										<span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/menu/home.svg" alt=""></span>
										<span class="dash-over-text">Moving Home</span>

									</div>
								</a>
								</div>


								<div class="col-md-12">
								<div class="gas_card">
								<div class="col-md-9 col-sm-12 mx-auto ">
									<div class="gas_box_icon gap_icon_dashboard">
										<span class="progress-bar-step">
											<ul>
												<li class="mov-step-2-f" style="text-align:left">
												<a href="javascript:void(0)">
												<img src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
													<span class="process-tab-cs text-center" id="fix-process-center">Select <br>Date</span>
												</a>
												</li>
												<li class="text-center">
												<a href="javascript:void(0)"><img src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
												<span class="process-tab-cs">Reading</span>
												</a>
												</li>
												<li class="last-home-finish" style="text-align:right"><a href="javascrpt:void(0)"><img src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
												<span class="process-tab-cs" id="last-finish">Finish</span>
												</a></li>
											</ul>
										</span>
										<span class="dash-over-text fw500" style="padding:30px 0px 20px;">Move out date</span>
										<span class="tenant-move">When is the last day you're the owner or tenant of your current home</span>
										<div class=" col-md-10 col-lg-5 col-sm-12 mx-auto ">

										 <!-- <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group"><input style="border-bottom:1px solid #d8d8d8!important; padding-bottom:10px;" id="datepicker" class="move-date-picker form-control" placeholder="MM/DD/YY" data-type="datepicker" data-guid="404f9cc5-9370-989a-5a86-de55d23a1c3d" data-datepicker="true" role="input"><span class="input-group-append" role="right-icon"><button class="btn btn-outline-secondary border-left-0" type="button"><i class="gj-icon">event</i></button></span></div> -->
										 <input style="border-bottom:1px solid #d8d8d8!important; padding-bottom:10px; background:none;" id="datepicker" class="move-date-picker" placeholder="MM/DD/YY" data-date-end-date="0d" readonly />
										</div>
										</div>
									</div>
									<span class="get-started-btn-c text-right">
									<a href="javascript:void(0)" id="move_step_2" style="padding:8px 50px;">Next</a>


								</span></div>
								</div>


							</div>
						</div>
