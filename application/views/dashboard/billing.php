<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 main-content">
            <div class="gas_card">
                                                    
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Overview/billing.svg" alt=""></span>
                    <span class="dash-over-text title">Billing</span>
                    <span class="dash-over-text account"> Your Bills</span>

                    <?php
						if( !empty($results) )
						{ ?>

                    <div class="table-responsive" style="padding: 0px 30px;">
                        <table class="table product-table msg-table">

                        <tr>
                            <th rowspan="2" style="width:14%;">Bill Name</th>
                            <th></th>
                            <th rowspan="2" style="width:13%;">Bill Date</th>
                            <th colspan="2"></th>
                        </tr>

                        <tr>
                        <th class="red_border"></th>
                        <th colspan="2" class="red_border"></th>
                        </tr>

                        <?php
						
							for( $i=0; $i<count($results); $i++ )
							{
								$billid = $results[$i]['id'] ; ?>
								<tr>
									<td class="datesmsg bill-date" colspan="2" style="width:45%">
										<?php echo date("F Y", strtotime($results[$i]['createdDttm']) ); ?>
									</td>
									
									<td class="titlesmsg bill-title" colspan="2">
										<?php echo date("Y-m-d", strtotime($results[$i]['createdDttm']) ); ?>
									</td>
									
									<td class="downloadPDFs bill-data" style="width:130px;padding-bottom: 0!important;">
										<a href="<?php echo base_url() ?>index.php/user/downloadbill/<?php echo $billid ?>" data-billid="<?php echo $billid ?>" target="_blank"  class="icon-b-c-p">
											<img src="<?php echo base_url() ?>dashboard/images/dashboard/Billing/show.svg" alt="">
										</a>
										<a href="<?php echo base_url() ?>index.php/user/downloadbill/<?php echo $billid ?>" data-billid="<?php echo $billid ?>" target="_blank" class="icon-b-c-p">
											<img src="<?php echo base_url() ?>dashboard/images/dashboard/Billing/download.svg" alt="">
										</a>										
									</td>

									
								</tr>
								
								<tr>
									<td class="table-gap" colspan="5"></td>
								</tr>

						<?php	}
                    
                    }
                    else
                    { ?>
                        <span class="pay-y-cp-b-l" >
                            You currently have no history of bills on your account.
                        </span>
                    <?php
                    } ?>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

