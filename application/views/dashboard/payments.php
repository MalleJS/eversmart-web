<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 main-content">
            <div class="gas_card">
                                                    
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Overview/payments.svg" alt=""></span>
                    <span class="dash-over-text title">Payments</span>
                    <span class="dash-over-text account"> Your payments and interest</span>

                    <div class="col-md-<?=($tariff != 'LOYAL FAMILY SAVER'?'12':'6')?> floatL">

                        <span class="tariff-h-c-p text-center line">Current Balance</span>

                        <div class="pay-c-p text-center">
                            <?php if ($balance >= 0 ) { ?>

                                <span class="payment-price-pay">

                                    <?php echo "£".number_format($balance,2); ?>

                                    <span class="incd">in debit</span>
                                </span>
                                <span class="pay-y-cp-b">You are currently in debit. To make a payment, please click on the 'Pay Now' button below.</span>

                            <?php } else { ?>

                                <span class="payment-price-pay">
                                    <?php
                                        $balance = str_ireplace('-', '', $balance);
                                    echo "£".number_format($balance,2);
                                    ?>
                                    <span class="incd">in credit</span>
                                </span>
                                <span class="pay-y-cp-b-l">If you would like to top-up your account balance, you can make a one-off payment by clicking the button below</span>

                            <?php  } ?>

                            <span class="tariff-bill-down">
                            <?php if( $this->session->userdata('login_data')['signup_type'] == '1' || $this->session->userdata('login_data')['signup_type'] == '0' )
                            { ?>

                                <?php if ($balance <= 0 )
                                { ?>
                                    <a href="<?php echo base_url() ?>index.php/user/pay_dashboard/?pay=<?php echo $balance ?>">Pay Now</a>

                                <?php
                                }
                                else
                                { ?>
                                    <a href="<?php echo base_url() ?>index.php/user/pay_dashboard/?pay=0">Top Up</a>
                                <?php
                                } ?>

                            <?php } ?>

                            <?php if( $this->session->userdata('login_data')['signup_type'] == '3')
                            {?>
                                <a href="https://eversmartpayments.paypoint.com/energy/" target="_blank">Top Up</a>
                            <?php
                            } ?>

                            </span>

                        </div>
                    </div>

                    <?php if($monthly_interest){ ?>
                        <div class="col-md-6 floatL">
                                    
                                <span class="tariff-h-c-p text-center line">Total Interest Earned</span>
                                <div class="pay-c-p text-center">
                                    <span class="payment-price-pay">
                                        <?="£".number_format($accrued_interest,2);?>
                                        <?php if($go_live_date != '') { ?>
                                            <span class="incd">since <?=date('d/m/Y', strtotime($go_live_date));?></span>
                                        <?php } else { ?>
                                            <span class="incd">&nbsp;</span>
                                        <?php }?>
                                    </span>
                                    <span class="pay-y-cp-b-l" >By paying for your yearly usage up front, you have accrued  <?="£".number_format($accrued_interest,2);?> to be redeemed as <b>credit</b> at the end of your term</span>


                                    <span class="tariff-bill-down">
                                        <a href="#" id="monthly_breakdown_button">Monthly Breakdown</a>
                                    </span>

                                </div>

                        </div>
                    <?php } ?>


                    <span class="tariff-h-c-p text-center line">Your Payments</span>

                    <?php if( $this->session->userdata('login_data')['signup_type'] == '1' || $this->session->userdata('login_data')['signup_type'] == '0' )  
                    { ?>
                        
                        <div class="table-responsive" style="padding: 0px 30px;">
                            <?php if( !empty($payment_junifer) || (strtolower($tariff) == 'loyal family saver' && $tariff_newspend > 0) )
                            { ?>
                                <table class="table product-table msg-table">

                                <tr>
                                    <th rowspan="2" style="width:220px;">Payment Month</th>
                                    <th></th>
                                    <th rowspan="2" style="width:50px;">Amount</th>
                                    <th colspan="2"></th>
                                </tr>

                                <tr>
                                <th class="red_border"></th>
                                <th colspan="2" class="red_border"></th>
                                </tr>

                                    <?php if(strtolower($tariff) == 'loyal family saver' && $tariff_newspend > 0) { ?>

                                        <tr>
                                            <td class="datesmsg" colspan="2">
                                                <?php echo date("F Y", strtotime($monthly_interest) ); ?>
                                            </td>

                                            <td class="titlesmsg" colspan="2">
                                                £<?php echo $tariff_newspend; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="table-gap" colspan="5"></td>
                                        </tr>

                                    <?php } ?>


                                <?php
                                for( $i=0; $i<count($payment_junifer); $i++ ) 
                                { ?>
                            
                                    <tr>
                                        <td class="datesmsg" colspan="2">
                                            <?php echo date("F Y", strtotime($payment_junifer[$i]['postedDt']) ); ?>
                                        </td>

                                        <td class="titlesmsg" colspan="2">
                                            £<?php echo $payment_junifer[$i]['amount']; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="table-gap" colspan="5"></td>
                                    </tr>
                                    
                                    <?php 
                                }
                        
                            }
                            else
                            { ?>
                                <span class="pay-y-cp-b-l" >
                                    You currently have no history of payments on your account.
                                </span>
                            <?php
                            } ?>
                            </table>
                        </div>
                    <?php 
                    } ?>

                </div>

            </div>
        </div>
    </div>
</div>


<div id="popup-container">

    <div class="popup-background">
        <div class="popup quote_layer_monthly">
            <div class="popup-inner">

                <div class="modal-header" id="contact-pop" style="border-bottom: none; margin-bottom: 20px">
                    <button type="button" class="close" id="close-interest-layer" aria-label="Close" style="font-size: 29px;"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Monthly Interest Earned</h4>
                </div>

                <div class="container quote_layer_text">

                    <?php if( !empty($accrued_interest_data) ){ ?>
                        <div class="table-responsive" style="padding: 0px 30px;">
                            <table class="table product-table msg-table">

                                <tr>
                                    <th rowspan="2" style="width:10%;">Month</th>
                                    <th></th>
                                    <th rowspan="2" style="width:10%;">Interest</th>
                                    <th colspan="2"></th>
                                </tr>

                                <tr>
                                    <th class="red_border"></th>
                                    <th class="red_border"></th>
                                </tr>

                                <?php
                                $AccruedInterestTotal = 0;

                                foreach ($accrued_interest_data as $AccruedInterestData) {
                                    $AccruedInterestTotal += $AccruedInterestData['monthly_interest'];
                                    ?>
                                    <tr>
                                        <td class="datesmsg" style="width:45%;" colspan="2"><?=date('F', strtotime($AccruedInterestData['created_date']));?></td>
                                        <td class="titlesmsg" colspan="2"><?='£'.$AccruedInterestData['monthly_interest'];?></td>
                                        <td class="downloadPDFs"></td>
                                    </tr>
                                    <tr>
                                        <td class="table-gap" colspan="4"></td>
                                    </tr>

                                <?php } ?>

                                <tr>
                                    <td class="datesmsg" style="width:45%;" colspan="2"><strong>Total</strong></td>
                                    <td class="titlesmsg" colspan="2"><strong><?='£'.number_format($AccruedInterestTotal,2);?></strong></td>
                                    <td class="downloadPDFs"></td>
                                </tr>

                                <?php }
                                else { ?>
                                    <span class="pay-y-cp-b-l" >At this moment in time, you have not yet earned any interest on your balance.</span> <?php
                                }?>
                            </table>
                        </div>
                </div>

            </div>
        </div>
    </div>
</div>
