<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eversmart</title>

<!-- Bootstrap -->
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet">

<link href="<?= base_url(); ?>css/styles.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>css/dashboard.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>css/responsive.css" rel="stylesheet" type="text/css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">




<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
    .dashboard .dashboardNav ul li
    {
        margin: 0 0 0 51px;
    }
</style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

		<div id="body">
		
		<div class="topbar" id="red_top">
			<div class="container">
			<div class="row">


					<span class="col-sm-12 col-md-4 col-lg-6 px-0">
                        <a  href="<?php echo base_url(); ?>">
                            <img class="logo_dash" src="<?php echo base_url(); ?>images/logo-eversmart-new.png"/>
                        </a>
					</span>
					
					<div class="col-sm-12 col-md-4 col-lg-2 text-right hideonmobile pull-right" id="navbarSupportedContent-3-main">
	<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">            	Mr. Fuel    <i class="fa fa-caret-down" aria-hidden="true"></i>                            </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2" x-placement="bottom-end">

                                                                            <a class="dropdown-item waves-effect waves-light" href="#"><i class="fa fa-info righ-padd" aria-hidden="true"></i>my account</a>
                                                                        <a class="dropdown-item waves-effect waves-light" href="#"><i class="fa fa-question righ-padd" aria-hidden="true"></i>help &amp; faq</a>
                                    <a class="dropdown-item waves-effect waves-light" href="#"><i class="fa fa-address-book righ-padd" aria-hidden="true"></i>contact us</a>
									<a id="logout_user" class="dropdown-item waves-effect waves-light" onclick="logout_user();" href="javascript:void(0)"><i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout</a>
                                </div>

                    </div>
					 

				</div>
			</div>
		</div>
		<!---------------------End Top Bar------------------>

	<section class="termandconditions">
				<div class="container-fluid">
					<div class="row amend">
						<div class="col-sm-12 col-md-12 col-lg-2 desk-menu-dashboard">
						
						
						
						    <?php $this->load->view('menu.php'); ?>
						</div>


						<div class="col-sm-12 col-md-12 col-lg-8 dashboard-panel-right">
						
						<div class="large-screen-fix dashboardss">
							<div class="row justify-content-center">
             