<?php require_once 'header.php';
   // debug($profile,1);
 ?>
 
 
 
<div id='response_dashboard'>
<div class="col-md-12 ">
								<a href="#" class="gas_card">
									<div class="gas_box_icon gap_icon_dashboard">
										<span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>images/dashboard/Overview/movehome.svg" alt=""></span>
										<span class="dash-over-text">Moving Home</span>
										
									</div>
								</a>														
								</div>

								<div class="col-md-12">
									<div class="gas_card">
									<div class="container-fluid">
										<br /><br />
										<ul class="list-unstyled multi-steps">
											<li id="1" class="is-active">Start</li>
											<li id="2">Select Date</li>
											<li id="3">Enter Readings</li>
											<li id="4">Enter Address</li>
											<li id="5">Finish</li>
										</ul>
										</div>
									</div>
								</div>
								
								
								<div class="col-md-12">
								<div class="gas_card">
								<div class=" col-sm-9 mx-auto ">
									<div class="gas_box_icon gap_icon_dashboard">
										<span class="move-home-can">Moving home can be stressful, but we've made<br> the moving energy part simple.</span>
										
										<span class="move-home-point">											
											<p><span class="mov-p">1</span>Tell us when you moving out.</p>
											<p><span class="mov-p">2</span>Start switching your new home to eversmart.</p>
										
										</span>
										
									</div>
									</div>
									<span class="get-started-btn-c">
									<a href="javascript:void(0)" id="step_1">Get Started</a>
									
									
								</div>														
								</div>
								</div><!-- response -->

 <?php require_once 'footer.php'; ?>