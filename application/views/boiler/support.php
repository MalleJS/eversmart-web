<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Boiler Process</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>

<body class="sky-blue-bg" id="boiler-switch-live">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
	<div class="main-inner">
		
			<div class="boiler-page-shape">
		
		<div class="topbar">
			<?php $this->load->view('layout/menu');  ?>
			<!--- End container-------->
			
		</div>
		<!---end top bar bar----------->	
		


<!----------------start postcode--------------------->

			<section class="postcode_top mt0">
				<div class="postcode_box">
					
						<div class="container">
						
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<h2>Can you give us a <strong>ring?</strong></h2>
									
								</div>
							</div>
							<div class="row">
								<div class="switch_box_main">
								<div class="row justify-content-center">
										<span class="back_step"><a href="#" class="material-icons" style="top:1px;">
												<img src="<?php echo base_url() ?>assets/images/back.svg"></a></span>
										<div class="col-md-8 text-center">
											<div class="gas_card ">
												<span class="notification_txt">Based on what you've told us, it'll be easier to talk about your new boiler installation over the phone.</span>
									<div class="row">
										<span class="col-sm-8 mx-auto">
												<span class="col-phone-icon">
											     <span class="material-icons"><img src="<?php echo base_url() ?>assets/images/boiler/call.svg" alt=""> </span>
												</span>
												<span class="col-icon-detail">
												 <a href="tel:03301027901"> 0330 102 7901</a>
												 <p>Lines are open 8am–9pm, 7 days a week</p>
											   </span>
										</span>	
										</div>
										
											
											</div>
										</div><!----------------End First Box--------------------->
										
		
													
								</div><!-------------------End Row---------------->
								
								
								</div>
							
							</div>
						</div>
						</div>
					</section>
<!----------------End postcode--------------------->		
				

</div>
</div>

<?php $this->load->view('layout/common_footer'); ?>