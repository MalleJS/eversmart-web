<!DOCTYPE html> 
<html lang="en">

<head> 
    <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | <?php if( !empty($title) ){ echo $title; }else{ echo 'Dashboard'; } ?></title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">

		   <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url() ?>dashboard/css/dashboard.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />



<style>
#note {
    margin:auto;
    position:fixed;
    opacity:0;
	bottom:-200px;
	right:0

}

div#note img {
    max-width: 200px;
}
.red-rounded-wave {
    background: transparent;
    background-size: contain;
    float: left;
    width: 100%;
}
.termandconditions {
    margin-top: 0;
    background: white;
}
.text-left.field-sorting {
    font-size: 16px;
    text-transform: uppercase;
}
th.no-sorter {
    font-size: 16px;
    text-transform: uppercase;
}
td .text-left {
    text-align: left!important;
    color: #000;
    font-weight: 400;
}
table.tablesorter thead .field-sorting, table.tablesorter thead .field-sorting.asc, table.tablesorter thead .field-sorting.desc, table.tablesorter thead .field-sorting.asc_disabled, table.tablesorter thead .field-sorting.desc_disabled {
    cursor: pointer;
    padding-right: 0!important;
}
.topbar .container {
    max-width: 1500px!important;
}
#my-energy-pd {
    padding-bottom: 0;
}

footer.main_footer {
    display: none;
}
#my-energy-pd .bolier_home-h-l {
    margin-top: 10px;
    position: relative;
    margin-top: -63px;
    font-size: 35px;
}
.loading-page {
margin:20% auto;
width: 100px
 }

 .loader {
  border: 8px solid #fda1b8;
  border-radius: 50%;
  border-top: 8px solid #f64d76;
  width: 90px;
  height: 90px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.btn {
    display: inline-block;
    padding: 4px 14px;
    margin-bottom: 0;
    font-size: 14px;
    line-height: 20px;
    color: #000!important;
}

.btn .caret {
    margin-top: 8px;
    margin-left: 0;
    display: none;
}

ul.pager a {
    background: #ea495c;
    color: #fff!important;
}
li.previous.first-button.disabled a {
    background: #ea495c!important;
}
section.termandconditions .col-sm-12.col-md-12.col-lg-8 {
    margin: auto;
    max-width: 100%!important;
    flex: 0 0 100%!important;
}

.large-screen-fix {
    width: 100%!important;
    margin: 0 auto;
}
.container {
    margin-right: auto;
    margin-left: auto;
    width: 100%!important;
    max-width: 100%!important;
}
.span12 {
    width: 100%!Important;
}
.maxy {
    max-width: 100%;
}
#ajax_list .span12 {
    padding: 0;
    margin: 0;
}
</style>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>


<body class="sky-blue-bg">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->

	 <div class="main-inner" id="main_content" >
		<div class="main-rounded-red-header nofixed">
		<div class="red-rounded-wave" id="dashboard_main_bg">
		<div class="topbar" id="red_top">

			<div class="container">
				<div class="row">


					<span class="col-sm-12 col-md-4 col-lg-6 px-0">
						<a class="logo" id="logo-dash" href="<?php echo base_url(); ?>">eversmart.</a>
					</span>

					<div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right" id="mobile_menu" style="position: relative; float:left; padding-top:10px">

					<span class="product-code-no-d">
		
					</span>
					</div>

                    <div class=" col-sm-12 col-md-4 col-lg-2 text-right" id="navbarSupportedContent-3-main">


						 <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2">
					
									<a id="logout_user" class="dropdown-item waves-effect waves-light" href="javascript:void(0)"><i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout</a>
                                </div>

                    </div>
 

				</div>
			</div>
			<!--- End container-------->
			<!--- End container-------->

		</div>

		<!---end top bar bar----------->


<!----------------start postcode-------------------->

		<section class="mt0 ">
				<div class="boiler_header-h" id="my-energy-pd">
						<div class="container">
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Pending Signups</span>

								</div>
							</div>
						</div><!------------------End Container----------------->

				</div><!------------------End Postcode------------->

			</section>
<!----------------End postcode--------------------->
		</div>
	</div>


		<!--------------------Start breadcrumbs Care Section------------>

		<section class="termandconditions">
				<div class="container-fluid">
		


						<div class="col-sm-12 col-md-12 col-lg-8">
						  <!---<div class="alert alert-warning">
								<strong>Notification:</strong> Your account is not activated with Junifer. Please activate your account to see your bills. You should have an activation email, if not, <a href="#">click here</a> to resend activation email.
							  </div>-->
						<div class="large-screen-fix">
							<div class="row justify-content-center">
                <?php //debug( $this->session->userdata('login_data') );
                  //echo  $this->session->userdata('login_data')['id'];
                 ?>
						<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
</head>
<body>
	<div style='height:20px;'></div>  
    <div class="maxy">
    <div class="maxy" class="container">
	<div class="maxy" class="col-md-12">
		<?php echo $output; ?>
        </div>
        </div>
    </div>


							</div>
							</div>
						</div>
 

						

						</div>


					</div>




				</div>
			
			</section>

<!--------------------End breadcrumbs Care Section------------->



</div>
      <!-- SCRIPTS -->
      <!-- JQuery -->


      <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


  </body>
  </html>
 