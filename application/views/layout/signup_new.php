<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />


    <style type="text/css">
      body{font-family: 'Quicksand', sans-serif!important; }
  #redd-btn-pc{padding:18px 29px!important}

.nogo {
    pointer-events: none;
    opacity: 0.6;
}

.registration-page-heading-energy {padding-bottom: 20px!important;}

  .loading-page {
  margin: 24% auto;
  width: 100px
   }
.bank-titles {
    text-align: center;
    font-size: 32px;
    display: block;
    margin-bottom: 25px;
    position: relative;
    margin-top: 40px;
}
 
.mobileonly.dashbaordmobilemenu {
    position: absolute;
    top: 8px!important;
    font-size: 45px;
    left: 0px;
    padding: 5px;
    cursor: pointer;
    z-index: 9999;
    color: white;
}
#account_name {text-align:center; margin-top:20px;}

.account-wrap input
{
max-width:20px;
text-align:center; 
font-size:32px; 
padding:8px 18px; 
border: 1px solid #ced4da!important;
margin:3px;
}
.account-wrap {
    text-align: center;
}
   .loader {
    border: 8px solid #fda1b8;
    border-radius: 50%;
    border-top: 8px solid #f64d76;
    width: 90px;
    height: 90px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }

  /* Safari */
  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  </style>

  <!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119184931-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119184931-1');
</script>

<style>
.password-box { margin:20px auto}
.password-progress {
  height: 12px;
  margin-top: 10px;
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

.progress-bar {
  float: left;
  height: 100%;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

.bg-red {
  background: #E74C3C;
  border: 1px solid #E74C3C;
}

.bg-orange {
  background: #F39C12;
  border: 1px solid #F39C12;
}

.bg-green {
  background: #1ABB9C;
  border: 1px solid #1ABB9C;
}
</style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
  </script>
  
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


<?php // debug($signup_info);?>

<div class="cd-panel cd-panel--from-left js-cd-panel-main">
   <header class="cd-panel__header">
      <p class="mobilementitles">Menu</p>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php  if(  empty($this->session->userdata('login_data')) ){?>
              <li><a href="<?php echo base_url(); ?>index.php/user/login">Login</a></li> 
           <?php } ?>

            <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>

            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & faq</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
            <?php  if(  $this->session->userdata('login_data') ){?>
                <li><a href="#" onclick="logout_user()" id="logout_user">Log out</a></li>
            <?php } ?>
        </ul>
      </section>
    </nav>
    </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->
<body class="sky-blue-bg">
    <!-- Start your project here-->
 <div class="loading-page">
<div class="loader"></div>
</div>

	<div class="main-rounded-red-header" id="main_content" style="display:none">
		<div class="red-rounded-wave">
    <div class="topbar homemobile">
 
 <div class="container mobileonly">
     <div class="row">


       <span class="col-sm-12 col-md-4 col-lg-6 px-0 ">
         <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>">eversmart.</a>
       </span>

       <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
   

     <!--
           <p class="accnum">Account Number: </p>
         <span class="product-code-no-d">
               <?php
                 /*if( $this->session->has_userdata('login_data') ){
                    if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
                     echo $this->session->userdata('login_data')['account_number'];
                     }
                     elseif($this->session->userdata('login_data')['signup_type'] == 3)
                     {
                       echo $this->session->userdata('login_data')['account_number'];
                     }
                 }else{
                   echo '-';
                 }*/
               ?>
       </span> -->
       </div>
       <div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="main">☰</button></div>
     

     </div>
   </div>

 <?php $this->load->view('layout/menu'); ?>
 </div>
		<!---end top bar bar----------->


<!----------------start postcode--------------------->


<!----------------End postcode--------------------->

			<!----------------start postcode-------------------->

		<section class="postcode_top mt0">
				<div class="postcode_box" id="background-none">
        <div class="container">

<?php if ($signup_info['quotation_pay_energy'] === 'prepay') {?>
  <form action="JavaScript:void(0)" id="registraion-form-dyball" class="switch-form" name="">
              <div id="step_1">
							<div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
									<img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<div class="registration-page-heading">
										Enter your Details<br><span>(All fields are required except phone number)</span>
										</div>
											<div class="md-form col-md-3 mb-4" style="float:left">
											<select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
													<option value="">Title</option>
													<option value="Mr" selected>Mr.</option>
													<option value="Mrs">Mrs.</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Prof">Prof</option>
													<option value="Dr">Dr</option>
												</select>
										</div>

												<div class="col-md-9 mb-4" style="float:left">
													<div class="md-form">
														<input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
												<div class="valid_first" style="display:none">
												<div class="input-group-addon">
												  <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
												 </div>


												</div>

										  <div style="display:none" class="first_error">
										  <div class="input-group-addon">
												<a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
										</div>
										</div>
                        </div>


												</div>

												<div class="col-md-12 mb-4" style=" clear:both">
													<div class="md-form" >
														<input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                            <div class="valid_last" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="last_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>


													</div>

												</div>


												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                            <div class="valid_email" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="email_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form show_hide_password">

														<input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                                        <div class="input-group-addon">
															<a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
														  </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                            <div class="valid_confirm_password" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													 <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>


												</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
                            <div class="valid_telephone" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
												 <div style="display:none" class="telephone_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>

													</div>
                         					</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
                            <div class="valid_phone_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>

													  <div style="display:none" class="phone_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
                            <div class="valid_datepicker" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
													</div> 

													<div class="col-md-12 mb-4">
													<div class="md-form">
														
                            <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                          
                           <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                           
                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                            <?php } ?> 

                            <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
  
													</div>
                        </div>
                        <div class="error_msg"></div>

                        <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                        <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                        <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                        <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">

<div style="text-align:center">
<form id="complete_form_dyball">
<button style="font-size:18px; padding:10px 84px; " class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
</form>
</div>

                        </div>


											</div>
										</div>
									</div>

								</div>
                <!-- row  -->
              </form>


              <?php } elseif ($monthyear === 'yearlypay') {?> 

			
<form action="JavaScript:void(0)" id="registraion-form-yearpay" class="switch-form" name="">
<div id="step_1">
<div class="row">
  <div class="col-md-8 mx-auto main-top-margin-60">
  <!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
    <img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
    <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
      <div class="content" style="padding:60px 0px">
      <div class="col-md-10 mx-auto">
      <div class="registration-page-heading">
      Enter your Details<br><span>(All fields are required except phone number)</span>
      </div>
        <div class="md-form col-md-3 mb-4" style="float:left">
        <select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
            <option value="">Title</option>
            <option value="Mr" selected>Mr.</option>
            <option value="Mrs">Mrs.</option>
            <option value="Miss">Miss</option>
            <option value="Ms">Ms</option>
            <option value="Prof">Prof</option>
            <option value="Dr">Dr</option>
          </select>
      </div>

          <div class="col-md-9 mb-4" style="float:left">
            <div class="md-form">
              <input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
          <div class="valid_first" style="display:none">
          <div class="input-group-addon">
            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
           </div>


          </div>

        <div style="display:none" class="first_error">
        <div class="input-group-addon">
          <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
      </div>
      </div>
          </div>


          </div>

          <div class="col-md-12 mb-4" style=" clear:both">
            <div class="md-form" >
              <input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
              <div class="valid_last" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

 <div style="display:none" class="last_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>


            </div>

          </div>


          <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
              <div class="valid_email" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

 <div style="display:none" class="email_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>

          </div>

          <div class="col-md-12 mb-4">
            <div class="md-form show_hide_password">

              <input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                          <div class="input-group-addon">
                <a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
                </div>
            </div>

          </div>

          <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
              <div class="valid_confirm_password" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
             <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>


          </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
              <div class="valid_telephone" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
           <div style="display:none" class="telephone_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>

            </div>
                     </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
              <div class="valid_phone_number" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

              <div style="display:none" class="phone_number_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>

            </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
              <div class="valid_datepicker" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
            </div>
            </div> 

            <div class="col-md-12 mb-4">
            <div class="md-form">
              
              <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
            
             <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
             
              <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
              <?php } ?> 

              <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

              <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

              <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>

            </div>
          </div>
          <div class="error_msg"></div>

          <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
          <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
          <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
          <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">

          <span class="input-group-btn btnNew" style="float: none;display: block;margin: auto;width: 50%;">
                <button style="font-size:18px; padding:10px 84px; float: none;"  class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>
          </span>


          </div>


        </div>
      </div>
    </div>

  </div>
  <!-- row  -->
</form>

              <?php } else {?> 

			
              <form action="JavaScript:void(0)" id="registraion-form" class="switch-form" name="">
              <div id="step_1">
							<div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
									<img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<div class="registration-page-heading">
										Enter your Details<br><span>(All fields are required except phone number)</span>
										</div>
											<div class="md-form col-md-3 mb-4" style="float:left">
											<select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
													<option value="">Title</option>
													<option value="Mr" selected>Mr.</option>
													<option value="Mrs">Mrs.</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Prof">Prof</option>
													<option value="Dr">Dr</option>
												</select>
										</div>

												<div class="col-md-9 mb-4" style="float:left">
													<div class="md-form">
														<input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
												<div class="valid_first" style="display:none">
												<div class="input-group-addon">
												  <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
												 </div>


												</div>

										  <div style="display:none" class="first_error">
										  <div class="input-group-addon">
												<a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
										</div>
										</div>
                        </div>


												</div>

												<div class="col-md-12 mb-4" style=" clear:both">
													<div class="md-form" >
														<input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                            <div class="valid_last" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="last_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>


													</div>

												</div>


												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                            <div class="valid_email" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="email_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form show_hide_password">

														<input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                                        <div class="input-group-addon">
															<a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
														  </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                            <div class="valid_confirm_password" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													 <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>


												</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
                            <div class="valid_telephone" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
												 <div style="display:none" class="telephone_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>

													</div>
                         					</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
                            <div class="valid_phone_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>

													  <div style="display:none" class="phone_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
                            <div class="valid_datepicker" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
													</div> 

													<div class="col-md-12 mb-4">
													<div class="md-form">
														
                            <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                          
                           <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                           
                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                            <?php } ?> 

                            <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
  
													</div>
                        </div>
                        <div class="error_msg"></div>

                        <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                        <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                        <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                        <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">

												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:50px; ">
															<button style="font-size:18px; padding:10px 84px;" id="next_step" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>
												</span>


                        </div>


											</div>
										</div>
									</div>

								</div>
                <!-- row  -->
              </form>

<?php } ?>

						</div>
            <!-- step 1 -->
            <div id="step_2" style="display:none">
              <div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<span class="back_step"><a href="javascript:void()" id="back_from" class="material-icons" style="top:-123px;">
									<img src="<?php echo base_url();?>assets/images/red-back-button.svg"></a></span>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:20px 0px 60px">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy-H">
										Bank Details for 12 Months
										</span>
										<!---<span class="registration-page-energy-p">
										 	&#163; <?php// if( !empty($summary_from_info['complete_price']) ) { echo $summary_from_info['complete_price']; }else{ echo '-'; } ?>
										</span>--->

										<span class="registration-page-energy-d" style="font-size:15px">
										Your payments will be the same every month, and we'll take them direct from bank
										</span>

											</div>
										</div>
									</div>

								</div>
						</div>
            <div class="row">
								<div class="col-md-8 mx-auto">
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
                      <form id="complete_form">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy">
										Because you selected Direct Debit as the payment mode <br>
										for Energy Payment, we require you're bank details
										</span>
                    <div id="form_submit" style="display:none"></div>
												<div class="col-md-12 " style="float:left; width:100%">
													<div class="md-form" >				

<div class="bank-titles">Account Number</div>
<div class="account-wrap">
<input type="text" maxlength="1" max="1" id="first-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="second-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="thrid-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="fourth-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="five-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="six-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="seven-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="eight-account-number" placeholder="0" onclick="this.select();"/>
</div>


														<input style="display:none" class="form-control" type="number" placeholder="Your Bank or society account Number" required name="account_number" id='account_number' maxlength="8">
														<!-- <label for="form1" class="bank-society-acc">Your Bank or society account Number<span class="required-field">*</span></label> -->
                            <div class="valid_account_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="account_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
												</div>
												<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
<div class="bank-titles">Sort Code</div>
<div class="account-wrap">
<input type="text" maxlength="1" max="1" id="first-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="second-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="thrid-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="fourth-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="five-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="six-account" placeholder="0" onclick="this.select();"/>
</div>


														<input style="display:none"  class="form-control" required maxlength="8" placeholder="Sort Code" type="text" name="sort_code" id="sort_code">

														<!-- <label for="form1" class="">Sort Code<span class="required-field">*</span></label> -->
                            <div class="valid_sort_code" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="sort_code_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
												</div>
													<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
														<input class="form-control" placeholder="Name of Account Holder" required  type="text" name="account_name" id="account_name">
														<!-- <label for="form1" class="">Name of Account Holder<span class="required-field">*</span></label> -->
                            <div class="valid_account_name" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="account_name_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>
                          <div class="error_msg" style="clear:both"></div>
                          <div class="signup_msg" style="clear:both"></div>
												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:30px;">
												<div id="signup-load" style="display:none;width:100%; text-align:center; float:left; padding:30px 0px;">
												<div class="loader-signup" ></div>
												</div>

															<button style="font-size:18px; padding:10px 84px; " class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
															</span>
															<span class="read-direct" style="margin-top:-18px;">Read the<a target="_blank" href="<?php echo base_url(); ?>assets/pdf/___Direct_Debit_Guarantee.pdf "> Direct Debit Gaurantee</a></span>
											</div>
                      </form>
										</div>
									</div>
								</div>
						</div>
            </div>
            <!-- step 2 -->

        </div> <!-- container -->
						</div>
					</section>
<!----------------End postcode-------------------->


		</div>


		<!--------------------Start breadcrumbs Care Section------------>



<!--------------------End breadcrumbs Care Section------------>


<!----------------Start Footer-------------------------->


    	<footer class="main_footer">
		<div class="footer-top-sky-rounded"></div>
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row">
    					<div class="col-sm-4 col-md-3">
    						<h4>eversmart.</h4>
    						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
							<h5>Opening Times: </h5>
              <p>MON - FRI 8am-8pm  <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
    					</div>
    					<div class="col-sm-4 col-md-2">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
    								<li><a href="#">Smart Meters</a></li>
    								<!-- <li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li> -->
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Useful Links</h4>
    							<ul class="links-vertical">
                    <li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
                    <li><a href="#">Consumer Checklist (UK)</a></li>
									  <li><a href="#">Consumer Checklist (Welsh)</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & Support</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-12 col-md-3 col-lg-3">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical" id="small-lower-text">
    								<li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i>   0330 102 7901</a></li>
									<li> <i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
									<li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; 2018 Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>

    	</footer>
    	<!----end footer------------>

</div>


<!-- end id main content -->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script> -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
      <!-- Bootstrap tooltips -->
      <!-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script> -->

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/passwordStrength.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
      <!-- common JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>
      <!------ Include the above in your HEAD tag ---------->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
      <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>


    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltippass"]').tooltip();
        });
    </script>
  	<script type="text/javascript">

$(function() {
  $("#password_visibility").click(function(){
        var pass_input = document.getElementById("password_input");
        if (pass_input.type === "password") {
            pass_input.type = "text";
            $(this).removeClass("fa-eye").addClass("fa-eye-slash")
        } else {
            pass_input.type = "password";
            $(this).removeClass("fa-eye-slash").addClass("fa-eye")
        }
   });

   $('.check_first').change(function(){
     $('.first_error').hide();
     if( characterOnly( $(this).val() ) ) {
       $('.waves-light').removeAttr('disabled');
       $('.valid_first').show();
     }else{
       console.log('fail');
       $('.valid_first').hide();
       //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
       $('.first_error').show();
       $( ".check_first" ).focus();
       $('.waves-light').attr('disabled','disabled');
     }
   });



   $('.check_last').change(function(){
     $('.last_error').hide();
     if( characterOnly( $(this).val() ) ) {
       $('.waves-light').removeAttr('disabled');
       $('.valid_last').show();
     }else{
       //console.log('fail');
       $('.valid_last').hide();
       //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
       $('.last_error').show();
       $('.waves-light').attr('disabled','disabled');
     }
   });

   $('#phone_number').change(function(){
     $('.phone_number_error').hide();
     if( isNumberKey( $(this).val() ) ) {
      // $('.waves-light').removeAttr('disabled');
       $('.valid_phone_number').show();
     }else{
       //console.log('fail');
       $('.valid_phone_number').hide();
       //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
       $('.phone_number_error').show();
       //$('.waves-light').attr('disabled','disabled');
     }
   });


   $('#sort_code').change(function(){
     $('.sort_code_error').hide();
     if( isNumberKey( $(this).val() ) ) {
      // $('.waves-light').removeAttr('disabled');
       $('.valid_sort_code').show();
     }else{
       //console.log('fail');
       $('.valid_sort_code').hide();
       //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
       $('.sort_code_error').show();
       //$('.waves-light').attr('disabled','disabled');
     }
   });

$('#account_number').change(function(){
  $('.account_number_error').hide();
  if( isNumberKey( $(this).val() ) ) {
    $('.waves-light').removeAttr('disabled');
    $('.valid_account_number').show();
  }else{
    //console.log('fail');
    $('.valid_account_number').hide();
    //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
    $('.account_number_error').show();
    ('.waves-light').attr('disabled','disabled');
  }
});


  //
  $('#telephone').change(function(){
    $('.telephone_error').hide();
    if( isNumberKey( $(this).val() ) ) {
      $('.waves-light').removeAttr('disabled');
      $('.valid_telephone').show();
    }else{
      //console.log('fail');
      $('.valid_telephone').hide();
      //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
      $('.telephone_error').show();
      $('.waves-light').attr('disabled','disabled');
    }
  });


  $('#account_name').change(function(){
    $('.account_name_error').hide();
    if( characterOnly( $(this).val() ) ) {
      $('.waves-light').removeAttr('disabled');
      $('.valid_account_name').show();
    }else{
      //console.log('fail');
      $('.valid_account_name').hide();
      //$('.error_msg').show('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Please input only character</div>');
      $('.account_name_error').show();
      $('.waves-light').attr('disabled','disabled');
    }
  });



});

// function isNumberKey(evt)
// {
//    var charCode = (evt.which) ? evt.which : evt.keyCode
//    if (charCode > 31 && (charCode < 48 || charCode > 57))
//       return false;
//
//    return true;
// }

function isNumberKey(inputvalue){
    var letters = /^[0-9+-]+$/;
    if(inputvalue.match(letters))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function characterOnly(inputvalue){
        var letters = /^[a-zA-Z ]+$/;
        if(inputvalue.match(letters))
        {
            // alert('Your registration number have accepted : you can try another');
            // document.form1.text1.focus();
            return true;
        }
        else
        {
            // alert('Please input alphanumeric characters only');
            return false;
        }

    }

function validateEmail(Email) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    return $.trim(Email).match(pattern) ? true : false;
}

function validatePassword(Password) {
    var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$_.!%*?&])[A-Za-z\d$@$_.!#%*?&]{8,}$/;

    return $.trim(Password).match(pattern) ? true : false;
}


$(document).ready(function(){

  //check all fields is non empty
  // if( $('#password').val() != '' && $('#confirm_password').val() != '' && $('.check_first').val() != '' && $('.check_last').val() != '' && $('#email').val() != '' && $('#datepicker').val() != '' &&  $('#telephone').val() != '' )
  // {
  //   $('#next_step').attr('disabled', 'disabled');
  //   return false;
  // }
  //alert($('#registraion-form').serialize());
  $('#registraion-form > input').keyup(function() {
    console.log('none');
        var empty = false;
        $('#registraion-form > input').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#next_step').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#next_step').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }
    });


   var dateage = new Date();
   dateage.setDate( dateage.getDate() - 0 );
   dateage.setFullYear( dateage.getFullYear() - 16 );


      $('#datepicker').datepicker({
          format: "dd-mm-yyyy",
          autoclose: true,
          uiLibrary: 'bootstrap4',
          endDate: new Date(dateage)
      });

      $('#password').passwordStrength();

      $('#back_from').click(function(){
      //$(document).on('click', '#back_from', function(){
        $('#step_1').fadeIn();
        $('#step_2').fadeOut();
        $('#registraion-form-2').attr('id','registraion-form');
        $('#next_step').attr('type','submit');
    });


$('#datepicker').bind('keyup','keydown', function(event) {
  	var inputLength = event.target.value.length;
		if (event.keyCode != 8){
		  if(inputLength === 2 || inputLength === 5){
			var thisVal = event.target.value;
			thisVal += '-';
			$(event.target).val(thisVal);
			}
      //$('.valid_datepicker').show();
		}
  });

$('#sort_code').bind('keyup','keydown', function(event) {
  	var inputLength = event.target.value.length;
		if (event.keyCode != 8){
		  if(inputLength === 2 || inputLength === 5){
			var thisVal = event.target.value;
			thisVal += '-';
			$(event.target).val(thisVal);
			}
		}
  });


  $('#confirm_password').keyup(function(){
      if( $(this).val() != $('#password').val() ){
          $('.confirm_password_error').show();
          $('.valid_confirm_password').hide();
          $('.waves-light').attr('disabled','disabled');
      }else{
        $('.confirm_password_error').hide();
        $('.valid_confirm_password').show();
        $('.waves-light').removeAttr('disabled');
      }
  });

$('#email').change(function(){
    var input_email = $(this).val();

    if( !validateEmail(input_email) ){
      $('.email_error').show();
      $('.valid_email').hide();
    }else{

    $.ajax({
      url: '<?php echo base_url(); ?>index.php/user/check_email',
      type: 'get',
      dataType: 'json',
      data: { email: input_email },
      success:function(response){
        console.log(response);
        $('.error_msg').empty();
        if( response.error == '1' ){
        //  $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Email already exits</div>');
        $('.email_error').show();
        $('.valid_email').hide();
          $('#next_step').attr('disabled','disabled');
        }else{
          //$('.error_msg').empty();
          $('.email_error').hide();
          $('.valid_email').show();
          $('#next_step').removeAttr('disabled');
        }
      }
    });
  }
});


      $('#registraion-form').submit(function(e){
      //$(document).on('submit', '#registraion-form', function(e){
          e.preventDefault();
          //alert($('#account_number').val());
          $('.error_msg').empty();

          if( !validateEmail( $('#email').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please provide valid email</div>');
          		return false;
          }

          if( !validatePassword( $('#password').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Passwords must contain the following:<ul><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul></div>');
          		return false;
          }

          if( !characterOnly( $('.check_last').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in last name</div>');
          		return false;
          }

          if( !characterOnly( $('.check_first').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in first name</div>');
          		return false;
          }



          if( $('#password').val() != $('#confirm_password').val()  ){
            $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Password did not match.</div>');
          }else{
            $('#step_1').fadeOut();
            $('#step_2').fadeIn();



              $.ajax({
                  //url: "<?php //echo base_url(); ?>index.php/user/enroll_customer",
                  url: "<?php echo base_url(); ?>index.php/user/changeformat",
                  type: 'post',
                  //dataType: 'json',
                  data: $('#registraion-form').serialize(),
                  beforeSend: function(){
                    //$(".signup_msg").html('<center><div class="loader"></div></center>');
					//$('.loading-page').fadeIn();
					$('#signup-load').fadeIn();
                  },
				  complete:function(){
					 // $('.loading-page').fadeOut();
					$('#signup-load').fadeOut();
				  },
                  success:function(response){
                    console.log(response);
                    $('#form_submit').html(response);

                    //$('.signup_msg').empty();
                    if( response.error == '0' ){

                      $('#registraion-form')[0].reset();
                      $('.btnNew').css('display','none');
                      $('#back_from').hide();
                      $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                    }
                    if( response.error == '1' ){
                      $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
                    }
                    if( response.error == '2' ){
                      $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
                    }
                  }
              });
            // }
            // else{
            //   $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> All fields are requied </div>');
            // }
          }


      });



  $('#registraion-form-dyball').submit(function(e){
      //$(document).on('submit', '#registraion-form', function(e){
          e.preventDefault();
          //alert($('#account_number').val());
          $('.error_msg').empty();

          if( !validateEmail( $('#email').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please provide valid email</div>');
          		return false;
          }

          if( !validatePassword( $('#password').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Passwords must contain the following:<ul><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul></div>');
          		return false;
          }

          if( !characterOnly( $('.check_last').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in last name</div>');
          		return false;
          }

          if( !characterOnly( $('.check_first').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in first name</div>');
          		return false;
          }



          if( $('#password').val() != $('#confirm_password').val()  ){
            $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Password did not match.</div>');
          }else{
          
            $.ajax({
            url: "<?php echo base_url(); ?>index.php/user/enroll_customer",
            //url: "<?php //echo base_url(); ?>index.php/user/changeformat",
            type: 'post',
            dataType: 'json',
            data: $('#registraion-form-dyball').serialize(),
            beforeSend: function(){
              $('.error_msg').empty();
              $('.signup_msg').empty();
              //$(".signup_msg").html('<center><div class="loader"></div></center>');
    //$('.loading-page').fadeIn();
    $('#signup-load').fadeIn();
            },
    complete:function(){
     // $('.loading-page').fadeOut();
    $('#signup-load').fadeOut();
    },
            success:function(response){
              console.log(response);
              //$('#form_submit').html(response);

              //$('.signup_msg').empty();
              if( response.error == '0' ){

                $('#registraion-form-dyball')[0].reset();
                $('.btnNew').css('display','none');
                $('#back_from').hide();
                //$('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');

         
                window.location.href="<?php echo base_url() ?>index.php/user/signup_success";
                
              }
              if( response.error == '1' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
              }
              if( response.error == '2' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
              }
            }
        });
            // }
            // else{
            //   $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> All fields are requied </div>');
            // }
          }


      });



 $('#registraion-form-yearpay').submit(function(e){
      //$(document).on('submit', '#registraion-form', function(e){
          e.preventDefault();
          //alert($('#account_number').val());
          $('.error_msg').empty();

          if( !validateEmail( $('#email').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please provide valid email</div>');
          		return false;
          }

          if( !validatePassword( $('#password').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Passwords must contain the following:<ul><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul></div>');
          		return false;
          }

          if( !characterOnly( $('.check_last').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in last name</div>');
          		return false;
          }

          if( !characterOnly( $('.check_first').val() ) )
          {
          		console.log('not valid');
          		$('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in first name</div>');
          		return false;
          }



          if( $('#password').val() != $('#confirm_password').val()  ){
            $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Password did not match.</div>');
          }else{
          
            $.ajax({
            url: "<?php echo base_url(); ?>index.php/user/enroll_customer",
            //url: "<?php //echo base_url(); ?>index.php/user/changeformat",
            type: 'post',
            dataType: 'json',
            data: $('#registraion-form-yearpay').serialize(),
            beforeSend: function(){
              $('.error_msg').empty();
              $('.signup_msg').empty();
              //$(".signup_msg").html('<center><div class="loader"></div></center>');
    //$('.loading-page').fadeIn();
    $('#signup-load').fadeIn();
            },
    complete:function(){
     // $('.loading-page').fadeOut();
    $('#signup-load').fadeOut();
    },
            success:function(response){
              console.log(response);
              //$('#form_submit').html(response);

              //$('.signup_msg').empty();
              if( response.error == '0' ){

                $('#registraion-form-yearpay')[0].reset();
                $('.btnNew').css('display','none');
                $('#back_from').hide();

                $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                window.location.href="<?php echo base_url() ?>index.php/user/signup_success";

             
                monthyear = 0 ;
                monthyear = '<?php echo $monthyear ?>' ;
                console.log(monthyear);

                if( monthyear  === 'yearlypay' ){

                $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                window.location.href="<?php echo base_url() ?>index.php/user/yearlypayment?pay=";

                }

                else {

                  $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                window.location.href="<?php echo base_url() ?>index.php/user/signup_success";

                }

              }
              if( response.error == '1' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
              }
              if( response.error == '2' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
              }
            }
        });
            // }
            // else{
            //   $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> All fields are requied </div>');
            // }
          }


      });

//complete_form submit
$(document).on('submit','#complete_form',function(e){

    e.preventDefault();

      if( !characterOnly( $('#account_name').val() ) )
      {
          console.log('not valid');
          $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only alphabet in account name</div>');
          return false;
      }

      if( !isNumberKey( $('#account_number').val() ) )
      {
          console.log('not valid');
          $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only numbers in account number</div>');
          return false;
      }

      if( !isNumberKey( $('#sort_code').val() ) )
      {
          console.log('not valid');
          $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Please input only numbers in sort code</div>');
          return false;
      }

        $.ajax({
            url: "<?php echo base_url(); ?>index.php/user/enroll_customer",
            //url: "<?php //echo base_url(); ?>index.php/user/changeformat",
            type: 'post',
            dataType: 'json',
            data: $('#complete_form').serialize(),
            beforeSend: function(){
              $('.error_msg').empty();
              $('.signup_msg').empty();
              //$(".signup_msg").html('<center><div class="loader"></div></center>');
    //$('.loading-page').fadeIn();
    $('#signup-load').fadeIn();
            },
    complete:function(){
     // $('.loading-page').fadeOut();
    $('#signup-load').fadeOut();
    },
            success:function(response){
              console.log(response);
              //$('#form_submit').html(response);

              //$('.signup_msg').empty();
              if( response.error == '0' ){

                $('#registraion-form')[0].reset();
                $('.btnNew').css('display','none');
                $('#back_from').hide();
                //$('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                window.location.href="<?php echo base_url() ?>index.php/user/signup_success";
              }
              if( response.error == '1' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
              }
              if( response.error == '2' ){
                $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
              }
            }
        });
      // }
      // else{
      //   $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> All fields are requied </div>');
      // }
    //}


});

	  $("#show_password").on('click', function() {
        //event.preventDefault();
        if($('#password').attr("type") == "text"){
            $('#password').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#password').attr("type") == "password"){
            $('#password').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });



});

var dashed = "-";
$('#first-account').change(function() {
    $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + $('#five-account').val() + $('#six-account').val());
});

$('#second-account').change(function() {
   $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + dashed + $('#five-account').val() + $('#six-account').val());
});


$('#thrid-account').change(function() {
   $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + dashed + $('#five-account').val() + $('#six-account').val());
});

$('#fourth-account').change(function() {
   $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + dashed + $('#five-account').val() + $('#six-account').val());
});
$('#five-account').change(function() {
    $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + dashed + $('#five-account').val() + $('#six-account').val());
});

$('#six-account').change(function() {
     $('#sort_code').val($('#first-account').val() + $('#second-account').val() + dashed + $('#thrid-account').val() + $('#fourth-account').val() + dashed + $('#five-account').val() + $('#six-account').val());
});


//////


$('#first-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});

$('#second-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});


$('#thrid-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});

$('#fourth-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});
$('#five-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});

$('#six-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});

$('#seven-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});

$('#eight-account-number').change(function() {
    $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
});


$(".account-wrap input").bind(".account-wrap input", function() {
        var $this = $(this);
        setTimeout(function() {
            if ( $this.val().length >= parseInt($this.attr("maxlength"),10) )
                $this.next("input").focus();
                $this.next("input").select();

        },0);
});
 $("#eight-account-number").bind("input", function() {
        var $this = $(this);
        setTimeout(function() {
            if ( $this.val().length >= parseInt($this.attr("maxlength"),10) )
            
               $("input#first-account").focus().select();

        },0);
});
 $("#six-account").bind("input", function() {
        var $this = $(this);
        setTimeout(function() {
            if ( $this.val().length >= parseInt($this.attr("maxlength"),10) )
            
               $("#account_name").focus();

        },0);
});
 
$(document).ready(function(){
    $('.account-wrap input').keypress(validateNumber);
});

jQuery("input#password" ).keypress(function() { 
if ($(".progress-bar").hasClass("bg-orange")) {
$('.passbtn').removeClass('nogo');
}
else {$('.passbtn').addClass('nogo');}
});

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
    	return true;
    }
};

  		</script>
  </body>
  </html>
