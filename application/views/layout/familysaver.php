</div>
</div>

<div id='family-saver-page-wrap'>
    <header>
        <h1>Family Saver Club</h1>
    </header>
    <section id='family-saver-video'>
        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
        <div class='embed-container'>
            <iframe src='https://www.youtube.com/embed/yDOBHDPD-EU' frameborder='0' allowfullscreen></iframe>
        </div>
        <div class='description-wrap'>
            <h2>Earn 12% interest on your credit balance when you pay for your energy yearly!</h2> 
            <p>We know that times can be tough and energy can get expensive – especially during the winter months. Here at Eversmart we’re always looking for ways to save you money and make your energy work harder for you. That’s why we’re pleased to introduce the Family Saver Club. Here’s how it works:</p>
            <a href="<?= base_url(); ?>index.php/quotation" class='get-a-quote'>Click here for a quote!</a>
        </div>
    </section>
    <section id='family-saver-quotation-steps'>
        <div class='quote-step'>
            <div class='img-wrap'>
                <img src='/assets/images/family-savers-page/step-1-icon.svg' alt='Step 1' />
            </div>
            <span class="step_heading">Step 1</span>
            <span class="step_intro">Go online and get a quote</span>
            <p class="step_detail">Give us a few simple details (it only takes 2 minutes) and we’ll give you a quote for your gas and electricity.</p>
        </div>
        <div class='quote-step'>
            <div class='img-wrap'>
                <img src='/assets/images/family-savers-page/step-2-icon.svg' alt='Step 2' />
            </div>
            <span class="step_heading">Step 2</span> 
            <span class="step_intro">Choose to pay yearly</span>
            <p class="step_detail">You will be given the option to pay for your energy yearly or monthly. Select “Yearly” and click on “Switch Now”. You will then be taken to a payment page where you can pay for your energy a year in advance.</p>
        </div>
        <div class='quote-step'>
            <div class='img-wrap'>
                <img src='/assets/images/family-savers-page/step-3-icon.svg' alt='Step 3' />
            </div>
            <span class="step_heading">Step 3</span>
            <span class="step_intro">Start earning!</span>
            <p class="step_detail">Once your switch is complete, we will add 1% interest to your credit balance each month. That’s better than a high street savings account or an ISA!</p>
        </div>
    </section>

    <section id='family-saver-payments-and-interest'>
        <div class='img-wrap'>
            <img src='/assets/images/family-savers-page/family-saver-portal-1.png' alt=''/>
        </div>
        <div class='caption'>
            <h2>Your Payments and Interest</h2>
            <p>You can view your accrued interest whenever you like by logging in to your account.</p>
        </div>
    </section>

    <section id='family-saver-benefits'>
        <h2>Other benefits of paying yearly:</h2>
        <ul class="fs-ul">
            <li>
                <div class='img-wrap'>
                    <img src='/assets/images/family-savers-page/shield-icon.png' alt=''/>
                </div>
                <span>Fixed prices for 12 months on the cheapest fixed energy tariff on the market</span>
            </li>
            <li>
                <div class='img-wrap'>
                    <img src='/assets/images/family-savers-page/ipad-icon.png' alt=''/>
                </div>
                <span>Top up your balance or withdraw excess credit whenever you like (T&Cs apply)</span>
            </li>
            <li>
                <div class='img-wrap'>
                    <img src='/assets/images/family-savers-page/clock-icon.png' alt=''/>
                </div>
                <span>Only make one simple payment for the whole year</span>
            </li>
            <li>
                <div class='img-wrap'>
                    <img src='/assets/images/family-savers-page/moneybag-icon.png' alt=''/>
                </div>
                <span>A typical user could earn up to £10 per month!</span>
            </li>
            <li>
                <div class='img-wrap fan-fix'>
                    <img src='/assets/images/family-savers-page/fan-icon.png' alt=''/>
                </div>
                <span>100% renewable electricity</span>
            </li>
        </ul>
    </section>
    <footer>
        <ul class='cta-list'>
            <li>
                <a href='mailto:loyalfamily@eversmartenergy.co.uk' target="_blank" class='cta-link' title=''>
                    <div class='img-wrap'>
                        <img src='/assets/images/ctas/envelope.svg' alt=''/>
                    </div>
                    <span>Email Us</span>
                </a>
            </li>
            <li>
                <a href='tel:0161-332-0022' target="_blank" class='cta-link' title=''>
                    <div class='img-wrap'>
                        <img src='/assets/images/ctas/phone.svg' class='phone-fix' alt=''/>
                    </div>
                    <span>0161 332 0022</span>
                </a>
            </li>
            <li>
                <a href='#' target="_blank" class='cta-link open_intercom' onclick="return false;" title=''>
                    <div class='img-wrap'>
                        <img src='/assets/images/ctas/headset.svg' class='headset-fix' alt=''/>
                    </div>
                    <span>Get a Call Back</span>
                </a>
            </li>
        </ul>
    </footer>
</div>

<script type='text/javascript'>
    document.addEventListener("DOMContentLoaded", function(event) {
        document.querySelector('.red-rounded-wave').classList.add('menuBarFix');
    });
</script>
