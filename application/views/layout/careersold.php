<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Careers</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="index-page">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
	<div class="main">
		<div class="topbar" id="red_top">
			<?php $this->load->view('layout/menu'); ?>
<!-- 			<div class="container">
				<div class="row">	
				
				<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-unique">
					<span class="col-sm-6 col-md-3 px-0">
						<a class="logo" href="index.html">eversmart.</a>
					</span>
					
					<div class="col-sm-6 col-md-9 text-right pull-right no-padding mobile_menu">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation" style="">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
                        <ul class="navbar-nav ml-auto">
                           <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="energy-switch-one.html">energy
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="boiler-switch-one.html">Boiler                               
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">smart meters</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">home services</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">help
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger" aria-labelledby="navbarDropdownMenuLink-2">
                                    <a class="dropdown-item waves-effect waves-light" href="#">help & faq</a>
                                    <a class="dropdown-item waves-effect waves-light" href="#">contact us</a>                                   
                                </div>
                            </li>
							
							<li><button type="button" class="btn btn-info btn-rounded waves-effect waves-light">LOGIN</button></li>
							<li><button style="margin-left:25px" type="button" class="btn btn-info btn-rounded waves-effect waves-light">TOP UP</button></li>
							
                        </ul>
                        
                    </div>					
					</div>
                </nav>
				</div>
			</div>
 -->			<!--- End container-------->
			
		</div>
		<!---end top bar bar----------->	


<!----------------start postcode-------------------->

			<section class="postcode_top mt0" id="red_bg">
				<div class="boiler_header-h">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Careers</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								</div>
							</div>							
						</div><!------------------End Container---------------->
							
				</div><!------------------End Postcode------------>
			</section>
<!----------------End postcode--------------------->						
				
<!--------------------Start breadcrumbs Care Section------------>
			<section class="breadcrumbs" id="faq_image">
				<div class="rounder-red-bg">
				</div>
				
			</section>
<!--------------------End breadcrumbs Care Section------------->	

		<!--------------- Careers Download---------------------->
		
			<section class="career-download">
				<div class="container">
					<div class="row">
						<div class="col-md-10 mx-auto text-center">
							<h1>We are growing</h1>							
							<h2>We are a gowing organisation with many opportunities. <br>
								If you would like to join us then please get in touch. </h2>
						
						</div>
					</div>

				<div class="row margintb60">
					<div class="col-sm-6">
						<div class="career-page">
							<a href="#" target="_blank">
							  <div class="content-overlay"></div>
							  <img class="content-image" src="<?php echo base_url(); ?>assets/img/careers1.png">
							  <div class="content-details fadeIn-bottom">
								<h3 class="content-title">Eversmart Vision</h3>
							
							  </div>
							</a>
						  </div>
					
					</div>
					
					<div class="col-sm-6">
					
						<div class="career-page">
							<a href="#" target="_blank">
							  <div class="content-overlay"></div>
							 <img class="content-image" src="<?php echo base_url(); ?>assets/img/careers2.png">
							  <div class="content-details fadeIn-bottom">
								<h3 class="content-title">Eversmart Benefits</h3>
								
							  </div>
							</a>
						  </div>
					
					</div>
				</div>

					
				</div>	
			</section>
		
		
			<!--------------- End Careers Download----------------------->

			
		<!--------------------Start breadcrumbs Care Section------------>
			<section class="cs_switching">
				<div class="container">
					<div class="row">
							<div class=" col-sm-10 mx-auto text-center">					
								<h1 class="cft_heading text-center">Do we have a role for you?</h1>
								<h2 style="text-center">Eversmart is truly revolutionising the energy and smart home industry</h2>
							</div>
					</div>
				
				<div class="row">
					<div class="col-sm-12">
						 <div id="accordion" class="accordion">
							<div class=" mb-0">
								<div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
									<a class="card-title">
										Graduate Energy Specialist
									</a>
								</div>
								<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
									<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
										aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
										craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
									</p>
								</div>
								<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<a class="card-title">
									 Head of Product
									</a>
								</div>
								<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
									<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
										aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
										craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
									</p>
								</div>
								<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<a class="card-title">
									  Illustrator
									</a>
								</div>
								<div id="collapseThree" class="collapse" data-parent="#accordion" >
									<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
										aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.
									</div>
								</div>
								<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<a class="card-title">
									  Illustrator
									</a>
								</div>
								<div id="collapseFour" class="collapse" data-parent="#accordion" >
									<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
										aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.
									</div>
								</div>
								<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									<a class="card-title">
									  Illustrator
									</a>
								</div>
								<div id="collapseFive" class="collapse" data-parent="#accordion" >
									<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
										aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. samus labore sustainable VHS.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
					
					
					
					
				</div>
									
			</section>
<!--------------------End breadcrumbs Care Section------------->

		
<!----------------Start Footer-------------------------->
	<footer class="main_footer">
		<div class="container">
			<div class="inner_footer">
				<div class="row">
					<div class="col-sm-4 col-md-3">
						<h4>eversmart.</h4>
						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
					</div>
					<div class="col-sm-4 col-md-2">
						<h4>Services</h4>
							<ul class="links-vertical">
								<li><a href="#">Energy</a></li>
								<li><a href="#">Smart Meters</a></li>
								<li><a href="#">Home Services</a></li>
							</ul>					
					</div>
					<div class="col-sm-4 col-md-2">
							<h4>Company</h4>
							<ul class="links-vertical">
								<li><a href="#">Contact</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
							</ul>
					</div>
					<div class="col-sm-4 col-md-2">
							<h4>Support</h4>
							<ul class="links-vertical">
								<li><a href="#">Help & FAQs</a></li>
								<li><a href="#">Contact Us</a></li>								
							</ul>
							<h5>Opening Times: </h5>
							<p>MON - FRI 8am-8pm SAT 9am-5pm <br> SUN 9am-5pm </p>
					</div>
					
					<div class="col-sm-4 col-md-3">
						<h4>Contact Us</h4>
						<ul class="links-vertical">
								<li><i class="material-icons">call</i>  <a href="tel:03301027901"> 0330 102 7901</a></li>
								<li> <i class="material-icons">email</i><a href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
							</ul>
					
					</div>
				
				</div>			
			</div>	
			
			<div class="social-footer text-center">
				<ul class="social-buttons socialButtonHome">
					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    
				</ul>
			</div>
			<!----End Social Footer------------>
			
			<div class="copyright text-center">
					&copy; 2018 Eversmart Energy Ltd - 09310427
				</div>
			
		</div>
		<!---------end Footer container------------>
	
	</footer>
	<!----end footer------------>
	
	</div>
	<!--------------End Main-----------------> 

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
	<script src="assets/js/eversmart.js?v=6" type="text/javascript"></script>
	<script type="text/javascript">
		$('#myCarousel').carousel({
			interval: 2000
		});
		</script>
</body>

</html>

