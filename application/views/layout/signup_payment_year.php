<div class="col-sm-12 pd0">
			<div class="tab-content " id="tab-data-box">
				<span class="pay-pay-now text-center">Yearly Payment</span>
				<div class="amountyearly">£<?php echo $year_price ?></div>
			</div>
		</div>

<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
			<div class="content pd0">
				<div class="tabbable tabs-left row">
					<div class="col-sm-12 pd0">
						<div class="tab-content " id="tab-data-box">
							<div id="billing_error_msg"></div>
							<div class="tab-pane active" id="b">
								<span class="pay-pay-now text-center">Payment Details</span>
								<div id="existing_billing_address">
									<?php
									$BillingAddress = $this->user_modal->get_customer_billing_addresses( $this->session->userdata('login_data')['customer_id'] );
									if(count($BillingAddress)>0) { ?>
										<select id="billing_address_id" name="billing_address_id">
											<option value="" selected="selected"><?=$this->session->userdata('login_data')['address']; ?>, <?=$this->session->userdata('login_data')['town_address']; ?>, <?=$this->session->userdata('login_data')['address_postcode']; ?> </option>
											<?php foreach ($BillingAddress as $key => $BillingAddressData) { ?>
												<option value="<?=$BillingAddressData['billing_address_id']?>"><?=$BillingAddressData['address_line_1']?>, <?=$BillingAddressData['address_line_2']?>, <?=$BillingAddressData['postcode']?></option>
											<?php } ?>
										</select>
									<?php } ?>
								</div>
								<input type="checkbox" name="toggle_new_card_address" id="toggle_new_card_address" style="display:none" />
								<span class="pay-form-db-tab">
								
										<div class="row">
											<div class="col-lg-12  col-sm-12 col-md-12">
												<div style="position:relative">
													<div id="card-type-icon1"  style="display: none;">
														<i class="fa fa-cc-visa" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
														<i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
													</div>
													<div id="card-element" class="StripeElement"></div>
												</div>
												<input type="hidden" id="amount_pay" value="<?php echo $year_price ; ?>">
											
												<input type="hidden" id="existing_line1" value="">
												<input type="hidden" id="existing_postcode" value="">
												<input type="hidden" id="payment-email" value=">">
											
												<input class="StripeElement ElementsApp InputElement" id="example1-name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" required="" autocomplete="name" novalidate>
												<div id="card-dd_expire"></div>
												<div id="card-dd_ccv"></div>
												<!-- Used to display form errors. -->
												<div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>
											</div>
											<div class="col-lg-12  col-sm-12 col-md-12 text-center">
												<button type="submit" id="stripebtn" style="margin-top:45px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">Submit Payment</button>
											</div>
										</div>
									
								</span>
							</div>
						</div>
					</div>
					<span class="pay-detail text-center">
						Security is one of the biggest considerations in everything we do. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. 
					</span>
				</div>
			</div>
		</div>