
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">Please provide a meter reading</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            We’re just getting your bill ready, and we need you to provide a <strong>meter reading</strong>. This will help keep you energy bills fair & accurate.<br><br>
                                                            The quickest & easiest way to submit a reading is to do it online in our client portal<br><br>
                                                            If you need any help, simply email <a href="mailto:hello@eversmartenergy.co.uk" target="_blank">hello@eversmartenergy.co.uk</a>. You can also find help & advice in our <a href="<?php echo base_url(); ?>index.php/Helpfaqs" target="_blank">FAQs page</a> and the <a href="<?php echo base_url(); ?>index.php/" target="_blank">Eversmart Community forum</a><br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->