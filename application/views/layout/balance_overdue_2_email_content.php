
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">Your balance is still overdue</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            <strong>Customer No: <?= $email_info['customer_number']; ?></strong><br>
                                                            <strong>Bill No: <?= $email_info['bill_number']; ?></strong><br><br>
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            We are still waiting for payment for your most recent invoice(s) and your account balance is now <strong>significantly overdue</strong>.<br><br>
                                                            To avoid any further action being taken, please make a payment as soon as possible. We have attached a letter explaining the different ways you can pay.<br><br>
                                                            If you have recently made a payment then thank you – you can ignore this email.<br><br>
                                                            <strong>Any questions or problems? </strong> Feel free to contact us at <a href="mailto:hello@eversmartenergy.co.uk" target="_blank">hello@eversmartenergy.co.uk</a>.<br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->