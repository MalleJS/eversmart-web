
<!------------------------------- START MAIN BODY ------------------------------->

<tr>
    <td valign="top" id="templateBody">

        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
            <tbody class="mcnTextBlockOuter">
            <tr>
                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                        <tr>
                            <td valign="top" width="600" style="width:600px;">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                            <h1 style="margin: 50px 0px; text-align: left;">
                                                <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">Moving home</span></span>
                                            </h1>

                                            <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            Hello Ops,<br><br>
                                                            A customer has informed us that they are moving home, here are the details: <br><br>
                                                            Junifer ID: <strong><?= $email_info['account_id']; ?></strong><br>
                                                            Name: <strong><?= $email_info['forename']. ' ' .$email_info['surname']; ?></strong><br>
                                                            New Address: <strong><?= $email_info['new_address']; ?></strong><br>
                                                            Moving Date: <strong><?= $email_info['moving_date']; ?></strong><br>
                                                            Last Electricity Reading: <strong><?= $email_info['last_reading_elec']; ?></strong><br>
                                                            Last Gas Reading: <strong><?= $email_info['last_reading_gas']; ?></strong><br><br>
                                                            Thank you<br><br>
                                                            The geniuses from downstairs.
                                                        </span>
                                            </font>
                                            <br><br><br>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<!------------------------------- END MAIN BODY ------------------------------->