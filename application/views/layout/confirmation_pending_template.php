<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 	
  
    <style>
        .checkmark{width: 56px;
        height: 56px;
        border-radius: 50%;
        display: block;
        stroke-width: 2;
        stroke: #fff;
        stroke-miterlimit: 10;
        margin: 60px auto; position: relative;}
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T44QPZV');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <!-- Start your project here-->

    <div class="main-rounded-red-header">

        <div class="red-rounded-wave">

            <div class="topbar" id="red_top">

                <?php $this->load->view('layout/menu'); ?>

            </div>


            <section class="postcode_top mt0">

                <div class="postcode_box" id="background-none">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-8 mx-auto mtop60 posi-rele">

                                <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">

                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark_circle_error" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark_check" fill="none" d="M16 16 36 36 M36 16 16 36"/>
                                    </svg>

                                    <div class="content" style="padding:20px 0px 60px">
                                        <div class="col-md-10 mx-auto">
                                            <span class="registration-page-heading-energy-H">Oops!</span>
                                            <span class="registration-page-energy-d confirm-pay" style="margin-bottom: 50px;"><?=$message?></span>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>

        </div>

    </div>

    <!-- End main section -->


		
    <!----------------Start Footer-------------------------->

    <footer class="main_footer">
        <div class="footer-top-sky-rounded"></div>
        <div class="container">
            <div class="inner_footer">
                <div class="row">
                    <div class="col-sm-4 col-md-3 aos-item" data-aos="fade-right">
                        <h4>eversmart.</h4>
                        <p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart
                            revolution, providing flexible, low cost energy with smart technology and exceptional
                            customer service.</p>
                        <h5>Opening Times: </h5>
                        <p>MON - FRI 8am-8pm <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-up">
                        <h4>Services</h4>
                        <ul class="links-vertical">
                            <li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
                            <li><a href="#">Smart Meters</a></li>
                            <li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-down">
                        <h4>Useful Links</h4>
                        <ul class="links-vertical">
                            <li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_eng.pdf ">Consumer Checklist (UK)</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_wel.pdf ">Consumer Checklist (Welsh)</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-up">
                        <h4>Support</h4>
                        <ul class="links-vertical">
                            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & Support</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
                        </ul>

                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 3 aos-item" data-aos="fade-left">
                        <h4>Contact Us</h4>
                        <ul class="links-vertical" id="small-lower-text">
                            <li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i> 0330 102 7901</a></li>
                            <li><i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry">hello@eversmartenergy.co.uk</a></li>
                            <li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="social-footer text-center">
                <ul class="social-buttons socialButtonHome">
                    <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
            <!----End Social Footer------------->

            <div class="copyright text-center">
                &copy; 2018 Eversmart Energy Ltd - 09310427
            </div>

        </div>
        <!---------end Footer container------------>

    </footer>

    <!----------------End Footer-------------------------->


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mdb.min.js"></script>
    <!-- common JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/common.js"></script>

    <script type="text/javascript">
  	
        $(function() {
            $("#password_visibility").click(function(){
                var pass_input = document.getElementById("password_input");
                if (pass_input.type === "password") {
                    pass_input.type = "text";
                    $(this).removeClass("fa-eye").addClass("fa-eye-slash")
                } else {
                    pass_input.type = "password";
                    $(this).removeClass("fa-eye-slash").addClass("fa-eye")
                }
            });

            $('#continue_login').click(function(){
                window.location.href='<?php echo base_url() ?>index.php/user/login';
            });
        });
    </script>

</body>
</html>

