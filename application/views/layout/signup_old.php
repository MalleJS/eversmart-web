<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

  <style>
  .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid blue;
    border-bottom: 16px solid blue;
    width: 30px;
    height: 30px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  </style>

  
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->

	<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">

			<?php $this->load->view('layout/menu'); ?>
			<!--- End container-------->

		</div>
		<!---end top bar bar----------->


<!----------------start postcode--------------------->


<!----------------End postcode--------------------->

			<!----------------start postcode--------------------->

		<section class="postcode_top mt0">
				<div class="postcode_box" id="background-none">

						<div class="container">
              <form action="JavaScript:void(0)" id="registraion-form" class="switch-form" name="">
              <div id="step_1">
							<div class="row">
								<div class="col-md-8 mx-auto mtop60">
								<span class="back_step" style="position: absolute;"><a href="boiler-switch-five.html" class="material-icons backknowusage" >
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"></a></span>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<div class="registration-page-heading">
										Enter your Details<br><span>(* Required Fields)</span>
										</div>
											<div class="md-form col-md-3 mb-4" style="float:left">
											<select class="custom-select usage_select" id="red-arrow" name="customer_title" required>
													<option selected="">Title</option>
													<option value="1">Mr.</option>
													<option value="2">Mrs.</option>
													<option value="3">Miss</option>
												</select>
										</div>

												<div class="col-md-9 mb-4" style="float:left">
													<div class="md-form">
														<input id="form1" class="form-control" type="text" required name="fname">
														<label for="form1" class="">First Name<span class="required-field">*</span></label>
													</div>

												</div>

												<div class="col-md-12 mb-4" style=" clear:both">
													<div class="md-form" >
														<input id="form1" class="form-control" type="text" name="lname">
														<label for="form1" class="">Last Name<span class="required-field">*</span></label>
													</div>

												</div>


												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="form1" class="form-control" type="email" name="email">
														<label for="form1" class="">Email Address<span class="required-field">*</span></label>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">

														<input id="password" class="form-control" type="password" name="customer_password">
														<label for="form1" class="">Enter Password<span class="required-field">*</span><i class="material-icons hide" id="removepassword">remove_red_eye</i></label>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="confirm_password" class="form-control" type="password" name="customer_confirm_password">
														<label for="form1" class="">Confirm Password<span class="required-field">*</span></label>
													</div>

												</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="form1" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="telephone">
														<label for="form1" class="">Telephone Number<span class="required-field">*</span></label>
													</div>
													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="form1" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
														<label for="form1" class="">Phone Number &nbsp;<span class="required-field-option">(Optional)</span></label>
													</div>
													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="datepicker" class="form-control " type="text" name="customer_dob">
														<label for="form1" class="date-icon">Date of Birth<span class="required-field">*</span></label>
													</div>
													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input id="form1" class="form-control" type="text" name="customer_address" value="<?php echo $signup_info['quotation_address']; ?>">
														<label for="form1" class="">Supply Address<span class="required-field">*</span></label>
													</div>
                        </div>
                        <div class="error_msg">

                        </div>
												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:50px;">
															<button style="font-size:18px; " id="next_step" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Your Payment Details</button>
												</span>
                        </div>


											</div>
										</div>
									</div>

								</div>
                <!-- row  -->
						</div>
            <!-- step 1 -->
            <div id="step_2" style="display:none">
              <div class="row">
								<div class="col-md-8 mx-auto mtop60">
								<span class="back_step"><a href="javascript:void()" id="back_from" class="material-icons" style="top:-138px;">
									<img src="<?php echo base_url();?>assets/images/back.svg"></a></span>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:20px 0px 60px">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy-H">
										Bank Details for 12 Months
										</span>
										<span class="registration-page-energy-p">
										 	&#163; 100.55
										</span>

										<span class="registration-page-energy-d">
										Your payments will be the same every month, and we'll take them direct from bank
										</span>

											</div>
										</div>
									</div>

								</div>
						</div>
            <div class="row">
								<div class="col-md-8 mx-auto">
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy">
										Because you selected Direct Debit as the payment mode <br>
										for Energy Payment, we require you're bank details
										</span>
												<div class="col-md-12 " style="float:left; width:100%">
													<div class="md-form" >
														<input class="form-control" type="text" name="account_number" id='account_number' onkeypress="return isNumberKey(event)">
														<label for="form1" class="">Your Bank or society account Number<span class="required-field">*</span></label>
													</div>
												</div>
												<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
														<input class="form-control" type="text" name="sort_code" id="sort_code" onkeypress="return isNumberKey(event)">
														<label for="form1" class="">Sort Code<span class="required-field">*</span></label>
													</div>
												</div>
													<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
														<input class="form-control" type="text" name="account_name" id="account_name">
														<label for="form1" class="">Name of Account Holder<span class="required-field">*</span></label>
													</div>
													</div>

                          <div class="signup_msg" style="clear:both"></div>
												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:30px;">
															<button style="font-size:18px; " class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
															</span>
															<span class="read-direct">Read the Direct Debit Gaurantee</span>
											</div>
										</div>
									</div>
								</div>
						</div>
            </div>
            <!-- step 2 -->
          </form>
        </div> <!-- container -->
						</div>
					</section>
<!----------------End postcode--------------------->


		</div>
		</div>

		<!--------------------Start breadcrumbs Care Section------------->



<!--------------------End breadcrumbs Care Section------------->


<!----------------Start Footer--------------------------->


    	<footer class="main_footer">
		<div class="footer-top-sky-rounded"></div>
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row">
    					<div class="col-sm-4 col-md-3">
    						<h4>eversmart.</h4>
    						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>

							<h5>Opening Times: </h5>
    						<p>MON - FRI 8am-8pm <br> SAT - SUN 9am-5pm </p>
    					</div>
    					<div class="col-sm-4 col-md-2">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="#">Energy</a></li>
    								<li><a href="#">Smart Meters</a></li>
    								<li><a href="#">Home Services</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Company</h4>
    							<ul class="links-vertical">
    								<li><a href="#">Contact</a></li>
    								<li><a href="#">Terms &amp; Conditions</a></li>
    								<li><a href="#">Privacy</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="#">Help & Support</a></li>
    								<li><a href="#">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-4 col-md-3">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical">
    								<li><i class="material-icons">call</i>  <a href="tel:03301027901"> 0330 102 7901</a></li>
    								<li> <i class="material-icons">email</i><a href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; 2018 Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>

    	</footer>
    	<!----end footer------------>


      <!-- SCRIPTS -->
      <!-- JQuery -->
      <!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script> -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
      <!-- Bootstrap tooltips -->
      <!-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script> -->

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
      <!-- common JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>
      <!------ Include the above in your HEAD tag ---------->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>



  	<script type="text/javascript">

$(function() {
  $("#password_visibility").click(function(){
        var pass_input = document.getElementById("password_input");
        if (pass_input.type === "password") {
            pass_input.type = "text";
            $(this).removeClass("fa-eye").addClass("fa-eye-slash")
        } else {
            pass_input.type = "password";
            $(this).removeClass("fa-eye-slash").addClass("fa-eye")
        }
   });

});

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : evt.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

$(document).ready(function(){
      $('#datepicker').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
      });

      $('#back_from').click(function(){
      //$(document).on('click', '#back_from', function(){
        $('#step_1').fadeIn();
        $('#step_2').fadeOut();
        $('#registraion-form-2').attr('id','registraion-form');
        $('#next_step').attr('type','submit');
    });

      $('#registraion-form').submit(function(e){
      //$(document).on('submit', '#registraion-form', function(e){
          e.preventDefault();
          //alert($('#account_number').val());
          $('.error_msg').empty();
          if( $('#password').val() != $('#confirm_password').val()  ){
            $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Password did not match.</div>');
          }else{
            $('#step_1').fadeOut();
            $('#step_2').fadeIn();
            //$('#registraion-form').attr('id','second_from');
            $('#next_step').attr('type','button');
            if( $('#account_number').val() && $('#account_name').val()  && $('#sort_code').val() )
            {
              $.ajax({
                  url: "<?php echo base_url(); ?>index.php/user/enroll_customer",
                  type: 'post',
                  dataType: 'json',
                  data: $('#registraion-form').serialize(),
                  beforeSend: function(){
                    $(".signup_msg").html('<center><div class="loader"></div></center>');
                  },
                  success:function(response){
                    //console.log(response);
                    $('.signup_msg').empty();
                    if( response.error == '0' ){
                      $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                    }
                    if( response.error == '1' ){
                      $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Server Error. Please try after sometime.</div>');
                    }
                  }
              });
            }
          }


      });
});
  		</script>
  </body>
  </html>
