<footer class="main_footer">
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row ie-fix">
    					<div class="col-sm-12 col-md-3 aos-item" data-aos="fade-right"> 
                            <!--<h4 id="main_footer_logo">eversmart.</h4> -->
                            <img class="main_footer_logo" src="<?php echo base_url(); ?>assets/images/logo-eversmart-new2.svg"/>  
    						<p>We’re a family-run energy business set up to take better care of people. We protect our customers from high prices and poor service</p>
							<h5>Opening Times: </h5>
							<p>WEEKDAYS: 8AM-6PM
                            <br>SATURDAYS: 9AM-1PM</p>
                            <h5>Emergency Only: </h5>
                            <p>WEEKDAYS: 6PM-8PM
                            <br>SATURDAYS: 1PM-5PM
                            <br>SUNDAYS: 9AM-5PM</p>
    					</div>
    					<div class="col-sm-12 col-md-2 aos-item" data-aos="fade-up">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
    								<!--<li><a href="#">Smart Meters</a></li> -->
    								<!--<li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li>-->
    							</ul>
    					</div>
    					<div class="col-sm-12 col-md-2 aos-item" data-aos="fade-down">
    							<h4>Useful Links</h4>
    							<ul class="links-vertical"> 
									<li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
    								<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/NEW_TCs.pdf">Terms &amp; Conditions</a></li>
									<li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
                                    <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/eversmart_charge_list.pdf">Energy Charge List</a></li>
                                    <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/priority_services_register_policy.pdf">Priority Services Register</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_eng.pdf">Consumer Checklist (UK)</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_wel.pdf">Consumer Checklist (Welsh)</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-12 col-md-2 aos-item" data-aos="fade-up">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & Support</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-12 col-md-3 col-lg-3 aos-item" data-aos="fade-left">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical" id="small-lower-text">
                                    <li id="contact-us-break"><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i>   0330 102 7901</a></li>
									<li><i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
									<li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; <?php Date('Y') ?> Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>
 
    	</footer>
        <!----end footer------------>


        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
        <!-- common JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>

        <script src="<?php echo base_url(); ?>/assets/js/aos.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>

        <!-- animation JavaScript -->
        <script src="<?php echo base_url(); ?>assets/js/aos.js"></script>

        <script>
        AOS.init({
        easing: 'ease-in-out-sine',
        delay: 100,
        });
        </script>

        <script type="text/javascript">
            $('#myCarousel').carousel({
                interval: 2000
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".facebookIconHome").click(function () {
                $(".facebookVideo").toggle();
                });

                $(".facebookIconHome").click(function () {
                $(this).addClass("fullColor");
                $('.smsIconHome').removeClass('fullColor');
                $('.twitterIconHome').removeClass('fullColor');
                $('.telegramIconHome').removeClass('fullColor');
                $(".fbImage").addClass('fbImageNew');
                $(".smsImage").removeClass('smsImageNew');
                });
                $(".smsIconHome").click(function () {
                $(this).addClass("fullColor");
                $('.facebookIconHome').removeClass('fullColor');
                $('.twitterIconHome').removeClass('fullColor');
                $('.telegramIconHome').removeClass('fullColor');
                $(".smsImage").addClass('smsImageNew');
                $(".fbImage").removeClass('fbImageNew');
                });

                //$(".twitterIconHome").click(function () {
                //                $(this).addClass("fullColor");
                //				$('.smsIconHome').removeClass('fullColor');
                //				$('.facebookIconHome').removeClass('fullColor');
                //				$('.telegramIconHome').removeClass('fullColor');
                //            });
                //			$(".telegramIconHome").click(function () {
                //                $(this).addClass("fullColor");
                //				$('.smsIconHome').removeClass('fullColor');
                //				$('.twitterIconHome').removeClass('fullColor');
                //				$('.facebookIconHome').removeClass('fullColor');
                //            });

                $('[data-toggle="tooltipmsn"]').tooltip();

                <?php if(!isset($this->session->userdata('login_data')['intercom_id'])){ ?>
                    // Shut down and reboot when there is no session data
                    window.Intercom('shutdown');
                    window.Intercom('boot', { app_id: "qjs70gbf" });
                <?php } ?>

            });

        </script>
        

        <!-------------- End Intercom ------------->
        <script>

            window.intercomSettings = {
                app_id: "qjs70gbf",
                custom_launcher_selector: ".open_intercom"
                <?php if(isset($this->session->userdata('login_data')['intercom_user_id'])){ ?>
                    , user_id: "<?=$this->session->userdata('login_data')['intercom_user_id'];?>"
                <?php } ?>
                <?php if(isset($this->session->userdata('login_data')['intercom_id'])){ ?>
                    , id: "<?=$this->session->userdata('login_data')['intercom_id'];?>"
                <?php } ?>
                <?php if(isset($this->session->userdata('login_data')['intercom_name'])){ ?>
                    , name: "<?=$this->session->userdata('login_data')['intercom_name'];?>"
                <?php } ?>
                <?php if(isset($this->session->userdata('login_data')['intercom_email'])){ ?>
                    , email: "<?=$this->session->userdata('login_data')['intercom_email'];?>"
                <?php } ?>
            };
        </script>

        <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qjs70gbf';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
        <!-------------- End Intercom ------------->

    </body>
</html>
