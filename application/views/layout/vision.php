<!----------------start heading-------------------->
<section class="mt0 ">
   <div class="boiler_header-h">
      <div class="container">
         <div class="row">
            <div class="col-md-10 mx-auto text-center">
               <span class="bolier_home-h-l">Our Vision</span>
            </div>
         </div>
      </div>
   </div>
</section>
<!----------------End heading-------------------->		
</div>
</div>
<!--------------------start first Section---->
<section class="career-shape-two-edit" id="vision_two_section">
   <div class="row play_store_margin">
   
   	  <div class="col-sm-12 vision_mobile">
         <div class=" padding-group-img">
            <img style="margin: 0 auto;" class="mobile_phone_img "  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_one.png" />
			
			<img  style="margin: 20px auto;" class="ipad_img "  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_one_ipad.png" />
           
         </div>
      </div>
      <div class="col-md-6" id="family-img">
         <div class=" padding-group-img">
            <img class="our-mission-background-img story_image2" style="width: 86%" src="<?php echo base_url(); ?>assets/images/career/vision_1_bg.png" />
            <img class="our-mission-background-img story_image1" style="width: 86%" src="<?php echo base_url(); ?>assets/images/career/vision_one.png" />
         </div>
      </div>
	  

      <div class="col-lg-6 col-sm-12 col-md-12">
         <div class="container mobilecon vision_group" style="padding-top: 50px;">
            <h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick" data-aos="fade-in">A family affair from the beginning
            </h2>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">Both as consumers, and people who’ve worked in the sector, we know first-hand what it feels like to be let down by the energy industry.</p>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">
               That’s why our CEO, Barney Cook, set out to repair the relationship between people and their energy suppliers.
            </p>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">Eversmart has been a family affair right from the start and we’re here to protect our customers from high prices and poor service.
            </p>
         </div>
      </div>
   </div>
</section>
<!---------------end first Section------------------->
<!--------------------start third Section---->
<section class="career-shape-two-edit" id="vision_section_three">
   <div class="row play_store_margin">
     <div class="col-sm-12 vision_mobile">
         <div class=" padding-group-img">
            <img style="float:right" class="ipad_img"  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_two_ipad.png" />
			 <img class="mobile_phone_img"  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_two.png" />
           
         </div>
      </div>
      <div class="col-lg-6 col-sm-12 col-md-12">
         <div class="container mobilecon vision_group">
            <h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick" data-aos="fade-in">Finding <br> a better way</h2>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">We can proudly say that we’re one of the cheapest energy suppliers out there. But great prices are just the start – we want to take better care of people.</p>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">
               That’s because our customers are more than just a number on a piece of paper; they are someone’s mum, dad, brother, sister, son, daughter, nan, grandad or best friend. We promise to take care of them as we would our own family.
            </p>
         </div>
      </div>
      <div class="col-md-6" id="family-img">
         <div class=" padding-group-img">
            <img class="our-mission-background-img vision_two_bg" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/vision_2_bg.png" />
            <img class="our-mission-background-img vision_two_img" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/vision_2_img.png" />
         </div>
      </div>
	  
	  
   </div>
   <!------------------End Container----------------->					
</section>
<section class="career-shape-two-edit" id="vision_section_four">
   <div class="row play_store_margin">
   	    <div class="col-sm-12 vision_mobile">
         <div class=" padding-group-img">
            <img class="mobile_phone_img"  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_three.png" />
			
			  <img style="float:left;margin-left: -20px;padding: 30px 0px 60px" class=" ipad_img"  src="<?php echo base_url(); ?>assets/images/career/mobile/vision_three_ipad.png" />
           
         </div>
      </div>
	
      <div class="col-md-6" id="family-img">
         <div class=" padding-group-img">
            <img class="our-mission-background-img vision_third_image2" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/vision_third.png">
            <img class="our-mission-background-img vision_third_image1" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/vision_third_2.png">
         </div>
      </div>
	 
      <div class="col-lg-6 col-sm-12 col-md-12">
         <div class="container mobilecon vision_group" style="" id="vision_join_family">
            <h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick aos-init aos-animate" data-aos="fade-in">Join the family</h2>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text aos-init aos-animate" style="min-width: 100%;" data-aos="fade-right">Over 40,000 people have already joined the Eversmart family – and we’re committed to giving those people the service and care they deserve.</p>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text aos-init" style="min-width: 100%;" data-aos="fade-right">
               We’re always looking for new ways to make life a little easier for people. Whether that’s giving you £50 credit when you refer a friend, or adding interest to your credit balance when you pay in advance.
            </p>
            <p class="hook aos-item  spacing-redesign font-style-quick career_text aos-init" style="min-width: 100%;" data-aos="fade-right">
               If you’re ready to join us simply give us a call, drop us a line on social media or get a quote online. We can’t wait to hear from you!
            </p>
         </div>
      </div>
	  

	  
	 
   </div>
   <!------------------End Container----------------->					
</section>