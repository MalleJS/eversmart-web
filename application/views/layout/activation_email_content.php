
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">Welcome aboard!</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            It’s official - your switch to Eversmart Energy has begun!<br><br>
                                                            Please click the ‘Activate’ button below.<br><br><br>
                                                            <table align="center">
                                                                <tr>
                                                                    <td style="width: 280px; height: 40px; background-color: #EA495C; text-align: center; border-radius: 6px;">
                                                                        <a style="width: 280px; height: 40px; text-decoration: none; color: white; border-radius: 6px; font-size:17px;" href="http://portal.eversmartenergy.co.uk/#/login/?t=<?php echo $email_info['token']; ?>">Activate</a>
                                                                    </td>
                                                                </tr>
                                                            </table><br><br>
                                                            Alternatively follow the link to activate your account.<br><br>
                                                            <a style="text-decoration: none; color: #202020;" href="http://portal.eversmartenergy.co.uk/#/login/?t=<?php echo $email_info['token']; ?>">https://portal.eversmartenergy.co.uk/#/login/?t=<?php echo $email_info['token']; ?></a><br><br>
                                                            <strong>Have any questions?</strong>Just drop us a line at <a href="mailto:hello@eversmartenergy.co.uk" target="_blank">hello@eversmartenergy.co.uk</a><br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->
