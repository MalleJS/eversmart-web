<?php if ($signup_info['quotation_pay_energy'] === 'prepay') {?>
    <form action="JavaScript:void(0)" id="registraion-form-dyball" class="switch-form" name="">
        <div id="step_1">
            <div class="row">
                <div class="col-md-8 mx-auto main-top-margin-60">
                    <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                        <div class="content" style="padding:60px 0px">
                            <div class="col-md-10 mx-auto">
                                <div class="registration-page-heading">
                                    Enter your Details<br><span>(All fields are required except phone number)</span>
                                </div>
                                <div class="md-form col-md-3 mb-4" style="float:left">
                                    <select class="custom-select usage_select" id="red-arrow" name="customer_title"
                                        tabindex="0" required>
                                        <option value="">Title</option>
                                        <option value="Mr" selected>Mr.</option>
                                        <option value="Mrs">Mrs.</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Prof">Prof</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                </div>
                                <div class="col-md-9 mb-4" style="float:left">
                                    <div class="md-form">
                                        <input tabindex="1" class="form-control check_first" type="text" required
                                            name="fname" placeholder="First Name">
                                        <div class="valid_first" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="first_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4" style=" clear:both">
                                    <div class="md-form">
                                        <input tabindex="2" placeholder="Last Name" id="form1"
                                            class="form-control check_last" type="text" name="lname" required>
                                        <div class="valid_last" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="last_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="3" placeholder="Email Address" id="email" class="form-control"
                                            type="email" name="email" required>
                                        <div class="valid_email" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="email_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form show_hide_password">
                                        <input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)" id="show_password">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                                        <div class="valid_confirm_password" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="confirm_password_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone" required>
                                        <div class="valid_telephone" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="telephone_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" type="text" name="customer_phone">
                                        <div class="valid_phone_number" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="phone_number_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob" required>
                                        <div class="valid_datepicker" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                                        <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                                        <?php } ?>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
                                    </div>
                                </div>
                                <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                                <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                                <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                                <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">
                                <div class="input-wrap prs_flag">
                                    <input type="checkbox" name="psr_flag" value="1" />
                                    <label for="psr_flag">Are you eligible for Priority Register Service?</label>
                                </div>
                                <div style="text-align:center">
                                    <form id="complete_form_dyball">
                                        <button style="font-size:18px; padding:10px 84px; width:100%;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
<?php } elseif ($monthyear === 'yearlypay') {?>
    <form action="JavaScript:void(0)" id="registraion-form-yearpay" class="switch-form" name="">
        <div id="step_1">
            <div class="row">
                <div class="col-md-8 mx-auto main-top-margin-60">
                    <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                        <div class="content" style="padding:60px 0px">
                            <div class="col-md-10 mx-auto">
                                <div class="registration-page-heading">
                                    Enter your Details<br><span>(All fields are required except phone number)</span>
                                </div>
                                <div class="md-form col-md-3 mb-4" style="float:left">
                                    <select class="custom-select usage_select" id="red-arrow" name="customer_title"
                                        tabindex="0" required>
                                        <option value="">Title</option>
                                        <option value="Mr" selected>Mr.</option>
                                        <option value="Mrs">Mrs.</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Prof">Prof</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                </div>
                                <div class="col-md-9 mb-4" style="float:left">
                                    <div class="md-form">
                                        <input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
                                        <div class="valid_first" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="first_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4" style=" clear:both">
                                    <div class="md-form">
                                        <input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                                        <div class="valid_last" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="last_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                                        <div class="valid_email" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" /></a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="email_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form show_hide_password">
                                        <input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)" id="show_password">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                                        <div class="valid_confirm_password" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="confirm_password_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone" required>
                                        <div class="valid_telephone" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="telephone_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" type="text" name="customer_phone">
                                        <div class="valid_phone_number" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="phone_number_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob" required>
                                        <div class="valid_datepicker" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                                        <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                                        <?php } ?>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
                                        <div class="input-wrap prs_flag">
                                            <input type="checkbox" name="psr_flag" value="1" />
                                            <label for="psr_flag">Are you eligible for Priority Register Service?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="error_msg"></div>
                                <div class="signup_msg" style="clear:both"></div>
                                <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                                <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                                <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                                <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">
                                <div class="col-sm-12 pd0">
                                    <div class="tab-content " id="tab-data-box">
                                        <span class="pay-pay-now text-center">Yearly Payment</span>
                                        <div class="amountyearly">£<?php echo $year_price ?></div>
                                    </div>
                                </div>
                                <div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
                                    <div class="content pd0">
                                        <div class="tabbable tabs-left row">
                                            <div class="col-sm-12 pd0">
                                                <div class="tab-content " id="tab-data-box">
                                                    <div id="billing_error_msg"></div>
                                                    <div class="tab-pane active" id="b">
                                                        <span class="pay-pay-now text-center">Payment Details</span>
                                                        <div id="existing_billing_address">
                                                            <?php
                                                                $BillingAddress = $this->user_modal->get_customer_billing_addresses( $this->session->userdata('login_data')['customer_id'] );
                                                                if(count($BillingAddress)>0) {
                                                            ?>
                                                                <select id="billing_address_id" name="billing_address_id">
                                                                    <option value="" selected="selected"> <?=$this->session->userdata('login_data')['address']; ?>, <?=$this->session->userdata('login_data')['town_address']; ?>, <?=$this->session->userdata('login_data')['address_postcode']; ?></option>
                                                                    <?php foreach ($BillingAddress as $key => $BillingAddressData) { ?>
                                                                        <option value="<?=$BillingAddressData['billing_address_id']?>"> <?=$BillingAddressData['address_line_1']?>, <?=$BillingAddressData['address_line_2']?>, <?=$BillingAddressData['postcode']?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            <?php } ?>
                                                        </div>
                                                        <input type="checkbox" name="toggle_new_card_address" id="toggle_new_card_address" style="display:none" />
                                                        <span class="pay-form-db-tab">
                                                            <div class="row">
                                                                <div class="col-lg-12  col-sm-12 col-md-12">
                                                                    <div style="position:relative">
                                                                        <div id="card-type-icon1" style="display: none;">
                                                                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
                                                                            <i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
                                                                        </div>
                                                                        <div id="card-element" class="StripeElement"></div>
                                                                    </div>
                                                                    <input type="hidden" id="amount_pay" value="<?php echo $year_price ; ?>">
                                                                    <input type="hidden" id="existing_line1" value="">
                                                                    <input type="hidden" id="existing_postcode" value="">
                                                                    <input type="hidden" id="payment-email" value=">">
                                                                    <input class="StripeElement ElementsApp InputElement" id="example1-name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" required="" autocomplete="name" novalidate>
                                                                    <div id="card-dd_expire"></div>
                                                                    <div id="card-dd_ccv"></div>
                                                                    <div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>
                                                                </div>
                                                                <div class="col-lg-12  col-sm-12 col-md-12 text-center">
                                                                    <div id="response-msg"></div>
                                                                    <button type="submit" id="stripebtn" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">Submit Payment</button>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="pay-detail text-center">
                                                Security is one of the biggest considerations in everything we do. Stripe
                                                has been audited by a PCI-certified auditor and is certified to PCI Service
                                                Provider Level 1. This is the most stringent level of certification
                                                available in the payments industry.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <button id='signup-continue-yearlypayments' style="font-size:18px;padding:10px 84px;float: none;text-align: center;margin-left: auto;margin-right: auto!important;/* position: absolute; */left: 0;right: 0;margin-left: auto!important;width: 100%;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="thank_you">
                <div class="payment-details">
                    <div class="col-md-10 col-sm-12 mx-auto ">
                        <div class="gas_box_icon gap_icon_dashboard">
                            <span class="progress-bar-step" id="swith-process-bar">
                                <svg class="checkmark" xmlns="" viewBox="0 0 52 52">
                                    <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                    <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
                            </span>
                            <div class="content" style="padding:0px 0px 30px">
                                <div class="col-md-10 mx-auto">
                                    <span class="registration-page-heading-energy-H">
                                        <span id="Transaction_status"></span>
                                        <span id="Transaction"></span>
                                    </span>
                                    <span class="registration-page-energy-d confirm-pay" style="padding-top:0px!important">Please check your emails</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
<?php } else {?>
    <form action="JavaScript:void(0)" id="registraion-form" class="switch-form" name="">
        <div id="step_1">
            <div class="row">
                <div class="col-md-8 mx-auto main-top-margin-60">
                    <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                        <div class="content" style="padding:60px 0px">
                            <div class="col-md-10 mx-auto">
                                <div class="registration-page-heading">
                                    Enter your Details<br><span>(All fields are required except phone number)</span>
                                </div>
                                <div class="md-form col-md-3 mb-4" style="float:left">
                                    <select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
                                        <option value="">Title</option>
                                        <option value="Mr" selected>Mr.</option>
                                        <option value="Mrs">Mrs.</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Prof">Prof</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                </div>
                                <div class="col-md-9 mb-4" style="float:left">
                                    <div class="md-form">
                                        <input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
                                        <div class="valid_first" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="first_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4" style=" clear:both">
                                    <div class="md-form">
                                        <input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                                        <div class="valid_last" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="last_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                                        <div class="valid_email" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="email_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form show_hide_password">
                                        <input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)" id="show_password">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                                        <div class="valid_confirm_password" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="confirm_password_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone" required>
                                        <div class="valid_telephone" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="telephone_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" type="text" name="customer_phone">
                                        <div class="valid_phone_number" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                        <div style="display:none" class="phone_number_error">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob" required>
                                        <div class="valid_datepicker" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="md-form">
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                                        <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                                        <?php } ?>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>
                                        <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
                                        <div class="input-wrap prs_flag">
                                            <input type="checkbox" name="psr_flag" value="1" />
                                            <label for="psr_flag">Are you eligible for Priority Register Service?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="error_msg"></div>
                                <div class="signup_msg" style="clear:both"></div>
                                <span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:50px; ">
                                    <button style="font-size:18px; padding:10px 84px;width:100%;" id="next_step" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

<?php } ?>
    <div id="step_2" style="display:none">
        <div class="row">
            <div class="col-md-8 mx-auto main-top-margin-60">
                <span class="back_step">
                    <a href="javascript:void()" id="back_from" class="material-icons" style="top:-123px;">
                        <img src="<?php echo base_url();?>assets/images/red-back-button.svg">
                    </a>
                </span>
                <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                    <div class="content" style="padding:20px 0px 60px">
                        <div class="col-md-10 mx-auto">
                            <span class="registration-page-heading-energy-H"> Enter your Bank Details</span>
                            <span class="registration-page-energy-d" style="font-size:15px">Your payments will be the same every month, and we'll take them direct from bank</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                    <div class="content" style="padding:60px 0px">
                        <form id="complete_form">
                            <div class="col-md-10 mx-auto">
                                <span class="registration-page-heading-energy">
                                    Because you selected Direct Debit as the payment mode <br>
                                    for Energy Payment, we require you're bank details
                                </span>
                                <div id="form_submit" style="display:none"></div>
                                <div class="col-md-12 " style="float:left; width:100%">
                                    <div class="md-form">
                                        <div class="bank-titles">Account Number</div>
                                        <div class="account-wrap">
                                            <input type="text" maxlength="1" max="1" id="first-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="second-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="thrid-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="fourth-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="five-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="six-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="seven-account-number" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="eight-account-number" placeholder="0" onclick="this.select();" />
                                        </div>
                                        <input style="display:none" class="form-control" type="number" placeholder="Your Bank or society account Number" required name="account_number" id='account_number' maxlength="8">
                                        <div class="valid_account_number" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none" class="account_number_error">
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 " style=" float:left; width:100%">
                                    <div class="md-form">
                                        <div class="bank-titles">Sort Code</div>
                                        <div class="account-wrap">
                                            <input type="text" maxlength="1" max="1" id="first-account" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="second-account" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="thrid-account" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="fourth-account" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="five-account" placeholder="0" onclick="this.select();" />
                                            <input type="text" maxlength="1" max="1" id="six-account" placeholder="0" onclick="this.select();" />
                                        </div>
                                        <input style="display:none" class="form-control" required maxlength="8" placeholder="Sort Code" type="text" name="sort_code" id="sort_code">
                                        <div class="valid_sort_code" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none" class="sort_code_error">
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 " style=" float:left; width:100%">
                                    <div class="md-form">
                                        <input class="form-control" placeholder="Name of Account Holder" required type="text" name="account_name" id="account_name">
                                        <div class="valid_account_name" style="display:none">
                                            <div class="input-group-addon">
                                                <a href="javascript:void(0)">
                                                    <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none" class="account_name_error">
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0)">
                                                <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="error_msg" style="clear:both"></div>
                                <div class="signup_msg" style="clear:both"></div>
                                <span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:30px;">
                                    <div id="signup-load" style="display:none;width:100%; text-align:center; float:left; padding:30px 0px;">
                                        <div class="loader-signup"></div>
                                    </div>
                                    <button style="font-size:18px; padding:10px 84px; " class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
                                </span>
                                <span class="read-direct" style="margin-top:-18px;">Read the<a target="_blank" href="<?php echo base_url(); ?>assets/pdf/___Direct_Debit_Guarantee.pdf "> Direct Debit Gaurantee</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-------------- Start WebToCase  ------------>

<?php $this->load->view('dashboard/webtocase_form'); ?>

<!-------------- End WebToCase  ------------>




<script type="text/javascript">
$(function() {

    function appendJuniferStatus(response) {
        if (response.junifer_id > 0) {
            // Success
            $('#subject').val('Registration Success - Junifer Customer ID:' + response.junifer_id + ' - Success');
            $('#description').val('Registration Success for ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val());
            $('#00N1n00000SB9OK').val(response.junifer_id); // Junifer customer id
            $('#00N1n00000SB9Pc').val('Registration Success');
        } else {
            //  Pending
            $('#subject').val('Registration Pending - Junifer Customer - Pending');
            $('#description').val('Registration Pending for ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val());
            $('#00N1n00000SB9OK').val(''); // Junifer customer id
            $('#00N1n00000SB9Pc').val('Registration Pending');
        };
    };

    // Direct debit - Junifer (Monthly)
    $('#registraion-form').submit(function(e) {
        e.preventDefault();
        $('.error_msg').empty();

        if (!validateSignupForm()) { return; };

        $('#step_1').fadeOut();
        $('#step_2').fadeIn();

        // ALL VALIDATION PASSED
        // JUNIFER - SET THE WEB TO CASE FIELDS - WE USE THEM LATER (complete_form submit)
        $('#00N1n00000SB9PS').val($("#red-arrow option:selected").text() + ' ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val()); // Name
        $('#wtc_email').val($("input[name=email]").val());
        $('#phone').val($("input[name=telephone]").val());
        $('#00N1n00000SB9PN').val($("input[name=customer_dob]").val()); // DOB
        $('#00N1n00000SB9PI').val($("input[name=quotation_first_line_address]").val()); // Address line 1
        $('#00N1n00000SB9Ph').val($("input[name=quotation_postcode]").val()); // Postcode
        $("#recordType").val('0121n0000007GkH'); // New Junifer Account
        // JUNIFER - END WEB TO CASE FIELDS
    }); // End Junifer (Monthly)

    // Direct debit - Junifer (Yearly)
    function registrationYearPayHandler(e) {
        try { e.preventDefault() } catch (er) {};
        $('.error_msg').empty();

        if (!validateSignupForm()) { return; };

        // JUNIFER - SET THE WEB TO CASE FIELDS
        $('#00N1n00000SB9PS').val($("#red-arrow option:selected").text() + ' ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val()); // Name
        $('#wtc_email').val($("input[name=email]").val());
        $('#phone').val($("input[name=telephone]").val());
        $('#00N1n00000SB9PN').val($("input[name=customer_dob]").val()); // DOB
        $('#00N1n00000SB9PI').val($("input[name=quotation_first_line_address]").val()); // Address line 1
        $('#00N1n00000SB9Ph').val($("input[name=quotation_postcode]").val()); // Postcode
        // JUNIFER - END WEB TO CASE FIELDS

        REQUEST("<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: $('#registraion-form-yearpay').serialize()
        }).then(function(response) {
            switch (response.error) {
                case '0':
                    // Only do this on success response
                    appendJuniferStatus(response);
                    Promise.all([
                        REQUEST('<?php echo base_url() ?>index.php/user/web_to_case_log',
                            'none', { // Log web to case form
                                method: 'post',
                                headers: new Headers({ 'Content-Type': 'application/json' }),
                                body: JSON.stringify(webToCaseBody())
                            }),
                        REQUEST(
                            'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8',
                            'none', { // Send the web to case form
                                method: 'post',
                                mode: 'no-cors',
                                headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
                                body: serialiseJSON(webToCaseSalesforceBody())
                            })
                    ]).then(function() {
                        dataLayer.push({
                            'ecommerce': {
                                'purchase': {
                                    'actionField': {
                                        'id': '<?php echo $tariff_detail[' priceId ']; ?>',
                                        'affiliation': '<?php echo $tariff_detail[' supplierName ']; ?>',
                                        'revenue': '<?php echo $tariff_detail[' newSpend ']; ?>',
                                        'shipping': 'BLANK',
                                        'coupon': ''
                                    },
                                    'products': [{
                                        'name': '<?php echo $tariff_detail[' tariffName ']; ?>',
                                        'id': '<?php echo $tariff_detail[' priceId ']; ?>',
                                        'price': '<?php echo $tariff_detail[' newSpend ']; ?>',
                                        'brand': 'Quote Conversion',
                                        'category': '<?php echo $signup_info[' quotation_for ']; ?>',
                                        'variant': 'BLANK',
                                        'quantity': 1,
                                    }]
                                }
                            }
                        });

                        $('#registraion-form-yearpay')[0].reset();
                        $('.btnNew').css('display', 'none');
                        $('#back_from').hide();
                        window.location.href = "<?php echo base_url() ?>index.php/user/signup_success";
                        $('.signup_msg').html( '<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>' );
                    });

                break;
                case '1':
                case '2':
                    $('#response-msg').html('<div class="alert alert-danger" role="alert">' + response.msg + '. <br/ ><br/ >Please call <a href="tel:0161 332 0022">0161 332 0022</a> to proceed with your quotation.</div>' );
                break;
            }
        });

        $('.error_msg').empty();
        $('.signup_msg').empty();

    };

    function dlPush() {
        dataLayer.push({
            'ecommerce': {
                'purchase': {
                    'actionField': {
                        'id': '<?php echo $tariff_detail['priceId']; ?>',
                        'affiliation': '<?php echo $tariff_detail['supplierName']; ?>',
                        'revenue': '<?php echo $tariff_detail['newSpend']; ?>',
                        'shipping': 'BLANK',
                        'coupon': ''
                    },
                    'products': [{
                        'name': '<?php echo $tariff_detail['tariffName']; ?>',
                        'id': '<?php echo $tariff_detail['priceId']; ?>',
                        'price': '<?php echo $tariff_detail['newSpend']; ?>',
                        'brand': 'Quote Conversion',
                        'category': '<?php echo $signup_info['quotation_for']; ?>',
                        'variant': 'BLANK',
                        'quantity': 1,
                    }, ]
                }
            }
        });
    }

    //complete_form submit
    $(document).on('submit', '#complete_form', function(e) {

        e.preventDefault();

        if (!validateAccountDetails()) {
            return false;
        };

        REQUEST("<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
                method: 'post',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                }),
                body: $('#complete_form,#registraion-form').serialize()
            })
            .then(function(response) {
                switch (response.error) {
                    case '0':
                        appendJuniferStatus(response);
                        Promise.all([
                                REQUEST( '<?php echo base_url(); ?>index.php/user/web_to_case_log', 'none', {
                                        method: 'post',
                                        body: JSON.stringify(webToCaseBody())
                                    }),
                                REQUEST( 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'json', {
                                        method: 'post',
                                        mode: 'no-cors',
                                        headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
                                        body: serialiseJSON(webToCaseSalesforceBody())
                                    })
                            ])
                            .then(function() {
                                $('#registraion-form')[0].reset();
                                $('.btnNew').css('display', 'none');
                                $('#back_from').hide();
                                dlPush();
                                window.location.href = response.msg == 'signup pending' ? "<?php echo base_url() ?>index.php/user/signup_pending" : "<?php echo base_url() ?>index.php/user/signup_success";
                            });

                    break;
                    case '1':
                    case '2':
                        $('.signup_msg').html( '<div class="alert alert-danger" role="alert"><strong>Error!</strong> ' + response.msg + '</div>');
                    break;
                };
            })
            .then(function() {
                $('#signup-load').fadeOut();
            });

        $('.error_msg').empty();
        $('.signup_msg').empty();
        $('#signup-load').fadeIn();

    });

    // Prepay - Dyball
    $('#registraion-form-dyball').submit(function(e) {

        e.preventDefault();
        $('.error_msg').empty();

        if (!validateSignupForm()) { return; };

        REQUEST("<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
                method: 'post',
                headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
                body: $('#registraion-form-dyball').serialize()
            })
            .then(function(response) {
                switch (response.error) {
                    case '0':
                        $('#registraion-form-dyball')[0].reset();
                        $('.btnNew').css('display', 'none');
                        $('#back_from').hide();
                        window.location.href = response.msg === 'signup pending' ? "<?php echo base_url() ?>index.php/user/signup_pending" : "<?php echo base_url() ?>index.php/user/signup_success";
                        dlPush();
                    break;
                    case '1':
                    case '2':
                        $('.signup_msg').html( '<div class="alert alert-danger" role="alert"><strong>Error!</strong>  ' + response.msg + '.</div>');
                    break;
                };
            })
            .then(function() {
                $('#signup-load').fadeOut();
            })

        $('.error_msg').empty();
        $('.signup_msg').empty();
        $('#signup-load').fadeIn();

    }); // End Dyball






    var stripe = Stripe('<?php echo $this->config->item(' stripe ')[' pk_test ']?>');
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            lineHeight: '18px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // for dd
    // Create an instance of the card Element.
    var card = elements.create('cardNumber', {
        style: style
    });
    var dd_ccv = elements.create('cardCvc', {
        style: style
    });
    var dd_expire = elements.create('cardExpiry', {
        style: style
    });
    // Add an instance of the card Element into the `card-element` <div>.
    if (document.getElementById("card-element")) {
        card.mount('#card-element');
        dd_ccv.mount('#card-dd_ccv');
        dd_expire.mount('#card-dd_expire');
    }

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors_dd');
        if (event.error) {
            displayError.textContent = event.error.message;
            displayError.style.display = "block";
        } else {
            displayError.style.display = "none";
            displayError.textContent = '';
        }
    });

    var form_yp = document.getElementById('registraion-form-yearpay');
    if (form_yp) {
        form_yp.addEventListener('submit', function(event) {
            populateEmailAddressPostcode();
            event.preventDefault();
            stripe.createToken(card).then(function(result) {

                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors_dd');
                    errorElement.textContent = result.error.message;

                } else {
                    // Validate the pay amount
                    var pay_errors = new Array();

                    if (document.getElementById('amount_pay').value == '') {
                        pay_errors += '<br>Please enter the amount you would like to pay';
                    }
                    if (document.getElementById('amount_pay').value != '' && document
                        .getElementById('amount_pay').value < 1) {
                        pay_errors += '<br>The amount must be over £1';
                    }
                    // Are there any errors?
                    if ($(pay_errors).toArray().length > 0) {
                        $('#pay_error_msg').html( '<div class="alert alert-danger" role="alert"><strong>Error! </strong>' + pay_errors + '</div>');
                        $('html, body').animate({ scrollTop : $('#pay_error_msg').offset().top - 100 }, 'fast');
                        return;
                    } else {
                        // Clear errors an d send the token to your server.
                        $('#pay_error_msg').empty();
                    }

                    // Are we adding a new address
                    if (document.getElementById('toggle_new_card_address').checked == true) {

                        // Set up errors array
                        var errors = new Array();

                        // Validate the required fields
                        if (document.getElementById('ba_line1').value == '') {
                            errors += '<br>Please enter address line 1 information';
                        }
                        if (document.getElementById('ba_line2').value == '') {
                            errors += '<br>Please enter address line 2 information';
                        }
                        if (document.getElementById('ba_postcode').value == '') {
                            errors += '<br>Please enter postcode';
                        }

                        // Are there any errors?
                        if ($(errors).toArray().length > 0) {
                            $('#billing_error_msg').html(
                                '<div class="alert alert-danger" role="alert"><strong>Error! </strong>' +
                                errors + '</div>');
                            $('html, body').animate({
                                scrollTop: $('#billing_error_msg').offset().top - 100
                            }, 'fast');
                            return false;
                        } else {
                            // Clear errors an d send the token to your server.
                            $('#billing_error_msg').empty();
                            stripeTokenHandleryp(result.token);
                        }

                    } else {
                        // Clear errors and send the token to your server.
                        $('#billing_error_msg').empty();
                        stripeTokenHandleryp(result.token);
                    }

                }
            });
        });
    };

    function stripeTokenHandleryp(token) {

        var form = document.getElementById('registraion-form-yearpay');

        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'token');
        hiddenInput.setAttribute('value', token.id);

        var amount = document.createElement('input');
        amount.setAttribute('type', 'hidden');
        amount.setAttribute('name', 'amount');
        amount.setAttribute('value', document.getElementById('amount_pay').value);

        var existing_line1 = document.createElement('input');
        existing_line1.setAttribute('type', 'hidden');
        existing_line1.setAttribute('name', 'existing_line1');
        existing_line1.setAttribute('value', document.getElementById('existing_line1').value);

        var existing_postcode = document.createElement('input');
        existing_postcode.setAttribute('type', 'hidden');
        existing_postcode.setAttribute('name', 'existing_postcode');
        existing_postcode.setAttribute('value', document.getElementById('existing_postcode').value);

        var email = document.createElement('input');
        email.setAttribute('type', 'hidden');
        email.setAttribute('name', 'email');
        email.setAttribute('value', document.getElementById('payment-email').value);

        if (billing_address_id) {
            var billing_address_id = document.createElement('input');
            billing_address_id.setAttribute('type', 'hidden');
            billing_address_id.setAttribute('name', 'billing_address_id');
            billing_address_id.setAttribute('value', document.getElementById('billing_address_id').value);
        }
        
        if (document.getElementById('toggle_new_card_address').checked) {

            var ba_line1 = document.createElement('input');
            ba_line1.setAttribute('type', 'hidden');
            ba_line1.setAttribute('name', 'ba_line1');
            ba_line1.setAttribute('value', document.getElementById('ba_line1').value);

            var ba_line2 = document.createElement('input');
            ba_line2.setAttribute('type', 'hidden');
            ba_line2.setAttribute('name', 'ba_line2');
            ba_line2.setAttribute('value', document.getElementById('ba_line2').value);

            var ba_line3 = document.createElement('input');
            ba_line3.setAttribute('type', 'hidden');
            ba_line3.setAttribute('name', 'ba_line3');
            ba_line3.setAttribute('value', document.getElementById('ba_line3').value);

            var ba_line4 = document.createElement('input');
            ba_line4.setAttribute('type', 'hidden');
            ba_line4.setAttribute('name', 'ba_line4');
            ba_line4.setAttribute('value', document.getElementById('ba_line4').value);

            var ba_postcode = document.createElement('input');
            ba_postcode.setAttribute('type', 'hidden');
            ba_postcode.setAttribute('name', 'ba_postcode');
            ba_postcode.setAttribute('value', document.getElementById('ba_postcode').value);

            form.appendChild(ba_line1);
            form.appendChild(ba_line2);
            form.appendChild(ba_line3);
            form.appendChild(ba_line4);
            form.appendChild(ba_postcode);

        }

        form.appendChild(hiddenInput);
        form.appendChild(amount);
        form.appendChild(existing_line1);
        form.appendChild(existing_postcode);
        form.appendChild(email);

        if (billing_address_id) {
            form.appendChild(billing_address_id);
        };



        REQUEST(base_url + 'index.php/api/create_yearly_customer', 'json', {
                method: 'post',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                }),
                body: $('#registraion-form-yearpay').serialize()
            })
            .then(function(res) {
                switch (res.api_status) {
                    case 1:
                        $('#Transaction').text(res.message);

                        $('#pay_card').fadeOut();
                        $('#thank_you').fadeIn();

                        $(".gas_card").css("display", "none");
                        $(".payment-details").css("display", "inline-block");

                        $('html, body').animate({
                            scrollTop: $('#Transaction_status').offset().top - 100
                        }, 'fast');
                        break;
                    default:
                        $('#card-errors').text(res.message);
                        break;
                }
            })
            .then(function() {
                registrationYearPayHandler({ preventDefault : function() {} });
            });

    };

}); // end of onload $(function(){})

</script>
</body>

</html>