<!-----------------Start Owel Div------------>
  <div class="right_owel">
  <!----------Start Section One--------------->
  <section class="page-header">
  <div class="container">
      <div class="col-sm-8 col-sm-push-4 col-md-8 col-md-push-4 col-lg-8 col-lg-push-4 pull-right">
        <div class="brand">
          <h1 class="title wow fadeIn" data-wow-delay="50ms">Switch and Save! </h1>
          <p class="hook wow fadeIn" data-wow-delay="100ms">Enjoy cheaper &amp; smarter energy.</p>

          <a class="btn btn-default btn-round btn-eversmart" href="<?php echo base_url(); ?>index.php/quotation">Get Quote</a>
        </div>
      </div>
      <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-4 col-sm-pull-8 col-md-4 col-md-pull-8 col-lg-3 col-lg-pull-8 pull-left">
        <div class="phone">
                    <video autoplay="" loop="" muted="" playsinline="" preload="auto" id="videoNew1">
                        <source src="https://www.eversmartenergy.co.uk/assets/vid/EVERSMARTTRY5.mp4" type="video/mp4">
                      </video>
          <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone"><br>

        </div>
      </div>
      </div>
    </section>
  <!----------End Start Section One--------------->
  <!-----------------start Section 2-------------------->
        <section class="section-why-switch">

          <div class="container">
                <div class="row">
                  <div class="col-sm-7 col-md-7 col-lg-8 col-lg-offset-1">
                    <div class="reasons">
                      <h2 class="title newTitle">Why switching to us is a smart move?</h2>
                      <div class="why_switch_box why_switch_box_first">

                          <div class="info info-horizontal">
                            <div class="icon col-sm-2 ">
                              01
                            </div>
                            <div class="col-sm-10 pull-left">
                              <span class="info-title">Save up to £280* per year</span>
                              <p>We offer some of the UKs lowest tariffs.</p>
                            </div>
                          </div>

                      </div>
                      <div class="why_switch_box">

                          <div class="info info-horizontal">
                            <div class="icon col-sm-2">
                              02
                            </div>
                            <div class="col-sm-10 pull-left">
                              <span class="info-title">Take control of your energy. Free Smart Meter</span>
                              <p>When you switch you will receive a FREE Smart Meter so you can see your usage and take control of your energy.</p>
                            </div>
                          </div>

                      </div>
                      <div class="why_switch_box">
                          <div class="info info-horizontal">
                            <div class="icon col-sm-2">
                              03
                            </div>
                            <div class="col-sm-10 pull-left">
                              <span class="info-title">Industry leading customer service. Never be on hold again</span>
                              <p>Our customer service experience is unique and one of a kind. Get in touch with our friendly team using the technology you use most. </p>
                            </div>
                          </div>

                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-3 col-lg-offset-1 hidden-xs phoneNewImage pull-right">
                    <div class="phone-container">

                      <div class="phone">

                      <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                      </div>
                    </div>
                  </div>
                </div>
          </div>
        </section>
<!-----------------End Section 2-------------------->
  </div>
  <!----End Right Owel--------------->

  <!----------------start Section 3--------------------->
      <section class="three_box_section">
    <div class="three_bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 mx-auto text-center">
            <h2>Get the latest in smart meter technology and simplify your life.</h2>
          </div>
          <div class="col-sm-12 text-center boxCenterImg">
            <img class="img-fluid " alt="EverSmart meters" src="<?php echo base_url(); ?>assets/img/eversmartihd.png">
          </div>
          <div class="col-sm-12 text-center">

            <a class="btn btn-md btn-eversmart btn-round mtop60" title="Get a free quote and smart meter." data-placement="top" data-toggle="tooltip" href="#">Get a FREE Smart Meter</a>
          </div>
        </div>
    </div>
    </div>
  </section>
<!----------------End Section 3--------------------->
<!----------------start Section 4--------------------->
  <section class="section-smart-customer-services">
    <div class="container">
      <h2>We believe a good supplier, is a supplier you shouldn't have to call.</h2>
      <div class="services-xs">
        <div class="row">
          <div class="col-xs-12">
            <div class="phone-container text-center">
              <div class="phone">
                <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                                  <img class="smsImage" role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                                  <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                  <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                </video>

              </div>
            </div>
          </div>
        </div>
        <div class="row social-icons">
          <div class="col-xs-2 col-xs-push-2 social-icon smsIconHome">
            <img src="<?php echo base_url(); ?>assets/img/chat.png">
          </div>
          <div class="col-xs-2 col-xs-push-2 social-icon facebookIconHome">
            <img src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg">
          </div>
          <div class="col-xs-2 col-xs-push-2 social-icon">
            <img src="<?php echo base_url(); ?>assets/img/telegram.png">
          </div>
          <div class="col-xs-2 col-xs-push-2 social-icon">
            <img src="<?php echo base_url(); ?>assets/img/twitter.png">
          </div>

        </div>
        <div class="row">
          <div class="col-sm-12 text-center">
            <a href="#" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn" data-wow-delay="400ms" title="Sign up for smart services." data-placement="top" data-toggle="tooltip">
              Get Smart Services
            </a>
          </div>
        </div>
      </div>
      <div class="services-sm">
        <div class="row">
          <div class="col-sm-6">
            <div class="phone-container text-center">
              <div class="phone">

                <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                                  <img class="smsImage" role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                                  <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                  <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                </video>
              </div>
            </div>
          </div>
          <div class="col-sm-5 col-sm-push-1">
            <div class="row social-icons">

                            <div class="info info-horizontal hover-info wow fadeIn" id="socialvid" data-wow-delay="400ms">
                <div class="icon icon-primary icon-fix facebookIconHome">
                  <img src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg" class="social-icon">
                </div>									<div class="description">
                  <h3 class="social-title">Facebook</h3>
                </div>
              </div>

              <div class="info info-horizontal hover-info wow fadeIn active" id="chatvid" data-wow-delay="0ms">
                <div class="icon icon-primary icon-fix smsIconHome">
                  <img src="<?php echo base_url(); ?>assets/img/chat.png" class="social-icon">
                </div>
                <div class="description">
                  <h3 class="social-title">SMS</h3>
                </div>
              </div>

              <div class="info info-horizontal hover-info wow fadeIn" id="twittervid" data-wow-delay="200ms">
                <div class="icon icon-primary icon-fix">
                  <img src="<?php echo base_url(); ?>assets/img/twitter.png" class="social-icon">
                </div>
                <div class="description">
                  <h3 class="social-title">Twitter</h3>
                </div>
              </div>
              <div class="info info-horizontal hover-info wow fadeIn" id="telegramvid" data-wow-delay="600ms">
                <div class="icon icon-primary icon-fix">
                  <img src="<?php echo base_url(); ?>assets/img/telegram.png" class="social-icon">
                </div>
                <div class="description">
                  <h3 class="social-title">Telegram</h3>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 text-center">
            <a href="#" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn" data-wow-delay="400ms" title="Sign up for smart services." data-placement="top" data-toggle="tooltip">
              Get Smart Services
            </a>
          </div>
        </div>
      </div>
      <div class="services-md-lg">
        <div class="row hidden-xs hidden-sm">
          <div class="col-md-4">
            <div class="info info-horizontal hover-info wow fadeIn active" id="chatvid" data-wow-delay="0ms">
              <div class="icon icon-primary right icon-fix smsIconHome">
                <img src="<?php echo base_url(); ?>assets/img/chat.png" class="social-icon">
              </div>
              <div class="description align-right">
                <h3 class="social-title">SMS</h3>
                <p>Add 07537417858  to your contacts just like a friend and begin instant messaging us</p>
              </div>
            </div>
            <div class="info info-horizontal hover-info wow fadeIn" id="socialvid" data-wow-delay="400ms">
              <div class="icon icon-primary right icon-fix facebookIconHome fullColor">
                <img src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg" class="social-icon">
              </div>
              <div class="description align-right">
                <h3 class="social-title">Facebook</h3>
                <p>Follow us at @eversmartenergy and if you prefer Facebook messenger drop us a line any time. </p>
              </div>
            </div>

          </div>
          <div class="col-md-4">
            <div class="phone-container text-center">
              <div class="phone">
                                <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                                  <img class="smsImage" role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                                  <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                  <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                </video>
              </div>
              <a href="#" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn" data-wow-delay="400ms" title="Sign up for smart services." data-placement="top" data-toggle="tooltip">
                Get Smart Services
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info info-horizontal hover-info wow fadeIn" id="twittervid" data-wow-delay="200ms">
              <div class="icon icon-primary icon-fix twitterIconHome">
                <img src="<?php echo base_url(); ?>assets/img/twitter.png" class="social-icon">
              </div>
              <div class="description">
                <h3 class="social-title">Twitter</h3>
                <p>If you have a question or just want to say hi. Tweet us using @eversmartenergy.</p>
              </div>
            </div>
            <div class="info info-horizontal hover-info wow fadeIn" id="telegramvid" data-wow-delay="600ms">
              <div class="icon icon-primary icon-fix telegramIconHome">
                <img src="<?php echo base_url(); ?>assets/img/telegram.png" class="social-icon">
              </div>
              <div class="description">
                <h3 class="social-title">Telegram</h3>
                <p>Download the telegram app and add us to your contacts. Speak to one of our team instantly.</p>
              </div>
            </div>

          </div>
        </div>
      </div>
      </div>
    </section>
<!----------------End Section 4--------------------->
  <!----------start Five Section------------->

    <section class="section-easy-steps">
      <div class="container">
      <div class="row">
        <div class="col-sm-10 mx-auto">
          <h2 class="title_step text-center">Switch in 3 easy steps, it only takes 2 minutes and we will do all the hard work for you.   </h2>
        </div>
      </div>
      <div class="steps-sm-md-lg">
        <div class="row">
          <div class="col-sm-4 text-center">
            <div class="info wow fadeIn" data-wow-delay="0ms">
              <div class="face">
                <i class="material-icons">face</i>
              </div>
              <span class="step_heading">Step 01</span>
              <span class="step_intro">Register Your Details</span>
              <p class="step_detail">Enter your post code and tell us a little bit about your energy usage</p>
            </div>
          </div>
          <div class="col-sm-4 text-center">
            <div class="info wow fadeIn" data-wow-delay="200ms">
              <div class="handset">
                <i class="material-icons">headset_mic</i>
              </div>
              <span class="step_heading">Step 02</span>
              <span class="step_intro">Get a quotation</span>
              <p class="step_detail">See for yourself how cheap we are</p>
            </div>
          </div>
          <div class="col-sm-4 text-center">
            <div class="info wow fadeIn" data-wow-delay="400ms">
              <div class="rose">
                <i class="material-icons">mail</i>
              </div>
              <span class="step_heading">Step 03</span>
              <span class="step_intro">You're all done </span>
              <p class="step_detail">within 21 days you'll start benefiting from cheaper smarter energy  </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <a class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton" data-wow-delay="400ms" title="Sign up for smart services." data-placement="top" data-toggle="tooltip" href="#">Make The Switch</a>
        </div>
      </div>
      </div>
    </section>
<!----------End Five Section------------->
<!----------------start Section 6--------------------->

    <section class="section-six mt0">
      <div class="inner_six">
          <div class="container">
            <div class="row">
              <div class="col-md-10 mx-auto text-center">
                <h2>Enjoy cheaper, smarter and simpler energy.</h2>
                <p class="instructions">Enter your postcode to find out how much you can save.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mx-auto">
                <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
                  <div class="content">
                    <form action="#" id="form-postcode-simple" class="switch-form" name="">
                      <div class="row wow fadeInUp" data-wow-delay="400ms">
                        <div class="form-group postCodeBox">
                          <label for="postcode">Postcode:</label>
                          <div class="input-group">
                            <input class="form-control" id="postcode" name="postcode" required="" type="text">
                            <span class="input-group-btn btnNew">
                            <button class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Get Quote</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </section>
<!----------------End Section 6--------------------->
