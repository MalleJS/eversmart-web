<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Boiler and Home Care</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
 	<link href="http://13.58.101.121/eversmartnew/assets/css/style.css" rel="stylesheet">
	<link href="http://13.58.101.121/homeservices/assets/css/homecare.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
	    <div id="main_content">
		<div class="green-box-radiant">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">	
			<div class="container">
				<div class="row">

				<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-unique">
					<span class="col-sm-6 col-md-3 px-0">
						<a class="logo" href="<?php echo base_url(); ?>">eversmart.</a>
					</span>

					<div class="col-sm-6 col-md-9 text-right pull-right no-padding mobile_menu">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation" style="">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse " id="navbarSupportedContent-3">
                        <ul class="navbar-nav ml-auto mobile_navi">
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Quote">energy
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="boiler-switch-one.html">boiler
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">smart meters</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">home services</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">help
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger" aria-labelledby="navbarDropdownMenuLink-2">
                                    <a class="dropdown-item waves-effect waves-light" href="#">help &amp; faqs</a>
                                    <a class="dropdown-item waves-effect waves-light" href="#">contact us</a>
                                </div>
                            </li>

							<li><button type="button" class="btn btn-info btn-rounded waves-effect waves-light white-btn" style="color:#0ce7fe!important;text-transform: capitalize;">Login</button></li>
							<li><button style="margin-left:25px;color:#0ce7fe!important;text-transform: capitalize;" type="button" class="btn btn-info btn-rounded waves-effect waves-light white-btn">Topup</button></li>

                        </ul>

                    </div>
					</div>
                </nav>
				</div>
			</div>
			<!--- End container-------->
			
		</div>
		<!---end top bar bar----------->	


<!----------------start postcode---------------------->

			<section class="postcode_top mt0">
				<div class="boiler_header-h">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Boiler & Home Care</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								</div>
							</div>							
						</div><!------------------End Container----------------->
							
				</div><!------------------End Postcode-------------->
			</section>
			
			</div>
			</div>
<!----------------End postcode--------------------->			

			
		<!--------------------Start breadcrumbs Care Section-------------->
			<section class="three-care-b home-ser-col-full">
				<div class="container">
				<div class="row">					
					<div class="col-sm-12 text-center">
					<h1 class="b-replace-h">How to get your new boiler?</h1>
					</div>
				</div>
				<div class="row">					
					<div class="col-sm-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b1.jpg" alt=""/></a>
							
							<h3>You Answers</h3>
							<p>Book a Survey</p>
							<span class="b-replace-detail">Answer a few questions about your old boiler and flue</span>
										
						</span>
					</div><!---------End Box One------------>
					<div class="col-sm-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b2.png" alt=""/></a>
							
							<h3>You Choose</h3>
							<p>Choose your Plan</p>	
							<span class="b-replace-detail">a new boiler and install date online</span>							
											
						</span>
					</div><!---------End Box One------------>
					
					<div class="col-sm-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b3.png" alt=""/></a>
							
							<h3>We Install</h3>
							<p>Our Engineer will install the boiler</p>
							<span class="b-replace-detail" id="gap0">using a Gas Safe registered expert,local to you</span>
							
											
						</span>
					</div><!---------End Box One------------>
					
				</div>
				<div class="row">					
					<div class="col-sm-12 text-center">
					<a href="#" id="swi-me" class="get-care btn btn-md  btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">SWITCH NOW</a>
						</div>
				</div>
				</div><!------------------End Container----------------->
			</section>
<!--------------------End breadcrumbs Care Section------------->	

<!--------------------start ever Repair---->

			<section class="ever_reapir" id="ever_reapir_gap">
			
				<div class="annaul_bg_box2">
					<div class="container">
					<div class="row">
							<div class="col-sm-6">
							 
							</div>
							  <div class="com-sm-12 col-lg-6 col-md-6  pull-right">
									
								  
										<h3>Why choose a Smart Thermostat?</h3>
										<p>The smart thermostat learns your behavior and helps to save energy by  </p>
										<a href="#"  class="get-care btn btn-md  btn-round weight-300 wow fadeInUp switchButton waves-effect waves-light pull-left ">Get Smart Boiler</a>
									
							
							  </div>
					</div>
					</div>
				</div>	
				
			</section>


		
		<!--------------------Start breadcrumbs Care Section-------------->
			<section class="three-care-b" id="b-offers-main">
				<div class="container">
				<div class="row justify-content-center">					
					<div class="col-sm-8 text-center">
					<h3 class="offer-main-h">What Boilers do we offer?</h3>
					<p class="offer-main-d">We Offer the most efficient and A-rated boiler available on the market.</p>
					</div>
				</div>
				<div class="row justify-content-center">					
					<div class="col-sm-10 text-center">
					
						<img src="<?php echo base_url() ?>assets/img/system.png" alt="" class="responsive"/>
						
					</div><!---------End Box One----------->
				</div>
				<div class="row">					
					<div class="col-sm-12 text-center">
					<a href="#" id="swi-me" class="get-care btn btn-md  btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">GET A NEW BOILER NOW</a>
						</div>
				</div>
				</div><!------------------End Container----------------->
			</section>
<!--------------------End breadcrumbs Care Section------------->	




<!--------------------start annual Section---->
			<section class="blue-annual" id="blue-annual-box-gap">
			<div class="annaul_bottom_bg_top"></div>
				<div class="annaul_bg_box">
					<div class="container">
					<div class="row justify-content-center">					
					<div class="col-sm-10 text-center">					
						<h3>What is the benefit of having a new boiler?</h3>						
					</div>
					</div>
						
						<div class="row justify-content-center mtop60">
							<div class="col-sm-5 pull-left ">
								<ul class="boiler-b-p">
									<li>Benefit Point Benefit Point </li>
									<li>Benefit Point Benefit Point </li>
									<li>Benefit Point Benefit Point </li>
									<li>Benefit Point Benefit Point </li>
								</ul>
								
							
							<div class="col-sm-12 text-left">
					<a href="#"  class="get-care btn btn-md  btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">GET A NEW BOILER NOW</a>
						</div>
							</div>
							
							<div class="col-sm-5 right">
								<img class="responsive" src="<?php echo base_url() ?>assets/img/family.png" alt=""/>
							
							</div>
						
						
								
						</div>
					</div>
					</div><!------------------End Container----------------->
					<div class="annaul_bottom_bg"></div>
			</section>
<!---------------end Annual Section------------------->


<!--------------------start annual_price Section---->
			<section class="annual_price" id="annaul-price-box-gap">
				
					<div class="container">
					<div class="row justify-content-center">
						<div class="col-sm-8 text-center">
						<span class="annual-h-b">Get your smart heating thermostat from just £9* per month</span>
						
					</div>
					</div>
						
						<div class="row">
							

							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" >
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												<h5>eversmart.<br>One</h5>
													<span class="card ever_rounded_p text-center">
														<span class="a-start">Starting</span>
														<span class="a-price"><span class="pound">&#163;</span> <span class="b-price-a">12</span><span class="month-a">/mo</span>
														</span>														
													</span>
												</span>											
												<ul class="step-ev-box">
													<li>Annual boiler service</li>
													<li>Boiler and controls </li>
												</ul>												
												<a href="#" type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Buy Now
												</a>
											</div>
										
									</div>
						
							</div><!---------End Box One------------>
							
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" >
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												<h5>eversmart.<br>Two</h5>
													<span class="card ever_rounded_p text-center">
														<span class="a-start">Starting</span>
														<span class="a-price"><span class="pound">&#163;</span> <span class="b-price-a">14</span><span class="month-a">/mo</span>
														</span>														
													</span>
												</span>											
												<ul class="step-ev-box">
													<li>Annual boiler service</li>
													<li>Boiler and controls </li>
													<li>Central heating</li>
													<li>Plumbing</li>
												</ul>												
												<a href="#" type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Buy Now
												</a>
											</div>
										
									</div>
						
							</div><!---------End Box One------------>
							
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" >
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												<h5>eversmart.<br>Three</h5>
													<span class="card ever_rounded_p text-center">
														<span class="a-start">Starting</span>
														<span class="a-price"><span class="pound">&#163;</span> <span class="b-price-a">18</span><span class="month-a">/mo</span>
														</span>														
													</span>
												</span>											
												<ul class="step-ev-box">
													<li>Annual boiler service</li>
													<li>Boiler and controls </li>
													<li>Central heating</li>
													<li>Plumbing</li>
													<li>Drains</li>
												</ul>												
												<a href="#" type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Buy Now
												</a>
											</div>
										
									</div>
						
							</div><!---------End Box One------------>
						
						
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" >
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												<h5>eversmart.<br>four</h5>
													<span class="card ever_rounded_p text-center">
														<span class="a-start">Starting</span>
														<span class="a-price"><span class="pound">&#163;</span> <span class="b-price-a">21</span><span class="month-a">/mo</span>
														</span>														
													</span>
												</span>											
												<ul class="step-ev-box">
													<li>Annual boiler service</li>
													<li>Boiler and controls </li>
													<li>Central heating</li>
													<li>Plumbing</li>
													<li>Drains</li>
													<li>Home electrics</li>
												</ul>												
												<a href="#" type="button" class="btn btn-primary btn-round btn-info" data-toggle="modal" data-target="#Triffinfo">
												  Buy Now
												</a>
											</div>
										
									</div>
						
							</div><!---------End Box One----------->
						
						
								
						</div>
					
					</div><!------------------End Container----------------->
					
			</section>
<!---------------end annual_price Section------------------->


<!----------------Start Footer---------------------------->
<?php $this->load->view('layout/common_footer'); ?>