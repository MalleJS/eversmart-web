<?php //if( $this->session->userdata('login_data')['account_status'] !=  0 ) {?>
			<div class="container hideonmobile">
				<div class="row">

				<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-unique">
					<span class="col-sm-6 col-md-3 px-0">
						<a class="logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
					</span>

					<div class="col-sm-6 col-md-9 text-right pull-right no-padding">

                    <div class="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto ">
							<li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="<?php echo base_url(); ?>index.php/quotation">energy
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>

                            <!--   <li class="nav-item">
                                <a class="nav-link waves-effect waves-light navbar-padding" href="<?php //echo base_url(); ?>index.php/FamilySaver">family saver club
                                </a>
                            </li>
							
							<li class="nav-item">
                                <a class="nav-link waves-effect waves-light navbar-padding" href="<?php //echo base_url(); ?>index.php/Smartmeter">smart meter
                                </a>
                            </li>
-->
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light navbar-padding" href="<?php //echo base_url(); ?>index.php/homeservices">boilers & heating
                                </a>
                            </li>
							 <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light navbar-padding" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">about
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Vision">our vision</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Career">careers</a>
                                </div>
                            </li>
							
                          
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light navbar-padding" id="navbarDropdownMenuLink" style="padding-right: 20px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">help
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Helpfaqs">help & faqs</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/contact_us">contact us</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Policies">privacy</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Terms">T&Cs</a>
                                </div>
                            </li>

							<?php if( !empty($this->session->userdata('login_data')['id']) ){ ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><?php
                                            if( $this->session->has_userdata('login_data') ){
                                                if(!isset($this->session->userdata('login_data')['admin_role_type_id']) || $this->session->userdata('login_data')['admin_role_type_id'] != '1') {

                                                    echo ucfirst($this->session->userdata('login_data')['title']) . '. ' . ucfirst($this->session->userdata('login_data')['last_name']);
                                                }
                                                else {
                                                    echo ucfirst($this->session->userdata('login_data')['forename']) . ' ' . ucfirst($this->session->userdata('login_data')['surname']);
                                                }
                                            }else{
                                                echo '-';
                                            }
                                        ?>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink">

                                        <?php

                                        if(isset($this->session->userdata('login_data')['admin_role_type_id']) && $this->session->userdata('login_data')['admin_role_type_id'] == '1')
                                        {?>
                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_customer_signup">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Customer Sign-up
                                            </a>

                                            <a class="dropdown-star waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_webtocase">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Web to Case
                                            </a>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_activation_email">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Resend Emails
                                            </a>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_email_templates">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Email Templates
                                            </a>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_datacapture">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Capture Form
                                            </a>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/Admin/admin_leadupdate">
                                                <i class="fa fa-star righ-padd" aria-hidden="true"></i>Lead Update
                                            </a>

                                            <a id="logout_user" class="dropdown-item waves-effect waves-light" onclick="logout_user();" href="javascript:void(0)">
                                                <i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout
                                            </a>
                                            <?php
                                        }
                                        else
                                            {

                                            if(($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3) && $this->session->userdata('login_data')['account_status']==1){ // Only allow access to my account if they are Dyball or Junifer active ?>

                                                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/user/account">
                                                    <i class="fa fa-info righ-padd" aria-hidden="true"></i>my account
                                                </a>

                                            <?php } else { ?>

                                                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/user/account_status">
                                                    <i class="fa fa-info righ-padd" aria-hidden="true"></i>my account
                                                </a>

                                            <?php } ?>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Helpfaqs">
                                                <i class="fa fa-question righ-padd" aria-hidden="true"></i>help &amp; faq
                                            </a>

                                            <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/contact_us">
                                                <i class="fa fa-address-book righ-padd" aria-hidden="true"></i>contact us
                                            </a>

                                            <a id="logout_user" class="dropdown-item waves-effect waves-light" onclick="logout_user();" href="javascript:void(0)">
                                                <i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout
                                            </a>
                                        <?php
                                        }?>
                                    </div>

                                </li>
                            <?php } ?>

                            <?php if( empty($this->session->userdata('login_data')['id']) ){ ?>
                                <li><a href="<?php echo base_url() ?>index.php/user/login/" class="btn btn-info btn-rounded waves-effect waves-light white-btn">LOGIN</a></li>
                            <?php } ?>

                            <li><button style="margin-left:25px" type="button" class="btn btn-info btn-rounded waves-effect waves-light white-btn"><a href="https://eversmartpayments.paypoint.com/energy/" target="_blank">TOP UP</a></button></li>

                        </ul>

                    </div>
					</div>
                </nav>
				</div>
			</div>



        <?php //} ?>
			<!--- End container-------->

		<!---end top bar bar----------->
