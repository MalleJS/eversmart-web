
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">Sorry, your current supplier has stopped your switch</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            <strong>Customer No: <?= $email_info['customer_number']; ?></strong><br>
                                                            <strong>Bill No: <?= $email_info['bill_number']; ?></strong><br><br>
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            <strong>We’re sorry to tell you this, but your current energy supplier has objected to you switching to us.</strong><br><br>
                                                            We have attached a letter containing more information about the objection.<br><br>
                                                            <strong>Why has my supplier objected?</strong><br>
                                                            This usually happens due to a problem with you account. You will need to contact your old supplier to find out the exact reason. Once the problem has been sorted, you will be able to start the switching process again.<br><br>
                                                            <strong>If you owe them money</strong><br>
                                                            If your account is in debt with your current supplier, you may need to pay off the remainder of the balance before you can switch over to us. Once this has been done, you can head to our website and register to switch again.<br><br>
                                                            <strong>What to do next</strong><br>
                                                            Contact your current energy supplier as soon as possible. Once the problem has been solved, you can apply to switch again at <a href="https://www.eversmartenergy.co.uk" target="_blank">www.eversmartenergy.co.uk</a><br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->