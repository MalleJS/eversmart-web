<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Privacy Policy</title>
    <!-- Font material icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <!--- Font Family Add----------------->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <script type="text/javascript">

        function send_email(email_template){

            if(document.getElementById('email_address').value!='') {

                $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/' + email_template,
                    type: 'post',
                    dataType: 'json',
                    data: $('#email').serialize(),
                    success: function (res) {
                        console.log(res.success);
                        var success_message = res.success;
                        if (success_message) {
                            document.getElementById("return_message").innerHTML = success_message;
                        }
                        else {
                            document.getElementById("return_message").innerHTML = 'Something went wrong';
                        }
                    }
                });
            }
            else {
                alert('Email address required');
            }
        }

    </script>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<!-- Start your project here-->

<div class="main-rounded-red-header">
    <div class="red-rounded-wave">

        <!---end top bar bar----------->


        <!----------------start postcode-------------------->

        <section class="mt0 ">
            <div class="boiler_header-h" style="padding-bottom:40px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mx-auto text-center">
                            <span class="bolier_home-h-l">Email Templates</span>
                        </div>
                    </div>
                </div><!------------------End Container----------------->

            </div><!------------------End Postcode------------>

        </section>
.
        <!--------------------Start breadcrumbs Care Section------------>

        <section class="termandconditions" style="padding-top: 50px; height: auto; background-color: white;">

            <div class="container">

                <p style="text-align: center; font-weight: 500; font-size: 18px;">enter your email address and press one of the links below to generate an email.</p><br>

                <form id="email" style="text-align: center;">
                    <input type="text" name="email_address" id="email_address" value="" placeholder="email@address.here" style="margin: 0 auto; text-align:  center; width: 450px; height: 50px; font-size: 22px;" />
                </form>

                <br><br>

                <div id="return_message" style="text-align: center; font-weight: bold; font-size: 1.2em; padding-bottom: 30px; color: green;"></div>

                <ul style="line-height: 2.5em; float: left; width: 25%;">
                    <li><a href="#" onclick="send_email('activation_email'); return false;">activation</a></li>
                    <li><a href="#" onclick="send_email('payment_plan_cancelled'); return false;">payment plan cancelled</a></li>
                    <li><a href="#" onclick="send_email('latest_energy_bill'); return false;">latest energy bill</a></li>
                    <li><a href="#" onclick="send_email('first_meter_reading'); return false;">first meter reading</a></li>
                    <li><a href="#" onclick="send_email('provide_meter_reading'); return false;">provide meter reading</a></li>
                </ul>

                <ul style="line-height: 2.5em; float: left; width: 25%;">
                    <li><a href="#" onclick="send_email('moving_home_email'); return false;">moving home</a></li>
                    <li><a href="#" onclick="send_email('leaving'); return false;">sorry to see you go</a></li>
                    <li><a href="#" onclick="send_email('balance_overdue_1'); return false;">balance overdue 1</a></li>
                    <li><a href="#" onclick="send_email('balance_overdue_2'); return false;">balance overdue 2</a></li>
                    <li><a href="#" onclick="send_email('balance_overdue_3'); return false;">balance overdue 3</a></li>
                </ul>

                <ul style="line-height: 2.5em; float: left; width: 25%;">
                    <li><a href="#" onclick="send_email('direct_debit_confirmation'); return false;">direct debit confirmation</a></li>
                    <li><a href="#" onclick="send_email('direct_debit_changes'); return false;">direct debit changes</a></li>
                    <li><a href="#" onclick="send_email('direct_debit_problem'); return false;">direct debit problem</a></li>
                    <li><a href="#" onclick="send_email('new_payment_plan'); return false;">new payment plan</a></li>
                    <li><a href="#" onclick="send_email('book_meter'); return false;">book meter</a></li>
                </ul>

                <ul style="line-height: 2.5em; float: left; width: 25%;">
                    <li><a href="#" onclick="send_email('new_payment_schedule'); return false;">new payment schedule</a></li>
                    <li><a href="#" onclick="send_email('objection'); return false;">objection</a></li>
                    <li><a href="#" onclick="send_email('previous_supplier_objection'); return false;">previous supplier objection</a></li>
                    <li><a href="#" onclick="send_email('registration_withdrawal'); return false;">registration withdrawal</a></li>
                    <li><a href="#" onclick="send_email('reset_password'); return false;">reset password</a></li>
                </ul>

            </div>

        </section>

        <!--------------------End breadcrumbs Care Section------------->


        <!----------------Start Footer-------------------------->


        <footer class="main_footer">

            <div class="container">

                <div class="social-footer text-center">
                    <ul class="social-buttons socialButtonHome">
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

                    </ul>
                </div>
                <!----End Social Footer------------->

                <div class="copyright text-center">
                    &copy; 2018 Eversmart Energy Ltd - 09310427
                </div>

            </div>
            <!---------end Footer container------------>

        </footer>
        <!----end footer------------>


        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
        <!-- common JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>
        <script type="text/javascript">
            $('#myCarousel').carousel({
                interval: 2000
            });
        </script>
        <script>
            function myFunction() {
                var x = document.getElementById("myInput");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>
</body>
</html>

