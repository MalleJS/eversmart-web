
            <!----------------start postcode---------------------->
            <section class="mt0" >
               <div class="boiler_header-h" id="padding-down">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-10 mx-auto text-center">
                           <span class="bolier_home-h-l">Frequently Asked Questions</span>
                           <span class="bolier_home-h-s">Join the Revolution and start Saving</span>
                        </div>
                     </div>
                  </div>
                  <!------------------End Container------------------>
               </div>
               <!------------------End Postcode-------------->
            </section>
            <!----------------End postcode---------------------->		
         </div>
      </div>
      <!----------------End postcode--------------------->		
      <!--------------------Start breadcrumbs Care Section-------------->
      <section class="cd-section" style="background-image:none; float:left; width:100%; padding-bottom:50px;">
      <div class="cd-section-grey">
         <div class="container">
            <div class="row">
               <div class=" col-sm-10 mx-auto aos-item" data-aos="fade-down">
                  <h1 class="cft_heading text-center">Customer FAQ Topics</h1>
                  <span class="faq_head text-center">Our help and advice section is growing all the time and contains many of the questions and enquiries that our customer service team answer everyday. If you can't find what you're looking for, please contact us.
                  </span>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Supply Loss" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/SupplyLoss.png" alt=""></span>
                           </div>
                           <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Supply Loss</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Switching" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/Switching.png" alt=""></span>
                           </div>
                           <!--- <div class="content">
                              <span class="gas_box_text dark_heading">
                              Switching</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Topping Up" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/ToppingUp.png" alt=""></span>
                           </div>
                           <!--- <div class="content">
                              <span class="gas_box_text dark_heading">
                              Topping Up</span>            
                           </div-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Replacement Key" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/ReplacementKey.png" alt=""></span>
                           </div>
                           <!--- <div class="content">
                              <span class="gas_box_text dark_heading">
                              Replacement Key</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
           
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Energy Usage" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/EnergyUsage.png" alt=""></span>
                           </div>
                           <!--- <div class="content">
                              <span class="gas_box_text dark_heading">
                              Energy Usage</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Smart Meters" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/SmartMeters.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Smart Meters</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Smart Meters Installation" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/SmartInstallation.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Smart Meters Installation</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Moving Home" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/MovingHome.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Moving Home</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
           
               <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Warm Home Discount" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/WarmHomeDiscount.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Warm Home Discount</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Eversmart Schemes" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/EversmartSchemes.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Eversmart Schemes</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Complaints" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/Complaints.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Complaints</span>            
                           </div>---->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
              <div class="col-lg-3 col-sm-12 col-md-6 main_card_box" id="faq">
                  <figure class="effect-sadie">
                     <figcaption>
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Help & Support" class="gas_card">
                           <div class="gas_box_icon gap_icon">
                              <span class="switch_icon"><img src="<?php echo base_url(); ?>assets/img/faq/Help&Support.png" alt=""></span>
                           </div>
                            <!---<div class="content">
                              <span class="gas_box_text dark_heading">
                              Help & Support</span>            
                           </div>-->
                           <span class="click_select_edit faq-red-color">SELECT</span>
                        </a>
                     </figcaption>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      </section>
      <!--------------------End breadcrumbs Care Section------------->
