<!----------------start postcode---------------------->

			<section class="mt0" >
				<div class="boiler_header-h" id="padding-down">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Frequently Asked Questions</span>
									
								
								</div>
							</div>							
						</div><!------------------End Container------------------>
							
				</div><!------------------End Postcode-------------->
				
			</section>
<!----------------End postcode---------------------->		
		</div>
		</div>
<!----------------End postcode--------------------->		

			
		<!--------------------Start breadcrumbs Care Section-------------->
			<section class="cd-section" style="background-image:none; float:left; width:100%; padding-bottom:50px;">
			<div class="cd-section-grey">
				<div class="container">
					<div class="row">
						<div class=" col-sm-10 mx-auto aos-item" data-aos="fade-down">
					
							<h1 class="cft_heading text-center">
							<?php if($_GET['m'] )
							{ 
								echo $_GET['m']; 
							}
							else
							{
								echo "Supply Loss";
							} ?> F.A.Qs</h1>
							<span class="faq_head text-center">The help & advice area contains answers to many of the questions we get asked every day. This section is growing all the time. If you can’t find what you’re looking for here, please <a href="<?php echo base_url(); ?>/index.php/contact_us">contact&nbsp;us</a>.</span>
 						</div>
					</div>

					<?php if($_GET['m'] == 'Supply Loss')
					{ ?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
										My gas/electricity doesn’t come back on after topping up. What should I do? 
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body">
											<p>If you have run out of credit (including emergency credit), your supply will be cut off and you will need to top up. For your safety, your supply will not return automatically once you are back in credit. Follow the instructions below to get back on supply:</p>
											<p>For electricity:</p>
											<ul>
												<li><p>Go to your electric meter and check that you are at least £1 back in positive credit.</p></li>
												<li><p>Press the blue A button or the red B button.</p></li>
												<li><p>Follow the instructions that appear on the screen.</p></li>
												<li><p>Your electricity supply should now be restored.</p></li>
											</ul>
											<p>For gas:</p>
											<ul>
												<li><p>Check that all gas appliances (e.g. your cooker) are switched off and are safe.</p></li>
												<li><p>Go to your gas meter and check that you are at least £1 back in positive credit.</p></li>
												<li><p>Press & hold the blue button in the middle of the meter, until you see a red light flashing in the bottom left.</p></li>
												<li><p>After around 15 seconds, you should hear the valve opening inside your meter. Your gas supply should now be restored.</p></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div><?php  
					}
					else if($_GET['m'] == 'Switching')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											If I have a debt, can I switch to Eversmart Energy?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Yes. Eversmart Energy will allow domestic prepayment customers to switch with a debt of £500 or less.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										Do I need to let my existing supplier know that I would like to switch to Eversmart Energy?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>No. We will contact your current supplier on your behalf to let them know that you’re switching. If you have a debt on your prepayment meter, please clear it beforehand. If your debt is less than £500, we should be able to transfer this across to us, but you will need to contact your existing supplier to progress this.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<a class="card-title">
										How quickly will my switch happen and when can I expect to be supplied by Eversmart Energy?
										</a>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>Your switch should take place within 21 days. We will write to you to let you know when this will happen.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										<a class="card-title">
										Should I expect to get a bill from Eversmart Energy?
										</a>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>You won’t receive a bill, but we will send you an annual statement showing your current tariff, usage and payments and an estimate of your usage for the following year.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										<a class="card-title">
										If I have a debt, am I able to switch away from Eversmart Energy?
										</a>
									</div>
									<div id="collapseFive" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>Yes. Eversmart Energy will usually allow a switch for a domestic prepayment customer with a debt of £500 or less. If you have a debt over £500 and you wish to switch, please contact our collections team to discuss your options.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Topping Up')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											How do I top up?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Once you’ve activated your new key you can buy credit at a Paypoint, Payzone or Post Office outlet and add it to your meter. You can buy up to £49 credit at a time. You can visit the following site to confirm where your nearest outlet is: www.paypoint.com</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										How do I transfer credit to my meter?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Simply insert your key into your meter. The screen will briefly show the amount of credit on the key. To ensure your credit is transferred successfully, please leave your key inserted for at least 5 seconds.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Replacement Key')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											When should I expect to receive my Eversmart Energy prepayment meter key?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>You’ll receive your Eversmart Energy key before you switch to us. We’ll send you a letter or an email to let you know when your switch is due to take place. In the meantime, it’s best to top up little and often so that your credit dwindles ready for you to begin using your Eversmart Energy key.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										How do I activate my key?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Before you buy credit on your new key, you’ll need to activate it. Just insert it into your meter for at least 30 seconds and it’ll update your details.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<a class="card-title">
										What do I do if I've lost or broken my key?
										</a>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>Looking after your key is important to ensure you stay on supply, but we know that there may be the odd occasion when you need a replacement. Please call us immediately if this happens and we will arrange for you to collect one from a local outlet – we’ll give you a reference number that you’ll need to provide to the shop. If you can’t get to a shop, we’ll arrange for a new key to be posted to you, but this may take up to 3 working days to arrive.</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Energy Usage')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
										My energy usage is too high – what can I do?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>If you think you are using too much energy, you can find independent tips and advice on the Energy Saving Trust website: <a href='http://www.energysavingtrust.org.uk/'>http://www.energysavingtrust.org.uk</a></p>
										<p>The government website also has information about grants for energy efficiency improvements: <a href='https://www.gov.uk/energy-grants-calculator'>https://www.gov.uk/energy-grants-calculator</a></p>
										<p>For advice regarding fuel poverty and debt, or impartial advice about switching energy suppliers, you can contact your local Citizen’s Advice Bureau: <a href='https://www.citizensadvice.org.uk/about-us/contact-us/'>https://www.citizensadvice.org.uk/about-us/contact-us/uk</a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Smart Meters')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
										How does my emergency credit work?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Your prepayment meter has an emergency credit facility that allows you to ‘borrow’ money from the meter if your credit runs low. Our standard emergency credit amount is £5.00. This credit should only be used in an emergency. You’ll have to pay back to the meter, any emergency credit you borrow along with any debt owed. This will also include standing charges that may have built up while you’ve been using the emergency supply.</p>
										<p style="font-weight:bold">Electricity</p>
										<p>What your meter will display: Display number</p>
										<p>What this means:</p>
										<ul>
											<li><p><span style="font-weight:bold">A or 1:</span> How much you have left before your emergency supply runs out, e.g. £2.50E - the ‘E’ means emergency credit mode.</p></li>
											<li><p><span style="font-weight:bold">B or 2:</span> How much you need to put onto your meter to get it back to normal, e.g. £7.00</p></li>
										</ul>

										<p style="font-weight:bold">Gas</p>
										<p>To use the emergency credit just put your card into the meter and press the red button A. Your meter will then display 'Emergency credit in use'</p>

										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										Tips and information on using your prepayment meter:
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Only ever use your Eversmart Energy registered key – this ensures that you stay on our cheapest tariff rate and that we receive your payments. Remember to top up regularly, which you can do at any PayPoint outlet or Payzone If you lose your key or it is damaged, please contact us immediately.</p>
										<p><span style="font-weight">Going on holiday?</span> Remember to top up enough credit to cover your daily standing charge If you have a debt, your debt recovery rate will be set to £3.60 as a minimum.</p>
										<p><span style="font-weight">Moving House?</span> Don’t forget to let us know by completing one of our home movers forms. If you have a debt on your domestic prepayment meter, you may be able to switch suppliers under an industry process called Debt Assignment Protocol (DAP). Please see our FAQs for more information - also be sure to contact your chosen new supplier. We also have a Priority Service Register (PSR), dedicated to offering extra help. If you need us to do anything differently – such as write to you in Braille, send your bills to a relative or carer, or help you read your meter – just let us know by registering for the PSR. For more information regarding the PSR, please follow this link: <a href="https://www.ofgem.gov.uk/consumers/household-gas-and-electricity-guide/extra-help-energy-services/priority-services-register-people-need">Ofgem - Priority Services Register FAQs</a></p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<a class="card-title">
										Do I need to provide meter readings for my prepayment meter?
										</a>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>Eversmart Energy will receive a meter reading each time you top up your meter, however there may be occasions where a meter reading would be helpful. If we have a tariff change, you move in or out, or switch provider, giving us readings will ensure your account remains accurate.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										<a class="card-title">
										I can’t reach my meter and I find it difficult to top up. If I switch to Eversmart Energy are you able to move it for me?
										</a>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>This is something you should discuss with your current supplier as it’s important you have the ability to top up your meter and stay on supply. The switch to Eversmart Energy will take at least 21 days and with something as important as keeping yourself on supply, this should be addressed immediately.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										<a class="card-title">
										How do I take a meter reading?
										</a>
									</div>
									<div id="collapseFive" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p><span style="font-weight:bold">Electricity meters:</span> To read your key meter press the blue button until ‘screen H’ appears. Your reading will then be displayed. If you have an Economy7 meter it’ll also display screens J and L. Please take a note of these readings too</p>
										<p><span style="font-weight:bold">Gas meters:</span> If your gas meter has dials, please note the numbers in black. If your meter has an LED (digital) screen, press the black button twice and then press the red button to show the ‘Meter Index’ screen. The number displayed is your meter reading.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Smart Meters Installation')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
										Do I have to pay for my smart meter when I sign up?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>No. Your smart meter is supplied and fitted completely for free.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										Who will install my smart meter?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Your smart meter will be fitted and tested by a qualified engineer.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<a class="card-title">
										How long does it take to fit a smart meter?
										</a>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>It usually takes up to one hour per meter (gas and/or electricity). If you are a dual-fuel customer, it should take no longer than 2 hours.</p>
										<p>Please note that dual-fuel customers will need two smart meters – one for your gas supply and one for your electricity. We will aim to install both in the same visit. You will only need one in-home display.
										</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										<a class="card-title">
										If I switch away from Eversmart in the future, will I be charged for having my smart meter removed?
										</a>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>No. Your new supplier should be able to use the same meter. If for some reason your new supplier needs to change your meter, they cannot charge you for it.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										<a class="card-title">
										Can I keep using my old my old key/card for topping up?
										</a>
									</div>
									<div id="collapseFive" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>No, but you will receive a new key when your smart meter is fitted. You will then be able to top up via SMS, our app, online or over the phone.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										<a class="card-title">
										Do I need the Internet for my smart meter to work?
										</a>
									</div>
									<div id="collapseSix" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>No. Your smart meter uses a built-in SIM card to communicate with the supplier. The smart meter also connects wirelessly with your in-home device – it does not use Wi-Fi.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Moving Home')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											I'm moving home. What do I need to let you know?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>If you’re moving home, or have moved into a property which we supply via a prepayment meter, you will need to provide us with some details which you can do by filling out our home movers form. Alternatively you can call us on 0330 102 7901</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										I have just moved into a property and there is a prepayment meter and no key. How do I top up?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>We can arrange for you to be able to collect a new key from a local outlet – we’ll need to give you a reference number to be able to do this so will need to ensure we process your new account with Eversmart Energy and can then give you the information you will require. If you can’t get to a shop, we can send your key in the post but this may take up to 3 working days. If you have no credit left, we may be able to arrange for an emergency appointment to ensure you remain on supply.</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Warm Home Discount')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											Do you offer support and help via the warm home discount?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>No, we don’t currently offer the warm home discount.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										Warm Home Discount Scheme
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>The scheme was introduced by the Government in 2010 in conjunction with the large energy suppliers to support elderly householders receiving benefits to heat their homes. Because of the significant cost of participating in the Warm Home scheme, only the larger players are able to afford this without significantly increasing the bills of their own customers. Therefore energy suppliers with fewer than 250,000 customers, like Eversmart Energy, are not obligated to take part. Any customer is able to cancel their contract during the sign-up procedure, and we are happy to process this in order to support individuals who wish to switch to another supplier so that they are able to get the Warm Home discount.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Eversmart Schemes')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											Do Eversmart provide any schemes?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>Eversmart do not currently offer any schemes at this moment in time.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } 
					else if($_GET['m'] == 'Complaints')
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											I want to make a complaint, what should I do?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>We set out to offer exceptional customer service and aim to give you an easier, smarter, more stress-free solution to your energy. If for any reason you are not happy and would like to raise a complaint, please contact us on 0330 102 7901 or email complaints@eversmartenergy.co.uk.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php }
					else
					{?>
						<div class="row">
							<div class="col-sm-12 col-lg-12 col-md-12">
							<div id="accordion" class="accordion">
								<div class=" mb-0">
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapseOne">
										<a class="card-title">
											Can you switch my meter from a prepayment meter to a standard credit meter?
										</a>
									</div>
									<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>This is something our customer service team can review and discuss with you. If you have a debt it’s likely you’ll have to pay this off before we agree to change your status. We will install smart meter for you and changing from a prepay meter to a credit meter is done at a touch of a button.
</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<a class="card-title">
										Can you switch my meter from a standard credit meter to a prepayment meter?
										</a>
									</div>
									<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
										<div class="card-body"><p>We can review and discuss this with you to look at the options that might best suit your situation. We will install smart meter for you and changing from a credit meter to a prepay meter is done at a touch of a button</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
										<a class="card-title">
										As a pre-pay customer, am I able to sign up to your online portal?
										</a>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>As you’re on a prepayment meter, you won’t receive regular bills or statements and won’t have the need to make payments online so there are no benefits to signing up to our online portal at the stage.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
										<a class="card-title">
										What are the advantages of using a prepayment meter?
										</a>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion" >
										<div class="card-body">
											<ul>
												<li><p>Pay as you go – no surprise bills</p></li>
												<li><p>You can monitor your consumption</p></li>
												<li><p>Emergency credit option</p></li>
											</ul>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
										<a class="card-title">
										How do standing charges work with my meter?
										</a>
									</div>
									<div id="collapseFive" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>Prepayment meters apply a daily standing charge, so it’s important you keep topping up your credit to avoid building up any debt – even if you don’t actually use any electricity/ gas. If you run out of credit the standing charge will keep building up as a debt which will be deducted in full next time you top up. You can find out how much your daily standing charge is from your meter display.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
										<a class="card-title">
										How does the meter collect debt?
										</a>
									</div>
									<div id="collapseSix" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>If you’ve had a prepayment meter put in to help you to repay a debt, it'll be collected from the credit you put onto your meter at a weekly rate that we’ve agreed with you. If you check the displays on your meter regularly, you’ll see the debt amount going down. Once it’s all paid off, your meter will automatically stop collecting the debt. If you’re having difficulty paying your debt and keeping your meter topped up, please contact us as soon as you can and we’ll be happy to discuss some options with you.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
										<a class="card-title">
										I'd like to switch provider to someone who offers the wam home discount - how can I do this with a debt on my prepayment meter?
										</a>
									</div>
									<div id="collapseSeven" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>You may be able to switch provider if your debt is below £500 under a process called Debt Assignment Protocol (DAP), you would need to contact your preferred supplier who’ll be able to talk you through this. If your debt is greater than £500, please contact us to discuss your options.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
										<a class="card-title">
										I'm struggling to pay back my debt with my current supplier, can you reduce what I'm paying?
										</a>
									</div>
									<div id="collapseEight" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>We can review your current payment rate once you’ve switched, but it’s likely it will need to stay the same to make sure the debt is paid off. If you’re struggling with costs, it’s important you compare our tariff to your current provider’s. If you’re struggling to pay for your usage, you may want to consider asking for advice from an independent organisation. You can contact Citizens Advice on 0300 330 1313, Step Change on 0800 138 1111 or National Debt Line on 0808 808 4000.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
										<a class="card-title">
										I'm off supply, what should I do?
										</a>
									</div>
									<div id="collapseNine" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>If your supply has stopped due to a fault with your meter or your payment key, please contact us immediately on 0330 102 7901. We will arrange for an engineer to visit and put the problem right within 3 hours if you notify us on a working day, Monday-Friday between 8am-7pm, or within 4 hours on any other day. If we don’t meet this timeframe under our guaranteed standards of service, we’ll pay you compensation of £30.</p>
										</div>
									</div>
									<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
										<a class="card-title">
										What do I do if I'm going away?
										</a>
									</div>
									<div id="collapseTen" class="collapse" data-parent="#accordion" >
										<div class="card-body"><p>If you’re going away make sure that your meter is topped up with a suitable amount of credit. Even if you've turned all your appliances off, the meter will still collect the daily standing charge and also any weekly repayments towards any debt you may owe. Make sure you've also got enough credit for appliances you’ve left on, such as your fridge and freezer. If you run out, there'll be nobody home to start the emergency credit. And nobody wants to come home to a defrosted freezer!</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					<a style="font-size: 18px;margin: auto;padding: 20px;font-weight:400;line-height: 30px;" href="<?php echo base_url(); ?>index.php/Helpfaqs">Click here to go back to the Customer FAQ Topics</a>
				</div>
			</div>
			</div>				
			</section>
<!--------------------End breadcrumbs Care Section------------->
