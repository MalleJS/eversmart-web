
<!-----------------Start Owel Div------------>
<!----------Start Section One-------------- -->
<section class="page-header">
    <div class="container homecon">
        <div class="row">
            <div id="top-pad">
                <div class="col-sm-12 pull-left aos-item topWave-position" id="pullIphoneRight" data-aos="fade-right">

                    <div class="familynewimage">
                        <img src="<?php echo base_url(); ?>assets/images/home/familynew.png"></div>
                </div>


            </div>
            <div class="col-sm-6 pull-right">
                <div class="brand">
                    <h1 class="title hometitles aos-item social-text-update font-style-quick" data-aos="fade-down">Welcome to Eversmart<br>
                    </h1>
                    <p class="hook aos-item welcome-sub-text-update font-style-quick" data-aos="fade-left">
                        We are here to protect our customers from high prices and poor service, to see how much you could save, get a quote now!
                    </p>
                    <a class="btn btn-default btn-round blue-landing-btn  aos-item" data-aos="fade-up" href="<?php echo base_url(); ?>index.php/quotation">Get a Quote</a>

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</section>

<!----------End Start Section One--------------->
<!-----------------start Section 2-------------------
	<section class="section-why-switch">
		<div class="swicthimage"></div>
		<div class="row videobg">
			<div class="col-md-6 our-mission-img">
				<video controls class="frontVid" autoplay="false" loop="true" playsinline="" preload="auto" muted>
					<source src="<?php echo base_url(); ?>assets/vid/CustomerTestimonial.mp4" type="video/mp4">
				</video>
			</div>
			<div class="col-md-6">
				<div class="container our-mission-container section-padding-our-mission our_commitment_heading">
					<h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick " data-aos="fade-in">Making your energy<br>work harder for you!</h2>
					<p class="hook aos-item our-mission-text spacing-redesign font-style-quick" data-aos="fade-left">
					<b>Simply pay for your energy annually, and you'll get access to the Family Saver Club – our cheapest ever tariff. We'll also add 1% interest to your credit balance every month. That’s better value than an ISA or a high street savings account!</b></p>
					<!-- <div style="text-align:center;">
						<a class="btn btn-default btn-round blue-landing-btn  aos-item" data-aos="fade-up" href="<?php echo base_url(); ?>index.php/quotation">Join The Family</a>
					<!-- </div>	 -
				</div>
			</div>

			</div>

<section class="famfam-section">
	<div class="container">
    <div class="row famfam">
        <div class="fambox col-sm-4">
		<div class="faminner">
		<a href="mailto:loyalfamily@eversmartenergy.co.uk">
		<img src="<?php echo base_url(); ?>assets/images/home/black-back-closed-envelope-shape.png" style="max-width: 158px;">

		<div class="famboxtext">Email Us</div>
		</a>
</div>
		</div>
        <div class="fambox col-sm-4">
		<div class="faminner">
		<a href="tel:01613320022">
		<img src="<?php echo base_url(); ?>assets/images/home/phone-receiver.png" style=" max-width: 116px;">

		<div class="famboxtext">0161 332 0022</div>
		</a>
		</div></div>
        <div class="fambox col-sm-4" >
		<div class="faminner">
		 <a href="#" onclick="return false;" class="open_intercom">
		<img src="<?php echo base_url(); ?>assets/images/home/headset.png">

		<div class="famboxtext famthrid">Get a Call Back</div>
		</a>
		</div></div>
    </div>
</div>

			<div class="row videobg2">
			<div class="col-md-6">
				<div class="container mobilecon" style="width: 70%; padding-top: 100px; padding-bottom: 50px;">
					<h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick" data-aos="fade-in">Treat yourself to 1 year of free energy*</h2>
					<p class="hook aos-item our-mission-text spacing-redesign font-style-quick" style="min-width: 100%;" data-aos="fade-right"> Refer a friend or family member to Eversmart and we’ll give you both £50 in credit!</p>
					<p class="hook aos-item our-mission-text spacing-redesign font-style-quick" style="min-width: 100%;" data-aos="fade-right">It would only take twenty people to pay for your energy for a whole year!*</p>
					<div class="lmore">
						<a class="btn btn-default btn-round blue-landing-btn  aos-item" style="text-transform: none;" data-aos="fade-up" href="https://www.eversmartenergy.co.uk/blog/share-the-love-and-earn-50-in-free-credit/" target="_blank">Learn More</a>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<!--<div class="group-images-ipad padding-group-img">
				<img class="our-mission-background-img image2" style="width: 100%" src="<?php echo base_url(); ?>assets/images/AdobeStockdif.png" />
				<img class="our-mission-background-img image1" style="width: 100%" src="<?php echo base_url(); ?>assets/images/AdobeStock_206361930 (1)@2x.png" />
				<img class="our-mission-background-img image3" src="<?php echo base_url(); ?>assets/images/Group 3563@2x.png" />
				</div>	-
				<video controls class="frontVid2" autoplay="false" loop="true" muted="" playsinline="" preload="auto">
					<source src="<?php echo base_url(); ?>assets/vid/eversmarthappypeople.mp4" type="video/mp4">
				</video>

				</div>
			</div>
		</div>
		<!-- <img class="topLineImg" id="topLineImgIpad" src="<?php echo base_url(); ?>assets/images/topLineImg.png" /> -
	</section>-->


<!----------start Six Section-------------->
<section class="section-easy-steps">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 mx-auto aos-item" data-aos="fade-down">
                <h2 class="title_step text-center text-800" >Switching is easy – we do all the<br>
                    hard work for you!.</h2>
            </div>
        </div>
        <div class="steps-sm-md-lg">
            <div class="row">
                <div class="dottydots" data-aos="fade-in"></div>
                <div class="col-sm-4 text-center aos-item" data-aos="fade-right">
                    <div class="info wow fadeIn">
                        <div class="face">
                            <i class="material-icons"><img class="easyimgicon" src="<?php echo base_url(); ?>assets/images/home/stepone.png" alt=""/></i>
                        </div>
                        <span class="step_heading">Step 1</span>
                        <span class="step_intro">Register Your Details</span>
                        <p class="step_detail">Enter your postcode and tell us a little bit about your energy usage.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center aos-item" data-aos="fade-up">
                    <div class="info wow fadeIn">
                        <div class="handset">
                            <i class="material-icons"><img class="easyimgicon" src="<?php echo base_url(); ?>assets/images/home/steptwo.png" alt=""/></i>
                        </div>
                        <span class="step_heading">Step 2</span>
                        <span class="step_intro">Get a quote</span>
                        <p class="step_detail">Find out how much you could save with Eversmart.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center aos-item" data-aos="fade-left">
                    <div class="info wow fadeIn">
                        <div class="rose">
                            <i class="material-icons"><img class="easyimgicon" src="<?php echo base_url(); ?>assets/images/home/stepthree.png" alt=""/></i>
                        </div>
                        <span class="step_heading">Step 3</span>
                        <span class="step_intro">You're all done </span>
                        <p class="step_detail">Sit back and let us take care of the rest. After 21 days you’ll be part of the Eversmart family.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center aos-item" data-aos="fade-in">
                <a class="btn btn-default btn-round blue-landing-btn  aos-item" data-aos="fade-up" href="<?php echo base_url(); ?>index.php/quotation">SWITCH NOW</a>
            </div>
        </div>
    </div>
</section>
<!----------End Six Section------------->


<section class="volt-section">
    <div class="container">
        <div class="row volt">
            <div class="volttitle col-sm-12" data-aos="fade-left"><img class="volttitleimg" src="<?php echo base_url(); ?>assets/images/home/voltyvolt.png" />volt</div>
            <div class="voltleft col-sm-7">
                <div class="voltimgone voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/imageone.png" /></div>
                <div class="voltimgtwo voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/imagetwo.png" /></div>
                <div class="voltimgthree voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/image3.png" /></div>
                <div class="voltimgfour voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/image4.png" /></div>
                <div class="voltimgfive voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/image5.png" /></div>
                <div class="voltimgsix voltimg" data-aos="zoom-in"><img src="<?php echo base_url(); ?>assets/images/home/image6.png" /></div>
                <div class="voltmoney moneyone" data-aos="zoom-in">£65</div>
                <div class="voltmoney moneytwo" data-aos="zoom-in">£50</div>
            </div>
            <div class="voltright col-sm-5" data-aos="fade-left"><h2 class="voltrighttitle">Sharing is caring</h2>
                <p>With Eversmart, you can take credit from your account and give it to a friend or family member if they’re also with Eversmart.</p>
                <p>We call it Volt.</p>
                <ul class="voltul">
                    <li class="voltli">Times can be tough for those you love</li>
                    <li class="voltli">Share your energy through volt</li>
                    <li class="voltli">Give someone the gift of warmth and light</li>
                </ul>
                <div class="voltbtn"><a class="btn btn-default btn-round  aos-item" data-aos="fade-up" href="#">Coming Soon</a></div>
            </div>

        </div>
    </div>
    <div class="voltbtm"></div>
</section>


<!-----------------End Section 2-------------------->
<!----------------start Section 3---------------------->
<section class="three-top">
    <div class="container" id="renewableEngFixSmall">
        <h1 class="aos-item renewable-text-main renewable-text-size font-style-quick" data-aos="fade-down">Renewable energy</h1>
        <p class="aos-item renewable-text renewable-sub-text-size font-style-quick" data-aos="fade-down">Our electricity comes from 100% renewable
            sources</p>
        <div style="text-align:center;">
            <a class="btn btn-default btn-round blue-landing-btn  aos-item" data-aos="fade-up" href="<?php echo base_url(); ?>index.php/quotation">Switch Now</a>
        </div>
    </div>
</section>
<!----------------End Section 3--------------------->

<!-------------Start section all assest store--------------->

<section class="section-device-tech">

    <div class="row play_store_margin tech_device_ng">



        <div class="col-lg-7 col-md-12 col-sm-12">
            <img  class="play_second_img large_tech"src="<?php echo base_url(); ?>assets/images/home/all_device_assest.png" alt="" />

            <img class="play_second_img mobile_phone_img"src="<?php echo base_url(); ?>assets/images/home/mobile/mobile_tech.png" alt="" />

            <img class="play_second_img ipad_img"src="<?php echo base_url(); ?>assets/images/home/mobile/ipad_tech.png" alt="" />




        </div>

        <div class="col-lg-5 col-md-12 col-sm-12 device_tech_header">
            <h2 class="title newTitle aos-item social-text-update-black play_store_header font-style-quick " data-aos="fade-in">Tech that makes life easier</h2>
            <p id="device_tech_tech"  class="hook aos-item play_store_text font-style-quick" style="min-width: 100%;" data-aos="fade-right"> Manage your account online or with the Eversmart app. Send us<br>meter readings and pay your bills on the go!</p>


        </div>




</section>
<!-------------End section assest store--------------->

<!-------------Start section play store--------------->

<section class="section-app-store">

    <div class="row playstorebg">

        <div class="col-sm-12 vision_mobile">
            <div class="padding-group-img">
                <img id="app_iphone" style="margin:20px auto;" class="our-mission-background-img mobile_phone_img "  src="<?php echo base_url(); ?>assets/images/home/iphone_play.png" alt="" />

                <img id="app_ipad"  style="margin:20px auto;" class="our-mission-background-img ipad_img "  src="<?php echo base_url(); ?>assets/images/home/iphone_play.png" alt="" />

            </div>
        </div>

        <div class="col-md-6">
            <div class="container mobilecon vision_group" style="padding-top: 100px;">
                <h2 class="title newTitle aos-item social-text-update-black play_store_header font-style-quick" data-aos="fade-in">Manage your home energy the easy
                    way</h2>
                <p class="hook aos-item play_store_text font-style-quick" style="min-width: 100%;" data-aos="fade-right"> Whether you manage your home energy by prepay or credit you can keep control of your account on our IOS and Android application. </p>

                <!-- <span class="play_download_header">Download our Prepay App</span>
								<ul class="link_play_store">
									<li><a href="#"><img  src="<?php echo base_url(); ?>assets/images/home/play-store.png" alt="" /></a></li>
									<li><a href="#"><img  src="<?php echo base_url(); ?>assets/images/home/app-store.png" alt="" /></a></li>
								</ul> -->
                <span class="play_download_header">Download our Prepay<!-- Credit --> App!</span>
                <ul class="link_play_store">
                    <li><a href="https://play.google.com/store/apps/details?id=com.paypoint.eversmart" target="_blank"><img  src="<?php echo base_url(); ?>assets/images/home/play-store.png" alt="" /></a></li>
                    <li><a href="https://itunes.apple.com/gb/app/eversmart/id1261416062?mt=8" target="_blank"><img  src="<?php echo base_url(); ?>assets/images/home/app-store.png" alt="" /></a></li>
                </ul>
                <span class="play_download_header">Credit App launching soon!</span>

            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12" id="family-img">
            <div class="row play_store_margin">
                <div class="col-lg-5 col-sm-12 col-md-6">
                    <img class="play_first_img" src="<?php echo base_url(); ?>assets/images/home/iphone_play.png" alt="" />
                </div>
                <div class="col-lg-5 col-sm-12 col-md-6">
                    <img  class="play_second_img"src="<?php echo base_url(); ?>assets/images/home/AndroidPhone.png" alt="" />

                </div>

            </div>
        </div>
    </div>

</section>
<!-------------End section play store--------------->


<!----------------start Section 4---------------------->
<section class="section-smart-customer-services">
    <!-- <img class="bottomLineImg" src="<?php echo base_url(); ?>assets/images/bottomLineImg.png" /> -->

    <div class="container">
        <h2 class="aos-item text-800 font-style-quick letshaveachat" data-aos="fade-down">Let’s have a chat!</h2>
        <p class="letschatp">We get it – you’re busy! If you don’t have time to talk to us over the phone, just send us a text or chat to us on social media. <br>We’re here to help 7 days a week.</p>
        <div class="services-xs">
            <div class="row">
                <div class="col-xs-12">
                    <div class="phone-container text-center">
                        <div class="phone">
                            <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                            <img class="smsImage" role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                            <!-- <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                            <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                            </video> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row social-icons text-center">
                <div class="media-phone-fix">
                    <div class="col-xs-2 col-xs-push-2 social-icon smsIconHome">
                        <a href="sms:07537417858"> <img src="<?php echo base_url(); ?>assets/img/chat.png"></a>
                    </div>
                    <div class="col-xs-2 col-xs-push-2 social-icon facebookIconHome">
                        <a href="https://www.facebook.com/eversmartenergy" target="_blank">  <img src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg"></a>
                    </div>
                    <div class="col-xs-2 col-xs-push-2 social-icon">
                        <a href="https://telegram.me/eversmart_bot"> <img src="<?php echo base_url(); ?>assets/img/telegram.png"></a>
                    </div>
                    <div class="col-xs-2 col-xs-push-2 social-icon">
                        <a href="https://twitter.com/eversmartenergy" target="_blank">  <img src="<?php echo base_url(); ?>assets/img/twitter.png"></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="#" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn blue-landing-btn" data-wow-delay="400ms" data-placement="top" data-toggle="tooltip">
                        Get A Smart Meter
                    </a>
                </div>
            </div>
        </div>
        <div class="services-sm">
            <div class="row">
                <div class="col-sm-6">
                    <div class="phone-container text-center">
                        <div class="phone">
                            <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                            <img class="smsImage" role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                            <!-- <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                            <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                            </video> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-push-1">
                    <div class="row social-icons">
                        <div class="info info-horizontal hover-info wow fadeIn active" id="chatvid" data-wow-delay="0ms">
                            <div class="icon icon-primary icon-fix smsIconHome">
                                <a href="sms:07537417858" style="color:#000"> <img src="<?php echo base_url(); ?>assets/img/chat.png">
                            </div>
                            <div class="description">
                                <h3 class="social-title">SMS</h3></a>
                            </div>
                        </div>
                        <div class="info info-horizontal hover-info wow fadeIn" id="socialvid" data-wow-delay="400ms">
                            <div class="icon icon-primary icon-fix facebookIconHome">
                                <a href="https://www.facebook.com/eversmartenergy" target="_blank" style="color:#000">  <img src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg">
                            </div>
                            <div class="description">
                                <h3 class="social-title">Facebook</h3></a>
                            </div>
                        </div>

                        <div class="info info-horizontal hover-info wow fadeIn" id="twittervid" data-wow-delay="200ms">
                            <div class="icon icon-primary icon-fix">
                                <a href="https://twitter.com/eversmartenergy" target="_blank" style="color:#000">  <img src="<?php echo base_url(); ?>assets/img/twitter.png">
                            </div>
                            <div class="description">
                                <h3 class="social-title">Twitter</h3></a>
                            </div>
                        </div>
                        <div class="info info-horizontal hover-info wow fadeIn" id="telegramvid" data-wow-delay="600ms">
                            <div class="icon icon-primary icon-fix">
                                <a href="https://telegram.me/eversmart_bot" style="color:#000"><img src="<?php echo base_url(); ?>assets/img/telegram.png" class="social-icon">
                            </div>
                            <div class="description">
                                <h3 class="social-title">Telegram</h3></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="<?php echo base_url(); ?>index.php/quotation" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn" data-placement="top" data-toggle="tooltip">
                        Get A Smart Meter
                    </a>
                </div>
            </div>
        </div>
        <div class="services-md-lg">
            <div class="row hidden-xs hidden-sm">
                <div class="col-md-4">
                    <div class="info info-horizontal hover-info wow fadeIn active" id="chatvid">
                        <a href="sms:07537417858" style="color:black">
                            <div class="icon icon-primary right icon-fix smsIconHome">
                                <img class="social-icon aos-item" data-aos="fade-in" src="<?php echo base_url(); ?>assets/img/chat.png">
                            </div>

                            <div class="description align-right aos-item" data-aos="fade-right">
                                <h3 class="social-title"  style="margin-top: 25px;">SMS</h3>
                                <p>Just add 07537 417858 to your contacts and text away!</p>
                            </div>
                        </a>
                    </div>
                    <div class="info info-horizontal hover-info wow fadeIn" id="socialvid">
                        <a href="https://www.facebook.com/eversmartenergy" target="_blank" style="color:black">
                            <div class="icon icon-primary right icon-fix facebookIconHome fullColor">
                                <img class="social-icon aos-item" data-aos="fade-in" src="<?php echo base_url(); ?>assets/img/facebookMessenger.svg">
                            </div>
                            <div class="description align-right aos-item" data-aos="fade-right">
                                <h3 class="social-title">Facebook</h3>
                                <p>Follow us @eversmartenergy and drop us a line on Messenger.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="phone-container text-center">
                        <div class="phone aos-item" data-aos="fade-up">
                            <img role="presentation" src="<?php echo base_url(); ?>assets/img/Iphone6.png" alt="Phone">
                            <img class="smsImage aos-item" data-aos="fade-in"  role="presentation" src="<?php echo base_url(); ?>assets/img/smsImage.png" alt="Phone">
                            <!-- <video autoplay="" muted="" playsinline="" preload="auto" class="fbImage fbImageNew">
                            <source src="https://www.eversmartenergy.co.uk/assets/vid/eversmartFbTry.mp4" type="video/mp4">
                            </video> -->
                        </div>
                        <!-- <a href="<?php echo base_url(); ?>index.php/quotation" class="btn btn-md btn-primary btn-round wow fadeInUp get_smart_btn  aos-item" data-aos="fade-up" data-placement="top" data-toggle="tooltip">
						Get A free Smart Meter
						</a> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-horizontal hover-info wow fadeIn" id="twittervid" data-wow-delay="200ms">
                        <a href="https://twitter.com/eversmartenergy" target="_blank" style="color:black">
                            <div class="icon icon-primary icon-fix twitterIconHome">

                                <img class="social-icon aos-item" data-aos="fade-in"  src="<?php echo base_url(); ?>assets/img/twitter.png">
                            </div>
                            <div class="description aos-item" data-aos="fade-left">
                                <h3 class="social-title">Twitter</h3>
                                <p>If you have a question or just want to say hi, tweet us at @eversmartenergy</p>
                            </div>
                        </a>
                    </div>
                    <div class="info info-horizontal hover-info wow fadeIn" id="telegramvid" data-wow-delay="600ms">
                        <a href="https://telegram.me/eversmart_bot" style="color:black">
                            <div class="icon icon-primary icon-fix telegramIconHome">
                                <img src="<?php echo base_url(); ?>assets/img/telegram.png" class="social-icon aos-item" data-aos="fade-in">
                            </div>
                            <div class="description aos-item" data-aos="fade-left">
                                <h3 class="social-title">Telegram</h3>
                                <p>Download the Telegram app and add us to your contacts. Chat to one of our team members instantly.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!----------------End Section 4--------------------->
<!----------start Five Section-------------->
<section class="trustpilot-Section">
    <div class="container-fluid">
        <h1 class="text-center mb-3 font-style-quick" style="font-weight: 400;">What our customers say about us</h1>

        <!-- TrustBox widget - List -->
        <div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="598107d80000ff0005a7edbb" data-style-height="500px" data-style-width="100%" data-theme="light" data-tags="SelectedReview">
            <a href="https://uk.trustpilot.com/review/www.eversmartenergy.co.uk" target="_blank">Trustpilot</a>
        </div>
        <!-- End TrustBox widget -->
    </div>
</section>
<!----------------End Section 5--------------------->

<section class="section cd-section" id="guides" style="padding-top:60px">
    <div class="cd-section-grey">
        <div class="container">
            <div class="row" id="center_background">
                <div class="col-md-12 text-center" >
                    <span class="category_care aos-item" style="padding-top: 100px; font-weight: 400; font-size: 20px; z-index:22" data-aos="fade-down"> Customer Care</span>
                    <span class="grey_heading text-center aos-item" style="font-size: 38px; font-weight: 400;  z-index:22" data-aos="fade-up"> Guides &amp; FAQs</span>
                </div>
                <div class="row bottomboxes">
                    <div class="threeboxes col-sm-4 noxonebottom">
                        <a href="https://consumer.paypoint.com/" style="color:#000">
                            <img class="btmboximg" src="<?php echo base_url(); ?>assets/images/home/btmimage.png" alt=""/>
                            <div class="threboxetitles">Where is my nearest paypoint?</div>
                        </a>
                    </div>

                    <div class="threeboxes col-sm-4 boxtwobottom" id="boxtwobottom">
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Switching" style="color:#000">
                            <img class="btmboximg" src="<?php echo base_url(); ?>assets/images/home/btmimageone.png" alt=""/>
                            <div class="threboxetitles">How long does the switch take?</div>
                        </a>
                    </div>

                    <div class="threeboxes col-sm-4 boxthreebottom">
                        <a href="<?php echo base_url(); ?>index.php/Faqs?m=Topping%20Up" style="color:#000">
                            <img class="btmboximg" src="<?php echo base_url(); ?>assets/images/home/btmimagetree.png" alt=""/>
                            <div class="threboxetitles">What payment options do I have?</div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>


</section>
