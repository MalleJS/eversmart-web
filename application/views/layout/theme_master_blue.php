<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Switching Energy Suppliers Made Easy - Eversmart</title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- add social sharing poster image -->
    <meta property="og:title" content="Get a quote now" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://www.eversmartenergy.co.uk/assets/images/new-world/social-posters/ESE-Cyber-Monday.png" />
    <meta property="og:description" content="Switch now and Save up to £250 on your energy bill and get £100 credit on your balance." />

	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	<!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <?php 
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'msie')!==false || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'trident')!==false) { ?> 
    <!-- Include IE style sheet -->
    <link href="<?= base_url(); ?>assets/css/ie_styles.css" rel="stylesheet" type="text/css"> 
  <?php } ?>


	
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  <!-- <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<!-- loader css -->
<style>
body{font-family: 'Quicksand', sans-serif!important; }


.loading-page {
margin:20% auto;
width: 100px
 }

 .loader {
  border: 8px solid #fda1b8;
  border-radius: 50%;
  border-top: 8px solid #f64d76;
  width: 90px;
  height: 90px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


</style>

<!-- START Rakuten Marketing Tracking -->
 
<script type="text/javascript">
 
  (function (url) {
 
      /*Tracking Bootstrap
 
      Set Up DataLayer objects/properties here*/
 
      if(!window.DataLayer){
 
        window.DataLayer = {};
 
      }
 
      if(!DataLayer.events){
 
        DataLayer.events = {};
 
      }
 
      DataLayer.events.SiteSection = "1";
 
      
    
var loc, ct = document.createElement("script"); 
 
    ct.type = "text/javascript"; 
 
    ct.async = true;
 
    ct.src = url;
 
    loc = document.getElementsByTagName('script')[0];
 
    loc.parentNode.insertBefore(ct, loc);
 
  }(document.location.protocol + "//intljs.rmtag.com/115780.ct.js"));
 
</script>
 
<!-- END Rakuten Marketing Tracking -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div class="cd-panel cd-panel--from-left js-cd-panel-main">
   <header class="cd-panel__header">
   <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php  if( empty($this->session->userdata('login_data')) ){ ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/login">Login</a></li>
            <?php } ?>


            <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
            <!--<li><a href="<?php echo base_url(); ?>index.php/FamilySaver">Family Saver</a></li>-->
            <li><a href="<?php echo base_url(); ?>index.php/Vision">Our Vision</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Career">Careers</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Terms">T&Cs</a></li>

            <?php  if(  $this->session->userdata('login_data') ){?>
                <li><a href="#" onclick="logout_user()" id="logout_user">Log out</a></li>
            <?php } ?>
        </ul>
      </section>
    </nav>
    </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->

<body class="sky-blue-bg" id="red-txt-efect">
  <!-- laoder -->
  <div class="loading-page">
<div class="loader"></div>
</div>

    <!-- Start your project here-->
    <main class="cd-main-content">
	<div class="main-inner" id="main_content" style="display:none">
	<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar homemobile">
    <div class="container mobileonly">
				<div class="row">


					<span class="col-sm-12 col-md-4 col-lg-6 px-0 ">
          <a class="logo" href="<?php echo base_url(); ?>"><img class="evrmenu" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
					</span>

					<div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">

                        <p class="accnum">Account Number: </p>

                        <?php
                            if( $this->session->has_userdata('login_data') ){
                                if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
                                    echo '<span class="product-code-no-d">'.$this->session->userdata('login_data')['account_number'].'</span>';
                                }
                                elseif($this->session->userdata('login_data')['signup_type'] == 3)
                                {
                                    echo '<span class="product-code-no-d">'.$this->session->userdata('login_data')['account_number'].'</span>';
                                }
                            }
                        ?>

					</div>
					<div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="main"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> </button></div>
				

				</div>
			</div>
    <?php $this->load->view('layout/menu'); ?>
    


	</div>
    <!-- content -->
		<?php $this->load->view('energy/quotation'); ?>

		</div>
		</div>
		<!-- footer -->
    <?php $this->load->view('layout/common_footer'); ?>
                  </div>
