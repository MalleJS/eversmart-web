
<!----------------start postcode-------------------->

			<section class="mt0 ">
				<div class="boiler_header-h">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Careers</span>
									
								</div>
							</div>							
						</div><!------------------End Container---------------->
							
				</div><!------------------End Postcode------------>
				
			</section>
<!----------------End postcode-------------------->		
		</div>
		</div>
	<!----------------start postcode--------------------->



		<!--------------- Careers Download------------------------>
		
			<section class="career-download cd-section">
				<div class="container">
					<div class="row">
						<div class="col-md-10 mx-auto text-center aos-item" data-aos="fade-down">
							<h1>We are growing</h1>							
							<h2>We are a growing organisation with many opportunities. <br>
								If you would like to join us then please get in touch. </h2>
						
						</div>
					</div>

				<div class="row margintb60">
					<div class="col-sm-12 col-md-6 col-lg-6 aos-item" data-aos="fade-right">
						<div class="career-page">
							<a href="<?php echo base_url(); ?>index.php/vision">
							  <div class="content-overlay"></div>
							  <img alt="" class="content-image" src="<?php echo base_url(); ?>assets/images/career/evertsmart_vision.png">
							  <div class="content-details">
								<h3 class="content-title">Eversmart Vision</h3>
							
							  </div>
							</a>
						
						  </div>
					
					</div>
					
					<div class="col-sm-12 col-md-6 col-lg-6 aos-item" data-aos="fade-left">
					
						<div class="career-page">
							<a href="https://www.eversmartenergy.co.uk/blog/whats-it-like-working-for-eversmart/" target="_blank">
								<div class="content-overlay"></div>
								<img alt="" class="content-image" src="<?php echo base_url(); ?>assets/images/career/eversmart_benefit.png">
								<div class="content-details">
									<h3 class="content-title">Eversmart Benefits</h3>
								</div>
							</a>
						
						</div> 
			
					</div>
				</div>

					
				</div>	
			</section>
		
		
			<!--------------- End Careers Download----------------------->


<!--------------------start annual Section---->
			<section class="career-shape-two-edit">		
					<div class="row play_store_margin">
					
			<div class="col-md-6" id="family-img">
				<div class="group-images-ipad padding-group-img">
				<img class="our-mission-background-img career_image2" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/bg_career.png" />
				<img class="our-mission-background-img career_image1" style="width: 100%" src="<?php echo base_url(); ?>assets/images/career/career_page.png" />
				
				</div>
				</div>
				
				<div class="col-lg-6 col-sm-12 col-md-12">
				<div class="container mobilecon vision_group" style=" padding-top: 50px;">
					<h2 class="title newTitle aos-item social-text-update-black our-mission-header font-style-quick" data-aos="fade-in">Careers at Eversmart</h2> 
					<p class="hook aos-item spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">Are you looking for an exciting new role in a modern, fast-moving company? Then you’ve come to the right place!</p> 
					<p class="hook aos-item  spacing-redesign font-style-quick career_text" style="min-width: 100%;" data-aos="fade-right">
					Eversmart is on a mission to shake-up the energy sector, and we need the right people to help us get there. If you have the right mixture of skills, flexibility and positive attitude, then we want to hear from you.
								</p>
					
				</div>	
			</div>
			
			 <div class="col-sm-12 vision_mobile">
         <div class="padding-group-img">
            <img style="margin:20px auto; width:77%; height:auto;" class="our-mission-background-img mobile_phone_img "  src="<?php echo base_url(); ?>assets/images/career/mobile/career_one.png" alt="" />
			<img style="margin:20px auto;" class="our-mission-background-img ipad_img "  src="<?php echo base_url(); ?>assets/images/career/mobile/career_ipad.png" alt="" />
           
         </div>
      </div>
											
					</div><!------------------End Container----------------->					
			</section>
<!---------------end Annual Section------------------->


			
		<!--------------------Start breadcrumbs Care Section-------------->
			<section class="cs_switching-career" id="career_third">
				<div class="container">
					<div class="row">
						<div class=" accordian_heading col-sm-10 mx-auto text-center aos-item" data-aos="fade-down">					
							<h1 class="cft_heading text-center accordian_heading">Do we have a role for you?</h1>
							<h2 style="text-center">Eversmart is truly revolutionising the energy and smart home industry</h2>

							<div class="row">
								<div class="col-sm-12 col-lg-12 col-md-12">
									<div id="accordion" class="accordion">
										<div class=" mb-0 career-list red-bullet">
											<?php 
											$count = 0;
											if( !empty($get_jobs) ){
											for($i = 0; $i < count($get_jobs); $i++)
											{
												if(!empty($get_jobs[$i]->acf->jbtitle) && $get_jobs[$i]->acf->jbopen != 0)
												{
													$count ++;
													?>
													<div class="card-header collapsed aos-item" data-aos="fade-right" data-toggle="collapse" href="#collapse<?php echo $i ?>" style="text-align: left">
														<a class="card-title">
															<?php echo $get_jobs[$i]->acf->jbtitle;
														?>
														</a>
													</div>
													<div class="cv-pop">
													<div id="collapse<?php echo $i ?>" class="card-body collapse career-col" data-parent="#accordion" >
														<div class="card-body career-list">

															<div class="container">
																<div class="row">
																	<div class="col-sm-9">
																		<div class="career-brief">
																			<?php echo $get_jobs[$i]->acf->jbbrief; ?>
																		</div>

																		

																		<div class="career-desc">
																			<?php echo $get_jobs[$i]->acf->jbdesc;
																			?>
																			<p>To apply for this role, please email your CV and cover letter to <a href="mailto:careers@eversmartenergy.co.uk">careers@eversmartenergy.co.uk</a></p>
																		</div>
																	</div>

																	<div class="col-sm-3">
																		<div class="career-btns">
																			<a href=" <?php echo $get_jobs[$i]->link ?> " target="_blank"><button class="career-btn top" type="button"><span class="career-btn-span">More</span><br>Info</button></a>
																			<button class="career-btn cv" id="reveal" type="button" value="<?php echo $get_jobs[$i]->acf->jbtitle; ?>"><span class="career-btn-span">Upload</span><br>CV</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<?php
												}
											}
											}
											if($count == 0)
											{
												?>
												<p style="font-size: 19px; padding: 15px">Sorry – there a no roles available at the moment, but be sure to check back soon!</p>
												<?php
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>					
				<div id="popup-container">
					<div class="popup-background">
						<div class="popup">
							<div class="modal-header" id="contact-pop">
								<button type="button" class="close" id="close" data-dismiss="slideOut" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h4 class="modal-title">Upload your CV</h4>
							</div>
						<form id="cv" name="" method="post" >
							
							<div class="car_form">
								<input type="hidden" id="jobrole" class="form-control" required="" name="role" value="">
								<div class="md-form">
									<input type="text" id="car_name" class="form-control" required="" name="name" onkeypress="return">
									<label for="form1" class="">Full Name</label>
								</div>
								<div class="md-form">
									<input type="email" id="car_email" class="form-control" required="" name="email">
									<label for="form1" class="">Email Address</label>
								</div>
								<div class="md-form">
									<input type="file" id="cv_upload" class="form-control input-file" required="" name="cv" accept=".doc,.docx,application/msword,application/pdf" style="padding: 3px;">>
									<label for="form1" class=""></label>
								</div>
								<div class="col-sm-12 text-center" style="padding-top:30px">
									<input value="Submit" style="border-radius:30px" class="btn btn-info btn-round btn-lg waves-effect waves-light " id="cs_form_sub_btn" type="submit">
									<div id="message"></div>
								</div>
								
							</div>
						</form>
						</div>
					</div>
				</div>
			</section>
<!--------------------End breadcrumbs Care Section------------->


