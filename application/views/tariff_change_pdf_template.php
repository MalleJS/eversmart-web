<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29/08/2018
 * Time: 08:36
 */


ini_set('memory_limit','128M');
set_time_limit(0);

$FullWidth = 190;
$HeadingFont = 15;
$StandardFont = 11;
$TableFont = 10;
$StandardLineHeight = 6;
$Spacer = 4;

// Instanciation of inherited class
$this->myfpdf = new Myfpdf();
$this->myfpdf->AliasNbPages();
$this->myfpdf->AddPage();
$this->myfpdf->SetFont('Arial','',$StandardFont);

$CurrentY = $this->myfpdf->GetY();

$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell(95,$StandardLineHeight,$customer_name,0,1);

$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->Cell(95,$StandardLineHeight,$address_line_1,0,1);
if( $address_line_2 != '' ) { $this->myfpdf->Cell(95,$StandardLineHeight,$address_line_2,0,1); }
if( $address_line_3 != '' ) { $this->myfpdf->Cell(95,$StandardLineHeight,$address_line_3,0,1); }
$this->myfpdf->Cell(95,$StandardLineHeight,$postcode,0,1);

$this->myfpdf->SetXY(105, $CurrentY);
$this->myfpdf->Cell(95,$StandardLineHeight,'Customer number: '.$customer_number,0,1, 'R');

$this->myfpdf->SetX(105);
$this->myfpdf->Cell(95,$StandardLineHeight,'Email address: '.$email,0,1, 'R');

$this->myfpdf->SetX(105);
$this->myfpdf->Cell(95,$StandardLineHeight,'Date: '. date('jS F Y', 1540598400),0,1, 'R');

$this->myfpdf->Ln($Spacer*5);
$this->myfpdf->SetTextColor('234','73','92');
$this->myfpdf->SetFontSize($HeadingFont);
$this->myfpdf->Cell($FullWidth,10,'Our variable tariff is increasing',0,1);


$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell($FullWidth,10,'Hello '.$customer_name.',',0,1);

$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->SetTextColor('0','0','0');

$this->myfpdf->Ln($Spacer);
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'We are writing to inform you that our variable prices are changing, and this is due to take effect on the '.$tariff_change_date.'. The price of your tariff is increasing as shown below.',0,'L');

$this->myfpdf->Ln($Spacer);
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'Here at Eversmart Energy we are keen to build a long term relationship with our customers - we will always ensure our tariffs reflect the market conditions and are priced fairly.',0,'L');


// If we have a matching pair of data for any of the rows we need to show the table
if(
    ($current_elec_sc > 0 && $new_elec_sc  > 0) ||
    ($current_elec_dur > 0 && $new_elec_dur  > 0) ||
    ($current_gas_sc > 0 && $new_gas_sc  > 0) ||
    ($current_gas_ur > 0 && $new_gas_ur  > 0)
) {


    // Table Header Start
    $this->myfpdf->Ln($Spacer);

    $this->myfpdf->SetFont('Arial', '', $TableFont);
    $this->myfpdf->SetFillColor('234', '73', '92');
    $this->myfpdf->SetTextColor('255', '255', '255');

    $CurrentY = $this->myfpdf->GetY();

    $this->myfpdf->Cell(30, $StandardLineHeight, '', 0, 1, '', 1);

    $this->myfpdf->SetXY(40, $CurrentY);
    $this->myfpdf->Cell(46, $StandardLineHeight, '', 0, 1, '', 1);

    $this->myfpdf->SetXY(86, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, 'Current Price until ', 0, 1, 'L', 1);

    $this->myfpdf->SetXY(124, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, 'New price from ', 0, 1, 'L', 1);

    $this->myfpdf->SetXY(162, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, 'Price ', 0, 1, 'L', 1);

    $CurrentY = $this->myfpdf->GetY();
    $this->myfpdf->Cell(30, $StandardLineHeight, '', 0, 1, '', 1);

    $this->myfpdf->SetXY(40, $CurrentY);
    $this->myfpdf->Cell(46, $StandardLineHeight, '', 0, 1, '', 1);

    $this->myfpdf->SetXY(86, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, $tariff_date_to, 0, 1, 'L', 1);

    $this->myfpdf->SetXY(124, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, $tariff_date_from, 0, 1, 'L', 1);

    $this->myfpdf->SetXY(162, $CurrentY);
    $this->myfpdf->Cell(38, $StandardLineHeight, 'Change ', 0, 1, 'L', 1);
    // End Table Header


    // Table Data Start

    $this->myfpdf->SetTextColor('0', '0', '0');
    $this->myfpdf->SetFillColor('211', '211', '211');
    $this->myfpdf->SetDrawColor('234', '73', '92'); // Border color


    // Electricity
    if (($current_elec_sc > 0 && $new_elec_sc > 0) || ($current_elec_dur > 0 && $new_elec_dur > 0) || ($current_elec_nur > 0 && $new_elec_nur > 0 && $rate_type == 'E7' )) { // If we have both prices for either row...show

        $CurrentY = $this->myfpdf->GetY();
        $this->myfpdf->Cell(25, $StandardLineHeight, 'Electricity', 0, 1, 'L', 1);

        // Elec - Standing charge
        $this->myfpdf->SetXY(35, $CurrentY);
        $this->myfpdf->Cell(51, $StandardLineHeight, 'Standing Charge (per day)', 0, 1, 'L', 1);


        if ($current_elec_sc > 0) {
            $CurrentElecSC = round(($current_elec_sc * 100), 2);
        }
        else {
            $CurrentElecSC = round(0, 2);
        }
        $this->myfpdf->SetXY(86, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $CurrentElecSC.'p', 0, 1, 'L', 1);


        if ($new_elec_sc > 0) {
            $NewElecSC = round(($new_elec_sc * 100), 2);
        }
        else {
            $NewElecSC = round(0, 2);
        }
        $this->myfpdf->SetXY(124, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $NewElecSC.'p', 0, 1, 'L', 1);


        if ($CurrentElecSC >= 0 && $NewElecSC >= 0) {
            $IncreaseElecSC = $NewElecSC - $CurrentElecSC;
        }
        else {
            $IncreaseElecSC = round(0, 2);
        }
        $this->myfpdf->SetXY(162, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $IncreaseElecSC.'p', 0, 1, 'L', 1);



        if($current_elec_nur > 0 && $new_elec_nur > 0 && $rate_type == 'E7' ) { // If we have night rate values, this label needs to say 'Day unit rate'

            $Border = 0;

            // New Row - spacer
            $CurrentY = $this->myfpdf->GetY();
            $this->myfpdf->Cell(25, $StandardLineHeight, '', $Border, 1, 'L', 1);

            // Elec - Unit rate
            $this->myfpdf->SetXY(35, $CurrentY);

            $this->myfpdf->Cell(51, $StandardLineHeight, 'Day unit rate (per kWh)', $Border, 1, 'L', 1);
        }
        else { // Otherwise we can just say 'Unit rate'

            $Border = 'B';

            // New Row - spacer
            $CurrentY = $this->myfpdf->GetY();
            $this->myfpdf->Cell(25, $StandardLineHeight, '', $Border, 1, 'L', 1);

            // Elec - Unit rate
            $this->myfpdf->SetXY(35, $CurrentY);

            $this->myfpdf->Cell(51, $StandardLineHeight, 'Unit rate (per kWh)', $Border, 1, 'L', 1);
        }

        if ($current_elec_dur > 0) {
            $CurrentElecDur = round(($current_elec_dur * 100), 2);
        }
        else {
            $CurrentElecDur = round(0, 2);
        }
        $this->myfpdf->SetXY(86, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $CurrentElecDur.'p', $Border, 1, 'L', 1);

        if ($new_elec_dur > 0) {
            $NewElecDur = round(($new_elec_dur * 100), 2);
        }
        else {
            $NewElecDur = round(0, 2);
        }
        $this->myfpdf->SetXY(124, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $NewElecDur.'p', $Border, 1, 'L', 1);


        if ($CurrentElecDur >= 0 && $NewElecDur >= 0) {
            $TotalElecDur = $NewElecDur - $CurrentElecDur;
        }
        else {
            $TotalElecDur = round(0, 2);
        }
        $this->myfpdf->SetXY(162, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $TotalElecDur.'p', $Border, 1, 'L', 1);


        // Night unit rate
        if($current_elec_nur > 0 && $new_elec_nur > 0 && $rate_type == 'E7' ) {

            $CurrentY = $this->myfpdf->GetY();
            $this->myfpdf->Cell(25, $StandardLineHeight, '', 'B', 1, 'L', 1);


            // Elec - Unit rate
            $this->myfpdf->SetXY(35, $CurrentY);
            $this->myfpdf->Cell(51, $StandardLineHeight, 'Night unit rate (per kWh)', 'B', 1, 'L', 1);

            if ($current_elec_nur > 0) {
                $CurrentElecNur = round(($current_elec_nur * 100), 2);
            } else {
                $CurrentElecNur = round(0, 2);
            }
            $this->myfpdf->SetXY(86, $CurrentY);
            $this->myfpdf->Cell(38, $StandardLineHeight, $CurrentElecNur . 'p', 'B', 1, 'L', 1);

            if ($new_elec_nur > 0) {
                $NewElecNur = round(($new_elec_nur * 100), 2);
            } else {
                $NewElecNur = round(0, 2);
            }
            $this->myfpdf->SetXY(124, $CurrentY);
            $this->myfpdf->Cell(38, $StandardLineHeight, $NewElecNur . 'p', 'B', 1, 'L', 1);


            if ($CurrentElecNur >= 0 && $NewElecNur >= 0) {
                $TotalElecNur = $NewElecNur - $CurrentElecNur;
            } else {
                $TotalElecNur = round(0, 2);
            }
            $this->myfpdf->SetXY(162, $CurrentY);
            $this->myfpdf->Cell(38, $StandardLineHeight, $TotalElecNur . 'p', 'B', 1, 'L', 1);

        }

    }


    // Gas
    if (($current_gas_sc > 0 && $new_gas_sc > 0) || ($current_gas_ur > 0 && $new_gas_ur > 0)) { // If we have both prices for either row...show

        $CurrentY = $this->myfpdf->GetY();
        $this->myfpdf->Cell(25, $StandardLineHeight, 'Gas', 'T', 1, 'L', 1);

        // Gas - Standard Charge

        $this->myfpdf->SetXY(35, $CurrentY);
        $this->myfpdf->Cell(51, $StandardLineHeight, 'Standing Charge (per day)', 'T', 1, 'L', 1);


        if ($current_gas_sc > 0 ) {
            $CurrentGasSC = round(($current_gas_sc * 100), 2);
        }
        else {
            $CurrentGasSC = round(0, 2);
        }
        $this->myfpdf->SetXY(86, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $CurrentGasSC.'p', 'T', 1, 'L', 1);


        if ($new_gas_sc > 0) {
            $NewGasSC = round(($new_gas_sc * 100), 2);
        }
        else {
            $NewGasSC = round(0, 2);
        }
        $this->myfpdf->SetXY(124, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $NewGasSC.'p', 'T', 1, 'L', 1);


        if ($CurrentGasSC >= 0 && $NewGasSC >= 0) {
            $IncreaseGasSC = $NewGasSC - $CurrentGasSC ;
        }
        else {
            $IncreaseGasSC = round(0, 2);
        }
        $this->myfpdf->SetXY(162, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $IncreaseGasSC.'p', 'T', 1, 'L', 1);


        // New row - spacer
        $CurrentY = $this->myfpdf->GetY();
        $this->myfpdf->Cell(25, $StandardLineHeight, '', 'B', 1, 'L', 1);


        // Gas - Unit Rate
        $this->myfpdf->SetXY(35, $CurrentY);
        $this->myfpdf->Cell(51, $StandardLineHeight, 'Unit rate (per kWh)', 'B', 1, 'L', 1);

        if ($current_gas_ur > 0 ) {
            $CurrentGasUR = round(($current_gas_ur * 100), 2);
        }
        else {
            $CurrentGasUR = round(0, 2);
        }
        $this->myfpdf->SetXY(86, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $CurrentGasUR.'p', 'B', 1, 'L', 1);


        if ($new_gas_ur > 0) {
            $NewGasUR = round(($new_gas_ur * 100), 2);
        }
        else {
            $NewGasUR = round(0, 2);
        }
        $this->myfpdf->SetXY(124, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $NewGasUR.'p', 'B', 1, 'L', 1);


        if ($CurrentGasUR >= 0 && $NewGasUR >= 0) {
            $TotalGasUR = $NewGasUR - $CurrentGasUR;
        }
        else {
            $TotalGasUR = round(0, 2);
        }
        $this->myfpdf->SetXY(162, $CurrentY);
        $this->myfpdf->Cell(38, $StandardLineHeight, $TotalGasUR.'p', 'B', 1, 'L', 1);


    }


    // End Table data


    $this->myfpdf->Ln($Spacer);
    $this->myfpdf->SetFont('Arial', '', $StandardFont);
    $this->myfpdf->MultiCell($FullWidth, $StandardLineHeight, '*All prices include VAT where applicable (this may differ from how prices are shown on your bill or statement). Your variable tariff prices may change in the future: we will always notify you in advance.', 0, 'L');

}


$this->myfpdf->SetTextColor('234','73','92');
$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell($FullWidth,10,'Energy savings',0,1);

$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->SetTextColor('0','0','0');
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'You can reduce your costs by using less energy. Contact the Energy Saving Advice Service on 0300 123 1234 or the Energy Saving Trust at www.energysavingtrust.org.uk for impartial advice on how to save energy.',0,'L');

////////////////////////////////////////////////////
///////////////////// NEW PAGE /////////////////////
////////////////////////////////////////////////////

$this->myfpdf->AddPage();

$this->myfpdf->SetTextColor('234','73','92');
$this->myfpdf->SetFontSize($HeadingFont);
$this->myfpdf->Cell($FullWidth,10,'Remember - it might be worth thinking about switching your tariff or supplier',0,1);

$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell($FullWidth,10,'What will happen next?',0,1);

$this->myfpdf->SetTextColor('0','0','0');
$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->MultiCellBlt(195,$StandardLineHeight,chr(149),'If you choose to remain with us on a variable product, you will be moved to our new variable rates details above, with affect from '.$tariff_change_date.'.');

$this->myfpdf->Ln($Spacer/2);

$this->myfpdf->MultiCellBlt(195,$StandardLineHeight,chr(149),'Alternatively, you can choose to select one of our other tariffs. You can find full details of these on our website https://www.eversmartenergy.co.uk/ or you can call and speak to one of our advisors on 0330 102 7901, who will be more than happy to provide you with more information.');

$this->myfpdf->Ln($Spacer/2);

$this->myfpdf->MultiCellBlt(195,$StandardLineHeight,chr(149),'Should you wish to switch supplier, you can do so without incurring any exit fees. Simply start a switch with your new chosen provider to move away. Please be aware that your switch could be prevented if there are any outstanding charges on your account.');


$this->myfpdf->SetTextColor('234','73','92');
$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell($FullWidth,10,'Independent switching advice',0,1);

$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->SetTextColor('0','0','0');
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'For free, impartial energy advice you can contact the Citizen\'s Advice Consumer Service (CACS) on 0845 404 0506 or on the web at www.citizensadvice.org.uk/energy. Their Energy Consumer Checklist and Know Your Rights are available on our website, or you can call us if you need a copy by post.',0,'L');

$this->myfpdf->Ln($Spacer);
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'The energy markets regulator Ofgem publishes a code of practice for online price comparison services (the Confidence Code), available at https://www.ofgem.gov.uk/sites/default/files/docs/2015/03/confidence_code_of_practice_0.pdf',0,'L');


//$this->myfpdf->AddPage();


$this->myfpdf->SetTextColor('234','73','92');
$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell($FullWidth,10,'Key Contractual Terms',0,1);


$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->SetTextColor('0','0','0');
$this->myfpdf->MultiCell($FullWidth,$StandardLineHeight,'You may end your contract at any time (subject to any applicable exit fees shown in the table above), but you must settle any outstanding debt and provide a final meter reading.',0,'L');


$this->myfpdf->Ln($Spacer*2);

$this->myfpdf->SetFont('Arial','',$StandardFont);
$this->myfpdf->Cell(70,$StandardLineHeight,'Kind regards,','0',1,'L');

$this->myfpdf->Ln($Spacer);

$this->myfpdf->AddFont('BrushScriptMT','','brush.php');
$this->myfpdf->SetFont('BrushScriptMT','',$StandardFont*2);
$this->myfpdf->Cell(70,7,'O Boland','0',1,'L');

$this->myfpdf->Ln($Spacer);

$this->myfpdf->SetFont('Arial','B',$StandardFont);
$this->myfpdf->Cell(70,$StandardLineHeight,'Owen Boland','0',1,'L');
$this->myfpdf->Cell(70,$StandardLineHeight,'Customer Service Manager','0',1,'L');
$this->myfpdf->Cell(70,$StandardLineHeight,'Eversmart Energy','0',1,'L');


$this->myfpdf->Output('F', $filename);
//$this->myfpdf->Output();