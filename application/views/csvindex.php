<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Customer Import</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" rel="stylesheet" />
 
        <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/jquery-3.2.1.min.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
   
      <!-- common JavaScript -->
<style>
    .container {
    max-width: 100%;
}
caption {
    padding-top: 1.75rem;
    padding-bottom: 0.75rem;
    color: #6c757d;
    text-align: left;
    caption-side: bottom;
    display:none;
}
.nav-tabs>.active>a, .nav-tabs>.active>a:hover {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
}

.nav-tabs>li>a {
    padding-top: 8px;
    padding-bottom: 8px;
    line-height: 20px;
    border: 1px solid transparent;
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
}




.nav-list {
    padding-right: 15px;
    padding-left: 15px;
    margin-bottom: 0
}

.nav-list>li>a,.nav-list .nav-header {
    margin-right: -15px;
    margin-left: -15px;
    text-shadow: 0 1px 0 rgba(255,255,255,0.5)
}

.nav-list>li>a {
    padding: 3px 15px
}

.nav-list>.active>a,.nav-list>.active>a:hover {
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
    background-color: #08c
}

.nav-list [class^="icon-"] {
    margin-right: 2px
}

.nav-list .divider {
    *width: 100%;
    height: 1px;
    margin: 9px 1px;
    *margin: -5px 0 5px;
    overflow: hidden;
    background-color: #e5e5e5;
    border-bottom: 1px solid #fff
}

.nav-tabs,.nav-pills {
    *zoom:1}

.nav-tabs:before,.nav-pills:before,.nav-tabs:after,.nav-pills:after {
    display: table;
    line-height: 0;
    /* content:""; */
}

.nav-tabs:after,.nav-pills:after {
    clear: both
}

.nav-tabs>li,.nav-pills>li {
    float: left
}

.nav-tabs>li>a,.nav-pills>li>a {
    padding-right: 12px;
    padding-left: 12px;
    margin-right: 2px;
    line-height: 14px
}

ul.nav.nav-tabs a {
    color: #ea495c;
    font-weight: 600;
    display: block;
}

.nav-tabs {
    border-bottom: 1px solid #ddd
}

.nav-tabs>li {
    margin-bottom: -1px
}

.nav-tabs>li>a {
    padding-top: 8px;
    padding-bottom: 8px;
    line-height: 20px;
    border: 1px solid transparent;
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0
}

.nav-tabs>li>a:hover {
    border-color: #eee #eee #ddd
}

.nav-tabs>.active>a,.nav-tabs>.active>a:hover {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent
}

.nav-pills>li>a {
    padding-top: 8px;
    padding-bottom: 8px;
    margin-top: 2px;
    margin-bottom: 2px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px
}

.nav-pills>.active>a,.nav-pills>.active>a:hover {
    color: #fff;
    background-color: #08c
}

.nav-stacked>li {
    float: none
}

.nav-stacked>li>a {
    margin-right: 0
}

.nav-tabs.nav-stacked {
    border-bottom: 0
}

.nav-tabs.nav-stacked>li>a {
    border: 1px solid #ddd;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0
}

.nav-tabs.nav-stacked>li:first-child>a {
    -webkit-border-top-right-radius: 4px;
    border-top-right-radius: 4px;
    -webkit-border-top-left-radius: 4px;
    border-top-left-radius: 4px;
    -moz-border-radius-topright: 4px;
    -moz-border-radius-topleft: 4px
}

.nav-tabs.nav-stacked>li:last-child>a {
    -webkit-border-bottom-right-radius: 4px;
    border-bottom-right-radius: 4px;
    -webkit-border-bottom-left-radius: 4px;
    border-bottom-left-radius: 4px;
    -moz-border-radius-bottomright: 4px;
    -moz-border-radius-bottomleft: 4px
}

.nav-tabs.nav-stacked>li>a:hover {
    z-index: 2;
    border-color: #ddd
}

.nav-pills.nav-stacked>li>a {
    margin-bottom: 3px
}

.nav-pills.nav-stacked>li:last-child>a {
    margin-bottom: 1px
}

.nav-tabs .dropdown-menu {
    -webkit-border-radius: 0 0 6px 6px;
    -moz-border-radius: 0 0 6px 6px;
    border-radius: 0 0 6px 6px
}

.nav-pills .dropdown-menu {
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px
}

.nav .dropdown-toggle .caret {
    margin-top: 6px;
    border-top-color: #08c;
    border-bottom-color: #08c
}

.nav .dropdown-toggle:hover .caret {
    border-top-color: #005580;
    border-bottom-color: #005580
}

.nav-tabs .dropdown-toggle .caret {
    margin-top: 8px
}

.nav .active .dropdown-toggle .caret {
    border-top-color: #fff;
    border-bottom-color: #fff
}

.nav-tabs .active .dropdown-toggle .caret {
    border-top-color: #555;
    border-bottom-color: #555
}

.nav>.dropdown.active>a:hover {
    cursor: pointer
}

.nav-tabs .open .dropdown-toggle,.nav-pills .open .dropdown-toggle,.nav>li.dropdown.open.active>a:hover {
    color: #fff;
    background-color: #999;
    border-color: #999
}

.nav li.dropdown.open .caret,.nav li.dropdown.open.active .caret,.nav li.dropdown.open a:hover .caret {
    border-top-color: #fff;
    border-bottom-color: #fff;
    opacity: 1;
    filter: alpha(opacity=100)
}
ul.nav.nav-tabs a {
    font-weight: 550!important;
    font-size: 14px;
    display: block;
}
        #note {
            margin: auto;
            position: fixed;
            opacity: 0;
            bottom: -200px;
            right: 0

        }

        div#note img {
            max-width: 200px;
        }

        .red-rounded-wave {
            background: transparent;
            background-size: contain;
            float: left;
            width: 100%;
        }

        .termandconditions {
            margin-top: 0;
            background: white;
        }

        .text-left.field-sorting {
            font-size: 16px;
            text-transform: uppercase;
        }

        th.no-sorter {
            font-size: 16px;
            text-transform: uppercase;
        }

        td .text-left {
            text-align: left !important;
            color: #000;
            font-weight: 400;
        }

        table.tablesorter thead .field-sorting, table.tablesorter thead .field-sorting.asc, table.tablesorter thead .field-sorting.desc, table.tablesorter thead .field-sorting.asc_disabled, table.tablesorter thead .field-sorting.desc_disabled {
            cursor: pointer;
            padding-right: 0 !important;
        }

        .topbar .container {
            max-width: 1500px !important;
        }

        #my-energy-pd {
            padding-bottom: 0;
        }

        footer.main_footer {
            display: none;
        }

        #my-energy-pd .bolier_home-h-l {
            margin-top: 10px;
            position: relative;
            margin-top: -63px;
            font-size: 35px;
        }

        .loading-page {
            margin: 20% auto;
            width: 100px
        }

        .loader {
            border: 8px solid #fda1b8;
            border-radius: 50%;
            border-top: 8px solid #f64d76;
            width: 90px;
            height: 90px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .btn {
            display: inline-block;
            padding: 4px 14px;
            margin-bottom: 0;
            font-size: 14px;
            line-height: 20px;
            color: #000 !important;
        }

        .btn .caret {
            margin-top: 8px;
            margin-left: 0;
            display: none;
        }

        ul.pager a {
            background: #ea495c;
            color: #fff !important;
        }

        li.previous.first-button.disabled a {
            background: #ea495c !important;
        }

        section.termandconditions .col-sm-12.col-md-12.col-lg-8 {
            margin: auto;
            max-width: 100% !important;
            flex: 0 0 100% !important;
        }

        .large-screen-fix {
            width: 100% !important;
            margin: 0 auto;
        }

        .container {
            margin-right: auto;
            margin-left: auto;
            width: 100% !important;
            max-width: 100% !important;
        }

        .span12 {
            width: 100% !Important;
        }

        .maxy {
            max-width: 100%;
        }

        #ajax_list .span12 {
            padding: 0;
            margin: 0;
        }

        .nav {
            background-color: white;
            width: 100%;
            padding-top: 20px;
            margin-bottom: 0;
        }

        .nav-tabs > li {
            float:none;
            display:inline-block;
            zoom:1;
            width: 20%;
        }

        .nav-tabs {
            text-align:center;
        }
</style> 
    </head>

    <body>


<body class="sky-blue-bg">

<!-- Start your project here-->
<div class="main-inner" id="main_content">
    <div class="main-rounded-red-header nofixed">
        <div class="red-rounded-wave" id="dashboard_main_bg">

            <div class="topbar" id="red_top">

                <div class="container">

                    <div class="row">

                                <span class="col-sm-12 col-md-4 col-lg-6 px-0">
                                    <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>">eversmart.</a>
                                </span>

                        <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
                            <!--<span class="product-code-no-d"></span>-->
                        </div>

                        <div class=" col-sm-12 col-md-4 col-lg-2 text-right" id="navbarSupportedContent-3-main">

                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></a>

                            <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2">
                                <a id="logout_user" onclick="logout_user()" class="dropdown-item waves-effect waves-light" href="javascript:void(0)">
                                    <i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- End container-------->

            </div>


            <section class="mt0 ">
                <div class="boiler_header-h" id="my-energy-pd">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 mx-auto text-center">
                                <span class="bolier_home-h-l"><?=$page_title?></span>
                            </div>
                        </div>
                    </div>
                </div>


                <ul class="nav nav-tabs">
                    <?php if(in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,2) )) { ?>
                        <li <?=($page_ref=='meter_booking'?'class="active"':'');?>>
                            <a href="<?=base_url();?>index.php/Examples/meter_bookings">Meter Bookings</a>
                        </li>
                        <li <?=($page_ref=='junifer_pending'?'class="active"':'');?>>
                            <a href="<?=base_url();?>index.php/Pending_signups/pending">Junifer Pending Sign Ups</a>
                        </li>
                        <li <?=($page_ref=='dyball_pending'?'class="active"':'');?>>
                            <a href="<?=base_url();?>index.php/Dyball_signups/pending">Dyball Pending Sign Ups</a>
                        </li>
                    <?php } ?>
                    <?php if(in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,3) )) { ?>
                        <li class="active">
                            <a href="<?=base_url();?>index.php/csvupload/upload">CSV Import</a>
                        </li>
                    <?php } ?>

                    <?php if(in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,3) )) { ?>
                        <li <?=($page_ref=='tariffs'?'class="active"':'');?>>
                            <a href="<?=base_url();?>index.php/Tariff_management/tariff">Tariff Management</a>
                        </li>
                    <?php } ?>
                    <?php if(in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,4) )) { ?>
                        <li <?=($page_ref=='webtocase'?'class="active"':'');?>>
                            <a href="<?=base_url();?>index.php/Admin/webtocase">Web To Case</a>
                        </li>
                    <?php } ?>
                </ul>

            </section>


            <section class="termandconditions">

                <div class="container-fluid">

                    <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="large-screen-fix">

                            <div class="row justify-content-center">

                                <div style='height:20px;'></div>

                                <div class="maxy">

                                    <div class="maxy" class="container">

                                        <div class="maxy" class="col-md-12">
                                        <div class="container" style="margin-top:50px">    
             <br>
 
             <?php if (isset($error)): ?>
                <div class="alert alert-error"><?php echo $error; ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
            
            
            <h2>Chris's Incredible CSV Importer</h2> 
                <form method="post" action="<?php echo base_url() ?>csv/importcsv" enctype="multipart/form-data">
                    <input type="file" name="userfile" ><br><br>
                    <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
                </form>
 
            <br><br>
            <table class="table table-striped table-hover table-bordered">
                <caption>Christopher J. Callaghan</caption>
                <thead>
                    <tr>
                        <th>ACCOUNT NUMBER</th>
                        <th>MPAN</th>
                        <th>MPRN</th>
                        <th>ELEC START DATE</th>
                        <th>CUSTOMER_NAME</th>
                        <th>CONTACT_ADDRESS_1</th>
                        <th>CONTACT_POST_CODE</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($addressbook == FALSE): ?>
                        <tr><td colspan="50">There is currently No Data</td></tr>
                    <?php else: ?>
                        <?php foreach ($addressbook as $row): ?>
                            <tr>
                                <td><?php echo $row['ACCOUNT_NUMBER']; ?></td>
                                <td><?php echo $row['MPAN']; ?></td>
                                <td><?php echo $row['MPRN']; ?></td>
                                <td><?php echo $row['ELEC_START_DATE']; ?></td>
                                <td><?php echo $row['CUSTOMER_NAME']; ?></td>
                                <td><?php echo $row['CONTACT_ADDRESS_1']; ?></td>
                                <td><?php echo $row['CONTACT_POST_CODE']; ?></td>
                                <td><?php echo $row['EMAIL']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
 
 
            <hr>
            <footer>
                <p>&copy;Eversmart 2018</p>
            </footer>
 
        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>
 
      
 
 
 
    </body>
</html>