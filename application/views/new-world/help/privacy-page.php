<section class="hero hero-header container">
    <h1>Privacy Policy</h1>
</section>

<div id="privacy-page-wrap" class="page container">
    <section class="hero hero-fullwidth background-white">
        <a class="btn btn-eversmart-jumbo" href="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/pdfs/Privacy+Policy.pdf" title="Go to Privacy Policy" target="_blank">
            <div class="wrap">
                <div class="btn-contents">Energy Privacy Policy</div>
            </div>
        </a>
        <a class="btn btn-eversmart-jumbo" href="http://eversm.art/HEprivacypolicy" title="Go to Privacy Policy" target="_blank">
            <div class="wrap">
                <div class="btn-contents">Home Services Privacy Policy</div>
            </div>
        </a>
    </section>
</div>