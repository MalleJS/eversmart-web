<section class="hero hero-header container">
    <h1>Contact Us</h1>
</section>

<div id="contact-page-wrap" class="page container">
    <section class="hero hero-fullwidth background-white">
        <p class="text-center">Our friendly customer care team are happy to help, if you have any queries then feel free to get in touch:</p>
        <br />

        <div class="text-center contact-items">
            <div data-type="energy">
                <a class="btn btn-eversmart-jumbo cta-link open_intercom" href="#" title="" onclick="return false;" target="_blank">
                    <div class="wrap">
                        <div class="btn-contents">
                            <img src="/assets/images/new-world/icons/headset.svg" style="margin-bottom: 18px;" />
                            <br />
                            Live Chat
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </section>
</div>