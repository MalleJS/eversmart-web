<section class="hero hero-header container">
    <h1>Frequently Asked Questions</h1>
</section>

<div id="faqs-page-wrap" class="page container">
    <section class="section-general hero hero-fullwidth background-white">
        <h2>Customer FAQ Topics</h2>
        <p class="text-left">Our help and advice section is growing all the time and contains many of the questions and enquiries that our customer service team answer everyday. If you can't find what you're looking for, please <a href="/contact_us" title="Go to the contact us page">contact us</a>.</p>
        
        <div class="faqs-type">
            <h3 class="text-center text-brand">Please pick from one of the options below:</h3>
            <div data-type="energy">
                <a class="btn btn-eversmart-jumbo FAQ-type-link" href="#" title="eversmart energy FAQS" data-type="energy">
                    <div class="wrap">
                        <div class="btn-contents">
                            <img src="./assets/images/new-world/home-services/icons/energy.png" style="margin-bottom: 18px;" />
                            <br />
                            Energy FAQS
                        </div>
                    </div>
                </a>
            </div>
            <div data-type="homeservices">
                <a class="btn btn-eversmart-jumbo FAQ-type-link" href="#" title="Home Services FAQS" data-type="homeservices">
                    <div class="wrap">
                        <div class="btn-contents">
                            <img src="./assets/images/new-world/home-services/icons/home.png" style="margin-bottom: 18px;" />
                            <br />
                            Home Services FAQS
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <section class="hero hero-fullwidth background-grey">
        <div class="layout-100-lg layout-100-md layout-100-sm layout-100-xs content-vertical-center" style="padding-bottom: 24px; padding-left: 6px; padding-right: 6px;">
            <a href="#" id="faqs-step-back-button">
                <div class="img-wrap">
                    <img src="/assets/images/new-world/icons/arrow-500-black.svg" alt="Go Back to Topics">
                </div>
                <span>Back</span>
            </a>
            <div class="topic-info-wrap"></div>
            <div class="topics-wrap">
                <ul class="topics-list">
                    <!-- DYNAMICLY ADDED CONTENT -->
                </ul>
            </div>
        </div>
    </section>
</div>