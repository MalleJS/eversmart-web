<section class="hero hero-header container">
    <h1>Emergency Numbers</h1>
</section>

<div id="emergency-page-wrap" class="page container">
    <section class="hero hero-padding-small hero-fullwidth background-white text-center">
        <h3 class='text-center'>National Gas Helpline</h3>
        <p>
            If you think you have a gas leak or can smell gas, leave the house and phone the National Gas Emergencies
            number immediately on <a class='contentLink' href="tel:0800 111 999">0800 111 999</a>.
        </p>

        <h3 class='text-center'>National Power Cut Helpline</h3>
        <p>
            If you have or suspect an electrical power cut, call the National Power Cut Helpline from any landline on <a class='contentLink' href="tel:105">105</a> or <a class='contentLink' href="tel:0800 31 63 105">0800 31 63 105</a> from a mobile (24 hours, 7
            days a week).
        </p>
    </section>
</div>