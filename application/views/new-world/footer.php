            <div class="sections-wrap">
				<section data-type="contact" style="text-align: center;">
					<img src="<?php echo base_url(); ?>assets/images/new-world/logos/logo-2019-black.png" alt="eversmart energy ltd." style="width: 150px; margin-left: auto; display: inline-block; margin-right: auto; margin-top: 32px; margin-bottom: 18px;"/>
					<br />
					<ul>
						<li style="text-align: center; width: 100%;"><a href="tel:03301027901" title="0330 102 7901" style="font-size:1.5em;">0330 102 7901</a></li>
						<li style="text-align: center; width: 100%;"><a href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry" title="hello@eversmartenergy.co.uk" style="font-size:1.5em; color: #ea495c" >hello@eversmartenergy.co.uk</a></li>
					</ul>
				</section>
				<section data-type="opening">
					<h5>Opening Times</h5>
					<div>
						<ul class="opening-hours">
							<li>WEEKDAYS: 8AM-6PM</li>
							<li>SATURDAYS: 9AM-1PM</li>
						</ul>
					</div>
					<br />
					<h5>Emergency Hours:</h5>
					<div>
						<ul class="opening-hours">
							<li>WEEKDAYS: 6PM-8PM</li>
							<li>SATURDAYS: 1PM-5PM</li>
							<li>SUNDAYS: 9AM-5PM</li>
						</ul>
					</div>
				</section>
				<section data-type="useful">
					<h5>Useful Links</h5>
					<ul>
						<li>
							<a href="/blog" title="Eversmart Blog" >Eversmart Blog</a>
						</li>
						<li>
							<a href="/Terms" title="Terms & Conditions" >Terms & Conditions</a>
						</li>
						<li>
							<a href="/Policies" title="Privacy" >Privacy</a>
						</li>
                        <li>
                            <a target="_blank" href="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/Tariff+Termination+Table+Draft_+(002)+02-05+2019.pdf" title="Tariff Termination" >Tariff Termination Table</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/Priority+Services+Register.pdf" title="PSR" >Priority Services Register</a>
                        </li>
						<?php if( !isset($hide_consumer_checklist) ){ ?>
							<li>
								<a href="/assets/pdf/consumercheck_eng.pdf" target="_blank" title="Consumer Checklist (UK)" >Consumer Checklist (UK)</a>
							</li>
							<li>
								<a href="/assets/pdf/consumercheck_wel.pdf" title="Consumer Checklist (Welsh)" >Consumer Checklist (Welsh)</a>
							</li>
						<?php }; ?>
					</ul>
				</section>
				<section data-type="services-support">
					<h5>Support</h5>
					<ul>
						<li>
							<a href="/Helpfaqs" title="Help & Support" >Help & Support</a>
						</li>
						<li>
							<a href="http://18.218.252.81" title="eversmart community">Eversmart Community</a>
						</li>
						<li>
							<a href="/contact_us" title="Contact Us" >Contact Us</a>
						</li>
					</ul>
				</section>
				
			</div>
			<div class="bottom">
				<div class="social-wrap">
					<ul>
						<li>
							<a href="https://twitter.com/eversmartenergy" target="_blank" rel="noopener" title="Twitter">
								<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDU2LjY5MyA1Ni42OTMiIGhlaWdodD0iNTYuNjkzcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iNTYuNjkzcHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwYXRoIGQ9Ik01Mi44MzcsMTUuMDY1Yy0xLjgxMSwwLjgwNS0zLjc2LDEuMzQ4LTUuODA1LDEuNTkxYzIuMDg4LTEuMjUsMy42ODktMy4yMyw0LjQ0NC01LjU5MmMtMS45NTMsMS4xNTktNC4xMTUsMi02LjQxOCwyLjQ1NCAgYy0xLjg0My0xLjk2NC00LjQ3LTMuMTkyLTcuMzc3LTMuMTkyYy01LjU4MSwwLTEwLjEwNiw0LjUyNS0xMC4xMDYsMTAuMTA3YzAsMC43OTEsMC4wODksMS41NjIsMC4yNjIsMi4zMDMgIGMtOC40LTAuNDIyLTE1Ljg0OC00LjQ0NS0yMC44MzMtMTAuNTZjLTAuODcsMS40OTItMS4zNjgsMy4yMjgtMS4zNjgsNS4wODJjMCwzLjUwNiwxLjc4NCw2LjYsNC40OTYsOC40MTIgIGMtMS42NTYtMC4wNTMtMy4yMTUtMC41MDgtNC41NzgtMS4yNjVjLTAuMDAxLDAuMDQyLTAuMDAxLDAuMDg1LTAuMDAxLDAuMTI4YzAsNC44OTYsMy40ODQsOC45OCw4LjEwOCw5LjkxICBjLTAuODQ4LDAuMjMtMS43NDEsMC4zNTQtMi42NjMsMC4zNTRjLTAuNjUyLDAtMS4yODUtMC4wNjMtMS45MDItMC4xODJjMS4yODcsNC4wMTUsNS4wMTksNi45MzgsOS40NDEsNy4wMTkgIGMtMy40NTksMi43MTEtNy44MTYsNC4zMjctMTIuNTUyLDQuMzI3Yy0wLjgxNSwwLTEuNjItMC4wNDgtMi40MTEtMC4xNDJjNC40NzQsMi44NjksOS43ODYsNC41NDEsMTUuNDkzLDQuNTQxICBjMTguNTkxLDAsMjguNzU2LTE1LjQsMjguNzU2LTI4Ljc1NmMwLTAuNDM4LTAuMDA5LTAuODc1LTAuMDI4LTEuMzA5QzQ5Ljc2OSwxOC44NzMsNTEuNDgzLDE3LjA5Miw1Mi44MzcsMTUuMDY1eiIvPjwvc3ZnPg==" alt="Twitter" />
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/eversmartenergy" target="_blank" rel="noopener" title="Facebook">
								<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxNzkyIiB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiB3aWR0aD0iMTc5MiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTU3OSAxMjhxMzUgMCA2MCAyNXQyNSA2MHYxMzY2cTAgMzUtMjUgNjB0LTYwIDI1aC0zOTF2LTU5NWgxOTlsMzAtMjMyaC0yMjl2LTE0OHEwLTU2IDIzLjUtODR0OTEuNS0yOGwxMjItMXYtMjA3cS02My05LTE3OC05LTEzNiAwLTIxNy41IDgwdC04MS41IDIyNnYxNzFoLTIwMHYyMzJoMjAwdjU5NWgtNzM1cS0zNSAwLTYwLTI1dC0yNS02MHYtMTM2NnEwLTM1IDI1LTYwdDYwLTI1aDEzNjZ6Ii8+PC9zdmc+" alt="Facebook" />
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/eversmartenergy/" target="_blank" rel="noopener" title="Instagram">
								<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIxLjAuMiwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCA1Ni43IDU2LjciIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDU2LjcgNTYuNyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTI4LjIsMTYuN2MtNywwLTEyLjgsNS43LTEyLjgsMTIuOHM1LjcsMTIuOCwxMi44LDEyLjhTNDEsMzYuNSw0MSwyOS41UzM1LjIsMTYuNywyOC4yLDE2Ljd6IE0yOC4yLDM3LjcKCQljLTQuNSwwLTguMi0zLjctOC4yLTguMnMzLjctOC4yLDguMi04LjJzOC4yLDMuNyw4LjIsOC4yUzMyLjcsMzcuNywyOC4yLDM3Ljd6Ii8+Cgk8Y2lyY2xlIGN4PSI0MS41IiBjeT0iMTYuNCIgcj0iMi45Ii8+Cgk8cGF0aCBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45CgkJaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zCgkJcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNgoJCWMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPgo8L2c+Cjwvc3ZnPgo=" alt="Instagram" />
							</a>
						</li>
					</ul>
				</div>
				<div class="statement-wrap">
					<span>© <?= date('Y') ?> Eversmart Energy Ltd - 09310427</span>
					<br />
					<p style="margin-top: 22px">Eversmart Ltd is authorised and regulated by the Financial Conduct Authority. Registered in England and Wales (No. 09245750). Registered office: 26 Brindley Road, Manchester, M169HQ</p>
				</div>
			</div>