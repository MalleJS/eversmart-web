            <div id="menu-bar">
            	<a href="/" id="logo-wrap">
            		<div class="site-logo"></div>
            	</a>
            	<a href="#" id="menu-icon" title="Menu Open / Close">
            		<button class="hamburger hamburger--vortex" type="button">
            			<span class="hamburger-box">
            				<span class="hamburger-inner"></span>
            			</span>
            		</button>
            	</a>
            </div>
            <section id="menu-wrap">
            	<nav id="menu">
            		<ul id="menu-list">
            			<!-- DYNAMICLY LOADED -->
            		</ul>
            		<div class="button-wrap">
            			<a href="http://portal.eversmartenergy.co.uk" target="_blank" class="menu-extras">My Account</a>
            			<a href="https://eversmartpayments.paypoint.com/energy/" class="menu-extras">Top-Up</a>
            			<a href="/quotation" class="menu-extras">Get Quote</a>
            		</div>
            	</nav>
            </section>