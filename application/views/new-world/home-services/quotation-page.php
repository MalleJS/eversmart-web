<div id="quotation-page">
    <div id="quote-step-wrap">
        <h2 id="quote-step-title"></h2>
        <a href="#" id="quote-step-back-button">
            <div class="button-wrap">
                <span>Back</span>
            </div>
        </a>
        <div id="quote-step">
            <!-- DYNAMICLY GENERATED -->
        </div>
    </div>
</div>
<div class="asterixs">
    <p>Eversmart Limited is authorised and regulated by the Financial Conduct Authority. Registered in England and Wales (No. 09245750). Registered office: 26 Brindley Road, Manchester, M169HQ</p>
</div>
