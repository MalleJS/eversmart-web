<script>
    // window.APIUrl = "http://staging.eversmartenergy.com";
</script>

<section class="hero hero-img">
    <h1>Boiler &amp; Home Protect</h1>
    <p class="text-center">
        Let us do the worrying so you don't have to.
    </p>
</section>

<div id="landing-page-wrap" class="page">
    <section class="hero hero-fullwidth background-grey">
        <div class="container">
            <div class="layout-100-lg layout-100-md layout-100-sm layout-100-xs hero-padding-small content-vertical-center">
                <br />
                <h1 class="text-center">Easy and Simple to get covered now</h1>
                <p class="text-center">
                We have a range of home protect products to ensure your family and your home are protected 24/7 365 days a year. Whether it’s your boiler, central heating, plumbing or electrics. Our home cover helps you avoid the unexpected costs of those “uh-oh” moments we can all do without.      
                </p>
            </div>
        </div>
    </section>

<div class="container">

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding-small content-vertical-center">
            <h3 class="text-left">Home Protect Bronze</h3>
            <ul class="items text-left">
                <li class="included"><span>Boiler</span></li>
                <li class="included"><span>Alternative Accommodation</span></li>
                <li><span>Central Heating</span></li>
                <li><span>Electricity Emergency</span></li>
                <li><span>Gas Emergency</span></li>
                <li><span>Water Emergency</span></li>
                <li><span>Plumbing &amp; Drainage</span></li>
                <li><span>Security, Lost Keys, Roof &amp; Pest</span></li>
            </ul>
            <a target="_blank"  class="product-button" href="/homeservices/quote#01t1n00000DuLMuAAN"> View Home Protect Bronze </a>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-bronze.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="hidden-xs layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-silver.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding-small content-vertical-center">
            <h3 class="text-left">Home Protect Silver</h3>
            <ul class="items text-left">
                <li class="included"><span>Boiler</span></li>
                <li class="included"><span>Alternative Accommodation</span></li>
                <li class="included"><span>Central Heating</span></li>
                <li><span>Electricity Emergency</span></li>
                <li><span>Gas Emergency</span></li>
                <li><span>Water Emergency</span></li>
                <li><span>Plumbing &amp; Drainage</span></li>
                <li><span>Security, Lost Keys, Roof &amp; Pest</span></li>
            </ul>
            <a target="_blank"  class="product-button" href="/homeservices/quote#01t1n00000DuLMzAAN"> View Home Protect Silver </a>
        </div>
        <div class="hidden-sm hidden-md hidden-lg hidden-xl layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-silver.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding-small content-vertical-center">
            <h3 class="text-left">Home Protect Gold</h3>
            <ul class="items text-left">
                <li class="included"><span>Boiler</span></li>
                <li class="included"><span>Alternative Accommodation</span></li>
                <li class="included"><span>Central Heating</span></li>
                <li class="included"><span>Electricity Emergency</span></li>
                <li class="included"><span>Gas Emergency</span></li>
                <li><span>Water Emergency</span></li>
                <li><span>Plumbing &amp; Drainage</span></li>
                <li><span>Security, Lost Keys, Roof &amp; Pest</span></li>
            </ul>
            <a target="_blank" class="product-button" href="/homeservices/quote#01t1n00000DuLN5AAN"> View Home Protect Gold </a>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-gold.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="hidden-xs layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-platinum.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding-small content-vertical-center">
            <h3 class="text-left">Home Protect Platinum</h3>
            <ul class="items text-left">
                <li class="included"><span>Boiler</span></li>
                <li class="included"><span>Alternative Accommodation</span></li>
                <li class="included"><span>Central Heating</span></li>
                <li class="included"><span>Electricity Emergency</span></li>
                <li class="included"><span>Gas Emergency</span></li>
                <li class="included"><span>Water Emergency</span></li>
                <li class="included"><span>Plumbing &amp; Drainage</span></li>
                <li class="included"><span>Security, Lost Keys, Roof &amp; Pest</span></li>
            </ul>
            <a target="_blank" class="product-button" href="/homeservices/quote#01t1n00000DuLNBAA3"> View Home Protect Platinum </a>
        </div>
        <div class="hidden-sm hidden-md hidden-lg hidden-xl layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/home-services/hp-platinum.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>
    </div>
</div>