<div id="signup-page">
    <div id="signup-body">
        <div class="form-wrap">
            <h1>Enter your details</h1>
            <form>
                <select class="required" name="title" data-wtc="title">
                    <option value="Title" selected disabled>Title</option>
                    <option value="M">M</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Miss">Miss</option>
                    <option value="Sir">Sir</option>
                    <option value="Dr">Dr</option>
                    <option value="Professor">Professor</option>
                    <option value="Chancellor">Chancellor</option>
                    <option value="Vice-Chancellor">Vice-Chancellor</option>
                    <option value="His Grace">His Grace</option>
                    <option value="His Lordship">His Lordship</option>
                    <option value="The Reverend">The Reverend</option>
                    <option value="Fr">Fr</option>
                    <option value="Pr">Pr</option>
                    <option value="Sr">Sr</option>
                    <option value="QC">QC</option>
                    <option value="Cl">Cl</option>
                    <option value="Dame">Dame</option>
                    <option value="Lord">Lord</option>
                    <option value="Lady">Lady</option>
                    <option value="Hon">Hon</option>
                    <option value="Rt Hon">Rt Hon</option>
                    <option value="The Most Honourable">The Most Honourable</option>
                </select>
                <input type="text" name="forename" class="required" placeholder="First Name" data-wtc="" />
                <input type="text" name="surname" class="required" placeholder="Last name" data-wtc="" />
                <input type="text" name="email" class="required" placeholder="Email" data-wtc="" />
                <input type="password" name="password" class="required" placeholder="Password" data-wtc="" />
                <div class="password-strength">
                    <div class="bar-wrap">
                        <div class="bar"></div>
                    </div>
                    <span class="caption">
                        <span class="label">Password Strength:</span>
                        <span class="value"></span>
                    </span>
                </div>
                <input type="password" name="password-confirm" class="required" placeholder="Confirm Password" data-wtc="" />
                <input type="tel" name="telephone" class="required" placeholder="Telephone" data-wtc="" />
                <input type="tel" name="phone" class="required" placeholder="Phone Number" data-wtc="" />
                <input type="date" name="dob" class="required" placeholder="Date of Birth" data-wtc="" />
                <input type="text" name="address-first-line" class="required" data-wtc="" readonly/>
                <input type="text" name="city" class="required" data-wtc="" readonly/>
                <input type="text" name="county" class="required" data-wtc="" readonly/>
                <input type="text" name="postcode" class="required" data-wtc="" readonly/>
                <div class="checkbox-wrap">
                    <a href="#" class="input checkbox"></a>
                    <p>Are you eligible for priority register service?</p>
                </div>
                <a href="#" class="submit" title="submit-form">signup</a>
            </form>
        </div>
    </div>
</div>