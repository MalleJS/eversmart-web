<?php require_once 'header.php';
   // debug($profile,1);
 ?>
 <style type="text/css">
     .dashboard.dashboard3 .dashboardTable {  max-width: 800px; }
     .dashboardTable table { text-align: left; }
     .dashboardTable .imageTop { border-radius: 10px; }
 </style>
        
        <div class="box">
            <?php if( !empty( $this->session->userdata('elec_mpan') )  ) { ?> 
            <div class="box1">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                <div class="text"><b>Electricity</b></br> <?= $elec_mpan; ?></div>
            </div>  
                <div class="greyLine"></div>
                <div class="boxText"><a href="<?= base_url() ?>index.php/portal/elec_reading">View/Update Reading</a></div>
            </div>
            
            <?php } if( !empty( $this->session->userdata('gas_mprn') )  ) { ?> 
            <div class="box1 box2">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/gas.svg"></div>
                <div class="text"><b>Gas</b></br><?= $gas_mprn; ?></div>
            </div> 
                <div class="greyLine"></div>
                <div class="boxText"><a href="<?= base_url() ?>index.php/portal/gas_reading">View/Update Reading</a></div>
            </div>
            <?php } ?>
            
        </div>
        
        <div class="dashBoardTableOuter">
        <div class="dashboardTable dashboardTable2">
            <div class="imageTop"> <img src="<?= base_url(); ?>img/billingIconHover.svg"> </div>
            <div class="tableHeading"><b>Profile</b></div>
            <div class="tableHeading" style="float: right;  margin-left:18px;margin-top: 15px;margin-right: 22px;"><b>Status</b> : <span style="color: green; font-weight: bold; font-size: 16px;"><?= $profile->Status->State; ?></span></div>
            
            <table cellpadding="1" cellspacing="1" width="100%" class="profileTable">
                <thead></thead>
                <tr>
                    <td width="30%"><b>Name</b></td>
                    <td width="70%"><?= ucfirst($profile->Name); ?></td>
                </tr>
                <tr>
                    <td width="30%"><b>Account Number</b></td>
                    <td width="70%"><?= ucfirst($profile->AccountNumber); ?></td>
                </tr>
                 <tr>
                    <td width="30%"><b>Address</b></td>
                    <td width="70%"><?= ucfirst($profile->Address); ?></td>
                </tr>
                 <tr>
                    <td width="30%"><b>Email</b></td>
                    <td width="70%"><?= $profile->Email; ?></td>
                </tr>
                <tr>
                    <td width="30%"><b>Telephone</b></td>
                    <td width="70%"><?= $profile->Telephone; ?></td>
                </tr>
                <tr>
                    <td width="30%"><b>Mobile</b></td>
                    <td width="70%"><?= $profile->Telephone; ?></td>
                </tr>
                <tr>
                    <td width="30%"><b>Payment Method</b></td>
                    <td width="70%"><?= ucfirst($profile->PaymentMethod->Name); ?></td>
                </tr>
           
            </table>
            </div>
        </div>
<?php require_once 'footer.php'; ?>
