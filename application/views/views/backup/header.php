<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eversmart</title>

<!-- Bootstrap -->
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet">
<link href="<?= base_url(); ?>css/styles.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<div id="body" style="display: none;" class="dashboard dashboard3">
	<div class="burgerMenu"><img src="<?= base_url(); ?>img/menu.png"></div>
	<div class="leftSide"> 
    <div class="cancelButton"><img src="<?= base_url(); ?>img/Cancel.png"></div>
    	<div class="logo">
        	<img src="<?= base_url(); ?>img/logo.png">
        </div>
        
        <div class="userImage">
        	<img src="<?= base_url(); ?>img/eversmart.png">
            <p><?php echo ( $this->session->userdata('account_name') )? $this->session->userdata('account_name') : 'Eversmart'; ?>  <!-- <img src="<?= base_url(); ?>img/downArrow.png"> -->
            </p>
           
        </div>
        
         <div class="dashboardList">
            <ul>
                <li>
                    <a href="<?= base_url(); ?>index.php/portal/account">
                    <img class="main" src="<?= base_url(); ?>img/overviewIcon.png">
                    <img class="hover" src="<?= base_url(); ?>img/overviewIconHover.png">
                     Overview</a>
                </li>
                <li>
                    <a href="<?= base_url(); ?>index.php/portal/elec_reading">
                    <img class="main" src="<?= base_url(); ?>img/energyUsageIcon.png"> 
                            <img class="hover" src="<?= base_url(); ?>img/energyUsageIconHover.png">
                            Electricity Reading</a>
                </li>
                <li>
                    <a href="<?= base_url(); ?>index.php/portal/gas_reading">
                    <img class="main" src="<?= base_url(); ?>img/readingIcon.png"> 
                            <img class="hover" src="<?= base_url(); ?>img/readingIconHover.png">
                            Gas Reading</a>
                </li>
                <li class="dropdownList2">
                    <a href="<?= base_url(); ?>index.php/portal/invoice">
                <div>
                    <img class="main" src="<?= base_url(); ?>img/billingIcon.png">
                    <img class="hover" src="<?= base_url(); ?>img/billingIconHover.png">
                     Invoice 
                     <!-- <img class="clickArrow3" src="<?= base_url(); ?>img/downArrow.png">
                    <img class="clickArrow4" src="<?= base_url(); ?>img/upArrow.png"> -->
                </div>
                     </a>
                 <!--  <ul class="listInner2">
                    <li><a href="<?= base_url(); ?>index.php/portal/elec_billing">Electricity</a></li>
                    <li><a href="<?= base_url(); ?>index.php/portal/gas_billing">Gas</a></li>
                   </ul> -->
                </li>
                <!-- <li class="dropdownList1">
                    <div>
                        <img class="main" src="<?= base_url(); ?>img/readingIcon.png">
                        <img class="hover" src="<?= base_url(); ?>img/readingIconHover.png">
                         Meter Reading
                        <img class="clickArrow1" src="<?= base_url(); ?>img/downArrow.png">
                        <img class="clickArrow2" src="<?= base_url(); ?>img/upArrow.png">
                     </div>
                     <ul class="listInner1">
                     	<li><a href="<?= base_url(); ?>index.php/portal/elec_reading">Electricity</a></li>
                        <li><a href="<?= base_url(); ?>index.php/portal/gas_reading">Gas</a></li>
                     </ul>
                </li> -->
                <!-- <li >
                    <a href="#">
                    <img class="main" src="<?= base_url(); ?>img/paymentIcon.png">
                    <img class="hover" src="<?= base_url(); ?>img/paymentIconHover.png"> Payment</a>
                </li>
                <li class="dropdownList3">
                    <div>
                        <img class="main" src="<?= base_url(); ?>img/energyUsageIcon.png"> 
                        <img class="hover" src="<?= base_url(); ?>img/energyUsageIconHover.png">
                        Energy usage 
                    </div>
                </li>
                <li>
                        <img class="main" src="<?= base_url(); ?>img/checkTarriffIcon.png">
                        <img class="hover" src="<?= base_url(); ?>img/checkTarriffIconHover.png">
                        Check Tariff
                </li>
                <li>
                    	<img class="main" src="<?= base_url(); ?>img/homeIcon.png">
                        <img class="hover" src="<?= base_url(); ?>img/homeIconHover.png">
                         Moving Home
                </li> -->
            </ul>
        </div>
        
    </div>

<div class="rightSide">


<div class="dashboardNav">
    <ul>
        <li><a href="<?= base_url(); ?>index.php/portal/user_logout"> Logout <img title="Logout" alt="Logout" style="width: 25px;" src="<?= base_url(); ?>img/logout.png"></a></li>
    </ul>
</div>