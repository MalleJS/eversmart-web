<?php require_once 'header.php'; ?>
<style type="text/css">
   .col-sm-12 .meterButton { width: 100%; }
</style>
        
        <div class="box">
            <div class="box1">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                <div class="text"><b>Electricity</b></br> <?= $this->session->userdata('elec_mpan'); ?></div>
            </div>  
                <div class="greyLine"></div>
                <div class="boxText" style="visibility: hidden;"><a href="#">View Statement</a></div>
            </div>
            
            
            <!-- <div class="box1 box2">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/gasIcon.png"></div>
                <div class="text"><b>Gas</b></br><?= $this->session->userdata('gas_mprn'); ?></div>
            </div> 
                <div class="greyLine"></div>
                <div class="boxText"><a href="#">View Statement</a></div>
            </div> -->
            
        </div>
        
        <div class="dashBoardTableOuter">
            <div class="dashboardTable dashboardTable2">
                <div class="boxTimer"> 
                    <div class="boxIcon bul-icon"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                    <div class="timer">
                        <ul>
                        <?php
                        $today_reading = str_split($reading_today);
                        for( $i=0; $i<count($today_reading); $i++ )
                        {
                        	echo '<li>'.$today_reading[$i].'</li>';
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="reading"><img src="<?= base_url(); ?>img/readingIcon2.png">Last Reading : <?= $reading_previous; ?></div>
                 </div>
                 <div class="meterHeading">Electricity Meter Reading</div>
                 
                 <div class="meterBottom">
                 	<!-- <div class="meterDate"><img src="<?= base_url(); ?>img/dateIcon.png">Sent on : 5th May 2017</div> -->
                    <div class="meterButton" data-toggle="modal" data-target="#myModal">Update Meter Reading</div>
                 </div>
             </div>
        </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
      <div class="modal-body">
        <div class="dashBoardTableOuter">
            <div class="dashboardTable dashboardTable2">
            <form id="elect_reading_submit" method="post">
                <div class="boxTimer"> 
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <div class="boxIcon"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                    <div class="timer">
                        <input type="text" name="elec_reading" class="form-control">
                    </div>
                 </div>
                 <div class="meterHeading">Electricity Meter Reading</div>
                 <div id="meterHeading"></div>
                 <div class="meterBottom">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="meterButton" data-dismiss="modal" >Cancel</div>                    
                        </div>
                        <div class="col-sm-6">
                            <div id="send_value_id" class="meterButton">Update</div>
                        </div>
                        <input type="submit" name="send_value" value="send_value" style="visibility: hidden;">
                    </div>
                    
                 </div>
                 </form>
             </div>
        </div>
    </div>

  </div>
</div>
<?php require_once 'footer.php'; ?>