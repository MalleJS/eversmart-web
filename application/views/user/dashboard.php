<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Overview</title>
    <!-- Font material icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">

    <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url(); ?>dashboard/css/dashboard.css" rel="stylesheet">
    <!--- Font Family Add---------------->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>

    <script>
        window.intercomSettings = {
        app_id: "qjs70gbf",
        custom_launcher_selector: ".open_intercom"
        <?php if(isset($this->session->userdata('login_data')['intercom_user_id'])){ ?>
            , user_id: "<?=$this->session->userdata('login_data')['intercom_user_id'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_id'])){ ?>
            , id: "<?=$this->session->userdata('login_data')['intercom_id'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_name'])){ ?>
            , name: "<?=$this->session->userdata('login_data')['intercom_name'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_email'])){ ?>
            , email: "<?=$this->session->userdata('login_data')['intercom_email'];?>"
        <?php } ?>
        };
    </script>

        <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qjs70gbf';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

    <script>(function () {
            var w = window;
            var ic = w.Intercom;
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', intercomSettings);
            } else {
                var d = document;
                var i = function () {
                    i.c(arguments)
                };
                i.q = [];
                i.c = function (args) {
                    i.q.push(args)
                };
                w.Intercom = i;

                function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/qjs70gbf';
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }

                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })()
    </script>

    <style>


        .loading-page {
            margin: 20% auto;
            width: 100px
        }

        .loader {
            border: 8px solid #fda1b8;
            border-radius: 50%;
            border-top: 8px solid #f64d76;
            width: 90px;
            height: 90px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg" id="nav-box-navi">

    <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<!-- Start your project here-->
<div class="loading-page">
    <div class="loader"></div>
</div>

<div class="cd-panel cd-panel--from-left js-cd-panel-left">
    <header class="cd-panel__header">
    <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
        <a href="#0" class="cd-panel__close js-cd-close"></a>
    </header>

    <div class="cd-panel__container">
        <div class="cd-panel__content">

            <nav id="menuslideoutjs" class="menu slideout-menu">
                <section class="menu-section">
                    <ul class="menu-section-list">
                        <?php if( $this->session->userdata('login_data')['signup_type'] != 3 ) { // Only show these if you are not a Dyball customer ?>
                            <li><a href="<?php echo base_url(); ?>index.php/user/dashboard">Overview</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/user/referral">Refer a friend</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/user/billing">Billing</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/user/meter_reading">Meter Reading</a></li>
                            <!--<li><a href="<?php// echo base_url(); ?>index.php/user/usage">Usage</a></li>-->
                            <li><a href="<?php echo base_url(); ?>index.php/user/referral">Messages</a></li>
                            <!--<li><a href="<?php// echo base_url(); ?>index.php/user/moving_home">Moving Home</a></li>-->
                        <?php } else { ?>
                            <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                        <?php } ?>
                    </ul>
                </section>
            </nav>


        </div> <!-- cd-panel__content -->
    </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->


<div class="cd-panel cd-panel--from-right js-cd-panel-main">
    <header class="cd-panel__header">
    <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
        <a href="#0" class="cd-panel__close js-cd-close"> </a>
    </header>

    <div class="cd-panel__container">
        <div class="cd-panel__content">
            <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
                <section class="menu-section">
                    <ul class="menu-section-list">
                        <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                            <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & faq</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
                        <li><a id="logout_user" onclick="logout_user()" href="#">logout</a></li>
                    </ul>
                </section>
            </nav>
        </div> <!-- cd-panel__content -->
    </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->
<div class="cd-main-content">
    <div class="main-inner" id="main_content" style="display:none">

        <div class="main-rounded-red-header">
            <div class="red-rounded-waves" id="dashboard_main_bg">
                <div class="topbar" id="red_top">

                    <div class="container">
                        <div class="row">
			
                            <span class="col-sm-12 col-md-4 col-lg-6 px-0">
                            <a  href="<?php echo base_url(); ?>">
                                <img class="logo_dash" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/>
                            </a>
                            </span>

                            <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile"id="mobile_menu" style="position: relative; float:left; padding-top:10px"></div>


                            <div class="mobileonly usermobilemenu toggle-button-menu-right cd-btn js-cd-panel-trigger"
                                 data-panel="main"><i class="fa fa-user" aria-hidden="true"></i></div>
                            <div class="mobileonly dashbaordmobilemenu">
                                <button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="left"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> 
                                </button>
                            </div>

                            <div class=" col-sm-12 col-md-4 col-lg-2 text-right hideonmobile"
                                 id="navbarSupportedContent-3-main">


                                <a class="nav-link dropdown-toggle waves-effect waves-light"
                                   id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="true">
                                    <?php
                                    if ($this->session->has_userdata('login_data')) {
                                        echo ucfirst($this->session->userdata('login_data')['title']) . '. ' . ucfirst($this->session->userdata('login_data')['last_name']);
                                    } else {
                                        echo '-';
                                    }
                                    ?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2">
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/user/account"><i class="fa fa-info righ-padd" aria-hidden="true"></i>my account</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Helpfaqs"><i class="fa fa-question righ-padd" aria-hidden="true"></i>help & faqs</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/contact_us"><i class="fa fa-address-book righ-padd" aria-hidden="true"></i>contact us</a>
                                    <a id="logout_user" onclick="logout_user()" class="dropdown-item waves-effect waves-light" href="#"><i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout</a>
                                </div>

                            </div>


                        </div>
                    </div>
                    <!--- End container-------->
                    <!--- End container-------->

                </div>
                <!---end top bar bar---------->

                <!------------------End Container----------------->

            </div><!------------------End Postcode------------->

            </section>
            <!----------------start postcode--------------------->


            <!----------------End postcode--------------------->
        </div>
    </div>

    <!--------------------Start breadcrumbs Care Section------------>
    <section class="termandconditions">
        <div class="container-fluid">
            <div class="row amend">
                <div class="welcome-ptl-container"><h1 class="welcome-ptl">Hi <?=trim(ucfirst($this->session->userdata('login_data')['first_name']))?>, welcome to your online portal! </h1></div>
                <div class="col-sm-12 col-md-12 col-lg-2 desk-menu-dashboard" style="top:149px">
                    <div class="left-nav-fix ">
                        <?php $this->load->view('dashboard/menu'); ?>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-8 dashboard-panel-right">
                    
                    <!---<div class="alert alert-warning">
                         <strong class="dash-s">Notification:</strong> Your account is not activated with Junifer. Please activate your account to see your bills. You should have an activation email, if not, <a href="#">click here</a> to resend activation email.
                       </div>-->
                    <div class="large-screen-fix dashboardss">
                        <div class="row">

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/billing"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Billing</span></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><p class="dash-p">Last Bill:<br><strong class="dash-s">
                                                    <?php
                                                    if(!empty($results)) {
                                                        $last_bill = end($results);
                                                        $last_bill_date = $last_bill['createdDttm'];
                                                        echo date('Y-m-d', strtotime($last_bill_date));
                                                    } else { ?> N/A <?php } ?>
                                                    </strong></p>
                                                </div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/billing.svg" alt=""></span></div>
                                                <div class="col-sm-4"><p class="dash-p">Next payment due:<br><strong class="dash-s"> N/A</strong></p></div>
                                            </div>
                                        </div>

                                    </div>
                                </a>

                            </div>

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/payments"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Payment</span></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><p class="dash-p">Current Balance:<br> <?php
                                                        if (!empty($balance)) {
                                                            if ($balance >= 0) {
                                                                echo "<strong class='dash-s'>-£".number_format($balance,2)."</strong>";
                                                            } else {
                                                                $balance = str_ireplace('-', '', $balance);
                                                                echo "<strong class='dash-s'>£".number_format($balance,2)."</strong>";
                                                            }
                                                        } else {
                                                            echo '<strong class="dash-s">N/A</strong>';
                                                        }

                                                        ?>
                                                    </p></div>
                                                <div class="col-sm-4">
                                                    <span class="switch_icon_dashboard">
                                                    <span class="switch_icon_dashboard">
                                                    <img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/payments.svg" alt=""></span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <p class="dash-p">Next payment due:<br><strong class="dash-s"> N/A
                                                            <?php /*          */ ?></strong></p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/meter_reading"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Meter Reading</span>
                                                </div>
                                                <div class="col-sm-4">

                                                    <p class="dash-p">
                                                        <?php
                                                        if (!empty($last_reading)) {
                                                            echo "Last Elec Reading: <br>";
                                                            echo '<strong class="dash-s">' . json_encode($last_reading['cumulative']) . '</strong>';
                                                        } else {
                                                            echo "Last Elec Reading: <br>";
                                                            echo '<strong class="dash-s">N/A</strong>';

                                                            ?>
                                                            <br>
                                                            <?php
                                                        } ?>
                                                    </p>

                                                </div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img
                                                                src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/meterreading.svg"
                                                                alt=""></span></div>
                                                <div class="col-sm-4">

                                                    <p class="dash-p">
                                                        <?php
                                                        if (!empty($last_reading_gas)) {
                                                            echo "Last Gas Reading: <br>";
                                                            echo '<strong class="dash-s">' . json_encode($last_reading_gas['cumulative']) . '</strong>';
                                                        } else {
                                                            echo "Last Gas Reading: <br>";
                                                            echo '<strong class="dash-s">N/A</strong>';
                                                            ?>
                                                            <br>
                                                            <?php
                                                        }
                                                        ?>
                                                    </p>


                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/referral"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12"><span
                                                            class="dash-over-text">Friend Referral</span></div>
                                                <div class="col-sm-4"><p class="dash-p">Friends Referred:
                                                        <?php
                                                        if (!isset($ref) || $ref == 0) {
                                                            echo '<strong class="dash-s">0</strong>';
                                                        } else {
                                                            echo '<br><strong class="dash-s">' . $ref . '</strong>';
                                                        }
                                                        ?>
                                                    </p></div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/refer.svg" alt=""></span></div>
                                                <div class="col-sm-4">
                                                    <p class="dash-p">Credit Accrued: 
                                                        <?php
                                                        if (!isset($format_bal) || $format_bal == 0) {
                                                            echo '<br><strong class="dash-s">£0</strong>';
                                                        } else {
                                                            echo '<br><strong class="dash-s">£' . $format_bal . '</strong>';
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6" >
                                <a href="javascript:void(0)" class="gas_card dash " > <?php // echo base_url() index.php/user/usage ?>

                                    <div class="gas_box_icon gap_icon_dashboard" style="opacity: 0.4">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Energy Usage - Coming Soon!</span>
                                                </div>
                                                <div class="col-sm-4">

                                                    <p class="dash-p">Elec:
                                                        <?php

                                                        if ($daily_use_elec_total == 0) {
                                                            echo '<strong class="dash-s">0 p/kWh</strong>';
                                                        } else {
                                                            echo '<strong class="dash-s">' . $daily_use_elec_total . ' p/kWh</strong>';
                                                        }

                                                        ?>
                                                    </p>

                                                </div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/usage.svg" alt=""></span></div>
                                                <div class="col-sm-4">
                                                    <p class="dash-p">Gas:
                                                        <?php if ($daily_use_gas_total == 0) {
                                                            echo '<strong class="dash-s">0 p/kWh</strong>';
                                                        } else {
                                                            echo '<strong class="dash-s">' . $daily_use_gas_total . ' p/kWh</strong>';
                                                        }


                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6">
                            <a href="<?php echo base_url() ?>index.php/user/messages"
                               class="gas_card dash dash-hover">
                                <div class="gas_box_icon gap_icon_dashboard">


                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12"><span class="dash-over-text">Messages</span>
                                            </div>
                                            <div class="col-sm-4"><p class="dash-p">
                                                    Total Messages<br>
                                                    <strong class="dash-s">
                                                        <?php
                                                        if (empty($junifer_msgs)) {
                                                            echo 'N/A';
                                                        } else {
                                                            echo count($junifer_msgs['results']);
                                                        }
                                                        ?></strong>
                                                </p></div>
                                            <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>dashboard/images/dashboard/Overview/messages.svg" alt=""></span></div>
                                            <div class="col-sm-4">
                                                <p class="dash-p">Last Message<br>
                                                    <strong class="dash-s dash-msg">
                                                        <?php
                                                        if (empty($junifer_msgs)) {
                                                            echo 'N/A';
                                                        } else {
                                                            $lastrecord = end($junifer_msgs['results']); //points to last index of array
                                                            print_r($lastrecord['type']);
                                                        }
                                                        ?>
                                                    </strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>

                        </div>
                    </div>

                </div>


            </div>


        </div>


        <?php $this->load->view('dashboard/faq_slider'); ?>

    </section>

    <!--------------------End breadcrumbs Care Section------------->


    <!----------------Start Footer-------------------------->

    <?php
    $this->load->view('dashboard/dashfooter');
    ?>
    <!----end footer------------>

    </div>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/mdb.min.js"></script>
    <!-- common JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/common.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#slideOut").toggleClass('showSlideOut');
            $('.slideOutTab').toggle("fast");


            var current = location.href;
            if (current.match(/dashboard/g) == 'dashboard') {
                $('.dashboard').attr('class', 'dashboard actiove-link');
            }

            this.$slideOut = $('#slideOut');

            // Slideout show
            this.$slideOut.find('.slideOutTab').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });

            this.$slideOut.find('#closy').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });
        });
    </script>
    </div>
</body>
</html>
