<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Login</title>
   <!-- Font material icon -->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
     <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
  <!--- Font Family Add----------------->
  <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->
</head>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

  <!-- Start your project here-->
  <div class="cd-main-content" >
    <div class="red-rounded-wave">
            <!----------------start postcode-------------------->
    <section class="postcode_top mt0 ">
        <div class="" id="login-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mx-auto mtop60 posi-rele">
                        <div class="col-md-12 mb-4 pb-5">
                            <div class="card">
                                <div class="card-body">
                                    <span class="loginheading">Login</span>

                                    <!--Body-->
                                    <form id="user_login_form">
                                        <div class="login-form">
                                            <div class="md-form">
                                                <?php
                                                    if( !empty( $this->input->get('t') ) ){
                                                        echo "<input type='hidden' value='".$this->input->get('t')."' name='token'>";
                                                    }
                                                ?>
                                                <input id="defaultForm-email" name="user_email" required="" class="form-control" type="email" placeholder="Email">
                                            </div>

                                            <div class="md-form">
                                                <input id="defaultForm-pass" class="form-control" type="password" required="" name="user_password" placeholder="Password">
                                                <span class="forgot-password"><a href="<?php echo base_url(); ?>index.php/Reset_one/resetone">Forgot Password?</a></span>
                                            </div>
                                            <div id="error_msg"></div>
                                            <div class="text-center">
                                                <button id="red-btn-default" class="btn btn-default waves-effect waves-light red-btn rounded-login-btn" type="submit">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('dashboard/faq_slider'); ?>
    </section>
<!----------------End postcode--------------------->

    </div>
</div>

<!--------------------End breadcrumbs Care Section------------->
<script type="text/javascript">
    $(document).ready(function(){
        $('#user_login_form').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url() ?>index.php/user/check_login/",
                type: 'post',
                dataType: 'json',
                data: $('#user_login_form').serialize(),
                beforeSend:function(){
                    // $('.loading-page').fadeIn();
                    // $('#main_content').fadeOut();
                    //  $('#error_msg').html('<div class="loading-page"><div class="loader"></div></div>');
                },
                success:function(response){
                    $('#error_msg').empty();

                    if( response.error =='0' ){

                        switch (response.msg) {

                            case 'admin_success' || 'cs_success':
                                destination = 'admin/admin_customer_signup';
                                break;

                            case 'tm_success':
                                destination = 'tariff_management/';
                                break;

                            case 'sf_success':
                                destination = 'admin/webtocase';
                                break;

                            case 'dyball_success':
                                destination = 'user/account';
                                break;

                            case 'finance_success':
                                destination = 'admin/admin_sassquatch_update';
                                break;

                            default:
                                destination = 'user/dashboard';
                                break;
                        }

                        $('#error_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Logged In. Please Wait.</div>');
                        setTimeout(function () { window.location.href = '<?php echo base_url() ?>'+destination }, 1500);

                    }
                    if (response.error == '1') {
                        $('#error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Awaiting account activation.</div>');
                    }
                    if (response.error == '2') {

                        $.ajax({
                            url: '<?php echo base_url() ?>customers/index.php/portal/login_check',
                            type: 'post',
                            data: $('#user_login_form').serialize(),
                            beforeSend: function( xhr ) {
                                $('#error_msg').html('<div class="alert alert-warning"> Please Wait.</div>');
                            },
                            success: function(response) {
                                if( response == 'done' ) {
                                    $('#error_msg').html('<div class="alert alert-success"><strong>Login Successful!</strong> Please Wait.</div>');
                                    setTimeout(function(){
                                        window.location.href = '<?php echo base_url() ?>customers/index.php/portal/account';
                                    }, 1000)
                                } else {
                                    $('#error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong> Wrong Email / Password</div>');
                                }
                            }
                        });

                    }
                    if ( response.error == '4' ) {
                        $('#error_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Logged In. Please Wait.</div>');
                        setTimeout(function(){ window.location.href="<?php echo base_url() ?>index.php/user/account_status"; },1500);
                    }
                    if ( response.error == '5' ) {
                        $('#error_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Logged In. Please Wait.</div>');
                        setTimeout(function(){ window.location.href="<?php echo base_url() ?>index.php/examples/meter_bookings"; },1500);
                    }
                }
            })
        });

        if ($(window).width() >= 1730) {
            $("#slideOut").toggleClass('showSlideOut');
            $('.slideOutTab').toggle("fast");
        }

        var current = location.href;
        if (current.match(/dashboard/g) == 'dashboard') {
            $('.dashboard').attr('class', 'dashboard actiove-link');
        }

        this.$slideOut = $('#slideOut');

        // Slideout show
        this.$slideOut.find('.slideOutTab').on('click', function () {
            $("#slideOut").toggleClass('showSlideOut');
            $('.slideOutTab').toggle("fast");
        });

        this.$slideOut.find('#closy').on('click', function () {
            $("#slideOut").toggleClass('showSlideOut');
            $('.slideOutTab').toggle("fast");
        });
        
    });
</script>

<!----------------Start Footer-------------------------->

<?php $this->load->view('layout/common_footer'); ?>
