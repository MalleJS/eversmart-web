<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Error 404 - Something went wrong</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/responsive.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="/assets/img/favicon.png">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T44QPZV');</script>
    <!-- End Google Tag Manager -->

</head>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="col-lg-12">
        <div class="title-404" >
            <img src="/assets/images/eversmart.png" alt="">
        </div>
    </div>

    <div class="col-lg-12">
        <div class="container">
            <div class="mydiv"></div>
            <div class="row">
                <div class="col-md-4">
                    <div class="error-bulb">
                        <img src="/assets/images/bulb.png" alt="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="error-404">
                        <span class="error-head">404</span><br>
                        <span class="error-sub">The bulb has blown!</span><br>
                        <span class="error-return">Return to home page</span><br>
                        <span class="error-link"><a href="https://www.eversmartenergy.co.uk/">https://www.eversmartenergy.co.uk</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>