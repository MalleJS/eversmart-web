<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>
	<?php else : ?>
	<!--header class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'twentyseventeen' ); ?></h2>
	</header-->
	<?php endif; ?>
	
	<h1 class="homepage">Welcome to the Eversmart Blog!</h1>
	<p class="homepage">Useful tips, industry talk and company news from the team at Eversmart&nbsp;Energy.</p>
	
	<div style="text-align:center">
	<?php the_widget( 'WP_Widget_Search' ); ?>
	</div>

	<div id="primary-wide" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
				) );

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>
			
			<div class="clear"></div>
			
			<div class="about-wrapper">
			<div class="about grid66 tablet100 mobile100">
			<h2 class="homepage">About Eversmart Energy</h2>
			<p>Hi. We’re Eversmart and this is our blog! We're a UK energy supplier with a simple goal - to repair the broken relationship between people and their energy company. We also happen to be one the <a href="https://www.eversmartenergy.co.uk/">cheapest energy suppliers</a> in the country!</p>
<p>Take a look around to find out what we’ve been up to here at Eversmart HQ. This is also where we share energy saving tips, industry news and advice on how to manage your account. You can find out more about us on our homepage - <a href="https://www.eversmartenergy.co.uk/">eversmartenergy.co.uk</a>.</p>
			</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
