var base_url = 'https://www.eversmartenergy.co.uk/customers/';

$(document).ready(function()
{


$(window).on('load', function() {
        // Animate loader off screen
       $("#loader-wrapper").fadeOut(2000);
        $("#body").fadeIn(2000);
    });
	
	var height=$( window ).height();
	//$(".loginOuter").css('height',height);
	
	if($(window).height()<800){
		$('.footer').addClass("footermobile");
	}else{
		$('.footer').removeClass("footermobile");
	}
	
	$(window).resize(function(){
			
			if($(window).height()<800){
				$('.footer').addClass("footermobile");
			}else{
				$('.footer').removeClass("footermobile");
			}
    });
	
	
	if($(window).height()<800){
		$('.footer').addClass("lg-foot");
	}else{
		$('.footer').removeClass("lg-foot");
	}
	
	$(window).resize(function(){
			
			if($(window).height()<800){
				$('.footer').addClass("footermobile");
			}else{
				$('.footer').removeClass("footermobile");
			}
    });


/* change password */
	
	$('#change_password').submit(function(e)
	{
		e.preventDefault();
		//alert();
		$.ajax({
				url: base_url+'index.php/portal/reset_password',
				type: 'post',
				data: $('#change_password').serialize(),
				success: function(response)
				{
					alert(response);
				}

			});

	});

	$('#selectAccount').change(function()
	{
			//alert($(this).val() );
			if( $(this).val() == '0' )
			{
				alert( 'Please select Account' );
			}
			else
			{
				$.ajax({
					url: base_url+'index.php/portal/session_account',
					type: 'get',
					data: { account_no: $(this).val() },
					success: function(response)
					{
						//alert(response);
						if( response == 'done' )
						{
							window.location.href = base_url+'index.php/portal/account';
						}
					}
				})
			}
	}); 


	$('#login_portal').submit(function(e)
	{
		e.preventDefault();
		$.ajax(
			{
				url: base_url+'index.php/portal/login_check',
				type: 'post',
				data: $('#login_portal').serialize(),
				beforeSend: function( xhr ) {
					    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
					    $('#error_div').html('<div class="alert alert-warning"> Please Wait.</div>');
					  },
				success: function(response)
				{
					if( response == 'done' )
					{
						$('#error_div').html('<div class="alert alert-success"><strong>Login Successful!</strong> Please Wait.</div>');
						setTimeout(function(){
							window.location.href = base_url+'index.php/portal/account';
						}, 1000)
					}
					else
					{
						$('#error_div').html('<div class="alert alert-danger">'+response+'</div>');
					}
				}
			});
	});


	$('#send_value_id').click(function(){
		$('#elect_reading_submit').trigger('submit');
	});

	$('#elect_reading_submit').validate({
		rules: 
		{
			elec_reading:
			{
				required: true,
				number: true,
				maxlength: 7
			}
		},
		submitHandler: function(form)
		{
			$.ajax({
				url: base_url+'index.php/portal/send_elec_reading',
				type: 'post',
				dataType: 'json',	
				data: $('#elect_reading_submit').serialize(),
				beforeSend: function(){
					$('#changeDiv').html('<div class="meterButton">Please Wait.. <img src="'+base_url+'img/input-spinner.gif" width="30px" /></div>');
				},
				success:function(response)
				{
					$('#changeDiv').html('<div id="send_value_id" class="meterButton">Update</div>');
					console.log(response.Message);
					if( response.Message == 'SUCCESS' )
					{
						$('#meterHeading').html('<b><span style="color:green">Meter Reading Updated</span></b>');
						setTimeout(function()
						{
								window.location.href='';
						},1000)
					}
					else
					{
						$('#meterHeading').html('<b><span style="color:red">'+response+'</span></b>');
					}
				}
			});
		}
	});


	$('#send_value_id_gas').click(function(){
		$('#gas_reading_submit').trigger('submit');
	});

	$('#gas_reading_submit').validate({
		rules: 
		{
			gas_reading:
			{
				required: true,
				number: true,
				maxlength: 7
			}
		},
		submitHandler: function(form)
		{
			$.ajax({
				url: base_url+'index.php/portal/send_gas_reading',
				type: 'post',
				dataType: 'json',
				data: $('#gas_reading_submit').serialize(),
				beforeSend: function(){
					$('#changeDiv').html('<div class="meterButton">Please Wait.. <img src="'+base_url+'img/input-spinner.gif" width="30px" /></div>');
				},
				success:function(response)
				{
					console.log(response.Message);
					$('#changeDiv').html('<div id="send_value_id" class="meterButton">Update</div>');
					if( response.Message == 'SUCCESS' )
					{
						$('#meterHeading').html('<b><span style="color:green">Meter Reading Updated</span></b>');
						setTimeout(function()
						{
								window.location.href='';
						},1000)
					}
					else
					{
						$('#meterHeading').html('<b><span style="color:red">'+response.Message+'</span></b>');
					}
				}
			});
		}
	});

$(function(){
    var current = location.pathname;
    $('.dashboardList li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href').indexOf(current) !== -1){
            $this.addClass('active22');
        }
		$('.active22 .hover').css('display', 'inline-block');
		$('.active22 .main').css('display', 'none');
    })
})
	
//$('.downloadPDF').click(function()
//{
//	$(this).data('invoiceID');
//})


});
/* end document ready */