<?php require_once 'header.php';
   // debug($profile,1);
 ?>

 <div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 main-content">
            <div class="gas_card">                                
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>images/dashboard/Overview/meterreading.svg" alt=""></span>

                    <?php
                        if( (
                            isset($session_data['elec_meter_serial']) && $session_data['elec_meter_serial'] != '' &&
                            preg_match('/^[1][3-8]P/',$session_data['elec_meter_serial']) == 1
                        ) || (
                            isset($session_data['gas_meter_serial']) && $session_data['gas_meter_serial'] != '' &&
                            preg_match('/^G4P/',$session_data['gas_meter_serial']) == 1
                        ) ) { // If you have a Secure smart meter
                    ?>
                        <span class="dash-over-text title">Smart Meter Readings</span>
                        <span class="dash-over-text account">We take automatic readings from your secure smart meter.</span>
                    <?php } else { ?>
                        <span class="dash-over-text title">Meter Reading</span>
                        <span class="dash-over-text account">Have a smart meter? Still getting asked for a manual reading? Our <a target="_blank" href="https://www.eversmartenergy.co.uk/blog/i-have-a-smart-meter-why-am-i-still-being-asked-for-a-meter-reading/">blog post</a> explains why...</span>
                    <?php } ?>
                </div>
            </div>
                    

                    <?php if (( $session_data['user_mprn'] === 'na' ) || ( $session_data['mpancore'] === 'na' )) {?>
                    <style>
                        .col-md-6.mobileres {
                            max-width: 100%!important;
                        }
                    </style>
                    <?php } ?>


                    <?php if( $session_data['mpancore'] != 'na' &&  $session_data['meterTypeElec'] != 'E7' ) { ?>

                        <div class="col-md-6 mobileres floatL">

                            <div id="select_elec" class="gas_card white-blue" style="height:769px; position:relative;">

                                <div class="gas_box_icon gap_icon_dashboard">

                                    <span class="switch_icon_dashboard"><img id="elec_icon" src="<?php echo base_url() ?>images/dashboard/Reading/Elec/elec.svg" alt=""></span>
                                    <span class="dash-over-text-blue">Electricity</span>
                                    <span class="dash-over-read">MPAN: <?php if($session_data['mpancore'] !=null) { echo $session_data['mpancore']; }else{ echo '-'; } ?></span>

                                    <form id="submit_reading_meter">

                                        <input type="hidden" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>" name="signupType" id="signupType">

                                        <div id="main_electricity_card">

                                            <div class="blue-reading-box ">

                                                <div class="blue-reading-box-inner text-center" id="elec_field_wrapper">

                                                    <span class="w-elec-icon text-center"></span>
                                                    <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $session_data['elec_meter_serial'] !=null ) { echo $session_data['elec_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                                    <ul class="reading-input-class">

                                                        <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst" ></li>

                                                        <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond" ></li>

                                                        <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird" ></li>

                                                        <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth" ></li>

                                                        <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive" ></li>

                                                        <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth" ></li>

                                                        <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh" ></li>

                                                        <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight" ></li>

                                                    </ul>

                                                    <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds" id="elec_submit_reds">

                                                    <input type="hidden" name="meterIdentifier" value="<?php if( !empty($last_reading['meter']) ) { echo $last_reading['meter']; } ?>">

                                                    <span class="meter-serial-txt text-center grey-meter">
                                                        <img src="<?php echo base_url() ?>images/dashboard/Reading/Elec/timer.svg" alt="">
                                                        Last Reading: <?php if( !empty($last_reading['cumulative']) ) { echo $last_reading['cumulative']; }else{ echo 'N/A'; } ?>
                                                    </span>

                                                </div>

                                            </div>

                                            <input type="hidden" name="elec_signup_type" value="<?php echo $session_data['signup_type'] ?>">

                                            <div id="elec_button">
                                                <input class="reading-submit" id="submit_meter" type="submit" value="Submit"/>
                                            </div>
                                            <br><br>
                                            <a style="position: absolute;  left: 19%; bottom: 40px;" href="#" class="meter-help-lnk" onclick="meter_reading_info_layer('elec'); return false;">Having trouble reading your electricity meter?</a>

                                            <div id="error_msg_elec"></div>
                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>
                
                    <?php }

                    if( $session_data['user_mprn'] != 'na') { ?>

                        <div class="col-md-6 mobileres floatL">

                            <div style="display:block" id="gas_card_selected" class="gas_card white-border">

                                <div class="gas_box_icon gap_icon_dashboard">

                                    <span class="switch_icon_dashboard"><img id="gas_icon" src="<?php echo base_url() ?>images/dashboard/Reading/gas/gas.svg" alt=""></span>
                                    <span class="dash-over-text-blue">Gas</span>
                                    <span class="dash-over-read">MPRN: <?php if( $session_data['user_mprn'] !=null ) { echo $session_data['user_mprn']; }else{ echo '-'; } ?></span>

                                    <form id="submit_reading_meter_gas">

                                        <div class="blue-reading-box ">

                                            <div class="blue-reading-box-inner text-center" id="gas_field_wrapper" style="background-color: #CCC;">

                                                <span class="w-elec-icon text-center"></span>
                                                <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $session_data['gas_meter_serial'] !=null ) { echo $session_data['gas_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit0" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_First"></li>

                                                    <li><input type="text" name="gas_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Second"></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit0" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Third"></li>

                                                    <li><input type="text" name="gas_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Fourth"></li>

                                                    <li><input type="text" name="gas_meter_submit2" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Five"></li>

                                                    <li><input type="text" name="gas_meter_submit3" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Sixth"></li>

                                                    <li><input type="text" name="gas_meter_submit4" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Seventh"></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit5" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Eight" ></li>

                                                </ul>

                                                <input style="display:none"  class="form-control" maxlength="6" placeholder="gas_submit_reds" type="text" name="gas_submit_reds" id="gas_submit_reds">

                                                <input type="hidden" id="gas_signup_type" name="gas_signup_type" value="<?php echo $session_data['signup_type'] ?>">

                                                <span class="meter-serial-txt text-center grey-meter">
                                                    <img src="<?php echo base_url() ?>images/dashboard/Reading/Elec/timer.svg" alt="">
                                                    Last Reading: <?php if( !empty($last_reading_gas['cumulative']) ) { echo $last_reading_gas['cumulative']; }else{ echo 'N/A'; } ?>
                                                </span>

                                            </div>

                                        </div>

                                        <div id="gas_button" style="visibility: hidden">
                                            <input class="reading-submit gold_bg" id="submit_reading_meter_gas" type="submit" value="Submit" style="margin-top: 130px;"/>
                                        </div>
                                        <br><br>
                                        <a href="#" class="meter-help-lnk" onclick="meter_reading_info_layer('gas'); return false;">Having trouble reading your gas meter?</a>
                                        <div id="error_msg_gas"></div>
                                    </form>

                                </div>

                            </div>

                        </div>

                    <?php } ?>



                    <?php if( $session_data['mpancore'] != 'na' &&  $session_data['meterTypeElec'] === 'E7' ) { ?>

                    <div class="col-md-6 mobileres floatL">

                        <div id="select_elec" class="gas_card white-blue">

                            <div class="gas_box_icon gap_icon_dashboard">

                                <span class="switch_icon_dashboard"><img id="elec_icon" src="<?php echo base_url() ?>images/dashboard/Reading/Elec/elec.svg" alt=""></span>
                                <span class="dash-over-text-blue">Electricity</span>
                                <span class="dash-over-read">MPAN: <?php if($session_data['mpancore'] !=null) { echo $session_data['mpancore']; }else{ echo '-'; } ?></span>

                                <form id="submit_reading_meter">

                                    <input type="hidden" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>" name="signupType" id="signupType">

                                    <div id="main_electricity_card">

                                        <div class="blue-reading-box ">

                                            <div class="blue-reading-box-inner text-center" id="elec_field_wrapper">

                                                <span class="w-elec-icon text-center"></span>
                                                <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $session_data['elec_meter_serial'] !=null ) { echo $session_data['elec_meter_serial']; }else{ echo 'N/A'; } ?></span>
                                                <div class="daynight">Day</div>
                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond" ></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh" ></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight" ></li>

                                                </ul>
                                                <div class="daynight">Night</div>
                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond_two" ></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh_two" ></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight_two" ></li>

                                                </ul>

                                                <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds" id="elec_submit_reds">

                                                <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds_two" id="elec_submit_reds_two">

                                                <input type="hidden" name="meterIdentifier" value="<?php if( !empty($last_reading['meter']) ) { echo $last_reading['meter']; } ?>">

                                                <span class="meter-serial-txt text-center grey-meter">
                                                    <img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/timer.svg" alt="">
                                                    Last Reading: <?php if( !empty($last_reading['cumulative']) ) { echo $last_reading['cumulative']; }else{ echo 'N/A'; } ?>
                                                </span>

                                            </div>

                                        </div>

                                        <input type="hidden" name="elec_signup_type" value="<?php echo $session_data['signup_type'] ?>">

                                        <div id="elec_button">
                                            <input class="reading-submit" id="submit_meter" type="submit" value="Submit"/>
                                        </div>
                                        <br><br>
                                        <a href="#" class="meter-help-lnk" onclick="meter_reading_info_layer('elec'); return false;">Having trouble reading your electricity meter?</a>
                                        <div id="error_msg_elec"></div>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>
                    <?php } ?>

                     <div class="gas_card">                                
                        <div class="gas_box_icon gap_icon_dashboard">
                            <div class="col-md-12 floatL">

                                <div class="table-responsive" id="elecReadingTable" style="padding: 0px 30px;">
                                    <span class="tariff-h-c-p text-center line">Previous Electricity Readings</span>
                                    <table class="table product-table msg-table">

                                        <tr>
                                            <th rowspan="2" style="width:9%;">Date</th>
                                            <th></th>
                                            <th rowspan="2" style="width:13%;">Reading</th>
                                            <th colspan="2"></th>
                                        </tr>

                                        <tr>
                                        <th class="red_border"></th>
                                        <th colspan="2" class="red_border"></th>
                                        </tr>

                                        <?php
                                        if( !empty($results) )
                                        {

                                            for( $r = 0; $r<count( $results ); $r++ ) 
                                            { ?>
                                                <tr>
                                                    <td class="datesmsg bill-date" colspan="2" style="width:45%">
                                                        <?php echo date("d/m/Y", strtotime($results[$r]['readingDttm']) ); ?>
                                                    </td>

                                                    <td class="titlesmsg bill-date downloadPDFs" colspan="2">
                                                        <?php echo $results[$r]['cumulative']; ?>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="table-gap" colspan="5"></td>
                                                </tr>
                                                
                                                <?php
                                            }

                                        } 
                                        else 
                                        { ?>
                                            <span class="pay-y-cp-b-l" >
                                                You currently have no history of electricity readings on your account.
                                            </span>
                                        <?php
                                        } ?>
                                    </table>
                                </div>

                                <div class="table-responsive" id="gasReadingTable" style="display:none; padding: 0px 30px;">
                                    <span class="tariff-h-c-p text-center line">Previous Gas Readings</span>
                                    <table class="table product-table msg-table">

                                        <tr>
                                            <th rowspan="2" style="width:9%;">Date</th>
                                            <th></th>
                                            <th rowspan="2" style="width:13%;">Reading</th>
                                            <th colspan="2"></th>
                                        </tr>

                                        <tr>
                                        <th class="red_border"></th>
                                        <th colspan="2" class="red_border"></th>
                                        </tr>

                                        <?php
                                        if( !empty($gas_reading) )
                                        {

                                            for( $rr = 0; $rr<count( $gas_reading ); $rr++ )
                                            { ?>
                                                <tr>
                                                    <td class="datesmsg bill-date" colspan="2" style="width:45%">
                                                        <?php echo date("d/m/Y", strtotime($gas_reading[$rr]['readingDttm']) ); ?>
                                                    </td>

                                                    <td class="titlesmsg bill-date downloadPDFs" colspan="2">
                                                        <?php
                                                            if (isset($gas_reading[$rr]['cumulative']) ) 
                                                            {
                                                                echo $gas_reading[$rr]['cumulative']; 
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="table-gap" colspan="5"></td>
                                                </tr>

                                                <?php
                                            }

                                        } 
                                        else 
                                        { ?>
                                            <span class="pay-y-cp-b-l" >
                                                You currently have no history of gas readings on your account.
                                            </span>
                                        <?php
                                        } ?>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




	






<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<?php if( $session_data['meterTypeElec'] != 'E7') { ?>

<script type="text/javascript">
    $(document).ready(function () {

    $('#DialFirst').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSecond').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });


    $('#DialThird').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialFourth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });
    $('#DialFive').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSixth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    ///gas

    $('#Dial_Gas_First').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Second').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    $('#Dial_Gas_Third').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Fourth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });
    $('#Dial_Gas_Five').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Sixth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    // close meter_reading_info
    $('#close').click(function(){
        $('#popup-container').addClass('out');
        $('body').removeClass('popup-active');
    });


    });
</script>
	<?php } ?>


    
<?php  if( $session_data['meterTypeElec'] === 'E7') { ?>
<script>
    $(document).ready(function () {

    $('#DialFirst').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSecond').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });


    $('#DialThird').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialFourth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });
    $('#DialFive').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSixth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });



   $('#DialFirst_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialSecond_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });


    $('#DialThird_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialFourth_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });
    $('#DialFive_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialSixth_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    ///gas

    $('#Dial_Gas_First').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Second').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    $('#Dial_Gas_Third').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Fourth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });
    $('#Dial_Gas_Five').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Sixth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    

    });
</script>
	<?php } ?>
    <script>

    $(document).ready(function () {
// hide all help to begin with
    
    $("#back_button").hide();
    $("#meter-detail-1").hide();
    $("#meter-detail-2").hide();
    $("#meter-detail-3").hide();
    $("#meter-detail-4").hide();
    $("#meter-detail-5").hide();
    $("#meter-detail-6").hide();

    });

    // meter_reading_info
    function meter_reading_info_layer(fuel_type)
    {
        if(fuel_type == 'gas')
        {
            $("#help-index").hide();
            $("#meter-detail-gas").show();
            $('#meter-info').text("Information about your meter");

        }
        else
        {
            $("#meter-detail-gas").hide();
        }
            $('#popup-container').removeAttr('class').addClass('reveal');
            $('body').addClass('popup-active');
    };

</script>

 
 
 
                            
<?php require_once 'footer.php'; ?>