<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eversmart</title>

<!-- Bootstrap -->
<link href="/assets/css/eversmart.css?v=6" rel="stylesheet">
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet">
<link href="<?= base_url(); ?>css/styles.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet" type="text/css"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>
<body class="login-bg">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div class="loginOuter">
<header>

<nav class="navbar navbar-danger navbar-transparent" id="sectionsNav">
   <div class="container">
    <div class="navbar-header">
     <button class="navbar-toggle hamburger hamburger--squeeze visible-xs" data-target="#navigation-example" data-toggle="collapse" type="button">
      <span class="hamburger-box">
       <span class="hamburger-inner"></span>
      </span>
     </button>
     <a class="navbar-brand" href="https://www.eversmartenergy.co.uk/">
      <img src="<?= base_url(); ?>img/logo.png">
      <div class="ripple-container"></div>
     </a>
    </div>
    <div class="navbar-collapse no-transition" id="navigation-example">
     <ul class="nav navbar-nav navbar-right">
      <li>
       <a href="/energy-quotation.php">energy</a>
      </li>
      <li>
       <a href="/smart-meters.php">smart meters</a>
      </li>
      <li>
       <a href="/home-services.php">home services</a>
      </li>
      <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown" href="#">help <b class="caret"></b></a>
       <ul class="dropdown-menu dropdown-with-icons">
        <li>
         <a href="/help.php"><i class="material-icons">help_outline</i> help &amp; faq</a>
        </li>
        <li>
         <a href="/contact.php"><i class="material-icons">email</i> contact us</a>
        </li>
       </ul>
      </li>
      <li>
       <a class="btn btn-info btn-round btn-sm" href="/customers/index.php/portal/" style="margin-top: 10px">Login<div class="ripple-container"></div></a>
      </li>
                        
                        <li>
                            <a class="btn btn-info btn-round btn-sm" href="https://eversmartpayments.paypoint.com/energy/" target="_blank" style="margin-top: 10px; margin-left:10px;">Top Up</a>
                        </li>
     </ul>
    </div>
   </div>
  </nav>
		
		
<!-- 
<div class="headerNav">
	<ul class="navInner">
    	<li>
        	<a href="<?= base_url(); ?>index.php/portal"><img src="<?= base_url(); ?>img/loginIcon.png">Login</a>
        </li>
        <li>
        	<a href="http://54.218.78.31:85/dashboard/Evermart_Site/register.html"><img src="<?= base_url(); ?>img/signUpIcon.png">Signup</a>
        
    </ul>
</div>
</li> -->
</header>


<div class="loginBox">

<div class="loginInner">
<h2>Forgot Password</h2>
	<!-- <div class="icons">
    	<img src="<?= base_url(); ?>img/fbIcon.png">
        <img src="<?= base_url(); ?>img/twitterIcon.png">
        <img src="<?= base_url(); ?>img/googleIcon.png">
     </div> -->
     
     <div class="row" id="error_div">
        <?php if( !empty($this->session->flashdata('session_msg')) )
            {
                echo '<div class="alert alert-danger"><strong>'.$this->session->flashdata('session_msg').'</strong></div>';
            }
         ?>
          <?php if( !empty($this->session->flashdata('logout_msg')) )
            {
                echo '<div class="alert alert-success"><strong>'.$this->session->flashdata('logout_msg').'</strong></div>';
            }
         ?>
            
     </div>
    <form id="change_password" method="post">
        <div class="email">
            <img src="<?= base_url(); ?>img/emailIcon.png">
            <input required="" type="email" name="email" placeholder="Email address">
        </div>

        <!-- <div class="password">
            <img src="<?= base_url(); ?>img/passwordIcon.png">
            <input required="" type="password" name="password" placeholder="Password">
        </div>
        <div class="forgotPswd">Forgot Password?</div> -->
    
    <!-- <div class="button"><a href="http://54.218.78.31:85/dashboard/Evermart_Site/gasMeterReadingDashboard.html">Login</a></div> -->
        <input type="submit" class="button" name="login"  value="Forgot Password">
    </form>

</div><!--loginInner-->
</div><!--loginBox-->


<footer class="footer footer-white footer-big">
   <div class="container">
    <div class="content">
     <div class="row">
      <div class="col-md-3">
                           
       <a href="#footer-info" class="footerLogo">
       <img src="<?= base_url(); ?>img/logoFooter.png">
       </a>
       <p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
      </div>
      <div class="col-md-2">
       <h5>Services</h5>
       <ul class="links-vertical">
        <li><a href="/energy-quotation.php">Energy</a></li>
        <li><a href="/smart-meters.php">Smart Meters</a></li>
        <li><a href="/home-services.php">Home Services</a></li>
       </ul>
      </div>
      <div class="col-md-2">
       <h5>Company</h5>
       <ul class="links-vertical">
        <li><a href="/contact.php">Contact</a></li>
        <li><a href="/terms-and-conditions.php">Terms &amp; Conditions</a></li>
        <li><a href="/privacy.php">Privacy</a></li>
       </ul>
      </div>
      <div class="col-md-2">
       <h5>Support</h5>
       <ul class="links-vertical">
        <li><a href="/help.php">Help &amp; Support</a></li>
        <li><a href="/contact.php">Contact Us</a></li>
                                <li><a>Opening Times: <br>MON - FRI 8am-8pm<br> SAT 8am-5pm<br> SUN 9am-5pm </a></li>
       </ul>
      </div>
      <div class="col-md-3">
       <h5>Contact Us</h5>
       <div class="quick-contact">
        <div class="info info-horizontal">
         <div class="icon icon-success">
          <i class="material-icons">phone</i>
         </div>
         <div class="description">
          <h4 class="info-title"><a href="tel:03301027901">0330 102 7901</a></h4>
         </div>
        </div>
        <div class="info info-horizontal">
         <div class="icon icon-info">
          <i class="material-icons">mail</i>
         </div>
         <div class="description">
          <h4 class="info-title"><a href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry">hello@eversmartenergy.co.uk</a></h4>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
    <hr>
    <ul class="social-buttons">
     <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
     <li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>
    </ul>
    <div class="copyright pull-center">
     &copy;
     <script>
      document.write(new Date().getFullYear())
     </script> Eversmart Energy Ltd - 09310427
    </div>
   </div>
  </footer>


</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?= base_url(); ?>js/jquery-3.2.1.min.js"></script>
<script src="<?= base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>js/common.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?= base_url(); ?>js/bootstrap.js"></script>
</body>
</html>
