<?php require_once 'header.php';
   // debug($profile,1);
 ?>
 
 
 
 <style>
    /* entire container, keeps perspective */
.card-container {
    -webkit-perspective: 800px;
    -moz-perspective: 800px;
    -o-perspective: 800px;
    perspective: 800px;
    margin-bottom: 30px;
}
/* flip the pane when hovered */
.card-container:not(.manual-flip):hover .card,
.card-container.hover.manual-flip .card{
    -webkit-transform: rotateY( 180deg );
    -moz-transform: rotateY( 180deg );
    -o-transform: rotateY( 180deg );
    transform: rotateY( 180deg );
}


.card-container.static:hover .card,
.card-container.static.hover .card {
    -webkit-transform: none;
    -moz-transform: none;
    -o-transform: none;
    transform: none;
}
/* flip speed goes here */
.card {
	 -webkit-transition: -webkit-transform .5s;
   -moz-transition: -moz-transform .5s;
     -o-transition: -o-transform .5s;
        transition: transform .5s;
-webkit-transform-style: preserve-3d;
   -moz-transform-style: preserve-3d;
     -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
	position: relative;
}

/* hide back of pane during swap */
.front, .back {
	-webkit-backface-visibility: hidden;
   -moz-backface-visibility: hidden;
     -o-backface-visibility: hidden;
        backface-visibility: hidden;
	position: absolute;
	top: 0;
	left: 0;
	background-color: #FFF;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.14);
}

/* front pane, placed above back */
.front {
	z-index: 2;
}
.front {
    background-color: #ea495c;
    cursor: pointer;
    color: white;
}
/* back, initially hidden pane */
.back {
		-webkit-transform: rotateY( 180deg );
   -moz-transform: rotateY( 180deg );
     -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
        z-index: 3;
}

.back .btn-simple{
    position: absolute;
    left: 0;
    bottom: 4px;
}

/*        Style       */

.card{
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 4px;
    color: #444444;
}
.card-container, .front, .back {
	width: 100%;
	max-height: 420px;
	border-radius: 30px; height:300px;
}
.card .cover{
    min-height: 105px;
    border-radius: 4px 4px 0 0;
    height: 255px;
}
.card .cover img{
    width: 100%;
}
.card .user{
    border-radius: 50%;
    display: block;
    min-height: 120px;
    margin: -55px auto 0;
    overflow: hidden;
    width: 120px;
}
.card .user img{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 4px solid #FFFFFF;
    width: 100%;
}

.card .content{
    background-color: rgba(0, 0, 0, 0);
    box-shadow: none;
    padding: 0 20px 20px;
}
.card .content .main {
    min-height: 160px;
    background: transparent;
}
.card .back .content .main {
    height: 215px;
}
.card .name {
    font-size: 22px;
    line-height: 28px;
    margin: 10px 0 0;
    text-align: center;
    text-transform: capitalize;
}
.card h5{
    margin: 5px 0;
    font-weight: 400;
    line-height: 20px;
}
.card .profession{
    color: #999999;
    text-align: center;
    margin-bottom: 20px;
}
.card .footer {
    border-top: 1px solid #EEEEEE;
    color: #999999;
    margin: 30px 0 0;
    padding: 10px 0 0;
    text-align: center;
}
.card .footer .social-links{
    font-size: 18px;
}
.card .footer .social-links a{
    margin: 0 7px;
}
.card .footer .btn-simple{
    margin-top: -6px;
}
.card .header {
    padding: 0;
   
}
.card .motto {
    border-bottom: 1px solid #EEEEEE;
    color: #fff;
    font-size: 23px;
    font-weight: 400;
    padding-bottom: 10px;
    text-align: center;
    padding: 15px 0;
    background-color: #ea495c;
    border-radius: 30px 30px 0px 0px;
}
.card .stats-container{
	width: 100%;
	margin-top: 50px;
}
.card .stats{
	display: block;
	float: left;
	width: 33.333333%;
	text-align: center;
}

.card .stats:first-child{
	border-right: 1px solid #EEEEEE;
}
.card .stats:last-child{
	border-left: 1px solid #EEEEEE;
}
.card .stats h4{
	font-weight: 300;
	margin-bottom: 5px;
}
.card .stats p{
	color: #777777;
}
/*      Just for presentation        

    .title{color: #506A85;text-align: center;font-weight: 300;font-size: 44px;margin-bottom: 90px;line-height: 90%;}
    .title small{font-size: 17px;color: #999;text-transform: uppercase;margin: 0;}
    .space-30{height: 30px;display: block;}
    .space-50{height: 50px;display: block;}
    .space-200{height: 200px;display: block;}
    .white-board{background-color: #FFFFFF;min-height: 200px;padding: 60px 60px 20px;}
    .ct-heart{color: #F74933;}
    pre.prettyprint{background-color: #ffffff;border: 1px solid #999;margin-top: 20px;padding: 20px;text-align: left;}
    .atv, .str{color: #05AE0E;}
    .tag, .pln, .kwd{color: #3472F7;}
    .atn{color: #2C93FF;}
    .pln{color: #333;}
    .com{color: #999;}
    .btn-simple{opacity: .8;color: #666666;background-color: transparent;}
    .btn-simple:hover,.btn-simple:focus{background-color: transparent;box-shadow: none;opacity: 1;}
    .btn-simple i{font-size: 16px;}
    .navbar-brand-logo{padding: 0;}
    .navbar-brand-logo .logo{border: 1px solid #333333;border-radius: 50%;float: left;overflow: hidden;width: 60px;}
    .navbar .navbar-brand-logo .brand{color: #FFFFFF;float: left;font-size: 18px;font-weight: 400;line-height: 20px;margin-left: 10px;margin-top: 10px;width: 60px;}
    .navbar-default .navbar-brand-logo .brand{color: #555;}

*/

/*       Fix bug for IE      */

@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
    .front, .back{-ms-backface-visibility: visible;backface-visibility: visible;}
    .back {visibility: hidden;-ms-transition: all 0.2s cubic-bezier(.92,.01,.83,.67);}
    .front{z-index: 4;}
    .card-container:not(.manual-flip):hover .back,.card-container.manual-flip.hover .back{z-index: 5;visibility: visible;}
}

.gas_card{padding:30px 50px;}

</style>    

<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 main-content">
            <div class="gas_card">                                   
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>images/dashboard/Overview/refer.svg" alt=""></span>
                    <span class="dash-over-text title">Friend Referral</span>
                    <span class="dash-over-text account"> Refer your friends for free energy!</span>

                    <div class="floatL">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="rewardsbox">
                                    <div class="totalrewards">
                                        <h2 class="line" style="margin:0px">Credit Accrued</h2>    
                                        <div class="totalrewardsamount"> 
                                            £<span><?php echo $format_bal ?></span>
                                        </div>
                                        <span class="pay-y-cp-b-l" >Each time you refer one of your friends, you'll receive £50! This will be added to your current balance once your friend has a live Eversmart account.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="rewardsbox">             
                                    <div class="lastthirtydays">
                                        <h2 class="line" style="margin:0px">Credit Redeemed</h2> 
                                        <div class="lastrewardsamount"> 
                                            £<span><?php echo $format_redeem_bal ?></span>
                                        </div>
                                        <span class="pay-y-cp-b-l" >If your current reward total is £50 or more, click the '<b>Redeem Reward</b>' button below to redeem £50 of credit! This will then be added to your account balance.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12"> 
                                <div class="rewardbtndiv">
                                    <span class="tariff-bill-down">
                                        <a class="cashoutlink" href="javascript:void(0)" >Redeem Reward</a>
                                    </span>
                                    <div id="responsecash">
                                        <div class="loadercash">
                                            Sending<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>
                                        </div>
                                    </div>
                                    <div id="responsecashnotice">
                                        Notice
                                    </div> 
                                </div>
                            </div>

                            <div class="col-12" >
                                <h1 class="spacetop welcome-ptl">Refer a friend</h1>
                                <span class="ref-text">Refer Eversmart to your friends. You'll get £50, they'll get £50, each time! Here's your personal link:</span>
                                <div class="referlink"onclick="copylinkss()"onmouseover="document.getElementById('share-tip-s').style.display='block';" onmouseout="document.getElementById('share-tip-s').style.display='none';">
                                    <img src="<?php echo base_url() ?>images/refer/link-72.svg" alt="" />
                                    <div class="getthis"> 
                                        <input type="text" class="myshareinput" style="width: 90%; text-align: center;" readonly="readonly" value="<?php echo $esesharelink; ?>" id="myshare">
                                    </div>
                                    <button onclick="copylinkss()" class="getlinks">
                                    <img src="<?php echo base_url() ?>images/refer/single-copy-04.svg" alt=""/></button>
                                </div>
                            
                                <div class="share-tip">
                                    <span id="share-tip-s">Click to copy your personal link!</span>
                                </div>

                                <div class="sharelinkss">
                                    <div class="sahretitle">
                                        <span class="ref-text">Share your link via:</span>
                                    </div>

                                    <div class="share emaillink">
                                        <img style="height:unset" src="<?php echo base_url() ?>images/refer/logo-whatsapp-Colored.svg" alt=""/>
                                        <a href="<?php echo $esesharelink ?>"></a>
                                    </div>

                                    <div class="share facebooklink">
                                        <a href="http://www.facebook.com/sharer.php?u=<?php echo $esesharelink ?>" target="_blank">
                                        <img style="height:unset" src="<?php echo base_url() ?>images/refer/logo-messenger.svg" alt=""/></a>
                                    </div>

                                    <div class="share twitterllink">
                                        <a href="https://twitter.com/share?url=<?php echo $esesharelink ?>" target="_blank">
                                        <img style="height:unset" src="<?php echo base_url() ?>images/refer/logo-twitter.svg" alt=""/></a>
                                    </div>

                                    </div> 
                                </div>

                                <div class="row main-body-block-refer">
                                    <div class="col-12 ">
                                        <h1 class="welcome-ptl">How it works</h1>
                                        <span class="ref-text">Invite your friends to sign up. Once their switch completes (in 21 days) you’ll both get your rewards.</span>
                                    </div>

                                    <div class="col-md-4 imgblock">
                                        <img style="position: relative;left: 25px;" src="<?php echo base_url() ?>images/refer/refer.svg" alt=""/>
                                        <span class="ref-text">Send out your link</span>
                                    </div>

                                    <div class="col-md-4 imgblock midz">
                                        <img src="<?php echo base_url() ?>images/refer/register.svg" alt=""/>    
                                        <span class="ref-text">Your friend signs up<br> (it only takes 3 minutes)</span>
                                    </div>

                                    <div class="col-md-4 imgblock">
                                        <img src="<?php echo base_url() ?>images/refer/earn.svg" alt=""/>    
                                        <span class="ref-text">You both get £50</span>
                                    </div>
                                </div>

                                <div class="col-12 ref-title">
                                    <h2 class="line">Your referrals</h2>
                                </div>

                            </div>

							<div class="row">
                            <div class="col-sm-12 minimin" style="min-height:300px;">
                                <div class="col-md-4 col-sm-6 floatly padding-right">
                                    <div class="card-container manual-flip first" onclick="rotateCard(this)">
                                        <div class="card">
                                            <div class="front first">
                                                <div class="cover">
                                                    <div class="refer-tabbed-content-titles">
                                                        <div class="invitessent">
                                                            <?php echo $ref ?>
                                                        </div>
                                                        Friends Referred
                                                    </div>
                                                </div>
                                            </div> <!-- end front panel -->

                                            <div class="back first">
                                                <div class="header">
                                                    <h5 class="motto">
                                                        Friends Referred
                                                    </h5>
                                                </div>

                                                <div class="content">
                                                    <div class="main">
                                        
                                                        <div class="refertext"><?php if($ref == 0)
                                                        {?>
                                                            <div class="noreferimg"> 
                                                                <img style="height:50px" src="<?php echo base_url() ?>images/refer/alert-circle-exc.svg" alt=""/>
                                                            </div>
                                                            Why not tell someone how great Eversmart Energy is?<?php 
                                                        } 
                                                        else
                                                        { 
                                                            if($friends_referred->referrals == null)
                                                            {
                                                                $friend_list = null;
                                                            }
                                                            else
                                                            {
                                                            ?> <p class="refertext">So far, you have referred 
                                                                <?php  if($ref == 1) { echo $ref ?>  person:
                                                                <?php } else  { echo $ref ?> people <?php } ?>

                                                                </p>
                                                            <?php 
                                                            }
                                                        } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <!-- end back panel -->
                                        </div> <!-- end card -->
                                    </div> <!-- end card-container -->
                                </div>
                                

                                <div class="col-md-4 col-sm-6 floatly">
                                    <div class="card-container manual-flip second" onclick="rotateCard(this)">
                                        <div class="card">
                                            <div class="front second">
                                                <div class="cover">
                                                    <div class="refer-tabbed-content-titles">
                                                        <div class="completeref">
                                                            <?php $counters=0;
                                                            if($pending->referrals == null)
                                                            {
                                                            }
                                                            else
                                                            {
                                                                for ($i = 0; $i < count($pending->referrals); $i++)
                                                                {
                                                                    $ref_status = $pending->referrals[$i]->referrerModerationStatus;
                                                                    $ref_reward =  $pending->referrals[$i]->referrerModerationStatus;
                                                                    $count_referred = $pending->referrals[$i]->referredReward;

                                                                    if($count_referred == null)
                                                                    {
                                                                        $counters++;
                                                                    }
                                                                }
                                                            }
                                                            echo $counters ?>
                                                        </div>      
                                                        Currently Switching
                                                    </div> 
                                                </div>
                                            </div> <!-- end front panel -->

                                            <div class="back second">
                                                <div class="header">
                                                    <h5 class="motto">Currently Switching</h5>
                                                </div>
                                                <div class="content">
                                                    <div class="main">
                                                        <div class="refertext"><?php 
                                                            if($counters == 0)
                                                            {
                                                                ?><p class="refertext">You currently have no pending rewards. </p><?php
                                                            }
                                                            else
                                                            {
                                                                if($counters == 1)
                                                                {
                                                                    ?><p class="refertext">You currently have <?php echo $counters ?> pending reward. </p><?php 
                                                                }
                                                                else
                                                                {
                                                                    ?><p class="refertext">You currently have <?php echo $counters ?> pending rewards. </p><?php 
                                                                }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <!-- end back panel -->
                                        </div> <!-- end card -->
                                    </div> <!-- end card-container -->
                                </div>

                                <div class="col-md-4 col-sm-6 floatly padding-left">
                                    <div class="card-container manual-flip last" onclick="rotateCard(this)">
                                        <div class="card">
                                            <div class="front last">
                                                <div class="cover">
                                                    <div class="refer-tabbed-content-titles">
                                                        <div class="invitessent"><?php $counterss=0;
                                                            if($friends_referred->referrals == null)
                                                            {
                                                                $rewards_earned = 0;
                                                            }
                                                            else
                                                            {
                                                                
                                                                for ($i = 0; $i < count($friends_referred->referrals); $i++)
                                                                {
                                                                    $ref_status = $friends_referred->referrals[$i]->referrerModerationStatus;
                                                                    $ref_reward =  $friends_referred->referrals[$i]->referrerModerationStatus;
                                                                    $count_referred = $friends_referred->referrals[$i]->referredReward;

                                                                    if($count_referred == null)
                                                                    {
                                                                        $counterss++;
                                                                    }
                                                                }

                                                                $rewards_earned = $ref - $counterss;
                                                            }
                                                            echo $rewards_earned; ?>
                                                        </div>    
                                                        Friends Switched
                                                    </div>
                                                </div>
                                            </div> <!-- end front panel -->

                                            <div class="back last">
                                                <div class="header">
                                                    <h5 class="motto">Friends Switched</h5>
                                                </div>

                                                <div class="content">
                                                    <div class="main">
                                    
                                                        <div class="refertext">
                                                            <?php 
                                                            if($rew == 0) 
                                                            {?>
                                                                You haven’t earned any rewards yet!<br> Looks like it’s time to share the Eversmart love with your friends.<?php 
                                                            } 
                                                            else
                                                            {
                                                                if($rew == 1)
                                                                {
                                                                    ?>You have earned <?php echo $rewards_earned ?> reward so far! <br>Share with more of your friends to earn even more rewards!<?php 
                                                                }
                                                                else
                                                                {
                                                                    ?>You have earned <?php echo $rewards_earned ?> rewards so far! <br>Share with more of your friends to earn even more rewards!<?php 
                                                                }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <!-- end back panel -->
                                        </div> <!-- end card -->
                                    </div> <!-- end card-container -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</div>
     
<script>
    function copylinkss() 
    {
        var copyText = document.getElementById("myshare");
        copyText.select();
        document.execCommand("copy");
    }
</script>
 
 
 
 
 
 <?php require_once 'footer.php'; ?>