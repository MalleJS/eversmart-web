<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* strpos that takes an array of values to match against a string
 * note the stupid argument order (to match strpos)
 */
function strpos_arr($haystack, $needle) {
	if(!is_array($needle)) $needle = array($needle);
	foreach($needle as $what) {
		if(($pos = strpos($haystack, $what))!==false) return $pos;
	}
	return false;
}
