<?php
/*  Portal Controller */

Class Portal extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $parameters = json_decode(file_get_contents('php://input'), true);
    }

    function index()
    {
        redirect('https://www.eversmartenergy.co.uk/index.php/user/login/');

        $this->load->helper('string');
        $this->load->view('login');
    }

    function login_check()
    {

        $email = $this->input->post('user_email');
        $password = $this->input->post('user_password');
        $loginString = $email.':'.$password;
        //$loginString = 'test.uat@glboothby.com:Password123';
        //$this->session->set_userdata('emailstring',$loginString );

        $encodedLogin = base64_encode($loginString);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Api_Url.'login',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Authorization: Basic '.$encodedLogin,
                'Client-IP: '.$this->Client_ip,
                'Transfer-Encoding:chunked',
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent
            ),
        ));


        $result = curl_exec($curl);

        $result_decode = json_decode($result);

        $loginResponse = $this->parseHttpResponse($result);
        //echo '<pre>'.print_r($loginResponse,1).'</pre>';

        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);

        //debug($result);

        if( $loginResponse['body']->Message == 'SUCCESS' )
        {
            $this->session->set_userdata('newaccount_number', $loginResponse['body']->Data->AccountNumbers );
            $this->session->set_userdata('account_number', $loginResponse['body']->Data->AccountNumbers[0] );
            if(!empty($cookie))
            {
                $this->session->set_userdata('account_cookie', $cookie['account'] );
                echo 'done';
            }
        }
        else
        {
            $this->logMessage('error', 'Error: ' . $loginResponse['body']->Message);
            //echo debug($loginResponse);
        }

        if( $result->Message == 'SUCCESS' )
        {
            $this->session->set_userdata('account_number', $result->Data->AccountNumbers[0] );
            echo 'done';
        }
        else
        {
            $this->logMessage('error', 'Error message: '.$result->Message,1);
        }
        $this->logMessage(
            'error', 'Email:'.$this->obscureEmail($email)
            . ',acct='.$this->session->userdata('account_number')
            . ',cookie='.substr($this->session->userdata('account_cookie'),0,8).'...',1);
    }

    public static function logMessage($errLevel, $msg, $depth=1)
    {
        $btStack = debug_backtrace($depth);
        $callerFrame = $btStack[$depth-1];

        $sid = substr(session_id(),0,8);
        $method = $callerFrame['class'].'.'.$callerFrame['function'];
        $lineNo = $callerFrame['line'];
        $fullMessage = 'SID:'.$sid.' '.$method.'('.$lineNo.'): '.$msg;

        log_message($errLevel, $fullMessage);
    }

    function check_session()
    {
        /*if( empty($this->session->userdata('account_number')) || empty($this->session->userdata('account_cookie')) )
        {
            $this->session->set_flashdata('session_msg', 'Session Expires');
            redirect('portal/index');
        }*/
    }

    function user_logout()
    {
        $this->session->unset_userdata('account_number');
        $this->session->unset_userdata('account_cookie');
        $this->session->set_flashdata('logout_msg', 'Logout Successful');

        redirect('https://www.eversmartenergy.co.uk/index.php/user/login/');
    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart)
        {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if(strpos($lines[0], 'HTTP/1.1') !== false )
            {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0]))
                {
                    // 4xx or 5xx error
                    // so log the error
                    $this->logMessage('error', 'Line 0='.$lines[0], 2);
                }

                foreach ($lines as $j => $line)
                {
                    if ($j === 0)
                    {
                        $headers['http_code'] = $line;
                    }
                    else
                    {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            }
            // If last element, probably body
            else if($i === sizeof($responseParts)-1)
            {
                $body = json_decode($responsePart);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }

    /*
    Account=KgInYYpL\/38WSRBdkWvmLPiWw4JBEGZVcm5XX\/2KWk4W5qRF4KCIZQgJ+BpO5kSGmNd\/LVc7Y0gywx5ZcGdcCdDJCO2JWIXhYhm44Oq4Y+Y3{*}; expires=Fri, 08-Sep-2017 12:33:24 GMT; path=\/
    */

    function unescapeHeaderValue($v)
    {
        // When json strings are parsed, escape characters like '\/' are converted to '/'
        return json_decode('"' . $v . '"');
    }

    function parseCookieHeader($header)
    {
        $headerUnescaped = $this->unescapeHeaderValue($header);
        $headerParts = explode("; ", $headerUnescaped);
        $accountPart = $headerParts[0];
        $expiresPart = $headerParts[1];

        if (empty($accountPart))
        {
            $msg = 'Empty cookie received from Red Fish';
            $this->logMessage('error', $msg, 2);
            // $this->redirectOnError($msg);
        }
        return array(
            'account' => substr($accountPart, strpos($accountPart, "=")+1),
            'expires'=> substr($expiresPart, strpos($expiresPart, "=")+1)
        );
    }

    function check_login()
    {

        $encodedLogin = base64_encode($this->session->userdata('emailstring'));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Api_Url.'login',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Authorization: Basic '.$encodedLogin,
                'Client-IP: '.$this->Client_ip,
                'Transfer-Encoding:chunked',
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent
            ),
        ));

        $result = curl_exec($curl);
        if($result === false)
        {
            $this->logMessage('error', 'Curl error: ' . curl_error($curl));
            $this->logMessage('error', 'URL: "' . $this->Api_Url.'login' . '"');
        }
        $data=explode("\n",$result);
        //debug($data,1);
        return $cookieString = str_replace('Set-Cookie: ', '', trim($data[9]));
    }

    function select_account()
    {
        $this->load->view('select_account');
    }

    function session_account()
    {
        $this->session->set_userdata('account_number', $this->input->get('account_no') );
        if( !empty($this->session->userdata('account_number')) )
        {
            echo 'done';
        }
    }


    function account()
    {

        $this->check_session();
        //$cookieString = $this->check_login();
        $curl = curl_init();

        $encodedLogin = base64_encode($this->session->userdata('emailstring'));
        $this->logMessage('error', 'Sending cookie: ' . substr($this->session->userdata('account_cookie'),0,8).'...',1);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Api_Url.'Account/'.$this->session->userdata('account_number'),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Authorization: Basic '.$encodedLogin,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
        ));
        $result = curl_exec($curl);
        if($result === false)
        {
            $this->logMessage('error', 'Curl error: ' . curl_error($curl));
            $this->logMessage('error', 'URL: "' . curl_getinfo($curl, CURLINFO_EFFECTIVE_URL) . '"');
            $this->logMessage('error', 'Account cookie='.substr($this->session->userdata('account_cookie'),0,8).'...');
        }
        $loginResponse = $this->parseHttpResponse($result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);

        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
            $this->logMessage('error', 'Set cookie: ' .substr($cookie['account'],0,8).'...',2);
        }
        if( $loginResponse )
        {
            if(  $loginResponse['body']->Message == 'SUCCESS' )
            {
                $data['elec_mpan'] = $loginResponse['body']->Data->Sites[0]->ElectricMeters[0]->Mpan;
                $data['profile'] = $loginResponse['body']->Data;
                $this->session->set_userdata('account_data', $loginResponse['body']->Data);
                $this->session->set_userdata('account_name', $loginResponse['body']->Data->Name);
                $this->session->set_userdata('sites_data',  $loginResponse['body']->Data->Sites);
                $this->session->set_userdata('elec_mpan', $loginResponse['body']->Data->Sites[0]->ElectricMeters[0]->Mpan);
                $this->session->set_userdata('gas_mprn', $loginResponse['body']->Data->Sites[0]->GasMeters[0]->Mprn);

                $data['gas_mprn'] = $loginResponse['body']->Data->Sites[0]->GasMeters[0]->Mprn;
                $this->load->view('account', $data);
            }
            else
            {
                $data['elec_mpan'] = $this->session->userdata('elec_mpan');
                $data['profile'] = $this->session->userdata('account_data');
                $data['gas_mprn'] = $this->session->userdata('gas_mprn');
                $this->load->view('account', $data);
            }
        }
        else
        {
            $data['profile'] = $this->session->userdata('account_data');
            $this->load->view('account', $data);
        }

    }

    function rf_account(){

        $curl = curl_init();

        $url_arr_key = array_keys($_REQUEST);
        $url_string = base64_decode($url_arr_key[0]);

        $url_params = explode('&',$url_string);
        $token_arr = explode('=',$url_params[0]);
        $account_arr = explode('=',$url_params[1]);

        $token = $token_arr[1];
        $account_id = $account_arr[1];

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Api_Url.'Account/'.$account_id,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$token
            ),
        ));
        $result = curl_exec($curl);

        $loginResponse = $this->parseHttpResponse($result);
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);

        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
            $this->logMessage('error', 'Set cookie: ' .substr($cookie['account'],0,8).'...',2);
        }
        if( $loginResponse )
        {
            if(  $loginResponse['body']->Message == 'SUCCESS' )
            {
                $data['elec_mpan'] = $loginResponse['body']->Data->Sites[0]->ElectricMeters[0]->Mpan;
                $data['gas_mprn'] = $loginResponse['body']->Data->Sites[0]->GasMeters[0]->Mprn;
                $data['profile'] = $loginResponse['body']->Data;

                $this->session->set_userdata('account_number', $loginResponse['body']->Data->AccountNumber );
                $this->session->set_userdata('account_data', $loginResponse['body']->Data);
                $this->session->set_userdata('account_name', $loginResponse['body']->Data->Name);
                $this->session->set_userdata('sites_data',  $loginResponse['body']->Data->Sites);
                $this->session->set_userdata('elec_mpan', $loginResponse['body']->Data->Sites[0]->ElectricMeters[0]->Mpan);
                $this->session->set_userdata('gas_mprn', $loginResponse['body']->Data->Sites[0]->GasMeters[0]->Mprn);

                $this->load->view('account', $data);
            }
            else
            {
                $data['elec_mpan'] = $this->session->userdata('elec_mpan');
                $data['gas_mprn'] = $this->session->userdata('gas_mprn');
                $data['profile'] = $this->session->userdata('account_data');
                $this->load->view('account', $data);
            }
        }
        else
        {
            $data['profile'] = $this->session->userdata('account_data');
            $this->load->view('account', $data);
        }

    }


    function elec_reading()
    {
        //debug($_SESSION);
        $this->check_session();
        //$cookieString = $this->check_login();

        //$cookieString = str_replace(' ', '+', $cookieString);

        $url = $this->Api_Url.'MeterReading/'.$this->session->userdata('account_number').'/Electricity/'.$this->session->userdata('elec_mpan');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
        ));

        $result = curl_exec($curl);
        if($result === false)
        {
            if (strpos($result['body']->Message, NOELECREADINGFOUNDMESSAGE) !== false)
            {
                $this->logMessage('error', NOELECREADINGFOUNDMESSAGE , '(Not an error, carrying on)');
                $session_msg = NOINVOICEFOUNDMESSAGE_USER;
            }
            else
            {
                $msg = 'Curl error: ' . curl_error($curl). ' URL: "' . $url . '"';
                $this->logMessage('error', $msg);
                $session_msg = 'An internal error has occurred. Logout and try again. Contact us if the error persists.';
            }

            $this->session->set_flashdata('session_msg', $session_msg);
            redirect('portal/index');
            $this->load->view('account');
        }

        //debug($Elec_result);
        $elec_result_decode = json_decode($result);

        $loginResponse = $this->parseHttpResponse($result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        /*debug($loginResponse);
        echo "</pre>";*/
        /*echo '-----';
        */
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);
        //	debug($cookie);
        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
        }
        if( !empty($loginResponse['body']->Mpan) )
        {
            $data['reading_today'] = $loginResponse['body']->Meters[0]->Registers[0]->Readings[0]->Reading;
            $data['reading_previous'] = $loginResponse['body']->Meters[0]->Registers[0]->Readings[1]->Reading;
            $data['all_reading'] = $loginResponse['body']->Meters[0]->Registers[0]->Readings;
            //debug($data,1);
            $this->load->view('electric_reading', $data);
        }
        else
        {
            $data['reading_today'] = '0';
            $data['all_reading'] = '';
            $this->load->view('electric_reading',$data);
        }
    }


    function gas_reading()
    {
        $this->check_session();
        $url = $this->Api_Url.'MeterReading/'.$this->session->userdata('account_number').'/Gas/'.$this->session->userdata('gas_mprn');
        $this->logMessage('error', 'Get cookie: ' .substr($this->session->userdata('account_cookie'),0,8).'...',2);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
        ));
        $result = curl_exec($curl);
        if($result === false)
        {
            $stars = '';
            if (strpos(curl_error($curl), 'No readings for this Mprn') !== false)
            {
                $stars = '****** ';
            }
            $this->logMessage('error', 'Curl error: ' . curl_error($curl));
            $this->logMessage('error', 'URL: "' . $url . '"');
        }
        $result_decode = json_decode($result);
        //debug($result);

        $loginResponse = $this->parseHttpResponse($result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);
        //debug($this->session->userdata('account_cookie'));
        /*
        echo "</pre>";*/
        //debug($result_decode,1);
        //debug( $cookie );
        if( !empty($this->session->userdata('gas_mprn')) )
        {
            if(!empty($cookie))
            {
                $this->session->set_userdata('account_cookie', $cookie['account'] );
            }
            $data['disable'] = '0';
        }
        else
        {
            $data['disable'] = '1';
        }

        //debug( $this->session->userdata('account_cookie') );

        if( !empty($loginResponse['body']->Mprn) )
        {

            $data['reading_today'] = $loginResponse['body']->Meters[0]->Readings[0]->Reading;
            $data['reading_previous'] = $loginResponse['body']->Meters[0]->Readings[1]->Reading;
            $data['all_reading'] = $loginResponse['body']->Meters[0]->Readings;
            //debug($data);
            $this->load->view('gas_reading', $data);
        }
        else
        {
            $data['reading_today'] = '0';
            $this->load->view('gas_reading',$data);
        }
    }


    function invoice()
    {
        $this->check_session();
        //$cookieString = $this->check_login();

        $url = $this->Api_Url.'Invoice/'.$this->session->userdata('account_number');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->Api_Url.'Invoice/'.$this->session->userdata('account_number'),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
        ));
        $result = curl_exec($curl);
        if($result === false)
        {
            $this->logMessage('error', 'Curl error: ' . curl_error($curl));
            $this->logMessage('error', 'URL: "' . $url . '"');
        }
        $result_decode = json_decode($result);
        $loginResponse = $this->parseHttpResponse($result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        //debug($loginResponse,1);
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);
        // debug($loginResponse);

        // echo "</pre>";
        // debug($cookie);
        // exit;
        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
        }
        if($loginResponse['body']->Message == 'SUCCESS')
        {
            $data['invoice'] = $loginResponse['body']->Data[0]->Invoices;
            $this->load->view('elec_billing',$data);
            $this->logMessage('error', '(Not an error, carrying on)');
        }
        else
        {
            if (strpos($loginResponse['body']->Message, NOINVOICEFOUNDMESSAGE))
            {
                $session_msg = NOINVOICEFOUNDMESSAGE;
                $this->logMessage('error', NOINVOICEFOUNDMESSAGE . ' (Not an error, carrying on)');
                $target = 'portal/account';
            }
            else
            {
                $session_msg =  'An internal error has occurred. Logout and try again. Contact us if the error persists.';
                $targetView = 'account';
            }
            // $this->session->set_flashdata('session_msg', $session_msg);
            // redirect('portal/index');
            $explode = str_replace('ERROR:','', $loginResponse['body']->Message);
            $data['error'] = $explode;
            $this->load->view('elec_billing',$data);
            $this->load->view('account');
        }
    }


    // Download Invoice as PDF

    function downloadInvoice($num)
    {
        //echo $this->Api_Url.'Invoice/'.$this->session->userdata('account_number').'/Download/63';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            //CURLOPT_URL => $this->Api_Url.'Invoice/'.$this->session->userdata('account_number').'/Download/'.$num,
            CURLOPT_URL => $this->Api_Url.'Invoice/'.$this->session->userdata('account_number').'?InvoiceNumber='.$num,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
        ));
        $result = curl_exec($curl);
        if($result === false)
        {
            $this->logMessage('error', 'Curl error: ' . curl_error($curl));
            $this->logMessage('error', 'URL: "' . $this->Api_Url.'login' . '"');
        }

        $loginResponse = $this->parseHttpResponse($result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');

        //debug($loginResponse,1);
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);

        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
        }
        $this->session->set_userdata('account_cookie', $cookie['account'] );

        header('Content-type: application/pdf');
        echo $result;



    }

    function elec_billing()
    {
        $this->check_session();
        $this->load->view('elec_billing');
    }

    function gas_billing()
    {
        $this->check_session();
        $this->load->view('gas_billing');
    }

    function send_elec_reading()
    {
        $this->check_session();
        $site_data = $this->session->userdata('sites_data');

        $reading_parameter = [
            'Reading' => $this->input->post('elec_reading'),
            'Register' => $site_data[0]->ElectricMeters[0]->Meters[0]->Registers[0],
            'SerialNumber' =>$site_data[0]->ElectricMeters[0]->Meters[0]->SerialNumber
        ];
        //echo '==='.$this->session->userdata('account_cookie');
        //debug($reading_parameter);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_URL => $this->Api_Url.'MeterReading/'.$this->session->userdata('account_number').'/Electricity/'.$site_data[0]->ElectricMeters[0]->Mpan,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $Elec_result = curl_exec($curl);
        $loginResponse = $this->parseHttpResponse($Elec_result);
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);
        /*debug($loginResponse);
        echo "</pre>";
        echo '<br>~~~~';
        debug($Elec_result);
        echo "</pre>";*/
        //debug($loginResponse);
        if($loginResponse['body']->Message == 'SUCCESS')
        {
            if(!empty($cookie))
            {
                $this->session->set_userdata('account_cookie', $cookie['account'] );
            }
            echo json_encode($loginResponse['body']);
        }
        else
        {
            /*$this->session->set_flashdata('session_msg', 'Session Expires');
            redirect('portal/index');*/
            echo json_encode($loginResponse['body']->Message);
        }

    }

    function send_gas_reading()
    {
        $this->check_session();
        $site_data = $this->session->userdata('sites_data');

        //debug($site_data);
        $reading_parameter = [
            'Reading' => $this->input->post('gas_reading'),
            'SerialNumber' =>$site_data[0]->GasMeters[0]->Meters[0]->SerialNumber
        ];
        //debug($reading_parameter);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_URL => $this->Api_Url.'MeterReading/'.$this->session->userdata('account_number').'/Gas/'.$site_data[0]->GasMeters[0]->Mprn,
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Authorization: Basic '.$this->session->userdata('emailstring'),
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
                'Cookie: Account='.$this->session->userdata('account_cookie')
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $Elec_result = curl_exec($curl);
        $elec_result_decode = json_decode($Elec_result);

        $loginResponse = $this->parseHttpResponse($Elec_result);
        $this->logRequestIfErrorResponse($curl, $loginResponse, __METHOD__ .'('.__LINE__.')');
        $cookie = $this->parseCookieHeader($loginResponse['headers']['Set-Cookie']);

        if(!empty($cookie))
        {
            $this->session->set_userdata('account_cookie', $cookie['account'] );
        }
        if($loginResponse['body']->Message == 'SUCCESS')
        {
            echo json_encode($loginResponse['body']);
        }
        else
        {
            echo json_encode($loginResponse['body']);
        }
    }



    function forgotpassword()
    {
        redirect('https://www.eversmartenergy.co.uk/index.php/user/login/');

        $this->load->view('forgotpassword');
    }

    function change_password()
    {

        $reading_parameter['Email'] = $this->input->post('email');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_URL => $this->Api_Url.'ForgottenPassword/',
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $Elec_result = curl_exec($curl);
        debug($Elec_result);
    }

    function reset_password()
    {

        $reading_parameter['Email'] = $this->input->post('user_email');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_URL => $this->Api_Url.'ResetPassword/',
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $Elec_result = curl_exec($curl);
        $elec_result_decode = json_decode($Elec_result);
        //debug($elec_result_decode,1);
        if($elec_result_decode->Message == 'SUCCESS')
        {
            echo 'Request sent. You will receive an email with the instruction to reset the password shortly. Thanks';
        }
        else
        {
            echo $elec_result_decode->Message;
        }
    }

    function changepassword()
    {
        redirect('https://www.eversmartenergy.co.uk/index.php/user/login/');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,

            CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($_REQUEST['t']).'&ev='.urlencode($_REQUEST['ev']),
            //CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.$_REQUEST['t'].'&ev='.$_REQUEST['ev'],
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
            ),
        ));
        $this->logMessage('error', $curl_setopt_array['CURLOPT_URL'],1);
        $Elec_result = curl_exec($curl);
        //debug($Elec_result,1);
        $elec_result_decode = json_decode($Elec_result);
        //debug($elec_result_decode,1);
        if($elec_result_decode->Message == 'SUCCESS')
        {
            $data['email'] = $elec_result_decode->Data->Email;
            $data['t'] = $_REQUEST['t'];
            $data['ev'] = $_REQUEST['ev'];
            $this->load->view('changepassword', $data);
        }
        else
        {
            $data['error'] = json_decode($Elec_result);
            $this->load->view('changepassword', $data);
        }
    }

    function new_password()
    {
        $reading_parameter['Email'] = $this->input->post('email');
        $reading_parameter['Password'] = $this->input->post('password');
        $reading_parameter['ConfirmPassword'] = $this->input->post('cpassword');

        //die();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_URL => $this->Api_Url.'RedeemPasswordReset/?t='.urlencode($this->input->post('t')).'&ev='.urlencode($this->input->post('ev')),
            CURLOPT_HTTPHEADER => array(
                'Api-Key: '.$this->Api_Key,
                'Api-Type: Website',
                'Client-IP: '.$this->Client_ip,
                'Content-Type:application/json',
                'Accept: application/json',
                'Client-UA:'.$this->client_agent,
            ),
            CURLOPT_POSTFIELDS => json_encode($reading_parameter),
        ));
        $Elec_result = curl_exec($curl);
        $elec_result_decode = json_decode($Elec_result);
        if($elec_result_decode->Message == 'SUCCESS')
        {
            $this->session->set_flashdata('logout_msg', 'Password Changed');
            redirect('portal/index');
        }
        else
        {
            $this->session->set_flashdata('session_msg', 'Password Change Fail');
            redirect('portal/changepassword');
        }
    }



    function logRequestIfErrorResponse($curl, $webResponse, $pfx)
    {
        $sid = substr(session_id(),0,8);
        $email = $this->session->userdata('emailstring');
        $accountNumber = $this->session->userdata('account_number');

        if (preg_match('/^HTTP\/1.1 [4,5]/', $webResponse['headers']['http_code']))
        {
            // 4xx or 5xx error
            // so log the caller details

            if (is_null($curl))
            {
                $this->logMessage('error', 'Curl object is null');
            }
            else
            {
                $curlinfo= curl_getinfo($curl);
                $errorString=  'Curl Err: '.$curlinfo['http_code'].', Url: '.$curlinfo['url'];
            }

            $errorString .= ' - ' . json_encode($webResponse['body']);
            $this->logMessage('error', $errorString, 2);
            $this->logMessage('error',
                'login='.$email
                . ',acct='.$accountNumber
                . ',cookie='.substr($this->session->userdata('account_cookie'),0,8).'...', 2);
            $this->logMessage('error', '<-----< called from', 3);
        }
    }

    const REDFISHERRORSTRINGS = array(
        "No HTTP resource was found that matches the request URI",
        "ERROR: No token received",
        "ERROR: Token is not valid",
        "An error has occurred",
        "Empty cookie received",
        "Curl error",
        "IP is not valid");

    const NOINVOICEFOUNDMESSAGE = "No invoices found for this account";
    const NOELECREADINGFOUNDMESSAGE = 'No readings for this Mpan.';
    const NOELECREADINGFOUNDMESSAGE_USER = 'No electric readings for this account.';

    /* strpos that takes an array of values to match against a string
     * note the stupid argument order (to match strpos)
     */
    function strpos_arr($haystack, $needle) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $what) {
            if(($pos = strpos($haystack, $what))!==false) return $pos;
        }
        return false;
    }

    function obscureEmail($email) {
        return $email;

        $parts = explode("@", $email, 2);
        $maskedEmail =
            substr($parts[0], 0,3)
            + "*****@***"
            + substr($parts[1], -4, 4);

        return $maskedEmail;
    }

    function redirectOnError($message)
    {
        if($this->strpos_arr($message, REDFISHERRORSTRINGS))
        {
            $message = 'An internal error has occurred. Logout and try again. Contact us if the error persists.';
        }
        $this->session->set_flashdata('session_msg', $message);

        redirect('portal/index');
        $this->load->view('account');
    }

}
