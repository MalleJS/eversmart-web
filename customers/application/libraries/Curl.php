<?php

Class Curl{

  function curl_request( $api_key , $data =null, $method = null )
  {
    $curl = curl_init();

    curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);

    //test url:
    //curl_setopt($curl,CURLOPT_URL, 'https://sandbox.svcs2.energylinx.com:443'.$api_key);

    // live url
    //curl_setopt($curl,CURLOPT_URL, 'https://svcs2.energylinx.com:443'.$api_key);

    //new live url
    curl_setopt($curl,CURLOPT_URL, 'https://apis2.awsprod.energylinx.com'.$api_key);    
if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' )
{
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
}
    // test token
    //curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization: Token b9f44c525cec9703a54c3b3168a14ff2e422758a'));

    //live token
    //curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization: Token 9479c19fbc0d8528a639ac20b15de2f2aecc9903'));

    // new live 
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization: Token 9479c19fbc0d8528a639ac20b15de2f2aecc9903'));
    if( $method == 'post' )
    {
      curl_setopt($curl, CURLOPT_POST, true);
    }
    if( !empty($data) )
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    //print_r( $data);
    //header('content-type: application/json');
    return curl_exec($curl);
 curl_close($curl);
  }

  function junifer_request_test($api_url,$data =null,$method = null){
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl,CURLOPT_URL, 'http://jatin-0afa8410-eval-test.apigee.net/eversmart'.$api_url);
    if( $method == 'post' )
    {
      curl_setopt($curl, CURLOPT_POST, true);
    }
    if( !empty($data) )
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
    header('content-type: application/json');
    return curl_exec($curl);
  }

  function junifer_request($api_url,$data =null,$method = null){
    $curl = curl_init();
    $data_json = json_encode($data);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
  
    
    if( $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '18.191.137.119' )  //local
{

    curl_setopt($curl,CURLOPT_URL, '134.213.125.150:44002'.$api_url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Api-Type: Website',
                // 'Client-IP: '.$_SERVER['REMOTE_ADDR'],
              // 'Client-IP: 127.0.0.1:8000',
                // 'Transfer-Encoding:chunked',
                'X-Junifer-X-apikey: zH7bDom4',
                'X-Junifer-X-username: apiuser1',
                'Content-Type:application/json',
                'Accept: application/json',
                // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
              ));

    curl_setopt($curl, CURLOPT_HEADER, 1);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if( $method == 'post'  )
    {
      curl_setopt($curl, CURLOPT_POST, true);
    }
    if( $method == 'put' )
    {
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    }
    if( !empty($data) )
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
    //header('content-type: application/json');
    return curl_exec($curl);
    //debug($httpcode,1);

 curl_close($curl);
}

if( $_SERVER['HTTP_HOST'] == '52.56.76.183' || $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk' ) //dev
{

  curl_setopt($curl,CURLOPT_URL, '134.213.12.122:43002'.$api_url);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
              'Api-Type: Website',
              // 'Client-IP: '.$_SERVER['REMOTE_ADDR'],
            // 'Client-IP: 127.0.0.1:8000',
              // 'Transfer-Encoding:chunked',
              'X-Junifer-X-apikey: dgiZaFDb',
              'X-Junifer-X-username: api',
              'Content-Type:application/json',
              'Accept: application/json',
              // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
            ));

            curl_setopt($curl, CURLOPT_HEADER, 1);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if( $method == 'post'  )
            {
              curl_setopt($curl, CURLOPT_POST, true);
            }
            if( $method == 'put' )
            {
              curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            if( !empty($data) )
            {
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            }
            //header('content-type: application/json');
            return curl_exec($curl);
            //debug($httpcode,1);
        
         curl_close($curl);
                   
}

}


  function curl_dyball($api_url,$data =null,$method = null){
        $curl = curl_init();
        $data_json = json_encode($data);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl,CURLOPT_URL, 'https://engq.dyballapi.co.uk'.$api_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Api-Type: Website',
                    // 'Client-IP: '.$_SERVER['REMOTE_ADDR'],
                   // 'Client-IP: 13.58.101.121',
                    // 'Transfer-Encoding:chunked',
                    'Content-Type:application/json',
                    'Accept: application/json',
                    // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
                  ));

        curl_setopt($curl, CURLOPT_HEADER, 1);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if( $method == 'post'  )
        {
          curl_setopt($curl, CURLOPT_POST, true);
        }
        if( $method == 'put' )
        {
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if( !empty($data) )
        {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }
        //header('content-type: application/json');
        return curl_exec($curl);
  }

  function junifer_request_usage($api_url,$data =null,$method = null){
    $curl = curl_init();
    $data_json = json_encode($data);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

    if( $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' || $_SERVER['HTTP_HOST'] == '18.191.137.119'  )  //local
    {
      curl_setopt($curl,CURLOPT_URL, '134.213.125.150:44002'.$api_url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Api-Type: Website',
        // 'Client-IP: '.$_SERVER['REMOTE_ADDR'],
      // 'Client-IP: 127.0.0.1:8000',
        // 'Transfer-Encoding:chunked',
        'X-Junifer-X-apikey: zH7bDom4',
        'X-Junifer-X-username: apiuser1',
        'Content-Type:application/json',
        'Accept: application/json',
        // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
      ));
  
    }
    
    if( $_SERVER['HTTP_HOST'] == '52.56.76.183' || $_SERVER['HTTP_HOST'] == 'www.eversmartenergy.co.uk') //dev
    {
      curl_setopt($curl,CURLOPT_URL, '134.213.12.122:43002'.$api_url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Api-Type: Website',
        // 'Client-IP: '.$_SERVER['REMOTE_ADDR'],
      // 'Client-IP: 127.0.0.1:8000',
        // 'Transfer-Encoding:chunked',
       // production junifer
       'X-Junifer-X-apikey: dgiZaFDb',
       'X-Junifer-X-username: api',
        'Content-Type:application/json',
        'Accept: application/json',
        // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
      ));
  
    }

  //  curl_setopt($curl, CURLOPT_HEADER, 1);
  //  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if( $method == 'post' )
    {
      curl_setopt($curl, CURLOPT_POST, true);
      
    }
    if( !empty($data) )
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
   
    return curl_exec($curl);
    //debug($httpcode,1);
     curl_close($curl);
  } 


  function curlPost($url,$data =null,$method = null){
    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);


     curl_setopt($curl, CURLOPT_HEADER, 1);

      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Api-Type: Website',
              //  'Client-IP: '.$_SERVER['REMOTE_ADDR'],
              //'Client-IP: 148.252.235.110',
             //   'Transfer-Encoding:chunked',
                'Content-Type:application/json',
                'Accept: application/json',
                // 'Client-UA:'.$_SERVER['HTTP_USER_AGENT']
              ));
    
     $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
 
    if( $method == 'post' ) 
    { 
      curl_setopt($curl, CURLOPT_POST, true); 
    }
    if( !empty($data) )
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
  
    return curl_exec($curl);
    //debug($httpcode,1);
    curl_close($curl);
  }
  
  function curlSaas($url,$data =null,$method = null){
      $curl = curl_init($url);
      
    
    //  curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
   // curl_setopt($curl_handle, CURLOPT_POST, FALSE);
    curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

    //  Test Tenant Alias (test_aewc1h1gdufsj) will need to be changed to Live one (ac6qste3t4yl0) in future
    curl_setopt($curl,CURLOPT_URL, 'https://app.referralsaasquatch.com/api/v1/ac6qste3t4yl0/'.$url);

    //  curl_setopt($curl, CURLOPT_HEADER, 1);
      
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Api-Type: Website',
          'Content-Type:application/json',
          'Accept: application/json',
          'Authorization: Basic QVBJX0tFWTpMSVZFX2FXcndhcTJOMnZZYmF3TDNoN0dFQnJlcmxVM3hGYVpY'                    
      ));
      
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      
      if( $method == 'post' )
      {
        curl_setopt($curl, CURLOPT_POST, true);
      }
      if( !empty($data) )
      {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
      }
      
      return curl_exec($curl);
      //debug($httpcode,1);
      curl_close($curl);
  }

  function wordCurl($url,$data =null,$method = null)
  {
    $curl = curl_init($url);
    
    //  curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    // curl_setopt($curl_handle, CURLOPT_POST, FALSE);
    curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($curl,CURLOPT_URL, 'https://www.eversmartenergy.co.uk/blog/wp-json/wp/v2/'.$url);

    //  curl_setopt($curl, CURLOPT_HEADER, 1);
      
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Cache-Control: no-cache',
        'Content-Type:application/json',
        'Accept: application/json'         
    ));
    
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
    if( $method == 'post' )
    {
      curl_setopt($curl, CURLOPT_POST, true);
    }
    if( !empty($data) )
    {
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    }
    
    return curl_exec($curl);
    //debug($httpcode,1);
    curl_close($curl);
  }

  function curlIntercom($url,$data =null,$method = null)
  {
    $curl = curl_init($url);
    
    //  curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    // curl_setopt($curl_handle, CURLOPT_POST, FALSE);
    curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($curl,CURLOPT_URL, 'https://api.intercom.io/'.$url);

    //  curl_setopt($curl, CURLOPT_HEADER, 1);
      
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Accept: application/json",
        "Content-Type: application/json",
        "Authorization: Bearer dG9rOjk5M2I3ZjE3X2IwYzJfNDgxZl9hOGU4X2I2MjBiMTYyZmNhNDoxOjA=",
        "cache-control: no-cache"        
      ));
      
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      
      if( $method == 'post' )
      {
        curl_setopt($curl, CURLOPT_POST, true);
      }
      if( !empty($data) )
      {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
      }
      
      return curl_exec($curl);
      //debug($httpcode,1);
      curl_close($curl);
  }

}

 ?>
