**Eversmart Energy Web - For Devs**

Listen up maggots, if you want to level up as a dev. You got to start using a decent environment to build cool stuff in.

---

## Docker Installation

1. First step, you'll need to check your hardware supports virtualization. You can find this information in the BIOS
2. Install Docker. https://docs.docker.com/install/
3. Once you've installed Docker, you'll need to open up a terminal window and run; **docker -v** to check you've got it installed correctly.
4. In your hidden icons or running applications, you should see the docker engine. Congratulations!

---

## Docker Compose

1. Go to your project root on a terminal
2. Run docker-compose up
3. You're ready to roll!

** You can visit port 8080 to access Phpmyadmin directly from your environmen **

---

## Let that forking workflow in & Git Basics

We use the forking git workflow to manage our versioning, here are a bunch of useful git commands if you aren't familiar with terminal.

---

**NOTE: PLEASE BE SURE THAT YOUR CODE WORKS BEFORE SUBMITTING A PULL REQUEST**

**Label your branches according to the type of work you are doing. I.e feature/EW-xxx, bugfix/EW-xxx, support/EW-xxx**

git checkout -b "hotfix/-name"

git status - show files

git add - add them to staging for commit

git commit -m 'Changes for'

git pull upstream master - when you want to get eversmart master code into jack master

git checkout -b feature/new-branch-name

** Pushing to the public repo
git push origin branch-name - push to my repository
git push upstream branch-name- push to eversmart repository

---


