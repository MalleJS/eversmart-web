(function(){

        // global variables
    var isDesktop = window.matchMedia("(min-width: 1024px)");

        // functions to run when we cross either side of the mobile/desktop main menu threshold
    function toggleMenuStyle( evt ){
        switch( evt.matches ){
            case true: // desktop
                resetMobileMenu();
            break;
            case false: // mobile
                closeDropDowns();
            break;
        };
    };

        // add class to menu items that we want to hide when we got to the desktop menu
    Handlebars.registerHelper( 'markDropDown', function( addToDropDown ) {
        switch( addToDropDown ){
            case true:
                return ' includedInDropDown';
        };
    });

        // reset the mobile menu to closed
    function resetMobileMenu(){

        var $menuIcon = $('#menu-icon');

        $menuIcon.removeClass( 'open' );
        TweenMax.set( $menuIcon.find('.inner > span:not(:nth-child(2))'), { rotation : '0deg' });
        TweenMax.set( $('#menu-wrap'), { left : '-100vw' });
        TweenMax.set( $menuIcon.find('.inner > span:nth-child(2)'), { opacity : 1 });
        TweenMax.set( $menuIcon.find('.inner > span:first-child'), { top : '8px' });
        TweenMax.set( $menuIcon.find('.inner > span:last-child'), { top : '28px' });
        $('body').removeClass('menu-open');

    };

        // mobile burger menu click functionality
    function evtClick_menuLink( evt ){
        try{ evt.preventDefault() }catch( er ){};

        var $this = $(evt.currentTarget);
        $this.off( 'click', evtClick_menuLink );

        switch( $this.hasClass( 'open' ) ){
            case true:
                $this.removeClass( 'open' );
            break;
            case false:
                $this.addClass( 'open' );
            break;
        };

        $this.children().first().toggleClass("is-active");
        $('body')[ $this.hasClass( 'open' ) ? 'addClass' : 'removeClass' ]('menu-open');
        $this.on( 'click', evtClick_menuLink );
    };

        // close dropdowns if open
    function closeDropDowns( el ){
        $( el ? el : '.drop-down-link.open' ).each(function( i, el ){
            evtClick_dropDownLink.call( el, {}, true );
        });
    };

        // called from a body click event listener
    function evtClick_hideDropDown( evt ){
        return !$(evt.target).hasClass('open') ? closeDropDowns() : false ; 
    };

        // called from a main menu header with a drop down
    function evtClick_dropDownLink( evt, overdide ){
        try{ evt.preventDefault() }catch( err ){};

        var $this = $(this);
        var otherDropDownsOpen = $('.drop-down-link.open').not( this );
        var TL = new TimelineMax({
            paused : true
        });

        otherDropDownsOpen.length && closeDropDowns( otherDropDownsOpen );

        switch( overdide || $this.hasClass('open') ){
            case true:
                $this.removeClass('open');
                TL.to( $this.next(), 0.3, { css:{ autoAlpha : 0, display : 'none' }, ease : Power2.easeInOut });
                $('body').off( 'click', evtClick_hideDropDown );
            break;
            case false:
                $this.addClass('open');
                TL.to( $this.next(), 0.3, { css:{ autoAlpha : 1, display : 'block' }, ease : Power2.easeInOut });
                $('body').on( 'click', evtClick_hideDropDown );
            break;
        };

        TL.play();

    };

        // checks what menu items need drop downs on desktop from the menu data and create an object of the reseults
    function retrieveDropDowns( data ){
        var dropDowns = [];
        data.forEach(function( item ){
            if( item.addToDropDown && dropDowns.indexOf( item.dropDown ) === -1 ){
                dropDowns.push( item.dropDown );
            };
        });
        dropDowns = dropDowns.map(function( item ){
            var toInclude = [];
            data.forEach(function( menuItem ){
                if( menuItem.dropDown === item ){
                    toInclude.push( menuItem );
                };
            });
            return { name : item, items : toInclude };
        });
        return dropDowns;
    };

        // load in the menu item template, then build and attatch required listeners
    REQUEST( '/templates/everywhere/menu-item.hbs', 'text' )
        .then(function( res ){
            $(function(){
                var template = Handlebars.compile( res );
                var menuData = [
                    {
                         title : 'Boilers & Heating',
                         name : 'Boilers & Heating',
                         href : '/homeservices',
                         addToDropDown : false
                    },
                    {
                        title : 'Our Vision',
                        name : 'Our Vision',
                        href : '/Vision',
                        addToDropDown : true,
                        dropDown : 'about'
                    },
                    {
                        title : 'Careers',
                        name : 'Careers',
                        href : '/Career',
                        addToDropDown : true,
                        dropDown : 'about'
                    },
                    {
                        title : 'FAQs',
                        name : 'FAQs',
                        href : '/Helpfaqs',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'Help',
                        name : 'Help',
                        href : 'http://help.eversmartenergy.co.uk',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'Contact us',
                        name : 'Contact us',
                        href : '/contact_us',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'Emergency numbers',
                        name : 'Emergency numbers',
                        href : '/Emergency',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'Priority Services Register',
                        name : 'Priority Services Register',
                        href : 'https://s3.eu-west-2.amazonaws.com/website-cdn-assets/Priority+Services+Register.pdf',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'Privacy',
                        name : 'Privacy',
                        href : '/Policies',
                        addToDropDown : true,
                        dropDown : 'help'
                    },
                    {
                        title : 'T&Cs',
                        name : 'T&Cs',
                        href : '/Terms',
                        addToDropDown : true,
                        dropDown : 'help'
                    }
                ];
                var parsedMenu = template({ forDropDown : false, menuData : menuData });
                var parsedMenuExtras = template({ forDropDown : true, dropDowns : retrieveDropDowns( menuData ) });

                $('#menu-list').append( parsedMenu );
                $('#menu-list').append( parsedMenuExtras );
                $('#menu-icon').on( 'click', evtClick_menuLink );
                $('.drop-down-link').on( 'click', evtClick_dropDownLink );
                isDesktop.addListener( toggleMenuStyle );
            });
        });

})();
