// Avoid `console` errors in browsers that lack a console.
(function () {
	let method;
	let videos;
	let noop = function () {};
	let methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
	];
	let length = methods.length;
	let console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}

	videos = document.getElementsByTagName("video");
	window.addEventListener('scroll', checkScroll, false);
	window.addEventListener('resize', checkScroll, false);

	var header = document.getElementById("MAIN-header");

	var sticky = header.offsetTop;

	window.onscroll = function () {
		if (!document.body.classList.contains("menu-open")) {
			if (window.pageYOffset > sticky) {
				header.classList.add("sticky");
			} else {
				header.classList.remove("sticky");
			}
		}
	};
}());


	// add x number of days to the current date
Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
};
Date.prototype.ordinal = function(){
	var d = this.getDate();
	return d+(31==d||21==d||1==d?"st":22==d||2==d?"nd":23==d||3==d?"rd":"th")
};

	// returns the date format DD/MM/YYYY
Date.prototype.formatted = function(){
	var date = this.getDate();
	var month = this.getMonth()+1;
	month = month < 10 ? '0'+month : month;
	date = date < 10 ? '0'+date : date;
	return date +'/'+ month +'/'+ this.getFullYear();
};

	// generate unique ID e.g. E2350143528-4164020887-938913176-2513998651
function uuid() {
	return 'E'+crypto.getRandomValues(new Uint32Array(4)).join('-');
}

	// turn { yes : 'test', no, 'test' } into yes=test&no=test
function serializeJSON( json ){
	return Object.keys( json ).map(function( item ){ return encodeURIComponent( item )+'='+encodeURIComponent( json[ item ] ) }).join('&');
};

	// query string to json
function queryToJSON( query ){
	var keyVals = query.split( '&' );
	var obj = {};

	keyVals.forEach(function( item ) {
		var split = item.split( '=' );
		obj[split[0]] = split[1];
	});

	return obj;
};


	// json object to query string
function serialiseJSON( data ){
	var payload = [];
	Object.keys( data ).forEach(function( key ){
		payload.push( key+'='+data[key] );
	});
	return payload.join( '&' );
};


	// request helper
function REQUEST( url, resType, opts ){
	opts = opts || {};
	return fetch( url, opts )
		.then(function( res ){
			return resType === 'none' ? res : res[ resType || 'json' ]() ;
		})
		.catch(function( err ){
			// togglePreloader();
			ERROR( err );
		});
};

	// reset event handler
function resetEventHandler( selector, evt, handler ){
    document.querySelectorAll( selector ).forEach(function( el ){
        el.removeEventListener( evt, handler );
        el.addEventListener( evt, handler );
    });
};

	// clean object
function cleanObj( obj ){
	return JSON.parse( JSON.stringify( obj ) );
}

function checkScroll(videos = '') {
	for (let i = 0; i < videos.length; i++) {
		var video = videos[i];
		var fraction = 0.8;

		var x = video.offsetLeft,
			y = video.offsetTop,
			w = video.offsetWidth,
			h = video.offsetHeight,
			r = x + w, //right
			b = y + h, //bottom
			visibleX, visibleY, visible;

		visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
		visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window
			.pageYOffset));

		visible = visibleX * visibleY / (w * h);

		if (visible > fraction) {
			video.play();
		} else {
			video.pause();
		}
	}
}
