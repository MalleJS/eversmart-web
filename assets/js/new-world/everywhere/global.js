window.global = {
    preloader: {
        $el: null,
        isActive: false,
        duration: 0.25
    },
    errors: {
        duration: 7000,
        selector: '#errors-wrap'
    }
};

function fireRakutenConverion() {
    let rm_trans = {
        affiliateConfig: {ranMID: '43604', discountType: 'item'},
        displayConfig: {rdMID: '7733'},
        orderid: Date.now() + "_" + Math.floor(Math.random() * 999999999),
        currency: 'GBP',
        customerStatus: 'Successful',
        conversionType: 'Sale',
        customerID: ' ',
        discountCode: ' ',
        discountAmount: 0,
        taxAmount: 0,
        lineitems: [{
            quantity: 1,
            unitPrice: 1,
            unitPriceLessTax: 1,
            SKU: 'ENERGY_SIGNUP',
            productName: 'ENERGY'
        }]

    };

    /*Do not edit any information beneath this line*/
    if(!window.DataLayer){window.DataLayer={Sale:{Basket:rm_trans}}}else{DataLayer.Sale=DataLayer.Sale||{Basket:rm_trans};DataLayer.Sale.Basket=DataLayer.Sale.Basket||rm_trans}DataLayer.Sale.Basket.Ready = true; function __readRMCookie(e){for(var a=e+"=",r=document.cookie.split(";"),t=0;t<r.length;t++){for(var n=r[t];" "==n.charAt(0);)n=n.substring(1,n.length);if(0==n.indexOf(a))return n.substring(a.length,n.length)}return""}function __readRMCookiev2(e,a){for(var r=__readRMCookie(a=a||"rmStore");r!==decodeURIComponent(r);)r=decodeURIComponent(r);for(var t=r.split("|"),n=0;n<t.length;n++){var i=t[n].split(":")[0],o=t[n].split(":")[1];if(i===e)return o}return""}function __readParam(e,a,r,t){var n=e||"",i=a||"",o=r||"",s=t||{},d=__readRMCookiev2(n),u=s[i],m=(d=s.ignoreCookie||!1?0:d)||u||o;return m=("string"!=typeof m||"false"!==m.toLowerCase())&&m}function sRAN(){var e=DataLayer&&DataLayer.Sale&&DataLayer.Sale.Basket?DataLayer.Sale.Basket:{},a=e.affiliateConfig||{},r=__readParam("atm","tagType","pixel",a),t=__readParam("adr","discountType","order",a),n=__readParam("acs","includeStatus","false",a),i=__readParam("arto","removeOrderTax","false",a),o=__readParam("artp","removeTaxFromProducts","false",a),s=__readParam("artd","removeTaxFromDiscount","false",a),d=__readParam("atr","taxRate",0,a);d=Number(d);var u=__readParam("ald","land",!1,{})||(a.land&&!0===a.land?__readRMCookie("ranLandDateTime"):a.land)||!1,m=__readParam("atrv","tr",!1,{})||(a.tr&&!0===a.tr?__readRMCookie("ranSiteID"):a.tr)||!1,l=!1,c=__readParam("amid","ranMID","",a)||e.ranMID;if(!c)return!1;if(!(void 0===a.allowCommission||"false"!==a.allowCommission))return!1;var p=e.orderid||"OrderNumberNotAvailable",f="",y="",_="",v="",N=e.currency||"";N=N.toUpperCase();var h=e.taxAmount?Math.abs(Math.round(100*Number(e.taxAmount))):0,g=e.discountAmount?Math.abs(Math.round(100*Number(e.discountAmount))):0;if(s&&d)var C=(100+Number(d))/100,g=Math.round(g/C);var b="pixel"===r?"ep":"mop"===r?"eventnvppixel":"ep",S=e.customerStatus||"",D=document.location.protocol+"//track.linksynergy.com/"+b+"?",w="";null!=S&&""!=S&&(n&&"EXISTING"==S.toUpperCase()||n&&"RETURNING"==S.toUpperCase())&&(w="R_");for(var P=[],x=0,T=0;T<(e.lineitems?e.lineitems.length:0);T++){for(var R=!1,k=window.JSON?JSON.parse(JSON.stringify(e.lineitems[T])):e.lineitems[T],L=0;L<P.length;L++)P[L].SKU===k.SKU&&(R=!0,P[L].quantity=Number(P[L].quantity)+Number(k.quantity));R||P.push(k),x+=Number(k.quantity)*Number(k.unitPriceLessTax||k.unitPrice)*100}for(T=0;T<P.length;T++){var k=P[T],I=encodeURIComponent(k.SKU),M=k.unitPriceLessTax||k.unitPrice,U=k.quantity,A=encodeURIComponent(k.productName)||"",O=Math.round(Number(M)*Number(U)*100);!o||!d||k.unitPriceLessTax&&k.unitPriceLessTax!==k.unitPrice||(O/=C=(100+d)/100),"item"===t.toLowerCase()&&g&&(O-=g*O/x),f+=w+I+"|",y+=U+"|",_+=Math.round(O)+"|",v+=w+A+"|"}f=f.slice(0,-1),y=y.slice(0,-1),_=_.slice(0,-1),v=v.slice(0,-1),g&&"order"===t.toLowerCase()?(f+="|"+w+"DISCOUNT",v+="|"+w+"DISCOUNT",y+="|0",_+="|-"+g):g&&"item"===t.toLowerCase()&&(l=!0),i&&h&&(f+="|"+w+"ORDERTAX",y+="|0",_+="|-"+h,v+="|"+w+"ORDERTAX"),D+="mid="+c+"&ord="+p+"&skulist="+f+"&qlist="+y+"&amtlist="+_+"&cur="+N+"&namelist="+v+"&img=1&",u&&(D+="land="+u+"&"),m&&(D+="tr="+m+"&"),l&&(D+="discount="+g+"&"),"&"===D[D.length-1]&&(D=D.slice(0,-1));var E,B=document.createElement("img");B.setAttribute("src",D),B.setAttribute("height","1px"),B.setAttribute("width","1px"),(E=document.getElementsByTagName("script")[0]).parentNode.insertBefore(B,E)}function sDisplay(){var e=null,a=null,r=null,t=null,n=window.DataLayer&&window.DataLayer.Sale&&window.DataLayer.Sale.Basket?window.DataLayer.Sale.Basket:{},i=n.displayConfig||{},o=n.customerStatus||"",s=n.discountAmount?Math.abs(Number(n.discountAmount)):0,d=null,u=__readParam("dmid","rdMID","",i);if(!u)return!1;var m=__readParam("dtm","tagType","js",i),l="if"===(m="js"===m||"if"===m||"img"===m?m:"js")?"iframe":"img"===m?m:"script",c="//"+__readParam("ddn","domain","tags.rd.linksynergy.com",i)+"/"+m+"/"+u,p=__readParam("dis","includeStatus","false",i),f="";if(null!=o&&""!=o&&(p&&"EXISTING"==o.toUpperCase()||p&&"RETURNING"==o.toUpperCase())&&(f="R_"),!n.orderid||!n.conversionType)return!1;r=0,a=f+n.orderid,e="",t="conv",d=n.currency;for(var y=0;y<(n.lineitems?n.lineitems.length:0);y++)r+=Number(n.lineitems[y].unitPriceLessTax)*Number(n.lineitems[y].quantity)||Number(n.lineitems[y].unitPrice)*Number(n.lineitems[y].quantity),e+=encodeURIComponent(n.lineitems[y].SKU)+",";r=Math.round(100*(r-s))/100,(e=e.slice(0,-1))&&(c=c.indexOf("?")>-1?c+"&prodID="+e:c+"/?prodID="+e),a&&(c=c.indexOf("?")>-1?c+"&orderNumber="+a:c+"/?orderNumber="+a),r&&(c=c.indexOf("?")>-1?c+"&price="+r:c+"/?price="+r),d&&(c=c.indexOf("?")>-1?c+"&cur="+d:c+"/?cur="+d),t&&(c=c.indexOf("?")>-1?c+"&pt="+t:c+"/?pt="+t);var _=document.createElement(l);_.src=c,"script"===l?_.type="text/javascript":"iframe"===l&&_.setAttribute("style","display: none;"),document.getElementsByTagName("body")[0].appendChild(_)}function sSearch(){var e=window.DataLayer&&window.DataLayer.Sale&&window.DataLayer.Sale.Basket?window.DataLayer.Sale.Basket:{},a=e.searchConfig||{},r=__readParam("smid","rsMID","",a);if(!r)return!1;var t=function(){var t=e.discountAmount?Math.abs(Number(e.discountAmount)):0,n=__readParam("sct","conversionType","conv",a),i=0,o="";if(!e.orderid)return!1;i=0,o=e.orderid;for(var s=0;s<(e.lineitems?e.lineitems.length:0);s++)i+=Number(e.lineitems[s].unitPrice)*Number(e.lineitems[s].quantity);i=Math.round(100*(i-t))/100;window.DataLayer.Sale.Basket;var d=[];d[0]="id="+r,d[1]="type="+n,d[2]="val="+i,d[3]="orderId="+o,d[4]="promoCode="+e.discountCode||"",d[5]="valueCurrency="+e.currency||"USD",d[6]="GCID=",d[7]="kw=",d[8]="product=",k_trackevent(d,"113")},n=document.location.protocol.indexOf("s")>-1?"https://":"http://";n+="113.xg4ken.com/media/getpx.php?cid="+r;var i=document.createElement("script");i.type="text/javascript",i.src=n,i.onload=t,i.onreadystatechange=function(){"complete"!=this.readyState&&"loaded"!=this.readyState||t()},document.getElementsByTagName("head")[0].appendChild(i)}sRAN(),sDisplay(),sSearch();
    console.log('RM Conversion')
}

function togglePreloader(overide) {
    global.preloader.$el = global.preloader.$el || $('#preloader-wrap');
    switch (overide || global.preloader.isActive) {
        case true:
            $('body').removeClass('preloader-open');
            global.preloader.isActive = false;
            TweenMax.to(global.preloader.$el, global.preloader.duration, {
                css: {
                    autoAlpha: 0,
                    display: 'none'
                },
                ease: Power2.easeInOut
            });
            break;
        case false:
            $('body').addClass('preloader-open');
            global.preloader.isActive = true;
            TweenMax.to(global.preloader.$el, global.preloader.duration, {
                css: {
                    autoAlpha: 1,
                    display: 'block'
                },
                ease: Power2.easeInOut
            });
            break;
    }
}

function adrollInit() {
    adroll_adv_id = "HLJA5ZFXUZHGTFLAOUWQOT";
    adroll_pix_id = "S6AXUXZNXBBXNICHNJE2XK";

    var _onload = function () {
        if (document.readyState && !/loaded|complete/.test(document.readyState)) {
            setTimeout(_onload, 10);
            return
        }
        if (!window.__adroll_loaded) {
            __adroll_loaded = true;
            setTimeout(_onload, 50);
            return
        }
        var scr = document.createElement("script");
        var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" :
            "http://a.adroll.com");
        scr.setAttribute('async', 'true');
        scr.type = "text/javascript";
        scr.src = host + "/j/roundtrip.js";
        ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
    };
    if (window.addEventListener) {
        window.addEventListener('load', _onload, false);
    } else {
        window.attachEvent('onload', _onload)
    }

    setTimeout(function () {
        $('.adroll_consent_buttons').find('#adroll_consent_accept').on('click', function () {
            window.__rmcp = [1, 2, 3, 4, 5]
            console.log('Consent tracking - Rakuten [FIRED]');
        })
    }, 4000);
}

function evtClick_error(evt, $target) {
    // if(evt !== false){
    //     evt.preventDefault();
    // }
    var $this = $target || evt.data;
    var $parent = $this.parents(global.errors.selector);

    $this.remove();
    $parent[$parent.find('li').length > 1 ? 'addClass' : 'removeClass']('show-close-all');
    if (!$parent.find('li').length) {
        $parent.addClass('hidden');
    }
}

function evtClick_closeAllErrors(evt) {
    evt.preventDefault();
    $(this).parents('#errors-wrap').find('li').toArray().forEach(function (error) {
        evtClick_error(false, $(error))
    });
    $(global.errors.selector).removeClass('show-close-all');
}

function ERROR(msg) {

    var id = uuid();
    var $wrap = $(global.errors.selector);
    var $list = $wrap.find('ul');

    $wrap.removeClass('hidden');
    $list.append(
        $('<li>', {
            id: id
        }).append(
            $('<a>', {
                href: '#',
                class: 'error-notice'
            }).append([
                $('<span>').text('Attention'),
                $('<span>').text(msg)
            ])
        )
    );

    $wrap[$wrap.find('li').length > 1 ? 'addClass' : 'removeClass']('show-close-all');

    var $error = $('#' + id);
    $error.find('a').on('click', null, $error, evtClick_error);
    setTimeout(function () {
        $('#' + id).length && evtClick_error.call(null, {}, $error);
    }, global.errors.duration);
    // console.error( msg );
}

$('#MAIN-content').find('.close-all-link').on('click', evtClick_closeAllErrors);


// open modal
function closeModal(evt) {
    evt.preventDefault();
    $('body').removeClass('preloader-open');
    var $wrap = $('#modal-wrap');
    TweenMax.to($wrap, 0.3, {
        autoAlpha: 0,
        display: 'none',
        ease: Power2.easeInOut
    });
    $wrap.find('.body').empty();
};

function openModal(cb) {
    $('body').addClass('preloader-open');
    var $wrap = $('#modal-wrap');
    TweenMax.to($wrap, 0.3, {
        autoAlpha: 1,
        display: 'block',
        ease: Power2.easeInOut,
        onComplete: cb.bind(null, $wrap)
    });
    $wrap.find('.backdrop').on('click', closeModal);
};


// custom checkbox
function checkboxHandler(evt) {
    evt.preventDefault();
    this.classList.contains('checked') ? this.classList.remove('checked') : this.classList.add('checked');
}

function resetCheckboxs() {
    resetEventHandler('.checkbox', 'click', checkboxHandler);
}

// custom tooltip
function evtClick_bodyClick(evt) {
    if (!$(evt.target).closest('.tooltip-wrap').length) {
        tooltipHandler.call(evt.data, {}, true);
        $('body').off('click', evtClick_bodyClick);
    }

}

function tooltipHandler(evt, overide) {
    evt.preventDefault();

    let $tooltip = $(this).parent();
    let condition = overide || $tooltip.hasClass('open');

    condition ? $tooltip.removeClass('open') : $tooltip.addClass('open');
    TweenMax.to($tooltip.find('.tooltip-body'), 0.3, {
        autoAlpha: condition ? 0 : 1,
        display: condition ? 'none' : 'block',
        ease: Power2.easeInOut
    });
    $('body').on('click', null, this, evtClick_bodyClick);

}

function resetTooltips() {
    resetEventHandler('.tooltip-link', 'click', tooltipHandler);
}

document.addEventListener("DOMContentLoaded", function () {
    resetCheckboxs();
    resetTooltips();
});

(function () {
    $(".pressLogo").on("click", function () {
        console.log($(this));
        let q = $(this).data("quote");
        $("#quoteSwitch .pressLogo.active").removeClass("active");
        $(".quoteWrap blockquote.active").removeClass("active");

        $(this).addClass("active");
        $('#' + q).addClass("active");
    });
}());