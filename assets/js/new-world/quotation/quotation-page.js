togglePreloader();
$(function () {
    var postcodeRegex = new RegExp("([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})");
    var quotation = {
        addresses: null,
        currentIndex: -1,
        currentStep: null,
        usageRoute: null,
        $wrap: $('#quote-step'),
        $title: $('#quote-step-title'),
        $progress: $('#steps'),
        $actions: $('#actionBtns'),
        templates: {},
        manualInserts: {
            metersGas: null,
            metersElec: null
        },
        wtc: [{
                database: "org_id",
                salesforce: "orgid",
                value: "00D0Y000000YWqc"
            },
            {
                database: "name",
                salesforce: "00N1n00000SB9PS",
                value: null
            },
            {
                database: "email",
                salesforce: "email",
                value: null
            },
            {
                database: "phone",
                salesforce: "phone",
                value: null
            },
            {
                database: "dob",
                salesforce: "00N1n00000SB9PN",
                value: null
            },
            {
                database: "address",
                salesforce: "00N1n00000SB9PI",
                mapping: "quotation_first_line_address",
                value: null
            },
            {
                database: "postcode",
                salesforce: "00N1n00000SB9Ph",
                value: null
            },
            {
                database: "subject",
                salesforce: "subject",
                value: null
            },
            {
                database: "description",
                salesforce: "description",
                value: null
            },
            {
                database: "junifer_customer_id",
                salesforce: "00N1n00000SB9OK",
                mapping: "junifer_customer_id",
                value: null
            },
            // {
            //     database: "dyball_account_id",
            //     salesforce: "00N1n00000SB9NR",
            //     value: null
            // },
            {
                database: "external_system_registration_status",
                salesforce: "00N1n00000SB9Pc",
                value: null
            },
            // {
            //     database: "contact_name",
            //     salesforce: "name",
            //     value: null
            // },
            // {
            //     database: "discount_elec",
            //     salesforce: "00N1n00000SakSP",
            //     value: null
            // },
            // {
            //     database: "discount_gas",
            //     salesforce: "00N1n00000SakSU",
            //     value: null
            // },
            {
                database: "supply_end_date_elec",
                salesforce: "00N1n00000SakSF",
                value: null
            },
            {
                database: "supply_end_date_gas",
                salesforce: "00N1n00000SakSK",
                value: null
            },
            // {
            //     database: "tariff_comparison_rate_gas",
            //     salesforce: "00N1n00000SajO2",
            //     value: null
            // },
            // {
            //     database: "tariff_comparison_rate_elec",
            //     salesforce: "00N1n00000SajO1",
            //     value: null
            // },
            {
                database: "expected_supply_start_date_gas",
                salesforce: "00N1n00000SajNw",
                value: null
            },
            {
                database: "expected_supply_start_date_elec",
                salesforce: "00N1n00000SajNv",
                value: null
            },
            {
                database: "estimated_annual_cost_elec",
                salesforce: "00N1n00000SajNt",
                value: null
            },
            {
                database: "estimated_annual_cost_gas",
                salesforce: "00N1n00000SajNu",
                value: null
            },
            {
                database: "gas_unit_rate",
                salesforce: "00N1n00000SajNs",
                mapping: "unitRate1Gas",
                value: null
            },
            {
                database: "elec_unit_rate",
                salesforce: "00N1n00000SajNr",
                mapping: "unitRate1Elec",
                value: null
            },
            {
                database: "daily_standing_charge_elec",
                salesforce: "00N1n00000SajNp",
                mapping: "standingChargeElec",
                value: null
            },
            {
                database: "daily_standing_charge_gas",
                salesforce: "00N1n00000SajNq",
                mapping: "standingChargeGas",
                value: null
            },
            {
                database: "assumed_annual_consumption_elec",
                salesforce: "00N1n00000SajNn",
                mapping: "quotation_elec_energy_usage",
                value: null
            },
            {
                database: "assumed_annual_consumption_gas",
                salesforce: "00N1n00000SajNo",
                mapping: "quotation_gas_energy_usage",
                value: null
            },
            {
                database: "record_type",
                salesforce: "recordType",
                value: null
            },
            {
                database: "company",
                salesforce: "company",
                value: null
            },
            {
                database: "case_send_date",
                salesforce: "00N1n00000SB9ST",
                value: null
            },
            {
                database: "meter_supply_number_elec",
                salesforce: "00N1n00000Saz7d",
                mapping: "quotation_mpanlower",
                value: null
            },
            {
                database: "meter_supply_number_gas",
                salesforce: "00N1n00000Saz7e",
                mapping: "quotation_mprn",
                value: null
            },
            {
                database: "product_name_gas",
                salesforce: "00N1n00000Saz7g",
                mapping: "gas_product_code",
                value: null
            },
            {
                database: "product_name_elec",
                salesforce: "00N1n00000Saz7f",
                mapping: "elec_product_code",
                value: null
            },
            {
                database: "meter_serial_number_elec",
                salesforce: "00N1n00000Saz7b",
                mapping: "quotation_elec_meter_serial",
                value: null
            },
            {
                database: "meter_serial_number_gas",
                salesforce: "00N1n00000Saz7c",
                mapping: "quotation_gas_meter_serial",
                value: null
            },
            {
                database: "billing_frequency",
                salesforce: "00N1n00000Sb2kW",
                mapping: "desiredPayTypes",
                value: null
            },
            {
                database: "tariff_name",
                salesforce: "00N1n00000Sb2k2",
                mapping: "tariffName",
                value: null
            }
        ],
        form: {
            postcode: null,
            quotation_for: null,
            quotation_address: null,
            quotation_first_line_address: null,
            quotation_dplo_address: null,
            quotation_town_address: null,
            quotation_city_address: null,
            meterTypeElec: null,
            quotation_mpan: null,
            mpan_in_db: null,
            quotation_mpanlower: null,
            quotation_elec_meter_serial: null,
            quotation_gas_meter_serial: null,
            desiredPayTypes: null,
            quotation_mprn: null,
            quotation_pay_energy: null,
            hasExistingSupply: 'no',
            quotation_supplier: null,
            existingSupplierIdElec: null,
            existingTariffNameElec: null,
            gas_selected_supplier: null,
            gas_supplier_id: null,
            gas_selected_tariff: null,
            quotation_hearabout: null,
            quotation_current_energy_usage: null,
            existingkWhsElec: null,
            existingkWhsIntervalElec: null,
            quotation_elec_energy_usage: null,
            quotation_elec_energy_interval: null,
            quotation_gas_energy_usage: null,
            quotation_gas_energy_interval: null,
            quotation_know_energy: null,
            quotation_supplier_id: null,
            quotation_tariff: null,
            selected_tariff_fuelType: null,
            existingSpendElec: null,
            existingSpendIntervalElec: null,
            existingSpendGas: null,
            existingSpendIntervalGas: null,
            typeOfHomeElec: null,
            peopleElec: null,
            bedroomsElec: null,
            errormsg: null,
            junifer_errormsg: null,
            summary: null,
            saasref: null,
            backpage: null,
            monthyear: null,
            paymentType: null,
            tariffName: null,
            newSpend: null,
            unitRate1Elec: null,
            standingChargeElec: null,
            unitRate1Gas: null,
            standingChargeGas: null,
            boiler_cover: null
        },
        steps: [{
                index: 0,
                title: "Enter your postcode below and we'll give you a quote",
                type: 'postcode',
                input: {
                    id: uuid(),
                    type: 'text',
                    name: 'postcode',
                    placeholder: 'POSTCODE',
                    className: 'postcode-field'
                }
            },
            {
                index: 1,
                title: 'Single or dual tariff?',
                type: 'buttons',
                radio: 'tariff_type',
                options: [{
                        id: uuid(),
                        selected: "selected",
                        img: '/assets/images/new-world/quotation/elec-and-gas.png',
                        imgAlt: 'Gas And Electricity',
                        populate: [{
                            field: 'quotation_for',
                            val: 'both'
                        }]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/elec.png',
                        imgAlt: 'Electricity Only',
                        populate: [{
                            field: 'quotation_for',
                            val: 'electricity'
                        }]
                    }
                ]
            },
            // {
            //     index: 2,
            //     title: "Your address",
            //     type: 'address',
            //     both: {
            //         id: uuid(),
            //         name: 'gas-and-elec',
            //         className: 'gas-and-elec-field'
            //     },
            //     gas: {
            //         id: uuid(),
            //         name: 'gas',
            //         className: 'gas-field'
            //     },
            //     mpan: {
            //         id: uuid(),
            //         name: 'mpan',
            //         className: 'mpan-field'
            //     },
            //     serial: {
            //         id: uuid(),
            //         type: 'text',
            //         name: 'serial',
            //         placeholder: 'Enter Your Gas Serial Number',
            //         className: 'serial-field'
            //     }
            // },
            {
                index: 2,
                title: 'How do you pay for your energy?',
                type: 'buttons',
                options: [{
                        id: uuid(),
                        selected: "selected",
                        img: '/assets/images/new-world/quotation/method-monthly.svg',
                        imgAlt: 'Pay Monthly',
                        populate: [{
                                field: 'desiredPayTypes',
                                val: 'directdebit'
                            },
                            {
                                field: 'quotation_pay_energy',
                                val: 'directdebit'
                            },
                            {
                                field: 'paymentType',
                                val: 'pay monthly'
                            }
                        ]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/method-payg.svg',
                        imgAlt: 'Pay As You Go',
                        populate: [{
                                field: 'desiredPayTypes',
                                val: 'prepay'
                            },
                            {
                                field: 'quotation_pay_energy',
                                val: 'prepay'
                            },
                            {
                                field: 'paymentType',
                                val: 'pay as you go'
                            }
                        ]
                    },
                    /*,
                    {
                        id : uuid(),
                        img : '/assets/images/new-world/quotation/method-yearly.svg',
                        imgAlt : 'Pay Yearly',
                        populate : [
                            {
                                field : 'desiredPayTypes',
                                val : 'directdebit'
                            },
                            {
                                field : 'quotation_pay_energy',
                                val : 'directdebit'
                            },
                            {
                                field : 'paymentType',
                                val : 'pay yearly'
                            }
                        ]
                    }*/
                ]
            },
            {
                index: 3,
                title: 'Do you know what your current energy usage is?',
                type: 'buttons',
                options: [{
                        id: uuid(),
                        selected: "selected",
                        img: '/assets/images/new-world/quotation/other.png',
                        imgAlt: "I Don't know",
                        goToAfter: 5,
                        populate: [{
                                field: 'quotation_current_energy_usage',
                                val: 'no'
                            },
                            {
                                field: 'backpage',
                                val: 'bedroom'
                            }
                        ]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/flash.png',
                        imgAlt: 'Enter Usage',
                        goToAfter: 4,
                        populate: [{
                                field: 'quotation_current_energy_usage',
                                val: 'no'
                            },
                            {
                                field: 'backpage',
                                val: 'bedroom'
                            }
                        ]
                    }
                ]
            },
            {
                index: 4,
                title: 'Do you know what your current per year energy usage is?',
                type: 'usage',
                fuelTypes: [{
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/elec.png',
                        imgAlt: 'Electricity',
                        type: 'elec',
                        populate: [{
                                field: 'quotation_current_energy_usage',
                                val: '0'
                            },
                            {
                                field: 'backpage',
                                val: 'usage'
                            }
                        ]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/gas.png',
                        imgAlt: "Gas",
                        type: 'gas',
                        populate: [{
                                field: 'quotation_current_energy_usage',
                                val: '0'
                            },
                            {
                                field: 'backpage',
                                val: 'usage'
                            }
                        ]
                    }
                ]
            },
            {
                index: 5,
                title: 'Which of these best describes your home?',
                goBackTo: 3,
                type: 'buttons',
                options: [{
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/terrace.png',
                        imgAlt: 'Terrace',
                        populate: [{
                            field: "typeOfHomeElec",
                            val: "5"
                        }]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/semi.png',
                        imgAlt: "Semi",
                        selected: "selected",
                        populate: [{
                            field: "typeOfHomeElec",
                            val: "4"
                        }]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/detached.png',
                        imgAlt: "Detached",
                        populate: [{
                            field: "typeOfHomeElec",
                            val: "2"
                        }]
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/Flats.png',
                        imgAlt: "Flats",
                        populate: [{
                            field: "typeOfHomeElec",
                            val: "3"
                        }]
                    }
                ]
            },
            {
                index: 6,
                title: 'How many bedrooms do you have?',
                type: 'buttons',
                options: [{
                        id: uuid(),

                        img: '/assets/images/new-world/quotation/bed-1.png',
                        imgAlt: 'Bed 1',
                        populate: bedTypeClicked,
                        value: 1
                    },
                    {
                        id: uuid(),
                        selected: "selected",
                        img: '/assets/images/new-world/quotation/bed-2.png',
                        imgAlt: "Bed 2",
                        populate: bedTypeClicked,
                        value: 2
                    },
                    {
                        id: uuid(),
                        img: '/assets/images/new-world/quotation/bed-3.png',
                        imgAlt: "Bed 3",
                        populate: bedTypeClicked,
                        value: 3
                    }
                ]
            },
            // {
            //     index: 7,
            //     title: 'How many people live at the address?',
            //     type: 'buttons',
            //     options: [{
            //             id: uuid(),
            //             img: '/assets/images/new-world/quotation/people-1.png',
            //             imgAlt: '1-2',
            //             populate: [{
            //                 field: "peopleElec",
            //                 val: 1
            //             }]
            //         },
            //         {
            //             id: uuid(),
            //             img: '/assets/images/new-world/quotation/people-2.png',
            //             imgAlt: '3-4',
            //             populate: [{
            //                 field: "peopleElec",
            //                 val: 2
            //             }]
            //         },
            //         {
            //             id: uuid(),
            //             img: '/assets/images/new-world/quotation/people-3-4.png',
            //             imgAlt: '3-4',
            //             populate: [{
            //                 field: "peopleElec",
            //                 val: 4
            //             }]
            //         },
            //         {
            //             id: uuid(),
            //             img: '/assets/images/new-world/quotation/people-5-6.png',
            //             imgAlt: '5-6',
            //             populate: [{
            //                 field: "peopleElec",
            //                 val: 6
            //             }]
            //         }
            //     ]
            // },
            {
                index: 7,
                title: 'Quotation Summary',
                type: 'summary',
                goBackTo: function () {
                    // TweenMax.set($('#quote-step-wrap'), {
                    //     padding: "50px 50px 100px 50px"
                    // });
                    return quotation.usageRoute ? 4 : 6;
                }
            },
            {
                index: 8,
                title: 'Your Details',
                type: 'details',
                both: {
                    id: uuid(),
                    name: 'gas-and-elec',
                    className: 'gas-and-elec-field'
                },
                gas: {
                    id: uuid(),
                    name: 'gas',
                    className: 'gas-field'
                },
                mpan: {
                    id: uuid(),
                    name: 'mpan',
                    className: 'mpan-field'
                },
                serial: {
                    id: uuid(),
                    type: 'text',
                    name: 'serial',
                    placeholder: 'Enter Your Gas Serial Number',
                    className: 'serial-field'
                },
                password: {
                    strength: {
                        index: {
                            0: "Worst",
                            1: "Bad",
                            2: "Weak",
                            3: "Good",
                            4: "Strong"
                        }
                    }
                }
            },
            // {
            //     index: 8,
            //     title: 'Signup',
            //     type: 'signup-energy',
            //     password: {
            //         strength: {
            //             index: {
            //                 0: "Worst",
            //                 1: "Bad",
            //                 2: "Weak",
            //                 3: "Good",
            //                 4: "Strong"
            //             }
            //         }
            //     }
            // },
            {
                index: 9,
                method: "POST",
                action: "/quotation/signup_success"
            }
        ],
        eseShareLink: null
    };

    // onload ininitialize
    Promise.all([REQUEST('/templates/quotation/buttons.hbs', 'text'), REQUEST('/templates/quotation/postcode.hbs', 'text'), REQUEST('/templates/quotation/address.hbs', 'text'), REQUEST('/templates/quotation/manual.hbs', 'text'), REQUEST('/templates/quotation/select.hbs', 'text'), REQUEST('/templates/quotation/usage.hbs', 'text'), REQUEST('/templates/quotation/summary.hbs', 'text'), REQUEST('/templates/quotation/details.hbs', 'text'), REQUEST('/templates/quotation/gas-dd.hbs', 'text'), REQUEST('/templates/quotation/welcome.hbs', 'text')])
        .then(function (templates) {
            togglePreloader();
            // store templates
            ['buttons', 'postcode', 'address', 'manual', 'select', 'usage', 'summary', 'details', 'gas-dd', 'welcome'].forEach(function (item, i) {
                quotation.templates[item] = templates[i]
            });


            let postcode = getUrlParameter("postcode");
            if (postcode !== undefined && postcode !== null) {
                let valid = validatePostcode(postcode);
                if (valid) {
                    quotation.form.postcode = valid;
                    cycleSteps(null, 1);
                } else {
                    cycleSteps(1);
                    $(".postcode-field").val(postcode);
                    ERROR("Please provide a valid UK postcode");
                }
            } else {
                cycleSteps(1);
            }

            gtag('event', "begin_checkout");
        });

    // $('#quote-step-back-button').on('click', evtClick_backButton);
    // $('#quoteStepBack').attr('disabled', true);
    $('#quoteStepBack').on('click', evtClick_backButton);
    $('#quoteStepForward').on('click', evtClick_buttonContinue);

    var code = getQueryVariable("rsCode");
    quotation.form.saasref = code;



    $("#saasref").val(code !== false ? code : '');
    code && REQUEST('/user/saascode', 'json', {
        method: 'post',
        body: JSON.stringify({
            code: code
        }),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(function (response) {
        quotation.$wrap.prepend(
            $('<h3>').text(
                code === 'EMMABRADLEY' || code === 'KATIEPRICE' || code === 'EMMADREW' ?
                'Join the Eversmart family today! You will get ' + response.saas_amount + ' in cash!' :
                'Join the Eversmart family today! You will get ' + response.referred_saas_amount + ' cash for switching using  ' + response.ref_name + '\'s code.'
            )
        );
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    // page specific helpers
    function addId(item) {
        item.id = uuid();
        return item;
    }

    function checkAllMatch(data) {
        return data.reduce(function (a, b) {
            return a === b ? a : undefined
        }) !== undefined;
    }

    function checkContentsPopulated(data) {
        return checkAllMatch([true].concat(data.map(function (item) {
            return Array.isArray(item.data) && item.data.length ? true : false
        })));
    }

    function validatePostcode(postcode) {
        postcode = postcode.replace(/\s/g, '').split("+").join("");
        let res = postcodeRegex.exec(postcode);
        if (Array.isArray(res)) {
            return postcode;
        }
        return false;
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }

    function getAdressNameOrNum(data) {
        var result = '';
        data.bnum !== "" && (result += data.bnum + ' ');
        data.subb !== "" && (result += data.subb + (data.bnam !== "" ? " " : ""));
        data.bnam !== "" && (result += data.bnam);
        return result;
    };

    function getAddressPostcode(data) {
        return data.tpco !== "" ? data.tpco : data.ppco !== "" ? data.ppco : data.pcod;
    };

    function constructAddressString(data) {
        return [getAdressNameOrNum(data), data.thor, data.town, getAddressPostcode(data)].join(' ');
    };

    // handlebar helpers
    Handlebars.registerHelper('addDataAttributes', function (data) {
        return data.populate instanceof Function ? '' : ' ' + data.populate.map(function (item) {
            return (item.field + '="' + item.val + '"')
        }).join(' ');
    });

    Handlebars.registerHelper('normaliseType', function (item) {
        switch (item.data.root.type) {
            case 'yearly':
                return 'Per Year';
            case 'monthly':
                return '<span class="monthlyPriceFigure">£' + item.data.root.monthlyPrice + '</span> Per Month';
            default:
                return 'Per Week';
        };
    });

    Handlebars.registerHelper('isActive', function (item) {
        return item.data.root.paymentType === quotation.form.paymentType ? ' active' : '';
    });

    // events
    function evtClick_backButton(evt) {
        console.log(evt);
        clearCurrentStepData();
        quotation.currentStep.goBackTo instanceof Function ? cycleSteps(null, quotation.currentStep.goBackTo()) : quotation.currentStep.goBackTo ? cycleSteps(null, quotation.currentStep.goBackTo) : cycleSteps(-1);
    };

    function evtClick_optionRadio(evt) {
        let $this = $(this);
        let prev = $(".buttonOpt.selected");
        prev.removeClass("selected");
        $this.addClass("selected");


        $('#quoteStepForward').click();
    }

    function evtClick_buttonContinue(evt) {
        switch (quotation.currentStep.type) {
            case "usage":
                evtClick_submitUsage(evt);
                break;
            case "details":
                evtClick_signupSubmit(evt);
                break;
            default:
                let $this = $(this);
                let id = $(`.buttonOpt.selected`).attr("id");

                let option = quotation.currentStep.options.find(function (item) {
                    return item.id === id;
                });

                if (Array.isArray(option.populate)) {
                    option.populate.forEach(function (item) {
                        quotation.form[item.field] = item.val;
                    });
                } else {
                    option.populate(option);
                }

                option.goToAfter ? cycleSteps(null, option.goToAfter) : cycleSteps(1);
                break;
        }
    }

    function evtClick_button(evt) {
        var $this = $(this);
        var option = evt.data.options.find(function (item) {
            return item.id === $this.attr('id')
        });

        if (!option) {
            ERROR('Option Not Found');
            return;
        }

        // If meter type is prepay and payment type selected is DD then error - K always prepay
        if (quotation.form.quotation_meter_type === 'K' && $this.attr('paymenttype') === 'pay monthly') {
            ERROR('It looks like you have a prepay meter - please select \'Pay As You Go\'.');
            return;
        }
        // If meter type is not prepay and the payment type selected is PAYG then error - N always direct debit
        if (quotation.form.quotation_meter_type === 'N' && $this.attr('paymenttype') === 'pay as you go') {
            ERROR('Your meter is not set up to handle prepayments - please select \'Pay Monthly\'.');
            return;

        }

        switch (Array.isArray(option.populate)) {
            case true:
                option.populate.forEach(function (item) {
                    quotation.form[item.field] = item.val;
                });
                break;
            case false:
                option.populate(option);
                break;
        }

        option.goToAfter ? cycleSteps(null, option.goToAfter) : cycleSteps(1);

    }

    function evtClick_submitPostcode(evt) {
        let $postcode = $('#' + evt.data.input.id).val();
        let valid = validatePostcode($postcode);
        if (valid) {
            quotation.form[evt.data.input.name] = valid;
            quotation.$actions.css("display", "flex");
            cycleSteps(1);
        } else {
            ERROR('Please Provide A Valid Postcode');
            return;
        }
    };

    function evtClick_submitSerial(evt) {
        evt.preventDefault();
        var $this = $(this);
        var $parent = $this.parents('.radio-wrap');
        var type = $parent.attr('data-for');
        var serials = $parent.find('.serial-input-wrap input').val();


        if (serials.length < 3) {
            ERROR('Invalid length of serial, please try again.');
            return;
        }


        if ($('.not-sure'))

            togglePreloader();

        Promise.all(serials.split(',').map(function (serial) {
            return findBySerial(type, serial.trim())
        })).then(function (responses) {

            togglePreloader();
            var hasContent = checkContentsPopulated(responses);
            var postcodesMatch = responses.length === 1 ? true : checkAllMatch(responses.map(function (item) {
                return item.data[0].meteringPoElintPostcode
            }));

            if (!hasContent) {
                ERROR('Couldn\'t find a match for the provided serials');
                manualInserts.metersElec = 0;
                manualInserts.mpanLower = 0
            }

            if (!postcodesMatch) {
                ERROR('The postcodes of the provided serials didnt match and as such we cannot continue, you must register different addresses separatly.');
                return;
            }

            $parent.addClass('serial-valid');

            var keyName = type === 'gas' ? 'metersGas' : 'metersElec';
            quotation.manualInserts[keyName] = responses.map(function (item) {
                if (type === 'elec') {

                    item.data[0].meterSerialElec = item.data[0].meterIdSerialNumber;
                    item.data[0].mpanLower = item.data[0].mpanCore;
                } else {
                    item.data[0].meterSerialGas = item.data[0].meterSerialNumber;
                }

                return item.data[0];
            });

            evt.data[keyName] = quotation.manualInserts[keyName];

            var opts = {
                reset: false,
                data: {
                    address: evt.data
                }
            };

            opts.data.skipElec = Array.isArray(opts.data.elecIndexs) || type === 'elec' ? null : true;
            opts.data.skipGas = Array.isArray(opts.data.gasIndexs) || type === 'gas' ? null : true;

            opts[type === 'gas' ? 'metersGasIndexArr' : 'metersElecIndexArr'] = quotation.manualInserts[keyName].map(function (item, i) {
                return i + ''
            });

            addAdressDataToForm(opts);

        });

    };

    function evtClick_SelectElecGas(evt) {
        evt.preventDefault();

        var $this = $(this);
        var isElec = $this.parents('.select-inner').attr('data-type') === 'elec';

        addToSelectedElecGas(evt.data, $this.parents('.select-inner'), $this.attr('data-index'));
        $this.parent().addClass('selected');
        var $wrap = $this.parents('.select-wrap');
        $wrap.find('li').length === $wrap.find('li.selected').length && $wrap.addClass('empty');

        function getIndexs() {
            return $this.parents('.select-list').find('.selected').map(function () {
                return $(this).attr('data-index')
            }).toArray();
        };

        var obj = {
            reset: false,
            data: evt.data
        };

        obj = appendManual(obj);
        obj[isElec ? 'metersElecIndexArr' : 'metersGasIndexArr'] = getIndexs();
        addAdressDataToForm(obj);

    };

    function evtClick_unselectElecGas(evt) {

        var $this = $(this);
        var $inner = $this.parents('.select-inner');
        var type = $inner.attr('data-type');
        var $selectWrap = $inner.find('.select-wrap');
        var $parent = $this.parent();
        var otherListItems = $this.parents('ul').find('li').not($parent);
        var indexs = otherListItems.map(function () {
            return $(this).attr('data-index')
        }).toArray();
        var isElec = type === 'elec';

        $selectWrap.hasClass('empty') && $selectWrap.removeClass('empty');
        $selectWrap.find('li[data-index="' + $parent.attr('data-index') + '"]').removeClass('selected');

        var addObj = {
            reset: false,
            data: evt.data
        };
        addObj[isElec ? 'metersElecIndexArr' : 'metersGasIndexArr'] = indexs;

        addObj = appendManual(addObj);
        !otherListItems.length && $this.parents('.selected-wrap').addClass('empty');
        $this.parents('li').remove();

        (!indexs.length && evt.data.selectData.length === 1) || (!indexs.length && !$('.selected-list').find('li').length) ? addAdressDataToForm({
            reset: true
        }): addAdressDataToForm(addObj);
    }

    function evtClick_submitAddress(evt) {
        cycleSteps(1);
    }

    function evtClick_submitUsage(evt) {
        evt.preventDefault();

        var values = {};

        function validate(val) {
            return val.filter(function (item) {
                return /^\d+$/.test(val)
            }).length === val.length;
        }

        switch (quotation.form.quotation_for) {
            case "both":

                values.gas = quotation.$wrap.find('.fuel-input[name="fuel-gas-input"]').val();
                values.elec = quotation.$wrap.find('.fuel-input[name="fuel-elec-input"]').val();

                if (validate([values.gas]) && validate([values.elec])) {

                    quotation.form.existingkWhsElec = values.elec;
                    quotation.form.existingkWhsIntervalElec = "year";
                    quotation.form.quotation_gas_energy_usage = values.gas;
                    quotation.form.quotation_gas_energy_interval = "year";
                    quotation.form.summary = "kwh_usage";

                    cycleSteps(3);
                } else {
                    ERROR("The provided values either arent populated or arent numbers only.");
                }


                break;
            case "electricity":

                values.elec = quotation.$wrap.find('.fuel-input[name="fuel-elec-input"]').val();
                if (validate([values.elec])) {
                    quotation.form.existingkWhsElec = values.elec;
                    quotation.form.existingkWhsIntervalElec = "year";
                    quotation.form.summary = "kwh_usage";
                    cycleSteps(3);
                } else {
                    ERROR("The provided value either arent populated or arent numbers only.");
                }


                break;
        }
    }

    function evtClick_cardActive(evt) {
        if (this.classList.contains('active')) return;
        $('.summary-card').each(function (i, item) {
            item.classList.contains('active') && item.classList.remove('active')
        });
        this.classList.add('active');
    }

    function evtClick_expandSection(evt) {
        evt.preventDefault();
        var $card = $(this).parents('.summary-card');

        if (!$card.hasClass('expand')) {
            $card.addClass('expand');
            $card.find('.summary-wrap').css({
                'max-height': '3000px'
            });
            $(this).find('span').text('Less info');
        } else {
            $card.removeClass('expand');
            $card.find('.summary-wrap').css({
                'max-height': '0px'
            });
            $(this).find('span').text('More info');
        };
    };

    function evtClick_submitPaymentPlan(evt) {
        evt.preventDefault();
        let type = $(this).parents('.PriceBlock-container').attr('data-type');
        let index = $(this).parents('.PriceBlock-container').attr('data-index');
        quotation.form.monthyear = type === 'yearly' ? 'yearlypay' : type === 'monthly' ? 'monthlypay' : '';
        $(this).parents("form").find("input[type=hidden]").each(function () {
            quotation.form[$(this).attr('name')] = $(this).val();
        });
        cycleSteps(1);
    }

    function evtClick_tabs(evt) {
        evt.preventDefault();
        let $this = $(this);
        let $wrap = $this.parents(".summary-wrap");

        let id;
        console.log(
            $wrap.find(".tab.active")
        )
        let oldId = $wrap.find('.tab.active').data('body-id');

        if ($this.hasClass('active')) {
            return;
        }

        $wrap.find('.tab').removeClass('active');
        $this.addClass('active');
        id = $this.attr('data-body-id');
        console.log(oldId, id);

        TweenMax.set($wrap.find('#' + oldId), {
            display: 'none'
        });
        TweenMax.set($wrap.find('#' + id), {
            display: 'block'
        });
    }

    function validateFields($elements) {
        $elements.each(function (i, el) {
            var $el = $(el);
            var value = $el.val();

            // Worst validation in the world
            switch (el.nodeName === "SELECT" ? 'select' : $el.attr('type')) {
                case 'select':
                    var condition = value === null || value == 0;
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid select fields needs to be populated');
                    break;
                case 'text':
                case 'password':
                case 'date':
                    var condition = value.length <= 1;
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid text|date field needs to be populated');
                    break;
                case 'email':
                    var condition = !validateEmail(value);
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid email field needs to be populated and valid');
                    break;
                case 'tel':
                    var condition = !value.length || !/^[0-9 ]*$/.test(value);
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid number field, it needs to be populated with numbers only');
                    break;
            }
        });
    }

    // step specific functions
    function storeFormValues($form) {

        console.log('storeFormValues');

        $form.find('input:not(.readonly), select').toArray().forEach(function (el) {
            var $el = $(el);
            if ($el.attr('data-wtc')) {
                quotation.form[$el.attr('data-wtc')] = $el.val();
            }
        });

        quotation.form.dob = quotation.form.dob.split('-').reverse().join('/');

        // $form.find('.opt-in-wrap').toArray().forEach(function( el ){
        //     var $el = $( el );
        //     quotation.form[ $el.attr('data-type') ] = $el.find('checkbox').hasClass('checked') ? 1 : 0 ;
        // });
    }


    function evtClick_psrToggle() {
        let current = $(this).parents('form').find('input.prs_flag').val();
        $(this).parents('form').find('input.prs_flag').val(current == 1 ? '' : 1);
    }

    function evtClick_boilerCoverToggle() {
        let current = $(this).parents('form').find('input.boiler_cover').val();
        $(this).parents('form').find('input.boiler_cover').val(current == 1 ? '' : 1);
    }

    function evtClick_signupSubmit(evt) {

        evt.preventDefault();

        console.log('evtClick_signupSubmit');
        console.log("Quote Form Obj: ", quotation.form);

        var $form = $("#customer_details")
        validateFields($form.find('input:not(.hidden).required, select:not(.hidden)'));

        if (!$form.find('.invalid').length) {

            togglePreloader();


            var card_details = {
                'accountNumber': $('input[name="account_number"]').val(),
                'sortCode': $('input[name="sort_code"]').val()
            };

            Promise.all([
                    REQUEST('/quotation/validate_card', 'json', {
                        method: 'post',
                        headers: new Headers({
                            'Content-Type': 'application/json'
                        }),
                        body: JSON.stringify(card_details)
                    })
                ])
                .then(function (res) {

                    switch (res[0].success) {

                        case true:

                            console.log('Passed card validation')
                            storeFormValues($form);

                            REQUEST('/user/enroll_customer', 'json', {
                                    method: 'post',
                                    headers: new Headers({
                                        'Content-Type': 'application/json'
                                    }),
                                    body: JSON.stringify(quotation.form)
                                })
                                .then(function (res) {

                                    console.log(res)

                                    quotation.form['junifer_customer_id'] = res.junifer_id;
                                    quotation.form['cust_info_id'] = res.cust_info_id;

                                    switch (res.error) {

                                        case true:
                                            console.log(res.msg)
                                            ERROR(res.msg);
                                            togglePreloader();
                                            break;

                                        case false:

                                            quotation.eseShareLink = res.ese_share_link;

                                            Promise.all([
                                                REQUEST(window.location.origin + '/user/web_to_case_log', 'none', {
                                                    method: 'post',
                                                    headers: new Headers({
                                                        'Content-Type': 'application/json'
                                                    }),
                                                    body: buildWTC(false, 'database')
                                                }),
                                                REQUEST('https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', { // Send the web to lead form
                                                    method: 'post',
                                                    mode: 'no-cors',
                                                    headers: new Headers({
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    }),
                                                    body: buildWTC(true, 'salesforce')
                                                })
                                            ]).then(function () {
                                                cycleSteps(null, 9);
                                                togglePreloader();
                                                window.scrollTo({
                                                    top: 0,
                                                    behavior: 'smooth'
                                                });
                                            });

                                            console.log('Enrolment success')
                                    }
                                });
                            break;

                        case false:

                            console.log('Failed card validation')
                            ERROR("The provided card details failed validation.");
                            break;
                    }
                });
        }
    }


    function evtClick_signupSubmitPP(evt) {

        evt.preventDefault();

        console.log('evtClick_signupSubmitPP');

        var $form = $(this).parents('form');
        validateFields($form.find('input:not(.hidden).required, select:not(.hidden)'));

        if (!$form.find('.invalid').length) {

            togglePreloader();

            storeFormValues($form);

            REQUEST('/user/enroll_customer', 'json', {
                    method: 'post',
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    }),
                    body: JSON.stringify(quotation.form)
                })
                .then(function (res) {

                    console.log(res)


                    switch (res.error) {

                        case true:
                            console.log(res.msg)
                            ERROR(res.msg);
                            togglePreloader();
                            break;

                        case false:
                            gtag('event', "sign_up", {
                                "method": "Quote Process"
                            });
                            cycleSteps(null, 9);
                            togglePreloader();
                            window.scrollTo({
                                top: 0,
                                behavior: 'smooth'
                            });
                    }
                });
        }
    }

    function evtKeyup_postcode(evt) {
        (evt.keyCode === 13 || evt.which === 13) && evtClick_submitPostcode(evt);
    }

    function evtKeyPress_sortcode(evt) {
        let key = evt.charCode || evt.keyCode || 0,
            text = $(this),
            val = text.val();

        if ((val.length === 2 || val.length === 5) && key !== 8) {
            text.val(val + '-');
        }

        if (val === '000000') {
            text.val(val + '-');
        }

        return (key === 8 || key === 9 || key === 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    }

    function evtKeyup_submitSerial(evt) {

        var $wrap = $(this).parents('.radio-wrap');
        var type = $(this).parents('.radio-wrap').attr('data-for');

        switch (type) {
            case 'gas':
                if (quotation.form.quotation_gas_meter_serial !== $(this).val()) {
                    quotation.form.quotation_gas_meter_serial = null;
                    quotation.form.quotation_mprn = null;
                    quotation.manualInserts.metersGas = null;
                    $wrap.removeClass('serial-valid');
                    showContinueButton();
                }
                break;
            default:
                if (quotation.form.quotation_elec_meter_serial !== $(this).val()) {
                    quotation.form.quotation_elec_meter_serial = null;
                    quotation.form.quotation_mpan = null;
                    quotation.manualInserts.metersElec = null;
                    $wrap.removeClass('serial-valid');
                    showContinueButton();
                }
                break;
        }

        (evt.keyCode === 13 || evt.which === 13) && evtClick_submitSerial.call(this, evt);
    }

    function evtKeyup_passwordStrength() {
        var password = zxcvbn($(this).val());
        var $wrap = quotation.$wrap.find('.password-strength');
        $wrap.find('.value').text(quotation.currentStep.password.strength.index[password.score]);
        TweenMax.to($wrap.find('.bar'), 0.3, {
            width: password.score === 0 ? '0%' : password.score === 1 ? '25%' : password.score === 2 ? '50%' : password.score === 3 ? '75%' : '100%',
            ease: Power2.easeInOut
        });
    }

    function evtBlur_passwordConfirm(evt) {
        var $this = $(this);
        if (quotation.$wrap.find('[name="password"]').val() !== $this.val()) {
            ERROR("Passwords dont match!");
            $this.addClass('invalid');
        } else {
            $this.removeClass('invalid');
        }
    }

    function evtBlur_emailCheck(evt) {
        var $this = $(this);
        var value = $this.val();

        if (!value.length || value.indexOf('@') === -1 || value.indexOf('.') === -1) {
            ERROR("Provided email address is not valid.");
            $this.addClass('invalid');
            return;
        } else {
            $this.removeClass('invalid');
        }

        REQUEST('/user/check_email', 'json', {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({
                email: value
            })
        }).then(function (res) {
            if (res.data.exists) {
                ERROR("Sorry a user already exists with that email, please use another");
                $this.addClass('invalid');
            } else {
                $this.removeClass('invalid');
            }
        });
    }

    // address step specific
    function addToSelectedElecGas(data, $wrap, index) {
        $wrap.find('.selected-wrap').removeClass('empty');

        var $selected = $wrap.find('.selected-list');
        var id = uuid();
        var isElec = $wrap.attr('data-type') === 'elec';

        $selected.append($('<li>', {
            'data-index': index
        }).append([$('<span>').text(data.address[isElec ? 'metersElec' : 'metersGas'][parseInt(index)][isElec ? 'mpanLower' : 'mprn']), $('<a>', {
            class: 'unselect-item',
            href: '#',
            id: id
        })]));
        $('#' + id).on('click', data, evtClick_unselectElecGas);
    };

    function findBySerial(type, serial) {
        return REQUEST('/quotation/find_address_from_meter_serial_both', 'json', {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            }),
            body: JSON.stringify({
                type: type,
                meterSerialNumber: serial
            })
        });
    };

    function addManual(address, inputData) {

        var template = Handlebars.compile(quotation.templates['manual']);
        var $wrap = quotation.$wrap.find('.serial');

        $wrap.append(template(inputData));

        $('.submit-serial').off('click', evtClick_submitSerial);
        $('.manual-entry-field').off('keyup', evtKeyup_submitSerial);

        $('.submit-serial').on('click', address, evtClick_submitSerial);
        $('.manual-entry-field').on('keyup', address, evtKeyup_submitSerial);

    };

    function addSelect(address, selectData, elecIndexs, gasIndexs) {

        var template = Handlebars.compile(quotation.templates['select']);
        var $wrap = quotation.$wrap.find('.select-mpan-mprn');

        $wrap.append(template(selectData));

        selectData.forEach(function (select) {
            var $el = $wrap.find('.select-inner[data-type="' + select.type + '"]');
            $el.find('.select-list').append(address[select.arrIdentifier].map(function (item, i) {
                return $('<li>', {
                    'data-index': i
                }).append($('<a>', {
                    href: '#',
                    class: 'select-link',
                    'data-index': i
                }).text(item[select.fieldIdentifier]))
            }));
            TweenMax.set($wrap, {
                display: 'inline-block'
            });
            var data = {
                address: address,
                selectData: selectData,
                elecIndexs: elecIndexs,
                gasIndexs: gasIndexs
            };
            data.skipElec = Array.isArray(elecIndexs) || select.type === 'elec' ? null : true;
            data.skipGas = Array.isArray(gasIndexs) || select.type === 'gas' ? null : true;
            $el.find('.select-link').on('click', data, evtClick_SelectElecGas);
        });

    };

    function appendManual(opts) {
        if (Array.isArray(quotation.manualInserts.metersElec)) {
            opts.data.skipElec = null;
            opts.data.address.metersElec = quotation.manualInserts.metersElec;
            opts.metersElecIndexArr = quotation.manualInserts.metersElec.map(function (item, i) {
                return i + ''
            });
        };

        if (Array.isArray(quotation.manualInserts.metersGas)) {
            opts.data.skipGas = null;
            opts.data.address.metersGas = quotation.manualInserts.metersGas;
            opts.metersGasIndexArr = quotation.manualInserts.metersGas.map(function (item, i) {
                return i + ''
            });
        };

        return opts;
    };

    function addressChecks(address) {
        switch (quotation.form.quotation_for) {

            // TODO: Blame Austin for this absolute fuckry
            case "both":

                // 0gas & 0elec
                if (!address.metersElec.length && !address.metersGas.length) {
                    addManual(address, {
                        type: 'text',
                        name: 'manual-elec-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'elec',
                        title: 'Electricity'
                    });
                    addManual(address, {
                        type: 'text',
                        name: 'manual-gas-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'gas',
                        title: 'Gas'
                    });
                    return;
                }


                // 1gas & 1elec
                if (address.metersElec.length === 1 && address.metersGas.length === 1) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address
                        },
                        metersElecIndexArr: ['0'],
                        metersGasIndexArr: ['0']
                    });
                    TweenMax.set(quotation.$wrap.find('.submit'), {
                        display: 'block'
                    });
                    return;
                }


                // 0gas & 1elec
                if (address.metersElec.length === 1 && !address.metersGas.length) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address
                        },
                        metersElecIndexArr: ['0']
                    });
                    addManual(address, {
                        type: 'text',
                        name: 'manual-gas-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'gas',
                        title: 'Gas'
                    });
                    return;
                }


                // 0gas & >1elec
                if (address.metersElec.length > 1 && !address.metersGas.length) {
                    addSelect(address, [{
                        type: 'elec',
                        title: 'MPAN',
                        arrIdentifier: 'metersElec',
                        fieldIdentifier: 'mpanLower'
                    }]);
                    addManual(address, {
                        type: 'text',
                        name: 'manual-gas-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'gas',
                        title: 'Gas'
                    });
                    return;
                }


                // 1gas & 0elec
                if (address.metersGas.length === 1 && !address.metersElec.length) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address
                        },
                        metersElecIndexArr: undefined,
                        metersGasIndexArr: ['0']
                    });
                    addManual(address, {
                        type: 'text',
                        name: 'manual-elec-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'elec',
                        title: 'Electricity'
                    });
                    return;
                }


                // >1gas & 0elec
                if (address.metersGas.length > 1 && !address.metersElec.length) {
                    addSelect(address, [{
                        type: 'gas',
                        title: 'MPRN',
                        arrIdentifier: 'metersGas',
                        fieldIdentifier: 'mprn'
                    }]);
                    addManual(address, {
                        type: 'text',
                        name: 'manual-elec-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'elec',
                        title: 'Electricity'
                    });
                    return;
                }


                // >1gas & 1elec
                if (address.metersGas.length > 1 && address.metersElec.length === 1) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address,
                            skipGas: true
                        },
                        metersElecIndexArr: ['0']
                    });
                    addSelect(address, [{
                        type: 'gas',
                        title: 'MPRN',
                        arrIdentifier: 'metersGas',
                        fieldIdentifier: 'mprn'
                    }], ['0']);
                    return;
                }


                // 1gas & >1elec
                if (address.metersElec.length > 1 && address.metersGas.length === 1) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address,
                            skipElec: true
                        },
                        metersGasIndexArr: ['0']
                    });
                    addSelect(address, [{
                        type: 'elec',
                        title: 'MPAN',
                        arrIdentifier: 'metersElec',
                        fieldIdentifier: 'mpanLower'
                    }], null, ['0']);
                    return;
                }


                // >1gas & >1elec
                if (address.metersElec.length > 1 && address.metersGas.length > 1) {
                    addSelect(address, [{
                        type: 'elec',
                        title: 'MPAN',
                        arrIdentifier: 'metersElec',
                        fieldIdentifier: 'mpanLower'
                    }, {
                        type: 'gas',
                        title: 'MPRN',
                        arrIdentifier: 'metersGas',
                        fieldIdentifier: 'mprn'
                    }]);
                    return;
                }


                break;
            case "electricity":

                // 0elec
                if (!address.metersElec.length) {
                    addManual(address, {
                        type: 'text',
                        name: 'manual-elec-entry',
                        id: uuid(),
                        className: 'manual-entry-field',
                        placeholder: 'e.g. 64765463',
                        for: 'elec',
                        title: 'Electricity'
                    });
                    return;
                };

                // 1elec
                if (address.metersElec.length === 1) {
                    addAdressDataToForm({
                        reset: false,
                        data: {
                            address: address,
                            skipGas: true
                        },
                        metersElecIndexArr: ['0']
                    });
                    return;
                };

                // >1elec
                if (address.metersElec.length > 1) {
                    addSelect(address, [{
                        type: 'elec',
                        title: 'MPAN',
                        arrIdentifier: 'metersElec',
                        fieldIdentifier: 'mpanLower'
                    }]);
                    return;
                };

                break;
        };
    };

    // summary step specific
    function buildElements(data) {
        return data.filter(function (item) {
                return typeof item.condition === 'undefined' || item.condition
            })
            .map(function (item) {
                var attrs = {};
                item.target && (attrs.target = item.target);
                item.href && (attrs.href = item.href);
                return $('<' + item.tag + '>', attrs).text(item.text);
            });
    };

    function buildSummary(t, data) {
        data.sort((a, b) => {
            // console.log(a, b);
            return parseFloat(a.price) - parseFloat(b.price);
        });

        // console.log(data);

        var $carousel = $('.PriceBlocks-slider').flickity({
            cellSelector: '.PriceBlock-container',
            initialIndex: 2
        });

        var flkty = $carousel.data('flickity');

        $carousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {
            if (typeof cellIndex == 'number') {
                $carousel.flickity('selectCell', cellIndex);
            }
        });

        $carousel.on('select.flickity', function () {
            flkty.getCellElements().forEach(function (cellElem) {
                cellElem.classList['remove']('is-next');
                cellElem.classList['remove']('is-previous');
            });

            var previousSlide = flkty.slides[flkty.selectedIndex - 1];
            var nextSlide = flkty.slides[flkty.selectedIndex + 1];
        })

        function changeSlideClasses(slide, method, className) {
            slide.flickity('getCellElements').forEach(function (cellElem) {
                cellElem.classList[method](className);
            });
        }

        var modal = new tingle.modal({
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            // cssClass: ["modalContainer"],x
            beforeOpen: function () {
                let cnt = $(".is-selected .infoModalContent").html();
                modal.setContent(cnt);
                $(".tingle-modal-box .tab").on('click', evtClick_tabs);
                $(".tingle-modal-box .clickSwitchNow").on('click', function (evt) {
                    modal.close();
                    $(".is-selected .to_signup_form").click();
                });
            },
            beforeClose: function () {
                // here's goes some logic
                // e.g. save content before closing the modal
                return true; // close the modal
                // return false; // nothing happens
            }
        });

        data.forEach(function (item, i) {
            let template = Handlebars.compile(t);
            let parsed = template(item);
            let idx = 0;
            if (i == 2) {
                idx = 2;
            }
            $carousel.flickity('insert', $(parsed), flkty.cells.length);

            ['tariff', 'benefits', 'important'].forEach(function (key) {
                $('[data-index="' + item.title + '"]').find('.summary-tab-content.' + key).append(buildElements(item[key].statements));
                i !== 0 ? $('[data-index="' + item.title + '"]').removeClass('active') : ''
            });

            // var tariff_title = ['tariff', 'benefits', 'important'].forEach(function (key) {
            //     $('[data-index="' + item.title + '"]').find()
            // });

        });

        $('.optionMoreInfo').on('click', function () {
            modal.open();
        });

        $carousel.flickity('select', 1);
        $carousel.flickity('resize');
        $carousel.flickity('reposition');

        // $('.expand-section').on('click', evtClick_expandSection);
        // $('.summary-card').on('click', evtClick_cardActive);
        $(".to_signup_form").on('click', evtClick_submitPaymentPlan);
        // console.log($(".summary-wrap").find('.tab'))

    };

    // bed step specific
    function bedTypeClicked(option) {
        switch (quotation.form.quotation_for) {
            case "both":
                switch (option.value) {
                    case 1:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 1600 : quotation.form.typeOfHomeElec == 5 ? 1700 : quotation.form.typeOfHomeElec === 4 ? 1800 : 1900;
                        quotation.form.quotation_gas_energy_usage = quotation.form.typeOfHomeElec == 3 ? 7400 : quotation.form.typeOfHomeElec == 5 ? 7600 : quotation.form.typeOfHomeElec === 4 ? 7800 : 8000;
                        break;
                    case 2:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 2800 : quotation.form.typeOfHomeElec == 5 ? 2900 : quotation.form.typeOfHomeElec === 4 ? 3000 : 3100;
                        quotation.form.quotation_gas_energy_usage = quotation.form.typeOfHomeElec == 3 ? 11400 : quotation.form.typeOfHomeElec == 5 ? 11600 : quotation.form.typeOfHomeElec === 4 ? 11800 : 12000;
                        break;
                    case 3:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 4300 : quotation.form.typeOfHomeElec == 5 ? 4400 : quotation.form.typeOfHomeElec === 4 ? 4500 : 4600;
                        quotation.form.quotation_gas_energy_usage = quotation.form.typeOfHomeElec == 3 ? 16400 : quotation.form.typeOfHomeElec == 5 ? 16600 : quotation.form.typeOfHomeElec === 4 ? 16800 : 17000;
                        break;
                };

                quotation.form.quotation_gas_energy_interval = 'year';
                if (!quotation.form.gas_selected_supplier) {
                    quotation.form.gas_selected_supplier = 'British Gas';
                    quotation.form.gas_supplier_id = 5;
                    quotation.form.gas_selected_tariff = 'Standard';
                };
                break;
            case "electricity":
                switch (option.value) {
                    case 1:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 1600 : quotation.form.typeOfHomeElec == 5 ? 1700 : quotation.form.typeOfHomeElec === 4 ? 1800 : 1900;
                        break;
                    case 2:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 2800 : quotation.form.typeOfHomeElec == 5 ? 2900 : quotation.form.typeOfHomeElec === 4 ? 3000 : 3100;
                        break;
                    case 3:
                        quotation.form.quotation_elec_energy_usage = quotation.form.typeOfHomeElec == 3 ? 4300 : quotation.form.typeOfHomeElec == 5 ? 4400 : quotation.form.typeOfHomeElec === 4 ? 4500 : 4600;
                        break;
                };
                break;
        };

        quotation.form.quotation_elec_energy_interval = 'year';
        quotation.form.quotation_know_energy = 'yes';
        quotation.form.bedroomsElec = option.value;

        if (!quotation.form.quotation_supplier) {
            quotation.form.quotation_supplier = 'British Gas';
            quotation.form.quotation_supplier_id = 5;
            quotation.form.quotation_tariff = 'Standard';
        };
    };

    // signup step specific
    function setupStripe() {

        // add template here
        togglePreloader();
        REQUEST('/templates/quotation/yearly-pay.hbs', 'text')
            .then(function (template) {
                togglePreloader();

                template = Handlebars.compile(template);
                quotation.$wrap.find('.extra-fields-wrap').append(template({
                    amount: 213.56
                }));

                // Create a Stripe client.
                var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');
                var card = stripe.elements().create('card', {
                    style: {
                        base: {
                            color: '#32325d',
                            lineHeight: '18px',
                            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                            fontSmoothing: 'antialiased',
                            fontSize: '16px',
                            '::placeholder': {
                                color: '#aab7c4'
                            }
                        },
                        invalid: {
                            color: '#fa755a',
                            iconColor: '#fa755a'
                        }
                    }
                });
                card.mount('#card-element');
                card.addEventListener('change', function (event) {
                    document.getElementById('card-errors').textContent = event.error ? event.error.message : ''
                });
                document.getElementById('payment-form').addEventListener('submit', evtClick_submitPayment);

            });

    };

    function evtClick_submitPayment(event) {
        event.preventDefault();

        // validate our form here before we proceed
        console.log('Need to validate form here!');
        return;

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                document.getElementById('card-errors').textContent = result.error.message;
                return;
            };
            stripeTokenHandler(result.token);
        });
    }

    // Submit the form to stripe with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        form.submit();
    }


    function setupDirectDebit() {
        togglePreloader();
        REQUEST('/templates/quotation/monthly-pay.hbs', 'text')
            .then(function (template) {
                togglePreloader();
                template = Handlebars.compile(template);
                quotation.$wrap.find('.extra-fields-wrap').append(template());
                quotation.$wrap.find('.submit').on('click', evtClick_signupSubmit);
                quotation.$wrap.find('.sort_code').on('keypress', evtKeyPress_sortcode);
            });
    }

    function setupPrepay() {
        togglePreloader();
        REQUEST('/templates/quotation/prepay.hbs', 'text')
            .then(function (template) {
                togglePreloader();
                template = Handlebars.compile(template);
                quotation.$wrap.find('.extra-fields-wrap').append(template());
                quotation.$wrap.find('.submit').on('click', evtClick_signupSubmitPP);
            });
    }

    function buildWTC(serialize, location) {

        let form = {};

        let startDate = new Date();
        startDate.setDate(startDate.getDate() + 21);
        let start_date = startDate.getDate() + '-' + startDate.getMonth() + 1 + '-' + startDate.getFullYear();

        let endDate = new Date();
        endDate.setDate(endDate.getDate() + 365);
        let end_date = endDate.getDate() + '-' + endDate.getMonth() + 1 + '-' + endDate.getFullYear();

        let caseDate = new Date();
        caseDate.setDate(caseDate.getDate());
        let caseSentDate = caseDate.getDate() + '-' + caseDate.getMonth() + 1 + '-' + caseDate.getFullYear();

        form.dob = quotation.form.dob.replace(/\//g, '-');
        form.subject = 'Registration Success - Junifer Account ID: ' + quotation.form.junifer_customer_id + ' - Success';
        form.description = 'Registration Success for ' + quotation.form.forename + ' ' + quotation.form.surname;

        switch (location) {

            case 'salesforce':

                quotation.wtc.forEach(function (item) {
                    if (item.salesforce in quotation.form || item.mapping in quotation.form) {
                        form[item.salesforce] = quotation.form[item.database] !== undefined ? quotation.form[item.database] : quotation.form[item.mapping];
                    }
                });

                form.orgid = '00D0Y000000YWqc';
                form['00N1n00000SB9PS'] = quotation.form.title + ' ' + quotation.form.forename + ' ' + quotation.form.surname;
                form['00N1n00000SB9Ph'] = quotation.form.postcode;
                form['00N1n00000SB9Pc'] = 'Registration Success';
                form['00N1n00000SajNv'] = start_date; // Expected supply start date Electricity
                form['00N1n00000SajNw'] = start_date; // Expected supply start date Gas
                form['00N1n00000SakSF'] = end_date; // Supply End Date Electricity
                form['00N1n00000SakSK'] = end_date; // Supply End Date Gas
                form['00N1n00000SB9ST'] = caseSentDate;
                form.recordType = '0121n0000007GkH';
                break;

            case 'database':

                quotation.wtc.forEach(function (item) {
                    if (item.database in quotation.form || item.mapping in quotation.form) {
                        form[item.database] = quotation.form[item.database] !== undefined ? quotation.form[item.database] : quotation.form[item.mapping];
                    }
                });

                form.org_id = '00D0Y000000YWqc';
                form.name = quotation.form.title + ' ' + quotation.form.forename + ' ' + quotation.form.surname;
                form.external_system_registration_status = 'Registration Success';
                form.expected_supply_start_date_elec = start_date;
                form.expected_supply_start_date_gas = start_date;
                form.supply_end_date_elec = end_date;
                form.supply_end_date_gas = end_date;
                form.case_send_date = caseSentDate;
                form.record_type = '0121n0000007GkH';
                break;
        }

        console.log(form)
        return serialize ? serializeJSON(form) : JSON.stringify(form);
    }

    // cycle process functionality
    function addDynamicContent(data) {
        switch (data.type) {
            case "details":
                togglePreloader();
                REQUEST('/quotation/address_lookup', 'json', {
                        method: 'post',
                        headers: new Headers({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }),
                        body: 'postcode=' + quotation.form.postcode
                    })
                    .then(function (json) {
                        togglePreloader();
                        quotation.addresses = json;
                        quotation.addresses.elec = quotation.addresses.elec.map(addId);
                        quotation.addresses.gas = quotation.addresses.gas.map(addId);
                        addOptions('#' + data.both.id, quotation.addresses.elec, 'Please Select Address', false);
                        switch (quotation.form.paymentType) {
                            case 'pay monthly':
                                setupDirectDebit();
                                break;
                            default:
                                setupPrepay();
                                break;
                        }
                    });
                break;
            case "summary":
                togglePreloader();
                try {
                    Promise.all([
                        REQUEST('/templates/quotation/summary-option.hbs', 'text'),
                        REQUEST('/quotation/summary', 'json', {
                            method: 'post',
                            headers: new Headers({
                                'Content-Type': 'application/json'
                            }),
                            body: JSON.stringify(quotation.form)
                        }),
                    ]).then(function (res) {
                        console.log(res);
                        togglePreloader();
                        if (typeof res[1] == "object" && res[1] != undefined && res[1] != null) {
                            buildSummary(res[0], res[1].data.new_data);
                        } else {
                            $('.PriceBlocks-slider').html("<h3>No results found</h3><p>Please go back and refine you criteria</p>");
                            ERROR("There were no tariffs matching your search criteria");
                        }
                    });
                } catch (e) {
                    console.log(e);
                }
                break;
            case "welcome":
                $('#referral-link').val(quotation.eseShareLink);
                break;
            case "signup-energy":
                quotation.$wrap.find('[data-field-name="address-first-line"]').text(quotation.form.quotation_first_line_address);
                quotation.$wrap.find('[data-field-name="city"]').text(quotation.form.quotation_town_address);
                quotation.$wrap.find('[data-field-name="county"]').text(quotation.form.quotation_city_address);
                quotation.$wrap.find('[data-field-name="postcode"]').text(quotation.form.postcode);
                switch ("monthlypay" || quotation.form.monthyear) {
                    case "monthlypay":

                        switch (quotation.form.paymentType) {
                            case 'pay monthly':
                                setupDirectDebit();
                                break;
                            default:
                                setupPrepay();
                                break;
                        }
                        break;

                    case "yearlypay":
                        setupStripe();
                        break;
                }
        }
    }

    function clearCurrentStepData() {
        switch (quotation.currentStep.type) {
            case "buttons":
                quotation.currentStep.options.forEach(function (step) {
                    var overide = null;
                    if (step.populate instanceof Function) {
                        overide = [{
                                field: 'quotation_elec_energy_usage'
                            },
                            {
                                field: 'quotation_gas_energy_usage'
                            },
                            {
                                field: 'quotation_gas_energy_interval'
                            },
                            {
                                field: 'gas_selected_supplier'
                            },
                            {
                                field: 'gas_supplier_id'
                            },
                            {
                                field: 'gas_selected_tariff'
                            },
                            {
                                field: 'quotation_elec_energy_interval'
                            },
                            {
                                field: 'quotation_know_energy'
                            },
                            {
                                field: 'quotation_current_energy_usage'
                            },
                            {
                                field: 'bedroomsElec'
                            },
                            {
                                field: 'summarypage'
                            },
                            {
                                field: 'quotation_supplier'
                            },
                            {
                                field: 'quotation_supplier_id'
                            },
                            {
                                field: 'quotation_tariff'
                            },
                        ];
                    }
                    (overide || step.populate).forEach(function (item) {
                        quotation.form[item.field] = null;
                    });
                });
                break;
            case "postcode":
                quotation.form[quotation.currentStep.input.name] = null;
                break;
            case "address":
                quotation.form.quotation_address = null;
                quotation.form.quotation_first_line_address = null;
                quotation.form.quotation_city_address = null;
                quotation.form.quotation_town_address = null;
                quotation.form.meterTypeElec = null;
                quotation.form.quotation_mpanlower = null;
                quotation.form.quotation_mpan = null;
                quotation.form.quotation_elec_meter_serial = null;
                quotation.form.quotation_mprn = null;
                quotation.form.quotation_gas_meter_serial = null;
                quotation.addresses = null;
                break;
            case "usage":
                quotation.usageRoute = null;
                quotation.form.existingkWhsElec = null;
                quotation.form.existingkWhsIntervalElec = null;
                quotation.form.quotation_gas_energy_usage = null;
                quotation.form.quotation_gas_energy_interval = null;
                quotation.form.summary = null;
                break;
        }
    }

    function addListeners(data) {
        switch (data.type) {
            case "buttons":
                quotation.$wrap.find('.buttonOpt').on('click', data, evtClick_optionRadio);
                break;
            case "postcode":
                quotation.$wrap.find('.submit').on('click', data, evtClick_submitPostcode);
                quotation.$wrap.find('#' + data.input.id).on('keyup', data, evtKeyup_postcode);
                break;
            case "address":
                quotation.$wrap.find('#' + data.both.id).on('change', data, evtChange_address);
                // quotation.$wrap.find('.submit').on('click', data, evtClick_submitAddress);
                break;
            case "usage":
                quotation.usageRoute = true;
                quotation.$wrap.find('.fuel-input').on('keyup', function (evt) {
                    (evt.keyCode === 13 || evt.which === 13) && evtClick_submitUsage.call(this, evt);
                });
                // quotation.$wrap.find('.submit-usage').on('click', data, evtClick_submitUsage);
                break;
            case "details":
                resetCheckboxs();
                // quotation.$wrap.find('#' + data.both.id).on('change', data, evtChange_address);
                quotation.$wrap.find('#' + data.both.id).on('change', data, evtChange_address);
                quotation.$wrap.find('[name="email"]').on('blur', evtBlur_emailCheck);
                quotation.$wrap.find('[name="password"]').on('keyup', evtKeyup_passwordStrength);
                quotation.$wrap.find('[name="password-confirm"]').on('blur', evtBlur_passwordConfirm);
                // quotation.$wrap.find('.submit').on('click', data, evtClick_signupSubmit);
                quotation.$wrap.find('.checkbox.psr').on('click', data, evtClick_psrToggle);
                quotation.$wrap.find('.checkbox.boiler').on('click', data, evtClick_boilerCoverToggle);
                break;
        }
    }

    function changeStep(next) {
        if (next.index === 9) {
            // Push to success page
            var url = `${next.action}`;
            var form = $('<form action="' + url + '" method="post">' +
                '<input type="text" name="ref" value="' + quotation.eseShareLink + '" />' +
                '<input type="text" name="uid" value="' + quotation.form.cust_info_id + '" />' +
                '</form>');
            $('body').append(form);
            form.submit();
            return;
        }
        quotation.$wrap.empty();
        quotation.currentStep = next;

        next.index === 0 || next.index === 11 ? TweenMax.set($('#quote-step-back-button'), {
            display: 'none'
        }) : TweenMax.set($('#quote-step-back-button'), {
            display: 'inline-block'
        });
        next.type === 'usage' && (next.elecOnly = quotation.form.quotation_for === 'electricity');

        var template = Handlebars.compile(quotation.templates[next.type]);

        if (next.index == 8) {
            next.quote = {
                tariff: quotation.form.tariffName,
                price: Math.floor(quotation.form.newSpend / 12),
                year: quotation.form.newSpend,
                boiler_cover_available: quotation.form.boiler_cover_available === "true" ? true : false
            }
        }

        // console.log(next.quote);

        quotation.$wrap.append(template(next));
        quotation.$title.text(next.title);
        addDynamicContent(next);
        if (next.index === 0 || next.index === 9) {
            quotation.$actions.hide();
        } else {
            quotation.$actions.css("display", "flex");
        }

        if (next.index === 9) {
            quotation.$progress.hide();
        }

        if (next.type === 'summary') {
            $('#quoteStepForward').attr('disabled', true);
        } else {
            $('#quoteStepForward').attr('disabled', false);
        }

        if (next.index < 7) {
            quotation.$progress.find("#quote").addClass("active");
            quotation.$progress.find("#quote").removeClass("done");
        }

        if (next.index == 7) {
            quotation.$progress.find("#details").removeClass("active");
            quotation.$progress.find("#quote").addClass("done");
        }

        if (next.index == 8) {
            quotation.$progress.find("#details").addClass("active");
        }

        if (next.index == 9) {
            quotation.$progress.find("#details").addClass("done");
            quotation.$progress.find("#details").addClass("active");
        }

        addListeners(next);
    }

    function cycleSteps(increment, skipTo) {

        let index = skipTo ? skipTo : quotation.currentIndex + increment;
        let step = quotation.steps.find(function (item) {
            return item.index === index
        });

        if (!step) {
            ERROR('Quotation step is invalid.');
            return;
        }
        changeStep(step);
        //   console.log(GA);
        quotation.currentIndex = index;
    }

    // New APIS
    function addOptions(selector, arr, placeholder) {
        $(selector).html(
            [$('<option selected disabled>', {
                value: '0'
            }).text(placeholder)].concat(arr.map(function (item) {
                return $('<option>', {
                    value: item.id || uuid()
                }).text(buildFullAdd(item))
            }))
        );
    };

    function buildFullAdd(i) {
        // console.log(i);
        $a = [];
        $a.push(i.address1);
        if ("address2" in i) {
            $a.push(i.address2);
        }
        $a.push(i.town);
        $a.push(i.county);
        $a.push(i.meteringPointPostcode);
        return $a.join(' ');
    }

    function evtChange_address(evt) {
        addAdressDataToForm({
            reset: true
        });
        $('#gasSpecific').remove();

        $t = $(this);
        let elec = quotation.addresses.elec.find(e => e.id == $t.val());
        let gas = quotation.addresses.gas.find(g => buildFullAdd(g) == buildFullAdd(elec));
        if (gas == undefined && quotation.form.quotation_for === "both") {
            // Gas Meter not found
            // Expose gas address dropdown
            console.log("Gas Address mismatch", "Exposing Gas Address list");
            let template = Handlebars.compile(quotation.templates['gas-dd']);
            $t.parents(".form-group").after(template(quotation.currentStep));
            addOptions(`#${quotation.currentStep.gas.id}`, quotation.addresses.gas, 'Please Select Address', false);
            $(`#${quotation.currentStep.gas.id}`).on('change', (evt) => {
                let v = $(`#${quotation.currentStep.gas.id}`).val();
                gas = quotation.addresses.gas.find(g => {
                    return g.id == v;
                });
                console.log("Gas Address Manually Specified");
                addAdressDataToForm({
                    reset: false,
                    data: {
                        e: elec,
                        g: gas
                    }
                });
            });
        } else {
            // Gas & Elec found
            console.log("Elec & Gas Address Match or Gas Not Required");
            addAdressDataToForm({
                reset: false,
                data: {
                    e: elec,
                    g: quotation.form.quotation_for === "both" ? gas : null
                }
            });
        }
    }

    function addAdressDataToForm(opts = NULL) {
        quotation.form.quotation_address = opts.reset ? null : buildFullAdd(opts.data.e);
        quotation.form.quotation_meter_type = opts.reset ? null : opts.data.e.meterTypeId;
        quotation.form.quotation_first_line_address = opts.reset ? null : opts.data.e.address1;
        quotation.form.quotation_city_address = opts.reset ? null : opts.data.e.county;
        quotation.form.quotation_town_address = opts.reset ? null : opts.data.e.town;

        if (opts.reset || !opts.data.skipElec) {
            quotation.form.meterTypeElec = opts.reset ? null : opts.data.e.meterTypeId;
            // quotation.form.quotation_mpanlower = opts.reset ? null : hasMetersElec ? joinProps(opts.data.address.metersElec, (opts.data.elecIndexs || opts.metersElecIndexArr), 'mpanLower') : null;
            quotation.form.quotation_mpan = opts.reset ? null : opts.data.e.mpanCore;
            quotation.form.quotation_elec_meter_serial = opts.reset ? null : opts.data.e.meterIdSerialNumber;
        }

        if (opts.reset || !opts.data.skipGas && opts.data.g !== null) {
            quotation.form.quotation_mprn = opts.reset ? null : opts.data.g.mprn;
            quotation.form.quotation_gas_meter_serial = opts.reset ? null : opts.data.g.meterSerialNumber;
        }

        // console.log(quotation.form);
    }
});
