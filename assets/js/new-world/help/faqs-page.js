$(function(){

    var faqs = {
        bodyOpen : false,
        type : null,
        $wrap : $('#faqs-page-wrap'),
        $topics : $('#faqs-page-wrap').find('.topics-list'),
        $topicInfo : $('.topic-info-wrap'),
        templates : {},
        data : [
            {
                id : uuid(),
                service : "energy",
                title : "Supply Loss",
                image : "/assets/images/new-world/help/faqs/SupplyLoss.svg",
                questions : [
                    {
                        title : "My gas/electricity doesn’t come back on after topping up. What should I do?",
                        content : [
                            {
                                tag : "p",
                                text : "If you have run out of credit (including emergency credit), your supply will be cut off and you will need to top up. For your safety, your supply will not return automatically once you are back in credit. Follow the instructions below to get back on supply:"
                            },
                            {
                                tag : "p",
                                text : "For electricity:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Go to your electric meter and check that you are at least £1 back in positive credit.',
                                    'Press the blue A button or the red B button.',
                                    'Follow the instructions that appear on the screen.',
                                    'Your electricity supply should now be restored.'
                                ]
                            },
                            {
                                tag : "p",
                                text : "For gas:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Check that all gas appliances (e.g. your cooker) are switched off and are safe.',
                                    'Go to your gas meter and check that you are at least £1 back in positive credit.',
                                    'Press & hold the blue button in the middle of the meter, until you see a red light flashing in the bottom left.',
                                    'After around 15 seconds, you should hear the valve opening inside your meter. Your gas supply should now be restored.'
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Switching",
                image : "/assets/images/new-world/help/faqs/Switching.svg",
                questions : [
                    {
                        title : "If I have a debt, can I switch to Eversmart Energy?",
                        content : [
                            {
                                tag : "p",
                                text : "Yes. Eversmart Energy will allow domestic prepayment customers to switch with a debt of £500 or less."
                            }
                        ]
                    },
                    {
                        title : "Do I need to let my existing supplier know that I would like to switch to Eversmart Energy?",
                        content : [
                            {
                                tag : "p",
                                text : "No. We will contact your current supplier on your behalf to let them know that you’re switching. If you have a debt on your prepayment meter, please clear it beforehand. If your debt is less than £500, we should be able to transfer this across to us, but you will need to contact your existing supplier to progress this."
                            }
                        ]
                    },
                    {
                        title : "How quickly will my switch happen and when can I expect to be supplied by Eversmart Energy?",
                        content : [
                            {
                                tag : "p",
                                text : "Your switch should take place within 21 days. We will write to you to let you know when this will happen."
                            }
                        ]
                    },
                    {
                        title : "Should I expect to get a bill from Eversmart Energy?",
                        content : [
                            {
                                tag : "p",
                                text : "You won’t receive a bill, but we will send you an annual statement showing your current tariff, usage and payments and an estimate of your usage for the following year."
                            }
                        ]
                    },
                    {
                        title : "If I have a debt, am I able to switch away from Eversmart Energy?",
                        content : [
                            {
                                tag : "p",
                                text : "Yes. Eversmart Energy will usually allow a switch for a domestic prepayment customer with a debt of £500 or less. If you have a debt over £500 and you wish to switch, please contact our collections team to discuss your options."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Topping Up",
                image : "/assets/images/new-world/help/faqs/ToppingUp.svg",
                questions : [
                    {
                        title : "How do I top up?",
                        content : [
                            {
                                tag : "p",
                                text : "Once you’ve activated your new key you can buy credit at a Paypoint, Payzone or Post Office outlet and add it to your meter. You can buy up to £49 credit at a time. You can visit the following site to confirm where your nearest outlet is: www.paypoint.com"
                            }
                        ]
                    },
                    {
                        title : "How do I transfer credit to my meter?",
                        content : [
                            {
                                tag : "p",
                                text : "Simply insert your key into your meter. The screen will briefly show the amount of credit on the key. To ensure your credit is transferred successfully, please leave your key inserted for at least 5 seconds."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Replacement Key",
                image : "/assets/images/new-world/help/faqs/ReplacementKey.svg",
                questions : [
                    {
                        title : "When should I expect to receive my Eversmart Energy prepayment meter key?",
                        content : [
                            {
                                tag : "p",
                                text : "You’ll receive your Eversmart Energy key before you switch to us. We’ll send you a letter or an email to let you know when your switch is due to take place. In the meantime, it’s best to top up little and often so that your credit dwindles ready for you to begin using your Eversmart Energy key."
                            }
                        ]
                    },
                    {
                        title : "How do I activate my key?",
                        content : [
                            {
                                tag : "p",
                                text : "Before you buy credit on your new key, you’ll need to activate it. Just insert it into your meter for at least 30 seconds and it’ll update your details."
                            }
                        ]
                    },
                    {
                        title : "What do I do if I've lost or broken my key?",
                        content : [
                            {
                                tag : "p",
                                text : "Looking after your key is important to ensure you stay on supply, but we know that there may be the odd occasion when you need a replacement. Please call us immediately if this happens and we will arrange for you to collect one from a local outlet – we’ll give you a reference number that you’ll need to provide to the shop. If you can’t get to a shop, we’ll arrange for a new key to be posted to you, but this may take up to 3 working days to arrive."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Energy Usage",
                image : "/assets/images/new-world/help/faqs/EnergyUsage.svg",
                questions : [
                    {
                        title : "My energy usage is too high – what can I do?",
                        content : [
                            {
                                tag : "p",
                                text : "If you think you are using too much energy, you can find independent tips and advice on the Energy Saving Trust website: http://www.energysavingtrust.org.uk"
                            },
                            {
                                tag : "p",
                                text : "The government website also has information about grants for energy efficiency improvements: https://www.gov.uk/energy-grants-calculator"
                            },
                            {
                                tag : "p",
                                text : "For advice regarding fuel poverty and debt, or impartial advice about switching energy suppliers, you can contact your local Citizen’s Advice Bureau: https://www.citizensadvice.org.uk/about-us/contact-us/uk"
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Gas / Electric Meters",
                image : "/assets/images/new-world/help/faqs/SmartMeters.svg",
                questions : [
                    {
                        title : "How does my emergency credit work?",
                        content : [
                            {
                                tag : "p",
                                text : "Your prepayment meter has an emergency credit facility that allows you to ‘borrow’ money from the meter if your credit runs low. Our standard emergency credit amount is £5.00. This credit should only be used in an emergency. You’ll have to pay back to the meter, any emergency credit you borrow along with any debt owed. This will also include standing charges that may have built up while you’ve been using the emergency supply."
                            },
                            {
                                tag : "h2",
                                text : "Electricity"
                            },
                            {
                                tag : "p",
                                text : "What your meter will display: Display number"
                            },
                            {
                                tag : "p",
                                text : "What this means:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'A or 1: How much you have left before your emergency supply runs out, e.g. £2.50E - the ‘E’ means emergency credit mode.',
                                    'B or 2: How much you need to put onto your meter to get it back to normal, e.g. £7.00'
                                ]
                            },
                            {
                                tag : "h2",
                                text : "Gas"
                            },
                            {
                                tag : "p",
                                text : "To use the emergency credit just put your card into the meter and press the red button A. Your meter will then display 'Emergency credit in use'"
                            }
                        ]
                    },
                    {
                        title : "Tips and information on using your prepayment meter:",
                        content : [
                            {
                                tag : "p",
                                text : "Only ever use your Eversmart Energy registered key – this ensures that you stay on our cheapest tariff rate and that we receive your payments. Remember to top up regularly, which you can do at any PayPoint outlet or Payzone If you lose your key or it is damaged, please contact us immediately."
                            },
                            {
                                tag : "p",
                                text : "Going on holiday? Remember to top up enough credit to cover your daily standing charge If you have a debt, your debt recovery rate will be set to £3.60 as a minimum."
                            },
                            {
                                tag : "p",
                                text : "Moving House? Don’t forget to let us know by completing one of our home movers forms. If you have a debt on your domestic prepayment meter, you may be able to switch suppliers under an industry process called Debt Assignment Protocol (DAP). Please see our FAQs for more information - also be sure to contact your chosen new supplier. We also have a Priority Service Register (PSR), dedicated to offering extra help. If you need us to do anything differently – such as write to you in Braille, send your bills to a relative or carer, or help you read your meter – just let us know by registering for the PSR. For more information regarding the PSR, please follow this link: Ofgem - Priority Services Register FAQs"
                            }
                        ]
                    },
                    {
                        title : "Do I need to provide meter readings for my prepayment meter?",
                        content : [
                            {
                                tag : "p",
                                text : "Eversmart Energy will receive a meter reading each time you top up your meter, however there may be occasions where a meter reading would be helpful. If we have a tariff change, you move in or out, or switch provider, giving us readings will ensure your account remains accurate."
                            }
                        ]
                    },
                    {
                        title : "I can’t reach my meter and I find it difficult to top up. If I switch to Eversmart Energy are you able to move it for me?",
                        content : [
                            {
                                tag : "p",
                                text : "This is something you should discuss with your current supplier as it’s important you have the ability to top up your meter and stay on supply. The switch to Eversmart Energy will take at least 21 days and with something as important as keeping yourself on supply, this should be addressed immediately."
                            }
                        ]
                    },
                    {
                        title : "How do I take a meter reading?",
                        content : [
                            {
                                tag : "p",
                                text : "Electricity meters: To read your key meter press the blue button until ‘screen H’ appears. Your reading will then be displayed. If you have an Economy7 meter it’ll also display screens J and L. Please take a note of these readings too"
                            },
                            {
                                tag : "p",
                                text : "Gas meters: If your gas meter has dials, please note the numbers in black. If your meter has an LED (digital) screen, press the black button twice and then press the red button to show the ‘Meter Index’ screen. The number displayed is your meter reading."
                            }
                        ]
                    },
                    {
                        title: "What is a meter serial number and where can I find it?",
                        content : [
                            {
                                tag : "p",
                                text : "Your meter serial number or " +
                                    "MSN is a unique reference number that is used to identify your meter." +
                                    " The meter serial number is usually a combination of bother letters and numbers and can" +
                                    " usually be found on your energy bill typically in the meter reads section. " +
                                    "It can also be found engraved on your meter or printed on to a sticker." +
                                    "Below are some examples of meter serial numbers. If you still struggling to locate your meter serial number please" +
                                    "do not hesitate to chat to one of our friendly team who will be happy to help you out."
                            },
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Smart Meters Installation",
                image : "/assets/images/new-world/help/faqs/SmartInstallation.svg",
                questions : [
                    {
                        title : "Do I have to pay for my smart meter when I sign up?",
                        content : [
                            {
                                tag : "p",
                                text : "No. Your smart meter is supplied and fitted completely for free."
                            }
                        ]
                    },
                    {
                        title : "Who will install my smart meter?",
                        content : [
                            {
                                tag : "p",
                                text : "Your smart meter will be fitted and tested by a qualified engineer."
                            }
                        ]
                    },
                    {
                        title : "How long does it take to fit a smart meter?",
                        content : [
                            {
                                tag : "p",
                                text : "It usually takes up to one hour per meter (gas and/or electricity). If you are a dual-fuel customer, it should take no longer than 2 hours."
                            },
                            {
                                tag : "p",
                                text : "Please note that dual-fuel customers will need two smart meters – one for your gas supply and one for your electricity. We will aim to install both in the same visit. You will only need one in-home display."
                            }
                        ]
                    },
                    {
                        title : "If I switch away from Eversmart in the future, will I be charged for having my smart meter removed?",
                        content : [
                            {
                                tag : "p",
                                text : "No. Your new supplier should be able to use the same meter. If for some reason your new supplier needs to change your meter, they cannot charge you for it."
                            }
                        ]
                    },
                    {
                        title : "Can I keep using my old my old key/card for topping up?",
                        content : [
                            {
                                tag : "p",
                                text : "No, but you will receive a new key when your smart meter is fitted. You will then be able to top up via SMS, our app, online or over the phone."
                            }
                        ]
                    },
                    {
                        title : "Do I need the Internet for my smart meter to work?",
                        content : [
                            {
                                tag : "p",
                                text : "No. Your smart meter uses a built-in SIM card to communicate with the supplier. The smart meter also connects wirelessly with your in-home device – it does not use Wi-Fi."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Moving Home",
                image : "/assets/images/new-world/help/faqs/MovingHome.svg",
                questions : [
                    {
                        title : "I'm moving home. What do I need to let you know?",
                        content : [
                            {
                                tag : "p",
                                text : "If you’re moving home, or have moved into a property which we supply via a prepayment meter, you will need to provide us with some details which you can do by filling out our home movers form. Alternatively you can call us on 0330 102 7901"
                            }
                        ]
                    },
                    {
                        title : "I have just moved into a property and there is a prepayment meter and no key. How do I top up?",
                        content : [
                            {
                                tag : "p",
                                text : "We can arrange for you to be able to collect a new key from a local outlet – we’ll need to give you a reference number to be able to do this so will need to ensure we process your new account with Eversmart Energy and can then give you the information you will require. If you can’t get to a shop, we can send your key in the post but this may take up to 3 working days. If you have no credit left, we may be able to arrange for an emergency appointment to ensure you remain on supply."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Warm Home Discount",
                image : "/assets/images/new-world/help/faqs/WarmHomeDiscount.svg",
                questions : [
                    {
                        title : "Do you offer support and help via the warm home discount?",
                        content : [
                            {
                                tag : "p",
                                text : "No, we don’t currently offer the warm home discount."
                            }
                        ]
                    },
                    {
                        title : "Warm Home Discount Scheme",
                        content : [
                            {
                                tag : "p",
                                text : "The scheme was introduced by the Government in 2010 in conjunction with the large energy suppliers to support elderly householders receiving benefits to heat their homes. Because of the significant cost of participating in the Warm Home scheme, only the larger players are able to afford this without significantly increasing the bills of their own customers. Therefore energy suppliers with fewer than 250,000 customers, like Eversmart Energy, are not obligated to take part. Any customer is able to cancel their contract during the sign-up procedure, and we are happy to process this in order to support individuals who wish to switch to another supplier so that they are able to get the Warm Home discount."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Eversmart Schemes",
                image : "/assets/images/new-world/help/faqs/EversmartSchemes.svg",
                questions : [
                    {
                        title : "Do Eversmart provide any schemes?",
                        content : [
                            {
                                tag : "p",
                                text : "Eversmart do not currently offer any schemes at this moment in time."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Complaints",
                image : "./assets/images/new-world/help/faqs/Complaints.svg",
                questions : [
                    {
                        title : "I want to make a complaint, what should I do?",
                        content : [
                            {
                                tag : "p",
                                text : "We set out to offer exceptional customer service and aim to give you an easier, smarter, more stress-free solution to your energy. If for any reason you are not happy and would like to raise a complaint, please contact us on 0330 102 7901 or email complaints@eversmartenergy.co.uk."
                            }
                        ]
                    },
                    {
                        title : "How do we handle our complaints?",
                        content : [
                            // Step 1
                            {
                                tag: "b",
                                text :
                                    "STEP 1: Let us know your complaint"

                            },
                            {
                                tag : "ul",
                                items : [
                                    'Call our friendly UK based Customer Service team on 0330 102 7901',
                                    'E-mail us at complaints@eversmartenergy.co.uk',
                                    'Write to us at Eversmart Energy, 10 Empress Business Park, Manchester, M16 9EA.'
                                ]
                            },
                            {
                                tag: "p",
                                text :
                                    "We can normally resolve your complaint straight away – " +
                                    "if we can’t, we’ll escalate your complaint and keep you informed throughout.\n" +
                                    "We aim to do this within 5 working days."
                            },
                            // Step 2
                            {
                                tag: "b",
                                text :
                                    "STEP 2: We will escalate your complaint"

                            },
                            {
                                tag: "p",
                                text :
                                    "There might be instances where our team cannot resolve your complaint straight away " +
                                    "for you. If you are unhappy with our resolution, your complaint will be escalated to the complaint manager.\n" +
                                    "At this stage we will acknowledge your complaint within 5 working days, hopefully we can get the resolution you are looking for. " +
                                    "Whilst handling your complaint we may:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Apologise',
                                    'Rectify what went wrong and tell you what we have done to resolve your complaint',
                                    'Tell you what we’ll do next'
                                ]
                            },
                            // Step 3
                            {
                                tag: "b",
                                text :
                                    "STEP 3: External Review of your complaint"

                            },
                            {
                                tag: "p",
                                text :
                                    "Our final stage is an external, independent and impartial review of your complaint by contacting the Ombudsman Service, " +
                                    "if you feel that your complaint has not been resolved satisfactorily.\n" +
                                    "Our regulators may agree to investigate your concerns if:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'You receive a deadlock letter from us. You can refer the complaint within 12 months of receiving this letter.',
                                    'You have not received a letter from us for at least 8 weeks since you raised the complaint.',
                                    'Tell you what we’ll do next'
                                ]
                            },
                            {
                                tag: "p",
                                text : "If the Ombudsman agrees with your complaint, they will recommend steps needed to put things right. These may include:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'An apology from Eversmart Energy',
                                    'Compensation from Eversmart Energy',
                                    'An explanation from Eversmart Energy',
                                    'Action from Eversmart Energy'
                                ]
                            },
                            {
                                tag : "p",
                                text : "Eversmart Energy is bound by the Ombudsman decision. If you aren’t satisfied with this resolution, you can pursue your concerns elsewhere but you will lose the right to the resolution offered by the Ombudsman."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "energy",
                title : "Help & Support",
                image : "/assets/images/new-world/help/faqs/Help&Support.svg",
                questions : [
                    {
                        title : "Can you switch my meter from a prepayment meter to a standard credit meter?",
                        content : [
                            {
                                tag : "p",
                                text : "This is something our customer service team can review and discuss with you. If you have a debt it’s likely you’ll have to pay this off before we agree to change your status. We will install smart meter for you and changing from a prepay meter to a credit meter is done at a touch of a button."
                            }
                        ]
                    },
                    {
                        title : "Can you switch my meter from a standard credit meter to a prepayment meter?",
                        content : [
                            {
                                tag : "p",
                                text : "We can review and discuss this with you to look at the options that might best suit your situation. We will install smart meter for you and changing from a credit meter to a prepay meter is done at a touch of a button"
                            }
                        ]
                    },
                    {
                        title : "As a pre-pay customer, am I able to sign up to your online portal?",
                        content : [
                            {
                                tag : "p",
                                text : "As you’re on a prepayment meter, you won’t receive regular bills or statements and won’t have the need to make payments online so there are no benefits to signing up to our online portal at the stage."
                            }
                        ]
                    },
                    {
                        title : "What are the advantages of using a prepayment meter?",
                        content : [
                            {
                                tag : "ul",
                                items : [
                                    'Pay as you go – no surprise bills',
                                    'You can monitor your consumption',
                                    'Emergency credit option'
                                ]
                            }
                        ]
                    },
                    {
                        title : "How do standing charges work with my meter?",
                        content : [
                            {
                                tag : "p",
                                text : "Prepayment meters apply a daily standing charge, so it’s important you keep topping up your credit to avoid building up any debt – even if you don’t actually use any electricity/ gas. If you run out of credit the standing charge will keep building up as a debt which will be deducted in full next time you top up. You can find out how much your daily standing charge is from your meter display."
                            }
                        ]
                    },
                    {
                        title : "How does the meter collect debt?",
                        content : [
                            {
                                tag : "p",
                                text : "If you’ve had a prepayment meter put in to help you to repay a debt, it'll be collected from the credit you put onto your meter at a weekly rate that we’ve agreed with you. If you check the displays on your meter regularly, you’ll see the debt amount going down. Once it’s all paid off, your meter will automatically stop collecting the debt. If you’re having difficulty paying your debt and keeping your meter topped up, please contact us as soon as you can and we’ll be happy to discuss some options with you."
                            }
                        ]
                    },
                    {
                        title : "I'd like to switch provider to someone who offers the wam home discount - how can I do this with a debt on my prepayment meter?",
                        content : [
                            {
                                tag : "p",
                                text : "You may be able to switch provider if your debt is below £500 under a process called Debt Assignment Protocol (DAP), you would need to contact your preferred supplier who’ll be able to talk you through this. If your debt is greater than £500, please contact us to discuss your options."
                            }
                        ]
                    },
                    {
                        title : "I'm struggling to pay back my debt with my current supplier, can you reduce what I'm paying?",
                        content : [
                            {
                                tag : "p",
                                text : "We can review your current payment rate once you’ve switched, but it’s likely it will need to stay the same to make sure the debt is paid off. If you’re struggling with costs, it’s important you compare our tariff to your current provider’s. If you’re struggling to pay for your usage, you may want to consider asking for advice from an independent organisation. You can contact Citizens Advice on 0300 330 1313, Step Change on 0800 138 1111 or National Debt Line on 0808 808 4000."
                            }
                        ]
                    },
                    {
                        title : "I'm off supply, what should I do?",
                        content : [
                            {
                                tag : "p",
                                text : "If your supply has stopped due to a fault with your meter or your payment key, please contact us immediately on 0330 102 7901. We will arrange for an engineer to visit and put the problem right within 3 hours if you notify us on a working day, Monday-Friday between 8am-7pm, or within 4 hours on any other day. If we don’t meet this timeframe under our guaranteed standards of service, we’ll pay you compensation of £30."
                            }
                        ]
                    },
                    {
                        title : "What do I do if I'm going away?",
                        content : [
                            {
                                tag : "p",
                                text : "If you’re going away make sure that your meter is topped up with a suitable amount of credit. Even if you've turned all your appliances off, the meter will still collect the daily standing charge and also any weekly repayments towards any debt you may owe. Make sure you've also got enough credit for appliances you’ve left on, such as your fridge and freezer. If you run out, there'll be nobody home to start the emergency credit. And nobody wants to come home to a defrosted freezer!"
                            }
                        ]
                    }
                ]
            },

            {
                id : uuid(),
                service : "homeservices",
                title : "Product",
                image : "./assets/images/new-world/home-services/icons/product.png",
                questions : [
                    {
                        title : "What does an annual service include?",
                        content : [
                            {
                                tag : "p",
                                text : "This is a visit we carry out in each contract year* to check your *boiler and *central heating system or *gas appliance is safe and in good working order. *(depending on what is included in your agreement)"
                            }
                        ]
                    },
                    {
                        title : "Where can I find what my home emergency policy covers?",
                        content : [
                            {
                                tag : "p",
                                text : "Your policy information will have been emailed to you in your welcome pack when you took out your policy. If you have misplaced your welcome pack, you can email or call us and we can send it to you again. You can also refer to terms and conditions to understand what your policy covers."
                            }
                        ]
                    },
                    {
                        title : "Do you cover LPG, Oil and Electric boilers?",
                        content : [
                            {
                                tag : "p",
                                text : "No, unfortunately we do not cover boilers fueled by LPG, Oil or Electric. Please refer to page 7 of your T&C’s."
                            }
                        ]
                    },
                    {
                        title : "Is there an age limit for boilers?",
                        content : [
                            {
                                tag : "p",
                                text : "There is no age limit on the boilers that we cover but please refer to basis of cover on page 3 of your t&c’s."
                            }
                        ]
                    },
                    {
                        title : "Can I manage my account online?",
                        content : [
                            {
                                tag : "p",
                                text : "Our online portal & app will be launching in spring. Until this time our dedicated customer service team can assist you with any requirements you have."
                            }
                        ]
                    },
                    {
                        title : "Can I speak to someone if I have a question?",
                        content : [
                            {
                                tag : "p",
                                text : "Speak to a member of our friendly team in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "homeservices",
                title : "Address",
                image : "./assets/images/new-world/home-services/icons/home.png",
                questions : [
                    {
                        title : "Can I speak to someone if I have a question?",
                        content : [
                            {
                                tag : "p",
                                text : "Speak to a member of our friendly team in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "Do you cover house shares, HMOs, flats or lodgers?",
                        content : [
                            {
                                tag : "p",
                                text : "Yes, as long as you are not in breach of definitions of coverage which can be found in pages 2 and 3 of your t&c’s"
                            }
                        ]
                    },
                    {
                        title : "Do you cover holiday homes or properties abroad?",
                        content : [
                            {
                                tag : "p",
                                text : "Please refer to geographical limits and general definitions on pages 2 and 3 of your t&c’s."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "homeservices",
                title : "Signup",
                image : "./assets/images/new-world/home-services/icons/user.png",
                questions : [
                    {
                        title : "Can I speak to someone if I have a question?",
                        content : [
                            {
                                tag : "p",
                                text : "Speak to a member of our friendly team in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "Can I manage my account online?",
                        content : [
                            {
                                tag : "p",
                                text : "Our online portal & app will be launching in spring. Until this time our dedicated customer service team can assist you with any requirements you have."
                            }
                        ]
                    },
                    {
                        title : "How can I make a claim?",
                        content : [
                            {
                                tag : "p",
                                text : "In order to make a claim by contacting our claims partner preferred management services on 0191 466 1298 or email info@preferredmanagement.co.uk."
                            }
                        ]
                    },
                    {
                        title : "Where can I find the terms & conditions of my home emergency policy?",
                        content : [
                            {
                                tag : "p",
                                text : "The terms and conditions can be found &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot; href=&quot;https://eversmartenergy--qa.cs108.my.salesforce.com/sfc/p/#1l0000008d2T/a/1l0000004JRl/dBl.xsbO.RRVlHfIukbrGA.TCxcn2P4HgXp9EI1vbtw&quot; title=&quot;T&amp;C's&quot;&gt;here&lt;/a&gt;."
                            }
                        ]
                    },
                    {
                        title : "Where can I find what my home emergency policy covers?",
                        content : [
                            {
                                tag : "p",
                                text : "Your policy information will have been emailed to you in your welcome pack when you took out your policy. If you have misplaced your welcome pack, you can email or call us and we can send it to you again. You can also refer to terms and conditions to understand what your policy covers."
                            }
                        ]
                    },
                    {
                        title : "Who will attend a home emergency?",
                        content : [
                            {
                                tag : "p",
                                text : "A qualified representative will be appointed by approved management services in the event of an emergency."
                            }
                        ]
                    },
                    {
                        title : "What is the definition of an emergency?",
                        content : [
                            {
                                tag : "p",
                                text : "A sudden or unexpected event which if not dealt with quickly may:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'lead to further damage.',
                                    'leave your home unsafe or unsecured.',
                                    'or cause danger to you or any other permanent resident of your home.',
                                    'Please refer to your t&c’s for policy definitions.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "How can I change the contact details you have for me?",
                        content : [
                            {
                                tag : "p",
                                text : "If you wish to change your contact details, please contact us in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;.',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "What happens if I move to a new house?",
                        content : [
                            {
                                tag : "p",
                                text : "If you move home, please contact us in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "What will happen if my home becomes uninhabitable?",
                        content : [
                            {
                                tag : "p",
                                text : "If your policy covers this and If the emergency responder determines and deems your home uninhabitable in line with the t&c’s, emergency polices cover alternative accommodation. Please refer to t&c’s for more information."
                            }
                        ]
                    }
                ]
            },
            {
                id : uuid(),
                service : "homeservices",
                title : "Payment",
                image : "./assets/images/new-world/home-services/icons/payment.png",
                questions : [
                    {
                        title : "Can I speak to someone if I have a question?",
                        content : [
                            {
                                tag : "p",
                                text : "Speak to a member of our friendly team in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "Where can I find the terms & conditions of my home emergency policy?",
                        content : [
                            {
                                tag : "p",
                                text : "The terms and conditions can be found &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot; href=&quot;https://eversmartenergy--qa.cs108.my.salesforce.com/sfc/p/#1l0000008d2T/a/1l0000004JRl/dBl.xsbO.RRVlHfIukbrGA.TCxcn2P4HgXp9EI1vbtw&quot; title=&quot;T&amp;C's&quot;&gt;here&lt;/a&gt;."
                            }
                        ]
                    },
                    {
                        title : "Can I pay for my policy using a credit / debit card?",
                        content : [
                            {
                                tag : "p",
                                text : "No, we only accept Direct Debit as a payment facility however this may change in future."
                            }
                        ]
                    },
                    {
                        title : "My Direct Debit has failed or I missed a payment? What should i do?",
                        content : [
                            {
                                tag : "p",
                                text : "You will receive a confirmation that a payment has been missed. We will automatically re-attempt to take payment within 24 hours. If the payment is still unsuccessful you can contact us in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;'
                                ]
                            },
                            {
                                tag : "p",
                                text : "Failing this, we will contact you. If the policy remains unpaid for 30 day period we reserve the right to cancel your policy."
                            }
                        ]
                    },
                    {
                        title : "When will my first payment be taken?",
                        content : [
                            {
                                tag : "p",
                                text : "Payment is usually taken 14 days after taking out the policy online."
                            }
                        ]
                    },
                    {
                        title : "How can I change when my Direct Debit is taken?",
                        content : [
                            {
                                tag : "p",
                                text : "If you wish to amend your direct debit date please contact us in one of the following ways:"
                            },
                            {
                                tag : "ul",
                                items : [
                                    'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                    'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                    'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                    'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                                ]
                            }
                        ]
                    },
                    {
                        title : "What happens to my renewal price after the first year?",
                        content : [
                            {
                                tag : "p",
                                text : "The price you'll pay at renewal is likely to increase as your introductory price will have ended, " +
                                    "but it will be tailored to your specific details. If you're paying by direct debit, " +
                                    "you will automatically renew; however, you can still choose to change or cancel your contract. " +
                                    "We'll contact you 30 days before your renewal date to remind you and provide you with a renewal comparison quote."
                            }
                        ]
                    },
                    {
                        title : "Do I have to pay an excess if I claim on my policy?",
                        content : [
                            {
                                tag : "p",
                                text : "Excess amounts depend on the policy you have taken. Please refer to your policy schedule which can be found in your welcome pack."
                            }
                        ]
                    },
                    {
                        title : "Are call-out charges, labour and parts included?",
                        content : [
                            {
                                tag : "p",
                                text : "Call out charges depend on your policy, labour and parts are included within your policy up to the policy cover limits."
                            }
                        ]
                    }
                ]
            }
        ]
    };

    function toggleBody(){

        var TL = new TimelineMax({ paused : true });

        if( !faqs.bodyOpen ){
            TL.to( $('#faqs-step-back-button'), 0.4, { opacity : 1 , ease : Power2.easeInOut }, 'first' );
            TL.to( faqs.$topics, 0.4, { css : { autoAlpha : 0, display : 'none' }, ease : Power2.easeInOut }, 'first' );
            TL.to( faqs.$topicInfo, 0.4, { css : { autoAlpha : 1, display : 'block' }, ease : Power2.easeInOut } );
        }else{
            TL.to( $('#faqs-step-back-button'), 0.3, { opacity : 0 , ease : Power2.easeInOut }, 'first' );
            TL.to( faqs.$topicInfo, 0.3, { css : { autoAlpha : 0, display : 'none' }, ease : Power2.easeInOut }, 'first' );
            TL.to( faqs.$topics, 0.4, { css : { autoAlpha : 1, display : 'block' }, ease : Power2.easeInOut, onComplete : function(){ faqs.$topicInfo.empty() } } );
        };

        TL.play();
        
        faqs.bodyOpen = !faqs.bodyOpen;
    }

    function evtClick_backButton( evt ){
        evt.preventDefault();
        if( !faqs.bodyOpen ){ return; }
        toggleBody();
    }

    function evtClick_questionExpand( evt ){
        evt.preventDefault();
        var $parent = $( this ).parents( '.question-wrap' );
        var condition = $parent.hasClass('expanded');
        condition ? $parent.removeClass('expanded') : $parent.addClass('expanded') ;
        TweenMax.to( $parent.find('.question-body'), 0.3, { autoAlpha : condition ? 0 : 1, display : condition ? 'none' : 'block', ease : Power2.easeInOut } );
    }

    function evtClick_topicSelected( evt ){
        evt.preventDefault();

        buildTopic( $( this ).attr('id'), toggleBody );
    }

    function parseContent( tag, txt ){
        return $( tag ).html( txt ).text();
    }

    function buildBody( data ){
        return data.map(function( item ){
            return item.tag !== 'ul' ? $( '<'+ item.tag +'>' ).html( parseContent( '<'+ item.tag +'>', item.text ) ) : $('<ul>').append( item.items.map(function( entry ){ return $('<li>').html( parseContent( '<li>', entry ) ) }) ) ;
        });
    }

    function buildTopic( id, cb ){
        var topic = faqs.data.find(function( item ){ return item.id === id  });

        faqs.$topicInfo.append( $('<h2>').text( topic.title ) );
        topic.questions.forEach(function( topic ){
            faqs.$topicInfo.append(
                $('<div>', { class : "question-wrap" }).append([
                    $('<a>', { class : "question-link", href : '#' }).text( topic.title ),
                    $('<div>', { class : "question-body" }).append( buildBody( topic.content ) )
                ])
            );
        });

        $('.question-link').on( 'click', evtClick_questionExpand );
        
        cb instanceof Function && cb();
    }

    function buildTopics(){
        faqs.$topics.empty();
        var template = Handlebars.compile( faqs.templates.topic );
        faqs.data.filter(function( item ){ return item.service === faqs.type }).forEach(function( item ){
            faqs.$topics.append( template( item ) );
        });
        $('.topic-link').on( 'click', evtClick_topicSelected );
    }

    function evtClick_typeFAQS( evt ){
        evt.preventDefault();
        var $this = $( this );
        faqs.$wrap.find('a[data-type]').removeClass('chosen');
        $this.addClass('chosen');
        faqs.type = $this.attr('data-type');
        faqs.templates.topic = evt.data;
        buildTopics();
    }

    REQUEST( '/templates/help/faqs/topics.hbs', 'text' )
        .then(function( templates ){
            console.log(faqs.$wrap.find('a[data-type]'), templates);
            faqs.$wrap.find('a[data-type]').on( 'click', null, templates, evtClick_typeFAQS );
        });
    
    $('#faqs-step-back-button').on( 'click', evtClick_backButton );
});
