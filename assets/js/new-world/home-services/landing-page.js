$(function(){

    /*var compare = {
        $wrap : $('#compare-wrap'),
        initialPeriod : null,
        templates : {},
        hasSetup : false,
        samples : [ 0, 6, 12, 18 ]
    };



    



        // handlebar helpers
    Handlebars.registerHelper( 'showFirstPayment', function( product ) {
        return product.PaymentSchedule === "Annual" ? ' annual' : '' ;
    });
    Handlebars.registerHelper( 'firstPayment', function( product ) {
        return product.PaymentSchedule === "Annual" ? '' : '£'+product.FirstPayment ;
    });
    Handlebars.registerHelper( 'payment', function( product ) {
        return product.PaymentSchedule === "Annual" ? '£'+product.AnnualRetailPremiumIncIPT : '£'+product.MonthlyPremiumIncIPT ;
    });
    Handlebars.registerHelper( 'isIncluded', function( product ) {
        return product.covered ? ' included' : ' not-included';
    });

    

    function updatePrice( $el, data ){
        var condition = data[0].PaymentSchedule === "Annual";
        $el.find('.price-wrap')[ condition ? 'addClass' : 'removeClass' ]( 'annual' );
        $el.find('.payment span').text( condition ? '£'+data[0].AnnualRetailPremiumIncIPT.toFixed(2)+'*' : '£'+data[0].MonthlyPremiumIncIPT.toFixed(2)+'*' );

        if( !condition ){
            $el.find('.first-month .value').text( condition ? '' : '£'+data[0].FirstPayment.toFixed(2)+'*' );
            $el.find('.monthly .value').text( condition ? '' : '£'+data[0].MonthlyPremiumIncIPT.toFixed(2)+'*' );
        };
    };

    function isValidProduct( $el, goToQuote ){

        if( !compare.hasSetup ){
            return;
        };

        var excess = $el.find('.excess-option.chosen').attr('data-type');
        var payment = $el.find('.payment-option.chosen').attr('data-type');
        var options = $el.find('.add-option.included').map(function( i, el ){ return $( el ).attr('data-type') });
        var products = compare.products.filter(function( item ){
            return item.ExcessLevel === +excess && options.length && options.filter(function( i, opt ){ return item.ItemsCovered.indexOf( opt ) > -1 && options.length === item.ItemsCovered.length }).length === options.length && item.PaymentSchedule === payment;
        });

        if( !products.length ){
            console.error("no products that match found!", $el);
        }else{
            console.log('products : ', products);
            updatePrice( $el, products );
            if( goToQuote ){
                window.location.href = window.location.href + '/quote#' + products[0].ProductId;
            }
        };

    };

    function evtClick_excessOption( evt ){
        try{ evt.preventDefault() }catch( err ){};
        
        var $this = $( this );
        var $choice = $this.parents('.choice');

        if( $this.hasClass('chosen') ){
            return;
        };

        $this.addClass('chosen');
        $choice.find('.excess-option').not(this).removeClass('chosen');

        isValidProduct( $choice );

        if( evt !== undefined && compare.hasSetup ){
            var $wrap = $choice.parent();
            var $otherChoices = $wrap.find( '.choice' ).not( $choice[0] );
            $otherChoices.toArray().forEach(function( el ){
                evtClick_excessOption.call( $( el ).find('.excess-option[data-type="'+ $this.attr('data-type') +'"]')[0] );
            });
        };
    };

    function evtClick_paymentOption( evt ){
        try{ evt.preventDefault() }catch( err ){};

        var $this = $( this );
        var $choice = $this.parents('.choice');

        if( $this.hasClass('chosen') ){
            return;
        };
        
        $this.addClass('chosen');
        $choice.find('.payment-option').not(this).removeClass('chosen');

        isValidProduct( $choice );

        if( evt !== undefined && compare.hasSetup ){
            var $wrap = $choice.parent();
            var $otherChoices = $wrap.find( '.choice' ).not( $choice[0] );
            $otherChoices.toArray().forEach(function( el ){
                evtClick_paymentOption.call( $( el ).find('.payment-option[data-type="'+ $this.attr('data-type') +'"]')[0] );
            });
        };
    };

    function buildCompare( data, template ){

        var opts = { items : data.filter(function( item, i ){ return compare.samples.indexOf( i ) > -1 }) };

        opts.items.map(function( opt ){
            opt.coverage = [];
            opt.ItemsCovered.forEach(function( item ){ opt.coverage.push({ covered : true, name : item }) });
            opt.ItemsNotCovered.forEach(function( item ){ opt.coverage.push({ covered : false, name : item }) });
            return opt;
        });

        compare.templates.items = Handlebars.compile( template );
        compare.$wrap.append( compare.templates.items( opts ) );

        $('.excess-option').on( 'click', evtClick_excessOption );
        $('.payment-option').on( 'click', evtClick_paymentOption );
        $('.add-option').on( 'click', function( evt ){ evt.preventDefault() });
        $('.get-quote-link').on( 'click', function( evt ){
            evt.preventDefault();
            isValidProduct( $(this).parents('.choice'), true );
        });

        appendOptions( opts );
        
    };

    function appendOptions( opts ){
        compare.$wrap.find('> .inner > ul > li').each(function( i, el ){
            var $el = $( el );
    
            $el.find('.excess-option[data-type="'+ opts.items[ i ].ExcessLevel +'"]').trigger('click');
            $el.find('.payment-option').toArray().forEach(function( element ){
                var $element = $(element);
                return $element.attr('data-type') === opts.items[ i ].PaymentSchedule && $element.trigger('click');
            });
        });
    };
    
    togglePreloader();
    Promise.all([ REQUEST( (window.APIUrl ? window.APIUrl : "") + '/homeservices/get_salesforce_products' ), REQUEST( '/templates/homeservices/compare.hbs', 'text' ), REQUEST((window.APIUrl ? window.APIUrl : "") + '/homeservices/initial_period') ])
        .then(function( res ){
            if (!res || res.length === 0) {
                return;
            }

            togglePreloader();
            compare.initialPeriod = res[2].initial_period;
            compare.products = res[0] ? res[0].Products : {};
            res[0] && buildCompare( res[0].Products, res[1] );
            resetTooltips();
            compare.hasSetup = true;
        });
*/
});
