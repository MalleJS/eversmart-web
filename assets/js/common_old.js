var redirect_url;
var group;
if(window.location.hostname == 'localhost')
{
	var base_url = 'http://'+window.location.hostname+'/eversmartnew/';
}
if( window.location.hostname == '54.218.78.31' )
{
	var base_url = 'http://'+window.location.hostname+'/';
}
if( window.location.hostname == '13.58.101.121' )
{
	var base_url = 'http://'+window.location.hostname+'/eversmartnew/';
}

$(document).ready(function()
{

// get url parameter value
  var current = location.href;
	var energy = current.match(/energy/g);


// loader show/hide

setTimeout(function(){ $('.loading-page').fadeOut();
 $('#main_content').fadeIn() },1000);
  //postcode quotation page
  $.ajax({
      url: base_url+'index.php/quotation/postcode',
      type: 'get',
      success:function(response)
      {
        $('#response').html(response);
      }
  });

  $(document).on('submit','#form-postcode-simple',function(e)
  {
		e.preventDefault();
		var form_postcode = $('#form-postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/quotefor',
				type: 'post',
				data: { postcode: form_postcode },
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
					//console.log(response);
					if( response == '2000' ) {
						$('#error_message').text('Input postcode did not match validation regex');
						$('.error_msg').show();
					}
					else {
						$('#quotation_form #quotation_postcode').val( form_postcode );
						$('#response').html(response);
					}
				},
      error: function(jqXHR, textStatus, errorThrown){
          alert(textStatus);
      }
		});
  });

	$(document).on( 'click', '#back_postcode', function(){
		//alert($('#quotation_postcode').val());
		//return false;
		var postcode = $('#quotation_postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/postcode',
				type: 'get',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
					$('#response').html(response);
					$('.md-form label').addClass('active');
					$('#form-postcode').val( postcode );
				}
		});
	});


	$(document).on('click', '.quotefor', function(){

			var select_quote = $(this).data('quotefor');
			if( select_quote == 'electricity' ){
				$.ajax({
						url: base_url+'index.php/quotation/address_list',
						type: 'get',
						beforeSend: function(){
							$('.loading').fadeIn();
						 	$('#main_content').fadeOut();
						},
						complete: function(){
							$('.loading').fadeOut();
						 	$('#main_content').fadeIn();
						},
						success:function(response)
						{
							$('#quotation_form #quotation_for').val( select_quote );
							$('#response').html(response);
						}
				});
			}else {
				$.ajax({
						url: base_url+'index.php/quotation/stop_quotation',
						type: 'get',
						beforeSend: function(){
							$('.loading').fadeIn();
						 	$('#main_content').fadeOut();
						},
						complete: function(){
							$('.loading').fadeOut();
						 	$('#main_content').fadeIn();
						},
						success:function(response)
						{
							$('#quotation_form #quotation_for').val( select_quote );
							$('#response').html(response);
						}
				});
			}
	});


	// BACK TO QUOTE FOR SECTION
	$(document).on( 'click', '#back_quotefor', function(){
		var form_postcode = $('#quotation_postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/quotefor',
				type: 'post',
				data: { postcode: form_postcode },
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
						$('#response').html(response);
				}
				});
	});

//get address by selection
$(document).on( 'submit', '#address_form', function(e){
	e.preventDefault();
		//var selected_address = $(this).val(); quotation_address
		var selected_address = $('#quotation_address').find(':selected').val();
		var selected_mpan = $(this).find(':selected').data('mpan');
		var selected_mprn = $(this).find(':selected').data('mprn');

		$.ajax({
				url: base_url+'index.php/quotation/payEnergy',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
						$('#quotation_form #quotation_address').val( selected_address );
						$('#quotation_form #quotation_mpan').val( selected_mpan );
						$('#quotation_form #quotation_mprn').val( selected_mprn );
						$('#response').html(response);
				}
				});
});

// back to address selection screen
	$(document).on('click', '#back_address', function(){
		$.ajax({
				url: base_url+'index.php/quotation/address_list',
				type: 'get',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
					$('#response').html(response);
				}
		});
	});

// get pay energy value
	$(document).on('click', '.pay_energy', function(){
		var selected_pay_energy = $(this).data('valuepayenergy');
			$.ajax({
					url: base_url+'index.php/quotation/know_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#quotation_pay_energy').val(selected_pay_energy);
							$('#response').html(response);
					}
				});

	});

// back from prepay
$(document).on('click','.backfromprepay', function(){
	$.ajax({
			url: base_url+'index.php/quotation/payEnergy',
			type: 'post',
			beforeSend: function(){
				$('.loading').fadeIn();
				$('#main_content').fadeOut();
			},
			complete: function(){
				$('.loading').fadeOut();
				$('#main_content').fadeIn();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
});

// value if current Supplier known
	$(document).on('click', '.know_current_energy', function(){
		var know_current_energy = $(this).data('know_current_energy');
		if(know_current_energy == 'no'){
			$.ajax({
					url: base_url+'index.php/quotation/hear_about',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}
		if( know_current_energy == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			var quotation_for = $('#quotation_for').val();

			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function () {
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						var quotation_for = $('#quotation_for').val();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
        },
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}

	});

// back to know supplier
	$(document).on('click','#back_known_supplier', function(){
		$.ajax({
				url: base_url+'index.php/quotation/know_supplier',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

	$(document).on('click','#back_known', function(){
		var know_supplier_value = $('#quotation_know_energy').val();
		var quotation_for = $('#quotation_for').val();
		if( know_supplier_value == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function () {
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
							$('#response').html(response);
					}
				});
		}
		if( know_supplier_value == 'no' ){
			$.ajax({
					url: base_url+'index.php/quotation/know_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#response').html(response);
					}
				});
		}
	});

//get selected supplier for Tariff
	$(document).on('change','#elec_selected_supplier', function(){
		var elec_supplier = $(this).val();
		var quotation_for = $('#quotation_for').val();
		$.ajax({
				url: base_url+'index.php/quotation/get_tariff',
				type: 'post',
				dataType: 'json',
				data: { elec_supplier: elec_supplier, quotation_for: quotation_for },
				beforeSend: function()
				{
					$('#elec_tariff_list').html('<option>Loading...</option>');
				},
				success:function(response)
				{
					console.log(response);
					$('#quotation_supplier').val(elec_supplier);
					if( response.error == '0' ){
						$('#elec_tariff_list').empty();
						var response_data = response.data;
							if( response.data.length == 0  )
							{
								$('#elec_tariff_list').html( '<option value="0">Please Select valid supplier</option>' );
							}else {
								$('#elec_tariff_list').html( '<option value="0">Please Select Tariff</option>' );
								for( i=0; i<response_data.length; i++ )
								{
											$('#elec_tariff_list').append( '<option value="'+response_data[i].tariffName+'" data-fuelType="'+response_data[i].fuelType+'">'+response_data[i].tariffName+'</option>' );
								}
							}
					}
					if(response.error == '1'){
						$('#elec_tariff_list').empty();
						$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
					if(response.error == '2'){
						$('#elec_tariff_list').empty();
							$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
				}
			});
	});

// get selected tariff and supplier
	$(document).on('click','#supplier_submit',function(){
		var selected_supplier = $('#elec_selected_supplier').find(':selected').val();
		var selected_tariff = $('#elec_tariff_list').find(':selected').val();
		var selected_tariff_fuelType = $('#elec_tariff_list').find(':selected').data('fueltype');
		var supplier_id = $('#elec_selected_supplier').find(':selected').data('supplierid');

		if( selected_supplier == 0 || selected_tariff == 0 )
		{
			alert('Please select valid and supplier and tariff');
		}
		else {
			$.ajax({
					url: base_url+'index.php/quotation/hear_about',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete:function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
							$('#quotation_supplier').val(selected_supplier);
							$('#quotation_tariff').val(selected_tariff);
							$('#quotation_supplier_id').val(supplier_id);
							$('#selected_tariff_fuelType').val(selected_tariff_fuelType);
					},
					success:function(response)
					{
						$('#response').html(response);
					}
				});
		}

	});

//get value of hear_about_us
	$(document).on('click', '.hear_about_us', function(){
		var headabout = $(this).data('hearabout');
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

//know current usage
	$(document).on('click', '.know_current_usage',function(){
		var know_current_usage = $(this).data('know_current_usage');
		var quotation_for = $('#quotation_for').val();
		//alert(quotation_for);
		if( know_current_usage == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();

			$.ajax({
					url: base_url+'index.php/quotation/enter_energy',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_usage').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elec_usage').css('display','none');
						// }
						if( quotation_for == 'electricity' ){
							$('#gas_usage').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_usage').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);
						$('#response').html(response);
					}
				});
		}
		if(know_current_usage == 'no'){

			$.ajax({
					url: base_url+'index.php/quotation/home_type',
					type: 'post',
					beforeSend: function(){
	$('.loading').fadeIn();
	$('#main_content').fadeOut();
},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);
						$('#response').html(response);
					}
				});
		}
	});

// back to hear about us
	$(document).on('click','.backtohearabout',function(){
		$.ajax({
				url: base_url+'index.php/quotation/hear_about',
				type: 'post',
				beforeSend: function(){
	$('.loading').fadeIn();
	$('#main_content').fadeOut();
},
				complete:function()
				{
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
						$('#quotation_supplier').val(selected_supplier);
						$('#quotation_tariff').val(selected_tariff);
				},
				success:function(response)
				{
					$('#response').html(response);
				}
			});
	})

//back to know energy usage_select
	$(document).on('click','.backknowusage', function(){
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
	$('.loading').fadeIn();
	$('#main_content').fadeOut();
},
				complete: function()
				{
					$('.loading').fadeOut();
	$('#main_content').fadeIn();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

//get energy usage
	$(document).on('click','#enter_usage_form',function(){
		var elec_usage = $('#enter_elec_usage').val();
		var elec_usage_select = $('.elec_usage_select').find(':selected').val();
		//get supplier and tariff
		var quotation_supplier = $('#quotation_supplier').val();
		var quotation_tariff = $('#quotation_tariff').val();
		var quotation_pay_energy = $('#quotation_pay_energy').val();
		if( !elec_usage || elec_usage_select == "0" ){
			alert('All fields are requied');
		}else{
			$('#quotation_elec_energy_usage').val(elec_usage)
			$('#quotation_elec_energy_interval').val(elec_usage_select);

			$('#quotation_form').trigger('submit');
		}
	});

//get static energy usage
	$(document).on('click','.home_type',function(){
			var elec_usage = $(this).data('usage');
			var elec_usage_select = $(this).data('interval');

			$('#quotation_elec_energy_usage').val(elec_usage)
			$('#quotation_elec_energy_interval').val(elec_usage_select);
	});

//submit quotation values
	$(document).on('submit', '#quotation_form', function(e){
			e.preventDefault();

			//alert( $('#quotation_form').serialize() );
			// $.ajax({
			// 		url: base_url+'index.php/quotation/summary',
			// 		type: 'post',
			// 		data: $('#quotation_form').serialize(),
			// 		complete: function()
			// 		{
			// 			$('#quotation_hearabout').val( headabout );
			// 		},
			// 		success:function(response)
			// 		{
			// 				$('#response').html(response);
			// 		}
			// 	});
			var elec_usage = $('#enter_elec_usage').val();
			var elec_usage_select = $('.elec_usage_select').find(':selected').val();
			//get supplier and tariff
			var quotation_supplier = $('#quotation_supplier').val();
			var quotation_tariff = $('#quotation_tariff').val();
			var quotation_pay_energy = $('#quotation_pay_energy').val();

			$.ajax({
					url: base_url+'index.php/quotation/get_projection',
					type: 'post',
					data: $('#quotation_form').serialize() + "&elec_usage="+elec_usage+ "&quotation_supplier="+ quotation_supplier + "&quotation_tariff=" +quotation_tariff + "&quotation_pay_energy="+quotation_pay_energy,
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
					//	$('#quotation_hearabout').val( headabout );
					},
					success:function(response)
					{
							console.log(response);
							if( response == '3042' ){
								$('#error_msg').text('The entered usage amount is not valid. Please enter valid usage');
							}else {
									$('#response').html(response);
							}
					}
				});
	});

});
