var redirect_url;
var group;
if(window.location.hostname == 'localhost')
{
	var base_url = 'http://'+window.location.hostname+'/eversmartnew/';
}
if( window.location.hostname == '54.218.78.31' )
{
	var base_url = 'http://'+window.location.hostname+'/';
}
if( window.location.hostname == '13.58.101.121' )
{
	var base_url = 'http://'+window.location.hostname+'/eversmartnew/';
}

$(document).ready(function()
{

// get url parameter value
  var current = location.href;
	var energy = current.match(/energy/g);


// loader show/hide

setTimeout(function(){ $('.loading-page').fadeOut();
 $('#main_content').fadeIn() },1000);
  //postcode quotation page
  $.ajax({
      url: base_url+'index.php/quotation/postcode',
      type: 'get',
      success:function(response)
      {
        $('#response').html(response);
      }
  });

  $(document).on('submit','#form-postcode-simple',function(e)
  {
		e.preventDefault();
		var form_postcode = $('#form-postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/quotefor',
				type: 'post',
				data: { postcode: form_postcode },
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
					//console.log(response);
					if( response == '2000' ) {
						$('#error_message').text('Input postcode did not match validation regex');
						$('.error_msg').show();
					}
					else {
						$('#quotation_form #quotation_postcode').val( form_postcode );
						$('#response').html(response);
					}
				},
      error: function(jqXHR, textStatus, errorThrown){
          alert(textStatus);
      }
		});
  });

	$(document).on( 'click', '#back_postcode', function(){
		//alert($('#quotation_postcode').val());
		//return false;
		var postcode = $('#quotation_postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/address_list',
				type: 'get',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				},
				success:function(response)
				{
					$('#response').html(response);
					$('.md-form label').addClass('active');
					$('#form-postcode').val( postcode );
				}
		});
	});


	$(document).on('click', '.quotefor', function(){

			var select_quote = $(this).data('quotefor');
			// if( select_quote == 'electricity' ){
				$.ajax({
						url: base_url+'index.php/quotation/payEnergy',
						type: 'get',
						beforeSend: function(){
							$('.loading').fadeIn();
						 	$('#main_content').fadeOut();
						},
						complete: function(){
							$('.loading').fadeOut();
						 	$('#main_content').fadeIn();
						},
						success:function(response)
						{
							$('#quotation_form #quotation_for').val( select_quote );
							$('#response').html(response);
						}
				});
			// }else {
			// 	$.ajax({
			// 			url: base_url+'index.php/quotation/stop_quotation',
			// 			type: 'get',
			// 			beforeSend: function(){
			// 				$('.loading').fadeIn();
			// 			 	$('#main_content').fadeOut();
			// 			},
			// 			complete: function(){
			// 				$('.loading').fadeOut();
			// 			 	$('#main_content').fadeIn();
			// 			},
			// 			success:function(response)
			// 			{
			// 				$('#quotation_form #quotation_for').val( select_quote );
			// 				$('#response').html(response);
			// 			}
			// 	});
			// }
	});


	// BACK TO QUOTE FOR SECTION
	$(document).on( 'click', '#back_quotefor', function(){
		var form_postcode = $('#quotation_postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/postcode',
				type: 'post',
				data: { postcode: form_postcode },
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				 	$('.md-form label').addClass('active');
				 	$('#form-postcode').val( form_postcode );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
				});
	});

//get address by selection
$(document).on( 'submit', '#address_form', function(e){
	e.preventDefault();
	// alert();
	// return false;
		//var selected_address = $(this).val(); quotation_address
		var selected_address = $('#quotation_address').find(':selected').val();
		var selected_mpan = $(this).find(':selected').data('mpan');
		var selected_mprn = $(this).find(':selected').data('mprn');
		var metertype = $(this).find(':selected').data('metertype');

		$.ajax({
				url: base_url+'index.php/quotation/new_quotefor',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				 		if( selected_mprn == null || selected_mprn == '' )
						{
							$('#gas_quote').css('display','none');
							$('#both_quote').css('display','none');
						}
						if( selected_mpan == '' || selected_mpan == null )
						{
							$('#elect_quote').hide();
							$('#both_quote').css('display','none');
						}
				},
				success:function(response)
				{
						$('#quotation_form #quotation_address').val( selected_address );
						$('#quotation_form #quotation_mpan').val( selected_mpan );
						$('#quotation_form #quotation_mprn').val( selected_mprn );
						$('#quotation_form #meterTypeElec').val( metertype );
						$('#response').html(response);
				}
				});
});

// back to address selection screen
	$(document).on('click', '#back_address', function(){
		var quotation_mpan = $('#quotation_form #quotation_mpan').val( );
		var quotation_mprn = $('#quotation_form #quotation_mprn').val( );
		var meterTypeElec=  $('#quotation_form #meterTypeElec').val(  );
		$.ajax({
				url: base_url+'index.php/quotation/new_quotefor',
				type: 'get',
				beforeSend: function(){
					$('.loading').fadeIn();
				 	$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
				 	$('#main_content').fadeIn();
				 	if( quotation_mprn == null || quotation_mprn == '' )
					{
						$('#gas_quote').css('display','none');
						$('#both_quote').css('display','none');
					}
					if( quotation_mpan == '' || quotation_mpan == null )
					{
						$('#elect_quote').hide();
						$('#both_quote').css('display','none');
					}
				},
				success:function(response)
				{
					$('#response').html(response);
				}
		});
	});

// get pay energy value
	$(document).on('click', '.pay_energy', function(){
		var selected_pay_energy = $(this).data('valuepayenergy');
			$.ajax({
					url: base_url+'index.php/quotation/know_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#quotation_pay_energy').val(selected_pay_energy);
							$('#response').html(response);
					}
				});

	});

// back from prepay
$(document).on('click','.backfromprepay', function(){
	$.ajax({
			url: base_url+'index.php/quotation/payEnergy',
			type: 'post',
			beforeSend: function(){
				$('.loading').fadeIn();
				$('#main_content').fadeOut();
			},
			complete: function(){
				$('.loading').fadeOut();
				$('#main_content').fadeIn();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
});

// value if current Supplier known
	$(document).on('click', '.know_current_energy', function(){
		var know_current_energy = $(this).data('know_current_energy');
		if(know_current_energy == 'no'){
			$.ajax({
					url: base_url+'index.php/quotation/hear_about',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}
		if( know_current_energy == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			var quotation_for = $('#quotation_for').val();

			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function () {
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						var quotation_for = $('#quotation_for').val();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
        },
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}

	});

// back to know supplier
	$(document).on('click','#back_known_supplier', function(){
		$.ajax({
				url: base_url+'index.php/quotation/know_supplier',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

	$(document).on('click','#back_known', function(){
		var know_supplier_value = $('#quotation_know_energy').val();
		var quotation_for = $('#quotation_for').val();
		if( know_supplier_value == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
					 	$('#main_content').fadeOut();
					},
					complete: function () {
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						$('.loading').fadeOut();
					 	$('#main_content').fadeIn();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
							$('#response').html(response);
					}
				});
		}
		if( know_supplier_value == 'no' ){
			$.ajax({
					url: base_url+'index.php/quotation/know_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function(){
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
					},
					success:function(response)
					{
						$('#response').html(response);
					}
				});
		}
	});

//get selected supplier for Tariff
	$(document).on('change','#elec_selected_supplier', function(){
		var elec_supplier = $(this).val();
		var quotation_for = $('#quotation_for').val();
		$.ajax({
				url: base_url+'index.php/quotation/get_tariff',
				type: 'post',
				dataType: 'json',
				data: { elec_supplier: elec_supplier, quotation_for: quotation_for, tariff_fuelType: 'electricity' },
				beforeSend: function()
				{
					$('#elec_tariff_list').html('<option>Loading...</option>');
				},
				success:function(response)
				{
					//console.log(response);
					$('#quotation_supplier').val(elec_supplier);
					if( response.error == '0' ){
						$('#elec_tariff_list').empty();
						var response_data = response.data;
							if( response.data.length == 0  )
							{
								$('#elec_tariff_list').html( '<option value="0">Please Select valid supplier</option>' );
							}else {
								$('#elec_tariff_list').html( '<option value="0">Please Select Tariff</option>' );
								console.log(response_data);
								for( i=0; i<response_data.length; i++ )
								{
								
											$('#elec_tariff_list').append( '<option value="'+response_data[i].tariffName+'" data-fuelType="'+response_data[i].fuelType+'">'+response_data[i].tariffName+'</option>' );
								}
							}
					}
					if(response.error == '1'){
						$('#elec_tariff_list').empty();
						$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
					if(response.error == '2'){
						$('#elec_tariff_list').empty();
							$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
				}
			});
	});

//get selected supplier for Tariff
	$(document).on('change','#gas_selected_supplier', function(){
		var elec_supplier = $(this).val();
		var quotation_for = $('#quotation_for').val();
		$.ajax({
				url: base_url+'index.php/quotation/get_tariff',
				type: 'post',
				dataType: 'json',
				data: { elec_supplier: elec_supplier, quotation_for: quotation_for, tariff_fuelType: 'gas' },
				beforeSend: function()
				{
					$('#gas_tariff_list').html('<option>Loading...</option>');
				},
				success:function(response)
				{
					console.log(response);
					$('#gas_selected_supplier').val(elec_supplier);
					if( response.error == '0' ){
						$('#gas_tariff_list').empty();
						var response_data = response.data;
							if( response.data.length == 0  )
							{
								$('#gas_tariff_list').html( '<option value="0">Please Select valid supplier</option>' );
							}else {
								$('#gas_tariff_list').html( '<option value="0">Please Select Tariff</option>' );
								for( i=0; i<response_data.length; i++ )
								{
											$('#gas_tariff_list').append( '<option value="'+response_data[i].tariffName+'" data-fuelType="'+response_data[i].fuelType+'">'+response_data[i].tariffName+'</option>' );
								}
							}
					}
					if(response.error == '1'){
						$('#gas_tariff_list').empty();
						$('#gas_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
					if(response.error == '2'){
						$('#gas_tariff_list').empty();
							$('#gas_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
				}
			});
	});

// get selected tariff and supplier
	$(document).on('click','#supplier_submit',function(){
		var selected_supplier = $('#elec_selected_supplier').find(':selected').val();
		var selected_tariff = $('#elec_tariff_list').find(':selected').val();
		var selected_tariff_fuelType = $('#elec_tariff_list').find(':selected').data('fueltype');
		var supplier_id = $('#elec_selected_supplier').find(':selected').data('supplierid');

		//gas
		var gas_selected_supplier = $('#gas_selected_supplier').find(':selected').val();
		var gas_selected_tariff = $('#gas_tariff_list').find(':selected').val();
		var gas_selected_tariff_fuelType = $('#gas_tariff_list').find(':selected').data('fueltype');
		var gas_supplier_id = $('#gas_selected_supplier').find(':selected').data('supplierid');

		if( $('#quotation_for').val() == 'both' )
		{
				if( selected_supplier == 0 || selected_tariff == 0 || gas_selected_supplier == 0 || gas_selected_tariff == 0 )
				{
					alert('Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/hear_about',
							type: 'post',
							beforeSend: function(){
								$('.loading').fadeIn();
								$('#main_content').fadeOut();
							},
							complete:function()
							{
								$('.loading').fadeOut();
								$('#main_content').fadeIn();
									$('#quotation_supplier').val(selected_supplier);
									$('#quotation_tariff').val(selected_tariff);
									$('#quotation_supplier_id').val(supplier_id);
									$('#selected_tariff_fuelType').val(selected_tariff_fuelType);

									// gas
									$('#gas_selected_supplier').val(gas_selected_supplier);
									$('#gas_selected_tariff').val(gas_selected_tariff);
									$('#gas_supplier_id').val(gas_supplier_id);
							},
							success:function(response)
							{
								$('#response').html(response);
							}
						});
				}
		}
		if( $('#quotation_for').val() == 'gas' ){
			if(  gas_selected_supplier == 0 || gas_selected_tariff == 0 )
				{
					alert('Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/hear_about',
							type: 'post',
							beforeSend: function(){
								$('.loading').fadeIn();
								$('#main_content').fadeOut();
							},
							complete:function()
							{
								$('.loading').fadeOut();
								$('#main_content').fadeIn();
									// $('#quotation_supplier').val(selected_supplier);
									// $('#quotation_tariff').val(selected_tariff);
									// $('#quotation_supplier_id').val(supplier_id);
									// $('#selected_tariff_fuelType').val(selected_tariff_fuelType);

									// gas
									$('#gas_selected_supplier').val(gas_selected_supplier);
									$('#gas_selected_tariff').val(gas_selected_tariff);
									$('#gas_supplier_id').val(gas_supplier_id);
							},
							success:function(response)
							{
								$('#response').html(response);
							}
						});
				}
		}
		if( $('#quotation_for').val() == 'electricity' ){
			if(  selected_supplier == 0 || selected_tariff == 0 )
				{
					alert('Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/hear_about',
							type: 'post',
							beforeSend: function(){
								$('.loading').fadeIn();
								$('#main_content').fadeOut();
							},
							complete:function()
							{
								$('.loading').fadeOut();
								$('#main_content').fadeIn();
									$('#quotation_supplier').val(selected_supplier);
									$('#quotation_tariff').val(selected_tariff);
									$('#quotation_supplier_id').val(supplier_id);
									$('#selected_tariff_fuelType').val(selected_tariff_fuelType);
							},
							success:function(response)
							{
								$('#response').html(response);
							}
						});
				}
		}
	});

//get value of hear_about_us
	$(document).on('click', '.hear_about_us', function(){
		var headabout = $(this).data('hearabout');
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete: function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

//know current usage
	$(document).on('click', '.know_current_usage',function(){
		var know_current_usage = $(this).data('know_current_usage');
		var quotation_for = $('#quotation_for').val();
		//alert(quotation_for);
		if( know_current_usage == 'kwh' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();

			$.ajax({
					url: base_url+'index.php/quotation/enter_energy',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_usage').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elec_usage').css('display','none');
						// }
						if( quotation_for == 'electricity' ){
							$('#gas_usage').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_usage').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);
						$('#response').html(response);
					}
				});
		}

		if( know_current_usage == 'spend' ){
			$.ajax({
					url: base_url+'index.php/quotation/enter_spend',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
						if( quotation_for == 'electricity' ){
							$('#gas_spend').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_spend').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);
						$('#response').html(response);
					}
				});
		}


		if(know_current_usage == 'no'){

			$.ajax({
					url: base_url+'index.php/quotation/home_type',
					type: 'post',
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);
						$('#response').html(response);
					}
				});
		}
	});

// back to current energy 
	$(document).on('click','#backtocurrentenergy', function(){
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete: function()
				{
					$('.loading').fadeOut();
	$('#main_content').fadeIn();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});


// back to hear about us
	$(document).on('click','.backtohearabout',function(){
		$.ajax({
				url: base_url+'index.php/quotation/hear_about',
				type: 'post',
				beforeSend: function(){
	$('.loading').fadeIn();
	$('#main_content').fadeOut();
},
				complete:function()
				{
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
						$('#quotation_supplier').val(selected_supplier);
						$('#quotation_tariff').val(selected_tariff);
				},
				success:function(response)
				{
					$('#response').html(response);
				}
			});
	})

//back to know energy usage_select
	$(document).on('click','.backknowusage', function(){
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
	$('.loading').fadeIn();
	$('#main_content').fadeOut();
},
				complete: function()
				{
					$('.loading').fadeOut();
	$('#main_content').fadeIn();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

//get energy usage
	$(document).on('click','#enter_usage_form',function(){
		var elec_usage = $('#enter_elec_usage').val();
		var elec_usage_select = $('.elec_usage_select').find(':selected').val();

		//gas
		var gas_enter_usage = $('#enter_gas_usage').val();
		var gas_interval = $('.gas_usage_select').find(':selected').val();

		//get supplier and tariff of electricity
		var quotation_supplier = $('#quotation_supplier').val();
		var quotation_tariff = $('#quotation_tariff').val();
		var quotation_pay_energy = $('#quotation_pay_energy').val();

		//get gas supplier and tariff
		var gas_supplier = $('#gas_selected_supplier').val();
		var gas_tariff = $('#gas_selected_tariff').val();
		var gas_supplierid = $('#gas_supplier_id').val();

		if( $('#quotation_for').val() == 'electricity' ){
			if( !elec_usage || elec_usage_select == "0" ){
				alert('All fields are requied');
			}else{
				$('#quotation_elec_energy_usage').val(elec_usage)
				$('#quotation_elec_energy_interval').val(elec_usage_select);

				$('#quotation_form').trigger('submit');
				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { elec_usage: elec_usage, elec_usage_select: elec_usage_select },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}
		}

		if( $('#quotation_for').val() == 'gas' ){
			if( !gas_enter_usage || gas_interval == "0" ){
				alert('All fields are requied');
			}else{
				$('#quotation_gas_energy_usage').val(gas_enter_usage);
				$('#quotation_gas_energy_interval').val(gas_interval);

				$('#quotation_form').trigger('submit');
				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { gas_enter_usage: gas_enter_usage, gas_interval: gas_interval },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}
		}
		if( $('#quotation_for').val() == 'both' ){
			if( !elec_usage || elec_usage_select == "0" || !gas_enter_usage || gas_interval == "0" ){
				alert('All fields are requied');
			}else{
				$('#quotation_elec_energy_usage').val(elec_usage);
				$('#quotation_elec_energy_interval').val(elec_usage_select);
				//gas
				$('#quotation_gas_energy_usage').val(gas_enter_usage)
				$('#quotation_gas_energy_interval').val(gas_interval);
				$('#quotation_form').trigger('submit');

				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { elec_usage: elec_usage, elec_usage_select: elec_usage_select, gas_enter_usage: gas_enter_usage, gas_interval: gas_interval },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}	
		}


	});


	$(document).on('submit','#enter_spend_form', function(e){
		e.preventDefault();
		var elec_spend = $('#output_elec_range').val();
		var elec_spend_interval = $('#elec_spend_interval').find(':selected').val();

		var gas_spend = $('#output_gas_range').val();
		var gas_spend_interval = $('#gas_spend_interval').find(':selected').val();
		//alert(elec_spend_interval+'==='+gas_spend_interval);

				if( $('#quotation_for').val() == 'electricity' ){
					if( !elec_spend ){
							alert('All fields are requied');
						}else{
							$('#existingSpendElec').val(elec_spend);
							$('#existingSpendIntervalElec').val(elec_spend_interval);
							$('#quotation_form').trigger('submit');
						}

				}
				if( $('#quotation_for').val() == 'gas' ){
					if( !gas_spend ){
							alert('All fields are requied');
						}else{
							$('#existingSpendGas').val(gas_spend);
							$('#existingSpendIntervalGas').val(gas_spend_interval);
							$('#quotation_form').trigger('submit');
						}
				}
				if( $('#quotation_for').val() == 'both' ){
						if( !elec_spend || !gas_spend ){
							alert('All fields are requied');
						}else{
							$('#existingSpendElec').val(elec_spend);
							$('#existingSpendIntervalElec').val(elec_spend_interval);
							//gas
							$('#existingSpendGas').val(gas_spend)
							$('#existingSpendIntervalGas').val(gas_spend_interval);
							$('#quotation_form').trigger('submit');
						}
				}
	});

//get static energy usage
	$(document).on('click','.home_type',function(){
			var typeOfHomeElec = $(this).data('hometype');
			// get number of people template
			$.ajax({
				url: base_url+'index.php/quotation/people_live',
				type: 'post',
				beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete:function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
					$('#typeOfHomeElec').val(typeOfHomeElec);
				},
				success:function(response){
					$('#response').html(response);
				}
			});
	});

//get number of people and redirect to bedroom template.
	// $(document).on('submit','#formpeoplelive',function(e){
	// 	e.preventDefault();
	// 	if( $('#js-amount-input').val() == '0' || $('#js-amount-input').val() == ' ' ){
	// 		alert('Please select valid number of people');
	// 	}else{
	// 		var selected_people = $('#js-amount-input').val();
	// 		$.ajax({
	// 			url: base_url+'index.php/quotation/number_bedroom',
	// 			type: 'post',
	// 			beforeSend:function(){
	// 				$('.loading').fadeIn();
	// 				$('#main_content').fadeOut();	
	// 			},
	// 			complete:function(){
	// 				$('.loading').fadeOut();
	// 				$('#main_content').fadeIn();
	// 				$('#peopleElec').val(selected_people);	
	// 			},success:function(response){
	// 				$('#response').html(response);
	// 			}
	// 		});
	// 	}
	// })

// back to home type template
	$(document).on('click','#backtohometype',function(){
		$.ajax({
			url: base_url+'index.php/quotation/home_type',
			type: 'post',
			beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete:function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	});

// get back to people live
	$(document).on('click','#backtopeople', function(){
		$.ajax({
			url: base_url+'index.php/quotation/people_live',
			type: 'post',
			beforeSend: function(){
					$('.loading').fadeIn();
					$('#main_content').fadeOut();
				},
				complete:function(){
					$('.loading').fadeOut();
					$('#main_content').fadeIn();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	})

//submit quotation values
	$(document).on('submit', '#quotation_form', function(e){
			e.preventDefault();

			//alert( $('#quotation_form').serialize() );
			// $.ajax({
			// 		url: base_url+'index.php/quotation/summary',
			// 		type: 'post',
			// 		data: $('#quotation_form').serialize(),
			// 		complete: function()
			// 		{
			// 			$('#quotation_hearabout').val( headabout );
			// 		},
			// 		success:function(response)
			// 		{
			// 				$('#response').html(response);
			// 		}
			// 	});
			var elec_usage = $('#enter_elec_usage').val();
			var elec_usage_select = $('.elec_usage_select').find(':selected').val();
			//get supplier and tariff
			var quotation_supplier = $('#quotation_supplier').val();
			var quotation_tariff = $('#quotation_tariff').val();
			var quotation_pay_energy = $('#quotation_pay_energy').val();

			$.ajax({
					url: base_url+'index.php/quotation/summary',
					type: 'post',
					data: $('#quotation_form').serialize() + "&elec_usage="+elec_usage+ "&quotation_supplier="+ quotation_supplier + "&quotation_tariff=" +quotation_tariff + "&quotation_pay_energy="+quotation_pay_energy,
					beforeSend: function(){
						$('.loading').fadeIn();
						$('#main_content').fadeOut();
					},
					complete: function()
					{
						$('.loading').fadeOut();
						$('#main_content').fadeIn();
					//	$('#quotation_hearabout').val( headabout );
					},
					success:function(response)
					{
							console.log(response);
							if( response == '3042' ){
								$('#backend_error').html('<center><div class="alert alert-danger" role="alert">Error! The search criteria provided is not recognised by our system.</div></center>');
							}else {
								$('#response').html(response);
							}
					}
				});
	});


	$(document).on('click', '.home_service_plan', function(){
			var get_homeserviceprice = $(this).data('homeserviceprice');
			var plan_number = $(this).data('planid');
			var orginial_price = $('#orginial_price').val()
			var total_price = (parseFloat(orginial_price) + parseFloat(get_homeserviceprice)).toFixed(2);
			$('#total_price').text(total_price);

			$('#plan_price').val(get_homeserviceprice);
			$('#complete_price').val(total_price);
			$('#plan_id').val(plan_number);
			$('#original_price').val(orginial_price);
			
			$('#select_plan_'+plan_number).css('display','none');
			$('#selected_plan_'+plan_number).css('display','block');
	});

// redirect to signup
	// $(document).on('submit','#quote_summary_form',function(e){
	// 	e.preventDefault();
	// 	window.location.href=base_url+'index.php/user/signup';
	// });

$(document).on('change','#value_elec_range',function(){
	$('#output_elec_range').val($(this).val());
});

$(document).on('change','#value_gas_range',function(){
	$('#output_gas_range').val($(this).val());
});


// select how many people live
$(document).on('click','.people_live', function(){
	var peopleElec = $(this).data('numberpeople');
	if( peopleElec == '0' || peopleElec ==' ' || peopleElec=='undefined' || peopleElec=='undefine' ){
		alert('Please select valid option');
	}else{
		$.ajax({
			url: base_url+'index.php/quotation/number_bedroom',
			type: 'post',
			beforeSend:function(){
				$('.loading').fadeIn();
				$('#main_content').fadeOut();	
			},
			complete:function(){
				$('.loading').fadeOut();
				$('#main_content').fadeIn();
				$('#peopleElec').val( peopleElec );
			},success:function(response){
				$('#response').html(response);
			}
		});
	}
	
});

// select how many bedroom
$(document).on('click','.bedroom_range', function(){
	var numberbedroom = $(this).data('numberbedroom');
	if( numberbedroom == '0' || numberbedroom ==' ' || numberbedroom=='undefined' || numberbedroom=='undefine' ){
		alert('Please select valid option');
	}else{
		$('#bedroomsElec').val( numberbedroom );
		$('#quotation_form').trigger('submit');
	}

});

});

