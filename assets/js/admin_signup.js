var base_url = window.location.hostname === 'localhost' || 'staging.eversmartenergy.co.uk' ? 'http://' + window.location.host + '/' : 'https://' + window.location.host + '/';
var stripe = Stripe('pk_test_1iPyX53CapEIo0TAkpNwR8hb');

var elements = stripe.elements(); // Create an instance of Elements.

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
    base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

var quotation = {};

// for dd
// Create an instance of the card Element.
var card = elements.create('cardNumber', {
    style: style
});
var dd_ccv = elements.create('cardCvc', {
    style: style
});
var dd_expire = elements.create('cardExpiry', {
    style: style
});
// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');
dd_ccv.mount('#card-dd_ccv');
dd_expire.mount('#card-dd_expire');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function (event) {
    console.log(event);
    //alert(event);
    var displayError = document.getElementById('card-errors_dd');
    if (event.error) {
        displayError.textContent = event.error.message;
        displayError.style.display = "block";
    } else {
        displayError.style.display = "none";
        displayError.textContent = '';
    }
});

var form_ypp = document.getElementById('newcust_form');
if (form_ypp) {
    form_ypp.addEventListener('submit', function (event) {

        event.preventDefault();
        stripe.createToken(card).then(function (result) {

            console.log('---> result :', result);

            if (result.error) {

                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors_dd');
                errorElement.textContent = result.error.message;

            } else {
                // no errors token created
                $('#stripe_token').val(result.token.id);
                $('#submit-form').removeAttr('disabled');
            }
        });
    });
}

function logout_user() {

    $.ajax({
        url: base_url + 'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url + 'index.php/user/login';
            }
        }
    });
}

function wtcDate(num) {
    var d = new Date();
    d.setDate(d.getDate() + num);
    return d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
}

function REQUEST(url, resType, opts) {
    opts = opts || {};
    return fetch(url, opts)
        .then(function (res) {
            return resType === 'none' ? res : res[resType || 'json'];
        })
        .catch(console.error);
}

$(document).ready(function () {
    // initialise form elements
    $('#address_na').hide();
    $('#form-end').hide();
    $('#pay-type').hide();
    $('#ddpay').hide();
    $('#stripe').hide();
    $('#ddbank').hide();
    $('#quote-or').hide();
    $('#na-addcheck-0').hide();
    $('#contact-addcheck-0').hide();
    $('#na-add-check').hide();
    $('#add-check').hide();
    $('#mpan-existing').hide();
    $('#mprn-existing').hide();
    $('#main-mpan').hide();
    $('#main-mprn').hide();
    $('#submit-panel').hide();
    $('#newcust_form #quotefor').val('both');
    $('#submit-form').attr('disabled', true);

    // display list of our tariffs
    $.ajax({
        url: base_url + 'index.php/admin/our_tariffs',
        type: 'get',
        beforeSend: function () {
            console.log('before send');
            $('#tariffselect').html('Loading...');
        },
        success: function (response) {
            console.log('successful shit');
            console.log(response);
            $('#tariffselect').html(response);
        }
    });

    function formatDate(input) {
        var datePart = input.match(/\d+/g),
            year = datePart[0].substring(0),
            month = datePart[1],
            day = datePart[2];
        return day + '-' + month + '-' + year;
    }



        /*
        // populates mpan/mprn based on entered postcode
        $(document).on('change', '#quotation_address', function () {


            var selected_mprn = $(this).find(':selected').data('mprn');
            var selectedpostcode = $(this).find(':selected').data('selectedpostcode');
            var selected_address = $('#quotation_address').find(':selected').val();
            var selected_bnum = $(this).find(':selected').data('bnum');
            var selected_thor = $(this).find(':selected').data('thor');
            var selected_dplo = $(this).find(':selected').data('dplo');
            var selected_town = $(this).find(':selected').data('town');
            var selected_cntys = $(this).find(':selected').data('cnty');
            var meter_type = $(this).find(':selected').data('metertype');
            var selected_mpan = $(this).find(':selected').data('mpan');
            var elecmeter = $(this).find(':selected').data('elecmeter');
            var mpanmetertype = $(this).find(':selected').data('mpanmetertype');
            var gasmeter = $(this).find(':selected').data('gasmeter');
            var mpanmeterverb = $(this).find(':selected').data('mpanmeterverb');
            var boiler_cover = $(this).find(':selected').data('boiler_cover');


            $('#select_add').val(selected_address);

            // Grab MPAN and make sure it is a string
            var mpanlower = $(this).find(':selected').data('mpanlower');
            mpanlower = typeof mpanlower === 'string' ? mpanlower : mpanlower + '';

            // If there are multiple mpans (there's a comma)
            if (mpanlower.indexOf(',') > -1) {
                // Show them in a dropdown to select
                $.ajax({
                    url: base_url + 'index.php/admin/multiple_mpan',
                    type: 'post',
                    data: {
                        mpan: mpanlower
                    },
                    beforeSend: function () {
                        $('#multiple_mpan_select').html('Loading...');
                    },
                    success: function (response) {
                        $('#multiple_mpan_select').html(response);
                    }
                });
            } else {
                // Clear the select
                $('#multiple_mpan_select').html('');
            }

            $('#contact-addcheck-0').prop("checked", false);
            $('#contact-addcheck-0').show();
            $('#add-check').show();

            if (!meter_type) {
                metertype = 'STD';
            }

            var first_line_adds = selected_bnum + ' ' + selected_thor;

            $('#mpan-existing .line-sep').addClass('lsfix');

            if (mpanlower) {
                $('#mtrpoint-mpancheck-0').prop("checked", true);
                $("#mpan-found-main").text(mpanlower);
                $("#mpan-found").text(mpanlower);
                $("#mpan-serial-orig").text('Meter Serial: ' + elecmeter);
                $("#mpan-type-orig").text('Meter Type: ' + mpanmetertype + ' (' + mpanmeterverb + ')');
                $("#mpan-serial").text('Meter Serial: ' + elecmeter);
                $("#mpan-type").text('Meter Type: ' + mpanmetertype + ' (' + mpanmeterverb + ')');
                $('#mpan-existing').show();
            }

            if ($('#selected_mpan').val()) {
                $('#mtrpoint-mpancheck-0').prop("checked", true);
                $("#mpan-found-main").text($('#selected_mpan').val());
                $("#mpan-found").text($('#selected_mpan').val());
                $('#mpan-existing').show();
            }

            if (selected_mprn) {
                $('#mtrpoint-mprncheck-0').prop("checked", true);
                $("#mprn-found-main").text(selected_mprn);
                $("#mprn-found").text(selected_mprn);
                $("#mprn-serial-orig").text('Meter Serial: ' + gasmeter);
                $("#mprn-serial").text('Meter Serial: ' + gasmeter);
                $('#mprn-existing').show();
            }

            // populate fields of hidden form with entered info
            $('#newcust_form #senderReference').val('Eversmart');
            $('#newcust_form #reference').val('Eversmart');
            $('#newcust_form #billingEntityCode').val('Eversmart');
            $('#newcust_form #marketingOptOutFl').val('false');
            $('#newcust_form #submittedSource').val('Tele');
            $('#newcust_form #changeOfTenancyFl').val('false');
            $('#newcust_form #primaryContact').val('true');
            $('#newcust_form #elecPrevSupplier').val('British Gas');
            $('#newcust_form #elecPrevTariff').val('Standard');
            $('#newcust_form #address_address1').val(first_line_adds);
            $('#newcust_form #address_address2').val(selected_dplo);
            $('#newcust_form #address_town').val(selected_town);
            $('#newcust_form #address_county').val(selected_cntys);
            $('#newcust_form #address_postcode').val(selectedpostcode);
            $('#newcust_form #address_countryCode').val('GB');
            $('#newcust_form #meterTypeElec').val(meter_type);
            $('#newcust_form #elecMeterSerial').val(elecmeter);
            $('#newcust_form #gasMeterSerial').val(gasmeter);
            $('#newcust_form #mpans').val(selected_mpan);
            $('#newcust_form #mprns').val(selected_mprn);
            $('#newcust_form #mpancore').val(mpanlower);
            $('#newcust_form #title').val('Mr');
            $('#newcust_form #boiler_cover').val(boiler_cover);

            if ($('#mtrpoint-mpancheck-0').prop("checked") == true && $('#mtrpoint-mprncheck-0').prop("checked") == true) {
                $('#newcust_form #quotefor').val('both');
            }

            if ($('#mtrpoint-mpancheck-0').prop("checked") == true && $('#mtrpoint-mprncheck-0').prop("checked") == false) {
                $('#newcust_form #quotefor').val('electricity');
            }

            if ($('#mtrpoint-mpancheck-0').prop('checked') == true || $('#mtrpoint-mpancheck-main').prop('checked') == true) {
                $('#mtrpoint-mpan').attr('readonly', true);
            } else {
                $('#mtrpoint-mpan').attr('readonly', false);
            }

            if ($('#mtrpoint-mprncheck-0').prop('checked') == true || $('#mtrpoint-mprncheck-main').prop('checked') == true) {
                $('#mtrpoint-mprn').attr('readonly', true);
            } else {
                $('#mtrpoint-mprn').attr('readonly', false);
            }

            if (!gasmeter) {
                $('.gasdropdownlist').css('display', 'block');
            }
        });
        */

    $(document).on('change', '#quotation_address_gas', function () {
        var selected_mprn = $(this).find(':selected').data('gasmprn_gas');
        var gasmeter = $(this).find(':selected').data('gasmeter_gas');

        if (selected_mprn && gasmeter) {
            if (gasmeter.indexOf(',') > -1) {
                $('#newcust_form #mprns').val('');
                $('#mtrpoint-mprncheck-0').prop("checked", false);
                $("#mprn-found-main").text('');
                $("#mprn-found").text('');
                $('#mprn-existing').hide();
                $('#newcust_form #gasMeterSerial').val('');
                $('.multiplemeters').css('display', 'block');
            } else {
                $('#newcust_form #mprns').val(selected_mprn);
                $('#mtrpoint-mprncheck-0').prop("checked", true);
                $("#mprn-found-main").text(selected_mprn);
                $("#mprn-found").text(selected_mprn);
                $('#mprn-existing').show();
                $('#newcust_form #gasMeterSerial').val(gasmeter);
                $('#mprn-serial').text('Meter Serial: ' + gasmeter);
            }
        } else {
            $('#newcust_form #mprns').val('');
            $('#mtrpoint-mprncheck-0').prop("checked", false);
            $("#mprn-found-main").text('');
            $("#mprn-found").text('');
            $('#mprn-existing').hide();
            $('#newcust_form #gasMeterSerial').val('');
            $('.missingmeters').css('display', 'block');
        }

        if ($('#mtrpoint-mprncheck-0').prop('checked') == true || $('#mtrpoint-mprncheck-main').prop('checked') == true) {
            $('#mtrpoint-mprn').attr('readonly', true);
        } else {
            $('#mtrpoint-mprn').attr('readonly', false);
        }
    });

    // multiple mpan select - deal with comma split
    $(document).on('change', '#selected_mpan', function () {
        var mpanlower = $('#address_select').find(':selected').data('mpanlower');
        var selected_mpan = $(this).val();
        var mpan = $('#address_select').find(':selected').data('mpan');
        var mpanList = mpan.split(',');
        var elecmeter = $('#address_select').find(':selected').data('elecmeter');
        var elecmeterList = elecmeter.split(',');

        if (mpanlower) {
            var selected = $(this)[0].selectedIndex;

            $('#mpan-existing .line-sep').addClass('lsfix');
            $('#mtrpoint-mpancheck-0').prop("checked", true);
            $("#mpan-found-main").text(selected_mpan);
            $("#mpan-found").text(selected_mpan);

            $('#newcust_form #mpancore').val(selected_mpan);
            $('#mpan-existing').show();

            if (selected === 1) {
                $('#newcust_form #mpans').val(mpanList[0]);
                $('#newcust_form #elecMeterSerial').val(elecmeterList[0]);
            }
            if (selected === 2) {
                $('#newcust_form #mpans').val(mpanList[1]);
                $('#newcust_form #elecMeterSerial').val(elecmeterList[1]);
            }
        }
    });

    // multiple mprn select - deal with comma split
    $(document).on('change', '#selected_mprn', function () {
        var mprn = $('#address_select').find(':selected').data('mprn');
        var selected_mprn = $(this).val();
        var mprnList = mprn.split(',');
        var gasmeter = $('#address_select').find(':selected').data('gasmeter');
        var gasmeterList = gasmeter.split(',');

        if (mprn) {
            var selected = $(this)[0].selectedIndex;

            $('#mprn-existing .line-sep').addClass('lsfix');
            $('#mtrpoint-mprncheck-0').prop("checked", true);
            $("#mprn-found-main").text(selected_mprn);
            $("#mprn-found").text(selected_mprn);

            $('#newcust_form #mprns').val(selected_mprn);
            $('#mprn-existing').show();

            if (selected === 1) {
                $('#newcust_form #mprns').val(mprnList[0]);
                $('#newcust_form #gasMeterSerial').val(gasmeterList[0]);
            }
            if (selected === 2) {
                $('#newcust_form #mprns').val(mprnList[1]);
                $('#newcust_form #gasMeterSerial').val(gasmeterList[1]);
            }
        }
    });

    // populates tariff fields on hidden form when selected from drop-down & amends fields based on which tariff is selected
    $(document).on('change', '#tariffselect', function () {
        $('#mtrpoint-mpan-eac').prop('selectedIndex', 0);
        $('#tariff-pay').prop('selectedIndex', 0);
        $('#newcust_form #eac').val('');
        $('#newcust_form #firstpaymentid').val('');
        $('#eac-override').val('');
        $('#aq-override').val('');
        $('#newcust_form #aq').val('');
        $('#admin_quote').html('');
        $('#submit-panel').hide();
        $('#elec_ur').val('');
        $('#elec_sc').val('');
        $('#gas_ur').val('');
        $('#gas_sc').val('');
        $('#newspend').val('');
        $('#newelec').val('');
        $('#newgas').val('');
        var strTar = $('#tariffselect option:selected').val();
        var paytype = $('#tariffselect').find(':selected').data('paytype');
        var fueltype = $('#tariffselect').find(':selected').data('fueltype');

        $('#newcust_form #tariff').val(strTar);
        $('#meter_details').hide();

        $('#gas_address_select').hide();
        $('#gas_quotation_address').html();

        $('#meter_details').hide();

        if (strTar.includes('Family Saver Club')) {
            $('#newcust_form #monthyear').val('yearlypay');
            $('#pay-type').show();
            $('#quote-or').show();
        } else {
            $('#newcust_form #monthyear').val('monthlypay');
            $('#pay-type').hide();
            $('#quote-or').hide();
        }

        if (paytype == '5') {
            $('#newcust_form #billingMethod').val('prepay');
            $('#ddbank').hide();
        } else {
            $('#newcust_form #billingMethod').val('directdebit');
            $('#ddbank').show();
        }

        if (fueltype == 'elecgas') {
            if ($('#mtrpoint-mprncheck-0').prop('checked') == false) {
                var selected_mprn = $('#address_select').find(':selected').data('mprn');
                $('#newcust_form #mprns').val(selected_mprn);
            }
            $('#newcust_form #quotefor').val('both');
            $('#mtrpoint-mprn-aq').attr('disabled', false);
            $('#aq-override').attr('readonly', false);
        }

        if (fueltype == 'elec') {
            if ($('#mtrpoint-mprncheck-0').prop('checked') == true) {
                $('#main-mprn').show();
                $('#mprn-existing').hide();

                $('#mtrpoint-mprncheck-0').prop("checked", false);
                $('#newcust_form #gasProductCode').val('');
                $('#newcust_form #mprns').val('');
            }
            $('#newcust_form #quotefor').val('electricity');
            $('select[name="mtrpoint-mprn-aq"]').val('default');
            $('#newcust_form #aq').val('');
            $('#newcust_form #gasMeterSerial').val('');
            $('#mtrpoint-mprn-aq').attr('disabled', true);
            $('#aq-override').attr('readonly', true);
        }
    });

    // pay types for Family Saver Club tariff
    $(document).on('change', '#tariff-pay', function () {
        var tarPay = $('#tariff-pay option:selected').val();

        switch (tarPay) {
            case '2':
                $('#ddbank').show();
                $('#stripe').hide();
                $('#newcust_form #firstpaymentid').val('');
                break;
            case '3':
            case '4':
                $('#ddbank,#stripe').hide();
                $('#newcust_form #firstpaymentid').val(tarPay);
                break;
            case '5':
                $('#ddbank').show();
                $('#stripe').hide();
                $('#newcust_form #firstpaymentid').val(tarPay);
                break;
            case '6': // stripe
                $('#ddbank').hide();
                $('#stripe').show();
                $('#newcust_form #firstpaymentid').val(tarPay);
                break;
            default:
                $('#ddbank').show();
                $('#newcust_form #firstpaymentid').val(tarPay);
                break;
        }
    });

    // updates hidden form fields as fields are being entered
    $('#contact-title').change(function () {
        $('#newcust_form #title').val($('#contact-title option:selected').val());
    });
    $('#contact-fname').change(function () {
        $('#newcust_form #forename').val($('#contact-fname').val());
        $('#newcust_form #address_careOf').val($('#contact-fname').val() + ' ' + $('#contact-lname').val());
    });
    $('#contact-lname').change(function () {
        $('#newcust_form #surname').val($('#contact-lname').val());
        $('#newcust_form #address_careOf').val($('#contact-fname').val() + ' ' + $('#contact-lname').val());
    });
    $('#contact-title').change(function () {
        $('#newcust_form #title').val($('#contact-title').val());
    });
    $('#contact-email').change(function () {
        $('#newcust_form #email').val($('#contact-email').val());
    });

    $('#contact-email').on('focusout', function () {
        $(this).val().length > 3 && $('#newCustForm').removeClass('hide');
    });

    $('#contact-email').on('keyup', function () {
        $(this).val().length < 3 && $('#newCustForm').addClass('hide');
    });
    $('#contact-postcode').on('keyup', function (evt) {
        if (evt.keyCode === 13 || evt.which === 13) {
            $('#contact-lookup').trigger('click');
        }
    });
    $('#mtrpoint-mpan').on('keyup', function () {
        $(this).val().length > 12 ? $('#mtrpoint-mpanadd').attr('disabled', false) : $('#mtrpoint-mpanadd').attr('disabled', true);
    });
    $('#mtrpoint-mprn').on('keyup', function () {
        $(this).val().length > 9 ? $('#mtrpoint-mprnadd').attr('disabled', false) : $('#mtrpoint-mprnadd').attr('disabled', true);
    });
    $('#contact-tel').change(function () {
        $('#newcust_form #phone2').val($('#contact-tel').val());
    });
    $('#contact-mobile').change(function () {
        $('#newcust_form #phone1').val($('#contact-mobile').val());
    });
    $('#contact-dob').change(function () {
        $('#newcust_form #dateOfBirth').val(formatDate($('#contact-dob').val()));
    });

    $('#bank-bname').change(function () {
        $('#newcust_form #bankAccountName').val($('#bank-bname').val());
    });
    $('#bank-bnum').change(function () {
        $('#newcust_form #bankAccountNumber').val($('#bank-bnum').val());
    });
    $('#bank-sort').change(function () {
        var sc = $('#bank-sort').val();
        var scEdit = sc.replace(/(\d{2})(\d{2})(\d{2})/, "$1-$2-$3");
        $('#newcust_form #bankAccountSortCode').val(scEdit);
    });

    if ($('#mtrpoint-mpancheck-0').prop('checked') == true || $('#mtrpoint-mpancheck-main').prop('checked') == true) {
        $('#mtrpoint-mpan').attr('readonly', true);
    } else {
        $('#mtrpoint-mpan').attr('readonly', false);
    }

    if ($('#mtrpoint-mprncheck-0').prop('checked') == true || $('#mtrpoint-mprncheck-main').prop('checked') == true) {
        $('#mtrpoint-mprn').attr('readonly', true);
    } else {
        $('#mtrpoint-mprn').attr('readonly', false);
    }

    if (!$('#address_na').is(':visible')) {
        $('#newcust_form #senderReference').val('Eversmart');
        $('#newcust_form #reference').val('Eversmart');
        $('#newcust_form #billingEntityCode').val('Eversmart');
        $('#newcust_form #marketingOptOutFl').val('false');
        $('#newcust_form #submittedSource').val('Tele');
        $('#newcust_form #changeOfTenancyFl').val('false');
        $('#newcust_form #primaryContact').val('true');
        $('#na-add1').change(function () {
            $('#newcust_form #address_address1').val($('#na-add1').val());
        });
        $('#na-add2').change(function () {
            $('#newcust_form #address_address2').val($('#na-add2').val());
        });
        $('#na-addtown').change(function () {
            $('#newcust_form #address_town').val($('#na-addtown').val());
        });
        $('#na-addcounty').change(function () {
            $('#newcust_form #address_county').val($('#na-addcounty').val());
        });
        $('#na-pcod').change(function () {
            $('#newcust_form #address_postcode').val($('#na-pcod').val());
        });
        $('#newcust_form #address_countryCode').val('GB');
    }

    $('#na-add1').on('input', function () {
        $('#na-addcheck-0').prop('checked', false);
    });
    $('#na-add2').on('input', function () {
        $('#na-addcheck-0').prop('checked', false);
    });
    $('#na-addtown').on('input', function () {
        $('#na-addcheck-0').prop('checked', false);
    });
    $('#na-addcounty').on('input', function () {
        $('#na-addcheck-0').prop('checked', false);
    });
    $('#na-pcod').on('input', function () {
        $('#na-addcheck-0').prop('checked', false);
    });

    $('#quote-override').change(function () {
        $('#newcust_form #newspend').val($('#quote-override').val());
    });

    $('#eac-override').change(function () {
        $('#newcust_form #eac').val($('#eac-override').val());
        $('select[name="mtrpoint-mpan-eac"]').val('default');
    });
    $('#aq-override').change(function () {
        $('#newcust_form #aq').val($('#aq-override').val());
        $('select[name="mtrpoint-mpan-aq"]').val('default');
    });

    $('#newcust_form #newspend').change(function () {
        $('#amount_pay').val($('#newcust_form #newspend').val());
    });

    $('input[name="cardnumber"]').change(function () {
        $('#newcust_form #card_no').val($('input[name="cardnumber"]').val());
    });
    $('#card_full_name').change(function () {
        $('#newcust_form #full_name').val($('#card_full_name').val());
    });
    $('input[name="exp-date"]').change(function () {
        $('#newcust_form #card_no').val($('input[name="exp-date"]').val());
    });
    $('input[name="cvc"]').change(function () {
        $('#newcust_form #card_no').val($('input[name="cvc"]').val());
    });

    $('#contact-email').focusout(function () {
        $('#contact-email').filter(function () {
            var email = $('#contact-email').val();
            var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!emailReg.test(email)) {
                alert('Please enter a valid email address');
            }
        })
    });

    // passed data from lead page
    if ($('#lead_flag').val() === '1') {
        $('#eac-override').val($('#lead_eac').val());
        $('#newcust_form #eac').val($('#lead_eac').val());
        $('#aq-override').val($('#lead_aq').val());
        $('#newcust_form #aq').val($('#lead_aq').val());
        var date = $('#lead_dob').val();
        var newdate = date.split("-").reverse().join("-");
        $('#contact-title').val($('#lead_title').val());
        $('#newcust_form #title').val($('#lead_title').val());
        $('#contact-fname').val($('#lead_forename').val());
        $('#newcust_form #forename').val($('#lead_forename').val());
        $('#contact-lname').val($('#lead_surname').val());
        $('#newcust_form #surname').val($('#lead_surname').val());
        $('#newcust_form #address_careOf').val($('#lead_forename').val() + ' ' + $('#lead_surname').val());
        $('#contact-dob').val(newdate);
        $('#newcust_form #dateOfBirth').val($('#lead_dob').val());
        $('#contact-email').val($('#lead_email').val());
        $('#newcust_form #email').val($('#lead_email').val());
        $('#contact-tel').val($('#lead_phone').val());
        $('#newcust_form #phone1').val($('#lead_phone').val());
        $('#contact-postcode').val($('#lead_postcode').val());
        $('#contact-lookup').click();
    }
});

$(document).on('click', '#check_msn_btn', function () {
    var Errors = [];
    if ($('#contact-postcode').val() == '') {
        Errors += " - Please enter a postcode\n";
    }
    if ($('#checkinput').val() == '') {
        Errors += " - Please enter a meter serial number\n";
    }
    if (Errors.length > 0) {
        alert('Please check the following:\n\n' + Errors);
    } else {
        var search_params = $('#contact-postcode, #checkinput').serialize();
        $.ajax({
            url: base_url + 'index.php/admin/admin_getmprn_energylinx',
            type: 'post',
            data: search_params,
            success: function (response) {
                response = JSON.parse(response);
                if (response.error == '1') {
                    $('#msn_msg').html('<div style="margin-top:10px;padding:0px;text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="padding-top:10px;text-align:center;font-size:12px;font-weight:400;"> ' + response.msg + ' </p></div>');
                } else {
                    $('#msn_msg').html('<div style="margin-top:10px;padding:0px;text-align:center" class="alert alert-success" role="alert"><p style="padding-top:10px;text-align:center;font-size:12px;font-weight:500;"> ' + response.msg + ' </p></div>');
                    $('#mtrpoint-mprncheck-0').prop("checked", true);
                    $('#newcust_form #quotefor').val('both');
                    $('#mprn-found-main').text(response.mprn);
                    $('#mprn-found').text(response.mprn);
                    $('#mprn-existing').show();
                    $('#newcust_form #mprns').val(response.mprn);
                    $('#newcust_form #gasMeterSerial').val(response.gasserial);
                    $('#mprn-serial').text('Meter Serial: ' + response.gasserial);
                }
            }
        });
    }
});

$(document).on('click', '#check_msn_btn2', function () {
    var Errors = [];
    if ($('#contact-postcode').val() == '') {
        Errors += " - Please enter a postcode\n";
    }
    if ($('#checkinput2').val() == '') {
        Errors += " - Please enter a meter serial number\n";
    }
    if (Errors.length > 0) {
        alert('Please check the following:\n\n' + Errors);
    } else {
        var search_params = $('#contact-postcode, #checkinput2').serialize();
        $.ajax({
            type: 'post',
            url: base_url + 'index.php/admin/admin_getmprn_junifer',
            data: search_params,
            success: function (response) {
                response = JSON.parse(response);
                if (response.error == '1') {
                    $('#msn2_msg').html('<div style="margin-top:10px;padding:0px;text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="padding-top:10px;text-align:center;font-size:12px;font-weight:400;"> ' + response.msg + ' </p></div>');
                } else {
                    $('#msn2_msg').html('<div style="margin-top:10px;padding:0px;text-align:center" class="alert alert-success" role="alert"><p style="padding-top:10px;text-align:center;font-size:12px;font-weight:500;"> ' + response.msg + ' </p></div>');
                    $('#mtrpoint-mprncheck-0').prop("checked", true);
                    $('#newcust_form #quotefor').val('both');
                    $('#mprn-found-main').text(response.mprn);
                    $('#mprn-found').text(response.mprn);
                    $('#mprn-existing').show();
                    $('#newcust_form #mprns').val(response.mprn);
                    $('#newcust_form #gasMeterSerial').val(response.gasserial);
                    $('#mprn-serial').text('Meter Serial: ' + response.gasserial);
                }
            }
        });
    }
});

function copyText() {
    var copyText = document.getElementById('select_add');
    copyText.select();
    document.execCommand('copy');
}

// validation - only numbers
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        return true;
    }
}

// change eac on form when selecting Home Type Elec
$("#mtrpoint-mpan-eac").change(function () {
    $('#eac-override').val('');
    var eac = $('#eac');
    var eac_override = $('#eac-override');
    var aq_override = $('#aq-override');
    var elec_consum = $("#mtrpoint-mpan-eac option:selected").val();
    switch (elec_consum) {
        case 'default':
            eac.val('');
            eac_override.val('');
            break;
        case '2':
            eac.val('1600');
            eac_override.val('1600');
            break;
        case '3':
            eac.val('1700');
            eac_override.val('1700');
            break;
        case '4':
            eac.val('1900');
            eac_override.val('1900');
            break;
        case '5':
            eac.val('1900');
            eac_override.val('1900');
            break;
        case '6':
            eac.val('2800');
            eac_override.val('2800');
            break;
        case '7':
            eac.val('2900');
            eac_override.val('2900');
            break;
        case '8':
            eac.val('3100');
            eac_override.val('3100');
            break;
        case '9':
            eac.val('3100');
            eac_override.val('3100');
            break;
        case '10':
            eac.val('4300');
            eac_override.val('4300');
            break;
        case '11':
            eac.val('4400');
            eac_override.val('4400');
            break;
        case '12':
            eac.val('4600');
            eac_override.val('4600');
            break;
        case '13':
            eac.val('4600');
            eac_override.val('4600');
            break;
    }

    if ($('#newcust_form #quotefor').val() === 'both') {
        $('#aq-override').val('');
        $('#mtrpoint-mprn-aq').val(elec_consum);
        var aq = $('#aq');
        var gas_consum = $('#mtrpoint-mprn-aq option:selected').val();
        switch (gas_consum) {
            case 'default':
                aq.val('');
                aq_override.val('');
                break;
            case '2':
                aq.val('7400');
                aq_override.val('7400');
                break;
            case '3':
                aq.val('7600');
                aq_override.val('7600');
                break;
            case '4':
                aq.val('8000');
                aq_override.val('8000');
                break;
            case '5':
                aq.val('8000');
                aq_override.val('8000');
                break;
            case '6':
                aq.val('11400');
                aq_override.val('11400');
                break;
            case '7':
                aq.val('11600');
                aq_override.val('11600');
                break;
            case '8':
                aq.val('12000');
                aq_override.val('12000');
                break;
            case '9':
                aq.val('12000');
                aq_override.val('12000');
                break;
            case '10':
                aq.val('16400');
                aq_override.val('16400');
                break;
            case '11':
                aq.val('16600');
                aq_override.val('16600');
                break;
            case '12':
                aq.val('17000');
                aq_override.val('17000');
                break;
            case '13':
                aq.val('17000');
                aq_override.val('1700');
                break;
        }
    }
});

// postcode search for address lookup
$('#contact-lookup').click(function (evt) {
    evt.preventDefault();
    $('#admin_signup_msg').html('');
    $('#update_result').html('');
    $('#multiple_mpan_select').html('');
    $('#multiple_mprn_select').html('');
    $('#contact-addcheck-0').prop("checked", false);
    $('#contact-addcheck-0').hide();
    $('#add-check').hide();
    $('#na-addcheck-0').prop("checked", false);
    $('#na-addcheck-0').hide();
    $('#na-add-check').hide();
    $('select[name="mtrpoint-mpan-eac"]').val('default');
    $('select[name="mtrpoint-mprn-aq"]').val('default');
    $('#mtrpoint-mpancheck-0').prop("checked", false);
    $('#mpan-found').html('');
    $('#mtrpoint-mprncheck-0').prop("checked", false);
    $('#mprn-found').html('');
    $('#select_add').val('');
    $('#newcust_form #mpancore').val('');
    $('#newcust_form #elecMeterSerial').val('');
    $('#newcust_form #gasMeterSerial').val('');
    $('#mpan-serial').text('');
    $('#mpan-type').text('');
    $('#mpan-serial-orig').text('');
    $('#mpan-type-orig').text('');
    $('#mprn-serial').text('');
    $('#mprn-serial-orig').text('');

    $.ajax({
        url: base_url + 'index.php/admin/address_lookup',
        data: {
            postcode: $('#contact-postcode').val()
        },
        type: 'POST',
        success: function (response) {

            response = JSON.parse(response);

            // if (!response) {
            //     $('#mpan-existing').hide();
            //     $('#mprn-existing').hide();
            //     $('#main-mpan').hide();
            //     $('#main-mprn').hide();
            //     $('#form-end').hide();
            //     $('#mtrpoint-mpan').attr('readonly', false);
            //     $('#mtrpoint-mprn').attr('readonly', false);
            //     $('#newcust_form #senderReference').val('Eversmart');
            //     $('#newcust_form #reference').val('Eversmart');
            //     $('#newcust_form #billingEntityCode').val('Eversmart');
            //     $('#newcust_form #marketingOptOutFl').val('false');
            //     $('#newcust_form #submittedSource').val('Tele');
            //     $('#newcust_form #changeOfTenancyFl').val('false');
            //     $('#newcust_form #primaryContact').val('true');
            //     $('#address_address1').val('');
            //     $('#address_address2').val('');
            //     $('#address_town').val('');
            //     $('#address_county').val('');
            //     $('#address_postcode').val('');
            //     $('#quotefor').val('');
            //     $('#elecProductCode').val('');
            //     $('#mpans').val('');
            //     $('#gasProductCode').val('');
            //     $('#mprns').val('');
            //     $('#na-addcheck-0').show();
            //     $('#na-add-check').show();
            //     $('#pcod-address').text('Address not found');
            //     $('#na-add1').val('');
            //     $('#na-add2').val('');
            //     $('#na-addtown').val('');
            //     $('#na-addcounty').val('');
            //     $('#address_select').html('<p style="position:relative;top:8px;">Please enter manually:</p>');
            //     $('#address_na').show();
            //     $('#na-pcod').val($('#contact-postcode').val());
            //     $('#newcust_form #address_postcode').val($('#na-pcod').val());
            // } else {
            //     $('#address_address1').val('');
            //     $('#address_address2').val('');
            //     $('#address_town').val('');
            //     $('#address_county').val('');
            //     $('#address_postcode').val('');
            //     $('#quotefor').val('');
            //     $('#elecProductCode').val('');
            //     $('#mpans').val('');
            //     $('#gasProductCode').val('');
            //     $('#mprns').val('');
            //     $('#na-add1').val('');
            //     $('#na-add2').val('');
            //     $('#na-addtown').val('');
            //     $('#na-addcounty').val('');
            //     $("#pcod-address").text('Address');
            //     $('#address_na').hide();
                $('#newcust_form #title').val($('#contact-title option:selected').val());

                $('#address_response').html(response.msg);
                $('#newcust_form #address_postcode').val(response.postcode);


            if ($('#lead_flag').val() === '1') {
                    $('#select_add').val($('#lead_address').val());
                }
            // }
        }
    });
});

// retain original mpan found from postcode search
$(document).on('change', '#mtrpoint-mpancheck-main', function () {
    if ($(this).prop("checked") == true && $('#mpan-existing').css('display') == 'none') {
        var mpanlower = $('#address_select').find(':selected').data('mpanlower');
        $(this).prop("checked", false);
        $('#main-mpan').hide();
        $('#mtrpoint-mpancheck-0').prop("checked", true);
        $('#mpan-existing .line-sep').addClass('lsfix');
        $('#mpan-found').html(mpanlower);
        $('#mpan-existing').show();
        $('#newcust_form #mpancore').val(mpanlower);
        $('#mpan-serial').show();
        $('#mpan-type').show();
    }

    if ($(this).prop("checked") == true && $('#main-mpan').css('display') == 'block' && $('#mpan-existing').css('display') == 'block') {
        var mpanlower = $('#address_select').find(':selected').data('mpanlower');
        $('#mpan-existing .line-sep').addClass('lsfix');
        $('#main-mpan').hide();
        $('#mpan-found').html(mpanlower);
        $(this).prop("checked", false);
        $('#mtrpoint-mpancheck-0').prop("checked", true);
        $('#newcust_form #mpancore').val(mpanlower);
        $('#mpan-serial').show();
        $('#mpan-type').show();
    }

    if ($(this).prop('checked') == true || $('#mtrpoint-mpancheck-0').prop('checked') == true) {
        $('#mtrpoint-mpan').attr('readonly', true);
    } else {
        $('#mtrpoint-mpan').attr('readonly', false);
    }
});

// retain original mprn found from postcode search
$(document).on('change', '#mtrpoint-mprncheck-main', function () {
    if ($(this).prop("checked") == true && $('#mprn-existing').css('display') == 'none') {
        var selected_mprn = $('#address_select').find(':selected').data('mprn') ? $('#address_select').find(':selected').data('mprn') : $('#quotation_address_gas').find(':selected').data('gasmprn_gas');
        $(this).prop("checked", false);
        $('#main-mprn').hide();
        $('#newcust_form #quotefor').val('both');
        $('#mtrpoint-mprncheck-0').prop("checked", true);
        $('#mprn-existing .line-sep').addClass('lsfix');
        $('#mprn-found').html(selected_mprn);
        $('#mprn-existing').show();
        $('#newcust_form #mprns').val(selected_mprn);
        $('#mprn-serial').show();
    }

    if ($(this).prop("checked") == true && $('#main-mprn').css('display') == 'block' && $('#mprn-existing').css('display') == 'block') {
        var selected_mprn = $('#address_select').find(':selected').data('mprn') ? $('#address_select').find(':selected').data('mprn') : $('#quotation_address_gas').find(':selected').data('gasmprn_gas');

        $('#mprn-existing .line-sep').addClass('lsfix');
        $('#main-mprn').hide();
        $('#mprn-found').html(selected_mprn);
        $(this).prop("checked", false);
        $('#mtrpoint-mprncheck-0').prop("checked", true);
        $('#newcust_form #mprns').val(selected_mprn);
        $('#newcust_form #quotefor').val('both');
        $('#mprn-serial').show();
    }

    if ($(this).prop('checked') == true || $('#mtrpoint-mprncheck-0').prop('checked') == true) {
        $('#mtrpoint-mprn').attr('readonly', true);
    } else {
        $('#mtrpoint-mprn').attr('readonly', false);
    }
});

// hide mpan fields if mpan checkbox is de-selected
$(document).on('change', '#mtrpoint-mpancheck-0', function () {
    if ($(this).prop("checked") == false) {
        $('select[name="mtrpoint-mpan-eac"]').val('default');
        $('#newcust_form #elecPrevSupplier').val('');
        $('#newcust_form #elecPrevTariff').val('');
        $('#newcust_form #mpancore').val('');
        $('#newcust_form #eac').val('');
    }

    if ($(this).prop("checked") == false && $('#mpan-found').val() != null) {
        $('#main-mpan').show();
        $('#mpan-existing').hide();
        $('#mtrpoint-mpan').val('');
    }

    if ($(this).prop("checked") == true && $('#mtrpoint-mprncheck-0').prop("checked") == true) {
        $('#newcust_form #quotefor').val('both');
    }

    if ($(this).prop("checked") == true && $('#mtrpoint-mprncheck-0').prop("checked") == false) {
        $('#newcust_form #quotefor').val('electricity');
    }

    if ($(this).prop('checked') == true) {
        $('#mtrpoint-mpan').attr('readonly', true);
    } else {
        $('#mtrpoint-mpan').attr('readonly', false);
    }
});

// hide mprn fields if mprn checkbox is de-selected
$(document).on('change', '#mtrpoint-mprncheck-0', function () {
    if ($(this).prop("checked") == false) {
        $('select[name="mtrpoint-mprn-aq"]').val('default');
        $('#newcust_form #gasProductCode').val('');
        $('#newcust_form #mprns').val('');
        $('#newcust_form #aq').val('');
    }

    if ($(this).prop("checked") == false && $('#mprn-found').val() != null) {
        $('#main-mprn').show();
        $('#mprn-existing').hide();
        $('#mtrpoint-mprn').val('');
    }

    if ($(this).prop("checked") == true && $('#mtrpoint-mpancheck-0').prop("checked") == true) {
        $('#newcust_form #quotefor').val('both');
    }

    if ($(this).prop("checked") == false && $('#mtrpoint-mpancheck-0').prop("checked") == true) {
        $('#newcust_form #quotefor').val('electricity');
    }

    if ($(this).prop('checked') == true) {
        $('#mtrpoint-mprn').attr('readonly', true);
    } else {
        $('#mtrpoint-mprn').attr('readonly', false);
    }
});

// add new mpan button
$('#mtrpoint-mpanadd').click(function () {
    if ($('#mtrpoint-mpan').val().length != 0 && $('#mtrpoint-mpan').val().length == 13) {
        $('#mtrpoint-mpancheck-0').prop("checked", true);
        $("#mpan-found").text($('#mtrpoint-mpan').val());
        $('#mpan-existing').show();
        $('select[name="mtrpoint-mpan-eac"]').val('default');
        $('#newcust_form #mpancore').val($('#mtrpoint-mpan').val());
        $('#newcust_form #mpans').val('');
        $(this).attr('disabled', true);
        $('#mpan-serial').text($('#mpan-serial-orig').text());
        $('#mpan-type').text($('#mpan-type-orig').text());
        $('#mpan-serial').hide();
        $('#mpan-type').hide();
    }

    if ($('#mtrpoint-mpancheck-main').prop('checked') == false && $('#mtrpoint-mpan').val().length != 0 && $('#mtrpoint-mpan').val().length == 13) {
        $('#mpan-existing .line-sep').removeClass('lsfix');
    }

    if ($('#mtrpoint-mpancheck-0').prop('checked') == true || $('#mtrpoint-mpancheck-main').prop('checked') == true) {
        $('#mtrpoint-mpan').attr('readonly', true);
    } else {
        $('#mtrpoint-mpan').attr('readonly', false);
    }

    setTimeout(function () {
        $('#mtrpoint-mpan').val('');

    }, 50);
});

// add new mprn button
$('#mtrpoint-mprnadd').click(function () {
    if ($('#mtrpoint-mprn').val().length != 0 && ($('#mtrpoint-mprn').val().length == 10 || $('#mtrpoint-mprn').val().length == 11)) {
        $('#mtrpoint-mprncheck-0').prop("checked", true);
        $("#mprn-found").text($('#mtrpoint-mprn').val());
        $('#mprn-existing').show();
        $('#newcust_form #mprns').val($('#mtrpoint-mprn').val());
        $(this).attr('disabled', true);
        $('#mprn-serial').text($('#mprn-serial-orig').text());
        $('#mprn-serial').hide();
    }

    if ($('#mtrpoint-mprncheck-main').prop('checked') == false && $('#mtrpoint-mprn').val().length != 0 && ($('#mtrpoint-mprn').val().length == 10 || $('#mtrpoint-mprn').val().length == 11)) {
        $('#mprn-existing .line-sep').removeClass('lsfix');
    }

    if ($('#mtrpoint-mprncheck-0').prop('checked') == true || $('#mtrpoint-mprncheck-main').prop('checked') == true) {
        $('#mtrpoint-mprn').attr('readonly', true);
    } else {
        $('#mtrpoint-mprn').attr('readonly', false);
    }

    if ($('#newcust_form #quotefor').val() == 'electricity') {
        $('#newcust_form #quotefor').val('both');
    }

    setTimeout(function () {
        $('#mtrpoint-mprn').val('');

    }, 50);
});

// populates hidden form with sales rep ID
$('#sel_sales_agent').change(function () {
    var rep = $('#sel_sales_agent option:selected').val();
    $('#newcust_form #created_by').val(rep);
});

// populate resident type on hidden form
$('#contact-resident').change(function () {
    var resident = $('#contact-resident option:selected').text();
    if (resident != '(Please Select)') {
        $('#newcust_form #residency').val(resident);
    }
});

// validate bank details
$('#bank-validate').click(function () {
    var sc = $('#bank-sort').val();
    var an = $('#bank-bnum').val();
    var bank_details = $('#bank-sort, #bank-bnum').serialize();

    var Errors = [];
    if (sc == '') {
        Errors += " - Please enter a sort code\n";
    }
    if (an == '') {
        Errors += " - Please enter an account number\n";
    }
    if (Errors.length > 0) {
        alert('Please check the following:\n\n' + Errors);
    } else {
        $.ajax({
            url: base_url + "index.php/admin/validate_bank_details",
            type: 'post',
            data: bank_details,
            success: function (response) {
                response = JSON.parse(response);
                if (response.error == '1') {
                    $('#validate-msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:20px;font-weight:500;"> ' + response.msg + ' </p></div>');
                    $('#submit-form').attr('disabled', true);
                } else {
                    $('#validate-msg').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:20px;font-weight:500;"> ' + response.msg + ' </p></div>');
                    $('#submit-form').attr('disabled', false);
                }
            }
        });
    }
});

// clear forms
$('#clear-form').click(function () {
    $('input').each(function (el) {
        $(el).val('');
    });
    $('#newcust_form')[0].reset();
    $('#contact-postcode').val('');
    $('#quotation_address').prop('selectedIndex', 0);
    $('select[name="tariffselect"]').prop('selectedIndex', '0');
    $('#mtrpoint-mpan-eac').prop('selectedIndex', 0);
    $('#mtrpoint-mprn-aq').prop('selectedIndex', 0);
    $('#contact-title').prop('selectedIndex', 0);
    $('#sel_sales_agent').prop('selectedIndex', 0);
    $('#form-end').hide();
    $('#ddbank').hide();
    $('#stripe').hide();
    $('#contact-addcheck-0').hide();
    $('#add-check').hide();
    $('#na-addcheck-0').hide();
    $('#na-add-check').hide();
    $('#mpan-existing').hide();
    $('#mprn-existing').hide();
    $('#main-mpan').hide();
    $('#main-mprn').hide();
    $('#quote-or').hide();
    $('#pay-type').hide();
    $('#submit-panel').hide();
    $('#admin_quote').html('');
    $('#select_add').val('');
    $('#newcust_form #quotefor').val('both');
    $('#submit-form').attr('disabled', true);
    $('#bank-bname').val('');
    $('#bank-bnum').val('');
    $('#bank-sort').val('');
    $('#validate-msg').html('');
    $('.gasdropdownlist').css('display', 'none');
    $('.missingmeters').css('display', 'none');
    $('.multiplemeters').css('display', 'none');
    $('#checkinput').val('');
    $('#checkinput2').val('');
    $('#address_response').html('');
});

// retrieve quote
$('#get-quote').click(function () {
    $('#newcust_form #elec_ur').val('');
    $('#newcust_form #elec_sc').val('');
    $('#newcust_form #gas_ur').val('');
    $('#newcust_form #gas_sc').val('');
    $('#submit-form').attr('disabled', true);
    $('#bank-bname').val('');
    $('#bank-bnum').val('');
    $('#bank-sort').val('');
    $('#validate-msg').html('');

    var Errors = [];
    if ($('#tariffselect :selected').text() == '(Please Select Tariff)' || $('#newcust_form #quotefor').val() == '') {
        Errors += ' - Please select a tariff from the drop-down\n';
    }
    if ($('#contact-postcode').val() == '') {
        Errors += ' - A customer postcode is required\n';
    }
    if ($('#newcust_form #eac').val() == '') {
        Errors += ' - Please select the customers home type\n';
    }
    if ($('#tariffselect :selected').val() == 'Family Saver Club') {
        if ($('#tariff-pay').val() == 'default') {
            Errors += ' - Please select a payment type\n';
        }

    }
    if (Errors.length > 0) {
        alert('Please check the following:\n\n' + Errors);
    } else {
        $('#form-end').show();
        $.ajax({
            url: base_url + "index.php/admin/calculate_newspend",
            type: 'post',
            //dataType: 'json',
            data: $('#newcust_form').serialize(),
            success: function (response) {
                response = JSON.parse(response);

                if (response.error == '1') { // fail
                    $('#admin_quote').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + ' </p></div>');
                } else {
                    $('#admin_quote').html('<div id="quote_response" class="alert alert-success" role="alert"><strong>Success!</strong>' +
                        '<p style="text-align:center;font-size:40px;font-weight:500;line-height:1"> ' + response.quote + ' </p>' +
                        '<p style="text-align:center;font-size:40px;font-weight:500;"> ' + response.quote_year + ' </p><br>' +
                        '<p style=text-align:center;font-size:20px;font-weight:400;"><strong>Electricity Unit Rate: </strong>' + parseFloat(response.elec_ur * 100).toFixed(2) + ' pence per kWh</p>' +
                        '<p style=text-align:center;font-size:20px;font-weight:400;"><strong>Electricity Standing Charge: </strong>' + parseFloat(response.elec_sc * 100).toFixed(1) + ' pence per day</p>');

                        if (response.gas_ur && response.gas_sc) {
                            $('#quote_response').append('<p style=text-align:center;font-size:20px;font-weight:400;"><strong>Gas Unit Rate: </strong>' + parseFloat(response.gas_ur * 100).toFixed(2) + ' pence per kWh</p>');
                            $('#quote_response').append('<p style=text-align:center;font-size:20px;font-weight:400;"><strong>Gas Standing Charge: </strong>' + parseFloat(response.gas_sc * 100).toFixed(1) + ' pence per day</p>');
                        }

                    $('#admin_quote').append('</div>');

                    $('#newcust_form #newspend').val(response.newspend);
                    $('#newcust_form #newelec').val(response.newelec);
                    $('#newcust_form #elec_ur').val(response.elec_ur);
                    $('#newcust_form #elec_sc').val(response.elec_sc);

                    if (response.gas_ur && response.gas_sc && response.newgas) {
                        $('#newcust_form #newgas').val(response.newgas);
                        $('#newcust_form #gas_ur').val(response.gas_ur);
                        $('#newcust_form #gas_sc').val(response.gas_sc);
                    }

                    $('#submit-panel').show();

                    if ($('#newcust_form #billingMethod').val() == 'prepay' || $('#newcust_form #firstpaymentid').val() == '4') {
                        $('#submit-form').attr('disabled', false);
                    } else {
                        $('#submit-form').attr('disabled', true);
                    }

                    REQUEST('/quotation/address_lookup', 'json', {
                        method: 'post',
                        headers: new Headers({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }),
                        body: 'postcode=' + $('#newcust_form #address_postcode').val()
                    })
                    .then(function (json) {
                        quotation.addresses = json;
                        quotation.addresses.elec = json.elec.map(addId);
                        quotation.addresses.gas = json.gas.map(addId);
                        addOptions('#quotation_address', quotation.addresses.elec, 'Please Select Address', false);
                    });
                }
            }
        });
    }
});

function addId(item) {
    item.id = uuid();
    return item;
}

function queryStringToJson(str) {
    var pairs = str.split('&');
    var keyVals = pairs.map(function (item) {
        var split = item.split('=');
        return {
            key: split[0],
            val: split[1]
        };
    });
    var json = {};
    keyVals.forEach(function (item) {
        json[item.key] = item.val;
    });
    return json;
};

function csa_wrapup() {
    var ser_form = $('#newcust_form').serialize();
    var str = JSON.stringify(ser_form);
    var res1 = str.replace(/%20/g, ' ');
    var res2 = res1.replace(/%40/g, '@');
    var obj = JSON.parse(res2);
    var csa_data = obj.split("&");
    var obj = {};
    for (var key in csa_data) {
        obj[csa_data[key].split("=")[0]] = csa_data[key].split("=")[1];
    }

    alert('ONCE YOU CLOSE THIS POP-UP, THE FORM WILL BE CLEARED!\n\n' +
        'Name: ' + obj['title'] + ' ' + obj['forename'] + ' ' + obj['surname'] + '\n' +
        'DOB: ' + obj['dateOfBirth'] + '\n' +
        'Email: ' + obj['email'] + '\n' +
        'Phone 1: ' + obj['phone1'] + '\n' +
        'Phone 2: ' + obj['phone2'] + '\n' +
        'Billing Method: ' + obj['billingMethod'] + '\n' +
        'Billing Address: ' + obj['address_address1'] + ' ' + obj['address_town'] + '\n' + obj['address_county'] + ' ' + obj['address_postcode'] + '\n' +
        'MPAN: ' + obj['mpancore'] + '\n' +
        'MPRN: ' + obj['mprns'] + '\n' +
        'Tariff: ' + obj['tariff'] + '\n' +
        'Meter Serial (elec): ' + obj['elecMeterSerial'] + '\n' +
        'Meter Serial (gas): ' + obj['gasMeterSerial'] + '\n' +
        'Quote: ' + obj['newspend'] + '\n' +
        'Unit Rate (elec): ' + obj['elec_ur'] + '\n' +
        'Standing Charge (elec): ' + obj['elec_sc'] + '\n' +
        'Unit Rate (gas): ' + obj['gas_ur'] + '\n' +
        'Standing Charge (gas): ' + obj['gas_sc'] + '\n' +
        'EAC: ' + obj['eac'] + '\n' +
        'AQ: ' + obj['aq'] + '\n' +
        'Bank Account Name: ' + obj['bankAccountName'] + '\n' +
        'Bank Sort Code: ' + obj['bankAccountSortCode'] + '\n' +
        'Bank Account Number: ' + obj['bankAccountNumber'] + '\n' +
        'ONCE YOU CLOSE THIS POP-UP, THE FORM WILL BE CLEARED!\n\n'
    );
}

function addOptions(selector, arr, placeholder) {
    $(selector).html(
        [$('<option selected disabled>', {
            value: '0'
        }).text(placeholder)].concat(arr.map(function (item) {
            return $('<option>', {
                value: item.id || uuid()
            }).text(buildFullAdd(item))
        }))
    );
};

function buildFullAdd(i) {
    $a = [];
    $a.push(i.address1);
    if ("address2" in i) {
        $a.push(i.address2);
    }
    $a.push(i.town);
    $a.push(i.county);
    $a.push(i.meteringPointPostcode);
    return $a.join(' ');
}

$(document).on('change', '#quotation_address', function () {

    var fueltype = $('#tariffselect').find(':selected').data('fueltype');

    addAdressDataToForm({
        reset: true
    });
    $('#gasSpecific').remove();

    $t = $(this);
    let elec = quotation.addresses.elec.find(e => e.id == $t.val());
    let gas = quotation.addresses.gas.find(g => buildFullAdd(g) == buildFullAdd(elec));
    
    $('#meter_details').show();
    $('#meter_type').html(elec.meterTypeId);
    $('#meter_serial').html(elec.meterIdSerialNumber);

    if (gas == undefined && fueltype === 'elecgas') {
        // Gas Meter not found
        // Expose gas address dropdown
        console.log("Gas Address mismatch", "Exposing Gas Address list");
        // let template = Handlebars.compile(quotation.templates['gas-dd']);
        // $t.parents(".form-group").after(template(quotation.currentStep));
        $(`#gas_address_select`).show();
        addOptions(`#gas_quotation_address`, quotation.addresses.gas, 'Please Select Address', false);
        $(`#gas_quotation_address`).on('change', (evt) => {
            console.log(evt.target.selectedOptions[0].value);

            gas = quotation.addresses.gas.find(g => {
                return g.id == evt.target.selectedOptions[0].value;
            });
            console.log("Gas Address Manually Specified");
            addAdressDataToForm({
                reset: false,
                data: {
                    e: elec,
                    g: gas
                }
            });
        });
    } else {
        // Gas & Elec found
        console.log("Elec & Gas Address Match or Gas Not Required");
        $(`#gas_address_select`).hide();
        addAdressDataToForm({
            reset: false,
            data: {
                e: elec,
                g: fueltype === 'elecgas' ? gas : null
            }
        });
    }
});

function addAdressDataToForm(opts = null) {

    $('#newcust_form #address_address1').val(opts.reset ? null : opts.data.e.address1);
    $('#newcust_form #address_address2').val(opts.reset ? null : opts.data.e.address2);
    $('#newcust_form #address_town').val(opts.reset ? null : opts.data.e.town);
    $('#newcust_form #address_county').val(opts.reset ? null : opts.data.e.county);
    $('#newcust_form #address_countryCode').val('GB');

    if (opts.reset || !opts.data.skipElec) {
        $('#newcust_form #meterTypeElec').val(opts.reset ? null : opts.data.e.meterTypeId);
        $('#newcust_form #elecMeterSerial').val(opts.reset ? null : opts.data.e.meterIdSerialNumber);
        $('#newcust_form #mpancore').val(opts.reset ? null : opts.data.e.mpanCore);
        $('#newcust_form #mpans').val(opts.reset ? null : opts.data.e.mpanCore);
    }

    if (opts.reset || !opts.data.skipGas && opts.data.g !== null) {
        $('#newcust_form #gasMeterSerial').val(opts.reset ? null : opts.data.g.meterSerialNumber);
        $('#newcust_form #mprns').val(opts.reset ? null : opts.data.g.mprn);
    }
}


// form validation
$('#submit-form').click(function () {
    var fueltype = $('#tariffselect').find(':selected').data('fueltype');
    var tarPay = $('#tariff-pay option:selected').val();
    var Errors = [];
    if ($('#contact-title').val() == '') {
        Errors += ' - A title is required\n';
    }
    if ($('#contact-fname').val() == '') {
        Errors += ' - A forename is required\n';
    }
    if ($('#contact-lname').val() == '') {
        Errors += ' - A surname is required\n';
    }
    if ($('#contact-dob').val() == '') {
        Errors += ' - A date of birth is required\n';
    }
    if ($('#contact-email').val() == '') {
        Errors += ' - An email address is required\n';
    }
    if ($('#contact-mobile').val() == '') {
        Errors += ' - A mobile number is required\n';
    }
    if ($('#tariffselect :selected').text() == '(Please Select Tariff)') {
        Errors += ' - Please select a tariff from the drop-down\n';
    }
    if ($('#tariff-payment :selected').text() == '(Please Select) ') {
        Errors += ' - Please select a payment method from the drop-down\n';
    }
    if ($('#contact-postcode').val() == '') {
        Errors += ' - A customer postcode is required\n';
    }
    if ($('#quotation_address :selected').text() == '(Please Select Address)') {
        Errors += ' - Please select a customer address from the drop-down\n';
    }
    if ($('#newcust_form #eac').val() == '') {
        Errors += ' - Please select the customers Home Type\n';
    }
    if (fueltype == 'elecgas') {
        if ($('#newcust_form #aq').val() == '') {
            Errors += ' - Please enter an AQ \n';
        }
    }
    if (tarPay !== '4') {
       if (($('#newcust_form #firstpaymentid').val() == '2' || $('#newcust_form #firstpaymentid').val() == '5') || $('#tariffselect :selected').val() != 'Safeguard PAYG') {
            if ($('#bank-bname').val() == '') {
                Errors += ' - Please enter a Bank Account Name\n';
            }
            if ($('#bank-bnum').val() == '') {
                Errors += ' - Please enter a Bank Account Number\n';
            }
            if ($('#bank-sort').val() == '') {
                Errors += ' - Please enter a Bank Account Sort Code\n';
            }
       }
   }
    if (Errors.length > 0) {
        alert('Please check the following:\n\n' + Errors);
    } else {
        var newCustomerFormData = $('#newcust_form').serialize();
        $.ajax({
            url: base_url + "index.php/admin/admin_enroll_customer",
            type: 'post',
            //dataType: 'json',
            data: newCustomerFormData,
            beforeSend: function () {
                $('#submit-form').attr('disabled', 'disabled');
            },
            success: function (response) {

                if ($('#billingMethod').val() == 'directdebit') {
                    response = JSON.parse(response);
                }

                if (response.error == '0') //success
                {

                    let ese_share_link = response.ese_share_link ? response.ese_share_link : '';

                    $('#admin_signup_msg').html('<div style="text-align:center; font-size:30px;font-weight:500;" class="alert alert-success" role="alert"><p>' + response.msg + '</p><p>' + ese_share_link + '</p></div>');
                    var formData = queryStringToJson(newCustomerFormData);

                    if (response.type == 'junifer') {
                        $('#recordType').val('0121n0000007GkH');

                        if (response.junifer_id > 0) {
                            // Success
                            formData['subject'] = 'Registration Success - Junifer Customer ID: ' + response.junifer_id + ' - Success';
                            formData['description'] = 'Registration Success for ' + formData['forename'] + ' ' + formData['surname'];
                            formData['junifer_customer_id'] = response.junifer_id;
                            formData['status'] = 'Registration Success';

                        } else {
                            //  Pending
                            formData['subject'] = 'Registration Pending - Junifer Customer - Pending';
                            formData['description'] = 'Registration Pending for ' + formData['forename'] + ' ' + formData['surname'];
                            formData['junifer_customer_id'] = '';
                            formData['status'] = 'Registration Pending';
                        }
                    } else {
                        $('#recordType').val('0121n0000007GkE');
                        $('#00N1n00000SB9NR').val(response.dyball_id);

                        formData['subject'] = 'Registration Pending - Dyball Customer - Pending';
                        formData['description'] = 'Registration Pending for ' + formData['forename'] + ' ' + formData['surname'];
                        formData['junifer_customer_id'] = '';
                        formData['dyball_account_id'] = response.dyball_id;
                        formData['status'] = 'Registration Pending';
                    }

                    if (response.elec_product_code) {
                        formData['elec_product_code'] = response.elec_product_code;
                    }

                    if (response.gas_product_code) {
                        formData['gas_product_code'] = response.gas_product_code;
                    }

                    formData['start_date'] = wtcDate(0);
                    formData['end_date'] = wtcDate(21);

                    Object.keys(formData).forEach(function (key) {
                        $('[data-map-name="' + key + '"]').val(decodeURIComponent(formData[key]));
                    });

                    var webToCaseLog = {};
                    $('#webToCaseAdmin').find('input, select').each(function (i, item) {
                        if (typeof $(this).attr('data-webtocase-name') !== "undefined" && $(this).val() !== '') {
                            webToCaseLog[$(this).attr('data-webtocase-name')] = $(this).val();
                        }
                    });

                    Promise.all([
                            REQUEST(base_url + 'index.php/user/web_to_case_log', 'none', { // Log web to case form
                                method: 'post',
                                headers: new Headers({
                                    'Content-Type': 'application/json'
                                }),
                                body: JSON.stringify(webToCaseLog)
                            }),
                            REQUEST('https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', { // Send the web to case form
                                method: 'post',
                                mode: 'no-cors',
                                headers: new Headers({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }),
                                body: $('#webToCaseAdmin').serialize()
                            })
                        ])
                        .then(function () {
                            csa_wrapup();
                            $('#clear-form').click();
                        });
                }
                if (response.error == '1') { // fail
                    $('#admin_signup_msg').html('<div style="text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + ' </p></div>');
                }
            },
            complete: function () {
                if ($('#lead_flag').val() === '1') {
                    var status = {
                        'lead_id': $('#lead_id').val(),
                        'lead_status_type_id': '5',
                    };
                    var serializedArr = JSON.stringify(status);

                    $.ajax({
                        url: base_url + "index.php/admin/update_lead",
                        type: 'post',
                        data: serializedArr,
                        success: function (response) {
                            response = JSON.parse(response);
                            if (response.error === '0') { // success
                                $('#lead_flag').val('0');
                                $('#lead_details').trigger('reset');
                                $('#update_result').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + ' </p></div>');
                            } else { // fail
                                $('#update_result').html('<div style="text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + ' </p></div>');
                            }
                        }
                    });
                }
            }
        });
    }
});

$(function(){
    $('#boiler_cover').on('click', function() {
        $(this).val() === "0" ?  $(this).val('1') : $(this).val('0');
        $('#newcust_form #boiler_cover').val($(this).val())
    });

    $('#psr_flag').on('click', function() {
        $(this).val() === "0" ? $(this).val('1') : $(this).val('0');
        $('#newcust_form #psr_flag').val($(this).val())
    });
});
