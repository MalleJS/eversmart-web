if(window.location.hostname === 'localhost' || window.location.host === '52.56.247.49')
{
    var base_url = 'http://'+window.location.host+'/';
}
if( window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'eversmartenergy.co.uk' || window.location.hostname === '52.56.76.183')
{
    var base_url = 'https://'+window.location.hostname+'/';
}

function logout_user() {

    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

// validation - only numbers
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
};

$( document ).ready(function()
{
    $('#email-lookup').attr( 'disabled', 'disabled' );
    $('#send-email').attr( 'disabled', 'disabled' );

    $.ajax({
        url: base_url+'index.php/admin/email_type_lookup',
        type: 'get',
        beforeSend:function(){
            $('#email_type').html('Loading...');
        },
        success:function(response)
        {
            $('#email_type').html(response);

        }
    });

    $('#search_surname, #search_email').on( 'keyup', function( evt ){
        $('#search_surname').val().length || $('#search_email').val().length ? $('#email-lookup').removeAttr( 'disabled' ) : $('#email-lookup').attr( 'disabled', 'disabled' ) ;
        if( evt.keyCode === 13 || evt.which === 13 ){ $('#email-lookup').trigger('click'); }
    });

    $('#account_outcome, #email_type').on( 'change', function( evt ){
        $('#account_outcome').find('option:selected').val() !== 'default' && $('#email_type').find('option:selected').val() !== 'default' ? $('#send-email').removeAttr( 'disabled' ) : $('#send-email').attr( 'disabled', 'disabled' );
    });
});

$('#email-lookup').submit(function (evt) {
    evt.preventDefault();
});

$(document).on('change','#email_outcome',function(){
    if($('#account_outcome :selected').text() !== '(Please Select Account)' && $('#email_type :selected').text() !== '(Please Select Email)') {
        $('#send-email').removeAttr( 'disabled' );
    }
});

$('#account_outcome').on( 'change', function( evt ){
    var id = $(this).find(':selected').data('id');
    var forename = $(this).find(':selected').data('forename');
    var email = $(this).find('option:selected').val();
    $('#email_form #id').val(id);
    $('#email_form #forename').val(forename);
    $('#email_form #email').val(email);
});

$('#email_type').on( 'change', function( evt ){
    var email_type = $(this).find('option:selected').text();
    $('#email_form #email_type').val(email_type.trim());
});

$('#email-lookup').click(function(){
    $('#send-email').attr( 'disabled', 'disabled' );
    $('#email_outcome').html('');
    $.ajax({
        url: base_url+"index.php/admin/admin_customer_lookup",
        type: 'post',
        data: $('#search-params').serialize(),
        beforeSend:function(){
            $('#account_outcome').html('Loading...');
        },
        success:function( response, success, resOpts )
        {
            if( resOpts.status === 204 )
            {
                $('#account_outcome').html('No results');

            }
            else
            {
                $('#account_outcome').html(response);
            }
        }
    });

});

$('#send-email').click(function(){

    $.ajax({
        url: base_url+"index.php/admin/admin_resend_email",
        type: 'post',
        data: $('#email_form').serialize(),
        success:function(response){
            $('#email_outcome').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:30px;font-weight:500;"> Email sent :) </p></div>');
        }
    });

});