if(window.location.hostname === 'localhost' || window.location.host === '52.56.247.49') {
    var base_url = 'http://'+window.location.host+'/';
}
if( window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'eversmartenergy.co.uk' || window.location.hostname === '52.56.76.183') {
    var base_url = 'https://'+window.location.hostname+'/';
}

function logout_user() {
    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

// validation - only numbers
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$( document ).ready(function()
{
    $('#dual-select').hide();
    $('#dual-quote').hide();
    $('#submit-panel').hide();
    $('#psr-yes').hide();
    $('#submit-form').attr( 'disabled', 'disabled' );

    $('#contact-postcode').change(function(){
        $('#supply_postcode').val($('#contact-postcode').val());
    });

    // alert for email validation
    $('#contact-email').focusout(function() {
        $('#contact-email').filter(function() {
            var email=$('#contact-email').val();
            var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if( !emailReg.test( email ) ) {
                alert('Please enter a valid email address');
            }
        })
    });

    // unchecks 'None of the above' checkbox if other checkbox is checked
    $('#mrk-consent-0, #mrk-consent-1, #mrk-consent-2, #mrk-consent-3, #mrk-consent-4, #mrk-consent-5').click(function() {
        if($(this).prop('checked') === true && $('#mrk-consent-6').prop('checked') === true) {
            $('#mrk-consent-6').prop("checked", false);
        }
    });

    // unchecks all other checkboxes if 'None of the above' checkbox is checked
    $('#mrk-consent-6').click(function() {
        if($(this).prop('checked') === true) {
            $('#mrk-consent-0, #mrk-consent-1, #mrk-consent-2, #mrk-consent-3, #mrk-consent-4, #mrk-consent-5').prop("checked", false);
        }
    });

    // shows psr script
    $('#psr-0').click(function() {
        if($(this).prop('checked') === true) {
            $('#psr-yes').fadeIn('slow');
        }
    });

    // hides psr script
    $('#psr-1').click(function() {
        if($(this).prop('checked') === true) {
            var psr_consent_y = $('#psr-consent-0');
            var psr_consent_n = $('#psr-consent-1');
            if (psr_consent_y.prop('checked') === true || psr_consent_n.prop('checked') === true) {
                psr_consent_y.prop('checked', false);
                psr_consent_n.prop('checked', false);
            }
            $('#psr-yes').fadeOut('slow');
        }
    });
});

// postcode search for address lookup
$('#contact-lookup').click(function( evt ) {
    evt.preventDefault();
    $('#admin_datacapture_msg').html('');
    var Errors = [];
    $('#admin_signup_msg').html('');
    if($('#contact-postcode').val() === '') { Errors += ' - A customer postcode is required\n'; }

    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {
        $.ajax({
            url: base_url + 'index.php/admin/address_lookup',
            data: {pcod: $('#contact-postcode').val()},
            type: 'POST',
            beforeSend: function () {
                $('#address_select').html('Loading...');
            },
            success: function (response, success, resOpts) {
                if (resOpts.status === 204) {
                    $('#form-end').hide();
                    $('#pcod-address').text('Address not found');
                } else {
                    $("#pcod-address").text('Address');
                    $('#address_select').html(response);
                }
            }
        });
    }
});

// fuel selector necessary for quote
$('#tariff-fuel').change(function() {
    var fuel_type = $('#tariff-fuel option:selected').text();
    if(fuel_type === 'Elec only') {
        $('#aq').val('');
        $('#quotefor').val('electricity');
    }
    if(fuel_type === 'Dual') {
        $('#quotefor').val('both');
        $('#tariff-consum-gas').val('1');
    }
});

// eac/aq selector necessary for quote
$('#tariff-consum-elec').change(function() {
    var eac = $('#eac');
    var elec_consum = $('#tariff-consum-elec option:selected').val();
    var fuel_type = $('#tariff-fuel option:selected').text();

    switch (elec_consum) {
        case '1': eac.val(''); break;
        case '2': eac.val('1600'); break;
        case '3': eac.val('1700'); break;
        case '4': eac.val('1800'); break;
        case '5': eac.val('1900'); break;
        case '6': eac.val('2800'); break;
        case '7': eac.val('2900'); break;
        case '8': eac.val('3000'); break;
        case '9': eac.val('3100'); break;
        case '10': eac.val('4300'); break;
        case '11': eac.val('4400'); break;
        case '12': eac.val('4500'); break;
        case '13': eac.val('4600'); break;
    }

    if(fuel_type === 'Dual') {
        $('#tariff-consum-gas').val(elec_consum);
        var aq = $('#aq');
        var gas_consum = $('#tariff-consum-gas option:selected').val();
        switch (gas_consum) {
            case '1': aq.val(''); break;
            case '2': aq.val('7400'); break;
            case '3': aq.val('7600'); break;
            case '4': aq.val('7800'); break;
            case '5': aq.val('8000'); break;
            case '6': aq.val('11400'); break;
            case '7': aq.val('11600'); break;
            case '8': aq.val('11800'); break;
            case '9': aq.val('12000'); break;
            case '10': aq.val('16400'); break;
            case '11': aq.val('16600'); break;
            case '12': aq.val('16800'); break;
            case '13': aq.val('17000'); break;
        }
    }
});

// clear forms
$('#clear-form').click(function( evt) {
    evt.preventDefault();
    $('html,body').animate({scrollTop: $('.topbar-admin').offset().top}, 'slow');
    $('#submit-form').attr( 'disabled', 'disabled' );
    $('#submit-panel').hide();
    $('#psr-yes').hide();

    $('input').each(function(el) {
        $(el).val('');
    });

    $('#tariff-consum-elec').prop('selectedIndex',0);
    $('#quotation_address').prop('selectedIndex',0);
    $('#contact-title').prop('selectedIndex',0);
    $('#eac').val('');
    $('#aq').val('');

    $('#tariff-tariff').val('Safeguard PAYG');

    for (var i=0; i<=6; i++) {
        $('#mrk-consent-'+i).prop('checked', false);
    }

    psr_check = document.getElementsByName('psr');
    psr_con_check = document.getElementsByName('psr-consent');

    for(var k=0, o=psr_check.length;k<o;k++) {
        psr_check[k].checked = psr_check.checked;
    }
    for(var l=0, p=psr_con_check.length;l<p;l++) {
        psr_con_check[l].checked = psr_con_check.checked;
    }
});

// retreive energylinx quote
$('#get-quote').click(function( evt ) {
    evt.preventDefault();
    $('#admin_datacapture_msg').html('');
    var address = $('#quotation_address option:selected').val();
    var elec_consum = $('#tariff-consum-elec :selected').val();
    var Errors = [];

    if($('#contact-postcode').val() === '') {
        Errors += ' - A customer postcode is required\n';
    }
    if(address  === '0') {
        Errors += ' - Please select a customer address from the drop-down\n';
    }
    if(elec_consum === '1') {
        Errors += ' - Please select a Home Type from the drop-down\n';
    }

    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {
        $('#quote-annual').val('');
        $('#quote-monthly').val('');
        $('#quote-elec-ur').val('');
        $('#quote-elec-sc').val('');
        $('#quote-gas-ur').val('');
        $('#quote-gas-sc').val('');

        $.ajax({
            url: base_url+"index.php/admin/calculate_newspend",
            type: 'post',
            data: $('#form-quote').serialize(),
            success:function(response){
                response = JSON.parse(response);
                if(response.error === '1')  {
                    $('#admin_quote').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> '+response.msg+' </p></div>');
                } else {
                    $('#submit-panel').show();
                    $('#quote-annual').val(response.newspend);
                    $('#quote-monthly').val(Math.round(response.newspend/52*100)/100);
                    if(response.elec_ur){$('#quote-elec-ur').val(response.elec_ur);}
                    if(response.elec_sc){$('#quote-elec-sc').val(response.elec_sc);}
                    if(response.gas_ur && response.gas_sc) {
                        $('#dual-quote').show();
                        if(response.gas_ur){$('#quote-gas-ur').val(response.gas_ur);}
                        if(response.gas_sc){$('#quote-gas-sc').val(response.gas_sc);}
                    } else {
                        $('#dual-quote').hide();
                    }
                    $('#psr-1').prop("checked", true);
                }
            }
        });
    }
});

// hide/show submit button
$('#form-submit').change(function() {
    var title = $('#contact-title option:selected');
    var fname = $('#contact-fname');
    var lname = $('#contact-lname');
    var dob = $('#contact-dob');
    var email = $('#contact-email');
    var phone = $('#contact-tel');
    if (title.val() !== 0 && fname.val() !== '' && lname.val() !== '' && dob.val() !== '' && email.val() !== '' && phone.val() !== '') {
        $('#submit-form').removeAttr( 'disabled' );
    } else {
        $('#submit-form').attr( 'disabled', 'disabled' );
    }

});

// form validation and submit
$('#submit-form').click(function(evt) {
    evt.preventDefault();
    var title = $('#contact-title option:selected').val();
    var Errors = [];

    if(title === '0') { Errors += ' - A title is required\n'; }
    if($('#contact-fname').val() === '') { Errors += ' - A forename is required\n'; }
    if($('#contact-lname').val() === '') { Errors += ' - A surname is required\n'; }
    if($('#contact-dob').val() === '') { Errors += ' - A date of birth is required\n'; }
    if($('#contact-email').val() === '') { Errors += ' - An email address is required\n'; }
    if($('#contact-tel').val() === '') { Errors += ' - A phone number is required\n'; }
    if ($('.marketing-check').filter(function(){  return $(this).find(':checked').length === 0 }).length > 0 ) {
        Errors += ' - Please select at least one marketing option\n';
    }
    if ($('#psr-0').prop('checked') === false && $('#psr-1').prop('checked') === false) {
        Errors += ' - Please select at least one PSR option\n';
    }
    if ($('#psr-0').prop('checked') === true && ( $('#psr-consent-0').prop('checked') === false || $('#psr-consent-1').prop('checked') === true ) ) {
        Errors += ' - If the customer wishes to sign upto the PSR, they must confirm that they are happy to share their information\n';
    }
    if (Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {
        var home_form = $($('#form-quote')[0].elements).not('#quotation_address, #contact-postcode, #tariff-fuel, #tariff-consum-elec, #tariff-consum-gas, #supply_postcode, #monthyear, #billingMethod, #quotefor, #elecPrevSupplier, #elecPrevTariff, #gasPrevSupplier, #gasPrevTariff').serialize();
        var consent_form = $('#form-submit').serialize();
        var data = home_form + '&' + consent_form;

        $.ajax({
            url: base_url + 'index.php/admin/lead_log',
            type: 'post',
            data: data,
            success: function (response) {
                response = JSON.parse(response);
                if (response.error == '0') {
                    $('#submit-form').attr( 'disabled', 'disabled' );
                    $('#admin_datacapture_msg').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + '  </p></div>');
                    $('#clear-form').click();
                } else {
                    $('#admin_datacapture_msg').html('<div style="text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> ' + response.msg + ' </p></div>');
                }
            }
        });
    }
});
