if(window.location.hostname === 'localhost' || window.location.host === '52.56.247.49')
{
    var base_url = 'http://'+window.location.host+'/';
}
if( window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'eversmartenergy.co.uk' || window.location.hostname === '52.56.76.183')
{
    var base_url = 'https://'+window.location.hostname+'/';
}

function logout_user() {

    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

// validation - only numbers
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
};

function getLeads()
{
    $.ajax({
        url: base_url + "admin/admin_get_leads",
        type: 'get',
        success: function (response) {
            response = JSON.parse(response);
            if (response.error == '0') {
                $('#search-results').show();
                trHTML = '';
                $.each(response.leads, function (i, item) {
                    trHTML += '<tr id="results">' +
                        '<td>' + item.lead_id + '</td>' +
                        '<td>' + item.forename + ' ' + item.surname + '</td>' +
                        '<td>' + item.address_full + '</td>' +
                        '<td hidden>' + item.dob + '</td>' +
                        '<td hidden>' + item.phone + '</td>' +
                        '<td>' + item.quote_annual + '</td>' +
                        '<td>' + item.status + '</td>' +
                        '<td hidden>' + item.eac + '</td>' +
                        '<td hidden>' + item.aq + '</td>' +
                        '<td hidden>' + item.email + '</td>' +
                        '<td hidden>' + item.tariff + '</td>' +
                        '<td hidden>' + item.address_postcode + '</td>' +
                        '<td hidden>' + item.title + '</td>' +
                        '<td hidden>' + item.forename + '</td>' +
                        '<td hidden>' + item.surname + '</td>' +
                        '<td hidden>' + item.status + '</td>' +
                        '</tr>';
                });
                createClickHandler();

            } else {
                $('#search-error').html('No leads found');
                $('#search-results').show();
            }
        }
    });
}

function createClickHandler() {
    $('#admin_lead_table').append(trHTML);

    var table = document.getElementById('admin_lead_table');
    var rows = table.getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler =
            function (row) {
                return function () {
                    var id_cell = row.getElementsByTagName('td')[0];
                    var name_cell = row.getElementsByTagName('td')[1];
                    var add_cell = row.getElementsByTagName('td')[2];
                    var dob_cell = row.getElementsByTagName('td')[3];
                    var phone_cell = row.getElementsByTagName('td')[4];
                    var quote_cell = row.getElementsByTagName('td')[5];
                    var eac_cell = row.getElementsByTagName('td')[7];
                    var aq_cell = row.getElementsByTagName('td')[8];
                    var email_cell = row.getElementsByTagName('td')[9];
                    var tariff_cell = row.getElementsByTagName('td')[10];
                    var pcod_cell = row.getElementsByTagName('td')[11];
                    var title_cell = row.getElementsByTagName('td')[12];
                    var forename_cell = row.getElementsByTagName('td')[13];
                    var surname_cell = row.getElementsByTagName('td')[14];
                    var status_cell = row.getElementsByTagName('td')[15];

                    var id = id_cell.innerHTML;
                    var name = name_cell.innerHTML;
                    var dob = dob_cell.innerHTML;
                    var phone = phone_cell.innerHTML;
                    var address = add_cell.innerHTML;
                    var eac = eac_cell.innerHTML;
                    var aq = aq_cell.innerHTML;
                    var quote = quote_cell.innerHTML;
                    var email = email_cell.innerHTML;
                    var tariff = tariff_cell.innerHTML;
                    var pcod = pcod_cell.innerHTML;
                    var title = title_cell.innerHTML;
                    var forename = forename_cell.innerHTML;
                    var surname = surname_cell.innerHTML;
                    var status = status_cell.innerHTML;

                    var date = dob;
                    var newdate = date.split("-").reverse().join("-");
                    var newadd = address.replace(/\s+/g, ' ').trim();
                    $('#selected-lead #lead_id').val(id);
                    $('#selected-lead #lead_status').val(status);
                    $('#selected-lead #lead-name').val(name);
                    $('#selected-lead #lead-dob').val(newdate);
                    $('#selected-lead #lead-phone').val(phone);
                    $('#selected-lead #lead-email').val(email);
                    $('#selected-lead #lead-address').val(newadd);
                    $('#selected-lead #lead_postcode').val(pcod);
                    $('#selected-lead #lead_title').val(title);
                    $('#selected-lead #lead_forename').val(forename);
                    $('#selected-lead #lead_surname').val(surname);
                    $('#selected-lead #lead-tariff').val(tariff);
                    $('#selected-lead #lead-eac').val(eac);
                    $('#selected-lead #lead-aq').val(aq);
                    $('#selected-lead #lead-quote').val(quote);

                    $('#selected-lead').show();
                    $('#lead-update').show();
                    $('#update').hide();
                    $('#update-status option').removeAttr("disabled");
                };
            };
        currentRow.onclick = createClickHandler(currentRow);
    }
}

$( document ).ready(function()
{
    $('#selected-lead').hide();
    $('#update').hide();
    getLeads();
});

$('#lead-lookup').click(function(evt) {
    evt.preventDefault();
    $('#admin_lead_table #results').html('');

    $.ajax({
        url: base_url + "admin/admin_get_leads",
        type: 'post',
        data: $('#lead-search').serialize(),
        success: function (response) {
            response = JSON.parse(response);
            if (response.error == '0') {
                $('#search-results').show();
                $('#search-error').hide();
                $('#search-error').html('');
                trHTML = '';
                if (JSON.stringify(response.leads) !== '[]') {
                    $.each(response.leads, function (i, item) {
                        trHTML += '<tr id="results">' +
                            '<td>' + item.lead_id + '</td>' +
                            '<td>' + item.forename + ' ' + item.surname + '</td>' +
                            '<td>' + item.address_full + '</td>' +
                            '<td hidden>' + item.dob + '</td>' +
                            '<td hidden>' + item.phone + '</td>' +
                            '<td>' + item.quote_annual + '</td>' +
                            '<td>' + item.status + '</td>' +
                            '<td hidden>' + item.eac + '</td>' +
                            '<td hidden>' + item.aq + '</td>' +
                            '<td hidden>' + item.email + '</td>' +
                            '<td hidden>' + item.tariff + '</td>' +
                            '<td hidden>' + item.address_postcode + '</td>' +
                            '<td hidden>' + item.title + '</td>' +
                            '<td hidden>' + item.forename + '</td>' +
                            '<td hidden>' + item.surname + '</td>' +
                            '<td hidden>' + item.status + '</td>' +
                            '</tr>';
                    });
                    createClickHandler();
                } else {
                    $('#admin_lead_table td').html('');
                    $('#search-results').hide();
                    $('#search-error').show();
                    $('#search-error').html('No leads found');
                }
            } else {
                $('#admin_lead_table td').html('');
                $('#search-results').hide();
                $('#search-error').show();
                $('#search-error').html('No leads found');
            }
        }
    });
});

$('#lead-update').click(function(evt) {
    evt.preventDefault();
    $('#lead-submit').attr('disabled', true);
    $('#update').show();
    $(this).hide();
    var current_status = $('#lead_status').val();
    $('#update-status option:contains('+current_status+')').attr("disabled", "disabled").siblings().removeAttr("disabled");
});

$('#lead-update-final').click(function(evt) {
    evt.preventDefault();
    var update_status = $('#update-status option:selected').val();
    var Errors = [];
    if(update_status == '0') { Errors += ' - Please select a new lead status\n'; }
    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {
        var status = {
            'lead_id': $('#lead_id').val(),
            'lead_status_type_id': update_status,
        };
        var serializedArr = JSON.stringify(status);
        console.log(serializedArr);

        $.ajax({
            url: base_url + "index.php/admin/update_lead",
            type: 'post',
            data: serializedArr,
            success: function (response) {
                response = JSON.parse(response);
                if(response.error === '0') {    // success
                    $('#lead-submit').attr('disabled', false);
                    $('#update').hide();
                    $('#lead-update').show();
                    $('#update-status').prop('selectedIndex',0);
                    $('#update_result').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:30px;font-weight:500;"> '+response.msg+' </p></div>');
                    $('#admin_lead_table #results').html('');
                    getLeads();
                } else {    //fail
                    $('#update_result').html('<div style="text-align:center" class="alert alert-danger" role="alert"><strong>Error!</strong><p style="text-align:center;font-size:30px;font-weight:500;"> '+response.msg+' </p></div>');
                }
            }
        });
    }
});

$('#lead-submit').click(function(evt) {
    evt.preventDefault();
    $('#lead_details').submit();
});
