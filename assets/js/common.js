var redirect_url;
var group;
var base_url = 'https://'+window.location.hostname+'/';

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : evt.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;


   return true;
}

function serialiseJSON( data ){
    var payload = [];
    Object.keys( data ).forEach(function( key ){
        payload.push( key + '=' + data[ key ] );
    });
    return payload.join('&');
};

function webToCaseBody(){
    return {
        org_id: $('#orgid').val(),
        name: $('#00N1n00000SB9PS').val(), // Name
        email: $('#wtc_email').val(),
        phone: $('#phone').val(),
        dob: $('#00N1n00000SB9PN').val(), // DOB
        address: $('#00N1n00000SB9PI').val(), // Address line 1
        postcode: $('#00N1n00000SB9Ph').val(), // Postcode
        subject: $('#subject').val(),
        description: $('#description').val(),
        junifer_customer_id: $('#00N1n00000SB9OK').val(), // Junifer customer id
        dyball_account_id: $('#00N1n00000SB9NR').val(), // dyball account number
        external_system_registration_status: $('#00N1n00000SB9Pc').val(), // External System Registration Status

        contact_name: $('#name').val(), // Contact Name
        discount_elec: $('#00N1n00000SakSP').val(), // Discount Elec
        discount_gas: $('#00N1n00000SakSU').val(), // Discounts Gas
        supply_end_date_elec: $('#00N1n00000SakSF').val(), // Supply End Date Electricity
        supply_end_date_gas: $('#00N1n00000SakSK').val(), // Supply End Date Gas
        tariff_comparison_rate_gas: $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
        tariff_comparison_rate_elec: $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
        expected_supply_start_date_gas: $('#00N1n00000SajNw').val(), // Expected supply start date Gas
        expected_supply_start_date_elec: $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
        estimated_annual_cost_elec: $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
        estimated_annual_cost_gas: $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
        gas_unit_rate: $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
        elec_unit_rate: $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
        daily_standing_charge_elec: $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
        daily_standing_charge_gas: $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
        assumed_annual_consumption_elec: $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
        assumed_annual_consumption_gas: $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
        record_type: $('#recordType').val(), // Case Record Type
        company: $('#company').val(),
        case_send_date: $('#00N1n00000SB9ST').val(), // Case send date & time

        meter_supply_number_elec: $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
        meter_supply_number_gas: $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
        product_name_gas: $('#00N1n00000Saz7g').val(), // Product Name Gas
        product_name_elec: $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
        meter_serial_number_elec: $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
        meter_serial_number_gas: $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas
    };
};

function webToCaseSalesforceBody(){
    return {
        orgid: $('#orgid').val(),
        '00N1n00000SB9PS': $('#00N1n00000SB9PS').val(), // Name
        email: $('#wtc_email').val(),
        phone: $('#phone').val(),
        '00N1n00000SB9PN': $('#00N1n00000SB9PN').val(), // DOB
        '00N1n00000SB9PI': $('#00N1n00000SB9PI').val(), // Address line 1
        '00N1n00000SB9Ph': $('#00N1n00000SB9Ph').val(), // Postcode
        subject: $('#subject').val(),
        description: $('#description').val(),
        '00N1n00000SB9OK': $('#00N1n00000SB9OK').val(), // Junifer customer id
        '00N1n00000SB9NR': $('#00N1n00000SB9NR').val(), // dyball account number
        '00N1n00000SB9Pc': $('#00N1n00000SB9Pc').val(), // External System Registration Status

        name: $('#name').val(), // Contact Name
        '00N1n00000SakSP': $('#00N1n00000SakSP').val(), // Discount Elec
        '00N1n00000SakSU': $('#00N1n00000SakSU').val(), // Discounts Gas
        '00N1n00000SakSF': $('#00N1n00000SakSF').val(), // Supply End Date Electricity
        '00N1n00000SakSK': $('#00N1n00000SakSK').val(), // Supply End Date Gas
        '00N1n00000SajO2': $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
        '00N1n00000SajO1': $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
        '00N1n00000SajNw': $('#00N1n00000SajNw').val(), // Expected supply start date Gas
        '00N1n00000SajNv': $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
        '00N1n00000SajNt': $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
        '00N1n00000SajNu': $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
        '00N1n00000SajNs': $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
        '00N1n00000SajNr': $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
        '00N1n00000SajNp': $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
        '00N1n00000SajNq': $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
        '00N1n00000SajNn': $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
        '00N1n00000SajNo': $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
        recordType: $('#recordType').val(), // Case Record Type
        company: $('#company').val(),
        '00N1n00000SB9ST': $('#00N1n00000SB9ST').val(), // Case send date & time

        '00N1n00000Saz7d': $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
        '00N1n00000Saz7e': $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
        '00N1n00000Saz7g': $('#00N1n00000Saz7g').val(), // Product Name Gas
        '00N1n00000Saz7f': $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
        '00N1n00000Saz7b': $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
        '00N1n00000Saz7c': $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

    };
};



// enter number in next block

function REQUEST( url, resType, opts ){
	opts = opts || {};
	return fetch( url, opts )
		.then(function( res ){
			return resType === 'none' ? res : res[ resType || 'json' ]() ;
		})
		.catch(console.error);
};

function addParam( key, val ){

	var keyVals = [];
	var exists;

	if( window.location.hash.indexOf('#?') > -1 ){
		var params = window.location.hash.replace('#?','').split('&') || [];
		params = params[0] === "" ? [] : params;
		keyVals = params.map(function( item ){
			var split = item.split('=');
			return { key : split[0], val : split[1] };
		});
		exists = keyVals.find(function( item ){ return item.key === key }) !== undefined;
	};
	
	if( exists ){
		keyVals = keyVals.map(function( item ){ return { key : item.key, val : val } });
	}else{
		keyVals.push({ key : key, val : val });
	};

	var queryParams = keyVals.map(function( item ){ return item.key+'='+item.val }).join('&');
	window.location.href = '#?'+queryParams;

};


$(document).ready(function()
{

	$('#circle').hover(function(){
		$('#downld-label').css('visibility', 'visible');
		$('#labelbackground').css('opacity', '1');
		}, function(){
			$('#downld-label').css('visibility', 'hidden');
		$('#labelbackground').css('opacity', '0');
	});

	// Contact page
	$('#cnt_email').click(function(){
		$('.email-modal').css("display","block");
		$('#popup-container').removeAttr('class').addClass('reveal');
		$('body').addClass('popup-active');
	});

	$('#cnt_message').click(function(){
		$('.popup').css("width","50%");
		$('.message-modal').css("display","block");
		$('#popup-container').removeAttr('class').addClass('reveal');
		$('body').addClass('popup-active');
	});
	  
	$('#close-email').click(function(){
		$('#popup-container').addClass('out');
		$('body').removeClass('popup-active');
		$('.email-modal').css("display","none");
		
	});

	$('#close-message').click(function(){
		$('#popup-container').addClass('out');
		$('body').removeClass('popup-active');
		$('.message-modal').css("display","none");
		$('.popup').css("width","unset");
	});


	// Careers - CV upload
	$('.cv').click(function(){
		var buttonId = $(this).attr('id');
		var jobrole = $(this);
		$('#popup-container').removeAttr('class').addClass(buttonId);
		$('body').addClass('popup-active');
		$('#jobrole').val(jobrole.val());
	});

	$('#close').click(function(){
		$('#popup-container').addClass('out');
		$('body').removeClass('popup-active');
	});


// get url parameter value
  var current = location.href;
	var energy = current.match(/energy/g);


// loader show/hide

setTimeout(function(){ $('.loading-page').hide();
 $('#main_content').show() },2000);
  //postcode quotation page
2
	if( document.querySelector('.postcode_top') ){
		$.ajax({
			url: base_url + 'index.php/quotation/new_quotefor',
			type: 'get',
			success: function (response) {
				addParam( 'title', 'fuelType' );
				$('#response').html(response);
			}
		});
	}

  $(document).on('submit','#form-postcode-simple',function(e){
		e.preventDefault();
		var form_postcode = $('#form-postcode').val();
		$.ajax({
				url: base_url+'quotation/quotefor',
				type: 'post',
				data: { postcode: form_postcode },
				beforeSend: function(){
					$('.loading-page').show();
				 	$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
				 	$('#main_content').show();
				},
				success:function(response)
				{
					addParam( 'title', 'address' );
					if( response == '2000' ) {
						$('#error_message').text('Postcode is not valid');
						$('.error_msg').show();
					}
					
					if( response == '2014' ) {
						console.log('WEIRD SHIT');
					}
					else {
						//$('#quotation_form #quotation_postcode').val( form_postcode );
						$('#response').html(response);
					}
				},
      error: function(jqXHR, textStatus, errorThrown){
          alert(textStatus);
      }
		});
  });

	$(document).on( 'click', '#back_postcode', function(){
		var postcode = $('#quotation_postcode').val();
		$.ajax({
				url: base_url+'index.php/quotation/address_list',
				type: 'get',
				beforeSend: function(){
					$('.loading-page').show();
				 	$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
				 	$('#main_content').show();
				},
				success:function(response)
				{
					$('#response').html(response);
					$('.md-form label').addClass('active');
					$('#form-postcode').val( postcode );
				}
		});
	});

	$(document).on('click', '.quotefor', function(){
		var select_quote = $(this).data('quotefor');
		// if( select_quote == 'electricity' ){
		$.ajax({
				url: base_url+'index.php/quotation/postcode',
				type: 'get',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response)
				{
					$('#quotation_form #quotation_for').val( select_quote );
					$('#response').html(response);
				}
		});
	});

	$(document).on( 'click', '#back_posty', function(){

        $('#quotation_form')[0].reset();
		var form_postcode = $('#quotation_postcode').val();

		$.ajax({
				url: base_url+'index.php/quotation/postcode',
				type: 'get',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response)
				{
					addParam( 'title', 'postcode' );
					$('#quotation_form #quotation_for').val( select_quote );
					$('#response').html(response);
				}
		});
	});

	$(document).on( 'click', '#back_posty', function(){
		var form_postcode = $('#quotation_postcode').val();
		$.ajax({
			url: base_url+'index.php/quotation/new_quotefor',
			type: 'post',
			data: { postcode: form_postcode },
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
				$('.md-form label').addClass('active');
				//	$('#form-postcode').val( form_postcode );
			},
			success:function(response){
				addParam( 'title', 'fuelType' );
				$('#response').html(response);
			}
		});
	});

	// BACK TO QUOTE FOR SECTION
	$(document).on( 'click', '#back_quotefor', function(){
		var form_postcode = $('#quotation_postcode').val();
		$.ajax({
			url: base_url+'index.php/quotation/postcode',
			type: 'post',
			data: { postcode: form_postcode },
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
				$('.md-form label').addClass('active');
				$('#form-postcode').val( form_postcode );
			},
			success:function(response){
				addParam( 'title', 'postcode' );
				$('#response').html(response);
			}
		});
	});

	$(document).on('submit','#cv',function(e)
	{
		var file_data = $('#cv_upload').prop('files')[0];
		
		if( file_data.type !== "application/pdf" && file_data.type !== "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && file_data.type !== "application/msword" && file_data.type !== "application/x-iwork-pages-sffpages" ){
			console.error('wrong file type selected.');
			return;
		};

		var form_data = new FormData();                  
		form_data.append('file', file_data);
		//alert(form_data);                             
		$.ajax({
			url: base_url+'index.php/career/savetofolder',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,                         
			type: 'post',
		});
	})


	$(document).on('submit','#cv',function(e)
	{
		e.preventDefault();

		var fullname = $('#cv_upload').val();
		var filename = fullname.split('\\');

		$.ajax({
			url: base_url+'index.php/career/email',
			type: 'post',
			data: $('#cv').serialize() + '&CV=' + filename[filename.length-1],
			
			success:function(data){
			if(data == 'OK') 
			{
				$('#message').html('<p>error</p>');
			} 
			else 
			{ 
				$('#message').html("<br><div class='alert alert-success' role='alert'><strong>Success! </strong>Thank you! We'll be in touch.</div>"); 
			}
			
			$('#cv')[0].reset();
			}
		});
	})


//check for mpan and mprn in database
$(document).on('change','#quotation_address',function(){
	var selected_address = $('#quotation_address').find(':selected').val();
	var selected_mpan = $(this).find(':selected').data('mpan');
	var selected_mprn = $(this).find(':selected').data('mprn');
	var mpanlower = $(this).find(':selected').data('mpanlower');


	var elecmeter = $(this).find(':selected').data('elecmeter');
	var gasmeter = $(this).find(':selected').data('gasmeter');

	if( mpanlower )
		{
			var error_return = '';
				$.ajax({
						url: base_url+'index.php/quotation/check_mpan',
						type: 'get',
						dataType: 'json',
						data: { mpan: mpanlower },
						success:function(response){
							$('#error_address_msg').empty();
							if( response.error == '1' ){
								$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert">'+ response.msg+'</div><center>');
								$('#continue_btn').hide();
								return false;
							}if(response.error == '0'  ){

								$('#continue_btn').show();
							}
						}
				});
		}

		if( selected_mprn )
			{
				var error_return = '';
					$.ajax({
							url: base_url+'index.php/quotation/check_mprn',
							type: 'get',
							dataType: 'json',
							data: { mprn: selected_mprn },
							success:function(response){
								$('#error_address_msg').empty();
								if( response.error == '1' ){
									$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert">'+ response.msg+'</div><center>');
									$('#continue_btn').hide();
									return false;
								}if(response.error == '0'  ){

									$('#continue_btn').show();
								}
							}
					});
			}

});


$(document).on('change','#quotation_address_gas',function(){
	var selected_address = $('#quotation_address').find(':selected').val();
	var selected_mpan = $(this).find(':selected').data('mpan');
	var selected_mprn_gas = $(this).find(':selected').data('mprn_gas');
	var mpanlower_gas = $(this).find(':selected').data('mpanlower_gas');


	var elecmeter = $(this).find(':selected').data('elecmeter');
	var gasmeter = $(this).find(':selected').data('gasmeter');

	if( mpanlower_gas )
		{
			var error_return = '';
				$.ajax({
						url: base_url+'index.php/quotation/check_mpan',
						type: 'get',
						dataType: 'json',
						data: { mpan: mpanlower_gas },
						success:function(response){
							$('#error_address_msg').empty();
							if( response.error == '1' ){
								$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert">'+ response.msg+'</div><center>');
								$('#continue_btn').hide();
								return false;
							}if(response.error == '0'  ){

								$('#continue_btn').show();
							}
						}
				});
		}

		if( selected_mprn_gas )
			{
				var error_return = '';
					$.ajax({
							url: base_url+'index.php/quotation/check_mprn',
							type: 'get',
							dataType: 'json',
							data: { mprn: selected_mprn_gas },
							success:function(response){
								$('#error_address_msg').empty();
								if( response.error == '1' ){
									$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert">'+ response.msg+'</div><center>');
									$('#continue_btn').hide();
									return false;
								}if(response.error == '0'  ){

									$('#continue_btn').show();
								}
							}
					});
			}

});


$(document).on('change','#quotation_address',function(){

	var selected_mpan = $(this).children('option:selected').data('mpan');
  var selected_mprn = $(this).children('option:selected').data('mprn');
  var mpanlower = $(this).children('option:selected').data('mpanlower');


  var elecmeter = $(this).children('option:selected').data('elecmeter');
  var gasmeter = $(this).children('option:selected').data('gasmeter');


  var singleorboth = $('input#quotation_for').val();

  if (singleorboth === 'both') {



	if ( !gasmeter && $(this).val() != 'noaddress' && $(this).val() != '0') {

		$('.gasdropdownlist').css('display','block');

	}
 }


   if ( !gasmeter && $(this).val() == 'noaddress' || !elecmeter && $(this).val() == 'noaddress') {

	$('.missingmeters').css('display','block');

	 $('.hidbtn').css('display','none');

	 if ( !gasmeter && $(this).val() == 'noaddress' ) {

		$('.missingmeters span.gsel').html('<span>gas</span>')

	 }

	 else if (!elecmeter && $(this).val() == 'noaddress') {

		$('.missingmeters span.gsel').html('<span>electric</span>')
	}
   } 

   else if (gasmeter && elecmeter) {
	   
   $('.missingmeters').css('display','none');
 
   $('.hidbtn').css('display','block');

   $('.gasdropdownlist').css('display','none');
   } 



});


$(document).on('change','#quotation_address_gas',function(){

	var selected_mpan = $(this).children('option:selected').data('mpan');
  var selected_mprn = $(this).children('option:selected').data('mprn');
  var mpanlower = $(this).children('option:selected').data('mpanlower');


  var elecmeter = $(this).children('option:selected').data('elecmeter');
  var gasmeter = $(this).children('option:selected').data('gasmeter_gas');


  var singleorboth = $('input#quotation_for').val();

  if (singleorboth === 'both') {



	if ( !gasmeter && $(this).val() != 'noaddress') {

		$('.gasdropdownlist').css('display','block');

	}

	
 }


   if ( !gasmeter && $(this).val() == 'noaddress' || !elecmeter && $(this).val() == 'noaddress') {

	$('.missingmeters').css('display','block');

	 $('.hidbtn').css('display','none');

	 if ( !gasmeter && $(this).val() == 'noaddress' ) {

		$('.missingmeters span.gsel').html('<span>gas</span>')

	 }

	 else if (!elecmeter && $(this).val() == 'noaddress') {

		$('.missingmeters span.gsel').html('<span>electric</span>')
	}
   } 

   else if (gasmeter ||  $(this).val() == '0' ) {
	   
   $('.missingmeters').css('display','none');
 
   $('.hidbtn').css('display','block');

 // $('.gasdropdownlist').css('display','none');
   } 



});

//get address by selection
$(document).on( 'submit', '#address_form', function(e){
	e.preventDefault();
	addParam( 'title', 'paymentType' );

	//alert();
	// return false;
		//var selected_address = $(this).val(); quotation_address



		var meterserial = $('#checkinput').val();

        var singleorboth = $('input#quotation_for').val();

		var selected_address = $('#quotation_address').find(':selected').val();

		var selected_bnum = $(this).find(':selected').data('bnum');
		var selected_subb = $(this).find(':selected').data('subb');
		var selected_bnam = $(this).find(':selected').data('bnam');
		var selected_thor = $(this).find(':selected').data('thor');
		var selected_dplo = $(this).find(':selected').data('dplo');
		var selected_town = $(this).find(':selected').data('town');
		var selected_cntys = $(this).find(':selected').data('cnty');

		var first_line_adds =  (selected_bnum || selected_subb || selected_bnam) + ' ' + selected_thor ;

		var selected_mpan = $(this).find(':selected').data('mpan');
		var selected_mprn = $(this).find(':selected').data('mprn');
		var mpanlower = $(this).find(':selected').data('mpanlower');
		var selectedpostcode = $(this).find(':selected').data('selectedpostcode');

		var elecmeter = $(this).find(':selected').data('elecmeter');
		var gasmeter = $(this).find(':selected').data('gasmeter');

		var gasmeter_gas = $('#quotation_address_gas').find(':selected').data('gasmeter_gas');
		var selected_mprn_gas = $('#quotation_address_gas').find(':selected').data('mprn_gas');

		var elecmeterinput =  $('#quotation_elec_meter_serial').val();
		var gasmeterinput = $('#quotation_gas_meter_serial').val();

		// Make sure postcode is valid
		var pattern = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})$/;
		var valid_postcode = $.trim(selectedpostcode).match(pattern) ? true : false;

		if(valid_postcode == false){
			var selectedpostcode = $('#quotation_address_gas').find(':selected').data('selectedpostcode_gas');
		};

		//check mpan or mprn in database
		//alert(mpanlower);
		var metertype = $(this).find(':selected').data('metertype');

		if (!metertype) {
			metertype = 'STD';
		} 

		if( selected_address == '0' || selected_address == null)
		{
			//alert('Please select valid option');
			$('#error_address_msg').empty();
			$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please select valid option</div><center>');
			return false;
		}else if( selected_mprn == '' && selected_mpan=="" )
		{
			$('#error_address_msg').empty();
			$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please select valid address.</div><center>');
			return false;
		}

       else if (singleorboth === 'both' && !elecmeter || singleorboth === 'both' && !gasmeter ) {

 	   if ( !elecmeter && $(this).val() == 'noaddress' && meterserial) {



	   $.ajax({
		url: base_url+'index.php/quotation/getmpan',
		type: 'get',
		dataType: 'json',
		data:{meterserial: meterserial, postcode: selectedpostcode},
		beforeSend: function(){
			$('.loading-page').show();
			 $('#main_content').hide();
		},
		complete: function(){
		
		},
		success:function(response)
		
		{

			if (response.success === 'success') {

			var checkedserial = response.elecserial;	
			var mpanLower_new = responce.mpanLower;
			var mpan = response.mpan;	
				var msg = response.msg;

				$('#quotation_form #quotation_address').val( selected_address );
				$('#quotation_form #quotation_first_line_address').val( first_line_adds );
				$('#quotation_form #quotation_dplo_address').val( selected_dplo );
				$('#quotation_form #quotation_town_address').val( selected_town );
				$('#quotation_form #quotation_city_address').val( selected_cntys );
				$('#quotation_form #quotation_postcode').val( selectedpostcode );
				$('#quotation_form #quotation_mprn').val( selected_mprn );
				$('#quotation_form #quotation_gas_meter_serial').val( gasmeter );
				

				$('#quotation_form #quotation_mpan').val( mpan );
				$('#quotation_form #meterTypeElec').val( metertype );
				$('#quotation_form #quotation_mpanlower').val( mpanLower_new );


				$('#quotation_form #quotation_elec_meter_serial').val( checkedserial );
	


				$('#error_address_msg').html("<div id='success_address_msg'>" +msg+ "</div>");

				
				$.ajax({
					url: base_url+'index.php/quotation/payEnergy',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						 $('#main_content').hide();
					},
					complete: function(){
						$('.loading-page').hide();
						 $('#main_content').show();
							 if( selected_mprn == null || selected_mprn == '' )
							{
								$('#gas_quote').css('display','none');
								$('#both_quote').css('display','none');
							}
							if( selected_mpan == '' || selected_mpan == null )
							{
								$('#elect_quote').hide();
								$('#both_quote').css('display','none');
							}
					},
					success:function(response)
					{							
							$('#response').html(response);
					}
					});
			}

			if (response.success === 'fail') {
				
				var msg = response.msg;
				
		
				
				$('#quotation_form input#errormsg').val(msg);

					$('#quotation_form #quotation_address').val( selected_address );
					$('#quotation_form #quotation_first_line_address').val( first_line_adds );
					$('#quotation_form #quotation_dplo_address').val( selected_dplo );
					$('#quotation_form #quotation_town_address').val( selected_town );
					$('#quotation_form #quotation_city_address').val( selected_cntys );
					$('#quotation_form #quotation_mpan').val( selected_mpan );
					$('#quotation_form #quotation_mprn').val( selected_mprn );
					$('#quotation_form #meterTypeElec').val( metertype );
					$('#quotation_form #quotation_mpanlower').val( mpanlower );
					$('#quotation_form #quotation_postcode').val( selectedpostcode );

					//alert(elecmeter); alert(gasmeter);

					$('#quotation_form #quotation_elec_meter_serial').val( elecmeter );
					$('#quotation_form #quotation_gas_meter_serial').val( gasmeter );


				$.ajax({
					url: base_url+'index.php/quotation/payEnergy',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						 $('#main_content').hide();
					},
					complete: function(){
						$('.loading-page').hide();
						 $('#main_content').show();
							 if( selected_mprn == null || selected_mprn == '' )
							{
								$('#gas_quote').css('display','none');
								$('#both_quote').css('display','none');
							}
							if( selected_mpan == '' || selected_mpan == null )
							{
								$('#elect_quote').hide();
								$('#both_quote').css('display','none');
							}
					},
					success:function(response)
					{							
							$('#response').html(response);
					}
					});
			 }
		}
		});

        } 

       if (!gasmeter && $('#quotation_address_gas').find(':selected').val() == 'noaddress' && meterserial) {
	   

		

		$.ajax({
			url: base_url+'index.php/quotation/getmprn',
			type: 'get',
			dataType: 'json',
			data:{meterserial: meterserial, postcode: selectedpostcode},
			beforeSend: function(){
				$('.loading-page').show();
				 $('#main_content').hide();
			},
			complete: function(){
			
			},
			success:function(response)
			
			{

				if (response.success === 'success') {

				var checkedserial = response.gasserial;	
				var mprn = response.mprn;	
           	 	var msg = response.msg;

					$('#quotation_form #quotation_address').val( selected_address );
					$('#quotation_form #quotation_first_line_address').val( first_line_adds );
					$('#quotation_form #quotation_dplo_address').val( selected_dplo );
					$('#quotation_form #quotation_town_address').val( selected_town );
					$('#quotation_form #quotation_city_address').val( selected_cntys );
					$('#quotation_form #quotation_mpan').val( selected_mpan );
					$('#quotation_form #meterTypeElec').val( metertype );
					$('#quotation_form #quotation_mpanlower').val( mpanlower );
					$('#quotation_form #quotation_postcode').val( selectedpostcode );
					$('#quotation_form #quotation_elec_meter_serial').val( elecmeter );

					//gas inputs needed
					$('#quotation_form #quotation_mprn').val( mprn );
					$('#quotation_form #quotation_gas_meter_serial').val(checkedserial);

					$('#error_address_msg').html("<div id='success_address_msg'>" +msg+ "</div>");

					
					$.ajax({
						url: base_url+'index.php/quotation/payEnergy',
						type: 'post',
						beforeSend: function(){
							$('.loading-page').show();
						 	$('#main_content').hide();
						},
						complete: function(){
							$('.loading-page').hide();
						 	$('#main_content').show();
						 		if( selected_mprn == null || selected_mprn == '' )
								{
									$('#gas_quote').css('display','none');
									$('#both_quote').css('display','none');
								}
								if( selected_mpan == '' || selected_mpan == null )
								{
									$('#elect_quote').hide();
									$('#both_quote').css('display','none');
								}
						},
						success:function(response)
						{							
								$('#response').html(response);
						}
						});
				}

				if (response.success === 'fail') {
					
					var msg = response.msg;
					
;
					
					$('#quotation_form input#errormsg').val(msg);

					$('#quotation_form #quotation_address').val( selected_address );
					$('#quotation_form #quotation_first_line_address').val( first_line_adds );
					$('#quotation_form #quotation_dplo_address').val( selected_dplo );
					$('#quotation_form #quotation_town_address').val( selected_town );
					$('#quotation_form #quotation_city_address').val( selected_cntys );
					$('#quotation_form #quotation_mpan').val( selected_mpan );
					$('#quotation_form #quotation_mprn').val( selected_mprn );
					$('#quotation_form #meterTypeElec').val( metertype );
					$('#quotation_form #quotation_mpanlower').val( mpanlower );
					$('#quotation_form #quotation_postcode').val( selectedpostcode );

					//alert(elecmeter); alert(gasmeter);

					$('#quotation_form #quotation_elec_meter_serial').val( elecmeter );
					$('#quotation_form #quotation_gas_meter_serial').val( gasmeter );


                	$.ajax({
						url: base_url+'index.php/quotation/payEnergy',
						type: 'post',
						beforeSend: function(){
							$('.loading-page').show();
						 	$('#main_content').hide();
						},
						complete: function(){
							$('.loading-page').hide();
						 	$('#main_content').show();
						 		if( selected_mprn == null || selected_mprn == '' )
								{
									$('#gas_quote').css('display','none');
									$('#both_quote').css('display','none');
								}
								if( selected_mpan == '' || selected_mpan == null )
								{
									$('#elect_quote').hide();
									$('#both_quote').css('display','none');
								}
						},
						success:function(response)
						{							
								$('#response').html(response);
						}
						});


				 }
			}
			});
	
	   } 

     if (gasmeter_gas) {
		$.ajax({
			url: base_url+'index.php/quotation/payEnergy',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				 $('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				 $('#main_content').show();
					 if( selected_mprn == null || selected_mprn == '' )
					{
						$('#gas_quote').css('display','none');
						$('#both_quote').css('display','none');
					}
					if( selected_mpan == '' || selected_mpan == null )
					{
						$('#elect_quote').hide();
						$('#both_quote').css('display','none');
					}
				},
				success:function(response)
				{
						$('#quotation_form #quotation_address').val( selected_address );
						$('#quotation_form #quotation_first_line_address').val( first_line_adds );
						$('#quotation_form #quotation_dplo_address').val( selected_dplo );
						$('#quotation_form #quotation_town_address').val( selected_town );
						$('#quotation_form #quotation_city_address').val( selected_cntys );
						$('#quotation_form #quotation_mpan').val( selected_mpan );
						$('#quotation_form #quotation_mprn').val( selected_mprn_gas );
						$('#quotation_form #meterTypeElec').val( metertype );
						$('#quotation_form #quotation_mpanlower').val( mpanlower );
						$('#quotation_form #quotation_postcode').val( selectedpostcode );
						$('#quotation_form #quotation_elec_meter_serial').val( elecmeter );
						$('#quotation_form #quotation_gas_meter_serial').val( gasmeter_gas );

						$('#response').html(response);
				}
				});
	}
}

		else{



				$.ajax({
						url: base_url+'index.php/quotation/payEnergy',
						type: 'post',
						beforeSend: function(){
							$('.loading-page').show();
						 	$('#main_content').hide();
						},
						complete: function(){
							$('.loading-page').hide();
						 	$('#main_content').show();
						 		if( selected_mprn == null || selected_mprn == '' )
								{
									$('#gas_quote').css('display','none');
									$('#both_quote').css('display','none');
								}
								if( selected_mpan == '' || selected_mpan == null )
								{
									$('#elect_quote').hide();
									$('#both_quote').css('display','none');
								}
						},
						success:function(response)
						{
								$('#quotation_form #quotation_address').val( selected_address );
								$('#quotation_form #quotation_first_line_address').val( first_line_adds );
								$('#quotation_form #quotation_dplo_address').val( selected_dplo );
								$('#quotation_form #quotation_town_address').val( selected_town );
								$('#quotation_form #quotation_city_address').val( selected_cntys );
								$('#quotation_form #quotation_mpan').val( selected_mpan );
								$('#quotation_form #quotation_mprn').val( selected_mprn );
								$('#quotation_form #meterTypeElec').val( metertype );
								$('#quotation_form #quotation_mpanlower').val( mpanlower );
								$('#quotation_form #quotation_postcode').val( selectedpostcode );

								//alert(elecmeter); alert(gasmeter);

								$('#quotation_form #quotation_elec_meter_serial').val( elecmeter );
								$('#quotation_form #quotation_gas_meter_serial').val( gasmeter );

								$('#response').html(response);
						}
						});
			}
});

// back to address selection screen
	$(document).on('click', '#back_address', function(){
		var quotation_mpan = $('#quotation_form #quotation_mpan').val( );
		var quotation_mprn = $('#quotation_form #quotation_mprn').val( );
		var meterTypeElec=  $('#quotation_form #meterTypeElec').val(  );
		$.ajax({
			url: base_url+'index.php/quotation/address_list',
			type: 'get',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
				if( quotation_mprn == null || quotation_mprn == '' )
				{
					$('#gas_quote').css('display','none');
					$('#both_quote').css('display','none');
				}
				if( quotation_mpan == '' || quotation_mpan == null )
				{
					$('#elect_quote').hide();
					$('#both_quote').css('display','none');
				}
			},
			success:function(response){
				addParam( 'title', 'address' );
				$('#response').html(response);
			}
		});
	});

// get pay energy value
	$(document).on('click', '.pay_energy', function(){
		$('#quotation_form').attr( 'data-payment-type', $(this).attr('data-payment-type') );
		var selected_pay_energy = $(this).data('valuepayenergy');
		$.ajax({
			url: base_url+'index.php/quotation/current_energy',
			type: 'post',

			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
			},
			success:function(response)
			{
				addParam( 'title', 'energyUsage' );
				$('#quotation_pay_energy').val(selected_pay_energy);
				$('#desiredPayTypes').val(selected_pay_energy);
				$('#response').html(response);
			}
		});

	});

// desire_pay selection desire_pay
$(document).on('click', '.desire_pay', function(){
	var selected_pay_energy = $(this).data('desire_pay');
		$.ajax({
				url: base_url+'index.php/quotation/know_supplier',
				//url: base_url+'index.php/quotation/get_desire_pay',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response)
				{
					$('#desiredPayTypes').val(selected_pay_energy);
						$('#response').html(response);
				}
			});

});

// back from desire pay
$(document).on('click', '#backtoprepay', function(){

	$.ajax({
		url: base_url+'index.php/quotation/payEnergy',
		//url: base_url+'index.php/quotation/get_desire_pay',
		type: 'post',
		beforeSend: function(){
			$('.loading-page').show();
			$('#main_content').hide();
		},
		complete: function(){
			$('.loading-page').hide();
			$('#main_content').show();
		},
		success:function(response){
			addParam( 'title', 'paymentType' );
			$('#response').html(response);
		}
	});

});


$(document).on('click', '#backtonumberof', function(){
	//var selected_pay_energy = $(this).data('desire_pay');
	//alert();
		$.ajax({
				url: base_url+'index.php/quotation/payEnergy',
				//url: base_url+'index.php/quotation/get_desire_pay',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response)
				{
					//$('#desiredPayTypes').val(selected_pay_energy);
						$('#response').html(response);
				}
			});

});

// back from prepay
$(document).on('click','#backfromprepay', function(){

	backpage = $('#backpage').val();

	if (!backpage){
		$.ajax({
			url: base_url+'index.php/quotation/number_bedroom',
			//url: base_url+'index.php/quotation/get_desire_pay',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
	}

	if(backpage =='bedroom'){

		$.ajax({
			url: base_url+'index.php/quotation/home_type',
			//url: base_url+'index.php/quotation/get_desire_pay',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
	}

	if(backpage =='kwh'){

		$.ajax({
			url: base_url+'index.php/quotation/enter_energy',
			//url: base_url+'index.php/quotation/get_desire_pay',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
	}


	
	if(backpage =='spend'){

		$.ajax({
			url: base_url+'index.php/quotation/enter_spend',
			//url: base_url+'index.php/quotation/get_desire_pay',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
			},
			success:function(response)
			{
					$('#response').html(response);
			}
		});
	}

});

// value if current Supplier known
	$(document).on('click', '.know_current_energy', function(){
		var know_current_energy = $(this).data('know_current_energy');
		if(know_current_energy == 'no'){
			$.ajax({
					url: base_url+'index.php/quotation/current_energy',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
					 	$('#main_content').hide();
					},
					complete: function(){
						$('.loading-page').hide();
					 	$('#main_content').show();
					},
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}
		if( know_current_energy == 'yes' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			var quotation_for = $('#quotation_for').val();

			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
					 	$('#main_content').hide();
					},
					complete: function () {
						$('.loading-page').hide();
					 	$('#main_content').show();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						var quotation_for = $('#quotation_for').val();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
        },
					success:function(response)
					{
						$('#quotation_know_energy').val(know_current_energy);
						$('#response').html(response);
					}
				});
		}

	});

// back to know supplier
/**	$(document).on('click','#back_known_supplier', function(){
		$.ajax({
				url: base_url+'index.php/quotation/know_supplier',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});
/** */
	$(document).on('click','#back_known_supplier', function(){
		$('#quotation_form').trigger('submit');
	});

	$(document).on('click','#back_known, .back_known', function(){
		var know_supplier_value = $('#quotation_know_energy').val();
		var quotation_for = $('#quotation_for').val();

		$('#quotation_supplier').val('');
		$('#quotation_supplier_id').val('');
		$('#quotation_tariff').val('');

		$('#gas_selected_supplier').val('');
		$('#gas_supplier_id').val('');
		$('#gas_selected_tariff').val('');

		
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();
			$.ajax({
					url: base_url+'index.php/quotation/select_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
					 	$('#main_content').hide();
					},
					complete: function () {
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_supplier').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elect_supplier').hide();
						// }
						$('.loading-page').hide();
					 	$('#main_content').show();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
							$('#response').html(response);
					}
				});
		
	/**	if( know_supplier_value == 'no' ){
			$.ajax({
					url: base_url+'index.php/quotation/know_supplier',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						$('#main_content').hide();
					},
					complete: function(){
						$('.loading-page').hide();
						$('#main_content').show();
					},
					success:function(response)
					{
						$('#response').html(response);
					}
				});
		}
		*/
	});

//get selected supplier for Tariff
	$(document).on('change','#elec_selected_supplier', function(){
		var elec_supplier = $(this).val();
		var quotation_for = $('#quotation_for').val();
		$.ajax({
				url: base_url+'index.php/quotation/get_tariff',
				type: 'post',
				dataType: 'json',
				data: { elec_supplier: elec_supplier, quotation_for: quotation_for, tariff_fuelType: 'electricity' },
				beforeSend: function()
				{
					$('#elec_tariff_list').html('<option>Loading...</option>');
				},
				success:function(response)
				{
					$('#quotation_supplier').val(elec_supplier);
					if( response.error == '0' ){
						$('#elec_tariff_list').empty();
						var response_data = response.data;
							if( response.data.length == 0  )
							{
								$('#elec_tariff_list').html( '<option value="0">Please Select valid supplier</option>' );
							}else {
								$('#elec_tariff_list').html( '<option value="0">Please Select Tariff</option>' );
								for( i=0; i<response_data.length; i++ )
								{

											$('#elec_tariff_list').append( '<option value="'+response_data[i].tariffName+'" data-fuelType="'+response_data[i].fuelType+'">'+response_data[i].tariffName+'</option>' );
								}
							}
					}
					if(response.error == '1'){
						$('#elec_tariff_list').empty();
						$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
					if(response.error == '2'){
						$('#elec_tariff_list').empty();
							$('#elec_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
				}
			});
	});

//get selected supplier for Tariff
	$(document).on('change','#gas_selected_supplier', function(){
		var elec_supplier = $(this).val();
		var quotation_for = $('#quotation_for').val();
		$.ajax({
				url: base_url+'index.php/quotation/get_tariff',
				type: 'post',
				dataType: 'json',
				data: { elec_supplier: elec_supplier, quotation_for: quotation_for, tariff_fuelType: 'gas' },
				beforeSend: function()
				{
					$('#gas_tariff_list').html('<option>Loading...</option>');
				},
				success:function(response)
				{
					$('#gas_selected_supplier').val(elec_supplier);
					if( response.error == '0' ){
						$('#gas_tariff_list').empty();
						var response_data = response.data;
							if( response.data.length == 0  )
							{
								$('#gas_tariff_list').html( '<option value="0">Please Select valid supplier</option>' );
							}else {
								$('#gas_tariff_list').html( '<option value="0">Please Select Tariff</option>' );
								for( i=0; i<response_data.length; i++ )
								{
											$('#gas_tariff_list').append( '<option value="'+response_data[i].tariffName+'" data-fuelType="'+response_data[i].fuelType+'">'+response_data[i].tariffName+'</option>' );
								}
							}
					}
					if(response.error == '1'){
						$('#gas_tariff_list').empty();
						$('#gas_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
					if(response.error == '2'){
						$('#gas_tariff_list').empty();
							$('#gas_tariff_list').html( '<option value="0">'+response.data+'</option>' );
					}
				}
			});
	});

// get selected tariff and supplier
	$(document).on('click','#supplier_submit',function(){
		var selected_supplier = $('#elec_selected_supplier').find(':selected').val();
		var selected_tariff = $('#elec_tariff_list').find(':selected').val();
		var selected_tariff_fuelType = $('#elec_tariff_list').find(':selected').data('fueltype');
		var supplier_id = $('#elec_selected_supplier').find(':selected').data('supplierid');

		//gas
		var gas_selected_supplier = $('#gas_selected_supplier').find(':selected').val();
		var gas_selected_tariff = $('#gas_tariff_list').find(':selected').val();
		var gas_selected_tariff_fuelType = $('#gas_tariff_list').find(':selected').data('fueltype');
		var gas_supplier_id = $('#gas_selected_supplier').find(':selected').data('supplierid');

		if( $('#quotation_for').val() == 'both' )
		{
				if( selected_supplier == 0 || selected_tariff == 0 || gas_selected_supplier == 0 || gas_selected_tariff == 0 )
				{
					$('#error_address_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/current_energy',
							type: 'post',
							beforeSend: function(){
								$('.loading-page').show();
								$('#main_content').hide();
							},
							complete:function()
							{
								$('.loading-page').hide();
								$('#main_content').show();
									$('#quotation_supplier').val(selected_supplier);
									$('#quotation_tariff').val(selected_tariff);
									$('#quotation_supplier_id').val(supplier_id);
									$('#quotation_know_energy').val('yes');
									$('#selected_tariff_fuelType').val(selected_tariff_fuelType);

									// gas
									$('#gas_selected_supplier').val(gas_selected_supplier);
									$('#gas_selected_tariff').val(gas_selected_tariff);
									$('#gas_supplier_id').val(gas_supplier_id);
									$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');



							},
							success:function(response)
							{

								$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');
								$('#response').html(response);
							}
						});
				}
		}
		if( $('#quotation_for').val() == 'gas' ){
			if(  gas_selected_supplier == 0 || gas_selected_tariff == 0 )
				{
					alert('Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/current_energy',
							type: 'post',
							beforeSend: function(){
								$('.loading-page').show();
								$('#main_content').hide();
							},
							complete:function()
							{
								$('.loading-page').hide();
								$('#main_content').show();
									// $('#quotation_supplier').val(selected_supplier);
									// $('#quotation_tariff').val(selected_tariff);
									// $('#quotation_supplier_id').val(supplier_id);
									// $('#selected_tariff_fuelType').val(selected_tariff_fuelType);

									// gas
									$('#gas_selected_supplier').val(gas_selected_supplier);
									$('#gas_selected_tariff').val(gas_selected_tariff);
									$('#gas_supplier_id').val(gas_supplier_id);
									$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');
							},
							success:function(response)
							{
								$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');
								$('#response').html(response);
							}
						});
				}
		}
		if( $('#quotation_for').val() == 'electricity' ){
			if(  selected_supplier == 0 || selected_tariff == 0 )
				{
					alert('Please select valid and supplier and tariff');
				}
				else {
					$.ajax({
							url: base_url+'index.php/quotation/current_energy',
							type: 'post',
							beforeSend: function(){
								$('.loading-page').show();
								$('#main_content').hide();
							},
							complete:function()
							{
								$('.loading-page').hide();
								$('#main_content').show();
									$('#quotation_supplier').val(selected_supplier);
									$('#quotation_tariff').val(selected_tariff);
									$('#quotation_supplier_id').val(supplier_id);
									$('#selected_tariff_fuelType').val(selected_tariff_fuelType);
									$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');
							},
							success:function(response)
							{
								$('.backtohearabout').attr('id', 'back_known');
								$('#back_known').removeClass('backtohearabout');
								$('#response').html(response);
							}
						});
				}
		}
	});

//get value of hear_about_us
	$(document).on('click', '.hear_about_us', function(){
		var headabout = $(this).data('hearabout');
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function(){
					$('.loading-page').hide();
					$('#main_content').show();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});

//know current usage
	$(document).on('click', '.know_current_usage',function(){
		var know_current_usage = $(this).data('know_current_usage');
		var quotation_for = $('#quotation_for').val();
		//alert(quotation_for);
		if( know_current_usage == 'kwh' ){
			var check_mprn = $('#quotation_mprn').val();
			var check_mpan = $('#quotation_mpan').val();

			$.ajax({
					url: base_url+'index.php/quotation/enter_energy',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						$('#main_content').hide();
					},
					complete: function()
					{
						$('.loading-page').hide();
						$('#main_content').show();
						// if( check_mprn == null || check_mprn == '' )
						// {
						// 	$('#gas_usage').css('display','none');
						// }
						// if( check_mpan == '' || check_mpan == null )
						// {
						// 	$('#elec_usage').css('display','none');
						// }

						if( quotation_for == 'electricity' ){
							$('#gas_usage').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_usage').css('display','none');
						}
					},
					success:function(response)
					{

						addParam( 'title', 'actualConsumption' );
						$('#quotation_current_energy_usage').val(know_current_usage);

						$('#existingSpendElec').val('');
						$('#existingSpendIntervalElec').val('');
						$('#existingSpendIntervalGas').val('');
						$('#existingSpendGas').val('');

						$('#backpage').val('kwh');

						$('#response').html(response);
					}
				});
		}

		if( know_current_usage == 'spend' ){
			$.ajax({
					url: base_url+'index.php/quotation/enter_spend',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						$('#main_content').hide();
					},
					complete: function()
					{
						$('.loading-page').hide();
						$('#main_content').show();
						if( quotation_for == 'electricity' ){
							$('#gas_spend').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_spend').css('display','none');
						}
					},
					success:function(response)
					{
						$('#quotation_current_energy_usage').val(know_current_usage);

						$('#quotation_elec_energy_usage').val('');
						$('#quotation_elec_energy_interval').val('');
						$('#quotation_gas_energy_usage').val('');
						$('#quotation_gas_energy_interval').val('');
						
						$('#backpage').val('spend');

						$('#response').html(response);
					}
				});
		}


		if(know_current_usage == 'no'){

			$.ajax({
					url: base_url+'index.php/quotation/home_type',
					type: 'post',
					beforeSend: function(){
						$('.loading-page').show();
						$('#main_content').hide();
					},
					complete: function()
					{
						$('.loading-page').hide();
						$('#main_content').show();
						if( quotation_for == 'electricity' ){
							$('#gas_supplier').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elect_supplier').css('display','none');
						}
					},
					success:function(response)
					{
						addParam( 'title', 'propertyType' );
						$('#quotation_current_energy_usage').val(know_current_usage);


						$('#existingSpendElec').val('');
						$('#existingSpendIntervalElec').val('');
						$('#existingSpendIntervalGas').val('');
						$('#existingSpendGas').val('');

						$('#backpage').val('bedroom');

						$('#response').html(response);
					}
				});
		}
	});

// back to current energy
	$(document).on('click','#backtocurrentenergy', function(){
		var headabout = $('#quotation_hearabout').val();
		$.ajax({
				url: base_url+'index.php/quotation/current_energy',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete: function()
				{
					$('.loading-page').hide();
	$('#main_content').show();
					$('#quotation_hearabout').val( headabout );
				},
				success:function(response)
				{
						$('#response').html(response);
				}
			});
	});


// back to hear about us
	$(document).on('click','.backtoknowsupp',function(){
		$.ajax({
				//url: base_url+'index.php/quotation/know_supplier',
				url: base_url+'index.php/quotation/select_supplier',
			
				type: 'post',
				beforeSend: function(){
	$('.loading-page').show();
	$('#main_content').hide();
},
				complete:function()
				{
					$('.loading-page').hide();
					$('#main_content').show();
					//	$('#quotation_supplier').val(selected_supplier);
					//	$('#quotation_tariff').val(selected_tariff);
				},
				success:function(response)
				{
					$('#response').html(response);
				}
			});
	})

//back to know energy usage_select
	$(document).on('click','.backknowusage', function(){
		var headabout = $('#quotation_hearabout').val();
		$.ajax({
			url: base_url+'index.php/quotation/current_energy',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function(){
				$('.loading-page').hide();
				$('#main_content').show();
				$('#quotation_hearabout').val( headabout );
			},
			success:function(response){
				addParam( 'title', 'energyUsage' );
				$('#response').html(response);
			}
		});
	});

	$(document).on('click', '#backtohousetype', function(){
		
		var quotation_for = $('#quotation_for').val();
		var know_current_usage = $(this).data('know_current_usage');

		$.ajax({
			url: base_url+'index.php/quotation/home_type',
			type: 'post',
			beforeSend: function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete: function()
			{
				$('.loading-page').hide();
				$('#main_content').show();
				if( quotation_for == 'electricity' ){
					$('#gas_supplier').css('display','none');
				}
				if( quotation_for == 'gas' ){
					$('#elect_supplier').css('display','none');
				}
			},
			success:function(response)
			{
				addParam( 'title', 'propertyType' );
				$('#quotation_current_energy_usage').val(know_current_usage);
				$('#existingSpendElec').val('');
				$('#existingSpendIntervalElec').val('');
				$('#existingSpendIntervalGas').val('');
				$('#existingSpendGas').val('');
				$('#backpage').val('bedroom');
				$('#response').html(response);
			}
		});

	});

//get energy usage
	$(document).on('click','#enter_usage_form',function(){
		addParam( 'title', 'summary' );
		var elec_usage = $('#enter_elec_usage').val();
		var elec_usage_select = $('.elec_usage_select').find(':selected').val();
		var energyusage = 'kwh_usage'; // kuljeet

		//gas
		var gas_enter_usage = $('#enter_gas_usage').val();
		var gas_interval = $('.gas_usage_select').find(':selected').val();

		//get supplier and tariff of electricity
		var quotation_supplier = $('#quotation_supplier').val();
		var quotation_tariff = $('#quotation_tariff').val();
		var quotation_pay_energy = $('#quotation_pay_energy').val();

		//get gas supplier and tariff
		var gas_supplier = $('#gas_selected_supplier').val();
		var gas_tariff = $('#gas_selected_tariff').val();
		var gas_supplierid = $('#gas_supplier_id').val();

		if( $('#quotation_for').val() == 'electricity' ){
			if( !elec_usage || elec_usage_select == "0" ){
				$('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please fill in all fields');
			}else{
				$('#quotation_elec_energy_usage').val(elec_usage)
				$('#quotation_elec_energy_interval').val(elec_usage_select);
				$('#summarypage').val(energyusage); //kuljeet
				$('#back_bedroom').addClass('backtobedroom');

				$('#quotation_form').trigger('submit');




				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { elec_usage: elec_usage, elec_usage_select: elec_usage_select },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}
		}

		if( $('#quotation_for').val() == 'gas' ){
			if( !gas_enter_usage || gas_interval == "0" ){
				alert('All fields are requied');
			}else{
				$('#quotation_gas_energy_usage').val(gas_enter_usage);
				$('#quotation_gas_energy_interval').val(gas_interval);

				$('#back_bedroom').addClass('backtobedroom');
				$('#summarypage').val(energyusage); // kuljeet
				$('#quotation_form').trigger('submit');


				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { gas_enter_usage: gas_enter_usage, gas_interval: gas_interval },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}
		}
		if( $('#quotation_for').val() == 'both' ){
			if( !elec_usage || elec_usage_select == "0" || !gas_enter_usage || gas_interval == "0" ){
				$('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please fill in all fields');
			}else{
				$('#quotation_elec_energy_usage').val(elec_usage);
				$('#quotation_elec_energy_interval').val(elec_usage_select);
				//gas
				$('#quotation_gas_energy_usage').val(gas_enter_usage)
				$('#quotation_gas_energy_interval').val(gas_interval);
				$('#back_bedroom').addClass('backtobedroom');
				$('#summarypage').val(energyusage);
				
				$('#quotation_form').trigger('submit');


				// $.ajax({
				// 	url: base_url+'index.php/quotation/enter_spend',
				// 	type: 'post',
				// 	data: { elec_usage: elec_usage, elec_usage_select: elec_usage_select, gas_enter_usage: gas_enter_usage, gas_interval: gas_interval },
				// 	success:function(response){
				// 		$('#response').html(response);
				// 	}
				// });
			}
		}


	});


	$(document).on('submit','#enter_spend_form', function(e){
		e.preventDefault();
		var elec_spend = $('#output_elec_range').val();
		var elec_spend_interval = $('#elec_spend_interval').find(':selected').val();
		var energyspend = ('enery_spend');
		var gas_spend = $('#output_gas_range').val();
		var gas_spend_interval = $('#gas_spend_interval').find(':selected').val();
		//alert(elec_spend_interval+'==='+gas_spend_interval);

				if( $('#quotation_for').val() == 'electricity' ){
					if( !elec_spend ){
							$('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please fill in all fields');
						}else{
							$('#existingSpendElec').val(elec_spend);
							$('#existingSpendIntervalElec').val(elec_spend_interval);
							$('#summarypage').val(energyspend);
							$('#quotation_form').trigger('submit');
						}

				}
				if( $('#quotation_for').val() == 'gas' ){
					if( !gas_spend ){
							$('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please fill in all fields');
						}else{
							$('#existingSpendGas').val(gas_spend);
							$('#existingSpendIntervalGas').val(gas_spend_interval);
							$('#summarypage').val(energyspend);
							$('#quotation_form').trigger('submit');
						}
				}
				if( $('#quotation_for').val() == 'both' ){
						if( !elec_spend || !gas_spend ){
							$('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Please fill in all fields');
						}else{
							$('#existingSpendElec').val(elec_spend);
							$('#existingSpendIntervalElec').val(elec_spend_interval);
							//gas
							$('#existingSpendGas').val(gas_spend)
							$('#existingSpendIntervalGas').val(gas_spend_interval);
							$('#summarypage').val(energyspend);
							$('#quotation_form').trigger('submit');
						}
				}
	});

//get static energy usage
	$(document).on('click','.home_type',function(){
			var typeOfHomeElec = $(this).data('hometype');
			// get number of people template
			$.ajax({
				url: base_url+'index.php/quotation/number_bedroom',
				type: 'post',
				beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){
					$('.loading-page').hide();
					$('#main_content').show();
					$('#typeOfHomeElec').val(typeOfHomeElec);
				},
				success:function(response){
					addParam( 'title', 'bedNumber' );
					$('#response').html(response);
				}
			});
	});


// select how many people live 25_may kuljeet
/*
$(document).on('click','.bedroom_range', function(){
	var numberbedroom = $(this).data('numberbedroom');
	var selectbedroom = ('select_bed');
	if( numberbedroom == '0' || numberbedroom ==' ' || numberbedroom=='undefined' || numberbedroom=='undefine' ){
		alert('Please select valid option');
	}else{
		$.ajax({
			url: base_url+'index.php/quotation/people_live',
			type: 'post',
			beforeSend:function(){
				$('.loading-page').show();
				$('#main_content').hide();
			},
			complete:function(){
				$('.loading-page').hide();
				$('#main_content').show();
				$('#bedroomsElec').val( numberbedroom );
	         	$('#summarypage').val(selectbedroom);
			},success:function(response){

				$('#response').html(response);

			}
		});
	}

});

**/
$(document).on('click','.bedroom_range', function(){
	addParam( 'title', 'summary' );
	var numberbedroom = $(this).data('numberbedroom');
	var hometype = $('#typeOfHomeElec').val();
	var selectbedroom = ('select_bed');
	if( numberbedroom == '0' || numberbedroom ==' ' || numberbedroom=='undefined' || numberbedroom=='undefine' ){
		alert('Please select valid option');
	}else{
		

		var elec_usage = $('#enter_elec_usage').val();
		var elec_usage_select = $('.elec_usage_select').find(':selected').val();
		var energyusage = 'kwh_usage'; // kuljeet

		//gas
		var gas_enter_usage = $('#enter_gas_usage').val();
		var gas_interval = $('.gas_usage_select').find(':selected').val();

		//get supplier and tariff of electricity
		var quotation_supplier = $('#quotation_supplier').val();
		var quotation_tariff = $('#quotation_tariff').val();
		var quotation_pay_energy = $('#quotation_pay_energy').val();

		//get gas supplier and tariff
		var gas_supplier = $('#gas_selected_supplier').val();
		var gas_tariff = $('#gas_selected_tariff').val();
		var gas_supplierid = $('#gas_supplier_id').val();

		var supplierelec = $('#quotation_supplier').val();
		var suppliergas = $('#gas_selected_supplier').val();


		if( $('#quotation_for').val() == 'electricity' ){


			if( numberbedroom == '1' ) {
                if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(1600);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_elec_energy_usage').val(1700);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(1800);
                }
                else{ // Detached
                    $('#quotation_elec_energy_usage').val(1900);
                }
				$('#quotation_elec_energy_interval').val('year');
				$('#quotation_know_energy').val('yes');
				$('#quotation_current_energy_usage').val('kwh');
				$('#bedroomsElec').val('1');
				$('#summarypage').val(energyusage); //kuljeet

				if (!supplierelec) {
					$('#quotation_supplier').val('British Gas');
					$('#quotation_supplier_id').val('5');
					$('#quotation_tariff').val('Standard');
				}
	
				$('#quotation_form').trigger('submit');
			}

			if( numberbedroom == '2' ) {
                if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(2800);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_elec_energy_usage').val(2900);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(3000);
                }
                else{ // Detached
                    $('#quotation_elec_energy_usage').val(3100);
                }
				$('#bedroomsElec').val('2');
				$('#quotation_elec_energy_interval').val('year');
				$('#quotation_know_energy').val('yes');
				$('#quotation_current_energy_usage').val('kwh');
				$('#summarypage').val(energyusage); //kuljeet

				if (!supplierelec) {
				$('#quotation_supplier').val('British Gas');
				$('#quotation_supplier_id').val('5');
				$('#quotation_tariff').val('Standard');
				}

				$('#quotation_form').trigger('submit');
			}

			if( numberbedroom == '3' ) {
                if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(4300);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_elec_energy_usage').val(4400);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(4500);
                }
                else{ // Detached
                    $('#quotation_elec_energy_usage').val(4600);
                }

				$('#bedroomsElec').val('3');
				
				$('#quotation_elec_energy_interval').val('year');
				$('#quotation_know_energy').val('yes');
				$('#quotation_current_energy_usage').val('kwh');
				$('#summarypage').val(energyusage); //kuljeet

				if (!supplierelec) {

				$('#quotation_supplier').val('British Gas');
				$('#quotation_supplier_id').val('5');
				$('#quotation_tariff').val('Standard');

				}

				$('#quotation_form').trigger('submit');
			}
			}
		
		if( $('#quotation_for').val() == 'both' ){


			if( numberbedroom == '1' ) {
                if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(1600);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_elec_energy_usage').val(1700);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(1800);
                }
                else{ // Detached
                    $('#quotation_elec_energy_usage').val(1900);
                }
				$('#quotation_elec_energy_interval').val('year');
				$('#bedroomsElec').val('1');
				//gas
                if(hometype == '3'){ // Flat
                    $('#quotation_gas_energy_usage').val(7400);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_gas_energy_usage').val(7600);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_gas_energy_usage').val(7800);
                }
                else{ // Detached
                    $('#quotation_gas_energy_usage').val(8000);
                }
                $('#quotation_gas_energy_interval').val('year');
				$('#quotation_current_energy_usage').val('kwh');
				$('#quotation_know_energy').val('yes');

				if (!supplierelec) {
				$('#quotation_supplier').val('British Gas');
				$('#quotation_supplier_id').val('5');
				$('#quotation_tariff').val('Standard');
				}
				
				if (!suppliergas) {
				$('#gas_selected_supplier').val('British Gas');
				$('#gas_supplier_id').val('5');
				$('#gas_selected_tariff').val('Standard');
				}
				
				
				$('#summarypage').val(energyusage);
				
				$('#quotation_form').trigger('submit'); 
			}

			if( numberbedroom == '2' ) {

				$('#bedroomsElec').val('2');
				if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(2800);
				}
				else if(hometype == '5'){ // Terrace
					$('#quotation_elec_energy_usage').val(2900);
                }
				else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(3000);
                }
				else{ // Detached
                    $('#quotation_elec_energy_usage').val(3100);
				}
				$('#quotation_elec_energy_interval').val('year');
				//gas
                if(hometype == '3'){ // Flat
                    $('#quotation_gas_energy_usage').val(11400);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_gas_energy_usage').val(11600);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_gas_energy_usage').val(11800);
                }
                else{ // Detached
                    $('#quotation_gas_energy_usage').val(12000);
                }

               
				$('#quotation_gas_energy_interval').val('year');
				$('#quotation_current_energy_usage').val('kwh');
				$('#quotation_know_energy').val('yes');

				if (!supplierelec) {
				$('#quotation_supplier').val('British Gas');
				$('#quotation_supplier_id').val('5');
				$('#quotation_tariff').val('Standard');
				}

				if (!suppliergas) {
				$('#gas_selected_supplier').val('British Gas');
				$('#gas_supplier_id').val('5');
				$('#gas_selected_tariff').val('Standard');
				}

				$('#summarypage').val(energyusage);
				
				$('#quotation_form').trigger('submit'); 
			}

			if( numberbedroom == '3' ) {

				$('#bedroomsElec').val('3');
                if(hometype == '3'){ // Flat
                    $('#quotation_elec_energy_usage').val(4300);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_elec_energy_usage').val(4400);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_elec_energy_usage').val(4500);
                }
                else{ // Detached
                    $('#quotation_elec_energy_usage').val(4600);
                }
                $('#quotation_elec_energy_usage').val();
				$('#quotation_elec_energy_interval').val('year');
				//gas
                if(hometype == '3'){ // Flat
                    $('#quotation_gas_energy_usage').val(16400);
                }
                else if(hometype == '5'){ // Terrace
                    $('#quotation_gas_energy_usage').val(16600);
                }
                else if(hometype == '4'){ // Semi
                    $('#quotation_gas_energy_usage').val(16800);
                }
                else{ // Detached
                    $('#quotation_gas_energy_usage').val(17000);
                }

                $('#quotation_gas_energy_usage').val()
				$('#quotation_gas_energy_interval').val('year');
				$('#quotation_current_energy_usage').val('kwh');
				$('#quotation_know_energy').val('yes');

				if (!supplierelec) {
				$('#quotation_supplier').val('British Gas');
				$('#quotation_supplier_id').val('5');
				$('#quotation_tariff').val('Standard');
			}

			if (!suppliergas) {	
				$('#gas_selected_supplier').val('British Gas');
				$('#gas_supplier_id').val('5');
				$('#gas_selected_tariff').val('Standard');
			}
				$('#summarypage').val(energyusage);
				
				$('#quotation_form').trigger('submit'); 
			}
	}
	}
});

// select how many bedroom
$(document).on('click','.people_live', function(){

	var peopleElec = $(this).data('numberpeople');
	if( peopleElec == '0' || peopleElec ==' ' || peopleElec=='undefined' || peopleElec=='undefine' ){
		alert('Please select valid option');
	}
	else{
		
		$('#peopleElec').val( peopleElec );
		$('#quotation_form').trigger('submit');
		$('#backtopeople').addClass('backtopeople');


	}

});


//get number of people and redirect to bedroom template.
	// $(document).on('submit','#formpeoplelive',function(e){
	// 	e.preventDefault();
	// 	if( $('#js-amount-input').val() == '0' || $('#js-amount-input').val() == ' ' ){
	// 		alert('Please select valid number of people');
	// 	}else{
	// 		var selected_people = $('#js-amount-input').val();
	// 		$.ajax({
	// 			url: base_url+'index.php/quotation/number_bedroom',
	// 			type: 'post',
	// 			beforeSend:function(){
	// 				$('.loading-page').show();
	// 				$('#main_content').hide();
	// 			},
	// 			complete:function(){
	// 				$('.loading-page').hide();
	// 				$('#main_content').show();
	// 				$('#peopleElec').val(selected_people);
	// 			},success:function(response){
	// 				$('#response').html(response);
	// 			}
	// 		});
	// 	}
	// })

// back to home type template
	$(document).on('click','#backtohometype',function(){
		$.ajax({
			url: base_url+'index.php/quotation/home_type',
			type: 'post',
			beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	});

	//back to bedroom form summary
	$(document).on('click','.select_bed',function(){
		$.ajax({
			url: base_url+'index.php/quotation/number_bedroom',
			type: 'post',
			beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	});


	//back to bedroom form energy
	$(document).on('click','.kwh_usage',function(){
		var quotation_for = $('#quotation_for').val();
		//alert(quotation_for);
		$.ajax({
			url: base_url+'index.php/quotation/enter_energy',
			type: 'post',
			beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){

						if( quotation_for == 'electricity' ){
							$('#gas_usage').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_usage').css('display','none');
						}
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	});



	//back to bedroom form spend
	$(document).on('click','.enery_spend',function(){
		var quotation_for = $('#quotation_for').val();
		//alert(quotation_for);
		$.ajax({
			url: base_url+'index.php/quotation/enter_spend',
			type: 'post',
			beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){
					if( quotation_for == 'electricity' ){
							$('#gas_spend').css('display','none');
						}
						if( quotation_for == 'gas' ){
							$('#elec_spend').css('display','none');
						}
						setTimeout( function() { $('.loading-page').hide();
					$('#main_content').show(); }, 1500 );

				},
				success:function(response){
					$('#response').html(response);
				}
		});
	});

// get back to people live
	$(document).on('click','#backtopeople', function(){
		$.ajax({
			url: base_url+'index.php/quotation/people_live',
			type: 'post',
			beforeSend: function(){
					$('.loading-page').show();
					$('#main_content').hide();
				},
				complete:function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response){
					$('#response').html(response);
				}
		});
	})

//submit quotation values
	$(document).on('submit', '#quotation_form', function(e){
			e.preventDefault();
	//alert($('#summarypage').val());
			// $.ajax({
			// 		url: base_url+'index.php/quotation/summary',
			// 		type: 'post',
			// 		data: $('#quotation_form').serialize(),
			// 		complete: function()
			// 		{
			// 			$('#quotation_hearabout').val( headabout );
			// 		},
			// 		success:function(response)
			// 		{
			// 				$('#response').html(response);
			// 		}
			// 	});
			var elec_usage = $('#enter_elec_usage').val();
			var elec_usage_select = $('.elec_usage_select').find(':selected').val();
			//get supplier and tariff
			var quotation_supplier = $('#quotation_supplier').val();
			var quotation_tariff = $('#quotation_tariff').val();
			var quotation_pay_energy = $('#quotation_pay_energy').val();
			var get_class=$('#summarypage').val();

			$.ajax({
					url: base_url+'index.php/quotation/summary',
					type: 'post',
					data: $('#quotation_form').serialize() + "&elec_usage="+elec_usage+ "&quotation_supplier="+ quotation_supplier + "&quotation_tariff=" +quotation_tariff + "&quotation_pay_energy="+quotation_pay_energy,
				
					beforeSend: function(){
						$('.loading-page').show();
						$('#main_content').hide();
					
					},
					complete: function()
					{
						$('.loading-page').hide();
						$('#main_content').show();
						$('#main-bak-dd #back_bedroom').addClass(get_class);
						$('#media-mobile-m #back_bedroom').addClass(get_class);
					//	$('#quotation_hearabout').val( headabout );
					},
					success:function(response)
					{
						$('#media-mobile-m #back_bedroom').addClass(get_class);
						$('#main-bak-dd #back_bedroom').addClass(get_class);

							$('.backend_error').empty();
							if( response == '3042' ){
								$('.backend_error').html('<center><div class="alert alert-danger" role="alert"><strong>Error! </strong>Unfortunately we do not have a tariff available for the search criteria you have provided </div><center>');
							}else if(response == '2000'){
								$('.backend_error').html('<center><div class="alert alert-danger" role="alert"><strong>Error! </strong>Validation error</div><center>');
							}
							else {
								$('#response').html(response);
							}
					}
				});
	});


	$(document).on('click', '.home_service_plan', function(){
			var get_homeserviceprice = $(this).data('homeserviceprice');
			var plan_number = $(this).data('planid');
			var orginial_price = $('#orginial_price').val()
			var total_price = (parseFloat(orginial_price) + parseFloat(get_homeserviceprice)).toFixed(2);
			$('#total_price').text(total_price);

			$('#plan_price').val(get_homeserviceprice);
			$('#complete_price').val(total_price);
			$('#plan_id').val(plan_number);
			$('#original_price').val(orginial_price);

			$('#select_plan_'+plan_number).css('display','none');
			$('#selected_plan_'+plan_number).css('display','block');
	});


	
$(document).on('submit','#quote_summary_form',function(e){
	 	e.preventDefault();
		window.location.href=base_url+'index.php/user/signup';
	 });


$(document).on('change','#value_elec_range',function(){
	$('#output_elec_range').val($(this).val());
});

$(document).on('change','#value_gas_range',function(){
	$('#output_gas_range').val($(this).val());
});





$(document).on('click','#continue_to_home_service', function(){
	$('#summary_div').hide();
	$('#home_service_option').show();
});

$(document).on('click','#summary_backup_div', function(){
	$('#summary_div').show();
	$('#home_service_option').hide();
});

$('.faq-contact-icons').hide();
	
	$('.faq-contact-help').click(function() {
		$('.faq-contact-help').hide();
		$('.faq-contact-icons').addClass("slide-icons");
		$('.faq-contact-icons').show();
		
	});

});



// Submit contact us form and send email
$(document).on('submit','#contact_us',function(e) {

	e.preventDefault();

	$.ajax({
		url: base_url+'index.php/contact_us/email',
		type: 'post',
		data: $('#contact_us').serialize(),

		success:function(data){

			if(data == 'OK') {

				$('#message').html('<p>error</p>');

			} else {

				$('#message').html("<br><div class='alert alert-success' role='alert'><strong>Success! </strong>Thank you! We'll be in touch.</div>"); }
			$('#contact_us')[0].reset();

		}
	});

});

function logout_user() {
	
    $.ajax({
        url: base_url + 'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href =  base_url + 'index.php/user/login';
            }
        }
    });

}


// click to mute/unmuted videos on front page
$(".frontVid").prop('muted', true);

$(".frontVid").click(function () {

    if ($(this).prop('muted')) {
        $(this).prop('muted', false);
       

    } else {
        $(this).prop('muted', true);
      
    }
});

$(".frontVid2").prop('muted', true);

$(".frontVid2").click(function () {
    if ($(this).prop('muted')) {
        $(this).prop('muted', false);
       

    } else {
        $(this).prop('muted', true);
      
    }
});

$(".famVid").prop('muted', true);

$(".famVid").click(function () {
    if ($(this).prop('muted')) {
        $(this).prop('muted', false);
       

    } else {
        $(this).prop('muted', true);
      
    }
});
