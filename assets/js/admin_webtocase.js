if(window.location.hostname === 'localhost' || window.location.host === '52.56.247.49') {
    var base_url = 'http://'+window.location.host+'/';
}
if( window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'eversmartenergy.co.uk' || window.location.hostname === '52.56.76.183') {
    var base_url = 'https://'+window.location.hostname+'/';
}

function logout_user()
{
    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

// validation - only numbers
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function numberDecimals(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function toDate(selector)
{
    var from = $(selector).val().split("-");
    return from[2] +'/'+ from[1] +'/'+ from[0];
}

function REQUEST( url, resType, opts )
{
    opts = opts || {};
    return fetch( url, opts )
        .then(function( res ){
            return resType === 'none' ? res : res[ resType || 'json' ] ;
        })
        .catch(console.error);
}

$( document ).ready(function()
{
    $('#dyball_id').hide();
    $('#00N1n00000SB9Pc').attr('disabled',true);
    $('#subject').attr('readonly',true);
    $('#description').attr('readonly',true);

    $('#00N1n00000Saz7f').val('ESE-STDVAR-E');
    $('#00N1n00000Saz7g').val('ESE-STDVAR-G');
});

$('input').each(function(){
    $(this).attr('spellcheck', false);
});

$('#JD_switch').on( 'change', function( evt ){
    if(this.checked) {
        evt.preventDefault();
        $('#junifer_id').show();
        $('#dyball_id').hide();
        $('[name=recordType]').val( '0121n0000007GkH' );
        $('#00N1n00000SB9NR').val('');
    } else {
        $('#dyball_id').show();
        $('#junifer_id').hide();
        $('[name=recordType]').val( '0121n0000007GkE' );
        $('#00N1n00000SB9OK').val('');
    }
});

$('#DE_switch').on( 'change', function( evt ){
    if(this.checked) {
        evt.preventDefault();
        $('#dual_switch').show();
    } else {
        $('#dual_switch').hide();
        $('#00N1n00000Saz7e').val('');
        $('#00N1n00000Saz7c').val('');
        $('#00N1n00000SajNo').val('');
        $('#00N1n00000SajNs').val('');
        $('#00N1n00000SajNq').val('');
        $('#00N1n00000SajNu').val('');
        $('#00N1n00000SajNw').val('');
        $('#00N1n00000SakSK').val('');
        $('#supp_start_gas').val('');
        $('#supp_end_gas').val('');
    }
});

$('#00N1n00000SB9PS, #00N1n00000SB9OK').on( 'change', function(){
    if($('#00N1n00000SB9PS').val() !== '' && $('#00N1n00000SB9OK').val() !== ''){
        $('#00N1n00000SB9Pc').removeAttr( 'disabled' );
    } else {
        $('#00N1n00000SB9Pc').attr( 'disabled', 'disabled');
        $('#subject').val('');
        $('#description').val('');
    }
});

$('#00N1n00000SB9Pc').on( 'change', function(){
    var status = $(this).val();
    if(status === 'default') {
        $('#subject').val('');
        $('#description').val('');
    } else if(status === 'Registration Pending') {
        $('#subject').val('Registration Pending - Junifer Customer - Pending');
        $('#description').val('Registration Pending for ' + $('#00N1n00000SB9PS').val());
    } else {
        $('#subject').val('Registration Success - Junifer Customer ID: ' + $('#00N1n00000SB9OK').val() + ' - Success');
        $('#description').val('Registration Success for ' + $('#00N1n00000SB9PS').val().replace(/\b\w/g, l => l.toUpperCase()));
    }
});

$('#00N1n00000Sb2k2').on( 'change', function(){
    var tariff = $(this).val();
    if($('#DE_switch').prop('checked')) {
        if (tariff === 'Simply Smart') {
            $('#00N1n00000Saz7f').val('ESE-STDVAR-E');
            $('#00N1n00000Saz7g').val('ESE-STDVAR-G');
        }

        if (tariff === 'Family Saver Club') {
            $('#00N1n00000Saz7f').val('FSC1R-F12-NOV2019-E');
            $('#00N1n00000Saz7g').val('FSC1R-F12-NOV2019-G');
        }

        if (tariff === 'Safeguard PAYG') {
            $('#00N1n00000Saz7f').val('');
            $('#00N1n00000Saz7g').val('');
        }
    } else {
        $('#00N1n00000Saz7g').val('');
    }
});

$('#clear-form').click(function(e) {
    e.preventDefault();
    $('input').each(function (i, el) {
        $(el).val('');
    });

    $('select').each(function (i, el) {
        $(this)[0].selectedIndex = 0;
    });
});

$('#submit-form').click(function(e){
    e.preventDefault();

    var Errors = [];
    if($('#webtocase #00N1n00000SB9PS').val() == '') { Errors += ' - Please enter a customer name\n'; }
    if($('#webtocase #email').val() == '') { Errors += ' - Please enter an email address\n'; }
    if($('#webtocase #phone').val() == '') { Errors += ' - Please enter a phone number\n'; }
    if($('#webtocase #dob').val() == '') { Errors += ' - Please enter a date of birth\n'; }
    if($('#webtocase #00N1n00000SB9PI').val() == '') { Errors += ' - Please enter an address\n'; }
    if($('#webtocase #00N1n00000SB9Ph').val() == '') { Errors += ' - Please enter a postcode\n'; }
    if($('#webtocase #junifer_customer_id').val() == '') { Errors += ' - A Junifer ID is required\n'; }
    if($('#webtocase #00N1n00000SB9Pc').val() == 'default') { Errors += ' - Please select a Registration Status\n'; }

    if($('#webtocase #00N1n00000Saz7d').val() == '') { Errors += ' - Please enter an MPAN \n'; }
    if($('#webtocase #00N1n00000Saz7b').val() == '') { Errors += ' - Please enter a Meter Serial Number (Elec)\n'; }
    if($('#webtocase #00N1n00000SajNn').val() == '') { Errors += ' - Please enter an EAC\n'; }
    if($('#webtocase #00N1n00000SajNr').val() == '') { Errors += ' - Please enter the Unit Rate (Elec)\n'; }
    if($('#webtocase #00N1n00000SajNp').val() == '') { Errors += ' - Please enter the Standing Charge (Elec)\n'; }
    if($('#webtocase #00N1n00000SajNt').val() == '') { Errors += ' - Please enter the Estimated Annual Cost (Elec)\n'; }
    if($('#webtocase #supp_start_elec').val() == '') { Errors += ' - Please enter the Supply Start Date (Elec)\n'; }
    if($('#webtocase #supp_end_elec').val() == '') { Errors += ' - Please enter the Supply End Date (Elec)\n'; }

    if($('#DE_switch').prop('checked')) {
        if($('#webtocase #00N1n00000Saz7e').val() == '') { Errors += ' - Please enter an MPRN \n'; }
        if($('#webtocase #00N1n00000Saz7c').val() == '') { Errors += ' - Please enter a Meter Serial Number (Gas)\n'; }
        if($('#webtocase #00N1n00000SajNo').val() == '') { Errors += ' - Please enter an AQ\n'; }
        if($('#webtocase #00N1n00000SajNs').val() == '') { Errors += ' - Please enter the Unit Rate (Gas)\n'; }
        if($('#webtocase #00N1n00000SajNq').val() == '') { Errors += ' - Please enter the Standing Charge (Gas)\n'; }
        if($('#webtocase #00N1n00000SajNu').val() == '') { Errors += ' - Please enter the Estimated Annual Cost (Gas)\n'; }
        if($('#webtocase #supp_start_gas').val() == '') { Errors += ' - Please enter the Supply Start Date (Gas)\n'; }
        if($('#webtocase #supp_end_gas').val() == '') { Errors += ' - Please enter the Supply End Date (Gas)\n'; }
    }

    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {
        var new_dob = toDate('#dob');
        var new_start_elec = toDate('#supp_start_elec');
        var new_end_elec = toDate('#supp_end_elec');

        if ($('#supp_start_gas').val() !== '') {
            var new_start_gas = toDate('#supp_start_gas');
            $('#00N1n00000SajNw').val(new_start_gas);
        } else {
            var new_start_gas = ''
        }
        if ($('#supp_start_gas').val() !== '') {
            var new_end_gas = toDate('#supp_end_gas');
            $('#00N1n00000SakSK').val(new_end_gas);
        } else {
            var new_end_gas = '';
        }

        $('#00N1n00000SB9PN').val(new_dob);
        $('#00N1n00000SajNv').val(new_start_elec);
        $('#00N1n00000SakSF').val(new_end_elec);

        // WEB TO CASE FIELDS
        $('#webToCaseAdmin #orgid').val($('#webtocase #org_id option:selected').val());     // org id
        $('#webToCaseAdmin #00N1n00000SB9PS').val($('#webtocase #00N1n00000SB9PS').val());  // full name
        $('#webToCaseAdmin #wtc_email').val($('#webtocase #wtc_email').val());              // email
        $('#webToCaseAdmin #phone').val($('#webtocase #phone').val());                      // phone
        $('#webToCaseAdmin #00N1n00000SB9PN').val((new_dob));                               // dob
        $('#webToCaseAdmin #00N1n00000SB9PI').val($('#webtocase #00N1n00000SB9PI').val());  // address1
        $('#webToCaseAdmin #00N1n00000SB9Ph').val($('#webtocase #00N1n00000SB9Ph').val());  // postcode
        $('#webToCaseAdmin #00N1n00000SB9OK').val($('#webtocase #00N1n00000SB9OK').val());  // junifer id
        $('#webToCaseAdmin #00N1n00000SB9NR').val($('#webtocase #00N1n00000SB9NR').val());  // dyball id
        $('#webToCaseAdmin #00N1n00000SB9Pc').val($('#webtocase #00N1n00000SB9Pc option:selected').text());  // status
        $('#webToCaseAdmin #subject').val($('#webtocase #subject').val());                  // subject
        $('#webToCaseAdmin #description').val($('#webtocase #description').val());          // desc
        $('#webToCaseAdmin #00N1n00000Sb2kW').val($('#webtocase #00N1n00000Sb2kW option:selected').text());  // bill frequency
        $('#webToCaseAdmin #00N1n00000Sb2k2').val($('#webtocase #00N1n00000Sb2k2 option:selected').text());  // tariff
        $('#webToCaseAdmin #recordType').val($('#webtocase #recordType option:selected').text());            // record type

        // ELEC
        $('#webToCaseAdmin #00N1n00000Saz7d').val($('#webtocase #00N1n00000Saz7d').val());  // mpan
        $('#webToCaseAdmin #00N1n00000Saz7b').val($('#webtocase #00N1n00000Saz7b').val());  // msn
        $('#webToCaseAdmin #00N1n00000SajNn').val($('#webtocase #00N1n00000SajNn').val());  // eac
        $('#webToCaseAdmin #00N1n00000SajNr').val($('#webtocase #00N1n00000SajNr').val());  // ur
        $('#webToCaseAdmin #00N1n00000SajNp').val($('#webtocase #00N1n00000SajNp').val());  // sc
        $('#webToCaseAdmin #00N1n00000SajNt').val($('#webtocase #00N1n00000SajNt').val());  // elec cost
        $('#webToCaseAdmin #00N1n00000SajNv').val(new_start_elec);                          // start date
        $('#webToCaseAdmin #00N1n00000SakSF').val(new_end_elec);                            // end date
        $('#webToCaseAdmin #00N1n00000Saz7f').val($('#webtocase #00N1n00000Saz7f').val());  // product name

        // GAS
        $('#webToCaseAdmin #00N1n00000Saz7e').val($('#webtocase #00N1n00000Saz7e').val());  // mprn
        $('#webToCaseAdmin #00N1n00000Saz7c').val($('#webtocase #00N1n00000Saz7c').val());  // msn
        $('#webToCaseAdmin #00N1n00000SajNo').val($('#webtocase #00N1n00000SajNo').val());  // aq
        $('#webToCaseAdmin #00N1n00000SajNs').val($('#webtocase #00N1n00000SajNs').val());  // ur
        $('#webToCaseAdmin #00N1n00000SajNq').val($('#webtocase #00N1n00000SajNq').val());  // sc
        $('#webToCaseAdmin #00N1n00000SajNu').val($('#webtocase #00N1n00000SajNu').val());  // gas cost
        $('#webToCaseAdmin #00N1n00000SajNw').val(new_start_gas);                           // start date
        $('#webToCaseAdmin #00N1n00000SakSK').val(new_end_gas);                             // end date
        $('#webToCaseAdmin #00N1n00000Saz7g').val($('#webtocase #00N1n00000Saz7g').val());  // product name

        var body = {};
        $('#webtocase').find('input, select').each(function( i, item ){
            if(typeof $(this).attr('data-webtocase-name') !== "undefined" && $(this).val() !== '' ) {
                    body[$(this).attr('data-webtocase-name')] = $(this).val();
            }
        });

        Promise.all([
            REQUEST( base_url + 'index.php/user/web_to_case_log', 'none', { // Log web to case form
                method : 'post',
                headers : new Headers({ 'Content-Type' : 'application/json' }),
                body : JSON.stringify( body )
            }),
            REQUEST( 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', { // Send the web to case form
                method : 'post',
                mode: 'no-cors',
                headers : new Headers({
                    'Content-Type' : 'application/x-www-form-urlencoded'
                }),
                body : $('#webToCaseAdmin').serialize()
            })
        ])
        .then(function(){
            $('#clear-form').click();
            $('#wtc_outcome').html('<div style="text-align:center" class="alert alert-success" role="alert"><p style="text-align:center;font-size:30px;font-weight:500;"> Web to case submitted </p></div>');
        })
    }
});